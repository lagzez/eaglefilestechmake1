<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="con-jst">
<description>&lt;b&gt;J.S.T. Connectors&lt;/b&gt;&lt;p&gt;
J.S.T Mfg Co.,Ltd.&lt;p&gt;
http://www.jst-mfg.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SM06B-SRSS-TB">
<description>&lt;b&gt;Shrouded Header, side entry type&lt;/b&gt;</description>
<wire x1="-3.873" y1="-1.473" x2="-3.873" y2="2.523" width="0.254" layer="51"/>
<wire x1="-3.873" y1="2.523" x2="3.873" y2="2.523" width="0.254" layer="51"/>
<wire x1="3.873" y1="2.523" x2="3.873" y2="-1.473" width="0.254" layer="51"/>
<wire x1="3.873" y1="-1.473" x2="-3.873" y2="-1.473" width="0.254" layer="51"/>
<wire x1="-3.873" y1="0.5" x2="-3.873" y2="2.523" width="0.254" layer="21"/>
<wire x1="-3.873" y1="2.523" x2="-3.5" y2="2.523" width="0.254" layer="21"/>
<wire x1="3.873" y1="0.5" x2="3.873" y2="2.523" width="0.254" layer="21"/>
<wire x1="3.873" y1="2.523" x2="3.5" y2="2.523" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-1.473" x2="2.5" y2="-1.473" width="0.254" layer="21"/>
<smd name="1" x="-2.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-1.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="-0.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="0.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="2.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="FIT@1" x="-3.8" y="-0.9" dx="1.2" dy="1.8" layer="1"/>
<smd name="FIT@2" x="3.8" y="-0.9" dx="1.2" dy="1.8" layer="1"/>
<text x="-5.08" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.35" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="BM06B-SRSS-TB">
<description>&lt;b&gt;Shrouded Header, top entry type&lt;/b&gt;</description>
<wire x1="-3.873" y1="1.473" x2="-3.873" y2="-1.427" width="0.254" layer="51"/>
<wire x1="-3.873" y1="-1.427" x2="3.873" y2="-1.427" width="0.254" layer="51"/>
<wire x1="3.873" y1="-1.427" x2="3.873" y2="1.473" width="0.254" layer="51"/>
<wire x1="3.873" y1="1.473" x2="-3.873" y2="1.473" width="0.254" layer="51"/>
<wire x1="-3.873" y1="-0.5" x2="-3.873" y2="-1.427" width="0.254" layer="21"/>
<wire x1="-3.873" y1="-1.427" x2="-3.5" y2="-1.427" width="0.254" layer="21"/>
<wire x1="3.873" y1="-0.5" x2="3.873" y2="-1.427" width="0.254" layer="21"/>
<wire x1="3.873" y1="-1.427" x2="3.5" y2="-1.427" width="0.254" layer="21"/>
<wire x1="-2.5" y1="1.473" x2="2.5" y2="1.473" width="0.254" layer="21"/>
<smd name="6" x="-2.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="-1.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="-0.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="0.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="1.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="1" x="2.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="FIT@1" x="3.8" y="0.9" dx="1.2" dy="1.8" layer="1"/>
<smd name="FIT@2" x="-3.8" y="0.9" dx="1.2" dy="1.8" layer="1"/>
<text x="-6.35" y="1.905" size="1.27" layer="25" rot="R270">&gt;NAME</text>
<text x="5.08" y="1.905" size="1.27" layer="27" rot="R270">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="KV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.524" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="K">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="?M06B-SRSS-TB" prefix="X">
<description>&lt;b&gt;Disconnectable Crimp style connector, 1.0mm pitch&lt;/b&gt;&lt;br&gt;6 contacts</description>
<gates>
<gate name="-1" symbol="KV" x="0" y="7.62" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="K" x="0" y="5.08" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="K" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="K" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="K" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-6" symbol="K" x="0" y="-5.08" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="S" package="SM06B-SRSS-TB">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="BM06B-SRSS-TB">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="diode">
<description>&lt;b&gt;Diodes&lt;/b&gt;&lt;p&gt;
Based on the following sources:
&lt;ul&gt;
&lt;li&gt;Motorola : www.onsemi.com
&lt;li&gt;Fairchild : www.fairchildsemi.com
&lt;li&gt;Philips : www.semiconductors.com
&lt;li&gt;Vishay : www.vishay.de
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="P6T15">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
diameter 9 mm, horizontal, grid 15.24 mm</description>
<wire x1="-4.699" y1="-4.572" x2="-4.699" y2="4.572" width="0.1524" layer="21"/>
<wire x1="4.699" y1="4.572" x2="4.699" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="4.572" x2="4.699" y2="4.572" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.572" x2="-4.699" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0" x2="6.223" y2="0" width="1.27" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.223" y2="0" width="1.27" layer="51"/>
<wire x1="-0.381" y1="0.508" x2="-0.381" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="0.381" y1="-0.508" x2="0.381" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.381" y1="-0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="0" y2="0" width="0.1524" layer="21"/>
<pad name="C" x="-7.62" y="0" drill="1.4986" shape="long"/>
<pad name="A" x="7.62" y="0" drill="1.4986" shape="long"/>
<text x="-4.6736" y="4.8514" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.3622" y="-3.302" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-4.572" x2="-2.921" y2="4.572" layer="21"/>
<rectangle x1="4.699" y1="-0.635" x2="5.969" y2="0.635" layer="21"/>
<rectangle x1="-5.969" y1="-0.635" x2="-4.699" y2="0.635" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SCHOTTKY">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.286" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="5KPXX" prefix="D">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
transient voltage suppressor</description>
<gates>
<gate name="1" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P6T15">
<connects>
<connect gate="1" pin="A" pad="A"/>
<connect gate="1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VDD">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.905" x2="0" y2="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VDD" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VDD" prefix="VDD">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VDD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun">
<packages>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762"/>
<pad name="2" x="2.5" y="0" drill="0.762"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-1206">
<wire x1="-1" y1="1" x2="-2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1" x2="1" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="0" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="0.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0.6" x2="-0.3" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="0" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="-0.889" y="1.397" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.778" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LED-0603">
<wire x1="0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="-0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="0" y1="0.17" x2="0.2338" y2="-0.14" width="0.2032" layer="21"/>
<wire x1="-0.0254" y1="0.1546" x2="-0.2184" y2="-0.14" width="0.2032" layer="21"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.6985" y="-0.889" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="1.0795" y="-1.016" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="LED10MM">
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7" shape="square"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
</package>
<package name="SSOP28DB">
<description>&lt;b&gt;Small Shrink Outline Package&lt;/b&gt;</description>
<wire x1="-5.2" y1="2.925" x2="5.2" y2="2.925" width="0.1524" layer="21"/>
<wire x1="5.2" y1="2.925" x2="5.2" y2="-2.925" width="0.1524" layer="21"/>
<wire x1="5.2" y1="-2.925" x2="-5.2" y2="-2.925" width="0.1524" layer="21"/>
<wire x1="-5.2" y1="-2.925" x2="-5.2" y2="2.925" width="0.1524" layer="21"/>
<wire x1="-5.038" y1="2.763" x2="5.038" y2="2.763" width="0.0508" layer="27"/>
<wire x1="5.038" y1="2.763" x2="5.038" y2="-2.763" width="0.0508" layer="27"/>
<wire x1="5.038" y1="-2.763" x2="-5.038" y2="-2.763" width="0.0508" layer="27"/>
<wire x1="-5.038" y1="-2.763" x2="-5.038" y2="2.763" width="0.0508" layer="27"/>
<circle x="-4.225" y="-1.95" radius="0.4596" width="0.1524" layer="21"/>
<smd name="28" x="-4.225" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="27" x="-3.575" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="26" x="-2.925" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="25" x="-2.275" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="24" x="-1.625" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="23" x="-0.975" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="22" x="-0.325" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="20" x="0.975" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="21" x="0.325" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="19" x="1.625" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="18" x="2.275" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="17" x="2.925" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="16" x="3.575" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="15" x="4.225" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="1" x="-4.225" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="2" x="-3.575" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="3" x="-2.925" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="4" x="-2.275" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="5" x="-1.625" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="6" x="-0.975" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="7" x="-0.325" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="8" x="0.325" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="9" x="0.975" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="10" x="1.625" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="11" x="2.275" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="12" x="2.925" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="13" x="3.575" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="14" x="4.225" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<text x="-3.81" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="0" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-4.3875" y1="2.9656" x2="-4.0625" y2="3.9" layer="51"/>
<rectangle x1="-4.3875" y1="-3.9" x2="-4.0625" y2="-2.9656" layer="51"/>
<rectangle x1="-3.7375" y1="-3.9" x2="-3.4125" y2="-2.9656" layer="51"/>
<rectangle x1="-3.0875" y1="-3.9" x2="-2.7625" y2="-2.9656" layer="51"/>
<rectangle x1="-3.7375" y1="2.9656" x2="-3.4125" y2="3.9" layer="51"/>
<rectangle x1="-3.0875" y1="2.9656" x2="-2.7625" y2="3.9" layer="51"/>
<rectangle x1="-2.4375" y1="2.9656" x2="-2.1125" y2="3.9" layer="51"/>
<rectangle x1="-1.7875" y1="2.9656" x2="-1.4625" y2="3.9" layer="51"/>
<rectangle x1="-1.1375" y1="2.9656" x2="-0.8125" y2="3.9" layer="51"/>
<rectangle x1="-0.4875" y1="2.9656" x2="-0.1625" y2="3.9" layer="51"/>
<rectangle x1="0.1625" y1="2.9656" x2="0.4875" y2="3.9" layer="51"/>
<rectangle x1="0.8125" y1="2.9656" x2="1.1375" y2="3.9" layer="51"/>
<rectangle x1="1.4625" y1="2.9656" x2="1.7875" y2="3.9" layer="51"/>
<rectangle x1="2.1125" y1="2.9656" x2="2.4375" y2="3.9" layer="51"/>
<rectangle x1="2.7625" y1="2.9656" x2="3.0875" y2="3.9" layer="51"/>
<rectangle x1="3.4125" y1="2.9656" x2="3.7375" y2="3.9" layer="51"/>
<rectangle x1="4.0625" y1="2.9656" x2="4.3875" y2="3.9" layer="51"/>
<rectangle x1="-2.4375" y1="-3.9" x2="-2.1125" y2="-2.9656" layer="51"/>
<rectangle x1="-1.7875" y1="-3.9" x2="-1.4625" y2="-2.9656" layer="51"/>
<rectangle x1="-1.1375" y1="-3.9" x2="-0.8125" y2="-2.9656" layer="51"/>
<rectangle x1="-0.4875" y1="-3.9" x2="-0.1625" y2="-2.9656" layer="51"/>
<rectangle x1="0.1625" y1="-3.9" x2="0.4875" y2="-2.9656" layer="51"/>
<rectangle x1="0.8125" y1="-3.9" x2="1.1375" y2="-2.9656" layer="51"/>
<rectangle x1="1.4625" y1="-3.9" x2="1.7875" y2="-2.9656" layer="51"/>
<rectangle x1="2.1125" y1="-3.9" x2="2.4375" y2="-2.9656" layer="51"/>
<rectangle x1="2.7625" y1="-3.9" x2="3.0875" y2="-2.9656" layer="51"/>
<rectangle x1="3.4125" y1="-3.9" x2="3.7375" y2="-2.9656" layer="51"/>
<rectangle x1="4.0625" y1="-3.9" x2="4.3875" y2="-2.9656" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="FT232R">
<wire x1="-10.16" y1="20.32" x2="10.16" y2="20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="20.32" x2="10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="-20.32" x2="-10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-20.32" x2="-10.16" y2="20.32" width="0.254" layer="94"/>
<text x="-7.62" y="20.828" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<pin name="RESET" x="-15.24" y="-5.08" length="middle" direction="in" function="dot"/>
<pin name="OSCI" x="-15.24" y="0" length="middle" direction="in"/>
<pin name="OSCO" x="-15.24" y="-2.54" length="middle" direction="out"/>
<pin name="DSR" x="15.24" y="2.54" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="DCD" x="15.24" y="0" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="RI" x="15.24" y="-2.54" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="3V3OUT" x="-15.24" y="7.62" length="middle" direction="out"/>
<pin name="USBDM" x="-15.24" y="17.78" length="middle"/>
<pin name="USBDP" x="-15.24" y="15.24" length="middle"/>
<pin name="GND7" x="-15.24" y="-12.7" length="middle" direction="pwr"/>
<pin name="GND18" x="-15.24" y="-15.24" length="middle" direction="pwr"/>
<pin name="GND21" x="-15.24" y="-17.78" length="middle" direction="pwr"/>
<pin name="TXD" x="15.24" y="17.78" length="middle" direction="out" rot="R180"/>
<pin name="RXD" x="15.24" y="15.24" length="middle" direction="in" rot="R180"/>
<pin name="VCCIO" x="-15.24" y="5.08" length="middle" direction="pwr"/>
<pin name="AGND" x="-15.24" y="-10.16" length="middle" direction="pwr"/>
<pin name="TEST" x="-15.24" y="-7.62" length="middle" direction="in"/>
<pin name="VCC" x="-15.24" y="10.16" length="middle" direction="pwr"/>
<pin name="TXLED" x="15.24" y="-7.62" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="RXLED" x="15.24" y="-10.16" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="RTS" x="15.24" y="7.62" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="CTS" x="15.24" y="10.16" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="DTR" x="15.24" y="5.08" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="PWREN" x="15.24" y="-12.7" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="TXDEN" x="15.24" y="-15.24" length="middle" direction="in" rot="R180"/>
<pin name="SLEEP" x="15.24" y="-17.78" length="middle" direction="in" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LEDs&lt;/b&gt;
Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. 5mm - Spark Fun Electronics SKU : COM-00529 (and others)</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FT232RL" prefix="IC">
<description>&lt;b&gt;USB UART&lt;/b&gt;
FT232RL 4&lt;sup&gt;th&lt;/sup&gt; Generation USB UART (USB &amp;lt;-&amp;gt; Serial) Controller. Spark Fun Electronics SKU : COM-00650</description>
<gates>
<gate name="G$1" symbol="FT232R" x="0" y="0"/>
</gates>
<devices>
<device name="SSOP" package="SSOP28DB">
<connects>
<connect gate="G$1" pin="3V3OUT" pad="17"/>
<connect gate="G$1" pin="AGND" pad="25"/>
<connect gate="G$1" pin="CTS" pad="11"/>
<connect gate="G$1" pin="DCD" pad="10"/>
<connect gate="G$1" pin="DSR" pad="9"/>
<connect gate="G$1" pin="DTR" pad="2"/>
<connect gate="G$1" pin="GND18" pad="18"/>
<connect gate="G$1" pin="GND21" pad="21"/>
<connect gate="G$1" pin="GND7" pad="7"/>
<connect gate="G$1" pin="OSCI" pad="27"/>
<connect gate="G$1" pin="OSCO" pad="28"/>
<connect gate="G$1" pin="PWREN" pad="14"/>
<connect gate="G$1" pin="RESET" pad="19"/>
<connect gate="G$1" pin="RI" pad="6"/>
<connect gate="G$1" pin="RTS" pad="3"/>
<connect gate="G$1" pin="RXD" pad="5"/>
<connect gate="G$1" pin="RXLED" pad="22"/>
<connect gate="G$1" pin="SLEEP" pad="12"/>
<connect gate="G$1" pin="TEST" pad="26"/>
<connect gate="G$1" pin="TXD" pad="1"/>
<connect gate="G$1" pin="TXDEN" pad="13"/>
<connect gate="G$1" pin="TXLED" pad="23"/>
<connect gate="G$1" pin="USBDM" pad="16"/>
<connect gate="G$1" pin="USBDP" pad="15"/>
<connect gate="G$1" pin="VCC" pad="20"/>
<connect gate="G$1" pin="VCCIO" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603-RES">
<wire x1="-1.6002" y1="0.6858" x2="1.6002" y2="0.6858" width="0.0508" layer="39"/>
<wire x1="1.6002" y1="0.6858" x2="1.6002" y2="-0.6858" width="0.0508" layer="39"/>
<wire x1="1.6002" y1="-0.6858" x2="-1.6002" y2="-0.6858" width="0.0508" layer="39"/>
<wire x1="-1.6002" y1="-0.6858" x2="-1.6002" y2="0.6858" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.1905" y1="-0.381" x2="0.1905" y2="0.381" layer="21"/>
</package>
<package name="1206">
<wire x1="-2.4003" y1="1.1049" x2="2.4003" y2="1.1049" width="0.0508" layer="39"/>
<wire x1="2.4003" y1="-1.1049" x2="-2.4003" y2="-1.1049" width="0.0508" layer="39"/>
<wire x1="-2.4003" y1="-1.1049" x2="-2.4003" y2="1.1049" width="0.0508" layer="39"/>
<wire x1="2.4003" y1="1.1049" x2="2.4003" y2="-1.1049" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="1.5621" x2="3.302" y2="1.5621" width="0.0508" layer="39"/>
<wire x1="3.302" y1="1.5621" x2="3.302" y2="-1.5621" width="0.0508" layer="39"/>
<wire x1="3.302" y1="-1.5621" x2="-3.302" y2="-1.5621" width="0.0508" layer="39"/>
<wire x1="-3.302" y1="-1.5621" x2="-3.302" y2="1.5621" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-1.4986" y1="0.8128" x2="1.4986" y2="0.8128" width="0.0508" layer="39"/>
<wire x1="1.4986" y1="0.8128" x2="1.4986" y2="-0.8128" width="0.0508" layer="39"/>
<wire x1="1.4986" y1="-0.8128" x2="-1.4986" y2="-0.8128" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="-0.8128" x2="-1.4986" y2="0.8128" width="0.0508" layer="39"/>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762" diameter="1.6764"/>
<pad name="2" x="2.5" y="0" drill="0.762" diameter="1.6764"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<wire x1="-3.9116" y1="-1.8034" x2="3.9116" y2="-1.8034" width="0.0508" layer="39"/>
<wire x1="3.9116" y1="-1.8034" x2="3.9116" y2="1.8034" width="0.0508" layer="39"/>
<wire x1="3.9116" y1="1.8034" x2="-3.9116" y2="1.8034" width="0.0508" layer="39"/>
<wire x1="-3.9116" y1="1.8034" x2="-3.9116" y2="-1.8034" width="0.0508" layer="39"/>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
</package>
<package name="AXIAL-0.1EZ">
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.524" y1="0" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="2.54" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="0" y="-2.159" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="0" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="0" y="0" radius="1.016" width="0" layer="30"/>
<circle x="2.54" y="0" radius="1.016" width="0" layer="30"/>
<circle x="0" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.4572" width="0" layer="29"/>
</package>
<package name="AXIAL-0.1">
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.524" y1="0" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.54" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="0" y="-2.159" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.2065" y1="0.6477" x2="1.2065" y2="0.6477" width="0.0508" layer="39"/>
<wire x1="1.2065" y1="0.6477" x2="1.2065" y2="-0.6477" width="0.0508" layer="39"/>
<wire x1="1.2065" y1="-0.6477" x2="-1.2065" y2="-0.6477" width="0.0508" layer="39"/>
<wire x1="-1.2065" y1="-0.6477" x2="-1.2065" y2="0.6477" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="330OHM1/10W1%(0603)" prefix="R" uservalue="yes">
<description>RES-00818</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-00818"/>
<attribute name="VALUE" value="330" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W-VERT-KIT" package="AXIAL-0.1EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W-VERT" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find discrete LEDs for illumination or indication, but no displays.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-1206">
<wire x1="-1" y1="1" x2="-2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1" x2="1" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="0" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="0.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0.6" x2="-0.3" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="0" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="-0.889" y="1.397" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.778" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LED-0603">
<wire x1="0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="-0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="0" y1="0.17" x2="0.2338" y2="-0.14" width="0.2032" layer="21"/>
<wire x1="-0.0254" y1="0.1546" x2="-0.2184" y2="-0.14" width="0.2032" layer="21"/>
<smd name="C" x="0" y="0.877" dx="1" dy="1" layer="1" roundness="30"/>
<smd name="A" x="0" y="-0.877" dx="1" dy="1" layer="1" roundness="30"/>
<text x="-0.6985" y="-0.889" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="1.0795" y="-1.016" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="LED10MM">
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7" shape="square"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
</package>
<package name="FKIT-LED-1206">
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="0.5" x2="-0.55" y2="0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LED3MM-NS">
<description>&lt;h3&gt;LED 3MM - No Silk&lt;/h3&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM-KIT">
<description>&lt;h3&gt;LED5MM-KIT&lt;/h3&gt;
5MM Through-hole LED&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="LED-1206-BOTTOM">
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<wire x1="1.016" y1="1.016" x2="2.7686" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.7686" y1="-1.016" x2="1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="3.175" y1="0" x2="3.3528" y2="0" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-1.016" x2="-2.7686" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.7686" y1="-1.016" x2="-2.7686" y2="1.016" width="0.127" layer="21"/>
<wire x1="-2.7686" y1="1.016" x2="-1.016" y2="1.016" width="0.127" layer="21"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.175" y1="0" x2="-3.81" y2="0" width="0.254" layer="21"/>
<wire x1="-3.48741875" y1="-0.368296875" x2="-3.48741875" y2="0.3556" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.81" y2="0" width="0.254" layer="21"/>
<wire x1="-3.489959375" y1="0.37591875" x2="-3.48741875" y2="0.373378125" width="0.254" layer="21"/>
<wire x1="-3.48741875" y1="0.373378125" x2="-3.48741875" y2="-0.370840625" width="0.254" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D" uservalue="yes">
<description>&lt;b&gt;LEDs&lt;/b&gt;
Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. 5mm - Spark Fun Electronics SKU : COM-00529 (and others)</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08794" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.027940625" x2="0" y2="-0.027940625" width="0.381" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="0.508" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="0.254" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL2">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-LARGE">
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4.826" y="0" drill="0.9" diameter="1.905"/>
<pad name="2" x="4.572" y="0" drill="0.9" diameter="1.905"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="GRM43D">
<wire x1="2.25" y1="1.6" x2="1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.3" y1="1.8" x2="2.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.8" x2="2.3" y2="-1.8" width="0.127" layer="21"/>
<smd name="A" x="1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<smd name="C" x="-1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<text x="-2" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-2" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.2" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="2.2" y2="1.6" layer="51"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-5MM">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.5" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.4" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="CTZ3">
<description>CTZ3 Series land pattern for variable capacitor - CTZ3E-50C-W1-PF</description>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="-1" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.2" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.05" y1="2.25" x2="-1.7" y2="1.45" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.45" x2="-1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.35" x2="-1.05" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.05" y1="2.25" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.35" x2="1.05" y2="-2.35" width="0.127" layer="21"/>
<smd name="+" x="0" y="2.05" dx="1.5" dy="1.2" layer="1"/>
<smd name="-" x="0" y="-2.05" dx="1.5" dy="1.2" layer="1"/>
<text x="-2" y="3" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2" y="-3.4" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="CAP-PTH-SMALLEZ">
<description>This is the "EZ" version of the .1" spaced ceramic thru-hole cap.&lt;br&gt;
It has reduced top mask to make it harder to put the component on the wrong side of the board.</description>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651" stop="no"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" stop="no"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
<circle x="0" y="0" radius="0.898025" width="0" layer="30"/>
<circle x="2.54" y="0" radius="0.915809375" width="0" layer="30"/>
<circle x="0" y="0" radius="0.40160625" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.40160625" width="0" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CAP-PTH-SMALL2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CAP-PTH-LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="GRM43D">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CAP-PTH-5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ASMD" package="CTZ3">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="CAP-PTH-SMALLEZ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AiXWare">
<packages>
<package name="AIXLOGO">
<rectangle x1="11.02995" y1="3.04165" x2="11.05535" y2="3.05435" layer="21"/>
<rectangle x1="11.00455" y1="3.05435" x2="11.08075" y2="3.06705" layer="21"/>
<rectangle x1="10.97915" y1="3.06705" x2="11.10615" y2="3.07975" layer="21"/>
<rectangle x1="10.95375" y1="3.07975" x2="11.13155" y2="3.09245" layer="21"/>
<rectangle x1="10.92835" y1="3.09245" x2="11.15695" y2="3.10515" layer="21"/>
<rectangle x1="10.90295" y1="3.10515" x2="11.18235" y2="3.11785" layer="21"/>
<rectangle x1="10.87755" y1="3.11785" x2="11.22045" y2="3.13055" layer="21"/>
<rectangle x1="10.85215" y1="3.13055" x2="11.24585" y2="3.14325" layer="21"/>
<rectangle x1="10.82675" y1="3.14325" x2="11.25855" y2="3.15595" layer="21"/>
<rectangle x1="10.80135" y1="3.15595" x2="11.28395" y2="3.16865" layer="21"/>
<rectangle x1="10.77595" y1="3.16865" x2="11.30935" y2="3.18135" layer="21"/>
<rectangle x1="10.75055" y1="3.18135" x2="11.33475" y2="3.19405" layer="21"/>
<rectangle x1="10.72515" y1="3.19405" x2="11.36015" y2="3.20675" layer="21"/>
<rectangle x1="10.69975" y1="3.20675" x2="11.38555" y2="3.21945" layer="21"/>
<rectangle x1="10.67435" y1="3.21945" x2="11.41095" y2="3.23215" layer="21"/>
<rectangle x1="10.64895" y1="3.23215" x2="11.43635" y2="3.24485" layer="21"/>
<rectangle x1="10.62355" y1="3.24485" x2="11.46175" y2="3.25755" layer="21"/>
<rectangle x1="10.59815" y1="3.25755" x2="11.48715" y2="3.27025" layer="21"/>
<rectangle x1="10.57275" y1="3.27025" x2="11.52525" y2="3.28295" layer="21"/>
<rectangle x1="10.54735" y1="3.28295" x2="11.53795" y2="3.29565" layer="21"/>
<rectangle x1="10.52195" y1="3.29565" x2="11.56335" y2="3.30835" layer="21"/>
<rectangle x1="10.49655" y1="3.30835" x2="11.58875" y2="3.32105" layer="21"/>
<rectangle x1="10.47115" y1="3.32105" x2="11.61415" y2="3.33375" layer="21"/>
<rectangle x1="10.44575" y1="3.33375" x2="11.63955" y2="3.34645" layer="21"/>
<rectangle x1="10.42035" y1="3.34645" x2="11.66495" y2="3.35915" layer="21"/>
<rectangle x1="10.39495" y1="3.35915" x2="11.69035" y2="3.37185" layer="21"/>
<rectangle x1="10.36955" y1="3.37185" x2="11.71575" y2="3.38455" layer="21"/>
<rectangle x1="10.34415" y1="3.38455" x2="11.74115" y2="3.39725" layer="21"/>
<rectangle x1="10.31875" y1="3.39725" x2="11.76655" y2="3.40995" layer="21"/>
<rectangle x1="10.30605" y1="3.40995" x2="11.79195" y2="3.42265" layer="21"/>
<rectangle x1="10.28065" y1="3.42265" x2="11.81735" y2="3.43535" layer="21"/>
<rectangle x1="10.25525" y1="3.43535" x2="11.84275" y2="3.44805" layer="21"/>
<rectangle x1="10.22985" y1="3.44805" x2="11.86815" y2="3.46075" layer="21"/>
<rectangle x1="10.19175" y1="3.46075" x2="11.89355" y2="3.47345" layer="21"/>
<rectangle x1="10.16635" y1="3.47345" x2="11.91895" y2="3.48615" layer="21"/>
<rectangle x1="10.15365" y1="3.48615" x2="11.94435" y2="3.49885" layer="21"/>
<rectangle x1="10.12825" y1="3.49885" x2="11.96975" y2="3.51155" layer="21"/>
<rectangle x1="10.10285" y1="3.51155" x2="11.99515" y2="3.52425" layer="21"/>
<rectangle x1="10.07745" y1="3.52425" x2="12.02055" y2="3.53695" layer="21"/>
<rectangle x1="10.05205" y1="3.53695" x2="12.04595" y2="3.54965" layer="21"/>
<rectangle x1="10.02665" y1="3.54965" x2="12.07135" y2="3.56235" layer="21"/>
<rectangle x1="10.00125" y1="3.56235" x2="12.09675" y2="3.57505" layer="21"/>
<rectangle x1="9.97585" y1="3.57505" x2="12.12215" y2="3.58775" layer="21"/>
<rectangle x1="9.95045" y1="3.58775" x2="12.14755" y2="3.60045" layer="21"/>
<rectangle x1="9.92505" y1="3.60045" x2="12.17295" y2="3.61315" layer="21"/>
<rectangle x1="9.89965" y1="3.61315" x2="12.19835" y2="3.62585" layer="21"/>
<rectangle x1="9.87425" y1="3.62585" x2="12.22375" y2="3.63855" layer="21"/>
<rectangle x1="9.84885" y1="3.63855" x2="12.24915" y2="3.65125" layer="21"/>
<rectangle x1="9.82345" y1="3.65125" x2="12.27455" y2="3.66395" layer="21"/>
<rectangle x1="9.79805" y1="3.66395" x2="12.29995" y2="3.67665" layer="21"/>
<rectangle x1="9.77265" y1="3.67665" x2="12.32535" y2="3.68935" layer="21"/>
<rectangle x1="9.74725" y1="3.68935" x2="12.35075" y2="3.70205" layer="21"/>
<rectangle x1="9.72185" y1="3.70205" x2="12.37615" y2="3.71475" layer="21"/>
<rectangle x1="9.69645" y1="3.71475" x2="12.40155" y2="3.72745" layer="21"/>
<rectangle x1="9.67105" y1="3.72745" x2="12.42695" y2="3.74015" layer="21"/>
<rectangle x1="9.64565" y1="3.74015" x2="12.45235" y2="3.75285" layer="21"/>
<rectangle x1="9.62025" y1="3.75285" x2="12.47775" y2="3.76555" layer="21"/>
<rectangle x1="9.59485" y1="3.76555" x2="12.50315" y2="3.77825" layer="21"/>
<rectangle x1="9.56945" y1="3.77825" x2="12.52855" y2="3.79095" layer="21"/>
<rectangle x1="9.54405" y1="3.79095" x2="12.55395" y2="3.80365" layer="21"/>
<rectangle x1="9.51865" y1="3.80365" x2="12.57935" y2="3.81635" layer="21"/>
<rectangle x1="9.49325" y1="3.81635" x2="12.60475" y2="3.82905" layer="21"/>
<rectangle x1="9.46785" y1="3.82905" x2="12.63015" y2="3.84175" layer="21"/>
<rectangle x1="9.44245" y1="3.84175" x2="12.65555" y2="3.85445" layer="21"/>
<rectangle x1="9.41705" y1="3.85445" x2="12.68095" y2="3.86715" layer="21"/>
<rectangle x1="9.39165" y1="3.86715" x2="12.70635" y2="3.87985" layer="21"/>
<rectangle x1="9.36625" y1="3.87985" x2="12.73175" y2="3.89255" layer="21"/>
<rectangle x1="9.34085" y1="3.89255" x2="12.75715" y2="3.90525" layer="21"/>
<rectangle x1="9.31545" y1="3.90525" x2="12.78255" y2="3.91795" layer="21"/>
<rectangle x1="9.29005" y1="3.91795" x2="12.80795" y2="3.93065" layer="21"/>
<rectangle x1="9.26465" y1="3.93065" x2="12.83335" y2="3.94335" layer="21"/>
<rectangle x1="9.23925" y1="3.94335" x2="12.85875" y2="3.95605" layer="21"/>
<rectangle x1="9.21385" y1="3.95605" x2="12.88415" y2="3.96875" layer="21"/>
<rectangle x1="9.18845" y1="3.96875" x2="12.90955" y2="3.98145" layer="21"/>
<rectangle x1="9.16305" y1="3.98145" x2="12.93495" y2="3.99415" layer="21"/>
<rectangle x1="9.13765" y1="3.99415" x2="12.96035" y2="4.00685" layer="21"/>
<rectangle x1="9.11225" y1="4.00685" x2="12.98575" y2="4.01955" layer="21"/>
<rectangle x1="9.08685" y1="4.01955" x2="13.01115" y2="4.03225" layer="21"/>
<rectangle x1="9.06145" y1="4.03225" x2="13.03655" y2="4.04495" layer="21"/>
<rectangle x1="9.03605" y1="4.04495" x2="13.06195" y2="4.05765" layer="21"/>
<rectangle x1="9.01065" y1="4.05765" x2="13.08735" y2="4.07035" layer="21"/>
<rectangle x1="8.98525" y1="4.07035" x2="13.11275" y2="4.08305" layer="21"/>
<rectangle x1="8.95985" y1="4.08305" x2="13.13815" y2="4.09575" layer="21"/>
<rectangle x1="8.93445" y1="4.09575" x2="13.16355" y2="4.10845" layer="21"/>
<rectangle x1="8.90905" y1="4.10845" x2="13.18895" y2="4.12115" layer="21"/>
<rectangle x1="8.88365" y1="4.12115" x2="13.21435" y2="4.13385" layer="21"/>
<rectangle x1="8.85825" y1="4.13385" x2="13.23975" y2="4.14655" layer="21"/>
<rectangle x1="8.83285" y1="4.14655" x2="13.26515" y2="4.15925" layer="21"/>
<rectangle x1="8.80745" y1="4.15925" x2="13.29055" y2="4.17195" layer="21"/>
<rectangle x1="8.78205" y1="4.17195" x2="13.31595" y2="4.18465" layer="21"/>
<rectangle x1="8.75665" y1="4.18465" x2="13.34135" y2="4.19735" layer="21"/>
<rectangle x1="8.73125" y1="4.19735" x2="13.36675" y2="4.21005" layer="21"/>
<rectangle x1="8.70585" y1="4.21005" x2="13.39215" y2="4.22275" layer="21"/>
<rectangle x1="8.68045" y1="4.22275" x2="13.41755" y2="4.23545" layer="21"/>
<rectangle x1="8.65505" y1="4.23545" x2="13.44295" y2="4.24815" layer="21"/>
<rectangle x1="8.62965" y1="4.24815" x2="13.46835" y2="4.26085" layer="21"/>
<rectangle x1="8.60425" y1="4.26085" x2="13.49375" y2="4.27355" layer="21"/>
<rectangle x1="8.57885" y1="4.27355" x2="13.51915" y2="4.28625" layer="21"/>
<rectangle x1="8.55345" y1="4.28625" x2="13.54455" y2="4.29895" layer="21"/>
<rectangle x1="8.52805" y1="4.29895" x2="13.56995" y2="4.31165" layer="21"/>
<rectangle x1="8.50265" y1="4.31165" x2="13.59535" y2="4.32435" layer="21"/>
<rectangle x1="8.47725" y1="4.32435" x2="13.62075" y2="4.33705" layer="21"/>
<rectangle x1="8.45185" y1="4.33705" x2="13.64615" y2="4.34975" layer="21"/>
<rectangle x1="8.42645" y1="4.34975" x2="13.67155" y2="4.36245" layer="21"/>
<rectangle x1="8.40105" y1="4.36245" x2="13.69695" y2="4.37515" layer="21"/>
<rectangle x1="8.37565" y1="4.37515" x2="13.72235" y2="4.38785" layer="21"/>
<rectangle x1="8.35025" y1="4.38785" x2="13.74775" y2="4.40055" layer="21"/>
<rectangle x1="8.32485" y1="4.40055" x2="13.77315" y2="4.41325" layer="21"/>
<rectangle x1="8.29945" y1="4.41325" x2="13.79855" y2="4.42595" layer="21"/>
<rectangle x1="8.27405" y1="4.42595" x2="13.82395" y2="4.43865" layer="21"/>
<rectangle x1="8.24865" y1="4.43865" x2="13.84935" y2="4.45135" layer="21"/>
<rectangle x1="8.22325" y1="4.45135" x2="13.87475" y2="4.46405" layer="21"/>
<rectangle x1="8.19785" y1="4.46405" x2="13.90015" y2="4.47675" layer="21"/>
<rectangle x1="8.17245" y1="4.47675" x2="13.92555" y2="4.48945" layer="21"/>
<rectangle x1="8.14705" y1="4.48945" x2="13.95095" y2="4.50215" layer="21"/>
<rectangle x1="8.12165" y1="4.50215" x2="13.97635" y2="4.51485" layer="21"/>
<rectangle x1="8.09625" y1="4.51485" x2="14.00175" y2="4.52755" layer="21"/>
<rectangle x1="8.07085" y1="4.52755" x2="14.02715" y2="4.54025" layer="21"/>
<rectangle x1="8.04545" y1="4.54025" x2="14.05255" y2="4.55295" layer="21"/>
<rectangle x1="8.02005" y1="4.55295" x2="14.07795" y2="4.56565" layer="21"/>
<rectangle x1="7.99465" y1="4.56565" x2="14.10335" y2="4.57835" layer="21"/>
<rectangle x1="7.96925" y1="4.57835" x2="14.12875" y2="4.59105" layer="21"/>
<rectangle x1="7.94385" y1="4.59105" x2="14.15415" y2="4.60375" layer="21"/>
<rectangle x1="7.91845" y1="4.60375" x2="14.17955" y2="4.61645" layer="21"/>
<rectangle x1="7.89305" y1="4.61645" x2="14.20495" y2="4.62915" layer="21"/>
<rectangle x1="7.86765" y1="4.62915" x2="14.23035" y2="4.64185" layer="21"/>
<rectangle x1="7.84225" y1="4.64185" x2="14.25575" y2="4.65455" layer="21"/>
<rectangle x1="7.81685" y1="4.65455" x2="14.28115" y2="4.66725" layer="21"/>
<rectangle x1="7.79145" y1="4.66725" x2="14.30655" y2="4.67995" layer="21"/>
<rectangle x1="7.76605" y1="4.67995" x2="14.33195" y2="4.69265" layer="21"/>
<rectangle x1="7.74065" y1="4.69265" x2="14.35735" y2="4.70535" layer="21"/>
<rectangle x1="7.71525" y1="4.70535" x2="14.38275" y2="4.71805" layer="21"/>
<rectangle x1="7.68985" y1="4.71805" x2="14.40815" y2="4.73075" layer="21"/>
<rectangle x1="7.66445" y1="4.73075" x2="14.43355" y2="4.74345" layer="21"/>
<rectangle x1="7.63905" y1="4.74345" x2="14.45895" y2="4.75615" layer="21"/>
<rectangle x1="7.61365" y1="4.75615" x2="14.48435" y2="4.76885" layer="21"/>
<rectangle x1="7.58825" y1="4.76885" x2="14.50975" y2="4.78155" layer="21"/>
<rectangle x1="7.56285" y1="4.78155" x2="14.53515" y2="4.79425" layer="21"/>
<rectangle x1="7.53745" y1="4.79425" x2="14.56055" y2="4.80695" layer="21"/>
<rectangle x1="7.51205" y1="4.80695" x2="14.58595" y2="4.81965" layer="21"/>
<rectangle x1="7.48665" y1="4.81965" x2="14.61135" y2="4.83235" layer="21"/>
<rectangle x1="7.46125" y1="4.83235" x2="14.63675" y2="4.84505" layer="21"/>
<rectangle x1="7.43585" y1="4.84505" x2="14.66215" y2="4.85775" layer="21"/>
<rectangle x1="7.41045" y1="4.85775" x2="14.68755" y2="4.87045" layer="21"/>
<rectangle x1="7.38505" y1="4.87045" x2="14.71295" y2="4.88315" layer="21"/>
<rectangle x1="7.35965" y1="4.88315" x2="14.73835" y2="4.89585" layer="21"/>
<rectangle x1="7.33425" y1="4.89585" x2="14.76375" y2="4.90855" layer="21"/>
<rectangle x1="7.30885" y1="4.90855" x2="14.78915" y2="4.92125" layer="21"/>
<rectangle x1="7.28345" y1="4.92125" x2="14.81455" y2="4.93395" layer="21"/>
<rectangle x1="7.25805" y1="4.93395" x2="14.83995" y2="4.94665" layer="21"/>
<rectangle x1="7.23265" y1="4.94665" x2="14.86535" y2="4.95935" layer="21"/>
<rectangle x1="7.20725" y1="4.95935" x2="14.89075" y2="4.97205" layer="21"/>
<rectangle x1="7.18185" y1="4.97205" x2="14.91615" y2="4.98475" layer="21"/>
<rectangle x1="7.15645" y1="4.98475" x2="14.94155" y2="4.99745" layer="21"/>
<rectangle x1="7.13105" y1="4.99745" x2="14.96695" y2="5.01015" layer="21"/>
<rectangle x1="7.10565" y1="5.01015" x2="14.99235" y2="5.02285" layer="21"/>
<rectangle x1="7.08025" y1="5.02285" x2="15.01775" y2="5.03555" layer="21"/>
<rectangle x1="7.05485" y1="5.03555" x2="15.04315" y2="5.04825" layer="21"/>
<rectangle x1="7.02945" y1="5.04825" x2="15.06855" y2="5.06095" layer="21"/>
<rectangle x1="7.00405" y1="5.06095" x2="15.09395" y2="5.07365" layer="21"/>
<rectangle x1="6.97865" y1="5.07365" x2="15.11935" y2="5.08635" layer="21"/>
<rectangle x1="6.95325" y1="5.08635" x2="15.14475" y2="5.09905" layer="21"/>
<rectangle x1="6.92785" y1="5.09905" x2="15.17015" y2="5.11175" layer="21"/>
<rectangle x1="6.90245" y1="5.11175" x2="15.19555" y2="5.12445" layer="21"/>
<rectangle x1="6.87705" y1="5.12445" x2="15.22095" y2="5.13715" layer="21"/>
<rectangle x1="6.85165" y1="5.13715" x2="15.24635" y2="5.14985" layer="21"/>
<rectangle x1="6.82625" y1="5.14985" x2="15.27175" y2="5.16255" layer="21"/>
<rectangle x1="6.80085" y1="5.16255" x2="15.29715" y2="5.17525" layer="21"/>
<rectangle x1="6.77545" y1="5.17525" x2="15.32255" y2="5.18795" layer="21"/>
<rectangle x1="6.75005" y1="5.18795" x2="15.34795" y2="5.20065" layer="21"/>
<rectangle x1="6.72465" y1="5.20065" x2="15.37335" y2="5.21335" layer="21"/>
<rectangle x1="6.69925" y1="5.21335" x2="15.39875" y2="5.22605" layer="21"/>
<rectangle x1="6.67385" y1="5.22605" x2="15.42415" y2="5.23875" layer="21"/>
<rectangle x1="6.64845" y1="5.23875" x2="15.44955" y2="5.25145" layer="21"/>
<rectangle x1="6.62305" y1="5.25145" x2="15.47495" y2="5.26415" layer="21"/>
<rectangle x1="6.59765" y1="5.26415" x2="15.50035" y2="5.27685" layer="21"/>
<rectangle x1="6.57225" y1="5.27685" x2="15.52575" y2="5.28955" layer="21"/>
<rectangle x1="6.54685" y1="5.28955" x2="15.55115" y2="5.30225" layer="21"/>
<rectangle x1="6.52145" y1="5.30225" x2="15.57655" y2="5.31495" layer="21"/>
<rectangle x1="6.49605" y1="5.31495" x2="15.60195" y2="5.32765" layer="21"/>
<rectangle x1="6.47065" y1="5.32765" x2="15.62735" y2="5.34035" layer="21"/>
<rectangle x1="6.44525" y1="5.34035" x2="15.65275" y2="5.35305" layer="21"/>
<rectangle x1="6.41985" y1="5.35305" x2="15.67815" y2="5.36575" layer="21"/>
<rectangle x1="6.39445" y1="5.36575" x2="15.70355" y2="5.37845" layer="21"/>
<rectangle x1="6.36905" y1="5.37845" x2="15.72895" y2="5.39115" layer="21"/>
<rectangle x1="6.34365" y1="5.39115" x2="15.75435" y2="5.40385" layer="21"/>
<rectangle x1="6.31825" y1="5.40385" x2="15.77975" y2="5.41655" layer="21"/>
<rectangle x1="6.29285" y1="5.41655" x2="15.80515" y2="5.42925" layer="21"/>
<rectangle x1="6.26745" y1="5.42925" x2="15.83055" y2="5.44195" layer="21"/>
<rectangle x1="6.24205" y1="5.44195" x2="15.85595" y2="5.45465" layer="21"/>
<rectangle x1="6.21665" y1="5.45465" x2="15.88135" y2="5.46735" layer="21"/>
<rectangle x1="6.19125" y1="5.46735" x2="15.90675" y2="5.48005" layer="21"/>
<rectangle x1="6.16585" y1="5.48005" x2="15.93215" y2="5.49275" layer="21"/>
<rectangle x1="6.14045" y1="5.49275" x2="15.95755" y2="5.50545" layer="21"/>
<rectangle x1="6.11505" y1="5.50545" x2="15.98295" y2="5.51815" layer="21"/>
<rectangle x1="6.08965" y1="5.51815" x2="16.00835" y2="5.53085" layer="21"/>
<rectangle x1="6.06425" y1="5.53085" x2="16.03375" y2="5.54355" layer="21"/>
<rectangle x1="6.03885" y1="5.54355" x2="16.05915" y2="5.55625" layer="21"/>
<rectangle x1="6.01345" y1="5.55625" x2="16.08455" y2="5.56895" layer="21"/>
<rectangle x1="5.98805" y1="5.56895" x2="16.10995" y2="5.58165" layer="21"/>
<rectangle x1="5.96265" y1="5.58165" x2="16.13535" y2="5.59435" layer="21"/>
<rectangle x1="5.93725" y1="5.59435" x2="16.16075" y2="5.60705" layer="21"/>
<rectangle x1="5.91185" y1="5.60705" x2="16.18615" y2="5.61975" layer="21"/>
<rectangle x1="5.88645" y1="5.61975" x2="16.21155" y2="5.63245" layer="21"/>
<rectangle x1="5.86105" y1="5.63245" x2="16.23695" y2="5.64515" layer="21"/>
<rectangle x1="5.83565" y1="5.64515" x2="16.26235" y2="5.65785" layer="21"/>
<rectangle x1="5.81025" y1="5.65785" x2="16.28775" y2="5.67055" layer="21"/>
<rectangle x1="5.78485" y1="5.67055" x2="16.31315" y2="5.68325" layer="21"/>
<rectangle x1="5.75945" y1="5.68325" x2="16.33855" y2="5.69595" layer="21"/>
<rectangle x1="5.73405" y1="5.69595" x2="16.36395" y2="5.70865" layer="21"/>
<rectangle x1="5.70865" y1="5.70865" x2="16.38935" y2="5.72135" layer="21"/>
<rectangle x1="5.68325" y1="5.72135" x2="16.41475" y2="5.73405" layer="21"/>
<rectangle x1="5.65785" y1="5.73405" x2="16.44015" y2="5.74675" layer="21"/>
<rectangle x1="5.63245" y1="5.74675" x2="16.46555" y2="5.75945" layer="21"/>
<rectangle x1="5.60705" y1="5.75945" x2="16.49095" y2="5.77215" layer="21"/>
<rectangle x1="5.58165" y1="5.77215" x2="16.51635" y2="5.78485" layer="21"/>
<rectangle x1="5.55625" y1="5.78485" x2="16.54175" y2="5.79755" layer="21"/>
<rectangle x1="5.53085" y1="5.79755" x2="16.56715" y2="5.81025" layer="21"/>
<rectangle x1="5.50545" y1="5.81025" x2="16.59255" y2="5.82295" layer="21"/>
<rectangle x1="5.48005" y1="5.82295" x2="16.61795" y2="5.83565" layer="21"/>
<rectangle x1="5.45465" y1="5.83565" x2="16.64335" y2="5.84835" layer="21"/>
<rectangle x1="5.42925" y1="5.84835" x2="16.66875" y2="5.86105" layer="21"/>
<rectangle x1="5.40385" y1="5.86105" x2="16.69415" y2="5.87375" layer="21"/>
<rectangle x1="5.37845" y1="5.87375" x2="16.71955" y2="5.88645" layer="21"/>
<rectangle x1="5.35305" y1="5.88645" x2="16.74495" y2="5.89915" layer="21"/>
<rectangle x1="5.32765" y1="5.89915" x2="16.77035" y2="5.91185" layer="21"/>
<rectangle x1="5.30225" y1="5.91185" x2="16.79575" y2="5.92455" layer="21"/>
<rectangle x1="5.27685" y1="5.92455" x2="16.82115" y2="5.93725" layer="21"/>
<rectangle x1="5.25145" y1="5.93725" x2="16.84655" y2="5.94995" layer="21"/>
<rectangle x1="5.22605" y1="5.94995" x2="16.87195" y2="5.96265" layer="21"/>
<rectangle x1="5.20065" y1="5.96265" x2="16.89735" y2="5.97535" layer="21"/>
<rectangle x1="5.17525" y1="5.97535" x2="16.92275" y2="5.98805" layer="21"/>
<rectangle x1="5.14985" y1="5.98805" x2="16.94815" y2="6.00075" layer="21"/>
<rectangle x1="5.12445" y1="6.00075" x2="16.97355" y2="6.01345" layer="21"/>
<rectangle x1="5.09905" y1="6.01345" x2="16.99895" y2="6.02615" layer="21"/>
<rectangle x1="5.07365" y1="6.02615" x2="17.02435" y2="6.03885" layer="21"/>
<rectangle x1="5.04825" y1="6.03885" x2="17.04975" y2="6.05155" layer="21"/>
<rectangle x1="5.02285" y1="6.05155" x2="17.07515" y2="6.06425" layer="21"/>
<rectangle x1="4.99745" y1="6.06425" x2="17.10055" y2="6.07695" layer="21"/>
<rectangle x1="4.97205" y1="6.07695" x2="17.12595" y2="6.08965" layer="21"/>
<rectangle x1="4.94665" y1="6.08965" x2="17.15135" y2="6.10235" layer="21"/>
<rectangle x1="4.92125" y1="6.10235" x2="17.17675" y2="6.11505" layer="21"/>
<rectangle x1="4.89585" y1="6.11505" x2="17.20215" y2="6.12775" layer="21"/>
<rectangle x1="4.87045" y1="6.12775" x2="17.22755" y2="6.14045" layer="21"/>
<rectangle x1="4.84505" y1="6.14045" x2="17.25295" y2="6.15315" layer="21"/>
<rectangle x1="4.81965" y1="6.15315" x2="17.27835" y2="6.16585" layer="21"/>
<rectangle x1="4.79425" y1="6.16585" x2="17.30375" y2="6.17855" layer="21"/>
<rectangle x1="4.76885" y1="6.17855" x2="17.32915" y2="6.19125" layer="21"/>
<rectangle x1="4.76885" y1="6.19125" x2="17.32915" y2="6.20395" layer="21"/>
<rectangle x1="4.76885" y1="6.20395" x2="17.32915" y2="6.21665" layer="21"/>
<rectangle x1="4.76885" y1="6.21665" x2="17.32915" y2="6.22935" layer="21"/>
<rectangle x1="4.76885" y1="6.22935" x2="17.32915" y2="6.24205" layer="21"/>
<rectangle x1="4.76885" y1="6.24205" x2="17.32915" y2="6.25475" layer="21"/>
<rectangle x1="4.76885" y1="6.25475" x2="17.32915" y2="6.26745" layer="21"/>
<rectangle x1="4.76885" y1="6.26745" x2="17.32915" y2="6.28015" layer="21"/>
<rectangle x1="4.76885" y1="6.28015" x2="17.32915" y2="6.29285" layer="21"/>
<rectangle x1="4.76885" y1="6.29285" x2="17.32915" y2="6.30555" layer="21"/>
<rectangle x1="4.76885" y1="6.30555" x2="17.32915" y2="6.31825" layer="21"/>
<rectangle x1="4.76885" y1="6.31825" x2="17.32915" y2="6.33095" layer="21"/>
<rectangle x1="4.76885" y1="6.33095" x2="17.32915" y2="6.34365" layer="21"/>
<rectangle x1="4.76885" y1="6.34365" x2="17.32915" y2="6.35635" layer="21"/>
<rectangle x1="4.76885" y1="6.35635" x2="17.32915" y2="6.36905" layer="21"/>
<rectangle x1="4.76885" y1="6.36905" x2="17.32915" y2="6.38175" layer="21"/>
<rectangle x1="4.76885" y1="6.38175" x2="17.32915" y2="6.39445" layer="21"/>
<rectangle x1="4.76885" y1="6.39445" x2="17.32915" y2="6.40715" layer="21"/>
<rectangle x1="4.76885" y1="6.40715" x2="17.32915" y2="6.41985" layer="21"/>
<rectangle x1="4.76885" y1="6.41985" x2="17.32915" y2="6.43255" layer="21"/>
<rectangle x1="4.76885" y1="6.43255" x2="17.32915" y2="6.44525" layer="21"/>
<rectangle x1="4.76885" y1="6.44525" x2="17.32915" y2="6.45795" layer="21"/>
<rectangle x1="4.76885" y1="6.45795" x2="17.32915" y2="6.47065" layer="21"/>
<rectangle x1="4.76885" y1="6.47065" x2="17.32915" y2="6.48335" layer="21"/>
<rectangle x1="4.76885" y1="6.48335" x2="17.32915" y2="6.49605" layer="21"/>
<rectangle x1="4.76885" y1="6.49605" x2="17.32915" y2="6.50875" layer="21"/>
<rectangle x1="4.76885" y1="6.50875" x2="17.32915" y2="6.52145" layer="21"/>
<rectangle x1="4.76885" y1="6.52145" x2="17.32915" y2="6.53415" layer="21"/>
<rectangle x1="4.76885" y1="6.53415" x2="17.32915" y2="6.54685" layer="21"/>
<rectangle x1="4.76885" y1="6.54685" x2="17.32915" y2="6.55955" layer="21"/>
<rectangle x1="4.76885" y1="6.55955" x2="17.32915" y2="6.57225" layer="21"/>
<rectangle x1="4.76885" y1="6.57225" x2="17.32915" y2="6.58495" layer="21"/>
<rectangle x1="4.76885" y1="6.58495" x2="17.32915" y2="6.59765" layer="21"/>
<rectangle x1="4.76885" y1="6.59765" x2="17.32915" y2="6.61035" layer="21"/>
<rectangle x1="4.76885" y1="6.61035" x2="17.32915" y2="6.62305" layer="21"/>
<rectangle x1="4.76885" y1="6.62305" x2="17.32915" y2="6.63575" layer="21"/>
<rectangle x1="4.76885" y1="6.63575" x2="17.32915" y2="6.64845" layer="21"/>
<rectangle x1="4.76885" y1="6.64845" x2="17.32915" y2="6.66115" layer="21"/>
<rectangle x1="4.76885" y1="6.66115" x2="17.32915" y2="6.67385" layer="21"/>
<rectangle x1="4.76885" y1="6.67385" x2="17.32915" y2="6.68655" layer="21"/>
<rectangle x1="4.76885" y1="6.68655" x2="17.32915" y2="6.69925" layer="21"/>
<rectangle x1="4.76885" y1="6.69925" x2="17.32915" y2="6.71195" layer="21"/>
<rectangle x1="4.76885" y1="6.71195" x2="17.32915" y2="6.72465" layer="21"/>
<rectangle x1="4.76885" y1="6.72465" x2="17.32915" y2="6.73735" layer="21"/>
<rectangle x1="4.76885" y1="6.73735" x2="17.32915" y2="6.75005" layer="21"/>
<rectangle x1="4.76885" y1="6.75005" x2="17.32915" y2="6.76275" layer="21"/>
<rectangle x1="4.76885" y1="6.76275" x2="17.32915" y2="6.77545" layer="21"/>
<rectangle x1="4.76885" y1="6.77545" x2="17.32915" y2="6.78815" layer="21"/>
<rectangle x1="4.76885" y1="6.78815" x2="17.32915" y2="6.80085" layer="21"/>
<rectangle x1="4.76885" y1="6.80085" x2="17.32915" y2="6.81355" layer="21"/>
<rectangle x1="4.76885" y1="6.81355" x2="17.32915" y2="6.82625" layer="21"/>
<rectangle x1="4.76885" y1="6.82625" x2="17.32915" y2="6.83895" layer="21"/>
<rectangle x1="4.76885" y1="6.83895" x2="17.32915" y2="6.85165" layer="21"/>
<rectangle x1="4.76885" y1="6.85165" x2="17.32915" y2="6.86435" layer="21"/>
<rectangle x1="4.76885" y1="6.86435" x2="17.32915" y2="6.87705" layer="21"/>
<rectangle x1="4.76885" y1="6.87705" x2="17.32915" y2="6.88975" layer="21"/>
<rectangle x1="4.76885" y1="6.88975" x2="17.32915" y2="6.90245" layer="21"/>
<rectangle x1="4.76885" y1="6.90245" x2="17.32915" y2="6.91515" layer="21"/>
<rectangle x1="4.76885" y1="6.91515" x2="17.32915" y2="6.92785" layer="21"/>
<rectangle x1="4.76885" y1="6.92785" x2="17.32915" y2="6.94055" layer="21"/>
<rectangle x1="4.76885" y1="6.94055" x2="17.32915" y2="6.95325" layer="21"/>
<rectangle x1="4.76885" y1="6.95325" x2="17.32915" y2="6.96595" layer="21"/>
<rectangle x1="4.76885" y1="6.96595" x2="17.32915" y2="6.97865" layer="21"/>
<rectangle x1="4.76885" y1="6.97865" x2="17.32915" y2="6.99135" layer="21"/>
<rectangle x1="4.76885" y1="6.99135" x2="17.32915" y2="7.00405" layer="21"/>
<rectangle x1="4.76885" y1="7.00405" x2="17.32915" y2="7.01675" layer="21"/>
<rectangle x1="4.76885" y1="7.01675" x2="17.32915" y2="7.02945" layer="21"/>
<rectangle x1="4.76885" y1="7.02945" x2="17.32915" y2="7.04215" layer="21"/>
<rectangle x1="4.76885" y1="7.04215" x2="17.32915" y2="7.05485" layer="21"/>
<rectangle x1="4.76885" y1="7.05485" x2="17.32915" y2="7.06755" layer="21"/>
<rectangle x1="4.76885" y1="7.06755" x2="17.32915" y2="7.08025" layer="21"/>
<rectangle x1="4.76885" y1="7.08025" x2="17.32915" y2="7.09295" layer="21"/>
<rectangle x1="4.76885" y1="7.09295" x2="17.32915" y2="7.10565" layer="21"/>
<rectangle x1="4.76885" y1="7.10565" x2="17.32915" y2="7.11835" layer="21"/>
<rectangle x1="4.76885" y1="7.11835" x2="17.32915" y2="7.13105" layer="21"/>
<rectangle x1="4.76885" y1="7.13105" x2="17.32915" y2="7.14375" layer="21"/>
<rectangle x1="4.76885" y1="7.14375" x2="17.32915" y2="7.15645" layer="21"/>
<rectangle x1="4.76885" y1="7.15645" x2="17.32915" y2="7.16915" layer="21"/>
<rectangle x1="4.76885" y1="7.16915" x2="17.32915" y2="7.18185" layer="21"/>
<rectangle x1="4.76885" y1="7.18185" x2="17.32915" y2="7.19455" layer="21"/>
<rectangle x1="4.76885" y1="7.19455" x2="17.32915" y2="7.20725" layer="21"/>
<rectangle x1="4.76885" y1="7.20725" x2="17.32915" y2="7.21995" layer="21"/>
<rectangle x1="4.76885" y1="7.21995" x2="17.32915" y2="7.23265" layer="21"/>
<rectangle x1="4.76885" y1="7.23265" x2="17.32915" y2="7.24535" layer="21"/>
<rectangle x1="4.76885" y1="7.24535" x2="17.32915" y2="7.25805" layer="21"/>
<rectangle x1="4.76885" y1="7.25805" x2="17.32915" y2="7.27075" layer="21"/>
<rectangle x1="4.76885" y1="7.27075" x2="17.32915" y2="7.28345" layer="21"/>
<rectangle x1="4.76885" y1="7.28345" x2="17.32915" y2="7.29615" layer="21"/>
<rectangle x1="4.76885" y1="7.29615" x2="13.53185" y2="7.30885" layer="21"/>
<rectangle x1="13.73505" y1="7.29615" x2="17.32915" y2="7.30885" layer="21"/>
<rectangle x1="4.76885" y1="7.30885" x2="13.46835" y2="7.32155" layer="21"/>
<rectangle x1="13.78585" y1="7.30885" x2="17.32915" y2="7.32155" layer="21"/>
<rectangle x1="4.76885" y1="7.32155" x2="13.43025" y2="7.33425" layer="21"/>
<rectangle x1="13.82395" y1="7.32155" x2="17.32915" y2="7.33425" layer="21"/>
<rectangle x1="4.76885" y1="7.33425" x2="13.40485" y2="7.34695" layer="21"/>
<rectangle x1="13.84935" y1="7.33425" x2="17.32915" y2="7.34695" layer="21"/>
<rectangle x1="4.76885" y1="7.34695" x2="13.37945" y2="7.35965" layer="21"/>
<rectangle x1="13.87475" y1="7.34695" x2="17.32915" y2="7.35965" layer="21"/>
<rectangle x1="4.76885" y1="7.35965" x2="13.36675" y2="7.37235" layer="21"/>
<rectangle x1="13.88745" y1="7.35965" x2="17.32915" y2="7.37235" layer="21"/>
<rectangle x1="4.76885" y1="7.37235" x2="13.34135" y2="7.38505" layer="21"/>
<rectangle x1="13.91285" y1="7.37235" x2="17.32915" y2="7.38505" layer="21"/>
<rectangle x1="4.76885" y1="7.38505" x2="13.32865" y2="7.39775" layer="21"/>
<rectangle x1="13.53185" y1="7.38505" x2="13.72235" y2="7.39775" layer="21"/>
<rectangle x1="13.92555" y1="7.38505" x2="17.32915" y2="7.39775" layer="21"/>
<rectangle x1="4.76885" y1="7.39775" x2="13.31595" y2="7.41045" layer="21"/>
<rectangle x1="13.48105" y1="7.39775" x2="13.77315" y2="7.41045" layer="21"/>
<rectangle x1="13.93825" y1="7.39775" x2="17.32915" y2="7.41045" layer="21"/>
<rectangle x1="4.76885" y1="7.41045" x2="13.30325" y2="7.42315" layer="21"/>
<rectangle x1="13.45565" y1="7.41045" x2="13.79855" y2="7.42315" layer="21"/>
<rectangle x1="13.95095" y1="7.41045" x2="17.32915" y2="7.42315" layer="21"/>
<rectangle x1="4.76885" y1="7.42315" x2="13.29055" y2="7.43585" layer="21"/>
<rectangle x1="13.43025" y1="7.42315" x2="13.82395" y2="7.43585" layer="21"/>
<rectangle x1="13.96365" y1="7.42315" x2="17.32915" y2="7.43585" layer="21"/>
<rectangle x1="4.76885" y1="7.43585" x2="13.27785" y2="7.44855" layer="21"/>
<rectangle x1="13.41755" y1="7.43585" x2="13.83665" y2="7.44855" layer="21"/>
<rectangle x1="13.96365" y1="7.43585" x2="17.32915" y2="7.44855" layer="21"/>
<rectangle x1="4.76885" y1="7.44855" x2="13.27785" y2="7.46125" layer="21"/>
<rectangle x1="13.39215" y1="7.44855" x2="13.84935" y2="7.46125" layer="21"/>
<rectangle x1="13.97635" y1="7.44855" x2="17.32915" y2="7.46125" layer="21"/>
<rectangle x1="4.76885" y1="7.46125" x2="13.26515" y2="7.47395" layer="21"/>
<rectangle x1="13.37945" y1="7.46125" x2="13.86205" y2="7.47395" layer="21"/>
<rectangle x1="13.98905" y1="7.46125" x2="17.32915" y2="7.47395" layer="21"/>
<rectangle x1="4.76885" y1="7.47395" x2="13.25245" y2="7.48665" layer="21"/>
<rectangle x1="13.36675" y1="7.47395" x2="13.87475" y2="7.48665" layer="21"/>
<rectangle x1="13.98905" y1="7.47395" x2="17.32915" y2="7.48665" layer="21"/>
<rectangle x1="4.76885" y1="7.48665" x2="13.25245" y2="7.49935" layer="21"/>
<rectangle x1="13.36675" y1="7.48665" x2="13.88745" y2="7.49935" layer="21"/>
<rectangle x1="14.00175" y1="7.48665" x2="17.32915" y2="7.49935" layer="21"/>
<rectangle x1="4.76885" y1="7.49935" x2="13.23975" y2="7.51205" layer="21"/>
<rectangle x1="13.35405" y1="7.49935" x2="13.90015" y2="7.51205" layer="21"/>
<rectangle x1="14.00175" y1="7.49935" x2="17.32915" y2="7.51205" layer="21"/>
<rectangle x1="4.76885" y1="7.51205" x2="13.23975" y2="7.52475" layer="21"/>
<rectangle x1="13.34135" y1="7.51205" x2="13.90015" y2="7.52475" layer="21"/>
<rectangle x1="14.01445" y1="7.51205" x2="17.32915" y2="7.52475" layer="21"/>
<rectangle x1="4.76885" y1="7.52475" x2="13.23975" y2="7.53745" layer="21"/>
<rectangle x1="13.34135" y1="7.52475" x2="13.91285" y2="7.53745" layer="21"/>
<rectangle x1="14.01445" y1="7.52475" x2="17.32915" y2="7.53745" layer="21"/>
<rectangle x1="4.76885" y1="7.53745" x2="13.22705" y2="7.55015" layer="21"/>
<rectangle x1="13.32865" y1="7.53745" x2="13.91285" y2="7.55015" layer="21"/>
<rectangle x1="14.01445" y1="7.53745" x2="17.32915" y2="7.55015" layer="21"/>
<rectangle x1="4.76885" y1="7.55015" x2="13.22705" y2="7.56285" layer="21"/>
<rectangle x1="13.32865" y1="7.55015" x2="13.92555" y2="7.56285" layer="21"/>
<rectangle x1="14.02715" y1="7.55015" x2="17.32915" y2="7.56285" layer="21"/>
<rectangle x1="4.76885" y1="7.56285" x2="13.22705" y2="7.57555" layer="21"/>
<rectangle x1="13.32865" y1="7.56285" x2="13.92555" y2="7.57555" layer="21"/>
<rectangle x1="14.02715" y1="7.56285" x2="17.32915" y2="7.57555" layer="21"/>
<rectangle x1="4.76885" y1="7.57555" x2="13.22705" y2="7.58825" layer="21"/>
<rectangle x1="13.31595" y1="7.57555" x2="13.92555" y2="7.58825" layer="21"/>
<rectangle x1="14.02715" y1="7.57555" x2="17.32915" y2="7.58825" layer="21"/>
<rectangle x1="4.76885" y1="7.58825" x2="13.93825" y2="7.60095" layer="21"/>
<rectangle x1="14.02715" y1="7.58825" x2="17.32915" y2="7.60095" layer="21"/>
<rectangle x1="4.76885" y1="7.60095" x2="13.93825" y2="7.61365" layer="21"/>
<rectangle x1="14.02715" y1="7.60095" x2="17.32915" y2="7.61365" layer="21"/>
<rectangle x1="4.76885" y1="7.61365" x2="13.93825" y2="7.62635" layer="21"/>
<rectangle x1="14.02715" y1="7.61365" x2="17.32915" y2="7.62635" layer="21"/>
<rectangle x1="4.76885" y1="7.62635" x2="13.93825" y2="7.63905" layer="21"/>
<rectangle x1="14.02715" y1="7.62635" x2="17.32915" y2="7.63905" layer="21"/>
<rectangle x1="4.76885" y1="7.63905" x2="13.93825" y2="7.65175" layer="21"/>
<rectangle x1="14.02715" y1="7.63905" x2="17.32915" y2="7.65175" layer="21"/>
<rectangle x1="4.76885" y1="7.65175" x2="13.93825" y2="7.66445" layer="21"/>
<rectangle x1="14.02715" y1="7.65175" x2="17.32915" y2="7.66445" layer="21"/>
<rectangle x1="4.76885" y1="7.66445" x2="13.93825" y2="7.67715" layer="21"/>
<rectangle x1="14.02715" y1="7.66445" x2="17.32915" y2="7.67715" layer="21"/>
<rectangle x1="4.76885" y1="7.67715" x2="13.93825" y2="7.68985" layer="21"/>
<rectangle x1="14.02715" y1="7.67715" x2="17.32915" y2="7.68985" layer="21"/>
<rectangle x1="4.76885" y1="7.68985" x2="13.93825" y2="7.70255" layer="21"/>
<rectangle x1="14.02715" y1="7.68985" x2="17.32915" y2="7.70255" layer="21"/>
<rectangle x1="4.76885" y1="7.70255" x2="6.85165" y2="7.71525" layer="21"/>
<rectangle x1="7.01675" y1="7.70255" x2="7.88035" y2="7.71525" layer="21"/>
<rectangle x1="8.04545" y1="7.70255" x2="10.96645" y2="7.71525" layer="21"/>
<rectangle x1="11.13155" y1="7.70255" x2="12.46505" y2="7.71525" layer="21"/>
<rectangle x1="12.63015" y1="7.70255" x2="13.93825" y2="7.71525" layer="21"/>
<rectangle x1="14.02715" y1="7.70255" x2="15.09395" y2="7.71525" layer="21"/>
<rectangle x1="15.25905" y1="7.70255" x2="16.04645" y2="7.71525" layer="21"/>
<rectangle x1="16.24965" y1="7.70255" x2="17.32915" y2="7.71525" layer="21"/>
<rectangle x1="4.76885" y1="7.71525" x2="6.78815" y2="7.72795" layer="21"/>
<rectangle x1="7.08025" y1="7.71525" x2="7.82955" y2="7.72795" layer="21"/>
<rectangle x1="8.09625" y1="7.71525" x2="10.91565" y2="7.72795" layer="21"/>
<rectangle x1="11.18235" y1="7.71525" x2="12.41425" y2="7.72795" layer="21"/>
<rectangle x1="12.68095" y1="7.71525" x2="13.55725" y2="7.72795" layer="21"/>
<rectangle x1="13.67155" y1="7.71525" x2="13.93825" y2="7.72795" layer="21"/>
<rectangle x1="14.02715" y1="7.71525" x2="15.03045" y2="7.72795" layer="21"/>
<rectangle x1="15.32255" y1="7.71525" x2="15.99565" y2="7.72795" layer="21"/>
<rectangle x1="16.30045" y1="7.71525" x2="17.32915" y2="7.72795" layer="21"/>
<rectangle x1="4.76885" y1="7.72795" x2="6.02615" y2="7.74065" layer="21"/>
<rectangle x1="6.12775" y1="7.72795" x2="6.76275" y2="7.74065" layer="21"/>
<rectangle x1="7.10565" y1="7.72795" x2="7.79145" y2="7.74065" layer="21"/>
<rectangle x1="8.13435" y1="7.72795" x2="8.60425" y2="7.74065" layer="21"/>
<rectangle x1="8.69315" y1="7.72795" x2="9.26465" y2="7.74065" layer="21"/>
<rectangle x1="9.35355" y1="7.72795" x2="9.63295" y2="7.74065" layer="21"/>
<rectangle x1="9.72185" y1="7.72795" x2="10.29335" y2="7.74065" layer="21"/>
<rectangle x1="10.38225" y1="7.72795" x2="10.87755" y2="7.74065" layer="21"/>
<rectangle x1="11.22045" y1="7.72795" x2="11.75385" y2="7.74065" layer="21"/>
<rectangle x1="11.84275" y1="7.72795" x2="12.37615" y2="7.74065" layer="21"/>
<rectangle x1="12.71905" y1="7.72795" x2="13.49375" y2="7.74065" layer="21"/>
<rectangle x1="13.74775" y1="7.72795" x2="13.93825" y2="7.74065" layer="21"/>
<rectangle x1="14.02715" y1="7.72795" x2="14.37005" y2="7.74065" layer="21"/>
<rectangle x1="14.45895" y1="7.72795" x2="15.00505" y2="7.74065" layer="21"/>
<rectangle x1="15.34795" y1="7.72795" x2="15.95755" y2="7.74065" layer="21"/>
<rectangle x1="16.33855" y1="7.72795" x2="17.32915" y2="7.74065" layer="21"/>
<rectangle x1="4.76885" y1="7.74065" x2="6.02615" y2="7.75335" layer="21"/>
<rectangle x1="6.12775" y1="7.74065" x2="6.73735" y2="7.75335" layer="21"/>
<rectangle x1="7.14375" y1="7.74065" x2="7.76605" y2="7.75335" layer="21"/>
<rectangle x1="8.15975" y1="7.74065" x2="8.60425" y2="7.75335" layer="21"/>
<rectangle x1="8.69315" y1="7.74065" x2="9.26465" y2="7.75335" layer="21"/>
<rectangle x1="9.35355" y1="7.74065" x2="9.63295" y2="7.75335" layer="21"/>
<rectangle x1="9.72185" y1="7.74065" x2="10.29335" y2="7.75335" layer="21"/>
<rectangle x1="10.38225" y1="7.74065" x2="10.85215" y2="7.75335" layer="21"/>
<rectangle x1="11.24585" y1="7.74065" x2="11.75385" y2="7.75335" layer="21"/>
<rectangle x1="11.84275" y1="7.74065" x2="12.35075" y2="7.75335" layer="21"/>
<rectangle x1="12.74445" y1="7.74065" x2="13.45565" y2="7.75335" layer="21"/>
<rectangle x1="13.78585" y1="7.74065" x2="13.93825" y2="7.75335" layer="21"/>
<rectangle x1="14.02715" y1="7.74065" x2="14.37005" y2="7.75335" layer="21"/>
<rectangle x1="14.45895" y1="7.74065" x2="14.97965" y2="7.75335" layer="21"/>
<rectangle x1="15.38605" y1="7.74065" x2="15.93215" y2="7.75335" layer="21"/>
<rectangle x1="16.36395" y1="7.74065" x2="17.32915" y2="7.75335" layer="21"/>
<rectangle x1="4.76885" y1="7.75335" x2="6.02615" y2="7.76605" layer="21"/>
<rectangle x1="6.12775" y1="7.75335" x2="6.71195" y2="7.76605" layer="21"/>
<rectangle x1="7.15645" y1="7.75335" x2="7.74065" y2="7.76605" layer="21"/>
<rectangle x1="8.18515" y1="7.75335" x2="8.60425" y2="7.76605" layer="21"/>
<rectangle x1="8.69315" y1="7.75335" x2="9.26465" y2="7.76605" layer="21"/>
<rectangle x1="9.35355" y1="7.75335" x2="9.63295" y2="7.76605" layer="21"/>
<rectangle x1="9.72185" y1="7.75335" x2="10.29335" y2="7.76605" layer="21"/>
<rectangle x1="10.38225" y1="7.75335" x2="10.82675" y2="7.76605" layer="21"/>
<rectangle x1="11.27125" y1="7.75335" x2="11.75385" y2="7.76605" layer="21"/>
<rectangle x1="11.84275" y1="7.75335" x2="12.32535" y2="7.76605" layer="21"/>
<rectangle x1="12.76985" y1="7.75335" x2="13.43025" y2="7.76605" layer="21"/>
<rectangle x1="13.82395" y1="7.75335" x2="13.93825" y2="7.76605" layer="21"/>
<rectangle x1="14.02715" y1="7.75335" x2="14.37005" y2="7.76605" layer="21"/>
<rectangle x1="14.45895" y1="7.75335" x2="14.95425" y2="7.76605" layer="21"/>
<rectangle x1="15.39875" y1="7.75335" x2="15.90675" y2="7.76605" layer="21"/>
<rectangle x1="16.38935" y1="7.75335" x2="17.32915" y2="7.76605" layer="21"/>
<rectangle x1="4.76885" y1="7.76605" x2="6.02615" y2="7.77875" layer="21"/>
<rectangle x1="6.12775" y1="7.76605" x2="6.68655" y2="7.77875" layer="21"/>
<rectangle x1="7.18185" y1="7.76605" x2="7.72795" y2="7.77875" layer="21"/>
<rectangle x1="8.19785" y1="7.76605" x2="8.60425" y2="7.77875" layer="21"/>
<rectangle x1="8.69315" y1="7.76605" x2="9.26465" y2="7.77875" layer="21"/>
<rectangle x1="9.35355" y1="7.76605" x2="9.63295" y2="7.77875" layer="21"/>
<rectangle x1="9.72185" y1="7.76605" x2="10.29335" y2="7.77875" layer="21"/>
<rectangle x1="10.38225" y1="7.76605" x2="10.80135" y2="7.77875" layer="21"/>
<rectangle x1="11.28395" y1="7.76605" x2="11.75385" y2="7.77875" layer="21"/>
<rectangle x1="11.84275" y1="7.76605" x2="12.29995" y2="7.77875" layer="21"/>
<rectangle x1="12.78255" y1="7.76605" x2="13.40485" y2="7.77875" layer="21"/>
<rectangle x1="13.84935" y1="7.76605" x2="13.93825" y2="7.77875" layer="21"/>
<rectangle x1="14.02715" y1="7.76605" x2="14.37005" y2="7.77875" layer="21"/>
<rectangle x1="14.45895" y1="7.76605" x2="14.92885" y2="7.77875" layer="21"/>
<rectangle x1="15.42415" y1="7.76605" x2="15.88135" y2="7.77875" layer="21"/>
<rectangle x1="16.40205" y1="7.76605" x2="17.32915" y2="7.77875" layer="21"/>
<rectangle x1="4.76885" y1="7.77875" x2="6.02615" y2="7.79145" layer="21"/>
<rectangle x1="6.12775" y1="7.77875" x2="6.67385" y2="7.79145" layer="21"/>
<rectangle x1="7.19455" y1="7.77875" x2="7.70255" y2="7.79145" layer="21"/>
<rectangle x1="8.22325" y1="7.77875" x2="8.60425" y2="7.79145" layer="21"/>
<rectangle x1="8.69315" y1="7.77875" x2="9.26465" y2="7.79145" layer="21"/>
<rectangle x1="9.35355" y1="7.77875" x2="9.63295" y2="7.79145" layer="21"/>
<rectangle x1="9.72185" y1="7.77875" x2="10.29335" y2="7.79145" layer="21"/>
<rectangle x1="10.38225" y1="7.77875" x2="10.78865" y2="7.79145" layer="21"/>
<rectangle x1="11.30935" y1="7.77875" x2="11.75385" y2="7.79145" layer="21"/>
<rectangle x1="11.84275" y1="7.77875" x2="12.28725" y2="7.79145" layer="21"/>
<rectangle x1="12.80795" y1="7.77875" x2="13.39215" y2="7.79145" layer="21"/>
<rectangle x1="13.86205" y1="7.77875" x2="13.93825" y2="7.79145" layer="21"/>
<rectangle x1="14.02715" y1="7.77875" x2="14.37005" y2="7.79145" layer="21"/>
<rectangle x1="14.45895" y1="7.77875" x2="14.91615" y2="7.79145" layer="21"/>
<rectangle x1="15.43685" y1="7.77875" x2="15.86865" y2="7.79145" layer="21"/>
<rectangle x1="16.12265" y1="7.77875" x2="16.18615" y2="7.79145" layer="21"/>
<rectangle x1="16.41475" y1="7.77875" x2="17.32915" y2="7.79145" layer="21"/>
<rectangle x1="4.76885" y1="7.79145" x2="6.02615" y2="7.80415" layer="21"/>
<rectangle x1="6.12775" y1="7.79145" x2="6.66115" y2="7.80415" layer="21"/>
<rectangle x1="6.85165" y1="7.79145" x2="7.01675" y2="7.80415" layer="21"/>
<rectangle x1="7.20725" y1="7.79145" x2="7.68985" y2="7.80415" layer="21"/>
<rectangle x1="7.89305" y1="7.79145" x2="8.04545" y2="7.80415" layer="21"/>
<rectangle x1="8.23595" y1="7.79145" x2="8.60425" y2="7.80415" layer="21"/>
<rectangle x1="8.69315" y1="7.79145" x2="9.26465" y2="7.80415" layer="21"/>
<rectangle x1="9.35355" y1="7.79145" x2="9.63295" y2="7.80415" layer="21"/>
<rectangle x1="9.72185" y1="7.79145" x2="10.29335" y2="7.80415" layer="21"/>
<rectangle x1="10.38225" y1="7.79145" x2="10.76325" y2="7.80415" layer="21"/>
<rectangle x1="10.96645" y1="7.79145" x2="11.11885" y2="7.80415" layer="21"/>
<rectangle x1="11.32205" y1="7.79145" x2="11.75385" y2="7.80415" layer="21"/>
<rectangle x1="11.84275" y1="7.79145" x2="12.26185" y2="7.80415" layer="21"/>
<rectangle x1="12.46505" y1="7.79145" x2="12.61745" y2="7.80415" layer="21"/>
<rectangle x1="12.82065" y1="7.79145" x2="13.36675" y2="7.80415" layer="21"/>
<rectangle x1="13.88745" y1="7.79145" x2="13.93825" y2="7.80415" layer="21"/>
<rectangle x1="14.02715" y1="7.79145" x2="14.37005" y2="7.80415" layer="21"/>
<rectangle x1="14.45895" y1="7.79145" x2="14.90345" y2="7.80415" layer="21"/>
<rectangle x1="15.09395" y1="7.79145" x2="15.25905" y2="7.80415" layer="21"/>
<rectangle x1="15.44955" y1="7.79145" x2="15.85595" y2="7.80415" layer="21"/>
<rectangle x1="16.03375" y1="7.79145" x2="16.26235" y2="7.80415" layer="21"/>
<rectangle x1="16.42745" y1="7.79145" x2="17.32915" y2="7.80415" layer="21"/>
<rectangle x1="4.76885" y1="7.80415" x2="6.02615" y2="7.81685" layer="21"/>
<rectangle x1="6.12775" y1="7.80415" x2="6.64845" y2="7.81685" layer="21"/>
<rectangle x1="6.81355" y1="7.80415" x2="7.06755" y2="7.81685" layer="21"/>
<rectangle x1="7.23265" y1="7.80415" x2="7.67715" y2="7.81685" layer="21"/>
<rectangle x1="7.84225" y1="7.80415" x2="8.08355" y2="7.81685" layer="21"/>
<rectangle x1="8.24865" y1="7.80415" x2="8.60425" y2="7.81685" layer="21"/>
<rectangle x1="8.69315" y1="7.80415" x2="9.26465" y2="7.81685" layer="21"/>
<rectangle x1="9.35355" y1="7.80415" x2="9.63295" y2="7.81685" layer="21"/>
<rectangle x1="9.72185" y1="7.80415" x2="10.29335" y2="7.81685" layer="21"/>
<rectangle x1="10.38225" y1="7.80415" x2="10.75055" y2="7.81685" layer="21"/>
<rectangle x1="10.92835" y1="7.80415" x2="11.16965" y2="7.81685" layer="21"/>
<rectangle x1="11.33475" y1="7.80415" x2="11.75385" y2="7.81685" layer="21"/>
<rectangle x1="11.84275" y1="7.80415" x2="12.24915" y2="7.81685" layer="21"/>
<rectangle x1="12.42695" y1="7.80415" x2="12.66825" y2="7.81685" layer="21"/>
<rectangle x1="12.83335" y1="7.80415" x2="13.35405" y2="7.81685" layer="21"/>
<rectangle x1="13.54455" y1="7.80415" x2="13.70965" y2="7.81685" layer="21"/>
<rectangle x1="13.90015" y1="7.80415" x2="13.93825" y2="7.81685" layer="21"/>
<rectangle x1="14.02715" y1="7.80415" x2="14.37005" y2="7.81685" layer="21"/>
<rectangle x1="14.45895" y1="7.80415" x2="14.89075" y2="7.81685" layer="21"/>
<rectangle x1="15.05585" y1="7.80415" x2="15.30985" y2="7.81685" layer="21"/>
<rectangle x1="15.47495" y1="7.80415" x2="15.84325" y2="7.81685" layer="21"/>
<rectangle x1="15.99565" y1="7.80415" x2="16.30045" y2="7.81685" layer="21"/>
<rectangle x1="16.44015" y1="7.80415" x2="17.32915" y2="7.81685" layer="21"/>
<rectangle x1="4.76885" y1="7.81685" x2="6.02615" y2="7.82955" layer="21"/>
<rectangle x1="6.12775" y1="7.81685" x2="6.62305" y2="7.82955" layer="21"/>
<rectangle x1="6.77545" y1="7.81685" x2="7.09295" y2="7.82955" layer="21"/>
<rectangle x1="7.23265" y1="7.81685" x2="7.66445" y2="7.82955" layer="21"/>
<rectangle x1="7.81685" y1="7.81685" x2="8.10895" y2="7.82955" layer="21"/>
<rectangle x1="8.26135" y1="7.81685" x2="8.60425" y2="7.82955" layer="21"/>
<rectangle x1="8.69315" y1="7.81685" x2="9.26465" y2="7.82955" layer="21"/>
<rectangle x1="9.35355" y1="7.81685" x2="9.63295" y2="7.82955" layer="21"/>
<rectangle x1="9.72185" y1="7.81685" x2="10.29335" y2="7.82955" layer="21"/>
<rectangle x1="10.38225" y1="7.81685" x2="10.73785" y2="7.82955" layer="21"/>
<rectangle x1="10.90295" y1="7.81685" x2="11.19505" y2="7.82955" layer="21"/>
<rectangle x1="11.34745" y1="7.81685" x2="11.75385" y2="7.82955" layer="21"/>
<rectangle x1="11.84275" y1="7.81685" x2="12.23645" y2="7.82955" layer="21"/>
<rectangle x1="12.40155" y1="7.81685" x2="12.69365" y2="7.82955" layer="21"/>
<rectangle x1="12.84605" y1="7.81685" x2="13.34135" y2="7.82955" layer="21"/>
<rectangle x1="13.50645" y1="7.81685" x2="13.74775" y2="7.82955" layer="21"/>
<rectangle x1="13.92555" y1="7.81685" x2="13.93825" y2="7.82955" layer="21"/>
<rectangle x1="14.02715" y1="7.81685" x2="14.37005" y2="7.82955" layer="21"/>
<rectangle x1="14.45895" y1="7.81685" x2="14.86535" y2="7.82955" layer="21"/>
<rectangle x1="15.01775" y1="7.81685" x2="15.33525" y2="7.82955" layer="21"/>
<rectangle x1="15.47495" y1="7.81685" x2="15.83055" y2="7.82955" layer="21"/>
<rectangle x1="15.97025" y1="7.81685" x2="16.32585" y2="7.82955" layer="21"/>
<rectangle x1="16.45285" y1="7.81685" x2="17.32915" y2="7.82955" layer="21"/>
<rectangle x1="4.76885" y1="7.82955" x2="6.02615" y2="7.84225" layer="21"/>
<rectangle x1="6.12775" y1="7.82955" x2="6.62305" y2="7.84225" layer="21"/>
<rectangle x1="6.76275" y1="7.82955" x2="7.11835" y2="7.84225" layer="21"/>
<rectangle x1="7.24535" y1="7.82955" x2="7.65175" y2="7.84225" layer="21"/>
<rectangle x1="7.79145" y1="7.82955" x2="8.13435" y2="7.84225" layer="21"/>
<rectangle x1="8.27405" y1="7.82955" x2="8.60425" y2="7.84225" layer="21"/>
<rectangle x1="8.69315" y1="7.82955" x2="9.26465" y2="7.84225" layer="21"/>
<rectangle x1="9.35355" y1="7.82955" x2="9.63295" y2="7.84225" layer="21"/>
<rectangle x1="9.72185" y1="7.82955" x2="10.29335" y2="7.84225" layer="21"/>
<rectangle x1="10.38225" y1="7.82955" x2="10.72515" y2="7.84225" layer="21"/>
<rectangle x1="10.87755" y1="7.82955" x2="11.22045" y2="7.84225" layer="21"/>
<rectangle x1="11.36015" y1="7.82955" x2="11.75385" y2="7.84225" layer="21"/>
<rectangle x1="11.84275" y1="7.82955" x2="12.22375" y2="7.84225" layer="21"/>
<rectangle x1="12.37615" y1="7.82955" x2="12.71905" y2="7.84225" layer="21"/>
<rectangle x1="12.85875" y1="7.82955" x2="13.32865" y2="7.84225" layer="21"/>
<rectangle x1="13.48105" y1="7.82955" x2="13.78585" y2="7.84225" layer="21"/>
<rectangle x1="14.02715" y1="7.82955" x2="14.37005" y2="7.84225" layer="21"/>
<rectangle x1="14.45895" y1="7.82955" x2="14.86535" y2="7.84225" layer="21"/>
<rectangle x1="15.00505" y1="7.82955" x2="15.36065" y2="7.84225" layer="21"/>
<rectangle x1="15.48765" y1="7.82955" x2="15.81785" y2="7.84225" layer="21"/>
<rectangle x1="15.94485" y1="7.82955" x2="16.33855" y2="7.84225" layer="21"/>
<rectangle x1="16.46555" y1="7.82955" x2="17.32915" y2="7.84225" layer="21"/>
<rectangle x1="4.76885" y1="7.84225" x2="6.02615" y2="7.85495" layer="21"/>
<rectangle x1="6.12775" y1="7.84225" x2="6.61035" y2="7.85495" layer="21"/>
<rectangle x1="6.73735" y1="7.84225" x2="7.13105" y2="7.85495" layer="21"/>
<rectangle x1="7.25805" y1="7.84225" x2="7.63905" y2="7.85495" layer="21"/>
<rectangle x1="7.76605" y1="7.84225" x2="8.14705" y2="7.85495" layer="21"/>
<rectangle x1="8.27405" y1="7.84225" x2="8.60425" y2="7.85495" layer="21"/>
<rectangle x1="8.69315" y1="7.84225" x2="9.26465" y2="7.85495" layer="21"/>
<rectangle x1="9.35355" y1="7.84225" x2="9.63295" y2="7.85495" layer="21"/>
<rectangle x1="9.72185" y1="7.84225" x2="10.29335" y2="7.85495" layer="21"/>
<rectangle x1="10.38225" y1="7.84225" x2="10.71245" y2="7.85495" layer="21"/>
<rectangle x1="10.85215" y1="7.84225" x2="11.24585" y2="7.85495" layer="21"/>
<rectangle x1="11.37285" y1="7.84225" x2="11.75385" y2="7.85495" layer="21"/>
<rectangle x1="11.84275" y1="7.84225" x2="12.21105" y2="7.85495" layer="21"/>
<rectangle x1="12.35075" y1="7.84225" x2="12.74445" y2="7.85495" layer="21"/>
<rectangle x1="12.87145" y1="7.84225" x2="13.31595" y2="7.85495" layer="21"/>
<rectangle x1="13.45565" y1="7.84225" x2="13.81125" y2="7.85495" layer="21"/>
<rectangle x1="14.02715" y1="7.84225" x2="14.37005" y2="7.85495" layer="21"/>
<rectangle x1="14.45895" y1="7.84225" x2="14.85265" y2="7.85495" layer="21"/>
<rectangle x1="14.97965" y1="7.84225" x2="15.37335" y2="7.85495" layer="21"/>
<rectangle x1="15.50035" y1="7.84225" x2="15.81785" y2="7.85495" layer="21"/>
<rectangle x1="15.93215" y1="7.84225" x2="16.36395" y2="7.85495" layer="21"/>
<rectangle x1="16.46555" y1="7.84225" x2="17.32915" y2="7.85495" layer="21"/>
<rectangle x1="4.76885" y1="7.85495" x2="6.02615" y2="7.86765" layer="21"/>
<rectangle x1="6.12775" y1="7.85495" x2="6.59765" y2="7.86765" layer="21"/>
<rectangle x1="6.72465" y1="7.85495" x2="7.14375" y2="7.86765" layer="21"/>
<rectangle x1="7.27075" y1="7.85495" x2="7.62635" y2="7.86765" layer="21"/>
<rectangle x1="7.75335" y1="7.85495" x2="8.17245" y2="7.86765" layer="21"/>
<rectangle x1="8.28675" y1="7.85495" x2="8.60425" y2="7.86765" layer="21"/>
<rectangle x1="8.69315" y1="7.85495" x2="9.26465" y2="7.86765" layer="21"/>
<rectangle x1="9.35355" y1="7.85495" x2="9.63295" y2="7.86765" layer="21"/>
<rectangle x1="9.72185" y1="7.85495" x2="10.29335" y2="7.86765" layer="21"/>
<rectangle x1="10.38225" y1="7.85495" x2="10.69975" y2="7.86765" layer="21"/>
<rectangle x1="10.83945" y1="7.85495" x2="11.25855" y2="7.86765" layer="21"/>
<rectangle x1="11.38555" y1="7.85495" x2="11.75385" y2="7.86765" layer="21"/>
<rectangle x1="11.84275" y1="7.85495" x2="12.19835" y2="7.86765" layer="21"/>
<rectangle x1="12.33805" y1="7.85495" x2="12.75715" y2="7.86765" layer="21"/>
<rectangle x1="12.88415" y1="7.85495" x2="13.30325" y2="7.86765" layer="21"/>
<rectangle x1="13.44295" y1="7.85495" x2="13.83665" y2="7.86765" layer="21"/>
<rectangle x1="14.02715" y1="7.85495" x2="14.37005" y2="7.86765" layer="21"/>
<rectangle x1="14.45895" y1="7.85495" x2="14.83995" y2="7.86765" layer="21"/>
<rectangle x1="14.96695" y1="7.85495" x2="15.38605" y2="7.86765" layer="21"/>
<rectangle x1="15.51305" y1="7.85495" x2="15.80515" y2="7.86765" layer="21"/>
<rectangle x1="15.91945" y1="7.85495" x2="16.36395" y2="7.86765" layer="21"/>
<rectangle x1="16.47825" y1="7.85495" x2="17.32915" y2="7.86765" layer="21"/>
<rectangle x1="4.76885" y1="7.86765" x2="6.02615" y2="7.88035" layer="21"/>
<rectangle x1="6.12775" y1="7.86765" x2="6.58495" y2="7.88035" layer="21"/>
<rectangle x1="6.71195" y1="7.86765" x2="7.15645" y2="7.88035" layer="21"/>
<rectangle x1="7.27075" y1="7.86765" x2="7.61365" y2="7.88035" layer="21"/>
<rectangle x1="7.74065" y1="7.86765" x2="8.18515" y2="7.88035" layer="21"/>
<rectangle x1="8.29945" y1="7.86765" x2="8.60425" y2="7.88035" layer="21"/>
<rectangle x1="8.69315" y1="7.86765" x2="9.26465" y2="7.88035" layer="21"/>
<rectangle x1="9.35355" y1="7.86765" x2="9.63295" y2="7.88035" layer="21"/>
<rectangle x1="9.72185" y1="7.86765" x2="10.29335" y2="7.88035" layer="21"/>
<rectangle x1="10.38225" y1="7.86765" x2="10.69975" y2="7.88035" layer="21"/>
<rectangle x1="10.82675" y1="7.86765" x2="11.27125" y2="7.88035" layer="21"/>
<rectangle x1="11.39825" y1="7.86765" x2="11.75385" y2="7.88035" layer="21"/>
<rectangle x1="11.84275" y1="7.86765" x2="12.19835" y2="7.88035" layer="21"/>
<rectangle x1="12.32535" y1="7.86765" x2="12.76985" y2="7.88035" layer="21"/>
<rectangle x1="12.89685" y1="7.86765" x2="13.29055" y2="7.88035" layer="21"/>
<rectangle x1="13.41755" y1="7.86765" x2="13.84935" y2="7.88035" layer="21"/>
<rectangle x1="14.02715" y1="7.86765" x2="14.37005" y2="7.88035" layer="21"/>
<rectangle x1="14.45895" y1="7.86765" x2="14.82725" y2="7.88035" layer="21"/>
<rectangle x1="14.95425" y1="7.86765" x2="15.39875" y2="7.88035" layer="21"/>
<rectangle x1="15.51305" y1="7.86765" x2="15.79245" y2="7.88035" layer="21"/>
<rectangle x1="15.90675" y1="7.86765" x2="16.37665" y2="7.88035" layer="21"/>
<rectangle x1="16.47825" y1="7.86765" x2="17.32915" y2="7.88035" layer="21"/>
<rectangle x1="4.76885" y1="7.88035" x2="6.02615" y2="7.89305" layer="21"/>
<rectangle x1="6.12775" y1="7.88035" x2="6.57225" y2="7.89305" layer="21"/>
<rectangle x1="6.69925" y1="7.88035" x2="7.16915" y2="7.89305" layer="21"/>
<rectangle x1="7.28345" y1="7.88035" x2="7.61365" y2="7.89305" layer="21"/>
<rectangle x1="7.72795" y1="7.88035" x2="8.19785" y2="7.89305" layer="21"/>
<rectangle x1="8.29945" y1="7.88035" x2="8.60425" y2="7.89305" layer="21"/>
<rectangle x1="8.69315" y1="7.88035" x2="9.26465" y2="7.89305" layer="21"/>
<rectangle x1="9.35355" y1="7.88035" x2="9.63295" y2="7.89305" layer="21"/>
<rectangle x1="9.72185" y1="7.88035" x2="10.29335" y2="7.89305" layer="21"/>
<rectangle x1="10.38225" y1="7.88035" x2="10.68705" y2="7.89305" layer="21"/>
<rectangle x1="10.81405" y1="7.88035" x2="11.28395" y2="7.89305" layer="21"/>
<rectangle x1="11.41095" y1="7.88035" x2="11.75385" y2="7.89305" layer="21"/>
<rectangle x1="11.84275" y1="7.88035" x2="12.18565" y2="7.89305" layer="21"/>
<rectangle x1="12.31265" y1="7.88035" x2="12.78255" y2="7.89305" layer="21"/>
<rectangle x1="12.90955" y1="7.88035" x2="13.27785" y2="7.89305" layer="21"/>
<rectangle x1="13.40485" y1="7.88035" x2="13.87475" y2="7.89305" layer="21"/>
<rectangle x1="14.02715" y1="7.88035" x2="14.37005" y2="7.89305" layer="21"/>
<rectangle x1="14.45895" y1="7.88035" x2="14.81455" y2="7.89305" layer="21"/>
<rectangle x1="14.94155" y1="7.88035" x2="15.41145" y2="7.89305" layer="21"/>
<rectangle x1="15.52575" y1="7.88035" x2="15.79245" y2="7.89305" layer="21"/>
<rectangle x1="15.89405" y1="7.88035" x2="16.38935" y2="7.89305" layer="21"/>
<rectangle x1="16.47825" y1="7.88035" x2="17.32915" y2="7.89305" layer="21"/>
<rectangle x1="4.76885" y1="7.89305" x2="6.02615" y2="7.90575" layer="21"/>
<rectangle x1="6.12775" y1="7.89305" x2="6.57225" y2="7.90575" layer="21"/>
<rectangle x1="6.68655" y1="7.89305" x2="7.18185" y2="7.90575" layer="21"/>
<rectangle x1="7.28345" y1="7.89305" x2="7.60095" y2="7.90575" layer="21"/>
<rectangle x1="7.71525" y1="7.89305" x2="8.19785" y2="7.90575" layer="21"/>
<rectangle x1="8.31215" y1="7.89305" x2="8.60425" y2="7.90575" layer="21"/>
<rectangle x1="8.69315" y1="7.89305" x2="9.26465" y2="7.90575" layer="21"/>
<rectangle x1="9.35355" y1="7.89305" x2="9.63295" y2="7.90575" layer="21"/>
<rectangle x1="9.72185" y1="7.89305" x2="10.29335" y2="7.90575" layer="21"/>
<rectangle x1="10.38225" y1="7.89305" x2="10.67435" y2="7.90575" layer="21"/>
<rectangle x1="10.80135" y1="7.89305" x2="11.29665" y2="7.90575" layer="21"/>
<rectangle x1="11.41095" y1="7.89305" x2="11.75385" y2="7.90575" layer="21"/>
<rectangle x1="11.84275" y1="7.89305" x2="12.17295" y2="7.90575" layer="21"/>
<rectangle x1="12.29995" y1="7.89305" x2="12.79525" y2="7.90575" layer="21"/>
<rectangle x1="12.90955" y1="7.89305" x2="13.27785" y2="7.90575" layer="21"/>
<rectangle x1="13.39215" y1="7.89305" x2="13.88745" y2="7.90575" layer="21"/>
<rectangle x1="14.02715" y1="7.89305" x2="14.37005" y2="7.90575" layer="21"/>
<rectangle x1="14.45895" y1="7.89305" x2="14.81455" y2="7.90575" layer="21"/>
<rectangle x1="14.92885" y1="7.89305" x2="15.42415" y2="7.90575" layer="21"/>
<rectangle x1="15.52575" y1="7.89305" x2="15.79245" y2="7.90575" layer="21"/>
<rectangle x1="15.89405" y1="7.89305" x2="16.38935" y2="7.90575" layer="21"/>
<rectangle x1="16.47825" y1="7.89305" x2="17.32915" y2="7.90575" layer="21"/>
<rectangle x1="4.76885" y1="7.90575" x2="6.02615" y2="7.91845" layer="21"/>
<rectangle x1="6.12775" y1="7.90575" x2="6.55955" y2="7.91845" layer="21"/>
<rectangle x1="6.67385" y1="7.90575" x2="7.18185" y2="7.91845" layer="21"/>
<rectangle x1="7.29615" y1="7.90575" x2="7.58825" y2="7.91845" layer="21"/>
<rectangle x1="7.70255" y1="7.90575" x2="8.21055" y2="7.91845" layer="21"/>
<rectangle x1="8.31215" y1="7.90575" x2="8.60425" y2="7.91845" layer="21"/>
<rectangle x1="8.69315" y1="7.90575" x2="9.26465" y2="7.91845" layer="21"/>
<rectangle x1="9.35355" y1="7.90575" x2="9.63295" y2="7.91845" layer="21"/>
<rectangle x1="9.72185" y1="7.90575" x2="10.29335" y2="7.91845" layer="21"/>
<rectangle x1="10.38225" y1="7.90575" x2="10.67435" y2="7.91845" layer="21"/>
<rectangle x1="10.78865" y1="7.90575" x2="11.30935" y2="7.91845" layer="21"/>
<rectangle x1="11.42365" y1="7.90575" x2="11.75385" y2="7.91845" layer="21"/>
<rectangle x1="11.84275" y1="7.90575" x2="12.17295" y2="7.91845" layer="21"/>
<rectangle x1="12.28725" y1="7.90575" x2="12.80795" y2="7.91845" layer="21"/>
<rectangle x1="12.92225" y1="7.90575" x2="13.26515" y2="7.91845" layer="21"/>
<rectangle x1="13.37945" y1="7.90575" x2="13.90015" y2="7.91845" layer="21"/>
<rectangle x1="14.02715" y1="7.90575" x2="14.37005" y2="7.91845" layer="21"/>
<rectangle x1="14.45895" y1="7.90575" x2="14.80185" y2="7.91845" layer="21"/>
<rectangle x1="14.91615" y1="7.90575" x2="15.42415" y2="7.91845" layer="21"/>
<rectangle x1="15.53845" y1="7.90575" x2="15.77975" y2="7.91845" layer="21"/>
<rectangle x1="15.88135" y1="7.90575" x2="16.38935" y2="7.91845" layer="21"/>
<rectangle x1="16.49095" y1="7.90575" x2="17.32915" y2="7.91845" layer="21"/>
<rectangle x1="4.76885" y1="7.91845" x2="6.02615" y2="7.93115" layer="21"/>
<rectangle x1="6.12775" y1="7.91845" x2="6.55955" y2="7.93115" layer="21"/>
<rectangle x1="6.66115" y1="7.91845" x2="7.58825" y2="7.93115" layer="21"/>
<rectangle x1="7.70255" y1="7.91845" x2="8.22325" y2="7.93115" layer="21"/>
<rectangle x1="8.32485" y1="7.91845" x2="8.60425" y2="7.93115" layer="21"/>
<rectangle x1="8.69315" y1="7.91845" x2="9.26465" y2="7.93115" layer="21"/>
<rectangle x1="9.35355" y1="7.91845" x2="9.63295" y2="7.93115" layer="21"/>
<rectangle x1="9.72185" y1="7.91845" x2="10.29335" y2="7.93115" layer="21"/>
<rectangle x1="10.38225" y1="7.91845" x2="10.66165" y2="7.93115" layer="21"/>
<rectangle x1="10.77595" y1="7.91845" x2="11.32205" y2="7.93115" layer="21"/>
<rectangle x1="11.43635" y1="7.91845" x2="11.75385" y2="7.93115" layer="21"/>
<rectangle x1="11.84275" y1="7.91845" x2="12.16025" y2="7.93115" layer="21"/>
<rectangle x1="12.27455" y1="7.91845" x2="12.82065" y2="7.93115" layer="21"/>
<rectangle x1="12.93495" y1="7.91845" x2="13.25245" y2="7.93115" layer="21"/>
<rectangle x1="13.36675" y1="7.91845" x2="13.91285" y2="7.93115" layer="21"/>
<rectangle x1="14.02715" y1="7.91845" x2="14.37005" y2="7.93115" layer="21"/>
<rectangle x1="14.45895" y1="7.91845" x2="14.80185" y2="7.93115" layer="21"/>
<rectangle x1="14.90345" y1="7.91845" x2="15.77975" y2="7.93115" layer="21"/>
<rectangle x1="15.86865" y1="7.91845" x2="16.40205" y2="7.93115" layer="21"/>
<rectangle x1="16.49095" y1="7.91845" x2="17.32915" y2="7.93115" layer="21"/>
<rectangle x1="4.76885" y1="7.93115" x2="6.02615" y2="7.94385" layer="21"/>
<rectangle x1="6.12775" y1="7.93115" x2="6.54685" y2="7.94385" layer="21"/>
<rectangle x1="6.66115" y1="7.93115" x2="7.57555" y2="7.94385" layer="21"/>
<rectangle x1="7.68985" y1="7.93115" x2="8.22325" y2="7.94385" layer="21"/>
<rectangle x1="8.32485" y1="7.93115" x2="8.60425" y2="7.94385" layer="21"/>
<rectangle x1="8.69315" y1="7.93115" x2="9.26465" y2="7.94385" layer="21"/>
<rectangle x1="9.35355" y1="7.93115" x2="9.63295" y2="7.94385" layer="21"/>
<rectangle x1="9.72185" y1="7.93115" x2="10.29335" y2="7.94385" layer="21"/>
<rectangle x1="10.38225" y1="7.93115" x2="10.66165" y2="7.94385" layer="21"/>
<rectangle x1="10.76325" y1="7.93115" x2="11.33475" y2="7.94385" layer="21"/>
<rectangle x1="11.43635" y1="7.93115" x2="11.75385" y2="7.94385" layer="21"/>
<rectangle x1="11.84275" y1="7.93115" x2="12.16025" y2="7.94385" layer="21"/>
<rectangle x1="12.26185" y1="7.93115" x2="12.83335" y2="7.94385" layer="21"/>
<rectangle x1="12.93495" y1="7.93115" x2="13.25245" y2="7.94385" layer="21"/>
<rectangle x1="13.36675" y1="7.93115" x2="13.92555" y2="7.94385" layer="21"/>
<rectangle x1="14.02715" y1="7.93115" x2="14.37005" y2="7.94385" layer="21"/>
<rectangle x1="14.45895" y1="7.93115" x2="14.78915" y2="7.94385" layer="21"/>
<rectangle x1="14.90345" y1="7.93115" x2="15.77975" y2="7.94385" layer="21"/>
<rectangle x1="15.86865" y1="7.93115" x2="16.40205" y2="7.94385" layer="21"/>
<rectangle x1="16.49095" y1="7.93115" x2="17.32915" y2="7.94385" layer="21"/>
<rectangle x1="4.76885" y1="7.94385" x2="6.02615" y2="7.95655" layer="21"/>
<rectangle x1="6.12775" y1="7.94385" x2="6.54685" y2="7.95655" layer="21"/>
<rectangle x1="6.64845" y1="7.94385" x2="7.57555" y2="7.95655" layer="21"/>
<rectangle x1="7.67715" y1="7.94385" x2="8.23595" y2="7.95655" layer="21"/>
<rectangle x1="8.33755" y1="7.94385" x2="8.60425" y2="7.95655" layer="21"/>
<rectangle x1="8.69315" y1="7.94385" x2="9.26465" y2="7.95655" layer="21"/>
<rectangle x1="9.35355" y1="7.94385" x2="9.63295" y2="7.95655" layer="21"/>
<rectangle x1="9.72185" y1="7.94385" x2="10.29335" y2="7.95655" layer="21"/>
<rectangle x1="10.38225" y1="7.94385" x2="10.64895" y2="7.95655" layer="21"/>
<rectangle x1="10.76325" y1="7.94385" x2="11.33475" y2="7.95655" layer="21"/>
<rectangle x1="11.44905" y1="7.94385" x2="11.75385" y2="7.95655" layer="21"/>
<rectangle x1="11.84275" y1="7.94385" x2="12.14755" y2="7.95655" layer="21"/>
<rectangle x1="12.26185" y1="7.94385" x2="12.83335" y2="7.95655" layer="21"/>
<rectangle x1="12.94765" y1="7.94385" x2="13.23975" y2="7.95655" layer="21"/>
<rectangle x1="13.35405" y1="7.94385" x2="13.92555" y2="7.95655" layer="21"/>
<rectangle x1="14.02715" y1="7.94385" x2="14.37005" y2="7.95655" layer="21"/>
<rectangle x1="14.45895" y1="7.94385" x2="14.78915" y2="7.95655" layer="21"/>
<rectangle x1="14.89075" y1="7.94385" x2="15.76705" y2="7.95655" layer="21"/>
<rectangle x1="15.86865" y1="7.94385" x2="16.40205" y2="7.95655" layer="21"/>
<rectangle x1="16.49095" y1="7.94385" x2="17.32915" y2="7.95655" layer="21"/>
<rectangle x1="4.76885" y1="7.95655" x2="6.02615" y2="7.96925" layer="21"/>
<rectangle x1="6.12775" y1="7.95655" x2="6.53415" y2="7.96925" layer="21"/>
<rectangle x1="6.63575" y1="7.95655" x2="7.56285" y2="7.96925" layer="21"/>
<rectangle x1="7.67715" y1="7.95655" x2="8.23595" y2="7.96925" layer="21"/>
<rectangle x1="8.33755" y1="7.95655" x2="8.60425" y2="7.96925" layer="21"/>
<rectangle x1="8.69315" y1="7.95655" x2="9.26465" y2="7.96925" layer="21"/>
<rectangle x1="9.35355" y1="7.95655" x2="9.63295" y2="7.96925" layer="21"/>
<rectangle x1="9.72185" y1="7.95655" x2="10.29335" y2="7.96925" layer="21"/>
<rectangle x1="10.38225" y1="7.95655" x2="10.64895" y2="7.96925" layer="21"/>
<rectangle x1="10.75055" y1="7.95655" x2="11.34745" y2="7.96925" layer="21"/>
<rectangle x1="11.44905" y1="7.95655" x2="11.75385" y2="7.96925" layer="21"/>
<rectangle x1="11.84275" y1="7.95655" x2="12.14755" y2="7.96925" layer="21"/>
<rectangle x1="12.24915" y1="7.95655" x2="12.84605" y2="7.96925" layer="21"/>
<rectangle x1="12.94765" y1="7.95655" x2="13.23975" y2="7.96925" layer="21"/>
<rectangle x1="13.34135" y1="7.95655" x2="13.93825" y2="7.96925" layer="21"/>
<rectangle x1="14.02715" y1="7.95655" x2="14.37005" y2="7.96925" layer="21"/>
<rectangle x1="14.45895" y1="7.95655" x2="14.77645" y2="7.96925" layer="21"/>
<rectangle x1="14.87805" y1="7.95655" x2="15.76705" y2="7.96925" layer="21"/>
<rectangle x1="15.85595" y1="7.95655" x2="16.40205" y2="7.96925" layer="21"/>
<rectangle x1="16.49095" y1="7.95655" x2="17.32915" y2="7.96925" layer="21"/>
<rectangle x1="4.76885" y1="7.96925" x2="6.02615" y2="7.98195" layer="21"/>
<rectangle x1="6.12775" y1="7.96925" x2="6.53415" y2="7.98195" layer="21"/>
<rectangle x1="6.63575" y1="7.96925" x2="7.56285" y2="7.98195" layer="21"/>
<rectangle x1="7.66445" y1="7.96925" x2="8.23595" y2="7.98195" layer="21"/>
<rectangle x1="8.33755" y1="7.96925" x2="8.60425" y2="7.98195" layer="21"/>
<rectangle x1="8.69315" y1="7.96925" x2="9.26465" y2="7.98195" layer="21"/>
<rectangle x1="9.35355" y1="7.96925" x2="9.63295" y2="7.98195" layer="21"/>
<rectangle x1="9.72185" y1="7.96925" x2="10.29335" y2="7.98195" layer="21"/>
<rectangle x1="10.38225" y1="7.96925" x2="10.63625" y2="7.98195" layer="21"/>
<rectangle x1="10.73785" y1="7.96925" x2="11.34745" y2="7.98195" layer="21"/>
<rectangle x1="11.44905" y1="7.96925" x2="11.75385" y2="7.98195" layer="21"/>
<rectangle x1="11.84275" y1="7.96925" x2="12.13485" y2="7.98195" layer="21"/>
<rectangle x1="12.23645" y1="7.96925" x2="12.84605" y2="7.98195" layer="21"/>
<rectangle x1="12.94765" y1="7.96925" x2="13.22705" y2="7.98195" layer="21"/>
<rectangle x1="13.34135" y1="7.96925" x2="13.93825" y2="7.98195" layer="21"/>
<rectangle x1="14.02715" y1="7.96925" x2="14.37005" y2="7.98195" layer="21"/>
<rectangle x1="14.45895" y1="7.96925" x2="14.77645" y2="7.98195" layer="21"/>
<rectangle x1="14.87805" y1="7.96925" x2="16.38935" y2="7.98195" layer="21"/>
<rectangle x1="16.49095" y1="7.96925" x2="17.32915" y2="7.98195" layer="21"/>
<rectangle x1="4.76885" y1="7.98195" x2="6.02615" y2="7.99465" layer="21"/>
<rectangle x1="6.12775" y1="7.98195" x2="6.53415" y2="7.99465" layer="21"/>
<rectangle x1="6.63575" y1="7.98195" x2="7.56285" y2="7.99465" layer="21"/>
<rectangle x1="7.66445" y1="7.98195" x2="8.24865" y2="7.99465" layer="21"/>
<rectangle x1="8.33755" y1="7.98195" x2="8.60425" y2="7.99465" layer="21"/>
<rectangle x1="8.69315" y1="7.98195" x2="9.26465" y2="7.99465" layer="21"/>
<rectangle x1="9.35355" y1="7.98195" x2="9.63295" y2="7.99465" layer="21"/>
<rectangle x1="9.72185" y1="7.98195" x2="10.29335" y2="7.99465" layer="21"/>
<rectangle x1="10.38225" y1="7.98195" x2="10.63625" y2="7.99465" layer="21"/>
<rectangle x1="10.73785" y1="7.98195" x2="11.36015" y2="7.99465" layer="21"/>
<rectangle x1="11.46175" y1="7.98195" x2="11.75385" y2="7.99465" layer="21"/>
<rectangle x1="11.84275" y1="7.98195" x2="12.13485" y2="7.99465" layer="21"/>
<rectangle x1="12.23645" y1="7.98195" x2="12.85875" y2="7.99465" layer="21"/>
<rectangle x1="12.96035" y1="7.98195" x2="13.22705" y2="7.99465" layer="21"/>
<rectangle x1="13.32865" y1="7.98195" x2="13.93825" y2="7.99465" layer="21"/>
<rectangle x1="14.02715" y1="7.98195" x2="14.37005" y2="7.99465" layer="21"/>
<rectangle x1="14.45895" y1="7.98195" x2="14.77645" y2="7.99465" layer="21"/>
<rectangle x1="14.87805" y1="7.98195" x2="16.38935" y2="7.99465" layer="21"/>
<rectangle x1="16.49095" y1="7.98195" x2="17.32915" y2="7.99465" layer="21"/>
<rectangle x1="4.76885" y1="7.99465" x2="6.02615" y2="8.00735" layer="21"/>
<rectangle x1="6.12775" y1="7.99465" x2="6.52145" y2="8.00735" layer="21"/>
<rectangle x1="6.62305" y1="7.99465" x2="7.55015" y2="8.00735" layer="21"/>
<rectangle x1="7.65175" y1="7.99465" x2="8.60425" y2="8.00735" layer="21"/>
<rectangle x1="8.69315" y1="7.99465" x2="9.26465" y2="8.00735" layer="21"/>
<rectangle x1="9.35355" y1="7.99465" x2="9.63295" y2="8.00735" layer="21"/>
<rectangle x1="9.72185" y1="7.99465" x2="10.29335" y2="8.00735" layer="21"/>
<rectangle x1="10.38225" y1="7.99465" x2="10.62355" y2="8.00735" layer="21"/>
<rectangle x1="10.73785" y1="7.99465" x2="11.36015" y2="8.00735" layer="21"/>
<rectangle x1="11.46175" y1="7.99465" x2="11.75385" y2="8.00735" layer="21"/>
<rectangle x1="11.84275" y1="7.99465" x2="12.12215" y2="8.00735" layer="21"/>
<rectangle x1="12.23645" y1="7.99465" x2="12.85875" y2="8.00735" layer="21"/>
<rectangle x1="12.96035" y1="7.99465" x2="13.22705" y2="8.00735" layer="21"/>
<rectangle x1="13.32865" y1="7.99465" x2="13.93825" y2="8.00735" layer="21"/>
<rectangle x1="14.02715" y1="7.99465" x2="14.37005" y2="8.00735" layer="21"/>
<rectangle x1="14.45895" y1="7.99465" x2="14.76375" y2="8.00735" layer="21"/>
<rectangle x1="14.86535" y1="7.99465" x2="16.38935" y2="8.00735" layer="21"/>
<rectangle x1="16.47825" y1="7.99465" x2="17.32915" y2="8.00735" layer="21"/>
<rectangle x1="4.76885" y1="8.00735" x2="6.02615" y2="8.02005" layer="21"/>
<rectangle x1="6.12775" y1="8.00735" x2="6.52145" y2="8.02005" layer="21"/>
<rectangle x1="6.62305" y1="8.00735" x2="7.55015" y2="8.02005" layer="21"/>
<rectangle x1="7.65175" y1="8.00735" x2="8.60425" y2="8.02005" layer="21"/>
<rectangle x1="8.69315" y1="8.00735" x2="9.26465" y2="8.02005" layer="21"/>
<rectangle x1="9.35355" y1="8.00735" x2="9.63295" y2="8.02005" layer="21"/>
<rectangle x1="9.72185" y1="8.00735" x2="10.29335" y2="8.02005" layer="21"/>
<rectangle x1="10.38225" y1="8.00735" x2="10.62355" y2="8.02005" layer="21"/>
<rectangle x1="10.72515" y1="8.00735" x2="11.37285" y2="8.02005" layer="21"/>
<rectangle x1="11.46175" y1="8.00735" x2="11.75385" y2="8.02005" layer="21"/>
<rectangle x1="11.84275" y1="8.00735" x2="12.12215" y2="8.02005" layer="21"/>
<rectangle x1="12.22375" y1="8.00735" x2="12.87145" y2="8.02005" layer="21"/>
<rectangle x1="12.96035" y1="8.00735" x2="13.21435" y2="8.02005" layer="21"/>
<rectangle x1="13.31595" y1="8.00735" x2="13.93825" y2="8.02005" layer="21"/>
<rectangle x1="14.02715" y1="8.00735" x2="14.37005" y2="8.02005" layer="21"/>
<rectangle x1="14.45895" y1="8.00735" x2="14.76375" y2="8.02005" layer="21"/>
<rectangle x1="14.86535" y1="8.00735" x2="16.37665" y2="8.02005" layer="21"/>
<rectangle x1="16.47825" y1="8.00735" x2="17.32915" y2="8.02005" layer="21"/>
<rectangle x1="4.76885" y1="8.02005" x2="6.02615" y2="8.03275" layer="21"/>
<rectangle x1="6.12775" y1="8.02005" x2="6.52145" y2="8.03275" layer="21"/>
<rectangle x1="6.62305" y1="8.02005" x2="7.55015" y2="8.03275" layer="21"/>
<rectangle x1="7.65175" y1="8.02005" x2="8.60425" y2="8.03275" layer="21"/>
<rectangle x1="8.69315" y1="8.02005" x2="9.26465" y2="8.03275" layer="21"/>
<rectangle x1="9.35355" y1="8.02005" x2="9.63295" y2="8.03275" layer="21"/>
<rectangle x1="9.72185" y1="8.02005" x2="10.29335" y2="8.03275" layer="21"/>
<rectangle x1="10.38225" y1="8.02005" x2="10.62355" y2="8.03275" layer="21"/>
<rectangle x1="10.72515" y1="8.02005" x2="11.37285" y2="8.03275" layer="21"/>
<rectangle x1="11.47445" y1="8.02005" x2="11.75385" y2="8.03275" layer="21"/>
<rectangle x1="11.84275" y1="8.02005" x2="12.12215" y2="8.03275" layer="21"/>
<rectangle x1="12.22375" y1="8.02005" x2="12.87145" y2="8.03275" layer="21"/>
<rectangle x1="12.97305" y1="8.02005" x2="13.21435" y2="8.03275" layer="21"/>
<rectangle x1="13.31595" y1="8.02005" x2="13.93825" y2="8.03275" layer="21"/>
<rectangle x1="14.02715" y1="8.02005" x2="14.37005" y2="8.03275" layer="21"/>
<rectangle x1="14.45895" y1="8.02005" x2="14.76375" y2="8.03275" layer="21"/>
<rectangle x1="14.86535" y1="8.02005" x2="16.36395" y2="8.03275" layer="21"/>
<rectangle x1="16.47825" y1="8.02005" x2="17.32915" y2="8.03275" layer="21"/>
<rectangle x1="4.76885" y1="8.03275" x2="6.02615" y2="8.04545" layer="21"/>
<rectangle x1="6.12775" y1="8.03275" x2="6.52145" y2="8.04545" layer="21"/>
<rectangle x1="6.61035" y1="8.03275" x2="7.55015" y2="8.04545" layer="21"/>
<rectangle x1="7.63905" y1="8.03275" x2="8.60425" y2="8.04545" layer="21"/>
<rectangle x1="8.69315" y1="8.03275" x2="9.26465" y2="8.04545" layer="21"/>
<rectangle x1="9.35355" y1="8.03275" x2="9.63295" y2="8.04545" layer="21"/>
<rectangle x1="9.72185" y1="8.03275" x2="10.29335" y2="8.04545" layer="21"/>
<rectangle x1="10.38225" y1="8.03275" x2="10.62355" y2="8.04545" layer="21"/>
<rectangle x1="10.72515" y1="8.03275" x2="11.37285" y2="8.04545" layer="21"/>
<rectangle x1="11.47445" y1="8.03275" x2="11.75385" y2="8.04545" layer="21"/>
<rectangle x1="11.84275" y1="8.03275" x2="12.12215" y2="8.04545" layer="21"/>
<rectangle x1="12.22375" y1="8.03275" x2="12.87145" y2="8.04545" layer="21"/>
<rectangle x1="12.97305" y1="8.03275" x2="13.21435" y2="8.04545" layer="21"/>
<rectangle x1="13.31595" y1="8.03275" x2="13.93825" y2="8.04545" layer="21"/>
<rectangle x1="14.02715" y1="8.03275" x2="14.37005" y2="8.04545" layer="21"/>
<rectangle x1="14.45895" y1="8.03275" x2="14.76375" y2="8.04545" layer="21"/>
<rectangle x1="14.85265" y1="8.03275" x2="16.35125" y2="8.04545" layer="21"/>
<rectangle x1="16.46555" y1="8.03275" x2="17.32915" y2="8.04545" layer="21"/>
<rectangle x1="4.76885" y1="8.04545" x2="6.02615" y2="8.05815" layer="21"/>
<rectangle x1="6.12775" y1="8.04545" x2="6.50875" y2="8.05815" layer="21"/>
<rectangle x1="6.61035" y1="8.04545" x2="7.53745" y2="8.05815" layer="21"/>
<rectangle x1="7.63905" y1="8.04545" x2="8.60425" y2="8.05815" layer="21"/>
<rectangle x1="8.69315" y1="8.04545" x2="9.26465" y2="8.05815" layer="21"/>
<rectangle x1="9.35355" y1="8.04545" x2="9.63295" y2="8.05815" layer="21"/>
<rectangle x1="9.72185" y1="8.04545" x2="10.29335" y2="8.05815" layer="21"/>
<rectangle x1="10.38225" y1="8.04545" x2="10.62355" y2="8.05815" layer="21"/>
<rectangle x1="10.71245" y1="8.04545" x2="11.37285" y2="8.05815" layer="21"/>
<rectangle x1="11.47445" y1="8.04545" x2="11.75385" y2="8.05815" layer="21"/>
<rectangle x1="11.84275" y1="8.04545" x2="12.12215" y2="8.05815" layer="21"/>
<rectangle x1="12.21105" y1="8.04545" x2="12.87145" y2="8.05815" layer="21"/>
<rectangle x1="12.97305" y1="8.04545" x2="13.21435" y2="8.05815" layer="21"/>
<rectangle x1="13.30325" y1="8.04545" x2="13.93825" y2="8.05815" layer="21"/>
<rectangle x1="14.02715" y1="8.04545" x2="14.37005" y2="8.05815" layer="21"/>
<rectangle x1="14.45895" y1="8.04545" x2="14.75105" y2="8.05815" layer="21"/>
<rectangle x1="14.85265" y1="8.04545" x2="16.33855" y2="8.05815" layer="21"/>
<rectangle x1="16.46555" y1="8.04545" x2="17.32915" y2="8.05815" layer="21"/>
<rectangle x1="4.76885" y1="8.05815" x2="6.02615" y2="8.07085" layer="21"/>
<rectangle x1="6.12775" y1="8.05815" x2="6.50875" y2="8.07085" layer="21"/>
<rectangle x1="6.61035" y1="8.05815" x2="7.53745" y2="8.07085" layer="21"/>
<rectangle x1="7.63905" y1="8.05815" x2="8.60425" y2="8.07085" layer="21"/>
<rectangle x1="8.69315" y1="8.05815" x2="9.26465" y2="8.07085" layer="21"/>
<rectangle x1="9.35355" y1="8.05815" x2="9.63295" y2="8.07085" layer="21"/>
<rectangle x1="9.72185" y1="8.05815" x2="10.29335" y2="8.07085" layer="21"/>
<rectangle x1="10.38225" y1="8.05815" x2="10.61085" y2="8.07085" layer="21"/>
<rectangle x1="10.71245" y1="8.05815" x2="11.38555" y2="8.07085" layer="21"/>
<rectangle x1="11.47445" y1="8.05815" x2="11.75385" y2="8.07085" layer="21"/>
<rectangle x1="11.84275" y1="8.05815" x2="12.10945" y2="8.07085" layer="21"/>
<rectangle x1="12.21105" y1="8.05815" x2="12.88415" y2="8.07085" layer="21"/>
<rectangle x1="12.97305" y1="8.05815" x2="13.20165" y2="8.07085" layer="21"/>
<rectangle x1="13.30325" y1="8.05815" x2="13.93825" y2="8.07085" layer="21"/>
<rectangle x1="14.02715" y1="8.05815" x2="14.37005" y2="8.07085" layer="21"/>
<rectangle x1="14.45895" y1="8.05815" x2="14.75105" y2="8.07085" layer="21"/>
<rectangle x1="14.85265" y1="8.05815" x2="16.31315" y2="8.07085" layer="21"/>
<rectangle x1="16.45285" y1="8.05815" x2="17.32915" y2="8.07085" layer="21"/>
<rectangle x1="4.76885" y1="8.07085" x2="6.02615" y2="8.08355" layer="21"/>
<rectangle x1="6.12775" y1="8.07085" x2="6.50875" y2="8.08355" layer="21"/>
<rectangle x1="6.61035" y1="8.07085" x2="7.53745" y2="8.08355" layer="21"/>
<rectangle x1="7.63905" y1="8.07085" x2="8.60425" y2="8.08355" layer="21"/>
<rectangle x1="8.69315" y1="8.07085" x2="9.26465" y2="8.08355" layer="21"/>
<rectangle x1="9.35355" y1="8.07085" x2="9.63295" y2="8.08355" layer="21"/>
<rectangle x1="9.72185" y1="8.07085" x2="10.29335" y2="8.08355" layer="21"/>
<rectangle x1="10.38225" y1="8.07085" x2="10.61085" y2="8.08355" layer="21"/>
<rectangle x1="10.71245" y1="8.07085" x2="11.38555" y2="8.08355" layer="21"/>
<rectangle x1="11.47445" y1="8.07085" x2="11.75385" y2="8.08355" layer="21"/>
<rectangle x1="11.84275" y1="8.07085" x2="12.10945" y2="8.08355" layer="21"/>
<rectangle x1="12.21105" y1="8.07085" x2="12.88415" y2="8.08355" layer="21"/>
<rectangle x1="12.97305" y1="8.07085" x2="13.20165" y2="8.08355" layer="21"/>
<rectangle x1="13.30325" y1="8.07085" x2="13.93825" y2="8.08355" layer="21"/>
<rectangle x1="14.02715" y1="8.07085" x2="14.37005" y2="8.08355" layer="21"/>
<rectangle x1="14.45895" y1="8.07085" x2="14.75105" y2="8.08355" layer="21"/>
<rectangle x1="14.85265" y1="8.07085" x2="16.28775" y2="8.08355" layer="21"/>
<rectangle x1="16.44015" y1="8.07085" x2="17.32915" y2="8.08355" layer="21"/>
<rectangle x1="4.76885" y1="8.08355" x2="6.02615" y2="8.09625" layer="21"/>
<rectangle x1="6.12775" y1="8.08355" x2="6.50875" y2="8.09625" layer="21"/>
<rectangle x1="6.61035" y1="8.08355" x2="7.53745" y2="8.09625" layer="21"/>
<rectangle x1="7.63905" y1="8.08355" x2="8.60425" y2="8.09625" layer="21"/>
<rectangle x1="8.69315" y1="8.08355" x2="9.26465" y2="8.09625" layer="21"/>
<rectangle x1="9.35355" y1="8.08355" x2="9.63295" y2="8.09625" layer="21"/>
<rectangle x1="9.72185" y1="8.08355" x2="10.29335" y2="8.09625" layer="21"/>
<rectangle x1="10.38225" y1="8.08355" x2="10.61085" y2="8.09625" layer="21"/>
<rectangle x1="10.71245" y1="8.08355" x2="11.38555" y2="8.09625" layer="21"/>
<rectangle x1="11.47445" y1="8.08355" x2="11.75385" y2="8.09625" layer="21"/>
<rectangle x1="11.84275" y1="8.08355" x2="12.10945" y2="8.09625" layer="21"/>
<rectangle x1="12.21105" y1="8.08355" x2="12.88415" y2="8.09625" layer="21"/>
<rectangle x1="12.97305" y1="8.08355" x2="13.20165" y2="8.09625" layer="21"/>
<rectangle x1="13.30325" y1="8.08355" x2="13.93825" y2="8.09625" layer="21"/>
<rectangle x1="14.02715" y1="8.08355" x2="14.37005" y2="8.09625" layer="21"/>
<rectangle x1="14.45895" y1="8.08355" x2="14.75105" y2="8.09625" layer="21"/>
<rectangle x1="14.85265" y1="8.08355" x2="16.22425" y2="8.09625" layer="21"/>
<rectangle x1="16.44015" y1="8.08355" x2="17.32915" y2="8.09625" layer="21"/>
<rectangle x1="4.76885" y1="8.09625" x2="6.02615" y2="8.10895" layer="21"/>
<rectangle x1="6.12775" y1="8.09625" x2="6.50875" y2="8.10895" layer="21"/>
<rectangle x1="6.61035" y1="8.09625" x2="7.53745" y2="8.10895" layer="21"/>
<rectangle x1="7.63905" y1="8.09625" x2="8.60425" y2="8.10895" layer="21"/>
<rectangle x1="8.69315" y1="8.09625" x2="9.26465" y2="8.10895" layer="21"/>
<rectangle x1="9.35355" y1="8.09625" x2="9.63295" y2="8.10895" layer="21"/>
<rectangle x1="9.72185" y1="8.09625" x2="10.29335" y2="8.10895" layer="21"/>
<rectangle x1="10.38225" y1="8.09625" x2="10.61085" y2="8.10895" layer="21"/>
<rectangle x1="10.71245" y1="8.09625" x2="11.38555" y2="8.10895" layer="21"/>
<rectangle x1="11.47445" y1="8.09625" x2="11.75385" y2="8.10895" layer="21"/>
<rectangle x1="11.84275" y1="8.09625" x2="12.10945" y2="8.10895" layer="21"/>
<rectangle x1="12.21105" y1="8.09625" x2="12.88415" y2="8.10895" layer="21"/>
<rectangle x1="12.97305" y1="8.09625" x2="13.20165" y2="8.10895" layer="21"/>
<rectangle x1="13.30325" y1="8.09625" x2="13.93825" y2="8.10895" layer="21"/>
<rectangle x1="14.02715" y1="8.09625" x2="14.37005" y2="8.10895" layer="21"/>
<rectangle x1="14.45895" y1="8.09625" x2="14.75105" y2="8.10895" layer="21"/>
<rectangle x1="14.85265" y1="8.09625" x2="16.14805" y2="8.10895" layer="21"/>
<rectangle x1="16.42745" y1="8.09625" x2="17.32915" y2="8.10895" layer="21"/>
<rectangle x1="4.76885" y1="8.10895" x2="6.02615" y2="8.12165" layer="21"/>
<rectangle x1="6.12775" y1="8.10895" x2="6.50875" y2="8.12165" layer="21"/>
<rectangle x1="7.33425" y1="8.10895" x2="7.53745" y2="8.12165" layer="21"/>
<rectangle x1="7.63905" y1="8.10895" x2="8.60425" y2="8.12165" layer="21"/>
<rectangle x1="8.69315" y1="8.10895" x2="9.26465" y2="8.12165" layer="21"/>
<rectangle x1="9.35355" y1="8.10895" x2="9.63295" y2="8.12165" layer="21"/>
<rectangle x1="9.72185" y1="8.10895" x2="10.29335" y2="8.12165" layer="21"/>
<rectangle x1="10.38225" y1="8.10895" x2="10.61085" y2="8.12165" layer="21"/>
<rectangle x1="10.71245" y1="8.10895" x2="11.38555" y2="8.12165" layer="21"/>
<rectangle x1="11.47445" y1="8.10895" x2="11.75385" y2="8.12165" layer="21"/>
<rectangle x1="11.84275" y1="8.10895" x2="12.10945" y2="8.12165" layer="21"/>
<rectangle x1="12.21105" y1="8.10895" x2="12.88415" y2="8.12165" layer="21"/>
<rectangle x1="12.97305" y1="8.10895" x2="13.20165" y2="8.12165" layer="21"/>
<rectangle x1="13.30325" y1="8.10895" x2="13.93825" y2="8.12165" layer="21"/>
<rectangle x1="14.02715" y1="8.10895" x2="14.37005" y2="8.12165" layer="21"/>
<rectangle x1="14.45895" y1="8.10895" x2="14.75105" y2="8.12165" layer="21"/>
<rectangle x1="15.57655" y1="8.10895" x2="16.07185" y2="8.12165" layer="21"/>
<rectangle x1="16.40205" y1="8.10895" x2="17.32915" y2="8.12165" layer="21"/>
<rectangle x1="4.76885" y1="8.12165" x2="6.02615" y2="8.13435" layer="21"/>
<rectangle x1="6.12775" y1="8.12165" x2="6.50875" y2="8.13435" layer="21"/>
<rectangle x1="7.33425" y1="8.12165" x2="7.53745" y2="8.13435" layer="21"/>
<rectangle x1="7.62635" y1="8.12165" x2="8.60425" y2="8.13435" layer="21"/>
<rectangle x1="8.69315" y1="8.12165" x2="9.26465" y2="8.13435" layer="21"/>
<rectangle x1="9.35355" y1="8.12165" x2="9.63295" y2="8.13435" layer="21"/>
<rectangle x1="9.72185" y1="8.12165" x2="10.29335" y2="8.13435" layer="21"/>
<rectangle x1="10.38225" y1="8.12165" x2="10.61085" y2="8.13435" layer="21"/>
<rectangle x1="10.71245" y1="8.12165" x2="11.38555" y2="8.13435" layer="21"/>
<rectangle x1="11.47445" y1="8.12165" x2="11.75385" y2="8.13435" layer="21"/>
<rectangle x1="11.84275" y1="8.12165" x2="12.10945" y2="8.13435" layer="21"/>
<rectangle x1="12.19835" y1="8.12165" x2="12.88415" y2="8.13435" layer="21"/>
<rectangle x1="12.97305" y1="8.12165" x2="13.20165" y2="8.13435" layer="21"/>
<rectangle x1="13.30325" y1="8.12165" x2="13.93825" y2="8.13435" layer="21"/>
<rectangle x1="14.02715" y1="8.12165" x2="14.37005" y2="8.13435" layer="21"/>
<rectangle x1="14.45895" y1="8.12165" x2="14.75105" y2="8.13435" layer="21"/>
<rectangle x1="15.57655" y1="8.12165" x2="16.00835" y2="8.13435" layer="21"/>
<rectangle x1="16.38935" y1="8.12165" x2="17.32915" y2="8.13435" layer="21"/>
<rectangle x1="4.76885" y1="8.13435" x2="6.02615" y2="8.14705" layer="21"/>
<rectangle x1="6.12775" y1="8.13435" x2="6.50875" y2="8.14705" layer="21"/>
<rectangle x1="7.33425" y1="8.13435" x2="7.53745" y2="8.14705" layer="21"/>
<rectangle x1="7.63905" y1="8.13435" x2="8.60425" y2="8.14705" layer="21"/>
<rectangle x1="8.69315" y1="8.13435" x2="9.26465" y2="8.14705" layer="21"/>
<rectangle x1="9.35355" y1="8.13435" x2="9.63295" y2="8.14705" layer="21"/>
<rectangle x1="9.72185" y1="8.13435" x2="10.29335" y2="8.14705" layer="21"/>
<rectangle x1="10.38225" y1="8.13435" x2="10.61085" y2="8.14705" layer="21"/>
<rectangle x1="10.71245" y1="8.13435" x2="11.38555" y2="8.14705" layer="21"/>
<rectangle x1="11.47445" y1="8.13435" x2="11.75385" y2="8.14705" layer="21"/>
<rectangle x1="11.84275" y1="8.13435" x2="12.10945" y2="8.14705" layer="21"/>
<rectangle x1="12.21105" y1="8.13435" x2="12.88415" y2="8.14705" layer="21"/>
<rectangle x1="12.97305" y1="8.13435" x2="13.20165" y2="8.14705" layer="21"/>
<rectangle x1="13.29055" y1="8.13435" x2="13.93825" y2="8.14705" layer="21"/>
<rectangle x1="14.02715" y1="8.13435" x2="14.37005" y2="8.14705" layer="21"/>
<rectangle x1="14.45895" y1="8.13435" x2="14.75105" y2="8.14705" layer="21"/>
<rectangle x1="15.57655" y1="8.13435" x2="15.95755" y2="8.14705" layer="21"/>
<rectangle x1="16.36395" y1="8.13435" x2="17.32915" y2="8.14705" layer="21"/>
<rectangle x1="4.76885" y1="8.14705" x2="6.02615" y2="8.15975" layer="21"/>
<rectangle x1="6.12775" y1="8.14705" x2="6.50875" y2="8.15975" layer="21"/>
<rectangle x1="7.33425" y1="8.14705" x2="7.53745" y2="8.15975" layer="21"/>
<rectangle x1="7.63905" y1="8.14705" x2="8.60425" y2="8.15975" layer="21"/>
<rectangle x1="8.69315" y1="8.14705" x2="9.26465" y2="8.15975" layer="21"/>
<rectangle x1="9.35355" y1="8.14705" x2="9.63295" y2="8.15975" layer="21"/>
<rectangle x1="9.72185" y1="8.14705" x2="10.29335" y2="8.15975" layer="21"/>
<rectangle x1="10.38225" y1="8.14705" x2="10.61085" y2="8.15975" layer="21"/>
<rectangle x1="10.71245" y1="8.14705" x2="11.38555" y2="8.15975" layer="21"/>
<rectangle x1="11.47445" y1="8.14705" x2="11.75385" y2="8.15975" layer="21"/>
<rectangle x1="11.84275" y1="8.14705" x2="12.10945" y2="8.15975" layer="21"/>
<rectangle x1="12.21105" y1="8.14705" x2="12.88415" y2="8.15975" layer="21"/>
<rectangle x1="12.97305" y1="8.14705" x2="13.20165" y2="8.15975" layer="21"/>
<rectangle x1="13.30325" y1="8.14705" x2="13.93825" y2="8.15975" layer="21"/>
<rectangle x1="14.02715" y1="8.14705" x2="14.37005" y2="8.15975" layer="21"/>
<rectangle x1="14.45895" y1="8.14705" x2="14.75105" y2="8.15975" layer="21"/>
<rectangle x1="15.57655" y1="8.14705" x2="15.93215" y2="8.15975" layer="21"/>
<rectangle x1="16.32585" y1="8.14705" x2="17.32915" y2="8.15975" layer="21"/>
<rectangle x1="4.76885" y1="8.15975" x2="6.02615" y2="8.17245" layer="21"/>
<rectangle x1="6.12775" y1="8.15975" x2="6.50875" y2="8.17245" layer="21"/>
<rectangle x1="7.33425" y1="8.15975" x2="7.53745" y2="8.17245" layer="21"/>
<rectangle x1="7.63905" y1="8.15975" x2="8.60425" y2="8.17245" layer="21"/>
<rectangle x1="8.69315" y1="8.15975" x2="9.26465" y2="8.17245" layer="21"/>
<rectangle x1="9.35355" y1="8.15975" x2="9.63295" y2="8.17245" layer="21"/>
<rectangle x1="9.72185" y1="8.15975" x2="10.29335" y2="8.17245" layer="21"/>
<rectangle x1="10.38225" y1="8.15975" x2="10.61085" y2="8.17245" layer="21"/>
<rectangle x1="10.71245" y1="8.15975" x2="11.38555" y2="8.17245" layer="21"/>
<rectangle x1="11.47445" y1="8.15975" x2="11.75385" y2="8.17245" layer="21"/>
<rectangle x1="11.84275" y1="8.15975" x2="12.10945" y2="8.17245" layer="21"/>
<rectangle x1="12.21105" y1="8.15975" x2="12.88415" y2="8.17245" layer="21"/>
<rectangle x1="12.97305" y1="8.15975" x2="13.20165" y2="8.17245" layer="21"/>
<rectangle x1="13.30325" y1="8.15975" x2="13.93825" y2="8.17245" layer="21"/>
<rectangle x1="14.02715" y1="8.15975" x2="14.37005" y2="8.17245" layer="21"/>
<rectangle x1="14.45895" y1="8.15975" x2="14.75105" y2="8.17245" layer="21"/>
<rectangle x1="15.57655" y1="8.15975" x2="15.90675" y2="8.17245" layer="21"/>
<rectangle x1="16.28775" y1="8.15975" x2="17.32915" y2="8.17245" layer="21"/>
<rectangle x1="4.76885" y1="8.17245" x2="6.02615" y2="8.18515" layer="21"/>
<rectangle x1="6.12775" y1="8.17245" x2="6.50875" y2="8.18515" layer="21"/>
<rectangle x1="6.61035" y1="8.17245" x2="7.23265" y2="8.18515" layer="21"/>
<rectangle x1="7.33425" y1="8.17245" x2="7.53745" y2="8.18515" layer="21"/>
<rectangle x1="7.63905" y1="8.17245" x2="8.60425" y2="8.18515" layer="21"/>
<rectangle x1="8.69315" y1="8.17245" x2="9.26465" y2="8.18515" layer="21"/>
<rectangle x1="9.35355" y1="8.17245" x2="9.63295" y2="8.18515" layer="21"/>
<rectangle x1="9.72185" y1="8.17245" x2="10.29335" y2="8.18515" layer="21"/>
<rectangle x1="10.38225" y1="8.17245" x2="10.61085" y2="8.18515" layer="21"/>
<rectangle x1="10.71245" y1="8.17245" x2="11.38555" y2="8.18515" layer="21"/>
<rectangle x1="11.47445" y1="8.17245" x2="11.75385" y2="8.18515" layer="21"/>
<rectangle x1="11.84275" y1="8.17245" x2="12.10945" y2="8.18515" layer="21"/>
<rectangle x1="12.21105" y1="8.17245" x2="12.88415" y2="8.18515" layer="21"/>
<rectangle x1="12.97305" y1="8.17245" x2="13.20165" y2="8.18515" layer="21"/>
<rectangle x1="13.30325" y1="8.17245" x2="13.93825" y2="8.18515" layer="21"/>
<rectangle x1="14.02715" y1="8.17245" x2="14.37005" y2="8.18515" layer="21"/>
<rectangle x1="14.45895" y1="8.17245" x2="14.75105" y2="8.18515" layer="21"/>
<rectangle x1="14.85265" y1="8.17245" x2="15.47495" y2="8.18515" layer="21"/>
<rectangle x1="15.57655" y1="8.17245" x2="15.88135" y2="8.18515" layer="21"/>
<rectangle x1="16.21155" y1="8.17245" x2="17.32915" y2="8.18515" layer="21"/>
<rectangle x1="4.76885" y1="8.18515" x2="6.02615" y2="8.19785" layer="21"/>
<rectangle x1="6.12775" y1="8.18515" x2="6.50875" y2="8.19785" layer="21"/>
<rectangle x1="6.61035" y1="8.18515" x2="7.23265" y2="8.19785" layer="21"/>
<rectangle x1="7.33425" y1="8.18515" x2="7.53745" y2="8.19785" layer="21"/>
<rectangle x1="7.63905" y1="8.18515" x2="8.60425" y2="8.19785" layer="21"/>
<rectangle x1="8.69315" y1="8.18515" x2="9.26465" y2="8.19785" layer="21"/>
<rectangle x1="9.35355" y1="8.18515" x2="9.63295" y2="8.19785" layer="21"/>
<rectangle x1="9.72185" y1="8.18515" x2="10.29335" y2="8.19785" layer="21"/>
<rectangle x1="10.38225" y1="8.18515" x2="10.61085" y2="8.19785" layer="21"/>
<rectangle x1="10.71245" y1="8.18515" x2="11.38555" y2="8.19785" layer="21"/>
<rectangle x1="11.47445" y1="8.18515" x2="11.75385" y2="8.19785" layer="21"/>
<rectangle x1="11.84275" y1="8.18515" x2="12.10945" y2="8.19785" layer="21"/>
<rectangle x1="12.21105" y1="8.18515" x2="12.88415" y2="8.19785" layer="21"/>
<rectangle x1="12.97305" y1="8.18515" x2="13.20165" y2="8.19785" layer="21"/>
<rectangle x1="13.30325" y1="8.18515" x2="13.93825" y2="8.19785" layer="21"/>
<rectangle x1="14.02715" y1="8.18515" x2="14.37005" y2="8.19785" layer="21"/>
<rectangle x1="14.45895" y1="8.18515" x2="14.75105" y2="8.19785" layer="21"/>
<rectangle x1="14.85265" y1="8.18515" x2="15.47495" y2="8.19785" layer="21"/>
<rectangle x1="15.57655" y1="8.18515" x2="15.86865" y2="8.19785" layer="21"/>
<rectangle x1="16.13535" y1="8.18515" x2="17.32915" y2="8.19785" layer="21"/>
<rectangle x1="4.76885" y1="8.19785" x2="6.02615" y2="8.21055" layer="21"/>
<rectangle x1="6.12775" y1="8.19785" x2="6.50875" y2="8.21055" layer="21"/>
<rectangle x1="6.61035" y1="8.19785" x2="7.23265" y2="8.21055" layer="21"/>
<rectangle x1="7.32155" y1="8.19785" x2="7.53745" y2="8.21055" layer="21"/>
<rectangle x1="7.63905" y1="8.19785" x2="8.60425" y2="8.21055" layer="21"/>
<rectangle x1="8.69315" y1="8.19785" x2="9.26465" y2="8.21055" layer="21"/>
<rectangle x1="9.35355" y1="8.19785" x2="9.63295" y2="8.21055" layer="21"/>
<rectangle x1="9.72185" y1="8.19785" x2="10.29335" y2="8.21055" layer="21"/>
<rectangle x1="10.38225" y1="8.19785" x2="10.61085" y2="8.21055" layer="21"/>
<rectangle x1="10.71245" y1="8.19785" x2="11.38555" y2="8.21055" layer="21"/>
<rectangle x1="11.47445" y1="8.19785" x2="11.75385" y2="8.21055" layer="21"/>
<rectangle x1="11.84275" y1="8.19785" x2="12.10945" y2="8.21055" layer="21"/>
<rectangle x1="12.21105" y1="8.19785" x2="12.88415" y2="8.21055" layer="21"/>
<rectangle x1="12.97305" y1="8.19785" x2="13.20165" y2="8.21055" layer="21"/>
<rectangle x1="13.30325" y1="8.19785" x2="13.93825" y2="8.21055" layer="21"/>
<rectangle x1="14.02715" y1="8.19785" x2="14.37005" y2="8.21055" layer="21"/>
<rectangle x1="14.45895" y1="8.19785" x2="14.75105" y2="8.21055" layer="21"/>
<rectangle x1="14.85265" y1="8.19785" x2="15.47495" y2="8.21055" layer="21"/>
<rectangle x1="15.56385" y1="8.19785" x2="15.85595" y2="8.21055" layer="21"/>
<rectangle x1="16.05915" y1="8.19785" x2="17.32915" y2="8.21055" layer="21"/>
<rectangle x1="4.76885" y1="8.21055" x2="6.02615" y2="8.22325" layer="21"/>
<rectangle x1="6.12775" y1="8.21055" x2="6.50875" y2="8.22325" layer="21"/>
<rectangle x1="6.61035" y1="8.21055" x2="7.23265" y2="8.22325" layer="21"/>
<rectangle x1="7.32155" y1="8.21055" x2="7.55015" y2="8.22325" layer="21"/>
<rectangle x1="7.63905" y1="8.21055" x2="8.60425" y2="8.22325" layer="21"/>
<rectangle x1="8.69315" y1="8.21055" x2="9.26465" y2="8.22325" layer="21"/>
<rectangle x1="9.35355" y1="8.21055" x2="9.63295" y2="8.22325" layer="21"/>
<rectangle x1="9.72185" y1="8.21055" x2="10.29335" y2="8.22325" layer="21"/>
<rectangle x1="10.38225" y1="8.21055" x2="10.62355" y2="8.22325" layer="21"/>
<rectangle x1="10.71245" y1="8.21055" x2="11.37285" y2="8.22325" layer="21"/>
<rectangle x1="11.47445" y1="8.21055" x2="11.75385" y2="8.22325" layer="21"/>
<rectangle x1="11.84275" y1="8.21055" x2="12.12215" y2="8.22325" layer="21"/>
<rectangle x1="12.21105" y1="8.21055" x2="12.87145" y2="8.22325" layer="21"/>
<rectangle x1="12.97305" y1="8.21055" x2="13.20165" y2="8.22325" layer="21"/>
<rectangle x1="13.30325" y1="8.21055" x2="13.93825" y2="8.22325" layer="21"/>
<rectangle x1="14.02715" y1="8.21055" x2="14.37005" y2="8.22325" layer="21"/>
<rectangle x1="14.45895" y1="8.21055" x2="14.75105" y2="8.22325" layer="21"/>
<rectangle x1="14.85265" y1="8.21055" x2="15.47495" y2="8.22325" layer="21"/>
<rectangle x1="15.56385" y1="8.21055" x2="15.84325" y2="8.22325" layer="21"/>
<rectangle x1="16.00835" y1="8.21055" x2="17.32915" y2="8.22325" layer="21"/>
<rectangle x1="4.76885" y1="8.22325" x2="6.02615" y2="8.23595" layer="21"/>
<rectangle x1="6.12775" y1="8.22325" x2="6.52145" y2="8.23595" layer="21"/>
<rectangle x1="6.61035" y1="8.22325" x2="7.23265" y2="8.23595" layer="21"/>
<rectangle x1="7.32155" y1="8.22325" x2="7.55015" y2="8.23595" layer="21"/>
<rectangle x1="7.65175" y1="8.22325" x2="8.60425" y2="8.23595" layer="21"/>
<rectangle x1="8.69315" y1="8.22325" x2="9.26465" y2="8.23595" layer="21"/>
<rectangle x1="9.35355" y1="8.22325" x2="9.63295" y2="8.23595" layer="21"/>
<rectangle x1="9.72185" y1="8.22325" x2="10.29335" y2="8.23595" layer="21"/>
<rectangle x1="10.38225" y1="8.22325" x2="10.62355" y2="8.23595" layer="21"/>
<rectangle x1="10.72515" y1="8.22325" x2="11.37285" y2="8.23595" layer="21"/>
<rectangle x1="11.47445" y1="8.22325" x2="11.75385" y2="8.23595" layer="21"/>
<rectangle x1="11.84275" y1="8.22325" x2="12.12215" y2="8.23595" layer="21"/>
<rectangle x1="12.22375" y1="8.22325" x2="12.87145" y2="8.23595" layer="21"/>
<rectangle x1="12.97305" y1="8.22325" x2="13.21435" y2="8.23595" layer="21"/>
<rectangle x1="13.30325" y1="8.22325" x2="13.93825" y2="8.23595" layer="21"/>
<rectangle x1="14.02715" y1="8.22325" x2="14.37005" y2="8.23595" layer="21"/>
<rectangle x1="14.45895" y1="8.22325" x2="14.76375" y2="8.23595" layer="21"/>
<rectangle x1="14.85265" y1="8.22325" x2="15.47495" y2="8.23595" layer="21"/>
<rectangle x1="15.56385" y1="8.22325" x2="15.83055" y2="8.23595" layer="21"/>
<rectangle x1="15.98295" y1="8.22325" x2="17.32915" y2="8.23595" layer="21"/>
<rectangle x1="4.76885" y1="8.23595" x2="6.02615" y2="8.24865" layer="21"/>
<rectangle x1="6.12775" y1="8.23595" x2="6.52145" y2="8.24865" layer="21"/>
<rectangle x1="6.62305" y1="8.23595" x2="7.21995" y2="8.24865" layer="21"/>
<rectangle x1="7.32155" y1="8.23595" x2="7.55015" y2="8.24865" layer="21"/>
<rectangle x1="7.65175" y1="8.23595" x2="8.60425" y2="8.24865" layer="21"/>
<rectangle x1="8.69315" y1="8.23595" x2="9.26465" y2="8.24865" layer="21"/>
<rectangle x1="9.35355" y1="8.23595" x2="9.63295" y2="8.24865" layer="21"/>
<rectangle x1="9.72185" y1="8.23595" x2="10.29335" y2="8.24865" layer="21"/>
<rectangle x1="10.38225" y1="8.23595" x2="10.62355" y2="8.24865" layer="21"/>
<rectangle x1="10.72515" y1="8.23595" x2="11.37285" y2="8.24865" layer="21"/>
<rectangle x1="11.47445" y1="8.23595" x2="11.75385" y2="8.24865" layer="21"/>
<rectangle x1="11.84275" y1="8.23595" x2="12.12215" y2="8.24865" layer="21"/>
<rectangle x1="12.22375" y1="8.23595" x2="12.87145" y2="8.24865" layer="21"/>
<rectangle x1="12.97305" y1="8.23595" x2="13.21435" y2="8.24865" layer="21"/>
<rectangle x1="13.31595" y1="8.23595" x2="13.93825" y2="8.24865" layer="21"/>
<rectangle x1="14.02715" y1="8.23595" x2="14.37005" y2="8.24865" layer="21"/>
<rectangle x1="14.45895" y1="8.23595" x2="14.76375" y2="8.24865" layer="21"/>
<rectangle x1="14.86535" y1="8.23595" x2="15.46225" y2="8.24865" layer="21"/>
<rectangle x1="15.56385" y1="8.23595" x2="15.83055" y2="8.24865" layer="21"/>
<rectangle x1="15.95755" y1="8.23595" x2="17.32915" y2="8.24865" layer="21"/>
<rectangle x1="4.76885" y1="8.24865" x2="6.02615" y2="8.26135" layer="21"/>
<rectangle x1="6.12775" y1="8.24865" x2="6.52145" y2="8.26135" layer="21"/>
<rectangle x1="6.62305" y1="8.24865" x2="7.21995" y2="8.26135" layer="21"/>
<rectangle x1="7.32155" y1="8.24865" x2="7.55015" y2="8.26135" layer="21"/>
<rectangle x1="7.65175" y1="8.24865" x2="8.60425" y2="8.26135" layer="21"/>
<rectangle x1="8.69315" y1="8.24865" x2="9.25195" y2="8.26135" layer="21"/>
<rectangle x1="9.35355" y1="8.24865" x2="9.63295" y2="8.26135" layer="21"/>
<rectangle x1="9.72185" y1="8.24865" x2="10.28065" y2="8.26135" layer="21"/>
<rectangle x1="10.38225" y1="8.24865" x2="10.62355" y2="8.26135" layer="21"/>
<rectangle x1="10.72515" y1="8.24865" x2="11.36015" y2="8.26135" layer="21"/>
<rectangle x1="11.46175" y1="8.24865" x2="11.75385" y2="8.26135" layer="21"/>
<rectangle x1="11.84275" y1="8.24865" x2="12.12215" y2="8.26135" layer="21"/>
<rectangle x1="12.22375" y1="8.24865" x2="12.85875" y2="8.26135" layer="21"/>
<rectangle x1="12.96035" y1="8.24865" x2="13.21435" y2="8.26135" layer="21"/>
<rectangle x1="13.31595" y1="8.24865" x2="13.93825" y2="8.26135" layer="21"/>
<rectangle x1="14.02715" y1="8.24865" x2="14.37005" y2="8.26135" layer="21"/>
<rectangle x1="14.45895" y1="8.24865" x2="14.76375" y2="8.26135" layer="21"/>
<rectangle x1="14.86535" y1="8.24865" x2="15.46225" y2="8.26135" layer="21"/>
<rectangle x1="15.56385" y1="8.24865" x2="15.81785" y2="8.26135" layer="21"/>
<rectangle x1="15.94485" y1="8.24865" x2="17.32915" y2="8.26135" layer="21"/>
<rectangle x1="4.76885" y1="8.26135" x2="6.02615" y2="8.27405" layer="21"/>
<rectangle x1="6.12775" y1="8.26135" x2="6.52145" y2="8.27405" layer="21"/>
<rectangle x1="6.62305" y1="8.26135" x2="7.21995" y2="8.27405" layer="21"/>
<rectangle x1="7.30885" y1="8.26135" x2="7.55015" y2="8.27405" layer="21"/>
<rectangle x1="7.65175" y1="8.26135" x2="8.60425" y2="8.27405" layer="21"/>
<rectangle x1="8.69315" y1="8.26135" x2="9.25195" y2="8.27405" layer="21"/>
<rectangle x1="9.35355" y1="8.26135" x2="9.63295" y2="8.27405" layer="21"/>
<rectangle x1="9.72185" y1="8.26135" x2="10.28065" y2="8.27405" layer="21"/>
<rectangle x1="10.38225" y1="8.26135" x2="10.62355" y2="8.27405" layer="21"/>
<rectangle x1="10.73785" y1="8.26135" x2="11.36015" y2="8.27405" layer="21"/>
<rectangle x1="11.46175" y1="8.26135" x2="11.75385" y2="8.27405" layer="21"/>
<rectangle x1="11.84275" y1="8.26135" x2="12.12215" y2="8.27405" layer="21"/>
<rectangle x1="12.23645" y1="8.26135" x2="12.85875" y2="8.27405" layer="21"/>
<rectangle x1="12.96035" y1="8.26135" x2="13.21435" y2="8.27405" layer="21"/>
<rectangle x1="13.31595" y1="8.26135" x2="13.93825" y2="8.27405" layer="21"/>
<rectangle x1="14.02715" y1="8.26135" x2="14.37005" y2="8.27405" layer="21"/>
<rectangle x1="14.45895" y1="8.26135" x2="14.76375" y2="8.27405" layer="21"/>
<rectangle x1="14.86535" y1="8.26135" x2="15.46225" y2="8.27405" layer="21"/>
<rectangle x1="15.55115" y1="8.26135" x2="15.81785" y2="8.27405" layer="21"/>
<rectangle x1="15.93215" y1="8.26135" x2="17.32915" y2="8.27405" layer="21"/>
<rectangle x1="4.76885" y1="8.27405" x2="6.02615" y2="8.28675" layer="21"/>
<rectangle x1="6.12775" y1="8.27405" x2="6.53415" y2="8.28675" layer="21"/>
<rectangle x1="6.63575" y1="8.27405" x2="7.20725" y2="8.28675" layer="21"/>
<rectangle x1="7.30885" y1="8.27405" x2="7.56285" y2="8.28675" layer="21"/>
<rectangle x1="7.66445" y1="8.27405" x2="8.24865" y2="8.28675" layer="21"/>
<rectangle x1="8.33755" y1="8.27405" x2="8.60425" y2="8.28675" layer="21"/>
<rectangle x1="8.69315" y1="8.27405" x2="9.25195" y2="8.28675" layer="21"/>
<rectangle x1="9.35355" y1="8.27405" x2="9.63295" y2="8.28675" layer="21"/>
<rectangle x1="9.72185" y1="8.27405" x2="10.28065" y2="8.28675" layer="21"/>
<rectangle x1="10.38225" y1="8.27405" x2="10.63625" y2="8.28675" layer="21"/>
<rectangle x1="10.73785" y1="8.27405" x2="11.36015" y2="8.28675" layer="21"/>
<rectangle x1="11.46175" y1="8.27405" x2="11.75385" y2="8.28675" layer="21"/>
<rectangle x1="11.84275" y1="8.27405" x2="12.13485" y2="8.28675" layer="21"/>
<rectangle x1="12.23645" y1="8.27405" x2="12.85875" y2="8.28675" layer="21"/>
<rectangle x1="12.96035" y1="8.27405" x2="13.22705" y2="8.28675" layer="21"/>
<rectangle x1="13.32865" y1="8.27405" x2="13.93825" y2="8.28675" layer="21"/>
<rectangle x1="14.02715" y1="8.27405" x2="14.37005" y2="8.28675" layer="21"/>
<rectangle x1="14.45895" y1="8.27405" x2="14.77645" y2="8.28675" layer="21"/>
<rectangle x1="14.87805" y1="8.27405" x2="15.44955" y2="8.28675" layer="21"/>
<rectangle x1="15.55115" y1="8.27405" x2="15.81785" y2="8.28675" layer="21"/>
<rectangle x1="15.91945" y1="8.27405" x2="17.32915" y2="8.28675" layer="21"/>
<rectangle x1="4.76885" y1="8.28675" x2="6.02615" y2="8.29945" layer="21"/>
<rectangle x1="6.12775" y1="8.28675" x2="6.53415" y2="8.29945" layer="21"/>
<rectangle x1="6.63575" y1="8.28675" x2="7.20725" y2="8.29945" layer="21"/>
<rectangle x1="7.30885" y1="8.28675" x2="7.56285" y2="8.29945" layer="21"/>
<rectangle x1="7.66445" y1="8.28675" x2="8.23595" y2="8.29945" layer="21"/>
<rectangle x1="8.33755" y1="8.28675" x2="8.60425" y2="8.29945" layer="21"/>
<rectangle x1="8.69315" y1="8.28675" x2="9.25195" y2="8.29945" layer="21"/>
<rectangle x1="9.34085" y1="8.28675" x2="9.63295" y2="8.29945" layer="21"/>
<rectangle x1="9.72185" y1="8.28675" x2="10.28065" y2="8.29945" layer="21"/>
<rectangle x1="10.36955" y1="8.28675" x2="10.63625" y2="8.29945" layer="21"/>
<rectangle x1="10.75055" y1="8.28675" x2="11.34745" y2="8.29945" layer="21"/>
<rectangle x1="11.44905" y1="8.28675" x2="11.75385" y2="8.29945" layer="21"/>
<rectangle x1="11.84275" y1="8.28675" x2="12.13485" y2="8.29945" layer="21"/>
<rectangle x1="12.24915" y1="8.28675" x2="12.84605" y2="8.29945" layer="21"/>
<rectangle x1="12.94765" y1="8.28675" x2="13.22705" y2="8.29945" layer="21"/>
<rectangle x1="13.32865" y1="8.28675" x2="13.93825" y2="8.29945" layer="21"/>
<rectangle x1="14.02715" y1="8.28675" x2="14.37005" y2="8.29945" layer="21"/>
<rectangle x1="14.45895" y1="8.28675" x2="14.77645" y2="8.29945" layer="21"/>
<rectangle x1="14.87805" y1="8.28675" x2="15.44955" y2="8.29945" layer="21"/>
<rectangle x1="15.55115" y1="8.28675" x2="15.80515" y2="8.29945" layer="21"/>
<rectangle x1="15.90675" y1="8.28675" x2="17.32915" y2="8.29945" layer="21"/>
<rectangle x1="4.76885" y1="8.29945" x2="6.02615" y2="8.31215" layer="21"/>
<rectangle x1="6.12775" y1="8.29945" x2="6.53415" y2="8.31215" layer="21"/>
<rectangle x1="6.63575" y1="8.29945" x2="7.20725" y2="8.31215" layer="21"/>
<rectangle x1="7.30885" y1="8.29945" x2="7.56285" y2="8.31215" layer="21"/>
<rectangle x1="7.67715" y1="8.29945" x2="8.23595" y2="8.31215" layer="21"/>
<rectangle x1="8.33755" y1="8.29945" x2="8.60425" y2="8.31215" layer="21"/>
<rectangle x1="8.69315" y1="8.29945" x2="9.25195" y2="8.31215" layer="21"/>
<rectangle x1="9.34085" y1="8.29945" x2="9.63295" y2="8.31215" layer="21"/>
<rectangle x1="9.72185" y1="8.29945" x2="10.28065" y2="8.31215" layer="21"/>
<rectangle x1="10.36955" y1="8.29945" x2="10.64895" y2="8.31215" layer="21"/>
<rectangle x1="10.75055" y1="8.29945" x2="11.34745" y2="8.31215" layer="21"/>
<rectangle x1="11.44905" y1="8.29945" x2="11.75385" y2="8.31215" layer="21"/>
<rectangle x1="11.84275" y1="8.29945" x2="12.14755" y2="8.31215" layer="21"/>
<rectangle x1="12.24915" y1="8.29945" x2="12.84605" y2="8.31215" layer="21"/>
<rectangle x1="12.94765" y1="8.29945" x2="13.22705" y2="8.31215" layer="21"/>
<rectangle x1="13.34135" y1="8.29945" x2="13.93825" y2="8.31215" layer="21"/>
<rectangle x1="14.02715" y1="8.29945" x2="14.37005" y2="8.31215" layer="21"/>
<rectangle x1="14.45895" y1="8.29945" x2="14.77645" y2="8.31215" layer="21"/>
<rectangle x1="14.87805" y1="8.29945" x2="15.44955" y2="8.31215" layer="21"/>
<rectangle x1="15.55115" y1="8.29945" x2="15.80515" y2="8.31215" layer="21"/>
<rectangle x1="15.90675" y1="8.29945" x2="17.32915" y2="8.31215" layer="21"/>
<rectangle x1="4.76885" y1="8.31215" x2="6.02615" y2="8.32485" layer="21"/>
<rectangle x1="6.12775" y1="8.31215" x2="6.54685" y2="8.32485" layer="21"/>
<rectangle x1="6.64845" y1="8.31215" x2="7.19455" y2="8.32485" layer="21"/>
<rectangle x1="7.29615" y1="8.31215" x2="7.57555" y2="8.32485" layer="21"/>
<rectangle x1="7.67715" y1="8.31215" x2="8.23595" y2="8.32485" layer="21"/>
<rectangle x1="8.33755" y1="8.31215" x2="8.60425" y2="8.32485" layer="21"/>
<rectangle x1="8.69315" y1="8.31215" x2="9.23925" y2="8.32485" layer="21"/>
<rectangle x1="9.34085" y1="8.31215" x2="9.63295" y2="8.32485" layer="21"/>
<rectangle x1="9.72185" y1="8.31215" x2="10.26795" y2="8.32485" layer="21"/>
<rectangle x1="10.36955" y1="8.31215" x2="10.64895" y2="8.32485" layer="21"/>
<rectangle x1="10.76325" y1="8.31215" x2="11.33475" y2="8.32485" layer="21"/>
<rectangle x1="11.43635" y1="8.31215" x2="11.75385" y2="8.32485" layer="21"/>
<rectangle x1="11.84275" y1="8.31215" x2="12.14755" y2="8.32485" layer="21"/>
<rectangle x1="12.26185" y1="8.31215" x2="12.83335" y2="8.32485" layer="21"/>
<rectangle x1="12.93495" y1="8.31215" x2="13.23975" y2="8.32485" layer="21"/>
<rectangle x1="13.34135" y1="8.31215" x2="13.93825" y2="8.32485" layer="21"/>
<rectangle x1="14.02715" y1="8.31215" x2="14.37005" y2="8.32485" layer="21"/>
<rectangle x1="14.45895" y1="8.31215" x2="14.78915" y2="8.32485" layer="21"/>
<rectangle x1="14.89075" y1="8.31215" x2="15.43685" y2="8.32485" layer="21"/>
<rectangle x1="15.53845" y1="8.31215" x2="15.80515" y2="8.32485" layer="21"/>
<rectangle x1="15.90675" y1="8.31215" x2="16.38935" y2="8.32485" layer="21"/>
<rectangle x1="16.47825" y1="8.31215" x2="17.32915" y2="8.32485" layer="21"/>
<rectangle x1="4.76885" y1="8.32485" x2="6.02615" y2="8.33755" layer="21"/>
<rectangle x1="6.12775" y1="8.32485" x2="6.54685" y2="8.33755" layer="21"/>
<rectangle x1="6.66115" y1="8.32485" x2="7.19455" y2="8.33755" layer="21"/>
<rectangle x1="7.29615" y1="8.32485" x2="7.57555" y2="8.33755" layer="21"/>
<rectangle x1="7.68985" y1="8.32485" x2="8.22325" y2="8.33755" layer="21"/>
<rectangle x1="8.32485" y1="8.32485" x2="8.60425" y2="8.33755" layer="21"/>
<rectangle x1="8.69315" y1="8.32485" x2="9.23925" y2="8.33755" layer="21"/>
<rectangle x1="9.34085" y1="8.32485" x2="9.63295" y2="8.33755" layer="21"/>
<rectangle x1="9.72185" y1="8.32485" x2="10.26795" y2="8.33755" layer="21"/>
<rectangle x1="10.36955" y1="8.32485" x2="10.66165" y2="8.33755" layer="21"/>
<rectangle x1="10.76325" y1="8.32485" x2="11.32205" y2="8.33755" layer="21"/>
<rectangle x1="11.43635" y1="8.32485" x2="11.75385" y2="8.33755" layer="21"/>
<rectangle x1="11.84275" y1="8.32485" x2="12.16025" y2="8.33755" layer="21"/>
<rectangle x1="12.26185" y1="8.32485" x2="12.82065" y2="8.33755" layer="21"/>
<rectangle x1="12.93495" y1="8.32485" x2="13.23975" y2="8.33755" layer="21"/>
<rectangle x1="13.35405" y1="8.32485" x2="13.93825" y2="8.33755" layer="21"/>
<rectangle x1="14.02715" y1="8.32485" x2="14.37005" y2="8.33755" layer="21"/>
<rectangle x1="14.45895" y1="8.32485" x2="14.78915" y2="8.33755" layer="21"/>
<rectangle x1="14.90345" y1="8.32485" x2="15.43685" y2="8.33755" layer="21"/>
<rectangle x1="15.53845" y1="8.32485" x2="15.80515" y2="8.33755" layer="21"/>
<rectangle x1="15.90675" y1="8.32485" x2="16.38935" y2="8.33755" layer="21"/>
<rectangle x1="16.47825" y1="8.32485" x2="17.32915" y2="8.33755" layer="21"/>
<rectangle x1="4.76885" y1="8.33755" x2="6.02615" y2="8.35025" layer="21"/>
<rectangle x1="6.12775" y1="8.33755" x2="6.55955" y2="8.35025" layer="21"/>
<rectangle x1="6.66115" y1="8.33755" x2="7.18185" y2="8.35025" layer="21"/>
<rectangle x1="7.28345" y1="8.33755" x2="7.58825" y2="8.35025" layer="21"/>
<rectangle x1="7.70255" y1="8.33755" x2="8.22325" y2="8.35025" layer="21"/>
<rectangle x1="8.32485" y1="8.33755" x2="8.60425" y2="8.35025" layer="21"/>
<rectangle x1="8.70585" y1="8.33755" x2="9.22655" y2="8.35025" layer="21"/>
<rectangle x1="9.34085" y1="8.33755" x2="9.63295" y2="8.35025" layer="21"/>
<rectangle x1="9.73455" y1="8.33755" x2="10.25525" y2="8.35025" layer="21"/>
<rectangle x1="10.36955" y1="8.33755" x2="10.66165" y2="8.35025" layer="21"/>
<rectangle x1="10.77595" y1="8.33755" x2="11.32205" y2="8.35025" layer="21"/>
<rectangle x1="11.42365" y1="8.33755" x2="11.75385" y2="8.35025" layer="21"/>
<rectangle x1="11.84275" y1="8.33755" x2="12.16025" y2="8.35025" layer="21"/>
<rectangle x1="12.27455" y1="8.33755" x2="12.82065" y2="8.35025" layer="21"/>
<rectangle x1="12.92225" y1="8.33755" x2="13.25245" y2="8.35025" layer="21"/>
<rectangle x1="13.35405" y1="8.33755" x2="13.92555" y2="8.35025" layer="21"/>
<rectangle x1="14.02715" y1="8.33755" x2="14.37005" y2="8.35025" layer="21"/>
<rectangle x1="14.45895" y1="8.33755" x2="14.80185" y2="8.35025" layer="21"/>
<rectangle x1="14.90345" y1="8.33755" x2="15.42415" y2="8.35025" layer="21"/>
<rectangle x1="15.52575" y1="8.33755" x2="15.80515" y2="8.35025" layer="21"/>
<rectangle x1="15.90675" y1="8.33755" x2="16.38935" y2="8.35025" layer="21"/>
<rectangle x1="16.47825" y1="8.33755" x2="17.32915" y2="8.35025" layer="21"/>
<rectangle x1="4.76885" y1="8.35025" x2="6.02615" y2="8.36295" layer="21"/>
<rectangle x1="6.12775" y1="8.35025" x2="6.55955" y2="8.36295" layer="21"/>
<rectangle x1="6.67385" y1="8.35025" x2="7.16915" y2="8.36295" layer="21"/>
<rectangle x1="7.28345" y1="8.35025" x2="7.58825" y2="8.36295" layer="21"/>
<rectangle x1="7.70255" y1="8.35025" x2="8.21055" y2="8.36295" layer="21"/>
<rectangle x1="8.31215" y1="8.35025" x2="8.60425" y2="8.36295" layer="21"/>
<rectangle x1="8.71855" y1="8.35025" x2="9.22655" y2="8.36295" layer="21"/>
<rectangle x1="9.32815" y1="8.35025" x2="9.63295" y2="8.36295" layer="21"/>
<rectangle x1="9.74725" y1="8.35025" x2="10.25525" y2="8.36295" layer="21"/>
<rectangle x1="10.35685" y1="8.35025" x2="10.67435" y2="8.36295" layer="21"/>
<rectangle x1="10.78865" y1="8.35025" x2="11.30935" y2="8.36295" layer="21"/>
<rectangle x1="11.42365" y1="8.35025" x2="11.75385" y2="8.36295" layer="21"/>
<rectangle x1="11.84275" y1="8.35025" x2="12.17295" y2="8.36295" layer="21"/>
<rectangle x1="12.28725" y1="8.35025" x2="12.80795" y2="8.36295" layer="21"/>
<rectangle x1="12.92225" y1="8.35025" x2="13.25245" y2="8.36295" layer="21"/>
<rectangle x1="13.36675" y1="8.35025" x2="13.91285" y2="8.36295" layer="21"/>
<rectangle x1="14.02715" y1="8.35025" x2="14.37005" y2="8.36295" layer="21"/>
<rectangle x1="14.45895" y1="8.35025" x2="14.80185" y2="8.36295" layer="21"/>
<rectangle x1="14.91615" y1="8.35025" x2="15.41145" y2="8.36295" layer="21"/>
<rectangle x1="15.52575" y1="8.35025" x2="15.80515" y2="8.36295" layer="21"/>
<rectangle x1="15.90675" y1="8.35025" x2="16.38935" y2="8.36295" layer="21"/>
<rectangle x1="16.47825" y1="8.35025" x2="17.32915" y2="8.36295" layer="21"/>
<rectangle x1="4.76885" y1="8.36295" x2="6.02615" y2="8.37565" layer="21"/>
<rectangle x1="6.12775" y1="8.36295" x2="6.57225" y2="8.37565" layer="21"/>
<rectangle x1="6.68655" y1="8.36295" x2="7.16915" y2="8.37565" layer="21"/>
<rectangle x1="7.27075" y1="8.36295" x2="7.60095" y2="8.37565" layer="21"/>
<rectangle x1="7.71525" y1="8.36295" x2="8.19785" y2="8.37565" layer="21"/>
<rectangle x1="8.31215" y1="8.36295" x2="8.60425" y2="8.37565" layer="21"/>
<rectangle x1="8.73125" y1="8.36295" x2="9.21385" y2="8.37565" layer="21"/>
<rectangle x1="9.32815" y1="8.36295" x2="9.63295" y2="8.37565" layer="21"/>
<rectangle x1="9.75995" y1="8.36295" x2="10.24255" y2="8.37565" layer="21"/>
<rectangle x1="10.35685" y1="8.36295" x2="10.67435" y2="8.37565" layer="21"/>
<rectangle x1="10.80135" y1="8.36295" x2="11.29665" y2="8.37565" layer="21"/>
<rectangle x1="11.41095" y1="8.36295" x2="11.75385" y2="8.37565" layer="21"/>
<rectangle x1="11.84275" y1="8.36295" x2="12.17295" y2="8.37565" layer="21"/>
<rectangle x1="12.29995" y1="8.36295" x2="12.79525" y2="8.37565" layer="21"/>
<rectangle x1="12.90955" y1="8.36295" x2="13.26515" y2="8.37565" layer="21"/>
<rectangle x1="13.37945" y1="8.36295" x2="13.90015" y2="8.37565" layer="21"/>
<rectangle x1="14.02715" y1="8.36295" x2="14.37005" y2="8.37565" layer="21"/>
<rectangle x1="14.45895" y1="8.36295" x2="14.81455" y2="8.37565" layer="21"/>
<rectangle x1="14.92885" y1="8.36295" x2="15.41145" y2="8.37565" layer="21"/>
<rectangle x1="15.51305" y1="8.36295" x2="15.80515" y2="8.37565" layer="21"/>
<rectangle x1="15.90675" y1="8.36295" x2="16.37665" y2="8.37565" layer="21"/>
<rectangle x1="16.47825" y1="8.36295" x2="17.32915" y2="8.37565" layer="21"/>
<rectangle x1="4.76885" y1="8.37565" x2="6.02615" y2="8.38835" layer="21"/>
<rectangle x1="6.12775" y1="8.37565" x2="6.58495" y2="8.38835" layer="21"/>
<rectangle x1="6.69925" y1="8.37565" x2="7.15645" y2="8.38835" layer="21"/>
<rectangle x1="7.27075" y1="8.37565" x2="7.61365" y2="8.38835" layer="21"/>
<rectangle x1="7.72795" y1="8.37565" x2="8.19785" y2="8.38835" layer="21"/>
<rectangle x1="8.29945" y1="8.37565" x2="8.60425" y2="8.38835" layer="21"/>
<rectangle x1="8.74395" y1="8.37565" x2="9.21385" y2="8.38835" layer="21"/>
<rectangle x1="9.31545" y1="8.37565" x2="9.63295" y2="8.38835" layer="21"/>
<rectangle x1="9.77265" y1="8.37565" x2="10.24255" y2="8.38835" layer="21"/>
<rectangle x1="10.34415" y1="8.37565" x2="10.68705" y2="8.38835" layer="21"/>
<rectangle x1="10.81405" y1="8.37565" x2="11.28395" y2="8.38835" layer="21"/>
<rectangle x1="11.41095" y1="8.37565" x2="11.75385" y2="8.38835" layer="21"/>
<rectangle x1="11.84275" y1="8.37565" x2="12.18565" y2="8.38835" layer="21"/>
<rectangle x1="12.31265" y1="8.37565" x2="12.78255" y2="8.38835" layer="21"/>
<rectangle x1="12.90955" y1="8.37565" x2="13.26515" y2="8.38835" layer="21"/>
<rectangle x1="13.39215" y1="8.37565" x2="13.88745" y2="8.38835" layer="21"/>
<rectangle x1="14.02715" y1="8.37565" x2="14.37005" y2="8.38835" layer="21"/>
<rectangle x1="14.45895" y1="8.37565" x2="14.82725" y2="8.38835" layer="21"/>
<rectangle x1="14.94155" y1="8.37565" x2="15.39875" y2="8.38835" layer="21"/>
<rectangle x1="15.51305" y1="8.37565" x2="15.81785" y2="8.38835" layer="21"/>
<rectangle x1="15.90675" y1="8.37565" x2="16.37665" y2="8.38835" layer="21"/>
<rectangle x1="16.46555" y1="8.37565" x2="17.32915" y2="8.38835" layer="21"/>
<rectangle x1="4.76885" y1="8.38835" x2="6.02615" y2="8.40105" layer="21"/>
<rectangle x1="6.12775" y1="8.38835" x2="6.58495" y2="8.40105" layer="21"/>
<rectangle x1="6.71195" y1="8.38835" x2="7.14375" y2="8.40105" layer="21"/>
<rectangle x1="7.25805" y1="8.38835" x2="7.61365" y2="8.40105" layer="21"/>
<rectangle x1="7.74065" y1="8.38835" x2="8.18515" y2="8.40105" layer="21"/>
<rectangle x1="8.29945" y1="8.38835" x2="8.60425" y2="8.40105" layer="21"/>
<rectangle x1="8.75665" y1="8.38835" x2="9.20115" y2="8.40105" layer="21"/>
<rectangle x1="9.31545" y1="8.38835" x2="9.63295" y2="8.40105" layer="21"/>
<rectangle x1="9.78535" y1="8.38835" x2="10.22985" y2="8.40105" layer="21"/>
<rectangle x1="10.34415" y1="8.38835" x2="10.69975" y2="8.40105" layer="21"/>
<rectangle x1="10.82675" y1="8.38835" x2="11.27125" y2="8.40105" layer="21"/>
<rectangle x1="11.39825" y1="8.38835" x2="11.75385" y2="8.40105" layer="21"/>
<rectangle x1="11.84275" y1="8.38835" x2="12.19835" y2="8.40105" layer="21"/>
<rectangle x1="12.32535" y1="8.38835" x2="12.76985" y2="8.40105" layer="21"/>
<rectangle x1="12.89685" y1="8.38835" x2="13.27785" y2="8.40105" layer="21"/>
<rectangle x1="13.40485" y1="8.38835" x2="13.87475" y2="8.40105" layer="21"/>
<rectangle x1="14.02715" y1="8.38835" x2="14.37005" y2="8.40105" layer="21"/>
<rectangle x1="14.45895" y1="8.38835" x2="14.82725" y2="8.40105" layer="21"/>
<rectangle x1="14.95425" y1="8.38835" x2="15.38605" y2="8.40105" layer="21"/>
<rectangle x1="15.50035" y1="8.38835" x2="15.81785" y2="8.40105" layer="21"/>
<rectangle x1="15.91945" y1="8.38835" x2="16.36395" y2="8.40105" layer="21"/>
<rectangle x1="16.46555" y1="8.38835" x2="17.32915" y2="8.40105" layer="21"/>
<rectangle x1="4.76885" y1="8.40105" x2="6.02615" y2="8.41375" layer="21"/>
<rectangle x1="6.12775" y1="8.40105" x2="6.59765" y2="8.41375" layer="21"/>
<rectangle x1="6.72465" y1="8.40105" x2="7.13105" y2="8.41375" layer="21"/>
<rectangle x1="7.24535" y1="8.40105" x2="7.62635" y2="8.41375" layer="21"/>
<rectangle x1="7.75335" y1="8.40105" x2="8.17245" y2="8.41375" layer="21"/>
<rectangle x1="8.28675" y1="8.40105" x2="8.60425" y2="8.41375" layer="21"/>
<rectangle x1="8.78205" y1="8.40105" x2="9.18845" y2="8.41375" layer="21"/>
<rectangle x1="9.30275" y1="8.40105" x2="9.63295" y2="8.41375" layer="21"/>
<rectangle x1="9.81075" y1="8.40105" x2="10.21715" y2="8.41375" layer="21"/>
<rectangle x1="10.33145" y1="8.40105" x2="10.69975" y2="8.41375" layer="21"/>
<rectangle x1="10.83945" y1="8.40105" x2="11.25855" y2="8.41375" layer="21"/>
<rectangle x1="11.38555" y1="8.40105" x2="11.75385" y2="8.41375" layer="21"/>
<rectangle x1="11.84275" y1="8.40105" x2="12.19835" y2="8.41375" layer="21"/>
<rectangle x1="12.33805" y1="8.40105" x2="12.75715" y2="8.41375" layer="21"/>
<rectangle x1="12.88415" y1="8.40105" x2="13.29055" y2="8.41375" layer="21"/>
<rectangle x1="13.41755" y1="8.40105" x2="13.86205" y2="8.41375" layer="21"/>
<rectangle x1="14.02715" y1="8.40105" x2="14.37005" y2="8.41375" layer="21"/>
<rectangle x1="14.45895" y1="8.40105" x2="14.83995" y2="8.41375" layer="21"/>
<rectangle x1="14.96695" y1="8.40105" x2="15.37335" y2="8.41375" layer="21"/>
<rectangle x1="15.48765" y1="8.40105" x2="15.81785" y2="8.41375" layer="21"/>
<rectangle x1="15.91945" y1="8.40105" x2="16.35125" y2="8.41375" layer="21"/>
<rectangle x1="16.46555" y1="8.40105" x2="17.32915" y2="8.41375" layer="21"/>
<rectangle x1="4.76885" y1="8.41375" x2="6.02615" y2="8.42645" layer="21"/>
<rectangle x1="6.12775" y1="8.41375" x2="6.61035" y2="8.42645" layer="21"/>
<rectangle x1="6.73735" y1="8.41375" x2="7.11835" y2="8.42645" layer="21"/>
<rectangle x1="7.24535" y1="8.41375" x2="7.63905" y2="8.42645" layer="21"/>
<rectangle x1="7.76605" y1="8.41375" x2="8.14705" y2="8.42645" layer="21"/>
<rectangle x1="8.27405" y1="8.41375" x2="8.60425" y2="8.42645" layer="21"/>
<rectangle x1="8.79475" y1="8.41375" x2="9.17575" y2="8.42645" layer="21"/>
<rectangle x1="9.30275" y1="8.41375" x2="9.63295" y2="8.42645" layer="21"/>
<rectangle x1="9.82345" y1="8.41375" x2="10.20445" y2="8.42645" layer="21"/>
<rectangle x1="10.33145" y1="8.41375" x2="10.71245" y2="8.42645" layer="21"/>
<rectangle x1="10.85215" y1="8.41375" x2="11.24585" y2="8.42645" layer="21"/>
<rectangle x1="11.37285" y1="8.41375" x2="11.75385" y2="8.42645" layer="21"/>
<rectangle x1="11.84275" y1="8.41375" x2="12.21105" y2="8.42645" layer="21"/>
<rectangle x1="12.35075" y1="8.41375" x2="12.74445" y2="8.42645" layer="21"/>
<rectangle x1="12.87145" y1="8.41375" x2="13.30325" y2="8.42645" layer="21"/>
<rectangle x1="13.43025" y1="8.41375" x2="13.83665" y2="8.42645" layer="21"/>
<rectangle x1="14.02715" y1="8.41375" x2="14.37005" y2="8.42645" layer="21"/>
<rectangle x1="14.45895" y1="8.41375" x2="14.85265" y2="8.42645" layer="21"/>
<rectangle x1="14.97965" y1="8.41375" x2="15.36065" y2="8.42645" layer="21"/>
<rectangle x1="15.48765" y1="8.41375" x2="15.83055" y2="8.42645" layer="21"/>
<rectangle x1="15.93215" y1="8.41375" x2="16.33855" y2="8.42645" layer="21"/>
<rectangle x1="16.45285" y1="8.41375" x2="17.32915" y2="8.42645" layer="21"/>
<rectangle x1="4.76885" y1="8.42645" x2="6.02615" y2="8.43915" layer="21"/>
<rectangle x1="6.12775" y1="8.42645" x2="6.62305" y2="8.43915" layer="21"/>
<rectangle x1="6.75005" y1="8.42645" x2="7.10565" y2="8.43915" layer="21"/>
<rectangle x1="7.23265" y1="8.42645" x2="7.65175" y2="8.43915" layer="21"/>
<rectangle x1="7.79145" y1="8.42645" x2="8.13435" y2="8.43915" layer="21"/>
<rectangle x1="8.27405" y1="8.42645" x2="8.60425" y2="8.43915" layer="21"/>
<rectangle x1="8.82015" y1="8.42645" x2="9.16305" y2="8.43915" layer="21"/>
<rectangle x1="9.29005" y1="8.42645" x2="9.63295" y2="8.43915" layer="21"/>
<rectangle x1="9.84885" y1="8.42645" x2="10.19175" y2="8.43915" layer="21"/>
<rectangle x1="10.31875" y1="8.42645" x2="10.72515" y2="8.43915" layer="21"/>
<rectangle x1="10.87755" y1="8.42645" x2="11.22045" y2="8.43915" layer="21"/>
<rectangle x1="11.36015" y1="8.42645" x2="11.75385" y2="8.43915" layer="21"/>
<rectangle x1="11.84275" y1="8.42645" x2="12.22375" y2="8.43915" layer="21"/>
<rectangle x1="12.37615" y1="8.42645" x2="12.71905" y2="8.43915" layer="21"/>
<rectangle x1="12.85875" y1="8.42645" x2="13.30325" y2="8.43915" layer="21"/>
<rectangle x1="13.45565" y1="8.42645" x2="13.81125" y2="8.43915" layer="21"/>
<rectangle x1="14.02715" y1="8.42645" x2="14.37005" y2="8.43915" layer="21"/>
<rectangle x1="14.45895" y1="8.42645" x2="14.86535" y2="8.43915" layer="21"/>
<rectangle x1="14.99235" y1="8.42645" x2="15.34795" y2="8.43915" layer="21"/>
<rectangle x1="15.47495" y1="8.42645" x2="15.83055" y2="8.43915" layer="21"/>
<rectangle x1="15.94485" y1="8.42645" x2="16.32585" y2="8.43915" layer="21"/>
<rectangle x1="16.44015" y1="8.42645" x2="17.32915" y2="8.43915" layer="21"/>
<rectangle x1="4.76885" y1="8.43915" x2="6.02615" y2="8.45185" layer="21"/>
<rectangle x1="6.12775" y1="8.43915" x2="6.62305" y2="8.45185" layer="21"/>
<rectangle x1="6.77545" y1="8.43915" x2="7.08025" y2="8.45185" layer="21"/>
<rectangle x1="7.21995" y1="8.43915" x2="7.66445" y2="8.45185" layer="21"/>
<rectangle x1="7.81685" y1="8.43915" x2="8.10895" y2="8.45185" layer="21"/>
<rectangle x1="8.26135" y1="8.43915" x2="8.60425" y2="8.45185" layer="21"/>
<rectangle x1="8.84555" y1="8.43915" x2="9.13765" y2="8.45185" layer="21"/>
<rectangle x1="9.27735" y1="8.43915" x2="9.63295" y2="8.45185" layer="21"/>
<rectangle x1="9.87425" y1="8.43915" x2="10.16635" y2="8.45185" layer="21"/>
<rectangle x1="10.30605" y1="8.43915" x2="10.73785" y2="8.45185" layer="21"/>
<rectangle x1="10.90295" y1="8.43915" x2="11.19505" y2="8.45185" layer="21"/>
<rectangle x1="11.34745" y1="8.43915" x2="11.75385" y2="8.45185" layer="21"/>
<rectangle x1="11.84275" y1="8.43915" x2="12.23645" y2="8.45185" layer="21"/>
<rectangle x1="12.40155" y1="8.43915" x2="12.69365" y2="8.45185" layer="21"/>
<rectangle x1="12.84605" y1="8.43915" x2="13.31595" y2="8.45185" layer="21"/>
<rectangle x1="13.46835" y1="8.43915" x2="13.79855" y2="8.45185" layer="21"/>
<rectangle x1="14.02715" y1="8.43915" x2="14.37005" y2="8.45185" layer="21"/>
<rectangle x1="14.45895" y1="8.43915" x2="14.86535" y2="8.45185" layer="21"/>
<rectangle x1="15.01775" y1="8.43915" x2="15.32255" y2="8.45185" layer="21"/>
<rectangle x1="15.46225" y1="8.43915" x2="15.84325" y2="8.45185" layer="21"/>
<rectangle x1="15.95755" y1="8.43915" x2="16.31315" y2="8.45185" layer="21"/>
<rectangle x1="16.44015" y1="8.43915" x2="17.32915" y2="8.45185" layer="21"/>
<rectangle x1="4.76885" y1="8.45185" x2="6.02615" y2="8.46455" layer="21"/>
<rectangle x1="6.12775" y1="8.45185" x2="6.64845" y2="8.46455" layer="21"/>
<rectangle x1="6.80085" y1="8.45185" x2="7.05485" y2="8.46455" layer="21"/>
<rectangle x1="7.20725" y1="8.45185" x2="7.67715" y2="8.46455" layer="21"/>
<rectangle x1="7.84225" y1="8.45185" x2="8.08355" y2="8.46455" layer="21"/>
<rectangle x1="8.24865" y1="8.45185" x2="8.60425" y2="8.46455" layer="21"/>
<rectangle x1="8.69315" y1="8.45185" x2="8.70585" y2="8.46455" layer="21"/>
<rectangle x1="8.87095" y1="8.45185" x2="9.11225" y2="8.46455" layer="21"/>
<rectangle x1="9.27735" y1="8.45185" x2="9.63295" y2="8.46455" layer="21"/>
<rectangle x1="9.72185" y1="8.45185" x2="9.73455" y2="8.46455" layer="21"/>
<rectangle x1="9.89965" y1="8.45185" x2="10.14095" y2="8.46455" layer="21"/>
<rectangle x1="10.30605" y1="8.45185" x2="10.75055" y2="8.46455" layer="21"/>
<rectangle x1="10.92835" y1="8.45185" x2="11.16965" y2="8.46455" layer="21"/>
<rectangle x1="11.33475" y1="8.45185" x2="11.75385" y2="8.46455" layer="21"/>
<rectangle x1="11.84275" y1="8.45185" x2="12.24915" y2="8.46455" layer="21"/>
<rectangle x1="12.42695" y1="8.45185" x2="12.66825" y2="8.46455" layer="21"/>
<rectangle x1="12.83335" y1="8.45185" x2="13.32865" y2="8.46455" layer="21"/>
<rectangle x1="13.50645" y1="8.45185" x2="13.76045" y2="8.46455" layer="21"/>
<rectangle x1="13.92555" y1="8.45185" x2="13.93825" y2="8.46455" layer="21"/>
<rectangle x1="14.02715" y1="8.45185" x2="14.37005" y2="8.46455" layer="21"/>
<rectangle x1="14.45895" y1="8.45185" x2="14.89075" y2="8.46455" layer="21"/>
<rectangle x1="15.04315" y1="8.45185" x2="15.29715" y2="8.46455" layer="21"/>
<rectangle x1="15.44955" y1="8.45185" x2="15.84325" y2="8.46455" layer="21"/>
<rectangle x1="15.98295" y1="8.45185" x2="16.28775" y2="8.46455" layer="21"/>
<rectangle x1="16.42745" y1="8.45185" x2="17.32915" y2="8.46455" layer="21"/>
<rectangle x1="4.76885" y1="8.46455" x2="6.02615" y2="8.47725" layer="21"/>
<rectangle x1="6.12775" y1="8.46455" x2="6.66115" y2="8.47725" layer="21"/>
<rectangle x1="6.83895" y1="8.46455" x2="7.01675" y2="8.47725" layer="21"/>
<rectangle x1="7.19455" y1="8.46455" x2="7.68985" y2="8.47725" layer="21"/>
<rectangle x1="7.89305" y1="8.46455" x2="8.04545" y2="8.47725" layer="21"/>
<rectangle x1="8.23595" y1="8.46455" x2="8.60425" y2="8.47725" layer="21"/>
<rectangle x1="8.69315" y1="8.46455" x2="8.71855" y2="8.47725" layer="21"/>
<rectangle x1="8.92175" y1="8.46455" x2="9.07415" y2="8.47725" layer="21"/>
<rectangle x1="9.26465" y1="8.46455" x2="9.63295" y2="8.47725" layer="21"/>
<rectangle x1="9.72185" y1="8.46455" x2="9.74725" y2="8.47725" layer="21"/>
<rectangle x1="9.95045" y1="8.46455" x2="10.10285" y2="8.47725" layer="21"/>
<rectangle x1="10.29335" y1="8.46455" x2="10.76325" y2="8.47725" layer="21"/>
<rectangle x1="10.96645" y1="8.46455" x2="11.11885" y2="8.47725" layer="21"/>
<rectangle x1="11.32205" y1="8.46455" x2="11.75385" y2="8.47725" layer="21"/>
<rectangle x1="11.84275" y1="8.46455" x2="12.26185" y2="8.47725" layer="21"/>
<rectangle x1="12.46505" y1="8.46455" x2="12.61745" y2="8.47725" layer="21"/>
<rectangle x1="12.82065" y1="8.46455" x2="13.35405" y2="8.47725" layer="21"/>
<rectangle x1="13.53185" y1="8.46455" x2="13.72235" y2="8.47725" layer="21"/>
<rectangle x1="13.91285" y1="8.46455" x2="13.93825" y2="8.47725" layer="21"/>
<rectangle x1="14.02715" y1="8.46455" x2="14.37005" y2="8.47725" layer="21"/>
<rectangle x1="14.45895" y1="8.46455" x2="14.90345" y2="8.47725" layer="21"/>
<rectangle x1="15.08125" y1="8.46455" x2="15.25905" y2="8.47725" layer="21"/>
<rectangle x1="15.43685" y1="8.46455" x2="15.85595" y2="8.47725" layer="21"/>
<rectangle x1="16.02105" y1="8.46455" x2="16.24965" y2="8.47725" layer="21"/>
<rectangle x1="16.41475" y1="8.46455" x2="17.32915" y2="8.47725" layer="21"/>
<rectangle x1="4.76885" y1="8.47725" x2="6.02615" y2="8.48995" layer="21"/>
<rectangle x1="6.12775" y1="8.47725" x2="6.67385" y2="8.48995" layer="21"/>
<rectangle x1="7.18185" y1="8.47725" x2="7.70255" y2="8.48995" layer="21"/>
<rectangle x1="8.22325" y1="8.47725" x2="8.60425" y2="8.48995" layer="21"/>
<rectangle x1="8.69315" y1="8.47725" x2="8.73125" y2="8.48995" layer="21"/>
<rectangle x1="9.25195" y1="8.47725" x2="9.63295" y2="8.48995" layer="21"/>
<rectangle x1="9.72185" y1="8.47725" x2="9.75995" y2="8.48995" layer="21"/>
<rectangle x1="10.28065" y1="8.47725" x2="10.78865" y2="8.48995" layer="21"/>
<rectangle x1="11.30935" y1="8.47725" x2="11.75385" y2="8.48995" layer="21"/>
<rectangle x1="11.84275" y1="8.47725" x2="12.28725" y2="8.48995" layer="21"/>
<rectangle x1="12.80795" y1="8.47725" x2="13.36675" y2="8.48995" layer="21"/>
<rectangle x1="13.88745" y1="8.47725" x2="13.93825" y2="8.48995" layer="21"/>
<rectangle x1="14.02715" y1="8.47725" x2="14.37005" y2="8.48995" layer="21"/>
<rectangle x1="14.45895" y1="8.47725" x2="14.91615" y2="8.48995" layer="21"/>
<rectangle x1="15.42415" y1="8.47725" x2="15.86865" y2="8.48995" layer="21"/>
<rectangle x1="16.08455" y1="8.47725" x2="16.18615" y2="8.48995" layer="21"/>
<rectangle x1="16.40205" y1="8.47725" x2="17.32915" y2="8.48995" layer="21"/>
<rectangle x1="4.76885" y1="8.48995" x2="6.02615" y2="8.50265" layer="21"/>
<rectangle x1="6.12775" y1="8.48995" x2="6.68655" y2="8.50265" layer="21"/>
<rectangle x1="7.15645" y1="8.48995" x2="7.72795" y2="8.50265" layer="21"/>
<rectangle x1="8.19785" y1="8.48995" x2="8.60425" y2="8.50265" layer="21"/>
<rectangle x1="8.69315" y1="8.48995" x2="8.75665" y2="8.50265" layer="21"/>
<rectangle x1="9.22655" y1="8.48995" x2="9.63295" y2="8.50265" layer="21"/>
<rectangle x1="9.72185" y1="8.48995" x2="9.78535" y2="8.50265" layer="21"/>
<rectangle x1="10.25525" y1="8.48995" x2="10.80135" y2="8.50265" layer="21"/>
<rectangle x1="11.28395" y1="8.48995" x2="11.75385" y2="8.50265" layer="21"/>
<rectangle x1="11.84275" y1="8.48995" x2="12.29995" y2="8.50265" layer="21"/>
<rectangle x1="12.78255" y1="8.48995" x2="13.37945" y2="8.50265" layer="21"/>
<rectangle x1="13.87475" y1="8.48995" x2="13.93825" y2="8.50265" layer="21"/>
<rectangle x1="14.02715" y1="8.48995" x2="14.37005" y2="8.50265" layer="21"/>
<rectangle x1="14.45895" y1="8.48995" x2="14.92885" y2="8.50265" layer="21"/>
<rectangle x1="15.39875" y1="8.48995" x2="15.88135" y2="8.50265" layer="21"/>
<rectangle x1="16.38935" y1="8.48995" x2="17.32915" y2="8.50265" layer="21"/>
<rectangle x1="4.76885" y1="8.50265" x2="6.02615" y2="8.51535" layer="21"/>
<rectangle x1="6.12775" y1="8.50265" x2="6.71195" y2="8.51535" layer="21"/>
<rectangle x1="7.14375" y1="8.50265" x2="7.74065" y2="8.51535" layer="21"/>
<rectangle x1="8.18515" y1="8.50265" x2="8.60425" y2="8.51535" layer="21"/>
<rectangle x1="8.69315" y1="8.50265" x2="8.78205" y2="8.51535" layer="21"/>
<rectangle x1="9.21385" y1="8.50265" x2="9.63295" y2="8.51535" layer="21"/>
<rectangle x1="9.72185" y1="8.50265" x2="9.81075" y2="8.51535" layer="21"/>
<rectangle x1="10.24255" y1="8.50265" x2="10.82675" y2="8.51535" layer="21"/>
<rectangle x1="11.27125" y1="8.50265" x2="11.75385" y2="8.51535" layer="21"/>
<rectangle x1="11.84275" y1="8.50265" x2="12.32535" y2="8.51535" layer="21"/>
<rectangle x1="12.76985" y1="8.50265" x2="13.40485" y2="8.51535" layer="21"/>
<rectangle x1="13.84935" y1="8.50265" x2="13.93825" y2="8.51535" layer="21"/>
<rectangle x1="14.02715" y1="8.50265" x2="14.37005" y2="8.51535" layer="21"/>
<rectangle x1="14.45895" y1="8.50265" x2="14.95425" y2="8.51535" layer="21"/>
<rectangle x1="15.38605" y1="8.50265" x2="15.90675" y2="8.51535" layer="21"/>
<rectangle x1="16.36395" y1="8.50265" x2="17.32915" y2="8.51535" layer="21"/>
<rectangle x1="4.76885" y1="8.51535" x2="6.02615" y2="8.52805" layer="21"/>
<rectangle x1="6.12775" y1="8.51535" x2="6.73735" y2="8.52805" layer="21"/>
<rectangle x1="7.11835" y1="8.51535" x2="7.76605" y2="8.52805" layer="21"/>
<rectangle x1="8.15975" y1="8.51535" x2="8.60425" y2="8.52805" layer="21"/>
<rectangle x1="8.69315" y1="8.51535" x2="8.79475" y2="8.52805" layer="21"/>
<rectangle x1="9.18845" y1="8.51535" x2="9.63295" y2="8.52805" layer="21"/>
<rectangle x1="9.72185" y1="8.51535" x2="9.82345" y2="8.52805" layer="21"/>
<rectangle x1="10.21715" y1="8.51535" x2="10.85215" y2="8.52805" layer="21"/>
<rectangle x1="11.24585" y1="8.51535" x2="11.75385" y2="8.52805" layer="21"/>
<rectangle x1="11.84275" y1="8.51535" x2="12.35075" y2="8.52805" layer="21"/>
<rectangle x1="12.74445" y1="8.51535" x2="13.43025" y2="8.52805" layer="21"/>
<rectangle x1="13.82395" y1="8.51535" x2="13.93825" y2="8.52805" layer="21"/>
<rectangle x1="14.02715" y1="8.51535" x2="14.37005" y2="8.52805" layer="21"/>
<rectangle x1="14.45895" y1="8.51535" x2="14.97965" y2="8.52805" layer="21"/>
<rectangle x1="15.36065" y1="8.51535" x2="15.91945" y2="8.52805" layer="21"/>
<rectangle x1="16.33855" y1="8.51535" x2="17.32915" y2="8.52805" layer="21"/>
<rectangle x1="4.76885" y1="8.52805" x2="6.02615" y2="8.54075" layer="21"/>
<rectangle x1="6.12775" y1="8.52805" x2="6.76275" y2="8.54075" layer="21"/>
<rectangle x1="7.09295" y1="8.52805" x2="7.79145" y2="8.54075" layer="21"/>
<rectangle x1="8.13435" y1="8.52805" x2="8.60425" y2="8.54075" layer="21"/>
<rectangle x1="8.69315" y1="8.52805" x2="8.83285" y2="8.54075" layer="21"/>
<rectangle x1="9.16305" y1="8.52805" x2="9.63295" y2="8.54075" layer="21"/>
<rectangle x1="9.72185" y1="8.52805" x2="9.86155" y2="8.54075" layer="21"/>
<rectangle x1="10.19175" y1="8.52805" x2="10.87755" y2="8.54075" layer="21"/>
<rectangle x1="11.22045" y1="8.52805" x2="11.75385" y2="8.54075" layer="21"/>
<rectangle x1="11.84275" y1="8.52805" x2="12.37615" y2="8.54075" layer="21"/>
<rectangle x1="12.71905" y1="8.52805" x2="13.45565" y2="8.54075" layer="21"/>
<rectangle x1="13.79855" y1="8.52805" x2="13.93825" y2="8.54075" layer="21"/>
<rectangle x1="14.03985" y1="8.52805" x2="14.37005" y2="8.54075" layer="21"/>
<rectangle x1="14.45895" y1="8.52805" x2="15.00505" y2="8.54075" layer="21"/>
<rectangle x1="15.33525" y1="8.52805" x2="15.94485" y2="8.54075" layer="21"/>
<rectangle x1="16.31315" y1="8.52805" x2="17.32915" y2="8.54075" layer="21"/>
<rectangle x1="4.76885" y1="8.54075" x2="6.02615" y2="8.55345" layer="21"/>
<rectangle x1="6.12775" y1="8.54075" x2="6.78815" y2="8.55345" layer="21"/>
<rectangle x1="7.05485" y1="8.54075" x2="7.82955" y2="8.55345" layer="21"/>
<rectangle x1="8.09625" y1="8.54075" x2="8.60425" y2="8.55345" layer="21"/>
<rectangle x1="8.69315" y1="8.54075" x2="8.87095" y2="8.55345" layer="21"/>
<rectangle x1="9.13765" y1="8.54075" x2="9.89965" y2="8.55345" layer="21"/>
<rectangle x1="10.16635" y1="8.54075" x2="10.91565" y2="8.55345" layer="21"/>
<rectangle x1="11.18235" y1="8.54075" x2="11.75385" y2="8.55345" layer="21"/>
<rectangle x1="11.84275" y1="8.54075" x2="12.41425" y2="8.55345" layer="21"/>
<rectangle x1="12.68095" y1="8.54075" x2="13.48105" y2="8.55345" layer="21"/>
<rectangle x1="13.76045" y1="8.54075" x2="15.03045" y2="8.55345" layer="21"/>
<rectangle x1="15.29715" y1="8.54075" x2="15.98295" y2="8.55345" layer="21"/>
<rectangle x1="16.28775" y1="8.54075" x2="17.32915" y2="8.55345" layer="21"/>
<rectangle x1="4.76885" y1="8.55345" x2="6.02615" y2="8.56615" layer="21"/>
<rectangle x1="6.12775" y1="8.55345" x2="6.85165" y2="8.56615" layer="21"/>
<rectangle x1="7.00405" y1="8.55345" x2="7.88035" y2="8.56615" layer="21"/>
<rectangle x1="8.04545" y1="8.55345" x2="8.60425" y2="8.56615" layer="21"/>
<rectangle x1="8.69315" y1="8.55345" x2="8.92175" y2="8.56615" layer="21"/>
<rectangle x1="9.08685" y1="8.55345" x2="9.95045" y2="8.56615" layer="21"/>
<rectangle x1="10.11555" y1="8.55345" x2="10.96645" y2="8.56615" layer="21"/>
<rectangle x1="11.13155" y1="8.55345" x2="11.75385" y2="8.56615" layer="21"/>
<rectangle x1="11.84275" y1="8.55345" x2="12.46505" y2="8.56615" layer="21"/>
<rectangle x1="12.63015" y1="8.55345" x2="13.53185" y2="8.56615" layer="21"/>
<rectangle x1="13.69695" y1="8.55345" x2="15.09395" y2="8.56615" layer="21"/>
<rectangle x1="15.24635" y1="8.55345" x2="16.03375" y2="8.56615" layer="21"/>
<rectangle x1="16.22425" y1="8.55345" x2="17.32915" y2="8.56615" layer="21"/>
<rectangle x1="4.76885" y1="8.56615" x2="6.02615" y2="8.57885" layer="21"/>
<rectangle x1="6.12775" y1="8.56615" x2="8.60425" y2="8.57885" layer="21"/>
<rectangle x1="8.69315" y1="8.56615" x2="11.75385" y2="8.57885" layer="21"/>
<rectangle x1="11.84275" y1="8.56615" x2="17.32915" y2="8.57885" layer="21"/>
<rectangle x1="4.76885" y1="8.57885" x2="6.02615" y2="8.59155" layer="21"/>
<rectangle x1="6.12775" y1="8.57885" x2="8.60425" y2="8.59155" layer="21"/>
<rectangle x1="8.69315" y1="8.57885" x2="11.75385" y2="8.59155" layer="21"/>
<rectangle x1="11.84275" y1="8.57885" x2="17.32915" y2="8.59155" layer="21"/>
<rectangle x1="4.76885" y1="8.59155" x2="6.02615" y2="8.60425" layer="21"/>
<rectangle x1="6.12775" y1="8.59155" x2="8.60425" y2="8.60425" layer="21"/>
<rectangle x1="8.69315" y1="8.59155" x2="11.75385" y2="8.60425" layer="21"/>
<rectangle x1="11.84275" y1="8.59155" x2="17.32915" y2="8.60425" layer="21"/>
<rectangle x1="4.76885" y1="8.60425" x2="6.02615" y2="8.61695" layer="21"/>
<rectangle x1="6.12775" y1="8.60425" x2="8.60425" y2="8.61695" layer="21"/>
<rectangle x1="8.69315" y1="8.60425" x2="11.75385" y2="8.61695" layer="21"/>
<rectangle x1="11.84275" y1="8.60425" x2="17.32915" y2="8.61695" layer="21"/>
<rectangle x1="4.76885" y1="8.61695" x2="6.02615" y2="8.62965" layer="21"/>
<rectangle x1="6.12775" y1="8.61695" x2="8.60425" y2="8.62965" layer="21"/>
<rectangle x1="8.69315" y1="8.61695" x2="11.75385" y2="8.62965" layer="21"/>
<rectangle x1="11.84275" y1="8.61695" x2="17.32915" y2="8.62965" layer="21"/>
<rectangle x1="4.76885" y1="8.62965" x2="6.02615" y2="8.64235" layer="21"/>
<rectangle x1="6.12775" y1="8.62965" x2="8.60425" y2="8.64235" layer="21"/>
<rectangle x1="8.69315" y1="8.62965" x2="11.75385" y2="8.64235" layer="21"/>
<rectangle x1="11.84275" y1="8.62965" x2="17.32915" y2="8.64235" layer="21"/>
<rectangle x1="4.76885" y1="8.64235" x2="6.02615" y2="8.65505" layer="21"/>
<rectangle x1="6.12775" y1="8.64235" x2="8.60425" y2="8.65505" layer="21"/>
<rectangle x1="8.69315" y1="8.64235" x2="11.75385" y2="8.65505" layer="21"/>
<rectangle x1="11.84275" y1="8.64235" x2="17.32915" y2="8.65505" layer="21"/>
<rectangle x1="4.76885" y1="8.65505" x2="6.02615" y2="8.66775" layer="21"/>
<rectangle x1="6.12775" y1="8.65505" x2="8.60425" y2="8.66775" layer="21"/>
<rectangle x1="8.69315" y1="8.65505" x2="11.75385" y2="8.66775" layer="21"/>
<rectangle x1="11.84275" y1="8.65505" x2="17.32915" y2="8.66775" layer="21"/>
<rectangle x1="4.76885" y1="8.66775" x2="6.02615" y2="8.68045" layer="21"/>
<rectangle x1="6.12775" y1="8.66775" x2="8.60425" y2="8.68045" layer="21"/>
<rectangle x1="8.69315" y1="8.66775" x2="11.75385" y2="8.68045" layer="21"/>
<rectangle x1="11.84275" y1="8.66775" x2="17.32915" y2="8.68045" layer="21"/>
<rectangle x1="4.76885" y1="8.68045" x2="6.02615" y2="8.69315" layer="21"/>
<rectangle x1="6.12775" y1="8.68045" x2="8.60425" y2="8.69315" layer="21"/>
<rectangle x1="8.69315" y1="8.68045" x2="11.75385" y2="8.69315" layer="21"/>
<rectangle x1="11.84275" y1="8.68045" x2="17.32915" y2="8.69315" layer="21"/>
<rectangle x1="4.76885" y1="8.69315" x2="6.02615" y2="8.70585" layer="21"/>
<rectangle x1="6.12775" y1="8.69315" x2="8.60425" y2="8.70585" layer="21"/>
<rectangle x1="8.69315" y1="8.69315" x2="11.75385" y2="8.70585" layer="21"/>
<rectangle x1="11.84275" y1="8.69315" x2="17.32915" y2="8.70585" layer="21"/>
<rectangle x1="4.76885" y1="8.70585" x2="6.02615" y2="8.71855" layer="21"/>
<rectangle x1="6.12775" y1="8.70585" x2="8.60425" y2="8.71855" layer="21"/>
<rectangle x1="8.69315" y1="8.70585" x2="11.75385" y2="8.71855" layer="21"/>
<rectangle x1="11.84275" y1="8.70585" x2="17.32915" y2="8.71855" layer="21"/>
<rectangle x1="4.76885" y1="8.71855" x2="6.02615" y2="8.73125" layer="21"/>
<rectangle x1="6.12775" y1="8.71855" x2="8.60425" y2="8.73125" layer="21"/>
<rectangle x1="8.69315" y1="8.71855" x2="11.75385" y2="8.73125" layer="21"/>
<rectangle x1="11.84275" y1="8.71855" x2="17.32915" y2="8.73125" layer="21"/>
<rectangle x1="4.76885" y1="8.73125" x2="6.02615" y2="8.74395" layer="21"/>
<rectangle x1="6.12775" y1="8.73125" x2="8.60425" y2="8.74395" layer="21"/>
<rectangle x1="8.69315" y1="8.73125" x2="11.75385" y2="8.74395" layer="21"/>
<rectangle x1="11.84275" y1="8.73125" x2="17.32915" y2="8.74395" layer="21"/>
<rectangle x1="4.76885" y1="8.74395" x2="6.02615" y2="8.75665" layer="21"/>
<rectangle x1="6.12775" y1="8.74395" x2="8.60425" y2="8.75665" layer="21"/>
<rectangle x1="8.69315" y1="8.74395" x2="11.75385" y2="8.75665" layer="21"/>
<rectangle x1="11.84275" y1="8.74395" x2="17.32915" y2="8.75665" layer="21"/>
<rectangle x1="4.76885" y1="8.75665" x2="6.02615" y2="8.76935" layer="21"/>
<rectangle x1="6.12775" y1="8.75665" x2="8.60425" y2="8.76935" layer="21"/>
<rectangle x1="8.69315" y1="8.75665" x2="11.75385" y2="8.76935" layer="21"/>
<rectangle x1="11.84275" y1="8.75665" x2="17.32915" y2="8.76935" layer="21"/>
<rectangle x1="4.76885" y1="8.76935" x2="6.02615" y2="8.78205" layer="21"/>
<rectangle x1="6.12775" y1="8.76935" x2="8.60425" y2="8.78205" layer="21"/>
<rectangle x1="8.69315" y1="8.76935" x2="11.75385" y2="8.78205" layer="21"/>
<rectangle x1="11.84275" y1="8.76935" x2="17.32915" y2="8.78205" layer="21"/>
<rectangle x1="4.76885" y1="8.78205" x2="6.02615" y2="8.79475" layer="21"/>
<rectangle x1="6.12775" y1="8.78205" x2="8.60425" y2="8.79475" layer="21"/>
<rectangle x1="8.69315" y1="8.78205" x2="11.75385" y2="8.79475" layer="21"/>
<rectangle x1="11.84275" y1="8.78205" x2="14.39545" y2="8.79475" layer="21"/>
<rectangle x1="14.44625" y1="8.78205" x2="17.32915" y2="8.79475" layer="21"/>
<rectangle x1="4.76885" y1="8.79475" x2="6.02615" y2="8.80745" layer="21"/>
<rectangle x1="6.12775" y1="8.79475" x2="8.60425" y2="8.80745" layer="21"/>
<rectangle x1="8.69315" y1="8.79475" x2="11.75385" y2="8.80745" layer="21"/>
<rectangle x1="11.84275" y1="8.79475" x2="14.37005" y2="8.80745" layer="21"/>
<rectangle x1="14.45895" y1="8.79475" x2="17.32915" y2="8.80745" layer="21"/>
<rectangle x1="4.76885" y1="8.80745" x2="6.02615" y2="8.82015" layer="21"/>
<rectangle x1="6.12775" y1="8.80745" x2="8.60425" y2="8.82015" layer="21"/>
<rectangle x1="8.69315" y1="8.80745" x2="11.75385" y2="8.82015" layer="21"/>
<rectangle x1="11.84275" y1="8.80745" x2="14.35735" y2="8.82015" layer="21"/>
<rectangle x1="14.47165" y1="8.80745" x2="17.32915" y2="8.82015" layer="21"/>
<rectangle x1="4.76885" y1="8.82015" x2="6.02615" y2="8.83285" layer="21"/>
<rectangle x1="6.12775" y1="8.82015" x2="8.60425" y2="8.83285" layer="21"/>
<rectangle x1="8.69315" y1="8.82015" x2="11.75385" y2="8.83285" layer="21"/>
<rectangle x1="11.84275" y1="8.82015" x2="14.35735" y2="8.83285" layer="21"/>
<rectangle x1="14.48435" y1="8.82015" x2="17.32915" y2="8.83285" layer="21"/>
<rectangle x1="4.76885" y1="8.83285" x2="6.02615" y2="8.84555" layer="21"/>
<rectangle x1="6.12775" y1="8.83285" x2="8.60425" y2="8.84555" layer="21"/>
<rectangle x1="8.69315" y1="8.83285" x2="11.75385" y2="8.84555" layer="21"/>
<rectangle x1="11.84275" y1="8.83285" x2="14.35735" y2="8.84555" layer="21"/>
<rectangle x1="14.48435" y1="8.83285" x2="17.32915" y2="8.84555" layer="21"/>
<rectangle x1="4.76885" y1="8.84555" x2="6.02615" y2="8.85825" layer="21"/>
<rectangle x1="6.12775" y1="8.84555" x2="8.60425" y2="8.85825" layer="21"/>
<rectangle x1="8.69315" y1="8.84555" x2="11.75385" y2="8.85825" layer="21"/>
<rectangle x1="11.84275" y1="8.84555" x2="14.35735" y2="8.85825" layer="21"/>
<rectangle x1="14.48435" y1="8.84555" x2="17.32915" y2="8.85825" layer="21"/>
<rectangle x1="4.76885" y1="8.85825" x2="5.56895" y2="8.87095" layer="21"/>
<rectangle x1="6.58495" y1="8.85825" x2="8.60425" y2="8.87095" layer="21"/>
<rectangle x1="8.69315" y1="8.85825" x2="11.75385" y2="8.87095" layer="21"/>
<rectangle x1="11.84275" y1="8.85825" x2="14.35735" y2="8.87095" layer="21"/>
<rectangle x1="14.48435" y1="8.85825" x2="17.32915" y2="8.87095" layer="21"/>
<rectangle x1="4.76885" y1="8.87095" x2="5.56895" y2="8.88365" layer="21"/>
<rectangle x1="6.58495" y1="8.87095" x2="8.60425" y2="8.88365" layer="21"/>
<rectangle x1="8.69315" y1="8.87095" x2="11.75385" y2="8.88365" layer="21"/>
<rectangle x1="11.84275" y1="8.87095" x2="14.35735" y2="8.88365" layer="21"/>
<rectangle x1="14.48435" y1="8.87095" x2="17.32915" y2="8.88365" layer="21"/>
<rectangle x1="4.76885" y1="8.88365" x2="5.56895" y2="8.89635" layer="21"/>
<rectangle x1="6.58495" y1="8.88365" x2="8.60425" y2="8.89635" layer="21"/>
<rectangle x1="8.69315" y1="8.88365" x2="11.75385" y2="8.89635" layer="21"/>
<rectangle x1="11.84275" y1="8.88365" x2="14.35735" y2="8.89635" layer="21"/>
<rectangle x1="14.48435" y1="8.88365" x2="17.32915" y2="8.89635" layer="21"/>
<rectangle x1="4.76885" y1="8.89635" x2="5.56895" y2="8.90905" layer="21"/>
<rectangle x1="6.58495" y1="8.89635" x2="8.60425" y2="8.90905" layer="21"/>
<rectangle x1="8.69315" y1="8.89635" x2="11.75385" y2="8.90905" layer="21"/>
<rectangle x1="11.84275" y1="8.89635" x2="14.37005" y2="8.90905" layer="21"/>
<rectangle x1="14.47165" y1="8.89635" x2="17.32915" y2="8.90905" layer="21"/>
<rectangle x1="4.76885" y1="8.90905" x2="5.56895" y2="8.92175" layer="21"/>
<rectangle x1="6.58495" y1="8.90905" x2="8.60425" y2="8.92175" layer="21"/>
<rectangle x1="8.69315" y1="8.90905" x2="11.75385" y2="8.92175" layer="21"/>
<rectangle x1="11.84275" y1="8.90905" x2="14.38275" y2="8.92175" layer="21"/>
<rectangle x1="14.44625" y1="8.90905" x2="17.32915" y2="8.92175" layer="21"/>
<rectangle x1="4.76885" y1="8.92175" x2="5.56895" y2="8.93445" layer="21"/>
<rectangle x1="6.58495" y1="8.92175" x2="8.60425" y2="8.93445" layer="21"/>
<rectangle x1="8.69315" y1="8.92175" x2="11.75385" y2="8.93445" layer="21"/>
<rectangle x1="11.84275" y1="8.92175" x2="17.32915" y2="8.93445" layer="21"/>
<rectangle x1="4.76885" y1="8.93445" x2="8.60425" y2="8.94715" layer="21"/>
<rectangle x1="8.69315" y1="8.93445" x2="11.75385" y2="8.94715" layer="21"/>
<rectangle x1="11.84275" y1="8.93445" x2="17.32915" y2="8.94715" layer="21"/>
<rectangle x1="4.76885" y1="8.94715" x2="8.60425" y2="8.95985" layer="21"/>
<rectangle x1="8.69315" y1="8.94715" x2="11.75385" y2="8.95985" layer="21"/>
<rectangle x1="11.84275" y1="8.94715" x2="17.32915" y2="8.95985" layer="21"/>
<rectangle x1="4.76885" y1="8.95985" x2="8.60425" y2="8.97255" layer="21"/>
<rectangle x1="8.69315" y1="8.95985" x2="11.75385" y2="8.97255" layer="21"/>
<rectangle x1="11.84275" y1="8.95985" x2="17.32915" y2="8.97255" layer="21"/>
<rectangle x1="4.76885" y1="8.97255" x2="8.60425" y2="8.98525" layer="21"/>
<rectangle x1="8.69315" y1="8.97255" x2="11.75385" y2="8.98525" layer="21"/>
<rectangle x1="11.84275" y1="8.97255" x2="17.32915" y2="8.98525" layer="21"/>
<rectangle x1="4.76885" y1="8.98525" x2="8.60425" y2="8.99795" layer="21"/>
<rectangle x1="8.68045" y1="8.98525" x2="11.75385" y2="8.99795" layer="21"/>
<rectangle x1="11.83005" y1="8.98525" x2="17.32915" y2="8.99795" layer="21"/>
<rectangle x1="4.76885" y1="8.99795" x2="17.32915" y2="9.01065" layer="21"/>
<rectangle x1="4.76885" y1="9.01065" x2="17.32915" y2="9.02335" layer="21"/>
<rectangle x1="4.76885" y1="9.02335" x2="17.32915" y2="9.03605" layer="21"/>
<rectangle x1="4.76885" y1="9.03605" x2="17.32915" y2="9.04875" layer="21"/>
<rectangle x1="4.76885" y1="9.04875" x2="17.32915" y2="9.06145" layer="21"/>
<rectangle x1="4.76885" y1="9.06145" x2="17.32915" y2="9.07415" layer="21"/>
<rectangle x1="4.76885" y1="9.07415" x2="17.32915" y2="9.08685" layer="21"/>
<rectangle x1="4.76885" y1="9.08685" x2="17.32915" y2="9.09955" layer="21"/>
<rectangle x1="4.76885" y1="9.09955" x2="17.32915" y2="9.11225" layer="21"/>
<rectangle x1="4.76885" y1="9.11225" x2="17.32915" y2="9.12495" layer="21"/>
<rectangle x1="4.76885" y1="9.12495" x2="17.32915" y2="9.13765" layer="21"/>
<rectangle x1="4.76885" y1="9.13765" x2="17.32915" y2="9.15035" layer="21"/>
<rectangle x1="4.76885" y1="9.15035" x2="17.32915" y2="9.16305" layer="21"/>
<rectangle x1="4.76885" y1="9.16305" x2="17.32915" y2="9.17575" layer="21"/>
<rectangle x1="4.76885" y1="9.17575" x2="17.32915" y2="9.18845" layer="21"/>
<rectangle x1="4.76885" y1="9.18845" x2="17.32915" y2="9.20115" layer="21"/>
<rectangle x1="4.76885" y1="9.20115" x2="17.32915" y2="9.21385" layer="21"/>
<rectangle x1="4.76885" y1="9.21385" x2="17.32915" y2="9.22655" layer="21"/>
<rectangle x1="4.76885" y1="9.22655" x2="17.32915" y2="9.23925" layer="21"/>
<rectangle x1="4.76885" y1="9.23925" x2="17.32915" y2="9.25195" layer="21"/>
<rectangle x1="4.76885" y1="9.25195" x2="17.32915" y2="9.26465" layer="21"/>
<rectangle x1="4.76885" y1="9.26465" x2="17.32915" y2="9.27735" layer="21"/>
<rectangle x1="4.76885" y1="9.27735" x2="17.32915" y2="9.29005" layer="21"/>
<rectangle x1="4.76885" y1="9.29005" x2="17.32915" y2="9.30275" layer="21"/>
<rectangle x1="4.76885" y1="9.30275" x2="17.32915" y2="9.31545" layer="21"/>
<rectangle x1="4.76885" y1="9.31545" x2="17.32915" y2="9.32815" layer="21"/>
<rectangle x1="4.76885" y1="9.32815" x2="17.32915" y2="9.34085" layer="21"/>
<rectangle x1="4.76885" y1="9.34085" x2="17.32915" y2="9.35355" layer="21"/>
<rectangle x1="4.76885" y1="9.35355" x2="17.32915" y2="9.36625" layer="21"/>
<rectangle x1="4.76885" y1="9.36625" x2="17.32915" y2="9.37895" layer="21"/>
<rectangle x1="4.76885" y1="9.37895" x2="17.32915" y2="9.39165" layer="21"/>
<rectangle x1="4.76885" y1="9.39165" x2="17.32915" y2="9.40435" layer="21"/>
<rectangle x1="4.76885" y1="9.40435" x2="17.32915" y2="9.41705" layer="21"/>
<rectangle x1="4.76885" y1="9.41705" x2="17.32915" y2="9.42975" layer="21"/>
<rectangle x1="4.76885" y1="9.42975" x2="17.32915" y2="9.44245" layer="21"/>
<rectangle x1="4.76885" y1="9.44245" x2="17.32915" y2="9.45515" layer="21"/>
<rectangle x1="4.76885" y1="9.45515" x2="17.32915" y2="9.46785" layer="21"/>
<rectangle x1="4.76885" y1="9.46785" x2="17.32915" y2="9.48055" layer="21"/>
<rectangle x1="4.76885" y1="9.48055" x2="17.32915" y2="9.49325" layer="21"/>
<rectangle x1="4.76885" y1="9.49325" x2="17.32915" y2="9.50595" layer="21"/>
<rectangle x1="4.76885" y1="9.50595" x2="17.32915" y2="9.51865" layer="21"/>
<rectangle x1="4.76885" y1="9.51865" x2="17.32915" y2="9.53135" layer="21"/>
<rectangle x1="4.76885" y1="9.53135" x2="17.32915" y2="9.54405" layer="21"/>
<rectangle x1="4.76885" y1="9.54405" x2="17.32915" y2="9.55675" layer="21"/>
<rectangle x1="4.76885" y1="9.55675" x2="17.32915" y2="9.56945" layer="21"/>
<rectangle x1="4.76885" y1="9.56945" x2="17.32915" y2="9.58215" layer="21"/>
<rectangle x1="4.76885" y1="9.58215" x2="17.32915" y2="9.59485" layer="21"/>
<rectangle x1="4.76885" y1="9.59485" x2="17.32915" y2="9.60755" layer="21"/>
<rectangle x1="4.76885" y1="9.60755" x2="17.32915" y2="9.62025" layer="21"/>
<rectangle x1="4.76885" y1="9.62025" x2="17.32915" y2="9.63295" layer="21"/>
<rectangle x1="4.76885" y1="9.63295" x2="17.32915" y2="9.64565" layer="21"/>
<rectangle x1="4.76885" y1="9.64565" x2="17.32915" y2="9.65835" layer="21"/>
<rectangle x1="4.76885" y1="9.65835" x2="17.32915" y2="9.67105" layer="21"/>
<rectangle x1="4.76885" y1="9.67105" x2="10.26795" y2="9.68375" layer="21"/>
<rectangle x1="10.36955" y1="9.67105" x2="11.06805" y2="9.68375" layer="21"/>
<rectangle x1="11.16965" y1="9.67105" x2="17.32915" y2="9.68375" layer="21"/>
<rectangle x1="4.76885" y1="9.68375" x2="5.59435" y2="9.69645" layer="21"/>
<rectangle x1="5.69595" y1="9.68375" x2="6.72465" y2="9.69645" layer="21"/>
<rectangle x1="6.83895" y1="9.68375" x2="7.42315" y2="9.69645" layer="21"/>
<rectangle x1="7.51205" y1="9.68375" x2="8.08355" y2="9.69645" layer="21"/>
<rectangle x1="8.21055" y1="9.68375" x2="9.07415" y2="9.69645" layer="21"/>
<rectangle x1="9.18845" y1="9.68375" x2="10.26795" y2="9.69645" layer="21"/>
<rectangle x1="10.38225" y1="9.68375" x2="11.06805" y2="9.69645" layer="21"/>
<rectangle x1="11.18235" y1="9.68375" x2="12.19835" y2="9.69645" layer="21"/>
<rectangle x1="12.29995" y1="9.68375" x2="13.32865" y2="9.69645" layer="21"/>
<rectangle x1="13.44295" y1="9.68375" x2="14.02715" y2="9.69645" layer="21"/>
<rectangle x1="14.11605" y1="9.68375" x2="14.86535" y2="9.69645" layer="21"/>
<rectangle x1="14.99235" y1="9.68375" x2="15.58925" y2="9.69645" layer="21"/>
<rectangle x1="16.45285" y1="9.68375" x2="17.32915" y2="9.69645" layer="21"/>
<rectangle x1="4.76885" y1="9.69645" x2="5.59435" y2="9.70915" layer="21"/>
<rectangle x1="5.70865" y1="9.69645" x2="6.72465" y2="9.70915" layer="21"/>
<rectangle x1="6.82625" y1="9.69645" x2="7.42315" y2="9.70915" layer="21"/>
<rectangle x1="7.51205" y1="9.69645" x2="8.09625" y2="9.70915" layer="21"/>
<rectangle x1="8.22325" y1="9.69645" x2="9.06145" y2="9.70915" layer="21"/>
<rectangle x1="9.17575" y1="9.69645" x2="10.25525" y2="9.70915" layer="21"/>
<rectangle x1="10.38225" y1="9.69645" x2="11.05535" y2="9.70915" layer="21"/>
<rectangle x1="11.18235" y1="9.69645" x2="12.19835" y2="9.70915" layer="21"/>
<rectangle x1="12.31265" y1="9.69645" x2="13.32865" y2="9.70915" layer="21"/>
<rectangle x1="13.43025" y1="9.69645" x2="14.02715" y2="9.70915" layer="21"/>
<rectangle x1="14.11605" y1="9.69645" x2="14.85265" y2="9.70915" layer="21"/>
<rectangle x1="14.97965" y1="9.69645" x2="15.58925" y2="9.70915" layer="21"/>
<rectangle x1="16.45285" y1="9.69645" x2="17.32915" y2="9.70915" layer="21"/>
<rectangle x1="4.76885" y1="9.70915" x2="5.60705" y2="9.72185" layer="21"/>
<rectangle x1="5.70865" y1="9.70915" x2="6.71195" y2="9.72185" layer="21"/>
<rectangle x1="6.82625" y1="9.70915" x2="7.42315" y2="9.72185" layer="21"/>
<rectangle x1="7.51205" y1="9.70915" x2="8.10895" y2="9.72185" layer="21"/>
<rectangle x1="8.22325" y1="9.70915" x2="9.04875" y2="9.72185" layer="21"/>
<rectangle x1="9.16305" y1="9.70915" x2="10.25525" y2="9.72185" layer="21"/>
<rectangle x1="10.39495" y1="9.70915" x2="11.05535" y2="9.72185" layer="21"/>
<rectangle x1="11.19505" y1="9.70915" x2="12.21105" y2="9.72185" layer="21"/>
<rectangle x1="12.31265" y1="9.70915" x2="13.31595" y2="9.72185" layer="21"/>
<rectangle x1="13.43025" y1="9.70915" x2="14.02715" y2="9.72185" layer="21"/>
<rectangle x1="14.11605" y1="9.70915" x2="14.83995" y2="9.72185" layer="21"/>
<rectangle x1="14.96695" y1="9.70915" x2="15.58925" y2="9.72185" layer="21"/>
<rectangle x1="16.45285" y1="9.70915" x2="17.32915" y2="9.72185" layer="21"/>
<rectangle x1="4.76885" y1="9.72185" x2="5.60705" y2="9.73455" layer="21"/>
<rectangle x1="5.72135" y1="9.72185" x2="6.71195" y2="9.73455" layer="21"/>
<rectangle x1="6.81355" y1="9.72185" x2="7.42315" y2="9.73455" layer="21"/>
<rectangle x1="7.51205" y1="9.72185" x2="8.12165" y2="9.73455" layer="21"/>
<rectangle x1="8.23595" y1="9.72185" x2="9.03605" y2="9.73455" layer="21"/>
<rectangle x1="9.16305" y1="9.72185" x2="10.24255" y2="9.73455" layer="21"/>
<rectangle x1="10.39495" y1="9.72185" x2="11.04265" y2="9.73455" layer="21"/>
<rectangle x1="11.19505" y1="9.72185" x2="12.21105" y2="9.73455" layer="21"/>
<rectangle x1="12.32535" y1="9.72185" x2="13.31595" y2="9.73455" layer="21"/>
<rectangle x1="13.41755" y1="9.72185" x2="14.02715" y2="9.73455" layer="21"/>
<rectangle x1="14.11605" y1="9.72185" x2="14.82725" y2="9.73455" layer="21"/>
<rectangle x1="14.96695" y1="9.72185" x2="15.58925" y2="9.73455" layer="21"/>
<rectangle x1="16.45285" y1="9.72185" x2="17.32915" y2="9.73455" layer="21"/>
<rectangle x1="4.76885" y1="9.73455" x2="5.61975" y2="9.74725" layer="21"/>
<rectangle x1="5.72135" y1="9.73455" x2="6.69925" y2="9.74725" layer="21"/>
<rectangle x1="6.81355" y1="9.73455" x2="7.42315" y2="9.74725" layer="21"/>
<rectangle x1="7.51205" y1="9.73455" x2="8.12165" y2="9.74725" layer="21"/>
<rectangle x1="8.24865" y1="9.73455" x2="9.02335" y2="9.74725" layer="21"/>
<rectangle x1="9.15035" y1="9.73455" x2="10.24255" y2="9.74725" layer="21"/>
<rectangle x1="10.39495" y1="9.73455" x2="11.04265" y2="9.74725" layer="21"/>
<rectangle x1="11.19505" y1="9.73455" x2="12.22375" y2="9.74725" layer="21"/>
<rectangle x1="12.32535" y1="9.73455" x2="13.30325" y2="9.74725" layer="21"/>
<rectangle x1="13.41755" y1="9.73455" x2="14.02715" y2="9.74725" layer="21"/>
<rectangle x1="14.11605" y1="9.73455" x2="14.81455" y2="9.74725" layer="21"/>
<rectangle x1="14.95425" y1="9.73455" x2="15.58925" y2="9.74725" layer="21"/>
<rectangle x1="16.45285" y1="9.73455" x2="17.32915" y2="9.74725" layer="21"/>
<rectangle x1="4.76885" y1="9.74725" x2="5.61975" y2="9.75995" layer="21"/>
<rectangle x1="5.73405" y1="9.74725" x2="6.69925" y2="9.75995" layer="21"/>
<rectangle x1="6.81355" y1="9.74725" x2="7.42315" y2="9.75995" layer="21"/>
<rectangle x1="7.51205" y1="9.74725" x2="8.13435" y2="9.75995" layer="21"/>
<rectangle x1="8.26135" y1="9.74725" x2="9.02335" y2="9.75995" layer="21"/>
<rectangle x1="9.13765" y1="9.74725" x2="10.24255" y2="9.75995" layer="21"/>
<rectangle x1="10.40765" y1="9.74725" x2="11.04265" y2="9.75995" layer="21"/>
<rectangle x1="11.20775" y1="9.74725" x2="12.22375" y2="9.75995" layer="21"/>
<rectangle x1="12.33805" y1="9.74725" x2="13.30325" y2="9.75995" layer="21"/>
<rectangle x1="13.41755" y1="9.74725" x2="14.02715" y2="9.75995" layer="21"/>
<rectangle x1="14.11605" y1="9.74725" x2="14.81455" y2="9.75995" layer="21"/>
<rectangle x1="14.94155" y1="9.74725" x2="15.58925" y2="9.75995" layer="21"/>
<rectangle x1="16.45285" y1="9.74725" x2="17.32915" y2="9.75995" layer="21"/>
<rectangle x1="4.76885" y1="9.75995" x2="5.63245" y2="9.77265" layer="21"/>
<rectangle x1="5.73405" y1="9.75995" x2="6.69925" y2="9.77265" layer="21"/>
<rectangle x1="6.80085" y1="9.75995" x2="7.42315" y2="9.77265" layer="21"/>
<rectangle x1="7.51205" y1="9.75995" x2="8.14705" y2="9.77265" layer="21"/>
<rectangle x1="8.27405" y1="9.75995" x2="9.01065" y2="9.77265" layer="21"/>
<rectangle x1="9.12495" y1="9.75995" x2="10.22985" y2="9.77265" layer="21"/>
<rectangle x1="10.40765" y1="9.75995" x2="11.02995" y2="9.77265" layer="21"/>
<rectangle x1="11.20775" y1="9.75995" x2="12.23645" y2="9.77265" layer="21"/>
<rectangle x1="12.33805" y1="9.75995" x2="13.30325" y2="9.77265" layer="21"/>
<rectangle x1="13.40485" y1="9.75995" x2="14.02715" y2="9.77265" layer="21"/>
<rectangle x1="14.11605" y1="9.75995" x2="14.80185" y2="9.77265" layer="21"/>
<rectangle x1="14.92885" y1="9.75995" x2="15.58925" y2="9.77265" layer="21"/>
<rectangle x1="16.45285" y1="9.75995" x2="17.32915" y2="9.77265" layer="21"/>
<rectangle x1="4.76885" y1="9.77265" x2="5.63245" y2="9.78535" layer="21"/>
<rectangle x1="5.74675" y1="9.77265" x2="6.68655" y2="9.78535" layer="21"/>
<rectangle x1="6.80085" y1="9.77265" x2="7.42315" y2="9.78535" layer="21"/>
<rectangle x1="7.51205" y1="9.77265" x2="8.15975" y2="9.78535" layer="21"/>
<rectangle x1="8.27405" y1="9.77265" x2="8.99795" y2="9.78535" layer="21"/>
<rectangle x1="9.11225" y1="9.77265" x2="10.22985" y2="9.78535" layer="21"/>
<rectangle x1="10.42035" y1="9.77265" x2="11.02995" y2="9.78535" layer="21"/>
<rectangle x1="11.22045" y1="9.77265" x2="12.23645" y2="9.78535" layer="21"/>
<rectangle x1="12.35075" y1="9.77265" x2="13.29055" y2="9.78535" layer="21"/>
<rectangle x1="13.40485" y1="9.77265" x2="14.02715" y2="9.78535" layer="21"/>
<rectangle x1="14.11605" y1="9.77265" x2="14.78915" y2="9.78535" layer="21"/>
<rectangle x1="14.91615" y1="9.77265" x2="15.58925" y2="9.78535" layer="21"/>
<rectangle x1="15.67815" y1="9.77265" x2="17.32915" y2="9.78535" layer="21"/>
<rectangle x1="4.76885" y1="9.78535" x2="5.64515" y2="9.79805" layer="21"/>
<rectangle x1="5.74675" y1="9.78535" x2="6.68655" y2="9.79805" layer="21"/>
<rectangle x1="6.78815" y1="9.78535" x2="7.42315" y2="9.79805" layer="21"/>
<rectangle x1="7.51205" y1="9.78535" x2="8.17245" y2="9.79805" layer="21"/>
<rectangle x1="8.28675" y1="9.78535" x2="8.98525" y2="9.79805" layer="21"/>
<rectangle x1="9.11225" y1="9.78535" x2="10.21715" y2="9.79805" layer="21"/>
<rectangle x1="10.42035" y1="9.78535" x2="11.01725" y2="9.79805" layer="21"/>
<rectangle x1="11.22045" y1="9.78535" x2="12.24915" y2="9.79805" layer="21"/>
<rectangle x1="12.35075" y1="9.78535" x2="13.29055" y2="9.79805" layer="21"/>
<rectangle x1="13.39215" y1="9.78535" x2="14.02715" y2="9.79805" layer="21"/>
<rectangle x1="14.11605" y1="9.78535" x2="14.77645" y2="9.79805" layer="21"/>
<rectangle x1="14.90345" y1="9.78535" x2="15.58925" y2="9.79805" layer="21"/>
<rectangle x1="15.67815" y1="9.78535" x2="17.32915" y2="9.79805" layer="21"/>
<rectangle x1="4.76885" y1="9.79805" x2="5.64515" y2="9.81075" layer="21"/>
<rectangle x1="5.75945" y1="9.79805" x2="6.67385" y2="9.81075" layer="21"/>
<rectangle x1="6.78815" y1="9.79805" x2="7.42315" y2="9.81075" layer="21"/>
<rectangle x1="7.51205" y1="9.79805" x2="8.17245" y2="9.81075" layer="21"/>
<rectangle x1="8.29945" y1="9.79805" x2="8.97255" y2="9.81075" layer="21"/>
<rectangle x1="9.09955" y1="9.79805" x2="10.21715" y2="9.81075" layer="21"/>
<rectangle x1="10.43305" y1="9.79805" x2="11.01725" y2="9.81075" layer="21"/>
<rectangle x1="11.23315" y1="9.79805" x2="12.24915" y2="9.81075" layer="21"/>
<rectangle x1="12.36345" y1="9.79805" x2="13.27785" y2="9.81075" layer="21"/>
<rectangle x1="13.39215" y1="9.79805" x2="14.02715" y2="9.81075" layer="21"/>
<rectangle x1="14.11605" y1="9.79805" x2="14.76375" y2="9.81075" layer="21"/>
<rectangle x1="14.89075" y1="9.79805" x2="15.58925" y2="9.81075" layer="21"/>
<rectangle x1="15.67815" y1="9.79805" x2="17.32915" y2="9.81075" layer="21"/>
<rectangle x1="4.76885" y1="9.81075" x2="5.64515" y2="9.82345" layer="21"/>
<rectangle x1="5.75945" y1="9.81075" x2="6.67385" y2="9.82345" layer="21"/>
<rectangle x1="6.77545" y1="9.81075" x2="7.42315" y2="9.82345" layer="21"/>
<rectangle x1="7.51205" y1="9.81075" x2="8.18515" y2="9.82345" layer="21"/>
<rectangle x1="8.31215" y1="9.81075" x2="8.97255" y2="9.82345" layer="21"/>
<rectangle x1="9.08685" y1="9.81075" x2="10.20445" y2="9.82345" layer="21"/>
<rectangle x1="10.31875" y1="9.81075" x2="10.33145" y2="9.82345" layer="21"/>
<rectangle x1="10.43305" y1="9.81075" x2="11.00455" y2="9.82345" layer="21"/>
<rectangle x1="11.11885" y1="9.81075" x2="11.13155" y2="9.82345" layer="21"/>
<rectangle x1="11.23315" y1="9.81075" x2="12.24915" y2="9.82345" layer="21"/>
<rectangle x1="12.36345" y1="9.81075" x2="13.27785" y2="9.82345" layer="21"/>
<rectangle x1="13.37945" y1="9.81075" x2="14.02715" y2="9.82345" layer="21"/>
<rectangle x1="14.11605" y1="9.81075" x2="14.75105" y2="9.82345" layer="21"/>
<rectangle x1="14.89075" y1="9.81075" x2="15.58925" y2="9.82345" layer="21"/>
<rectangle x1="15.67815" y1="9.81075" x2="17.32915" y2="9.82345" layer="21"/>
<rectangle x1="4.76885" y1="9.82345" x2="5.65785" y2="9.83615" layer="21"/>
<rectangle x1="5.75945" y1="9.82345" x2="6.66115" y2="9.83615" layer="21"/>
<rectangle x1="6.77545" y1="9.82345" x2="7.42315" y2="9.83615" layer="21"/>
<rectangle x1="7.51205" y1="9.82345" x2="8.19785" y2="9.83615" layer="21"/>
<rectangle x1="8.32485" y1="9.82345" x2="8.95985" y2="9.83615" layer="21"/>
<rectangle x1="9.07415" y1="9.82345" x2="10.20445" y2="9.83615" layer="21"/>
<rectangle x1="10.30605" y1="9.82345" x2="10.33145" y2="9.83615" layer="21"/>
<rectangle x1="10.44575" y1="9.82345" x2="11.00455" y2="9.83615" layer="21"/>
<rectangle x1="11.10615" y1="9.82345" x2="11.13155" y2="9.83615" layer="21"/>
<rectangle x1="11.24585" y1="9.82345" x2="12.26185" y2="9.83615" layer="21"/>
<rectangle x1="12.36345" y1="9.82345" x2="13.26515" y2="9.83615" layer="21"/>
<rectangle x1="13.37945" y1="9.82345" x2="14.02715" y2="9.83615" layer="21"/>
<rectangle x1="14.11605" y1="9.82345" x2="14.73835" y2="9.83615" layer="21"/>
<rectangle x1="14.87805" y1="9.82345" x2="15.58925" y2="9.83615" layer="21"/>
<rectangle x1="15.67815" y1="9.82345" x2="17.32915" y2="9.83615" layer="21"/>
<rectangle x1="4.76885" y1="9.83615" x2="5.65785" y2="9.84885" layer="21"/>
<rectangle x1="5.77215" y1="9.83615" x2="6.66115" y2="9.84885" layer="21"/>
<rectangle x1="6.76275" y1="9.83615" x2="7.42315" y2="9.84885" layer="21"/>
<rectangle x1="7.51205" y1="9.83615" x2="8.21055" y2="9.84885" layer="21"/>
<rectangle x1="8.32485" y1="9.83615" x2="8.94715" y2="9.84885" layer="21"/>
<rectangle x1="9.07415" y1="9.83615" x2="10.19175" y2="9.84885" layer="21"/>
<rectangle x1="10.30605" y1="9.83615" x2="10.34415" y2="9.84885" layer="21"/>
<rectangle x1="10.44575" y1="9.83615" x2="10.99185" y2="9.84885" layer="21"/>
<rectangle x1="11.10615" y1="9.83615" x2="11.14425" y2="9.84885" layer="21"/>
<rectangle x1="11.24585" y1="9.83615" x2="12.26185" y2="9.84885" layer="21"/>
<rectangle x1="12.37615" y1="9.83615" x2="13.26515" y2="9.84885" layer="21"/>
<rectangle x1="13.36675" y1="9.83615" x2="14.02715" y2="9.84885" layer="21"/>
<rectangle x1="14.11605" y1="9.83615" x2="14.73835" y2="9.84885" layer="21"/>
<rectangle x1="14.86535" y1="9.83615" x2="15.58925" y2="9.84885" layer="21"/>
<rectangle x1="15.67815" y1="9.83615" x2="17.32915" y2="9.84885" layer="21"/>
<rectangle x1="4.76885" y1="9.84885" x2="5.67055" y2="9.86155" layer="21"/>
<rectangle x1="5.77215" y1="9.84885" x2="6.64845" y2="9.86155" layer="21"/>
<rectangle x1="6.76275" y1="9.84885" x2="7.42315" y2="9.86155" layer="21"/>
<rectangle x1="7.51205" y1="9.84885" x2="8.21055" y2="9.86155" layer="21"/>
<rectangle x1="8.33755" y1="9.84885" x2="8.93445" y2="9.86155" layer="21"/>
<rectangle x1="9.06145" y1="9.84885" x2="10.19175" y2="9.86155" layer="21"/>
<rectangle x1="10.29335" y1="9.84885" x2="10.34415" y2="9.86155" layer="21"/>
<rectangle x1="10.45845" y1="9.84885" x2="10.99185" y2="9.86155" layer="21"/>
<rectangle x1="11.09345" y1="9.84885" x2="11.14425" y2="9.86155" layer="21"/>
<rectangle x1="11.25855" y1="9.84885" x2="12.27455" y2="9.86155" layer="21"/>
<rectangle x1="12.37615" y1="9.84885" x2="13.25245" y2="9.86155" layer="21"/>
<rectangle x1="13.36675" y1="9.84885" x2="14.02715" y2="9.86155" layer="21"/>
<rectangle x1="14.11605" y1="9.84885" x2="14.72565" y2="9.86155" layer="21"/>
<rectangle x1="14.85265" y1="9.84885" x2="15.58925" y2="9.86155" layer="21"/>
<rectangle x1="15.67815" y1="9.84885" x2="17.32915" y2="9.86155" layer="21"/>
<rectangle x1="4.76885" y1="9.86155" x2="5.67055" y2="9.87425" layer="21"/>
<rectangle x1="5.78485" y1="9.86155" x2="6.64845" y2="9.87425" layer="21"/>
<rectangle x1="6.75005" y1="9.86155" x2="7.42315" y2="9.87425" layer="21"/>
<rectangle x1="7.51205" y1="9.86155" x2="8.22325" y2="9.87425" layer="21"/>
<rectangle x1="8.35025" y1="9.86155" x2="8.92175" y2="9.87425" layer="21"/>
<rectangle x1="9.04875" y1="9.86155" x2="10.17905" y2="9.87425" layer="21"/>
<rectangle x1="10.29335" y1="9.86155" x2="10.35685" y2="9.87425" layer="21"/>
<rectangle x1="10.45845" y1="9.86155" x2="10.97915" y2="9.87425" layer="21"/>
<rectangle x1="11.09345" y1="9.86155" x2="11.15695" y2="9.87425" layer="21"/>
<rectangle x1="11.25855" y1="9.86155" x2="12.27455" y2="9.87425" layer="21"/>
<rectangle x1="12.38885" y1="9.86155" x2="13.25245" y2="9.87425" layer="21"/>
<rectangle x1="13.35405" y1="9.86155" x2="14.02715" y2="9.87425" layer="21"/>
<rectangle x1="14.11605" y1="9.86155" x2="14.71295" y2="9.87425" layer="21"/>
<rectangle x1="14.83995" y1="9.86155" x2="15.58925" y2="9.87425" layer="21"/>
<rectangle x1="15.67815" y1="9.86155" x2="17.32915" y2="9.87425" layer="21"/>
<rectangle x1="4.76885" y1="9.87425" x2="5.68325" y2="9.88695" layer="21"/>
<rectangle x1="5.78485" y1="9.87425" x2="6.63575" y2="9.88695" layer="21"/>
<rectangle x1="6.75005" y1="9.87425" x2="7.42315" y2="9.88695" layer="21"/>
<rectangle x1="7.51205" y1="9.87425" x2="8.23595" y2="9.88695" layer="21"/>
<rectangle x1="8.36295" y1="9.87425" x2="8.92175" y2="9.88695" layer="21"/>
<rectangle x1="9.03605" y1="9.87425" x2="10.17905" y2="9.88695" layer="21"/>
<rectangle x1="10.29335" y1="9.87425" x2="10.35685" y2="9.88695" layer="21"/>
<rectangle x1="10.47115" y1="9.87425" x2="10.97915" y2="9.88695" layer="21"/>
<rectangle x1="11.09345" y1="9.87425" x2="11.15695" y2="9.88695" layer="21"/>
<rectangle x1="11.27125" y1="9.87425" x2="12.28725" y2="9.88695" layer="21"/>
<rectangle x1="12.38885" y1="9.87425" x2="13.23975" y2="9.88695" layer="21"/>
<rectangle x1="13.35405" y1="9.87425" x2="14.02715" y2="9.88695" layer="21"/>
<rectangle x1="14.11605" y1="9.87425" x2="14.70025" y2="9.88695" layer="21"/>
<rectangle x1="14.82725" y1="9.87425" x2="15.58925" y2="9.88695" layer="21"/>
<rectangle x1="15.67815" y1="9.87425" x2="17.32915" y2="9.88695" layer="21"/>
<rectangle x1="4.76885" y1="9.88695" x2="5.68325" y2="9.89965" layer="21"/>
<rectangle x1="5.79755" y1="9.88695" x2="6.63575" y2="9.89965" layer="21"/>
<rectangle x1="6.75005" y1="9.88695" x2="7.42315" y2="9.89965" layer="21"/>
<rectangle x1="7.51205" y1="9.88695" x2="8.24865" y2="9.89965" layer="21"/>
<rectangle x1="8.37565" y1="9.88695" x2="8.90905" y2="9.89965" layer="21"/>
<rectangle x1="9.02335" y1="9.88695" x2="10.16635" y2="9.89965" layer="21"/>
<rectangle x1="10.28065" y1="9.88695" x2="10.35685" y2="9.89965" layer="21"/>
<rectangle x1="10.47115" y1="9.88695" x2="10.96645" y2="9.89965" layer="21"/>
<rectangle x1="11.08075" y1="9.88695" x2="11.15695" y2="9.89965" layer="21"/>
<rectangle x1="11.27125" y1="9.88695" x2="12.28725" y2="9.89965" layer="21"/>
<rectangle x1="12.40155" y1="9.88695" x2="13.23975" y2="9.89965" layer="21"/>
<rectangle x1="13.35405" y1="9.88695" x2="14.02715" y2="9.89965" layer="21"/>
<rectangle x1="14.11605" y1="9.88695" x2="14.68755" y2="9.89965" layer="21"/>
<rectangle x1="14.81455" y1="9.88695" x2="15.58925" y2="9.89965" layer="21"/>
<rectangle x1="15.67815" y1="9.88695" x2="17.32915" y2="9.89965" layer="21"/>
<rectangle x1="4.76885" y1="9.89965" x2="5.69595" y2="9.91235" layer="21"/>
<rectangle x1="5.79755" y1="9.89965" x2="6.63575" y2="9.91235" layer="21"/>
<rectangle x1="6.73735" y1="9.89965" x2="7.42315" y2="9.91235" layer="21"/>
<rectangle x1="7.51205" y1="9.89965" x2="8.26135" y2="9.91235" layer="21"/>
<rectangle x1="8.37565" y1="9.89965" x2="8.89635" y2="9.91235" layer="21"/>
<rectangle x1="9.02335" y1="9.89965" x2="10.16635" y2="9.91235" layer="21"/>
<rectangle x1="10.28065" y1="9.89965" x2="10.36955" y2="9.91235" layer="21"/>
<rectangle x1="10.47115" y1="9.89965" x2="10.96645" y2="9.91235" layer="21"/>
<rectangle x1="11.08075" y1="9.89965" x2="11.16965" y2="9.91235" layer="21"/>
<rectangle x1="11.27125" y1="9.89965" x2="12.29995" y2="9.91235" layer="21"/>
<rectangle x1="12.40155" y1="9.89965" x2="13.23975" y2="9.91235" layer="21"/>
<rectangle x1="13.34135" y1="9.89965" x2="14.02715" y2="9.91235" layer="21"/>
<rectangle x1="14.11605" y1="9.89965" x2="14.67485" y2="9.91235" layer="21"/>
<rectangle x1="14.81455" y1="9.89965" x2="15.58925" y2="9.91235" layer="21"/>
<rectangle x1="15.67815" y1="9.89965" x2="17.32915" y2="9.91235" layer="21"/>
<rectangle x1="4.76885" y1="9.91235" x2="5.69595" y2="9.92505" layer="21"/>
<rectangle x1="5.81025" y1="9.91235" x2="6.62305" y2="9.92505" layer="21"/>
<rectangle x1="6.73735" y1="9.91235" x2="7.42315" y2="9.92505" layer="21"/>
<rectangle x1="7.51205" y1="9.91235" x2="8.26135" y2="9.92505" layer="21"/>
<rectangle x1="8.38835" y1="9.91235" x2="8.88365" y2="9.92505" layer="21"/>
<rectangle x1="9.01065" y1="9.91235" x2="10.16635" y2="9.92505" layer="21"/>
<rectangle x1="10.26795" y1="9.91235" x2="10.36955" y2="9.92505" layer="21"/>
<rectangle x1="10.48385" y1="9.91235" x2="10.96645" y2="9.92505" layer="21"/>
<rectangle x1="11.06805" y1="9.91235" x2="11.16965" y2="9.92505" layer="21"/>
<rectangle x1="11.28395" y1="9.91235" x2="12.29995" y2="9.92505" layer="21"/>
<rectangle x1="12.41425" y1="9.91235" x2="13.22705" y2="9.92505" layer="21"/>
<rectangle x1="13.34135" y1="9.91235" x2="14.02715" y2="9.92505" layer="21"/>
<rectangle x1="14.11605" y1="9.91235" x2="14.66215" y2="9.92505" layer="21"/>
<rectangle x1="14.80185" y1="9.91235" x2="15.58925" y2="9.92505" layer="21"/>
<rectangle x1="15.67815" y1="9.91235" x2="17.32915" y2="9.92505" layer="21"/>
<rectangle x1="4.76885" y1="9.92505" x2="5.70865" y2="9.93775" layer="21"/>
<rectangle x1="5.81025" y1="9.92505" x2="6.62305" y2="9.93775" layer="21"/>
<rectangle x1="6.72465" y1="9.92505" x2="7.42315" y2="9.93775" layer="21"/>
<rectangle x1="7.51205" y1="9.92505" x2="8.27405" y2="9.93775" layer="21"/>
<rectangle x1="8.40105" y1="9.92505" x2="8.87095" y2="9.93775" layer="21"/>
<rectangle x1="8.99795" y1="9.92505" x2="10.15365" y2="9.93775" layer="21"/>
<rectangle x1="10.26795" y1="9.92505" x2="10.38225" y2="9.93775" layer="21"/>
<rectangle x1="10.48385" y1="9.92505" x2="10.95375" y2="9.93775" layer="21"/>
<rectangle x1="11.06805" y1="9.92505" x2="11.18235" y2="9.93775" layer="21"/>
<rectangle x1="11.28395" y1="9.92505" x2="12.31265" y2="9.93775" layer="21"/>
<rectangle x1="12.41425" y1="9.92505" x2="13.22705" y2="9.93775" layer="21"/>
<rectangle x1="13.32865" y1="9.92505" x2="14.02715" y2="9.93775" layer="21"/>
<rectangle x1="14.11605" y1="9.92505" x2="14.66215" y2="9.93775" layer="21"/>
<rectangle x1="14.78915" y1="9.92505" x2="15.58925" y2="9.93775" layer="21"/>
<rectangle x1="15.67815" y1="9.92505" x2="17.32915" y2="9.93775" layer="21"/>
<rectangle x1="4.76885" y1="9.93775" x2="5.70865" y2="9.95045" layer="21"/>
<rectangle x1="5.82295" y1="9.93775" x2="6.61035" y2="9.95045" layer="21"/>
<rectangle x1="6.72465" y1="9.93775" x2="7.42315" y2="9.95045" layer="21"/>
<rectangle x1="7.51205" y1="9.93775" x2="8.28675" y2="9.95045" layer="21"/>
<rectangle x1="8.41375" y1="9.93775" x2="8.87095" y2="9.95045" layer="21"/>
<rectangle x1="8.98525" y1="9.93775" x2="10.15365" y2="9.95045" layer="21"/>
<rectangle x1="10.25525" y1="9.93775" x2="10.38225" y2="9.95045" layer="21"/>
<rectangle x1="10.49655" y1="9.93775" x2="10.95375" y2="9.95045" layer="21"/>
<rectangle x1="11.05535" y1="9.93775" x2="11.18235" y2="9.95045" layer="21"/>
<rectangle x1="11.29665" y1="9.93775" x2="12.31265" y2="9.95045" layer="21"/>
<rectangle x1="12.42695" y1="9.93775" x2="13.21435" y2="9.95045" layer="21"/>
<rectangle x1="13.32865" y1="9.93775" x2="14.02715" y2="9.95045" layer="21"/>
<rectangle x1="14.11605" y1="9.93775" x2="14.64945" y2="9.95045" layer="21"/>
<rectangle x1="14.77645" y1="9.93775" x2="15.58925" y2="9.95045" layer="21"/>
<rectangle x1="15.67815" y1="9.93775" x2="17.32915" y2="9.95045" layer="21"/>
<rectangle x1="4.76885" y1="9.95045" x2="5.72135" y2="9.96315" layer="21"/>
<rectangle x1="5.82295" y1="9.95045" x2="6.61035" y2="9.96315" layer="21"/>
<rectangle x1="6.71195" y1="9.95045" x2="7.42315" y2="9.96315" layer="21"/>
<rectangle x1="7.51205" y1="9.95045" x2="8.29945" y2="9.96315" layer="21"/>
<rectangle x1="8.42645" y1="9.95045" x2="8.85825" y2="9.96315" layer="21"/>
<rectangle x1="8.98525" y1="9.95045" x2="10.14095" y2="9.96315" layer="21"/>
<rectangle x1="10.25525" y1="9.95045" x2="10.39495" y2="9.96315" layer="21"/>
<rectangle x1="10.49655" y1="9.95045" x2="10.94105" y2="9.96315" layer="21"/>
<rectangle x1="11.05535" y1="9.95045" x2="11.19505" y2="9.96315" layer="21"/>
<rectangle x1="11.29665" y1="9.95045" x2="12.32535" y2="9.96315" layer="21"/>
<rectangle x1="12.42695" y1="9.95045" x2="13.21435" y2="9.96315" layer="21"/>
<rectangle x1="13.31595" y1="9.95045" x2="14.02715" y2="9.96315" layer="21"/>
<rectangle x1="14.11605" y1="9.95045" x2="14.63675" y2="9.96315" layer="21"/>
<rectangle x1="14.76375" y1="9.95045" x2="15.58925" y2="9.96315" layer="21"/>
<rectangle x1="15.67815" y1="9.95045" x2="17.32915" y2="9.96315" layer="21"/>
<rectangle x1="4.76885" y1="9.96315" x2="5.72135" y2="9.97585" layer="21"/>
<rectangle x1="5.82295" y1="9.96315" x2="6.59765" y2="9.97585" layer="21"/>
<rectangle x1="6.71195" y1="9.96315" x2="7.42315" y2="9.97585" layer="21"/>
<rectangle x1="7.51205" y1="9.96315" x2="8.31215" y2="9.97585" layer="21"/>
<rectangle x1="8.42645" y1="9.96315" x2="8.84555" y2="9.97585" layer="21"/>
<rectangle x1="8.97255" y1="9.96315" x2="10.14095" y2="9.97585" layer="21"/>
<rectangle x1="10.24255" y1="9.96315" x2="10.39495" y2="9.97585" layer="21"/>
<rectangle x1="10.50925" y1="9.96315" x2="10.94105" y2="9.97585" layer="21"/>
<rectangle x1="11.04265" y1="9.96315" x2="11.19505" y2="9.97585" layer="21"/>
<rectangle x1="11.30935" y1="9.96315" x2="12.32535" y2="9.97585" layer="21"/>
<rectangle x1="12.42695" y1="9.96315" x2="13.20165" y2="9.97585" layer="21"/>
<rectangle x1="13.31595" y1="9.96315" x2="14.02715" y2="9.97585" layer="21"/>
<rectangle x1="14.11605" y1="9.96315" x2="14.62405" y2="9.97585" layer="21"/>
<rectangle x1="14.75105" y1="9.96315" x2="15.58925" y2="9.97585" layer="21"/>
<rectangle x1="15.67815" y1="9.96315" x2="17.32915" y2="9.97585" layer="21"/>
<rectangle x1="4.76885" y1="9.97585" x2="5.72135" y2="9.98855" layer="21"/>
<rectangle x1="5.83565" y1="9.97585" x2="6.59765" y2="9.98855" layer="21"/>
<rectangle x1="6.69925" y1="9.97585" x2="7.42315" y2="9.98855" layer="21"/>
<rectangle x1="7.51205" y1="9.97585" x2="8.31215" y2="9.98855" layer="21"/>
<rectangle x1="8.43915" y1="9.97585" x2="8.83285" y2="9.98855" layer="21"/>
<rectangle x1="8.95985" y1="9.97585" x2="10.12825" y2="9.98855" layer="21"/>
<rectangle x1="10.24255" y1="9.97585" x2="10.40765" y2="9.98855" layer="21"/>
<rectangle x1="10.50925" y1="9.97585" x2="10.92835" y2="9.98855" layer="21"/>
<rectangle x1="11.04265" y1="9.97585" x2="11.20775" y2="9.98855" layer="21"/>
<rectangle x1="11.30935" y1="9.97585" x2="12.32535" y2="9.98855" layer="21"/>
<rectangle x1="12.43965" y1="9.97585" x2="13.20165" y2="9.98855" layer="21"/>
<rectangle x1="13.30325" y1="9.97585" x2="14.02715" y2="9.98855" layer="21"/>
<rectangle x1="14.11605" y1="9.97585" x2="14.61135" y2="9.98855" layer="21"/>
<rectangle x1="14.73835" y1="9.97585" x2="15.58925" y2="9.98855" layer="21"/>
<rectangle x1="15.67815" y1="9.97585" x2="17.32915" y2="9.98855" layer="21"/>
<rectangle x1="4.76885" y1="9.98855" x2="5.73405" y2="10.00125" layer="21"/>
<rectangle x1="5.83565" y1="9.98855" x2="6.58495" y2="10.00125" layer="21"/>
<rectangle x1="6.69925" y1="9.98855" x2="7.42315" y2="10.00125" layer="21"/>
<rectangle x1="7.51205" y1="9.98855" x2="8.32485" y2="10.00125" layer="21"/>
<rectangle x1="8.45185" y1="9.98855" x2="8.82015" y2="10.00125" layer="21"/>
<rectangle x1="8.94715" y1="9.98855" x2="10.12825" y2="10.00125" layer="21"/>
<rectangle x1="10.22985" y1="9.98855" x2="10.40765" y2="10.00125" layer="21"/>
<rectangle x1="10.52195" y1="9.98855" x2="10.92835" y2="10.00125" layer="21"/>
<rectangle x1="11.02995" y1="9.98855" x2="11.20775" y2="10.00125" layer="21"/>
<rectangle x1="11.32205" y1="9.98855" x2="12.33805" y2="10.00125" layer="21"/>
<rectangle x1="12.43965" y1="9.98855" x2="13.18895" y2="10.00125" layer="21"/>
<rectangle x1="13.30325" y1="9.98855" x2="14.02715" y2="10.00125" layer="21"/>
<rectangle x1="14.11605" y1="9.98855" x2="14.59865" y2="10.00125" layer="21"/>
<rectangle x1="14.73835" y1="9.98855" x2="15.58925" y2="10.00125" layer="21"/>
<rectangle x1="15.67815" y1="9.98855" x2="17.32915" y2="10.00125" layer="21"/>
<rectangle x1="4.76885" y1="10.00125" x2="5.73405" y2="10.01395" layer="21"/>
<rectangle x1="5.84835" y1="10.00125" x2="6.58495" y2="10.01395" layer="21"/>
<rectangle x1="6.68655" y1="10.00125" x2="7.42315" y2="10.01395" layer="21"/>
<rectangle x1="7.51205" y1="10.00125" x2="8.33755" y2="10.01395" layer="21"/>
<rectangle x1="8.46455" y1="10.00125" x2="8.82015" y2="10.01395" layer="21"/>
<rectangle x1="8.93445" y1="10.00125" x2="10.11555" y2="10.01395" layer="21"/>
<rectangle x1="10.22985" y1="10.00125" x2="10.42035" y2="10.01395" layer="21"/>
<rectangle x1="10.52195" y1="10.00125" x2="10.91565" y2="10.01395" layer="21"/>
<rectangle x1="11.02995" y1="10.00125" x2="11.22045" y2="10.01395" layer="21"/>
<rectangle x1="11.32205" y1="10.00125" x2="12.33805" y2="10.01395" layer="21"/>
<rectangle x1="12.45235" y1="10.00125" x2="13.18895" y2="10.01395" layer="21"/>
<rectangle x1="13.29055" y1="10.00125" x2="14.02715" y2="10.01395" layer="21"/>
<rectangle x1="14.11605" y1="10.00125" x2="14.58595" y2="10.01395" layer="21"/>
<rectangle x1="14.72565" y1="10.00125" x2="15.58925" y2="10.01395" layer="21"/>
<rectangle x1="15.67815" y1="10.00125" x2="17.32915" y2="10.01395" layer="21"/>
<rectangle x1="4.76885" y1="10.01395" x2="5.74675" y2="10.02665" layer="21"/>
<rectangle x1="5.84835" y1="10.01395" x2="6.57225" y2="10.02665" layer="21"/>
<rectangle x1="6.68655" y1="10.01395" x2="7.42315" y2="10.02665" layer="21"/>
<rectangle x1="7.51205" y1="10.01395" x2="8.35025" y2="10.02665" layer="21"/>
<rectangle x1="8.47725" y1="10.01395" x2="8.80745" y2="10.02665" layer="21"/>
<rectangle x1="8.93445" y1="10.01395" x2="10.11555" y2="10.02665" layer="21"/>
<rectangle x1="10.21715" y1="10.01395" x2="10.42035" y2="10.02665" layer="21"/>
<rectangle x1="10.53465" y1="10.01395" x2="10.91565" y2="10.02665" layer="21"/>
<rectangle x1="11.01725" y1="10.01395" x2="11.22045" y2="10.02665" layer="21"/>
<rectangle x1="11.33475" y1="10.01395" x2="12.35075" y2="10.02665" layer="21"/>
<rectangle x1="12.45235" y1="10.01395" x2="13.17625" y2="10.02665" layer="21"/>
<rectangle x1="13.29055" y1="10.01395" x2="14.02715" y2="10.02665" layer="21"/>
<rectangle x1="14.11605" y1="10.01395" x2="14.58595" y2="10.02665" layer="21"/>
<rectangle x1="14.71295" y1="10.01395" x2="15.58925" y2="10.02665" layer="21"/>
<rectangle x1="15.67815" y1="10.01395" x2="17.32915" y2="10.02665" layer="21"/>
<rectangle x1="4.76885" y1="10.02665" x2="5.74675" y2="10.03935" layer="21"/>
<rectangle x1="5.86105" y1="10.02665" x2="6.57225" y2="10.03935" layer="21"/>
<rectangle x1="6.67385" y1="10.02665" x2="7.42315" y2="10.03935" layer="21"/>
<rectangle x1="7.51205" y1="10.02665" x2="8.35025" y2="10.03935" layer="21"/>
<rectangle x1="8.47725" y1="10.02665" x2="8.79475" y2="10.03935" layer="21"/>
<rectangle x1="8.92175" y1="10.02665" x2="10.10285" y2="10.03935" layer="21"/>
<rectangle x1="10.21715" y1="10.02665" x2="10.43305" y2="10.03935" layer="21"/>
<rectangle x1="10.53465" y1="10.02665" x2="10.90295" y2="10.03935" layer="21"/>
<rectangle x1="11.01725" y1="10.02665" x2="11.23315" y2="10.03935" layer="21"/>
<rectangle x1="11.33475" y1="10.02665" x2="12.35075" y2="10.03935" layer="21"/>
<rectangle x1="12.46505" y1="10.02665" x2="13.17625" y2="10.03935" layer="21"/>
<rectangle x1="13.27785" y1="10.02665" x2="14.02715" y2="10.03935" layer="21"/>
<rectangle x1="14.11605" y1="10.02665" x2="14.57325" y2="10.03935" layer="21"/>
<rectangle x1="14.70025" y1="10.02665" x2="15.58925" y2="10.03935" layer="21"/>
<rectangle x1="15.67815" y1="10.02665" x2="17.32915" y2="10.03935" layer="21"/>
<rectangle x1="4.76885" y1="10.03935" x2="5.75945" y2="10.05205" layer="21"/>
<rectangle x1="5.86105" y1="10.03935" x2="6.57225" y2="10.05205" layer="21"/>
<rectangle x1="6.67385" y1="10.03935" x2="7.42315" y2="10.05205" layer="21"/>
<rectangle x1="7.51205" y1="10.03935" x2="8.36295" y2="10.05205" layer="21"/>
<rectangle x1="8.48995" y1="10.03935" x2="8.78205" y2="10.05205" layer="21"/>
<rectangle x1="8.90905" y1="10.03935" x2="10.10285" y2="10.05205" layer="21"/>
<rectangle x1="10.21715" y1="10.03935" x2="10.43305" y2="10.05205" layer="21"/>
<rectangle x1="10.54735" y1="10.03935" x2="10.90295" y2="10.05205" layer="21"/>
<rectangle x1="11.01725" y1="10.03935" x2="11.23315" y2="10.05205" layer="21"/>
<rectangle x1="11.34745" y1="10.03935" x2="12.36345" y2="10.05205" layer="21"/>
<rectangle x1="12.46505" y1="10.03935" x2="13.17625" y2="10.05205" layer="21"/>
<rectangle x1="13.27785" y1="10.03935" x2="14.02715" y2="10.05205" layer="21"/>
<rectangle x1="14.11605" y1="10.03935" x2="14.56055" y2="10.05205" layer="21"/>
<rectangle x1="14.68755" y1="10.03935" x2="15.58925" y2="10.05205" layer="21"/>
<rectangle x1="15.67815" y1="10.03935" x2="17.32915" y2="10.05205" layer="21"/>
<rectangle x1="4.76885" y1="10.05205" x2="5.75945" y2="10.06475" layer="21"/>
<rectangle x1="5.87375" y1="10.05205" x2="6.55955" y2="10.06475" layer="21"/>
<rectangle x1="6.67385" y1="10.05205" x2="7.42315" y2="10.06475" layer="21"/>
<rectangle x1="7.51205" y1="10.05205" x2="8.37565" y2="10.06475" layer="21"/>
<rectangle x1="8.50265" y1="10.05205" x2="8.76935" y2="10.06475" layer="21"/>
<rectangle x1="8.89635" y1="10.05205" x2="10.09015" y2="10.06475" layer="21"/>
<rectangle x1="10.20445" y1="10.05205" x2="10.43305" y2="10.06475" layer="21"/>
<rectangle x1="10.54735" y1="10.05205" x2="10.89025" y2="10.06475" layer="21"/>
<rectangle x1="11.00455" y1="10.05205" x2="11.23315" y2="10.06475" layer="21"/>
<rectangle x1="11.34745" y1="10.05205" x2="12.36345" y2="10.06475" layer="21"/>
<rectangle x1="12.47775" y1="10.05205" x2="13.16355" y2="10.06475" layer="21"/>
<rectangle x1="13.27785" y1="10.05205" x2="14.02715" y2="10.06475" layer="21"/>
<rectangle x1="14.11605" y1="10.05205" x2="14.54785" y2="10.06475" layer="21"/>
<rectangle x1="14.67485" y1="10.05205" x2="15.58925" y2="10.06475" layer="21"/>
<rectangle x1="15.67815" y1="10.05205" x2="17.32915" y2="10.06475" layer="21"/>
<rectangle x1="4.76885" y1="10.06475" x2="5.77215" y2="10.07745" layer="21"/>
<rectangle x1="5.87375" y1="10.06475" x2="6.55955" y2="10.07745" layer="21"/>
<rectangle x1="6.66115" y1="10.06475" x2="7.42315" y2="10.07745" layer="21"/>
<rectangle x1="7.51205" y1="10.06475" x2="8.38835" y2="10.07745" layer="21"/>
<rectangle x1="8.51535" y1="10.06475" x2="8.76935" y2="10.07745" layer="21"/>
<rectangle x1="8.88365" y1="10.06475" x2="10.09015" y2="10.07745" layer="21"/>
<rectangle x1="10.20445" y1="10.06475" x2="10.44575" y2="10.07745" layer="21"/>
<rectangle x1="10.54735" y1="10.06475" x2="10.89025" y2="10.07745" layer="21"/>
<rectangle x1="11.00455" y1="10.06475" x2="11.24585" y2="10.07745" layer="21"/>
<rectangle x1="11.34745" y1="10.06475" x2="12.37615" y2="10.07745" layer="21"/>
<rectangle x1="12.47775" y1="10.06475" x2="13.16355" y2="10.07745" layer="21"/>
<rectangle x1="13.26515" y1="10.06475" x2="14.02715" y2="10.07745" layer="21"/>
<rectangle x1="14.11605" y1="10.06475" x2="14.53515" y2="10.07745" layer="21"/>
<rectangle x1="14.66215" y1="10.06475" x2="15.58925" y2="10.07745" layer="21"/>
<rectangle x1="15.67815" y1="10.06475" x2="17.32915" y2="10.07745" layer="21"/>
<rectangle x1="4.76885" y1="10.07745" x2="5.77215" y2="10.09015" layer="21"/>
<rectangle x1="6.66115" y1="10.07745" x2="7.42315" y2="10.09015" layer="21"/>
<rectangle x1="7.51205" y1="10.07745" x2="8.40105" y2="10.09015" layer="21"/>
<rectangle x1="8.52805" y1="10.07745" x2="8.75665" y2="10.09015" layer="21"/>
<rectangle x1="8.88365" y1="10.07745" x2="10.09015" y2="10.09015" layer="21"/>
<rectangle x1="10.19175" y1="10.07745" x2="10.44575" y2="10.09015" layer="21"/>
<rectangle x1="10.56005" y1="10.07745" x2="10.89025" y2="10.09015" layer="21"/>
<rectangle x1="10.99185" y1="10.07745" x2="11.24585" y2="10.09015" layer="21"/>
<rectangle x1="11.36015" y1="10.07745" x2="12.37615" y2="10.09015" layer="21"/>
<rectangle x1="13.26515" y1="10.07745" x2="14.02715" y2="10.09015" layer="21"/>
<rectangle x1="14.11605" y1="10.07745" x2="14.52245" y2="10.09015" layer="21"/>
<rectangle x1="14.66215" y1="10.07745" x2="15.58925" y2="10.09015" layer="21"/>
<rectangle x1="15.67815" y1="10.07745" x2="17.32915" y2="10.09015" layer="21"/>
<rectangle x1="4.76885" y1="10.09015" x2="5.78485" y2="10.10285" layer="21"/>
<rectangle x1="6.64845" y1="10.09015" x2="7.42315" y2="10.10285" layer="21"/>
<rectangle x1="7.51205" y1="10.09015" x2="8.40105" y2="10.10285" layer="21"/>
<rectangle x1="8.52805" y1="10.09015" x2="8.74395" y2="10.10285" layer="21"/>
<rectangle x1="8.87095" y1="10.09015" x2="10.07745" y2="10.10285" layer="21"/>
<rectangle x1="10.19175" y1="10.09015" x2="10.45845" y2="10.10285" layer="21"/>
<rectangle x1="10.56005" y1="10.09015" x2="10.87755" y2="10.10285" layer="21"/>
<rectangle x1="10.99185" y1="10.09015" x2="11.25855" y2="10.10285" layer="21"/>
<rectangle x1="11.36015" y1="10.09015" x2="12.38885" y2="10.10285" layer="21"/>
<rectangle x1="13.25245" y1="10.09015" x2="14.02715" y2="10.10285" layer="21"/>
<rectangle x1="14.11605" y1="10.09015" x2="14.50975" y2="10.10285" layer="21"/>
<rectangle x1="14.64945" y1="10.09015" x2="15.58925" y2="10.10285" layer="21"/>
<rectangle x1="15.67815" y1="10.09015" x2="17.32915" y2="10.10285" layer="21"/>
<rectangle x1="4.76885" y1="10.10285" x2="5.78485" y2="10.11555" layer="21"/>
<rectangle x1="6.64845" y1="10.10285" x2="7.42315" y2="10.11555" layer="21"/>
<rectangle x1="7.51205" y1="10.10285" x2="8.41375" y2="10.11555" layer="21"/>
<rectangle x1="8.54075" y1="10.10285" x2="8.73125" y2="10.11555" layer="21"/>
<rectangle x1="8.85825" y1="10.10285" x2="10.07745" y2="10.11555" layer="21"/>
<rectangle x1="10.17905" y1="10.10285" x2="10.45845" y2="10.11555" layer="21"/>
<rectangle x1="10.57275" y1="10.10285" x2="10.87755" y2="10.11555" layer="21"/>
<rectangle x1="10.97915" y1="10.10285" x2="11.25855" y2="10.11555" layer="21"/>
<rectangle x1="11.37285" y1="10.10285" x2="12.38885" y2="10.11555" layer="21"/>
<rectangle x1="13.25245" y1="10.10285" x2="14.02715" y2="10.11555" layer="21"/>
<rectangle x1="14.11605" y1="10.10285" x2="14.49705" y2="10.11555" layer="21"/>
<rectangle x1="14.63675" y1="10.10285" x2="15.58925" y2="10.11555" layer="21"/>
<rectangle x1="15.67815" y1="10.10285" x2="17.32915" y2="10.11555" layer="21"/>
<rectangle x1="4.76885" y1="10.11555" x2="5.79755" y2="10.12825" layer="21"/>
<rectangle x1="6.63575" y1="10.11555" x2="7.42315" y2="10.12825" layer="21"/>
<rectangle x1="7.51205" y1="10.11555" x2="8.42645" y2="10.12825" layer="21"/>
<rectangle x1="8.55345" y1="10.11555" x2="8.71855" y2="10.12825" layer="21"/>
<rectangle x1="8.84555" y1="10.11555" x2="10.06475" y2="10.12825" layer="21"/>
<rectangle x1="10.17905" y1="10.11555" x2="10.47115" y2="10.12825" layer="21"/>
<rectangle x1="10.57275" y1="10.11555" x2="10.86485" y2="10.12825" layer="21"/>
<rectangle x1="10.97915" y1="10.11555" x2="11.27125" y2="10.12825" layer="21"/>
<rectangle x1="11.37285" y1="10.11555" x2="12.40155" y2="10.12825" layer="21"/>
<rectangle x1="13.23975" y1="10.11555" x2="14.02715" y2="10.12825" layer="21"/>
<rectangle x1="14.11605" y1="10.11555" x2="14.49705" y2="10.12825" layer="21"/>
<rectangle x1="14.62405" y1="10.11555" x2="15.58925" y2="10.12825" layer="21"/>
<rectangle x1="15.67815" y1="10.11555" x2="17.32915" y2="10.12825" layer="21"/>
<rectangle x1="4.76885" y1="10.12825" x2="5.79755" y2="10.14095" layer="21"/>
<rectangle x1="6.63575" y1="10.12825" x2="7.42315" y2="10.14095" layer="21"/>
<rectangle x1="7.51205" y1="10.12825" x2="8.43915" y2="10.14095" layer="21"/>
<rectangle x1="8.56615" y1="10.12825" x2="8.71855" y2="10.14095" layer="21"/>
<rectangle x1="8.83285" y1="10.12825" x2="10.06475" y2="10.14095" layer="21"/>
<rectangle x1="10.16635" y1="10.12825" x2="10.47115" y2="10.14095" layer="21"/>
<rectangle x1="10.58545" y1="10.12825" x2="10.86485" y2="10.14095" layer="21"/>
<rectangle x1="10.96645" y1="10.12825" x2="11.27125" y2="10.14095" layer="21"/>
<rectangle x1="11.38555" y1="10.12825" x2="12.40155" y2="10.14095" layer="21"/>
<rectangle x1="13.23975" y1="10.12825" x2="14.02715" y2="10.14095" layer="21"/>
<rectangle x1="14.11605" y1="10.12825" x2="14.48435" y2="10.14095" layer="21"/>
<rectangle x1="14.61135" y1="10.12825" x2="15.58925" y2="10.14095" layer="21"/>
<rectangle x1="15.67815" y1="10.12825" x2="17.32915" y2="10.14095" layer="21"/>
<rectangle x1="4.76885" y1="10.14095" x2="5.79755" y2="10.15365" layer="21"/>
<rectangle x1="6.62305" y1="10.14095" x2="7.42315" y2="10.15365" layer="21"/>
<rectangle x1="7.51205" y1="10.14095" x2="8.45185" y2="10.15365" layer="21"/>
<rectangle x1="8.57885" y1="10.14095" x2="8.70585" y2="10.15365" layer="21"/>
<rectangle x1="8.83285" y1="10.14095" x2="10.05205" y2="10.15365" layer="21"/>
<rectangle x1="10.16635" y1="10.14095" x2="10.48385" y2="10.15365" layer="21"/>
<rectangle x1="10.58545" y1="10.14095" x2="10.85215" y2="10.15365" layer="21"/>
<rectangle x1="10.96645" y1="10.14095" x2="11.28395" y2="10.15365" layer="21"/>
<rectangle x1="11.38555" y1="10.14095" x2="12.40155" y2="10.15365" layer="21"/>
<rectangle x1="13.22705" y1="10.14095" x2="14.02715" y2="10.15365" layer="21"/>
<rectangle x1="14.11605" y1="10.14095" x2="14.47165" y2="10.15365" layer="21"/>
<rectangle x1="14.59865" y1="10.14095" x2="15.58925" y2="10.15365" layer="21"/>
<rectangle x1="15.67815" y1="10.14095" x2="17.32915" y2="10.15365" layer="21"/>
<rectangle x1="4.76885" y1="10.15365" x2="5.81025" y2="10.16635" layer="21"/>
<rectangle x1="6.62305" y1="10.15365" x2="7.42315" y2="10.16635" layer="21"/>
<rectangle x1="7.51205" y1="10.15365" x2="8.45185" y2="10.16635" layer="21"/>
<rectangle x1="8.57885" y1="10.15365" x2="8.69315" y2="10.16635" layer="21"/>
<rectangle x1="8.82015" y1="10.15365" x2="10.05205" y2="10.16635" layer="21"/>
<rectangle x1="10.15365" y1="10.15365" x2="10.48385" y2="10.16635" layer="21"/>
<rectangle x1="10.59815" y1="10.15365" x2="10.85215" y2="10.16635" layer="21"/>
<rectangle x1="10.95375" y1="10.15365" x2="11.28395" y2="10.16635" layer="21"/>
<rectangle x1="11.39825" y1="10.15365" x2="12.41425" y2="10.16635" layer="21"/>
<rectangle x1="13.22705" y1="10.15365" x2="14.02715" y2="10.16635" layer="21"/>
<rectangle x1="14.59865" y1="10.15365" x2="15.58925" y2="10.16635" layer="21"/>
<rectangle x1="15.67815" y1="10.15365" x2="17.32915" y2="10.16635" layer="21"/>
<rectangle x1="4.76885" y1="10.16635" x2="5.81025" y2="10.17905" layer="21"/>
<rectangle x1="5.92455" y1="10.16635" x2="6.50875" y2="10.17905" layer="21"/>
<rectangle x1="6.61035" y1="10.16635" x2="7.42315" y2="10.17905" layer="21"/>
<rectangle x1="7.51205" y1="10.16635" x2="8.46455" y2="10.17905" layer="21"/>
<rectangle x1="8.59155" y1="10.16635" x2="8.68045" y2="10.17905" layer="21"/>
<rectangle x1="8.80745" y1="10.16635" x2="10.03935" y2="10.17905" layer="21"/>
<rectangle x1="10.15365" y1="10.16635" x2="10.49655" y2="10.17905" layer="21"/>
<rectangle x1="10.59815" y1="10.16635" x2="10.83945" y2="10.17905" layer="21"/>
<rectangle x1="10.95375" y1="10.16635" x2="11.29665" y2="10.17905" layer="21"/>
<rectangle x1="11.39825" y1="10.16635" x2="12.41425" y2="10.17905" layer="21"/>
<rectangle x1="12.52855" y1="10.16635" x2="13.11275" y2="10.17905" layer="21"/>
<rectangle x1="13.21435" y1="10.16635" x2="14.02715" y2="10.17905" layer="21"/>
<rectangle x1="14.64945" y1="10.16635" x2="15.58925" y2="10.17905" layer="21"/>
<rectangle x1="15.67815" y1="10.16635" x2="17.32915" y2="10.17905" layer="21"/>
<rectangle x1="4.76885" y1="10.17905" x2="5.82295" y2="10.19175" layer="21"/>
<rectangle x1="5.92455" y1="10.17905" x2="6.50875" y2="10.19175" layer="21"/>
<rectangle x1="6.61035" y1="10.17905" x2="7.42315" y2="10.19175" layer="21"/>
<rectangle x1="7.51205" y1="10.17905" x2="8.47725" y2="10.19175" layer="21"/>
<rectangle x1="8.60425" y1="10.17905" x2="8.66775" y2="10.19175" layer="21"/>
<rectangle x1="8.79475" y1="10.17905" x2="10.03935" y2="10.19175" layer="21"/>
<rectangle x1="10.14095" y1="10.17905" x2="10.49655" y2="10.19175" layer="21"/>
<rectangle x1="10.61085" y1="10.17905" x2="10.83945" y2="10.19175" layer="21"/>
<rectangle x1="10.94105" y1="10.17905" x2="11.29665" y2="10.19175" layer="21"/>
<rectangle x1="11.41095" y1="10.17905" x2="12.42695" y2="10.19175" layer="21"/>
<rectangle x1="12.52855" y1="10.17905" x2="13.11275" y2="10.19175" layer="21"/>
<rectangle x1="13.21435" y1="10.17905" x2="14.02715" y2="10.19175" layer="21"/>
<rectangle x1="14.68755" y1="10.17905" x2="15.58925" y2="10.19175" layer="21"/>
<rectangle x1="15.67815" y1="10.17905" x2="17.32915" y2="10.19175" layer="21"/>
<rectangle x1="4.76885" y1="10.19175" x2="5.82295" y2="10.20445" layer="21"/>
<rectangle x1="5.93725" y1="10.19175" x2="6.49605" y2="10.20445" layer="21"/>
<rectangle x1="6.61035" y1="10.19175" x2="7.42315" y2="10.20445" layer="21"/>
<rectangle x1="7.51205" y1="10.19175" x2="8.48995" y2="10.20445" layer="21"/>
<rectangle x1="8.61695" y1="10.19175" x2="8.66775" y2="10.20445" layer="21"/>
<rectangle x1="8.79475" y1="10.19175" x2="10.02665" y2="10.20445" layer="21"/>
<rectangle x1="10.14095" y1="10.19175" x2="10.49655" y2="10.20445" layer="21"/>
<rectangle x1="10.61085" y1="10.19175" x2="10.82675" y2="10.20445" layer="21"/>
<rectangle x1="10.94105" y1="10.19175" x2="11.29665" y2="10.20445" layer="21"/>
<rectangle x1="11.41095" y1="10.19175" x2="12.42695" y2="10.20445" layer="21"/>
<rectangle x1="12.54125" y1="10.19175" x2="13.10005" y2="10.20445" layer="21"/>
<rectangle x1="13.21435" y1="10.19175" x2="14.02715" y2="10.20445" layer="21"/>
<rectangle x1="14.72565" y1="10.19175" x2="15.58925" y2="10.20445" layer="21"/>
<rectangle x1="15.67815" y1="10.19175" x2="17.32915" y2="10.20445" layer="21"/>
<rectangle x1="4.76885" y1="10.20445" x2="5.83565" y2="10.21715" layer="21"/>
<rectangle x1="5.93725" y1="10.20445" x2="6.49605" y2="10.21715" layer="21"/>
<rectangle x1="6.59765" y1="10.20445" x2="7.42315" y2="10.21715" layer="21"/>
<rectangle x1="7.51205" y1="10.20445" x2="8.50265" y2="10.21715" layer="21"/>
<rectangle x1="8.62965" y1="10.20445" x2="8.65505" y2="10.21715" layer="21"/>
<rectangle x1="8.78205" y1="10.20445" x2="10.02665" y2="10.21715" layer="21"/>
<rectangle x1="10.14095" y1="10.20445" x2="10.50925" y2="10.21715" layer="21"/>
<rectangle x1="10.62355" y1="10.20445" x2="10.82675" y2="10.21715" layer="21"/>
<rectangle x1="10.94105" y1="10.20445" x2="11.30935" y2="10.21715" layer="21"/>
<rectangle x1="11.42365" y1="10.20445" x2="12.43965" y2="10.21715" layer="21"/>
<rectangle x1="12.54125" y1="10.20445" x2="13.10005" y2="10.21715" layer="21"/>
<rectangle x1="13.20165" y1="10.20445" x2="14.02715" y2="10.21715" layer="21"/>
<rectangle x1="14.75105" y1="10.20445" x2="15.58925" y2="10.21715" layer="21"/>
<rectangle x1="15.67815" y1="10.20445" x2="17.32915" y2="10.21715" layer="21"/>
<rectangle x1="4.76885" y1="10.21715" x2="5.83565" y2="10.22985" layer="21"/>
<rectangle x1="5.94995" y1="10.21715" x2="6.48335" y2="10.22985" layer="21"/>
<rectangle x1="6.59765" y1="10.21715" x2="7.42315" y2="10.22985" layer="21"/>
<rectangle x1="7.51205" y1="10.21715" x2="8.50265" y2="10.22985" layer="21"/>
<rectangle x1="8.62965" y1="10.21715" x2="8.64235" y2="10.22985" layer="21"/>
<rectangle x1="8.76935" y1="10.21715" x2="10.01395" y2="10.22985" layer="21"/>
<rectangle x1="10.12825" y1="10.21715" x2="10.50925" y2="10.22985" layer="21"/>
<rectangle x1="10.62355" y1="10.21715" x2="10.81405" y2="10.22985" layer="21"/>
<rectangle x1="10.92835" y1="10.21715" x2="11.30935" y2="10.22985" layer="21"/>
<rectangle x1="11.42365" y1="10.21715" x2="12.43965" y2="10.22985" layer="21"/>
<rectangle x1="12.55395" y1="10.21715" x2="13.08735" y2="10.22985" layer="21"/>
<rectangle x1="13.20165" y1="10.21715" x2="14.02715" y2="10.22985" layer="21"/>
<rectangle x1="14.77645" y1="10.21715" x2="15.58925" y2="10.22985" layer="21"/>
<rectangle x1="15.67815" y1="10.21715" x2="17.32915" y2="10.22985" layer="21"/>
<rectangle x1="4.76885" y1="10.22985" x2="5.84835" y2="10.24255" layer="21"/>
<rectangle x1="5.94995" y1="10.22985" x2="6.48335" y2="10.24255" layer="21"/>
<rectangle x1="6.58495" y1="10.22985" x2="7.42315" y2="10.24255" layer="21"/>
<rectangle x1="7.51205" y1="10.22985" x2="8.51535" y2="10.24255" layer="21"/>
<rectangle x1="8.75665" y1="10.22985" x2="10.01395" y2="10.24255" layer="21"/>
<rectangle x1="10.12825" y1="10.22985" x2="10.52195" y2="10.24255" layer="21"/>
<rectangle x1="10.62355" y1="10.22985" x2="10.81405" y2="10.24255" layer="21"/>
<rectangle x1="10.92835" y1="10.22985" x2="11.32205" y2="10.24255" layer="21"/>
<rectangle x1="11.42365" y1="10.22985" x2="12.45235" y2="10.24255" layer="21"/>
<rectangle x1="12.55395" y1="10.22985" x2="13.08735" y2="10.24255" layer="21"/>
<rectangle x1="13.18895" y1="10.22985" x2="14.02715" y2="10.24255" layer="21"/>
<rectangle x1="14.78915" y1="10.22985" x2="15.58925" y2="10.24255" layer="21"/>
<rectangle x1="15.67815" y1="10.22985" x2="17.32915" y2="10.24255" layer="21"/>
<rectangle x1="4.76885" y1="10.24255" x2="5.84835" y2="10.25525" layer="21"/>
<rectangle x1="5.94995" y1="10.24255" x2="6.47065" y2="10.25525" layer="21"/>
<rectangle x1="6.58495" y1="10.24255" x2="7.42315" y2="10.25525" layer="21"/>
<rectangle x1="7.51205" y1="10.24255" x2="8.52805" y2="10.25525" layer="21"/>
<rectangle x1="8.74395" y1="10.24255" x2="10.01395" y2="10.25525" layer="21"/>
<rectangle x1="10.11555" y1="10.24255" x2="10.52195" y2="10.25525" layer="21"/>
<rectangle x1="10.63625" y1="10.24255" x2="10.81405" y2="10.25525" layer="21"/>
<rectangle x1="10.91565" y1="10.24255" x2="11.32205" y2="10.25525" layer="21"/>
<rectangle x1="11.43635" y1="10.24255" x2="12.45235" y2="10.25525" layer="21"/>
<rectangle x1="12.55395" y1="10.24255" x2="13.07465" y2="10.25525" layer="21"/>
<rectangle x1="13.18895" y1="10.24255" x2="14.02715" y2="10.25525" layer="21"/>
<rectangle x1="14.11605" y1="10.24255" x2="14.56055" y2="10.25525" layer="21"/>
<rectangle x1="14.81455" y1="10.24255" x2="15.58925" y2="10.25525" layer="21"/>
<rectangle x1="15.67815" y1="10.24255" x2="17.32915" y2="10.25525" layer="21"/>
<rectangle x1="4.76885" y1="10.25525" x2="5.86105" y2="10.26795" layer="21"/>
<rectangle x1="5.96265" y1="10.25525" x2="6.47065" y2="10.26795" layer="21"/>
<rectangle x1="6.57225" y1="10.25525" x2="7.42315" y2="10.26795" layer="21"/>
<rectangle x1="7.51205" y1="10.25525" x2="8.54075" y2="10.26795" layer="21"/>
<rectangle x1="8.74395" y1="10.25525" x2="10.00125" y2="10.26795" layer="21"/>
<rectangle x1="10.11555" y1="10.25525" x2="10.53465" y2="10.26795" layer="21"/>
<rectangle x1="10.63625" y1="10.25525" x2="10.80135" y2="10.26795" layer="21"/>
<rectangle x1="10.91565" y1="10.25525" x2="11.33475" y2="10.26795" layer="21"/>
<rectangle x1="11.43635" y1="10.25525" x2="12.46505" y2="10.26795" layer="21"/>
<rectangle x1="12.56665" y1="10.25525" x2="13.07465" y2="10.26795" layer="21"/>
<rectangle x1="13.17625" y1="10.25525" x2="14.02715" y2="10.26795" layer="21"/>
<rectangle x1="14.11605" y1="10.25525" x2="14.62405" y2="10.26795" layer="21"/>
<rectangle x1="14.82725" y1="10.25525" x2="15.58925" y2="10.26795" layer="21"/>
<rectangle x1="15.67815" y1="10.25525" x2="17.32915" y2="10.26795" layer="21"/>
<rectangle x1="4.76885" y1="10.26795" x2="5.86105" y2="10.28065" layer="21"/>
<rectangle x1="5.96265" y1="10.26795" x2="6.45795" y2="10.28065" layer="21"/>
<rectangle x1="6.57225" y1="10.26795" x2="7.42315" y2="10.28065" layer="21"/>
<rectangle x1="7.51205" y1="10.26795" x2="8.54075" y2="10.28065" layer="21"/>
<rectangle x1="8.73125" y1="10.26795" x2="10.00125" y2="10.28065" layer="21"/>
<rectangle x1="10.10285" y1="10.26795" x2="10.53465" y2="10.28065" layer="21"/>
<rectangle x1="10.64895" y1="10.26795" x2="10.80135" y2="10.28065" layer="21"/>
<rectangle x1="10.90295" y1="10.26795" x2="11.33475" y2="10.28065" layer="21"/>
<rectangle x1="11.44905" y1="10.26795" x2="12.46505" y2="10.28065" layer="21"/>
<rectangle x1="12.56665" y1="10.26795" x2="13.06195" y2="10.28065" layer="21"/>
<rectangle x1="13.17625" y1="10.26795" x2="14.02715" y2="10.28065" layer="21"/>
<rectangle x1="14.11605" y1="10.26795" x2="14.66215" y2="10.28065" layer="21"/>
<rectangle x1="14.83995" y1="10.26795" x2="15.58925" y2="10.28065" layer="21"/>
<rectangle x1="16.37665" y1="10.26795" x2="17.32915" y2="10.28065" layer="21"/>
<rectangle x1="4.76885" y1="10.28065" x2="5.87375" y2="10.29335" layer="21"/>
<rectangle x1="5.97535" y1="10.28065" x2="6.45795" y2="10.29335" layer="21"/>
<rectangle x1="6.55955" y1="10.28065" x2="7.42315" y2="10.29335" layer="21"/>
<rectangle x1="7.51205" y1="10.28065" x2="8.55345" y2="10.29335" layer="21"/>
<rectangle x1="8.71855" y1="10.28065" x2="9.98855" y2="10.29335" layer="21"/>
<rectangle x1="10.10285" y1="10.28065" x2="10.54735" y2="10.29335" layer="21"/>
<rectangle x1="10.64895" y1="10.28065" x2="10.78865" y2="10.29335" layer="21"/>
<rectangle x1="10.90295" y1="10.28065" x2="11.34745" y2="10.29335" layer="21"/>
<rectangle x1="11.44905" y1="10.28065" x2="12.47775" y2="10.29335" layer="21"/>
<rectangle x1="12.57935" y1="10.28065" x2="13.06195" y2="10.29335" layer="21"/>
<rectangle x1="13.16355" y1="10.28065" x2="14.02715" y2="10.29335" layer="21"/>
<rectangle x1="14.11605" y1="10.28065" x2="14.68755" y2="10.29335" layer="21"/>
<rectangle x1="14.85265" y1="10.28065" x2="15.58925" y2="10.29335" layer="21"/>
<rectangle x1="16.37665" y1="10.28065" x2="17.32915" y2="10.29335" layer="21"/>
<rectangle x1="4.76885" y1="10.29335" x2="5.87375" y2="10.30605" layer="21"/>
<rectangle x1="5.97535" y1="10.29335" x2="6.44525" y2="10.30605" layer="21"/>
<rectangle x1="6.55955" y1="10.29335" x2="7.42315" y2="10.30605" layer="21"/>
<rectangle x1="7.51205" y1="10.29335" x2="8.56615" y2="10.30605" layer="21"/>
<rectangle x1="8.70585" y1="10.29335" x2="9.98855" y2="10.30605" layer="21"/>
<rectangle x1="10.09015" y1="10.29335" x2="10.54735" y2="10.30605" layer="21"/>
<rectangle x1="10.66165" y1="10.29335" x2="10.78865" y2="10.30605" layer="21"/>
<rectangle x1="10.89025" y1="10.29335" x2="11.34745" y2="10.30605" layer="21"/>
<rectangle x1="11.46175" y1="10.29335" x2="12.47775" y2="10.30605" layer="21"/>
<rectangle x1="12.57935" y1="10.29335" x2="13.04925" y2="10.30605" layer="21"/>
<rectangle x1="13.16355" y1="10.29335" x2="14.02715" y2="10.30605" layer="21"/>
<rectangle x1="14.11605" y1="10.29335" x2="14.71295" y2="10.30605" layer="21"/>
<rectangle x1="14.86535" y1="10.29335" x2="15.58925" y2="10.30605" layer="21"/>
<rectangle x1="16.37665" y1="10.29335" x2="17.32915" y2="10.30605" layer="21"/>
<rectangle x1="4.76885" y1="10.30605" x2="5.87375" y2="10.31875" layer="21"/>
<rectangle x1="5.98805" y1="10.30605" x2="6.44525" y2="10.31875" layer="21"/>
<rectangle x1="6.54685" y1="10.30605" x2="7.42315" y2="10.31875" layer="21"/>
<rectangle x1="7.51205" y1="10.30605" x2="8.56615" y2="10.31875" layer="21"/>
<rectangle x1="8.70585" y1="10.30605" x2="9.97585" y2="10.31875" layer="21"/>
<rectangle x1="10.09015" y1="10.30605" x2="10.56005" y2="10.31875" layer="21"/>
<rectangle x1="10.66165" y1="10.30605" x2="10.77595" y2="10.31875" layer="21"/>
<rectangle x1="10.89025" y1="10.30605" x2="11.36015" y2="10.31875" layer="21"/>
<rectangle x1="11.46175" y1="10.30605" x2="12.47775" y2="10.31875" layer="21"/>
<rectangle x1="12.59205" y1="10.30605" x2="13.04925" y2="10.31875" layer="21"/>
<rectangle x1="13.15085" y1="10.30605" x2="14.02715" y2="10.31875" layer="21"/>
<rectangle x1="14.11605" y1="10.30605" x2="14.73835" y2="10.31875" layer="21"/>
<rectangle x1="14.87805" y1="10.30605" x2="15.58925" y2="10.31875" layer="21"/>
<rectangle x1="16.37665" y1="10.30605" x2="17.32915" y2="10.31875" layer="21"/>
<rectangle x1="4.76885" y1="10.31875" x2="5.88645" y2="10.33145" layer="21"/>
<rectangle x1="5.98805" y1="10.31875" x2="6.44525" y2="10.33145" layer="21"/>
<rectangle x1="6.54685" y1="10.31875" x2="7.42315" y2="10.33145" layer="21"/>
<rectangle x1="7.51205" y1="10.31875" x2="8.55345" y2="10.33145" layer="21"/>
<rectangle x1="8.71855" y1="10.31875" x2="9.97585" y2="10.33145" layer="21"/>
<rectangle x1="10.07745" y1="10.31875" x2="10.56005" y2="10.33145" layer="21"/>
<rectangle x1="10.67435" y1="10.31875" x2="10.77595" y2="10.33145" layer="21"/>
<rectangle x1="10.87755" y1="10.31875" x2="11.36015" y2="10.33145" layer="21"/>
<rectangle x1="11.47445" y1="10.31875" x2="12.49045" y2="10.33145" layer="21"/>
<rectangle x1="12.59205" y1="10.31875" x2="13.04925" y2="10.33145" layer="21"/>
<rectangle x1="13.15085" y1="10.31875" x2="14.02715" y2="10.33145" layer="21"/>
<rectangle x1="14.11605" y1="10.31875" x2="14.75105" y2="10.33145" layer="21"/>
<rectangle x1="14.89075" y1="10.31875" x2="15.58925" y2="10.33145" layer="21"/>
<rectangle x1="16.37665" y1="10.31875" x2="17.32915" y2="10.33145" layer="21"/>
<rectangle x1="4.76885" y1="10.33145" x2="5.88645" y2="10.34415" layer="21"/>
<rectangle x1="6.00075" y1="10.33145" x2="6.43255" y2="10.34415" layer="21"/>
<rectangle x1="6.53415" y1="10.33145" x2="7.42315" y2="10.34415" layer="21"/>
<rectangle x1="7.51205" y1="10.33145" x2="8.55345" y2="10.34415" layer="21"/>
<rectangle x1="8.73125" y1="10.33145" x2="9.96315" y2="10.34415" layer="21"/>
<rectangle x1="10.07745" y1="10.33145" x2="10.57275" y2="10.34415" layer="21"/>
<rectangle x1="10.67435" y1="10.33145" x2="10.76325" y2="10.34415" layer="21"/>
<rectangle x1="10.87755" y1="10.33145" x2="11.37285" y2="10.34415" layer="21"/>
<rectangle x1="11.47445" y1="10.33145" x2="12.49045" y2="10.34415" layer="21"/>
<rectangle x1="12.60475" y1="10.33145" x2="13.03655" y2="10.34415" layer="21"/>
<rectangle x1="13.13815" y1="10.33145" x2="14.02715" y2="10.34415" layer="21"/>
<rectangle x1="14.11605" y1="10.33145" x2="14.76375" y2="10.34415" layer="21"/>
<rectangle x1="14.89075" y1="10.33145" x2="15.58925" y2="10.34415" layer="21"/>
<rectangle x1="16.37665" y1="10.33145" x2="17.32915" y2="10.34415" layer="21"/>
<rectangle x1="4.76885" y1="10.34415" x2="5.89915" y2="10.35685" layer="21"/>
<rectangle x1="6.00075" y1="10.34415" x2="6.43255" y2="10.35685" layer="21"/>
<rectangle x1="6.53415" y1="10.34415" x2="7.42315" y2="10.35685" layer="21"/>
<rectangle x1="7.51205" y1="10.34415" x2="8.54075" y2="10.35685" layer="21"/>
<rectangle x1="8.74395" y1="10.34415" x2="9.96315" y2="10.35685" layer="21"/>
<rectangle x1="10.06475" y1="10.34415" x2="10.57275" y2="10.35685" layer="21"/>
<rectangle x1="10.68705" y1="10.34415" x2="10.76325" y2="10.35685" layer="21"/>
<rectangle x1="10.86485" y1="10.34415" x2="11.37285" y2="10.35685" layer="21"/>
<rectangle x1="11.48715" y1="10.34415" x2="12.50315" y2="10.35685" layer="21"/>
<rectangle x1="12.60475" y1="10.34415" x2="13.03655" y2="10.35685" layer="21"/>
<rectangle x1="13.13815" y1="10.34415" x2="14.02715" y2="10.35685" layer="21"/>
<rectangle x1="14.11605" y1="10.34415" x2="14.77645" y2="10.35685" layer="21"/>
<rectangle x1="14.90345" y1="10.34415" x2="15.58925" y2="10.35685" layer="21"/>
<rectangle x1="16.36395" y1="10.34415" x2="17.32915" y2="10.35685" layer="21"/>
<rectangle x1="4.76885" y1="10.35685" x2="5.89915" y2="10.36955" layer="21"/>
<rectangle x1="6.01345" y1="10.35685" x2="6.41985" y2="10.36955" layer="21"/>
<rectangle x1="6.53415" y1="10.35685" x2="7.42315" y2="10.36955" layer="21"/>
<rectangle x1="7.51205" y1="10.35685" x2="8.52805" y2="10.36955" layer="21"/>
<rectangle x1="8.74395" y1="10.35685" x2="9.95045" y2="10.36955" layer="21"/>
<rectangle x1="10.06475" y1="10.35685" x2="10.57275" y2="10.36955" layer="21"/>
<rectangle x1="10.68705" y1="10.35685" x2="10.75055" y2="10.36955" layer="21"/>
<rectangle x1="10.86485" y1="10.35685" x2="11.37285" y2="10.36955" layer="21"/>
<rectangle x1="11.48715" y1="10.35685" x2="12.50315" y2="10.36955" layer="21"/>
<rectangle x1="12.61745" y1="10.35685" x2="13.02385" y2="10.36955" layer="21"/>
<rectangle x1="13.13815" y1="10.35685" x2="14.02715" y2="10.36955" layer="21"/>
<rectangle x1="14.11605" y1="10.35685" x2="14.78915" y2="10.36955" layer="21"/>
<rectangle x1="14.90345" y1="10.35685" x2="15.58925" y2="10.36955" layer="21"/>
<rectangle x1="15.67815" y1="10.35685" x2="17.32915" y2="10.36955" layer="21"/>
<rectangle x1="4.76885" y1="10.36955" x2="5.91185" y2="10.38225" layer="21"/>
<rectangle x1="6.01345" y1="10.36955" x2="6.41985" y2="10.38225" layer="21"/>
<rectangle x1="6.52145" y1="10.36955" x2="7.42315" y2="10.38225" layer="21"/>
<rectangle x1="7.51205" y1="10.36955" x2="8.51535" y2="10.38225" layer="21"/>
<rectangle x1="8.75665" y1="10.36955" x2="9.95045" y2="10.38225" layer="21"/>
<rectangle x1="10.06475" y1="10.36955" x2="10.58545" y2="10.38225" layer="21"/>
<rectangle x1="10.69975" y1="10.36955" x2="10.75055" y2="10.38225" layer="21"/>
<rectangle x1="10.86485" y1="10.36955" x2="11.38555" y2="10.38225" layer="21"/>
<rectangle x1="11.49985" y1="10.36955" x2="12.51585" y2="10.38225" layer="21"/>
<rectangle x1="12.61745" y1="10.36955" x2="13.02385" y2="10.38225" layer="21"/>
<rectangle x1="13.12545" y1="10.36955" x2="14.02715" y2="10.38225" layer="21"/>
<rectangle x1="14.11605" y1="10.36955" x2="14.80185" y2="10.38225" layer="21"/>
<rectangle x1="14.91615" y1="10.36955" x2="15.58925" y2="10.38225" layer="21"/>
<rectangle x1="15.67815" y1="10.36955" x2="17.32915" y2="10.38225" layer="21"/>
<rectangle x1="4.76885" y1="10.38225" x2="5.91185" y2="10.39495" layer="21"/>
<rectangle x1="6.01345" y1="10.38225" x2="6.40715" y2="10.39495" layer="21"/>
<rectangle x1="6.52145" y1="10.38225" x2="7.42315" y2="10.39495" layer="21"/>
<rectangle x1="7.51205" y1="10.38225" x2="8.51535" y2="10.39495" layer="21"/>
<rectangle x1="8.76935" y1="10.38225" x2="9.93775" y2="10.39495" layer="21"/>
<rectangle x1="10.05205" y1="10.38225" x2="10.58545" y2="10.39495" layer="21"/>
<rectangle x1="10.69975" y1="10.38225" x2="10.73785" y2="10.39495" layer="21"/>
<rectangle x1="10.85215" y1="10.38225" x2="11.38555" y2="10.39495" layer="21"/>
<rectangle x1="11.49985" y1="10.38225" x2="12.51585" y2="10.39495" layer="21"/>
<rectangle x1="12.61745" y1="10.38225" x2="13.01115" y2="10.39495" layer="21"/>
<rectangle x1="13.12545" y1="10.38225" x2="14.02715" y2="10.39495" layer="21"/>
<rectangle x1="14.11605" y1="10.38225" x2="14.81455" y2="10.39495" layer="21"/>
<rectangle x1="14.91615" y1="10.38225" x2="15.58925" y2="10.39495" layer="21"/>
<rectangle x1="15.67815" y1="10.38225" x2="17.32915" y2="10.39495" layer="21"/>
<rectangle x1="4.76885" y1="10.39495" x2="5.92455" y2="10.40765" layer="21"/>
<rectangle x1="6.02615" y1="10.39495" x2="6.40715" y2="10.40765" layer="21"/>
<rectangle x1="6.50875" y1="10.39495" x2="7.42315" y2="10.40765" layer="21"/>
<rectangle x1="7.51205" y1="10.39495" x2="8.50265" y2="10.40765" layer="21"/>
<rectangle x1="8.62965" y1="10.39495" x2="8.65505" y2="10.40765" layer="21"/>
<rectangle x1="8.78205" y1="10.39495" x2="9.93775" y2="10.40765" layer="21"/>
<rectangle x1="10.05205" y1="10.39495" x2="10.59815" y2="10.40765" layer="21"/>
<rectangle x1="10.69975" y1="10.39495" x2="10.73785" y2="10.40765" layer="21"/>
<rectangle x1="10.85215" y1="10.39495" x2="11.39825" y2="10.40765" layer="21"/>
<rectangle x1="11.49985" y1="10.39495" x2="12.52855" y2="10.40765" layer="21"/>
<rectangle x1="12.63015" y1="10.39495" x2="13.01115" y2="10.40765" layer="21"/>
<rectangle x1="13.11275" y1="10.39495" x2="14.02715" y2="10.40765" layer="21"/>
<rectangle x1="14.11605" y1="10.39495" x2="14.81455" y2="10.40765" layer="21"/>
<rectangle x1="14.92885" y1="10.39495" x2="15.58925" y2="10.40765" layer="21"/>
<rectangle x1="15.67815" y1="10.39495" x2="17.32915" y2="10.40765" layer="21"/>
<rectangle x1="4.76885" y1="10.40765" x2="5.92455" y2="10.42035" layer="21"/>
<rectangle x1="6.02615" y1="10.40765" x2="6.39445" y2="10.42035" layer="21"/>
<rectangle x1="6.50875" y1="10.40765" x2="7.42315" y2="10.42035" layer="21"/>
<rectangle x1="7.51205" y1="10.40765" x2="8.48995" y2="10.42035" layer="21"/>
<rectangle x1="8.61695" y1="10.40765" x2="8.65505" y2="10.42035" layer="21"/>
<rectangle x1="8.78205" y1="10.40765" x2="9.93775" y2="10.42035" layer="21"/>
<rectangle x1="10.03935" y1="10.40765" x2="10.59815" y2="10.42035" layer="21"/>
<rectangle x1="10.71245" y1="10.40765" x2="10.73785" y2="10.42035" layer="21"/>
<rectangle x1="10.83945" y1="10.40765" x2="11.39825" y2="10.42035" layer="21"/>
<rectangle x1="11.51255" y1="10.40765" x2="12.52855" y2="10.42035" layer="21"/>
<rectangle x1="12.63015" y1="10.40765" x2="12.99845" y2="10.42035" layer="21"/>
<rectangle x1="13.11275" y1="10.40765" x2="14.02715" y2="10.42035" layer="21"/>
<rectangle x1="14.11605" y1="10.40765" x2="14.82725" y2="10.42035" layer="21"/>
<rectangle x1="14.92885" y1="10.40765" x2="15.58925" y2="10.42035" layer="21"/>
<rectangle x1="15.67815" y1="10.40765" x2="17.32915" y2="10.42035" layer="21"/>
<rectangle x1="4.76885" y1="10.42035" x2="5.93725" y2="10.43305" layer="21"/>
<rectangle x1="6.03885" y1="10.42035" x2="6.39445" y2="10.43305" layer="21"/>
<rectangle x1="6.49605" y1="10.42035" x2="7.42315" y2="10.43305" layer="21"/>
<rectangle x1="7.51205" y1="10.42035" x2="8.47725" y2="10.43305" layer="21"/>
<rectangle x1="8.60425" y1="10.42035" x2="8.66775" y2="10.43305" layer="21"/>
<rectangle x1="8.79475" y1="10.42035" x2="9.92505" y2="10.43305" layer="21"/>
<rectangle x1="10.03935" y1="10.42035" x2="10.61085" y2="10.43305" layer="21"/>
<rectangle x1="10.71245" y1="10.42035" x2="10.72515" y2="10.43305" layer="21"/>
<rectangle x1="10.83945" y1="10.42035" x2="11.41095" y2="10.43305" layer="21"/>
<rectangle x1="11.51255" y1="10.42035" x2="12.54125" y2="10.43305" layer="21"/>
<rectangle x1="12.64285" y1="10.42035" x2="12.99845" y2="10.43305" layer="21"/>
<rectangle x1="13.10005" y1="10.42035" x2="14.02715" y2="10.43305" layer="21"/>
<rectangle x1="14.11605" y1="10.42035" x2="14.82725" y2="10.43305" layer="21"/>
<rectangle x1="14.92885" y1="10.42035" x2="15.58925" y2="10.43305" layer="21"/>
<rectangle x1="15.67815" y1="10.42035" x2="17.32915" y2="10.43305" layer="21"/>
<rectangle x1="4.76885" y1="10.43305" x2="5.93725" y2="10.44575" layer="21"/>
<rectangle x1="6.03885" y1="10.43305" x2="6.38175" y2="10.44575" layer="21"/>
<rectangle x1="6.49605" y1="10.43305" x2="7.42315" y2="10.44575" layer="21"/>
<rectangle x1="7.51205" y1="10.43305" x2="8.47725" y2="10.44575" layer="21"/>
<rectangle x1="8.59155" y1="10.43305" x2="8.68045" y2="10.44575" layer="21"/>
<rectangle x1="8.80745" y1="10.43305" x2="9.92505" y2="10.44575" layer="21"/>
<rectangle x1="10.02665" y1="10.43305" x2="10.61085" y2="10.44575" layer="21"/>
<rectangle x1="10.82675" y1="10.43305" x2="11.41095" y2="10.44575" layer="21"/>
<rectangle x1="11.52525" y1="10.43305" x2="12.54125" y2="10.44575" layer="21"/>
<rectangle x1="12.64285" y1="10.43305" x2="12.98575" y2="10.44575" layer="21"/>
<rectangle x1="13.10005" y1="10.43305" x2="14.02715" y2="10.44575" layer="21"/>
<rectangle x1="14.11605" y1="10.43305" x2="14.83995" y2="10.44575" layer="21"/>
<rectangle x1="14.94155" y1="10.43305" x2="15.58925" y2="10.44575" layer="21"/>
<rectangle x1="15.67815" y1="10.43305" x2="17.32915" y2="10.44575" layer="21"/>
<rectangle x1="4.76885" y1="10.44575" x2="5.94995" y2="10.45845" layer="21"/>
<rectangle x1="6.05155" y1="10.44575" x2="6.38175" y2="10.45845" layer="21"/>
<rectangle x1="6.48335" y1="10.44575" x2="7.42315" y2="10.45845" layer="21"/>
<rectangle x1="7.51205" y1="10.44575" x2="8.46455" y2="10.45845" layer="21"/>
<rectangle x1="8.59155" y1="10.44575" x2="8.69315" y2="10.45845" layer="21"/>
<rectangle x1="8.82015" y1="10.44575" x2="9.91235" y2="10.45845" layer="21"/>
<rectangle x1="10.02665" y1="10.44575" x2="10.62355" y2="10.45845" layer="21"/>
<rectangle x1="10.82675" y1="10.44575" x2="11.42365" y2="10.45845" layer="21"/>
<rectangle x1="11.52525" y1="10.44575" x2="12.55395" y2="10.45845" layer="21"/>
<rectangle x1="12.65555" y1="10.44575" x2="12.98575" y2="10.45845" layer="21"/>
<rectangle x1="13.08735" y1="10.44575" x2="14.02715" y2="10.45845" layer="21"/>
<rectangle x1="14.11605" y1="10.44575" x2="14.83995" y2="10.45845" layer="21"/>
<rectangle x1="14.94155" y1="10.44575" x2="15.58925" y2="10.45845" layer="21"/>
<rectangle x1="15.67815" y1="10.44575" x2="17.32915" y2="10.45845" layer="21"/>
<rectangle x1="4.76885" y1="10.45845" x2="5.94995" y2="10.47115" layer="21"/>
<rectangle x1="6.05155" y1="10.45845" x2="6.38175" y2="10.47115" layer="21"/>
<rectangle x1="6.48335" y1="10.45845" x2="7.42315" y2="10.47115" layer="21"/>
<rectangle x1="7.51205" y1="10.45845" x2="8.45185" y2="10.47115" layer="21"/>
<rectangle x1="8.57885" y1="10.45845" x2="8.69315" y2="10.47115" layer="21"/>
<rectangle x1="8.82015" y1="10.45845" x2="9.91235" y2="10.47115" layer="21"/>
<rectangle x1="10.01395" y1="10.45845" x2="10.62355" y2="10.47115" layer="21"/>
<rectangle x1="10.81405" y1="10.45845" x2="11.42365" y2="10.47115" layer="21"/>
<rectangle x1="11.53795" y1="10.45845" x2="12.55395" y2="10.47115" layer="21"/>
<rectangle x1="12.65555" y1="10.45845" x2="12.98575" y2="10.47115" layer="21"/>
<rectangle x1="13.08735" y1="10.45845" x2="14.02715" y2="10.47115" layer="21"/>
<rectangle x1="14.11605" y1="10.45845" x2="14.83995" y2="10.47115" layer="21"/>
<rectangle x1="14.94155" y1="10.45845" x2="15.58925" y2="10.47115" layer="21"/>
<rectangle x1="15.67815" y1="10.45845" x2="17.32915" y2="10.47115" layer="21"/>
<rectangle x1="4.76885" y1="10.47115" x2="5.94995" y2="10.48385" layer="21"/>
<rectangle x1="6.06425" y1="10.47115" x2="6.36905" y2="10.48385" layer="21"/>
<rectangle x1="6.47065" y1="10.47115" x2="7.42315" y2="10.48385" layer="21"/>
<rectangle x1="7.51205" y1="10.47115" x2="8.43915" y2="10.48385" layer="21"/>
<rectangle x1="8.56615" y1="10.47115" x2="8.70585" y2="10.48385" layer="21"/>
<rectangle x1="8.83285" y1="10.47115" x2="9.89965" y2="10.48385" layer="21"/>
<rectangle x1="10.01395" y1="10.47115" x2="10.63625" y2="10.48385" layer="21"/>
<rectangle x1="10.81405" y1="10.47115" x2="11.43635" y2="10.48385" layer="21"/>
<rectangle x1="11.53795" y1="10.47115" x2="12.55395" y2="10.48385" layer="21"/>
<rectangle x1="12.66825" y1="10.47115" x2="12.97305" y2="10.48385" layer="21"/>
<rectangle x1="13.07465" y1="10.47115" x2="14.02715" y2="10.48385" layer="21"/>
<rectangle x1="14.11605" y1="10.47115" x2="14.83995" y2="10.48385" layer="21"/>
<rectangle x1="14.94155" y1="10.47115" x2="15.58925" y2="10.48385" layer="21"/>
<rectangle x1="15.67815" y1="10.47115" x2="17.32915" y2="10.48385" layer="21"/>
<rectangle x1="4.76885" y1="10.48385" x2="5.96265" y2="10.49655" layer="21"/>
<rectangle x1="6.06425" y1="10.48385" x2="6.36905" y2="10.49655" layer="21"/>
<rectangle x1="6.47065" y1="10.48385" x2="7.42315" y2="10.49655" layer="21"/>
<rectangle x1="7.51205" y1="10.48385" x2="8.42645" y2="10.49655" layer="21"/>
<rectangle x1="8.56615" y1="10.48385" x2="8.71855" y2="10.49655" layer="21"/>
<rectangle x1="8.84555" y1="10.48385" x2="9.89965" y2="10.49655" layer="21"/>
<rectangle x1="10.00125" y1="10.48385" x2="10.63625" y2="10.49655" layer="21"/>
<rectangle x1="10.80135" y1="10.48385" x2="11.43635" y2="10.49655" layer="21"/>
<rectangle x1="11.55065" y1="10.48385" x2="12.56665" y2="10.49655" layer="21"/>
<rectangle x1="12.66825" y1="10.48385" x2="12.97305" y2="10.49655" layer="21"/>
<rectangle x1="13.07465" y1="10.48385" x2="14.02715" y2="10.49655" layer="21"/>
<rectangle x1="14.11605" y1="10.48385" x2="14.85265" y2="10.49655" layer="21"/>
<rectangle x1="14.95425" y1="10.48385" x2="15.58925" y2="10.49655" layer="21"/>
<rectangle x1="15.67815" y1="10.48385" x2="17.32915" y2="10.49655" layer="21"/>
<rectangle x1="4.76885" y1="10.49655" x2="5.96265" y2="10.50925" layer="21"/>
<rectangle x1="6.07695" y1="10.49655" x2="6.35635" y2="10.50925" layer="21"/>
<rectangle x1="6.45795" y1="10.49655" x2="7.42315" y2="10.50925" layer="21"/>
<rectangle x1="7.51205" y1="10.49655" x2="8.42645" y2="10.50925" layer="21"/>
<rectangle x1="8.55345" y1="10.49655" x2="8.73125" y2="10.50925" layer="21"/>
<rectangle x1="8.85825" y1="10.49655" x2="9.88695" y2="10.50925" layer="21"/>
<rectangle x1="10.00125" y1="10.49655" x2="10.64895" y2="10.50925" layer="21"/>
<rectangle x1="10.80135" y1="10.49655" x2="11.44905" y2="10.50925" layer="21"/>
<rectangle x1="11.55065" y1="10.49655" x2="12.56665" y2="10.50925" layer="21"/>
<rectangle x1="12.68095" y1="10.49655" x2="12.96035" y2="10.50925" layer="21"/>
<rectangle x1="13.06195" y1="10.49655" x2="14.02715" y2="10.50925" layer="21"/>
<rectangle x1="14.11605" y1="10.49655" x2="14.85265" y2="10.50925" layer="21"/>
<rectangle x1="14.95425" y1="10.49655" x2="15.58925" y2="10.50925" layer="21"/>
<rectangle x1="15.67815" y1="10.49655" x2="17.32915" y2="10.50925" layer="21"/>
<rectangle x1="4.76885" y1="10.50925" x2="5.97535" y2="10.52195" layer="21"/>
<rectangle x1="6.07695" y1="10.50925" x2="6.35635" y2="10.52195" layer="21"/>
<rectangle x1="6.45795" y1="10.50925" x2="7.42315" y2="10.52195" layer="21"/>
<rectangle x1="7.51205" y1="10.50925" x2="8.41375" y2="10.52195" layer="21"/>
<rectangle x1="8.54075" y1="10.50925" x2="8.73125" y2="10.52195" layer="21"/>
<rectangle x1="8.85825" y1="10.50925" x2="9.88695" y2="10.52195" layer="21"/>
<rectangle x1="10.00125" y1="10.50925" x2="10.64895" y2="10.52195" layer="21"/>
<rectangle x1="10.80135" y1="10.50925" x2="11.44905" y2="10.52195" layer="21"/>
<rectangle x1="11.56335" y1="10.50925" x2="12.57935" y2="10.52195" layer="21"/>
<rectangle x1="12.68095" y1="10.50925" x2="12.96035" y2="10.52195" layer="21"/>
<rectangle x1="13.06195" y1="10.50925" x2="14.02715" y2="10.52195" layer="21"/>
<rectangle x1="14.11605" y1="10.50925" x2="14.85265" y2="10.52195" layer="21"/>
<rectangle x1="14.95425" y1="10.50925" x2="15.58925" y2="10.52195" layer="21"/>
<rectangle x1="15.67815" y1="10.50925" x2="17.32915" y2="10.52195" layer="21"/>
<rectangle x1="4.76885" y1="10.52195" x2="5.97535" y2="10.53465" layer="21"/>
<rectangle x1="6.07695" y1="10.52195" x2="6.34365" y2="10.53465" layer="21"/>
<rectangle x1="6.45795" y1="10.52195" x2="7.42315" y2="10.53465" layer="21"/>
<rectangle x1="7.51205" y1="10.52195" x2="8.40105" y2="10.53465" layer="21"/>
<rectangle x1="8.52805" y1="10.52195" x2="8.74395" y2="10.53465" layer="21"/>
<rectangle x1="8.87095" y1="10.52195" x2="9.87425" y2="10.53465" layer="21"/>
<rectangle x1="9.98855" y1="10.52195" x2="10.64895" y2="10.53465" layer="21"/>
<rectangle x1="10.78865" y1="10.52195" x2="11.44905" y2="10.53465" layer="21"/>
<rectangle x1="11.56335" y1="10.52195" x2="12.57935" y2="10.53465" layer="21"/>
<rectangle x1="12.68095" y1="10.52195" x2="12.94765" y2="10.53465" layer="21"/>
<rectangle x1="13.06195" y1="10.52195" x2="14.02715" y2="10.53465" layer="21"/>
<rectangle x1="14.11605" y1="10.52195" x2="14.85265" y2="10.53465" layer="21"/>
<rectangle x1="14.95425" y1="10.52195" x2="15.58925" y2="10.53465" layer="21"/>
<rectangle x1="15.67815" y1="10.52195" x2="17.32915" y2="10.53465" layer="21"/>
<rectangle x1="4.76885" y1="10.53465" x2="5.98805" y2="10.54735" layer="21"/>
<rectangle x1="6.08965" y1="10.53465" x2="6.34365" y2="10.54735" layer="21"/>
<rectangle x1="6.44525" y1="10.53465" x2="7.42315" y2="10.54735" layer="21"/>
<rectangle x1="7.51205" y1="10.53465" x2="8.38835" y2="10.54735" layer="21"/>
<rectangle x1="8.51535" y1="10.53465" x2="8.75665" y2="10.54735" layer="21"/>
<rectangle x1="8.88365" y1="10.53465" x2="9.87425" y2="10.54735" layer="21"/>
<rectangle x1="9.98855" y1="10.53465" x2="10.66165" y2="10.54735" layer="21"/>
<rectangle x1="10.78865" y1="10.53465" x2="11.46175" y2="10.54735" layer="21"/>
<rectangle x1="11.57605" y1="10.53465" x2="12.59205" y2="10.54735" layer="21"/>
<rectangle x1="12.69365" y1="10.53465" x2="12.94765" y2="10.54735" layer="21"/>
<rectangle x1="13.04925" y1="10.53465" x2="14.02715" y2="10.54735" layer="21"/>
<rectangle x1="14.11605" y1="10.53465" x2="14.85265" y2="10.54735" layer="21"/>
<rectangle x1="14.95425" y1="10.53465" x2="15.58925" y2="10.54735" layer="21"/>
<rectangle x1="15.67815" y1="10.53465" x2="17.32915" y2="10.54735" layer="21"/>
<rectangle x1="4.76885" y1="10.54735" x2="5.98805" y2="10.56005" layer="21"/>
<rectangle x1="6.08965" y1="10.54735" x2="6.33095" y2="10.56005" layer="21"/>
<rectangle x1="6.44525" y1="10.54735" x2="7.42315" y2="10.56005" layer="21"/>
<rectangle x1="7.51205" y1="10.54735" x2="8.38835" y2="10.56005" layer="21"/>
<rectangle x1="8.51535" y1="10.54735" x2="8.76935" y2="10.56005" layer="21"/>
<rectangle x1="8.89635" y1="10.54735" x2="9.86155" y2="10.56005" layer="21"/>
<rectangle x1="9.97585" y1="10.54735" x2="10.66165" y2="10.56005" layer="21"/>
<rectangle x1="10.77595" y1="10.54735" x2="11.46175" y2="10.56005" layer="21"/>
<rectangle x1="11.57605" y1="10.54735" x2="12.59205" y2="10.56005" layer="21"/>
<rectangle x1="12.69365" y1="10.54735" x2="12.93495" y2="10.56005" layer="21"/>
<rectangle x1="13.04925" y1="10.54735" x2="14.02715" y2="10.56005" layer="21"/>
<rectangle x1="14.11605" y1="10.54735" x2="14.85265" y2="10.56005" layer="21"/>
<rectangle x1="14.95425" y1="10.54735" x2="15.58925" y2="10.56005" layer="21"/>
<rectangle x1="15.67815" y1="10.54735" x2="17.32915" y2="10.56005" layer="21"/>
<rectangle x1="4.76885" y1="10.56005" x2="6.00075" y2="10.57275" layer="21"/>
<rectangle x1="6.10235" y1="10.56005" x2="6.33095" y2="10.57275" layer="21"/>
<rectangle x1="6.43255" y1="10.56005" x2="7.42315" y2="10.57275" layer="21"/>
<rectangle x1="7.51205" y1="10.56005" x2="8.37565" y2="10.57275" layer="21"/>
<rectangle x1="8.50265" y1="10.56005" x2="8.76935" y2="10.57275" layer="21"/>
<rectangle x1="8.89635" y1="10.56005" x2="9.86155" y2="10.57275" layer="21"/>
<rectangle x1="9.97585" y1="10.56005" x2="10.66165" y2="10.57275" layer="21"/>
<rectangle x1="10.77595" y1="10.56005" x2="11.47445" y2="10.57275" layer="21"/>
<rectangle x1="11.57605" y1="10.56005" x2="12.60475" y2="10.57275" layer="21"/>
<rectangle x1="12.70635" y1="10.56005" x2="12.93495" y2="10.57275" layer="21"/>
<rectangle x1="13.03655" y1="10.56005" x2="14.02715" y2="10.57275" layer="21"/>
<rectangle x1="14.11605" y1="10.56005" x2="14.85265" y2="10.57275" layer="21"/>
<rectangle x1="14.95425" y1="10.56005" x2="15.58925" y2="10.57275" layer="21"/>
<rectangle x1="15.67815" y1="10.56005" x2="17.32915" y2="10.57275" layer="21"/>
<rectangle x1="4.76885" y1="10.57275" x2="6.00075" y2="10.58545" layer="21"/>
<rectangle x1="6.10235" y1="10.57275" x2="6.31825" y2="10.58545" layer="21"/>
<rectangle x1="6.43255" y1="10.57275" x2="7.42315" y2="10.58545" layer="21"/>
<rectangle x1="7.51205" y1="10.57275" x2="8.36295" y2="10.58545" layer="21"/>
<rectangle x1="8.48995" y1="10.57275" x2="8.78205" y2="10.58545" layer="21"/>
<rectangle x1="8.90905" y1="10.57275" x2="9.86155" y2="10.58545" layer="21"/>
<rectangle x1="9.96315" y1="10.57275" x2="10.66165" y2="10.58545" layer="21"/>
<rectangle x1="10.78865" y1="10.57275" x2="11.47445" y2="10.58545" layer="21"/>
<rectangle x1="11.58875" y1="10.57275" x2="12.60475" y2="10.58545" layer="21"/>
<rectangle x1="12.70635" y1="10.57275" x2="12.92225" y2="10.58545" layer="21"/>
<rectangle x1="13.03655" y1="10.57275" x2="14.02715" y2="10.58545" layer="21"/>
<rectangle x1="14.11605" y1="10.57275" x2="14.85265" y2="10.58545" layer="21"/>
<rectangle x1="14.95425" y1="10.57275" x2="15.58925" y2="10.58545" layer="21"/>
<rectangle x1="15.67815" y1="10.57275" x2="17.32915" y2="10.58545" layer="21"/>
<rectangle x1="4.76885" y1="10.58545" x2="6.01345" y2="10.59815" layer="21"/>
<rectangle x1="6.11505" y1="10.58545" x2="6.31825" y2="10.59815" layer="21"/>
<rectangle x1="6.41985" y1="10.58545" x2="7.42315" y2="10.59815" layer="21"/>
<rectangle x1="7.51205" y1="10.58545" x2="8.36295" y2="10.59815" layer="21"/>
<rectangle x1="8.47725" y1="10.58545" x2="8.79475" y2="10.59815" layer="21"/>
<rectangle x1="8.92175" y1="10.58545" x2="9.84885" y2="10.59815" layer="21"/>
<rectangle x1="9.96315" y1="10.58545" x2="10.64895" y2="10.59815" layer="21"/>
<rectangle x1="10.78865" y1="10.58545" x2="11.48715" y2="10.59815" layer="21"/>
<rectangle x1="11.58875" y1="10.58545" x2="12.61745" y2="10.59815" layer="21"/>
<rectangle x1="12.71905" y1="10.58545" x2="12.92225" y2="10.59815" layer="21"/>
<rectangle x1="13.02385" y1="10.58545" x2="14.02715" y2="10.59815" layer="21"/>
<rectangle x1="14.11605" y1="10.58545" x2="14.85265" y2="10.59815" layer="21"/>
<rectangle x1="14.95425" y1="10.58545" x2="15.58925" y2="10.59815" layer="21"/>
<rectangle x1="15.67815" y1="10.58545" x2="17.32915" y2="10.59815" layer="21"/>
<rectangle x1="4.76885" y1="10.59815" x2="6.01345" y2="10.61085" layer="21"/>
<rectangle x1="6.11505" y1="10.59815" x2="6.30555" y2="10.61085" layer="21"/>
<rectangle x1="6.41985" y1="10.59815" x2="7.42315" y2="10.61085" layer="21"/>
<rectangle x1="7.51205" y1="10.59815" x2="8.35025" y2="10.61085" layer="21"/>
<rectangle x1="8.47725" y1="10.59815" x2="8.80745" y2="10.61085" layer="21"/>
<rectangle x1="8.93445" y1="10.59815" x2="9.84885" y2="10.61085" layer="21"/>
<rectangle x1="9.95045" y1="10.59815" x2="10.64895" y2="10.61085" layer="21"/>
<rectangle x1="10.80135" y1="10.59815" x2="11.48715" y2="10.61085" layer="21"/>
<rectangle x1="11.60145" y1="10.59815" x2="12.61745" y2="10.61085" layer="21"/>
<rectangle x1="12.71905" y1="10.59815" x2="12.92225" y2="10.61085" layer="21"/>
<rectangle x1="13.02385" y1="10.59815" x2="14.02715" y2="10.61085" layer="21"/>
<rectangle x1="14.11605" y1="10.59815" x2="14.83995" y2="10.61085" layer="21"/>
<rectangle x1="14.94155" y1="10.59815" x2="15.58925" y2="10.61085" layer="21"/>
<rectangle x1="15.67815" y1="10.59815" x2="17.32915" y2="10.61085" layer="21"/>
<rectangle x1="4.76885" y1="10.61085" x2="6.02615" y2="10.62355" layer="21"/>
<rectangle x1="6.12775" y1="10.61085" x2="6.30555" y2="10.62355" layer="21"/>
<rectangle x1="6.40715" y1="10.61085" x2="7.42315" y2="10.62355" layer="21"/>
<rectangle x1="7.51205" y1="10.61085" x2="8.33755" y2="10.62355" layer="21"/>
<rectangle x1="8.46455" y1="10.61085" x2="8.80745" y2="10.62355" layer="21"/>
<rectangle x1="8.93445" y1="10.61085" x2="9.83615" y2="10.62355" layer="21"/>
<rectangle x1="9.95045" y1="10.61085" x2="10.63625" y2="10.62355" layer="21"/>
<rectangle x1="10.80135" y1="10.61085" x2="11.49985" y2="10.62355" layer="21"/>
<rectangle x1="11.60145" y1="10.61085" x2="12.61745" y2="10.62355" layer="21"/>
<rectangle x1="12.73175" y1="10.61085" x2="12.90955" y2="10.62355" layer="21"/>
<rectangle x1="13.01115" y1="10.61085" x2="14.02715" y2="10.62355" layer="21"/>
<rectangle x1="14.11605" y1="10.61085" x2="14.83995" y2="10.62355" layer="21"/>
<rectangle x1="14.94155" y1="10.61085" x2="15.58925" y2="10.62355" layer="21"/>
<rectangle x1="15.67815" y1="10.61085" x2="17.32915" y2="10.62355" layer="21"/>
<rectangle x1="4.76885" y1="10.62355" x2="6.02615" y2="10.63625" layer="21"/>
<rectangle x1="6.12775" y1="10.62355" x2="6.30555" y2="10.63625" layer="21"/>
<rectangle x1="6.40715" y1="10.62355" x2="7.42315" y2="10.63625" layer="21"/>
<rectangle x1="7.51205" y1="10.62355" x2="8.32485" y2="10.63625" layer="21"/>
<rectangle x1="8.45185" y1="10.62355" x2="8.82015" y2="10.63625" layer="21"/>
<rectangle x1="8.94715" y1="10.62355" x2="9.83615" y2="10.63625" layer="21"/>
<rectangle x1="9.93775" y1="10.62355" x2="10.63625" y2="10.63625" layer="21"/>
<rectangle x1="10.81405" y1="10.62355" x2="11.49985" y2="10.63625" layer="21"/>
<rectangle x1="11.61415" y1="10.62355" x2="12.63015" y2="10.63625" layer="21"/>
<rectangle x1="12.73175" y1="10.62355" x2="12.90955" y2="10.63625" layer="21"/>
<rectangle x1="13.01115" y1="10.62355" x2="14.02715" y2="10.63625" layer="21"/>
<rectangle x1="14.11605" y1="10.62355" x2="14.83995" y2="10.63625" layer="21"/>
<rectangle x1="14.94155" y1="10.62355" x2="15.58925" y2="10.63625" layer="21"/>
<rectangle x1="15.67815" y1="10.62355" x2="17.32915" y2="10.63625" layer="21"/>
<rectangle x1="4.76885" y1="10.63625" x2="6.02615" y2="10.64895" layer="21"/>
<rectangle x1="6.14045" y1="10.63625" x2="6.29285" y2="10.64895" layer="21"/>
<rectangle x1="6.39445" y1="10.63625" x2="7.42315" y2="10.64895" layer="21"/>
<rectangle x1="7.51205" y1="10.63625" x2="8.32485" y2="10.64895" layer="21"/>
<rectangle x1="8.43915" y1="10.63625" x2="8.83285" y2="10.64895" layer="21"/>
<rectangle x1="8.95985" y1="10.63625" x2="9.82345" y2="10.64895" layer="21"/>
<rectangle x1="9.93775" y1="10.63625" x2="10.62355" y2="10.64895" layer="21"/>
<rectangle x1="10.81405" y1="10.63625" x2="11.51255" y2="10.64895" layer="21"/>
<rectangle x1="11.61415" y1="10.63625" x2="12.63015" y2="10.64895" layer="21"/>
<rectangle x1="12.74445" y1="10.63625" x2="12.89685" y2="10.64895" layer="21"/>
<rectangle x1="12.99845" y1="10.63625" x2="14.02715" y2="10.64895" layer="21"/>
<rectangle x1="14.11605" y1="10.63625" x2="14.83995" y2="10.64895" layer="21"/>
<rectangle x1="14.94155" y1="10.63625" x2="15.58925" y2="10.64895" layer="21"/>
<rectangle x1="15.67815" y1="10.63625" x2="17.32915" y2="10.64895" layer="21"/>
<rectangle x1="4.76885" y1="10.64895" x2="6.03885" y2="10.66165" layer="21"/>
<rectangle x1="6.14045" y1="10.64895" x2="6.29285" y2="10.66165" layer="21"/>
<rectangle x1="6.39445" y1="10.64895" x2="7.42315" y2="10.66165" layer="21"/>
<rectangle x1="7.51205" y1="10.64895" x2="8.31215" y2="10.66165" layer="21"/>
<rectangle x1="8.43915" y1="10.64895" x2="8.84555" y2="10.66165" layer="21"/>
<rectangle x1="8.97255" y1="10.64895" x2="9.82345" y2="10.66165" layer="21"/>
<rectangle x1="9.92505" y1="10.64895" x2="10.62355" y2="10.66165" layer="21"/>
<rectangle x1="10.82675" y1="10.64895" x2="11.51255" y2="10.66165" layer="21"/>
<rectangle x1="11.62685" y1="10.64895" x2="12.64285" y2="10.66165" layer="21"/>
<rectangle x1="12.74445" y1="10.64895" x2="12.89685" y2="10.66165" layer="21"/>
<rectangle x1="12.99845" y1="10.64895" x2="14.02715" y2="10.66165" layer="21"/>
<rectangle x1="14.11605" y1="10.64895" x2="14.82725" y2="10.66165" layer="21"/>
<rectangle x1="14.94155" y1="10.64895" x2="15.58925" y2="10.66165" layer="21"/>
<rectangle x1="15.67815" y1="10.64895" x2="17.32915" y2="10.66165" layer="21"/>
<rectangle x1="4.76885" y1="10.66165" x2="6.03885" y2="10.67435" layer="21"/>
<rectangle x1="6.15315" y1="10.66165" x2="6.28015" y2="10.67435" layer="21"/>
<rectangle x1="6.39445" y1="10.66165" x2="7.42315" y2="10.67435" layer="21"/>
<rectangle x1="7.51205" y1="10.66165" x2="8.29945" y2="10.67435" layer="21"/>
<rectangle x1="8.42645" y1="10.66165" x2="8.84555" y2="10.67435" layer="21"/>
<rectangle x1="8.97255" y1="10.66165" x2="9.81075" y2="10.67435" layer="21"/>
<rectangle x1="9.92505" y1="10.66165" x2="10.61085" y2="10.67435" layer="21"/>
<rectangle x1="10.82675" y1="10.66165" x2="11.52525" y2="10.67435" layer="21"/>
<rectangle x1="11.62685" y1="10.66165" x2="12.64285" y2="10.67435" layer="21"/>
<rectangle x1="12.75715" y1="10.66165" x2="12.88415" y2="10.67435" layer="21"/>
<rectangle x1="12.99845" y1="10.66165" x2="14.02715" y2="10.67435" layer="21"/>
<rectangle x1="14.11605" y1="10.66165" x2="14.82725" y2="10.67435" layer="21"/>
<rectangle x1="14.92885" y1="10.66165" x2="15.58925" y2="10.67435" layer="21"/>
<rectangle x1="15.67815" y1="10.66165" x2="17.32915" y2="10.67435" layer="21"/>
<rectangle x1="4.76885" y1="10.67435" x2="6.05155" y2="10.68705" layer="21"/>
<rectangle x1="6.15315" y1="10.67435" x2="6.28015" y2="10.68705" layer="21"/>
<rectangle x1="6.38175" y1="10.67435" x2="7.42315" y2="10.68705" layer="21"/>
<rectangle x1="7.51205" y1="10.67435" x2="8.28675" y2="10.68705" layer="21"/>
<rectangle x1="8.41375" y1="10.67435" x2="8.85825" y2="10.68705" layer="21"/>
<rectangle x1="8.98525" y1="10.67435" x2="9.81075" y2="10.68705" layer="21"/>
<rectangle x1="9.92505" y1="10.67435" x2="10.61085" y2="10.68705" layer="21"/>
<rectangle x1="10.83945" y1="10.67435" x2="11.52525" y2="10.68705" layer="21"/>
<rectangle x1="11.63955" y1="10.67435" x2="12.65555" y2="10.68705" layer="21"/>
<rectangle x1="12.75715" y1="10.67435" x2="12.88415" y2="10.68705" layer="21"/>
<rectangle x1="12.98575" y1="10.67435" x2="14.02715" y2="10.68705" layer="21"/>
<rectangle x1="14.11605" y1="10.67435" x2="14.81455" y2="10.68705" layer="21"/>
<rectangle x1="14.92885" y1="10.67435" x2="15.58925" y2="10.68705" layer="21"/>
<rectangle x1="15.67815" y1="10.67435" x2="17.32915" y2="10.68705" layer="21"/>
<rectangle x1="4.76885" y1="10.68705" x2="6.05155" y2="10.69975" layer="21"/>
<rectangle x1="6.15315" y1="10.68705" x2="6.26745" y2="10.69975" layer="21"/>
<rectangle x1="6.38175" y1="10.68705" x2="7.42315" y2="10.69975" layer="21"/>
<rectangle x1="7.51205" y1="10.68705" x2="8.27405" y2="10.69975" layer="21"/>
<rectangle x1="8.40105" y1="10.68705" x2="8.87095" y2="10.69975" layer="21"/>
<rectangle x1="8.99795" y1="10.68705" x2="9.79805" y2="10.69975" layer="21"/>
<rectangle x1="9.91235" y1="10.68705" x2="10.59815" y2="10.69975" layer="21"/>
<rectangle x1="10.71245" y1="10.68705" x2="10.72515" y2="10.69975" layer="21"/>
<rectangle x1="10.83945" y1="10.68705" x2="11.52525" y2="10.69975" layer="21"/>
<rectangle x1="11.63955" y1="10.68705" x2="12.65555" y2="10.69975" layer="21"/>
<rectangle x1="12.75715" y1="10.68705" x2="12.87145" y2="10.69975" layer="21"/>
<rectangle x1="12.98575" y1="10.68705" x2="14.02715" y2="10.69975" layer="21"/>
<rectangle x1="14.11605" y1="10.68705" x2="14.81455" y2="10.69975" layer="21"/>
<rectangle x1="14.92885" y1="10.68705" x2="15.58925" y2="10.69975" layer="21"/>
<rectangle x1="15.67815" y1="10.68705" x2="17.32915" y2="10.69975" layer="21"/>
<rectangle x1="4.76885" y1="10.69975" x2="6.06425" y2="10.71245" layer="21"/>
<rectangle x1="6.16585" y1="10.69975" x2="6.26745" y2="10.71245" layer="21"/>
<rectangle x1="6.36905" y1="10.69975" x2="7.42315" y2="10.71245" layer="21"/>
<rectangle x1="7.51205" y1="10.69975" x2="8.27405" y2="10.71245" layer="21"/>
<rectangle x1="8.40105" y1="10.69975" x2="8.88365" y2="10.71245" layer="21"/>
<rectangle x1="9.01065" y1="10.69975" x2="9.79805" y2="10.71245" layer="21"/>
<rectangle x1="9.91235" y1="10.69975" x2="10.59815" y2="10.71245" layer="21"/>
<rectangle x1="10.71245" y1="10.69975" x2="10.73785" y2="10.71245" layer="21"/>
<rectangle x1="10.85215" y1="10.69975" x2="11.53795" y2="10.71245" layer="21"/>
<rectangle x1="11.65225" y1="10.69975" x2="12.66825" y2="10.71245" layer="21"/>
<rectangle x1="12.76985" y1="10.69975" x2="12.87145" y2="10.71245" layer="21"/>
<rectangle x1="12.97305" y1="10.69975" x2="14.02715" y2="10.71245" layer="21"/>
<rectangle x1="14.11605" y1="10.69975" x2="14.80185" y2="10.71245" layer="21"/>
<rectangle x1="14.91615" y1="10.69975" x2="15.58925" y2="10.71245" layer="21"/>
<rectangle x1="15.67815" y1="10.69975" x2="17.32915" y2="10.71245" layer="21"/>
<rectangle x1="4.76885" y1="10.71245" x2="6.06425" y2="10.72515" layer="21"/>
<rectangle x1="6.16585" y1="10.71245" x2="6.25475" y2="10.72515" layer="21"/>
<rectangle x1="6.36905" y1="10.71245" x2="7.42315" y2="10.72515" layer="21"/>
<rectangle x1="7.51205" y1="10.71245" x2="8.26135" y2="10.72515" layer="21"/>
<rectangle x1="8.38835" y1="10.71245" x2="8.88365" y2="10.72515" layer="21"/>
<rectangle x1="9.01065" y1="10.71245" x2="9.78535" y2="10.72515" layer="21"/>
<rectangle x1="9.89965" y1="10.71245" x2="10.58545" y2="10.72515" layer="21"/>
<rectangle x1="10.69975" y1="10.71245" x2="10.73785" y2="10.72515" layer="21"/>
<rectangle x1="10.85215" y1="10.71245" x2="11.53795" y2="10.72515" layer="21"/>
<rectangle x1="11.65225" y1="10.71245" x2="12.66825" y2="10.72515" layer="21"/>
<rectangle x1="12.76985" y1="10.71245" x2="12.85875" y2="10.72515" layer="21"/>
<rectangle x1="12.97305" y1="10.71245" x2="14.02715" y2="10.72515" layer="21"/>
<rectangle x1="14.11605" y1="10.71245" x2="14.80185" y2="10.72515" layer="21"/>
<rectangle x1="14.91615" y1="10.71245" x2="15.58925" y2="10.72515" layer="21"/>
<rectangle x1="15.67815" y1="10.71245" x2="17.32915" y2="10.72515" layer="21"/>
<rectangle x1="4.76885" y1="10.72515" x2="6.07695" y2="10.73785" layer="21"/>
<rectangle x1="6.17855" y1="10.72515" x2="6.25475" y2="10.73785" layer="21"/>
<rectangle x1="6.35635" y1="10.72515" x2="7.42315" y2="10.73785" layer="21"/>
<rectangle x1="7.51205" y1="10.72515" x2="8.24865" y2="10.73785" layer="21"/>
<rectangle x1="8.37565" y1="10.72515" x2="8.89635" y2="10.73785" layer="21"/>
<rectangle x1="9.02335" y1="10.72515" x2="9.78535" y2="10.73785" layer="21"/>
<rectangle x1="9.89965" y1="10.72515" x2="10.58545" y2="10.73785" layer="21"/>
<rectangle x1="10.69975" y1="10.72515" x2="10.75055" y2="10.73785" layer="21"/>
<rectangle x1="10.85215" y1="10.72515" x2="11.55065" y2="10.73785" layer="21"/>
<rectangle x1="11.65225" y1="10.72515" x2="12.68095" y2="10.73785" layer="21"/>
<rectangle x1="12.78255" y1="10.72515" x2="12.85875" y2="10.73785" layer="21"/>
<rectangle x1="12.96035" y1="10.72515" x2="14.02715" y2="10.73785" layer="21"/>
<rectangle x1="14.11605" y1="10.72515" x2="14.78915" y2="10.73785" layer="21"/>
<rectangle x1="14.90345" y1="10.72515" x2="15.58925" y2="10.73785" layer="21"/>
<rectangle x1="15.67815" y1="10.72515" x2="17.32915" y2="10.73785" layer="21"/>
<rectangle x1="4.76885" y1="10.73785" x2="6.07695" y2="10.75055" layer="21"/>
<rectangle x1="6.17855" y1="10.73785" x2="6.24205" y2="10.75055" layer="21"/>
<rectangle x1="6.35635" y1="10.73785" x2="7.42315" y2="10.75055" layer="21"/>
<rectangle x1="7.51205" y1="10.73785" x2="8.24865" y2="10.75055" layer="21"/>
<rectangle x1="8.36295" y1="10.73785" x2="8.90905" y2="10.75055" layer="21"/>
<rectangle x1="9.03605" y1="10.73785" x2="9.78535" y2="10.75055" layer="21"/>
<rectangle x1="9.88695" y1="10.73785" x2="10.58545" y2="10.75055" layer="21"/>
<rectangle x1="10.68705" y1="10.73785" x2="10.75055" y2="10.75055" layer="21"/>
<rectangle x1="10.86485" y1="10.73785" x2="11.55065" y2="10.75055" layer="21"/>
<rectangle x1="11.66495" y1="10.73785" x2="12.68095" y2="10.75055" layer="21"/>
<rectangle x1="12.78255" y1="10.73785" x2="12.84605" y2="10.75055" layer="21"/>
<rectangle x1="12.96035" y1="10.73785" x2="14.02715" y2="10.75055" layer="21"/>
<rectangle x1="14.11605" y1="10.73785" x2="14.77645" y2="10.75055" layer="21"/>
<rectangle x1="14.90345" y1="10.73785" x2="15.58925" y2="10.75055" layer="21"/>
<rectangle x1="15.67815" y1="10.73785" x2="17.32915" y2="10.75055" layer="21"/>
<rectangle x1="4.76885" y1="10.75055" x2="6.08965" y2="10.76325" layer="21"/>
<rectangle x1="6.19125" y1="10.75055" x2="6.24205" y2="10.76325" layer="21"/>
<rectangle x1="6.34365" y1="10.75055" x2="7.42315" y2="10.76325" layer="21"/>
<rectangle x1="7.51205" y1="10.75055" x2="8.23595" y2="10.76325" layer="21"/>
<rectangle x1="8.36295" y1="10.75055" x2="8.92175" y2="10.76325" layer="21"/>
<rectangle x1="9.04875" y1="10.75055" x2="9.77265" y2="10.76325" layer="21"/>
<rectangle x1="9.88695" y1="10.75055" x2="10.57275" y2="10.76325" layer="21"/>
<rectangle x1="10.68705" y1="10.75055" x2="10.76325" y2="10.76325" layer="21"/>
<rectangle x1="10.86485" y1="10.75055" x2="11.56335" y2="10.76325" layer="21"/>
<rectangle x1="11.66495" y1="10.75055" x2="12.69365" y2="10.76325" layer="21"/>
<rectangle x1="12.79525" y1="10.75055" x2="12.84605" y2="10.76325" layer="21"/>
<rectangle x1="12.94765" y1="10.75055" x2="14.02715" y2="10.76325" layer="21"/>
<rectangle x1="14.11605" y1="10.75055" x2="14.76375" y2="10.76325" layer="21"/>
<rectangle x1="14.89075" y1="10.75055" x2="15.58925" y2="10.76325" layer="21"/>
<rectangle x1="15.67815" y1="10.75055" x2="17.32915" y2="10.76325" layer="21"/>
<rectangle x1="4.76885" y1="10.76325" x2="6.08965" y2="10.77595" layer="21"/>
<rectangle x1="6.19125" y1="10.76325" x2="6.24205" y2="10.77595" layer="21"/>
<rectangle x1="6.34365" y1="10.76325" x2="7.42315" y2="10.77595" layer="21"/>
<rectangle x1="7.51205" y1="10.76325" x2="8.22325" y2="10.77595" layer="21"/>
<rectangle x1="8.35025" y1="10.76325" x2="8.92175" y2="10.77595" layer="21"/>
<rectangle x1="9.04875" y1="10.76325" x2="9.77265" y2="10.77595" layer="21"/>
<rectangle x1="9.87425" y1="10.76325" x2="10.57275" y2="10.77595" layer="21"/>
<rectangle x1="10.67435" y1="10.76325" x2="10.76325" y2="10.77595" layer="21"/>
<rectangle x1="10.87755" y1="10.76325" x2="11.56335" y2="10.77595" layer="21"/>
<rectangle x1="11.67765" y1="10.76325" x2="12.69365" y2="10.77595" layer="21"/>
<rectangle x1="12.79525" y1="10.76325" x2="12.84605" y2="10.77595" layer="21"/>
<rectangle x1="12.94765" y1="10.76325" x2="14.02715" y2="10.77595" layer="21"/>
<rectangle x1="14.11605" y1="10.76325" x2="14.73835" y2="10.77595" layer="21"/>
<rectangle x1="14.87805" y1="10.76325" x2="15.58925" y2="10.77595" layer="21"/>
<rectangle x1="15.67815" y1="10.76325" x2="17.32915" y2="10.77595" layer="21"/>
<rectangle x1="4.76885" y1="10.77595" x2="6.10235" y2="10.78865" layer="21"/>
<rectangle x1="6.20395" y1="10.77595" x2="6.22935" y2="10.78865" layer="21"/>
<rectangle x1="6.33095" y1="10.77595" x2="7.42315" y2="10.78865" layer="21"/>
<rectangle x1="7.51205" y1="10.77595" x2="8.21055" y2="10.78865" layer="21"/>
<rectangle x1="8.33755" y1="10.77595" x2="8.93445" y2="10.78865" layer="21"/>
<rectangle x1="9.06145" y1="10.77595" x2="9.75995" y2="10.78865" layer="21"/>
<rectangle x1="9.87425" y1="10.77595" x2="10.56005" y2="10.78865" layer="21"/>
<rectangle x1="10.67435" y1="10.77595" x2="10.77595" y2="10.78865" layer="21"/>
<rectangle x1="10.87755" y1="10.77595" x2="11.57605" y2="10.78865" layer="21"/>
<rectangle x1="11.67765" y1="10.77595" x2="12.70635" y2="10.78865" layer="21"/>
<rectangle x1="12.80795" y1="10.77595" x2="12.83335" y2="10.78865" layer="21"/>
<rectangle x1="12.93495" y1="10.77595" x2="14.02715" y2="10.78865" layer="21"/>
<rectangle x1="14.11605" y1="10.77595" x2="14.72565" y2="10.78865" layer="21"/>
<rectangle x1="14.87805" y1="10.77595" x2="15.58925" y2="10.78865" layer="21"/>
<rectangle x1="15.67815" y1="10.77595" x2="17.32915" y2="10.78865" layer="21"/>
<rectangle x1="4.76885" y1="10.78865" x2="6.10235" y2="10.80135" layer="21"/>
<rectangle x1="6.20395" y1="10.78865" x2="6.22935" y2="10.80135" layer="21"/>
<rectangle x1="6.33095" y1="10.78865" x2="7.42315" y2="10.80135" layer="21"/>
<rectangle x1="7.51205" y1="10.78865" x2="8.21055" y2="10.80135" layer="21"/>
<rectangle x1="8.32485" y1="10.78865" x2="8.94715" y2="10.80135" layer="21"/>
<rectangle x1="9.07415" y1="10.78865" x2="9.75995" y2="10.80135" layer="21"/>
<rectangle x1="9.86155" y1="10.78865" x2="10.56005" y2="10.80135" layer="21"/>
<rectangle x1="10.66165" y1="10.78865" x2="10.77595" y2="10.80135" layer="21"/>
<rectangle x1="10.89025" y1="10.78865" x2="11.57605" y2="10.80135" layer="21"/>
<rectangle x1="11.69035" y1="10.78865" x2="12.70635" y2="10.80135" layer="21"/>
<rectangle x1="12.80795" y1="10.78865" x2="12.83335" y2="10.80135" layer="21"/>
<rectangle x1="12.93495" y1="10.78865" x2="14.02715" y2="10.80135" layer="21"/>
<rectangle x1="14.11605" y1="10.78865" x2="14.70025" y2="10.80135" layer="21"/>
<rectangle x1="14.86535" y1="10.78865" x2="15.58925" y2="10.80135" layer="21"/>
<rectangle x1="15.67815" y1="10.78865" x2="17.32915" y2="10.80135" layer="21"/>
<rectangle x1="4.76885" y1="10.80135" x2="6.10235" y2="10.81405" layer="21"/>
<rectangle x1="6.31825" y1="10.80135" x2="7.42315" y2="10.81405" layer="21"/>
<rectangle x1="7.51205" y1="10.80135" x2="8.19785" y2="10.81405" layer="21"/>
<rectangle x1="8.32485" y1="10.80135" x2="8.95985" y2="10.81405" layer="21"/>
<rectangle x1="9.08685" y1="10.80135" x2="9.74725" y2="10.81405" layer="21"/>
<rectangle x1="9.86155" y1="10.80135" x2="10.54735" y2="10.81405" layer="21"/>
<rectangle x1="10.66165" y1="10.80135" x2="10.78865" y2="10.81405" layer="21"/>
<rectangle x1="10.89025" y1="10.80135" x2="11.58875" y2="10.81405" layer="21"/>
<rectangle x1="11.69035" y1="10.80135" x2="12.70635" y2="10.81405" layer="21"/>
<rectangle x1="12.92225" y1="10.80135" x2="14.02715" y2="10.81405" layer="21"/>
<rectangle x1="14.11605" y1="10.80135" x2="14.67485" y2="10.81405" layer="21"/>
<rectangle x1="14.85265" y1="10.80135" x2="15.58925" y2="10.81405" layer="21"/>
<rectangle x1="15.67815" y1="10.80135" x2="17.32915" y2="10.81405" layer="21"/>
<rectangle x1="4.76885" y1="10.81405" x2="6.11505" y2="10.82675" layer="21"/>
<rectangle x1="6.31825" y1="10.81405" x2="7.42315" y2="10.82675" layer="21"/>
<rectangle x1="7.51205" y1="10.81405" x2="8.18515" y2="10.82675" layer="21"/>
<rectangle x1="8.31215" y1="10.81405" x2="8.95985" y2="10.82675" layer="21"/>
<rectangle x1="9.08685" y1="10.81405" x2="9.74725" y2="10.82675" layer="21"/>
<rectangle x1="9.84885" y1="10.81405" x2="10.54735" y2="10.82675" layer="21"/>
<rectangle x1="10.64895" y1="10.81405" x2="10.78865" y2="10.82675" layer="21"/>
<rectangle x1="10.90295" y1="10.81405" x2="11.58875" y2="10.82675" layer="21"/>
<rectangle x1="11.70305" y1="10.81405" x2="12.71905" y2="10.82675" layer="21"/>
<rectangle x1="12.92225" y1="10.81405" x2="14.02715" y2="10.82675" layer="21"/>
<rectangle x1="14.11605" y1="10.81405" x2="14.64945" y2="10.82675" layer="21"/>
<rectangle x1="14.83995" y1="10.81405" x2="15.58925" y2="10.82675" layer="21"/>
<rectangle x1="15.67815" y1="10.81405" x2="17.32915" y2="10.82675" layer="21"/>
<rectangle x1="4.76885" y1="10.82675" x2="6.11505" y2="10.83945" layer="21"/>
<rectangle x1="6.31825" y1="10.82675" x2="7.42315" y2="10.83945" layer="21"/>
<rectangle x1="7.51205" y1="10.82675" x2="8.17245" y2="10.83945" layer="21"/>
<rectangle x1="8.29945" y1="10.82675" x2="8.97255" y2="10.83945" layer="21"/>
<rectangle x1="9.09955" y1="10.82675" x2="9.73455" y2="10.83945" layer="21"/>
<rectangle x1="9.84885" y1="10.82675" x2="10.53465" y2="10.83945" layer="21"/>
<rectangle x1="10.64895" y1="10.82675" x2="10.78865" y2="10.83945" layer="21"/>
<rectangle x1="10.90295" y1="10.82675" x2="11.58875" y2="10.83945" layer="21"/>
<rectangle x1="11.70305" y1="10.82675" x2="12.71905" y2="10.83945" layer="21"/>
<rectangle x1="12.92225" y1="10.82675" x2="14.02715" y2="10.83945" layer="21"/>
<rectangle x1="14.11605" y1="10.82675" x2="14.61135" y2="10.83945" layer="21"/>
<rectangle x1="14.82725" y1="10.82675" x2="15.58925" y2="10.83945" layer="21"/>
<rectangle x1="15.67815" y1="10.82675" x2="17.32915" y2="10.83945" layer="21"/>
<rectangle x1="4.76885" y1="10.83945" x2="6.12775" y2="10.85215" layer="21"/>
<rectangle x1="6.30555" y1="10.83945" x2="7.42315" y2="10.85215" layer="21"/>
<rectangle x1="7.51205" y1="10.83945" x2="8.17245" y2="10.85215" layer="21"/>
<rectangle x1="8.28675" y1="10.83945" x2="8.98525" y2="10.85215" layer="21"/>
<rectangle x1="9.11225" y1="10.83945" x2="9.73455" y2="10.85215" layer="21"/>
<rectangle x1="9.84885" y1="10.83945" x2="10.53465" y2="10.85215" layer="21"/>
<rectangle x1="10.64895" y1="10.83945" x2="10.80135" y2="10.85215" layer="21"/>
<rectangle x1="10.91565" y1="10.83945" x2="11.60145" y2="10.85215" layer="21"/>
<rectangle x1="11.71575" y1="10.83945" x2="12.73175" y2="10.85215" layer="21"/>
<rectangle x1="12.90955" y1="10.83945" x2="14.02715" y2="10.85215" layer="21"/>
<rectangle x1="14.11605" y1="10.83945" x2="14.53515" y2="10.85215" layer="21"/>
<rectangle x1="14.80185" y1="10.83945" x2="15.58925" y2="10.85215" layer="21"/>
<rectangle x1="15.67815" y1="10.83945" x2="17.32915" y2="10.85215" layer="21"/>
<rectangle x1="4.76885" y1="10.85215" x2="6.12775" y2="10.86485" layer="21"/>
<rectangle x1="6.30555" y1="10.85215" x2="7.42315" y2="10.86485" layer="21"/>
<rectangle x1="7.51205" y1="10.85215" x2="8.15975" y2="10.86485" layer="21"/>
<rectangle x1="8.28675" y1="10.85215" x2="8.99795" y2="10.86485" layer="21"/>
<rectangle x1="9.12495" y1="10.85215" x2="9.72185" y2="10.86485" layer="21"/>
<rectangle x1="9.83615" y1="10.85215" x2="10.52195" y2="10.86485" layer="21"/>
<rectangle x1="10.63625" y1="10.85215" x2="10.80135" y2="10.86485" layer="21"/>
<rectangle x1="10.91565" y1="10.85215" x2="11.60145" y2="10.86485" layer="21"/>
<rectangle x1="11.71575" y1="10.85215" x2="12.73175" y2="10.86485" layer="21"/>
<rectangle x1="12.90955" y1="10.85215" x2="14.02715" y2="10.86485" layer="21"/>
<rectangle x1="14.78915" y1="10.85215" x2="15.58925" y2="10.86485" layer="21"/>
<rectangle x1="16.45285" y1="10.85215" x2="17.32915" y2="10.86485" layer="21"/>
<rectangle x1="4.76885" y1="10.86485" x2="6.14045" y2="10.87755" layer="21"/>
<rectangle x1="6.29285" y1="10.86485" x2="7.42315" y2="10.87755" layer="21"/>
<rectangle x1="7.51205" y1="10.86485" x2="8.14705" y2="10.87755" layer="21"/>
<rectangle x1="8.27405" y1="10.86485" x2="8.99795" y2="10.87755" layer="21"/>
<rectangle x1="9.12495" y1="10.86485" x2="9.72185" y2="10.87755" layer="21"/>
<rectangle x1="9.83615" y1="10.86485" x2="10.52195" y2="10.87755" layer="21"/>
<rectangle x1="10.63625" y1="10.86485" x2="10.81405" y2="10.87755" layer="21"/>
<rectangle x1="10.92835" y1="10.86485" x2="11.61415" y2="10.87755" layer="21"/>
<rectangle x1="11.72845" y1="10.86485" x2="12.74445" y2="10.87755" layer="21"/>
<rectangle x1="12.89685" y1="10.86485" x2="14.02715" y2="10.87755" layer="21"/>
<rectangle x1="14.76375" y1="10.86485" x2="15.58925" y2="10.87755" layer="21"/>
<rectangle x1="16.45285" y1="10.86485" x2="17.32915" y2="10.87755" layer="21"/>
<rectangle x1="4.76885" y1="10.87755" x2="6.14045" y2="10.89025" layer="21"/>
<rectangle x1="6.29285" y1="10.87755" x2="7.42315" y2="10.89025" layer="21"/>
<rectangle x1="7.51205" y1="10.87755" x2="8.13435" y2="10.89025" layer="21"/>
<rectangle x1="8.26135" y1="10.87755" x2="9.01065" y2="10.89025" layer="21"/>
<rectangle x1="9.13765" y1="10.87755" x2="9.70915" y2="10.89025" layer="21"/>
<rectangle x1="9.82345" y1="10.87755" x2="10.50925" y2="10.89025" layer="21"/>
<rectangle x1="10.62355" y1="10.87755" x2="10.81405" y2="10.89025" layer="21"/>
<rectangle x1="10.92835" y1="10.87755" x2="11.61415" y2="10.89025" layer="21"/>
<rectangle x1="11.72845" y1="10.87755" x2="12.74445" y2="10.89025" layer="21"/>
<rectangle x1="12.89685" y1="10.87755" x2="14.02715" y2="10.89025" layer="21"/>
<rectangle x1="14.73835" y1="10.87755" x2="15.58925" y2="10.89025" layer="21"/>
<rectangle x1="16.45285" y1="10.87755" x2="17.32915" y2="10.89025" layer="21"/>
<rectangle x1="4.76885" y1="10.89025" x2="6.15315" y2="10.90295" layer="21"/>
<rectangle x1="6.28015" y1="10.89025" x2="7.42315" y2="10.90295" layer="21"/>
<rectangle x1="7.51205" y1="10.89025" x2="8.13435" y2="10.90295" layer="21"/>
<rectangle x1="8.24865" y1="10.89025" x2="9.02335" y2="10.90295" layer="21"/>
<rectangle x1="9.15035" y1="10.89025" x2="9.70915" y2="10.90295" layer="21"/>
<rectangle x1="9.82345" y1="10.89025" x2="10.50925" y2="10.90295" layer="21"/>
<rectangle x1="10.62355" y1="10.89025" x2="10.82675" y2="10.90295" layer="21"/>
<rectangle x1="10.92835" y1="10.89025" x2="11.62685" y2="10.90295" layer="21"/>
<rectangle x1="11.72845" y1="10.89025" x2="12.75715" y2="10.90295" layer="21"/>
<rectangle x1="12.88415" y1="10.89025" x2="14.02715" y2="10.90295" layer="21"/>
<rectangle x1="14.71295" y1="10.89025" x2="15.58925" y2="10.90295" layer="21"/>
<rectangle x1="16.45285" y1="10.89025" x2="17.32915" y2="10.90295" layer="21"/>
<rectangle x1="4.76885" y1="10.90295" x2="6.15315" y2="10.91565" layer="21"/>
<rectangle x1="6.28015" y1="10.90295" x2="7.42315" y2="10.91565" layer="21"/>
<rectangle x1="7.51205" y1="10.90295" x2="8.12165" y2="10.91565" layer="21"/>
<rectangle x1="8.24865" y1="10.90295" x2="9.03605" y2="10.91565" layer="21"/>
<rectangle x1="9.16305" y1="10.90295" x2="9.70915" y2="10.91565" layer="21"/>
<rectangle x1="9.81075" y1="10.90295" x2="10.50925" y2="10.91565" layer="21"/>
<rectangle x1="10.61085" y1="10.90295" x2="10.82675" y2="10.91565" layer="21"/>
<rectangle x1="10.94105" y1="10.90295" x2="11.62685" y2="10.91565" layer="21"/>
<rectangle x1="11.74115" y1="10.90295" x2="12.75715" y2="10.91565" layer="21"/>
<rectangle x1="12.88415" y1="10.90295" x2="14.02715" y2="10.91565" layer="21"/>
<rectangle x1="14.67485" y1="10.90295" x2="15.58925" y2="10.91565" layer="21"/>
<rectangle x1="16.45285" y1="10.90295" x2="17.32915" y2="10.91565" layer="21"/>
<rectangle x1="4.76885" y1="10.91565" x2="6.16585" y2="10.92835" layer="21"/>
<rectangle x1="6.26745" y1="10.91565" x2="7.42315" y2="10.92835" layer="21"/>
<rectangle x1="7.51205" y1="10.91565" x2="8.10895" y2="10.92835" layer="21"/>
<rectangle x1="8.23595" y1="10.91565" x2="9.03605" y2="10.92835" layer="21"/>
<rectangle x1="9.16305" y1="10.91565" x2="9.69645" y2="10.92835" layer="21"/>
<rectangle x1="9.81075" y1="10.91565" x2="10.49655" y2="10.92835" layer="21"/>
<rectangle x1="10.61085" y1="10.91565" x2="10.83945" y2="10.92835" layer="21"/>
<rectangle x1="10.94105" y1="10.91565" x2="11.63955" y2="10.92835" layer="21"/>
<rectangle x1="11.74115" y1="10.91565" x2="12.76985" y2="10.92835" layer="21"/>
<rectangle x1="12.87145" y1="10.91565" x2="14.02715" y2="10.92835" layer="21"/>
<rectangle x1="14.62405" y1="10.91565" x2="15.58925" y2="10.92835" layer="21"/>
<rectangle x1="16.45285" y1="10.91565" x2="17.32915" y2="10.92835" layer="21"/>
<rectangle x1="4.76885" y1="10.92835" x2="6.16585" y2="10.94105" layer="21"/>
<rectangle x1="6.26745" y1="10.92835" x2="7.42315" y2="10.94105" layer="21"/>
<rectangle x1="7.51205" y1="10.92835" x2="8.10895" y2="10.94105" layer="21"/>
<rectangle x1="8.22325" y1="10.92835" x2="9.04875" y2="10.94105" layer="21"/>
<rectangle x1="9.17575" y1="10.92835" x2="9.69645" y2="10.94105" layer="21"/>
<rectangle x1="9.79805" y1="10.92835" x2="10.49655" y2="10.94105" layer="21"/>
<rectangle x1="10.59815" y1="10.92835" x2="10.83945" y2="10.94105" layer="21"/>
<rectangle x1="10.94105" y1="10.92835" x2="11.63955" y2="10.94105" layer="21"/>
<rectangle x1="11.74115" y1="10.92835" x2="12.76985" y2="10.94105" layer="21"/>
<rectangle x1="12.87145" y1="10.92835" x2="14.02715" y2="10.94105" layer="21"/>
<rectangle x1="14.54785" y1="10.92835" x2="15.58925" y2="10.94105" layer="21"/>
<rectangle x1="16.44015" y1="10.92835" x2="17.32915" y2="10.94105" layer="21"/>
<rectangle x1="4.76885" y1="10.94105" x2="17.32915" y2="10.95375" layer="21"/>
<rectangle x1="4.76885" y1="10.95375" x2="17.32915" y2="10.96645" layer="21"/>
<rectangle x1="4.76885" y1="10.96645" x2="17.32915" y2="10.97915" layer="21"/>
<rectangle x1="4.76885" y1="10.97915" x2="17.32915" y2="10.99185" layer="21"/>
<rectangle x1="4.76885" y1="10.99185" x2="17.32915" y2="11.00455" layer="21"/>
<rectangle x1="4.76885" y1="11.00455" x2="17.32915" y2="11.01725" layer="21"/>
<rectangle x1="4.76885" y1="11.01725" x2="17.32915" y2="11.02995" layer="21"/>
<rectangle x1="4.76885" y1="11.02995" x2="17.32915" y2="11.04265" layer="21"/>
<rectangle x1="4.76885" y1="11.04265" x2="17.32915" y2="11.05535" layer="21"/>
<rectangle x1="4.76885" y1="11.05535" x2="17.32915" y2="11.06805" layer="21"/>
<rectangle x1="4.76885" y1="11.06805" x2="17.32915" y2="11.08075" layer="21"/>
<rectangle x1="4.76885" y1="11.08075" x2="17.32915" y2="11.09345" layer="21"/>
<rectangle x1="4.76885" y1="11.09345" x2="17.32915" y2="11.10615" layer="21"/>
<rectangle x1="4.76885" y1="11.10615" x2="17.32915" y2="11.11885" layer="21"/>
<rectangle x1="4.76885" y1="11.11885" x2="17.32915" y2="11.13155" layer="21"/>
<rectangle x1="4.76885" y1="11.13155" x2="17.32915" y2="11.14425" layer="21"/>
<rectangle x1="4.76885" y1="11.14425" x2="17.32915" y2="11.15695" layer="21"/>
<rectangle x1="4.76885" y1="11.15695" x2="17.32915" y2="11.16965" layer="21"/>
<rectangle x1="4.76885" y1="11.16965" x2="17.32915" y2="11.18235" layer="21"/>
<rectangle x1="4.76885" y1="11.18235" x2="17.32915" y2="11.19505" layer="21"/>
<rectangle x1="4.76885" y1="11.19505" x2="17.32915" y2="11.20775" layer="21"/>
<rectangle x1="4.76885" y1="11.20775" x2="17.32915" y2="11.22045" layer="21"/>
<rectangle x1="4.76885" y1="11.22045" x2="17.32915" y2="11.23315" layer="21"/>
<rectangle x1="4.76885" y1="11.23315" x2="17.32915" y2="11.24585" layer="21"/>
<rectangle x1="4.76885" y1="11.24585" x2="17.32915" y2="11.25855" layer="21"/>
<rectangle x1="4.76885" y1="11.25855" x2="17.32915" y2="11.27125" layer="21"/>
<rectangle x1="4.76885" y1="11.27125" x2="17.32915" y2="11.28395" layer="21"/>
<rectangle x1="4.76885" y1="11.28395" x2="17.32915" y2="11.29665" layer="21"/>
<rectangle x1="4.76885" y1="11.29665" x2="17.32915" y2="11.30935" layer="21"/>
<rectangle x1="4.76885" y1="11.30935" x2="17.32915" y2="11.32205" layer="21"/>
<rectangle x1="4.76885" y1="11.32205" x2="17.32915" y2="11.33475" layer="21"/>
<rectangle x1="4.76885" y1="11.33475" x2="17.32915" y2="11.34745" layer="21"/>
<rectangle x1="4.76885" y1="11.34745" x2="17.32915" y2="11.36015" layer="21"/>
<rectangle x1="4.76885" y1="11.36015" x2="17.32915" y2="11.37285" layer="21"/>
<rectangle x1="4.76885" y1="11.37285" x2="17.32915" y2="11.38555" layer="21"/>
<rectangle x1="4.76885" y1="11.38555" x2="17.32915" y2="11.39825" layer="21"/>
<rectangle x1="4.76885" y1="11.39825" x2="17.32915" y2="11.41095" layer="21"/>
<rectangle x1="4.76885" y1="11.41095" x2="17.32915" y2="11.42365" layer="21"/>
<rectangle x1="4.76885" y1="11.42365" x2="17.32915" y2="11.43635" layer="21"/>
<rectangle x1="4.76885" y1="11.43635" x2="17.32915" y2="11.44905" layer="21"/>
<rectangle x1="4.76885" y1="11.44905" x2="17.32915" y2="11.46175" layer="21"/>
<rectangle x1="4.76885" y1="11.46175" x2="17.32915" y2="11.47445" layer="21"/>
<rectangle x1="4.76885" y1="11.47445" x2="17.32915" y2="11.48715" layer="21"/>
<rectangle x1="4.76885" y1="11.48715" x2="17.32915" y2="11.49985" layer="21"/>
<rectangle x1="4.76885" y1="11.49985" x2="17.32915" y2="11.51255" layer="21"/>
<rectangle x1="4.76885" y1="11.51255" x2="17.32915" y2="11.52525" layer="21"/>
<rectangle x1="4.76885" y1="11.52525" x2="17.32915" y2="11.53795" layer="21"/>
<rectangle x1="4.76885" y1="11.53795" x2="17.32915" y2="11.55065" layer="21"/>
<rectangle x1="4.76885" y1="11.55065" x2="17.32915" y2="11.56335" layer="21"/>
<rectangle x1="4.76885" y1="11.56335" x2="17.32915" y2="11.57605" layer="21"/>
<rectangle x1="4.76885" y1="11.57605" x2="17.32915" y2="11.58875" layer="21"/>
<rectangle x1="4.76885" y1="11.58875" x2="17.32915" y2="11.60145" layer="21"/>
<rectangle x1="4.76885" y1="11.60145" x2="17.32915" y2="11.61415" layer="21"/>
<rectangle x1="4.76885" y1="11.61415" x2="17.32915" y2="11.62685" layer="21"/>
<rectangle x1="4.76885" y1="11.62685" x2="17.32915" y2="11.63955" layer="21"/>
<rectangle x1="4.76885" y1="11.63955" x2="17.32915" y2="11.65225" layer="21"/>
<rectangle x1="4.76885" y1="11.65225" x2="17.32915" y2="11.66495" layer="21"/>
<rectangle x1="4.76885" y1="11.66495" x2="17.32915" y2="11.67765" layer="21"/>
<rectangle x1="4.76885" y1="11.67765" x2="17.32915" y2="11.69035" layer="21"/>
<rectangle x1="4.76885" y1="11.69035" x2="17.32915" y2="11.70305" layer="21"/>
<rectangle x1="4.76885" y1="11.70305" x2="17.32915" y2="11.71575" layer="21"/>
<rectangle x1="4.76885" y1="11.71575" x2="17.32915" y2="11.72845" layer="21"/>
<rectangle x1="4.76885" y1="11.72845" x2="17.32915" y2="11.74115" layer="21"/>
<rectangle x1="4.76885" y1="11.74115" x2="17.32915" y2="11.75385" layer="21"/>
<rectangle x1="4.76885" y1="11.75385" x2="17.32915" y2="11.76655" layer="21"/>
<rectangle x1="4.76885" y1="11.76655" x2="17.32915" y2="11.77925" layer="21"/>
<rectangle x1="4.76885" y1="11.77925" x2="17.32915" y2="11.79195" layer="21"/>
<rectangle x1="4.76885" y1="11.79195" x2="17.32915" y2="11.80465" layer="21"/>
<rectangle x1="4.76885" y1="11.80465" x2="17.32915" y2="11.81735" layer="21"/>
<rectangle x1="4.76885" y1="11.81735" x2="17.32915" y2="11.83005" layer="21"/>
<rectangle x1="4.76885" y1="11.83005" x2="17.32915" y2="11.84275" layer="21"/>
<rectangle x1="4.76885" y1="11.84275" x2="17.32915" y2="11.85545" layer="21"/>
<rectangle x1="4.76885" y1="11.85545" x2="17.32915" y2="11.86815" layer="21"/>
<rectangle x1="4.76885" y1="11.86815" x2="17.32915" y2="11.88085" layer="21"/>
<rectangle x1="4.76885" y1="11.88085" x2="17.32915" y2="11.89355" layer="21"/>
<rectangle x1="4.76885" y1="11.89355" x2="17.32915" y2="11.90625" layer="21"/>
<rectangle x1="4.76885" y1="11.90625" x2="17.32915" y2="11.91895" layer="21"/>
<rectangle x1="4.76885" y1="11.91895" x2="17.32915" y2="11.93165" layer="21"/>
<rectangle x1="4.76885" y1="11.93165" x2="17.32915" y2="11.94435" layer="21"/>
<rectangle x1="4.76885" y1="11.94435" x2="17.32915" y2="11.95705" layer="21"/>
<rectangle x1="4.76885" y1="11.95705" x2="17.32915" y2="11.96975" layer="21"/>
<rectangle x1="4.76885" y1="11.96975" x2="17.32915" y2="11.98245" layer="21"/>
<rectangle x1="4.76885" y1="11.98245" x2="17.32915" y2="11.99515" layer="21"/>
<rectangle x1="4.76885" y1="11.99515" x2="17.32915" y2="12.00785" layer="21"/>
<rectangle x1="4.76885" y1="12.00785" x2="17.32915" y2="12.02055" layer="21"/>
<rectangle x1="4.76885" y1="12.02055" x2="17.32915" y2="12.03325" layer="21"/>
<rectangle x1="4.76885" y1="12.03325" x2="17.32915" y2="12.04595" layer="21"/>
<rectangle x1="4.76885" y1="12.04595" x2="17.32915" y2="12.05865" layer="21"/>
<rectangle x1="4.76885" y1="12.05865" x2="17.32915" y2="12.07135" layer="21"/>
<rectangle x1="4.76885" y1="12.07135" x2="17.32915" y2="12.08405" layer="21"/>
<rectangle x1="4.76885" y1="12.08405" x2="17.32915" y2="12.09675" layer="21"/>
<rectangle x1="4.76885" y1="12.09675" x2="17.32915" y2="12.10945" layer="21"/>
<rectangle x1="4.76885" y1="12.10945" x2="17.32915" y2="12.12215" layer="21"/>
<rectangle x1="4.76885" y1="12.12215" x2="17.32915" y2="12.13485" layer="21"/>
<rectangle x1="4.76885" y1="12.13485" x2="17.32915" y2="12.14755" layer="21"/>
<rectangle x1="4.76885" y1="12.14755" x2="17.32915" y2="12.16025" layer="21"/>
<rectangle x1="4.76885" y1="12.16025" x2="17.32915" y2="12.17295" layer="21"/>
<rectangle x1="4.76885" y1="12.17295" x2="17.32915" y2="12.18565" layer="21"/>
<rectangle x1="4.76885" y1="12.18565" x2="17.32915" y2="12.19835" layer="21"/>
<rectangle x1="4.76885" y1="12.19835" x2="17.32915" y2="12.21105" layer="21"/>
<rectangle x1="4.76885" y1="12.21105" x2="17.32915" y2="12.22375" layer="21"/>
<rectangle x1="4.76885" y1="12.22375" x2="17.32915" y2="12.23645" layer="21"/>
<rectangle x1="4.76885" y1="12.23645" x2="17.32915" y2="12.24915" layer="21"/>
<rectangle x1="4.76885" y1="12.24915" x2="17.32915" y2="12.26185" layer="21"/>
<rectangle x1="4.76885" y1="12.26185" x2="17.32915" y2="12.27455" layer="21"/>
<rectangle x1="4.76885" y1="12.27455" x2="17.32915" y2="12.28725" layer="21"/>
<rectangle x1="4.76885" y1="12.28725" x2="17.32915" y2="12.29995" layer="21"/>
<rectangle x1="4.76885" y1="12.29995" x2="17.32915" y2="12.31265" layer="21"/>
<rectangle x1="4.76885" y1="12.31265" x2="17.32915" y2="12.32535" layer="21"/>
<rectangle x1="4.76885" y1="12.32535" x2="17.32915" y2="12.33805" layer="21"/>
<rectangle x1="4.76885" y1="12.33805" x2="17.32915" y2="12.35075" layer="21"/>
<rectangle x1="4.76885" y1="12.35075" x2="17.32915" y2="12.36345" layer="21"/>
<rectangle x1="4.76885" y1="12.36345" x2="17.32915" y2="12.37615" layer="21"/>
<rectangle x1="4.76885" y1="12.37615" x2="17.32915" y2="12.38885" layer="21"/>
<rectangle x1="4.76885" y1="12.38885" x2="17.32915" y2="12.40155" layer="21"/>
<rectangle x1="4.76885" y1="12.40155" x2="17.32915" y2="12.41425" layer="21"/>
<rectangle x1="4.76885" y1="12.41425" x2="17.32915" y2="12.42695" layer="21"/>
<rectangle x1="4.76885" y1="12.42695" x2="17.32915" y2="12.43965" layer="21"/>
<rectangle x1="4.76885" y1="12.43965" x2="17.32915" y2="12.45235" layer="21"/>
<rectangle x1="4.76885" y1="12.45235" x2="17.32915" y2="12.46505" layer="21"/>
<rectangle x1="4.76885" y1="12.46505" x2="17.32915" y2="12.47775" layer="21"/>
<rectangle x1="4.79425" y1="12.47775" x2="17.30375" y2="12.49045" layer="21"/>
<rectangle x1="4.81965" y1="12.49045" x2="17.27835" y2="12.50315" layer="21"/>
<rectangle x1="4.84505" y1="12.50315" x2="17.25295" y2="12.51585" layer="21"/>
<rectangle x1="4.87045" y1="12.51585" x2="17.22755" y2="12.52855" layer="21"/>
<rectangle x1="4.89585" y1="12.52855" x2="17.20215" y2="12.54125" layer="21"/>
<rectangle x1="4.92125" y1="12.54125" x2="17.17675" y2="12.55395" layer="21"/>
<rectangle x1="4.94665" y1="12.55395" x2="17.15135" y2="12.56665" layer="21"/>
<rectangle x1="4.97205" y1="12.56665" x2="17.12595" y2="12.57935" layer="21"/>
<rectangle x1="4.99745" y1="12.57935" x2="17.10055" y2="12.59205" layer="21"/>
<rectangle x1="5.02285" y1="12.59205" x2="17.07515" y2="12.60475" layer="21"/>
<rectangle x1="5.04825" y1="12.60475" x2="17.04975" y2="12.61745" layer="21"/>
<rectangle x1="5.07365" y1="12.61745" x2="17.02435" y2="12.63015" layer="21"/>
<rectangle x1="5.09905" y1="12.63015" x2="16.99895" y2="12.64285" layer="21"/>
<rectangle x1="5.12445" y1="12.64285" x2="16.97355" y2="12.65555" layer="21"/>
<rectangle x1="5.14985" y1="12.65555" x2="16.94815" y2="12.66825" layer="21"/>
<rectangle x1="5.17525" y1="12.66825" x2="16.92275" y2="12.68095" layer="21"/>
<rectangle x1="5.20065" y1="12.68095" x2="16.89735" y2="12.69365" layer="21"/>
<rectangle x1="5.22605" y1="12.69365" x2="16.87195" y2="12.70635" layer="21"/>
<rectangle x1="5.25145" y1="12.70635" x2="16.84655" y2="12.71905" layer="21"/>
<rectangle x1="5.27685" y1="12.71905" x2="16.82115" y2="12.73175" layer="21"/>
<rectangle x1="5.30225" y1="12.73175" x2="16.79575" y2="12.74445" layer="21"/>
<rectangle x1="5.32765" y1="12.74445" x2="16.77035" y2="12.75715" layer="21"/>
<rectangle x1="5.35305" y1="12.75715" x2="16.74495" y2="12.76985" layer="21"/>
<rectangle x1="5.37845" y1="12.76985" x2="16.71955" y2="12.78255" layer="21"/>
<rectangle x1="5.40385" y1="12.78255" x2="16.69415" y2="12.79525" layer="21"/>
<rectangle x1="5.42925" y1="12.79525" x2="16.66875" y2="12.80795" layer="21"/>
<rectangle x1="5.45465" y1="12.80795" x2="16.64335" y2="12.82065" layer="21"/>
<rectangle x1="5.48005" y1="12.82065" x2="16.61795" y2="12.83335" layer="21"/>
<rectangle x1="5.50545" y1="12.83335" x2="16.59255" y2="12.84605" layer="21"/>
<rectangle x1="5.53085" y1="12.84605" x2="16.56715" y2="12.85875" layer="21"/>
<rectangle x1="5.55625" y1="12.85875" x2="16.54175" y2="12.87145" layer="21"/>
<rectangle x1="5.58165" y1="12.87145" x2="16.51635" y2="12.88415" layer="21"/>
<rectangle x1="5.60705" y1="12.88415" x2="16.49095" y2="12.89685" layer="21"/>
<rectangle x1="5.63245" y1="12.89685" x2="16.46555" y2="12.90955" layer="21"/>
<rectangle x1="5.65785" y1="12.90955" x2="16.44015" y2="12.92225" layer="21"/>
<rectangle x1="5.68325" y1="12.92225" x2="16.41475" y2="12.93495" layer="21"/>
<rectangle x1="5.70865" y1="12.93495" x2="16.38935" y2="12.94765" layer="21"/>
<rectangle x1="5.73405" y1="12.94765" x2="16.36395" y2="12.96035" layer="21"/>
<rectangle x1="5.75945" y1="12.96035" x2="16.33855" y2="12.97305" layer="21"/>
<rectangle x1="5.78485" y1="12.97305" x2="16.31315" y2="12.98575" layer="21"/>
<rectangle x1="5.81025" y1="12.98575" x2="16.28775" y2="12.99845" layer="21"/>
<rectangle x1="5.83565" y1="12.99845" x2="16.26235" y2="13.01115" layer="21"/>
<rectangle x1="5.86105" y1="13.01115" x2="16.23695" y2="13.02385" layer="21"/>
<rectangle x1="5.88645" y1="13.02385" x2="16.21155" y2="13.03655" layer="21"/>
<rectangle x1="5.91185" y1="13.03655" x2="16.18615" y2="13.04925" layer="21"/>
<rectangle x1="5.93725" y1="13.04925" x2="16.16075" y2="13.06195" layer="21"/>
<rectangle x1="5.96265" y1="13.06195" x2="16.13535" y2="13.07465" layer="21"/>
<rectangle x1="5.98805" y1="13.07465" x2="16.10995" y2="13.08735" layer="21"/>
<rectangle x1="6.01345" y1="13.08735" x2="16.08455" y2="13.10005" layer="21"/>
<rectangle x1="6.03885" y1="13.10005" x2="16.05915" y2="13.11275" layer="21"/>
<rectangle x1="6.06425" y1="13.11275" x2="16.03375" y2="13.12545" layer="21"/>
<rectangle x1="6.08965" y1="13.12545" x2="16.00835" y2="13.13815" layer="21"/>
<rectangle x1="6.11505" y1="13.13815" x2="15.98295" y2="13.15085" layer="21"/>
<rectangle x1="6.14045" y1="13.15085" x2="15.95755" y2="13.16355" layer="21"/>
<rectangle x1="6.16585" y1="13.16355" x2="15.93215" y2="13.17625" layer="21"/>
<rectangle x1="6.19125" y1="13.17625" x2="15.90675" y2="13.18895" layer="21"/>
<rectangle x1="6.21665" y1="13.18895" x2="15.88135" y2="13.20165" layer="21"/>
<rectangle x1="6.24205" y1="13.20165" x2="15.85595" y2="13.21435" layer="21"/>
<rectangle x1="6.26745" y1="13.21435" x2="15.83055" y2="13.22705" layer="21"/>
<rectangle x1="6.29285" y1="13.22705" x2="15.80515" y2="13.23975" layer="21"/>
<rectangle x1="6.31825" y1="13.23975" x2="15.77975" y2="13.25245" layer="21"/>
<rectangle x1="6.34365" y1="13.25245" x2="15.75435" y2="13.26515" layer="21"/>
<rectangle x1="6.36905" y1="13.26515" x2="15.72895" y2="13.27785" layer="21"/>
<rectangle x1="6.39445" y1="13.27785" x2="15.70355" y2="13.29055" layer="21"/>
<rectangle x1="6.41985" y1="13.29055" x2="15.67815" y2="13.30325" layer="21"/>
<rectangle x1="6.44525" y1="13.30325" x2="15.65275" y2="13.31595" layer="21"/>
<rectangle x1="6.47065" y1="13.31595" x2="15.62735" y2="13.32865" layer="21"/>
<rectangle x1="6.49605" y1="13.32865" x2="15.60195" y2="13.34135" layer="21"/>
<rectangle x1="6.52145" y1="13.34135" x2="15.57655" y2="13.35405" layer="21"/>
<rectangle x1="6.54685" y1="13.35405" x2="15.55115" y2="13.36675" layer="21"/>
<rectangle x1="6.57225" y1="13.36675" x2="15.52575" y2="13.37945" layer="21"/>
<rectangle x1="6.59765" y1="13.37945" x2="15.50035" y2="13.39215" layer="21"/>
<rectangle x1="6.62305" y1="13.39215" x2="15.47495" y2="13.40485" layer="21"/>
<rectangle x1="6.64845" y1="13.40485" x2="15.44955" y2="13.41755" layer="21"/>
<rectangle x1="6.67385" y1="13.41755" x2="15.42415" y2="13.43025" layer="21"/>
<rectangle x1="6.69925" y1="13.43025" x2="15.39875" y2="13.44295" layer="21"/>
<rectangle x1="6.72465" y1="13.44295" x2="15.37335" y2="13.45565" layer="21"/>
<rectangle x1="6.75005" y1="13.45565" x2="15.34795" y2="13.46835" layer="21"/>
<rectangle x1="6.77545" y1="13.46835" x2="15.32255" y2="13.48105" layer="21"/>
<rectangle x1="6.80085" y1="13.48105" x2="15.29715" y2="13.49375" layer="21"/>
<rectangle x1="6.82625" y1="13.49375" x2="15.27175" y2="13.50645" layer="21"/>
<rectangle x1="6.85165" y1="13.50645" x2="15.24635" y2="13.51915" layer="21"/>
<rectangle x1="6.87705" y1="13.51915" x2="15.22095" y2="13.53185" layer="21"/>
<rectangle x1="6.90245" y1="13.53185" x2="15.19555" y2="13.54455" layer="21"/>
<rectangle x1="6.92785" y1="13.54455" x2="15.17015" y2="13.55725" layer="21"/>
<rectangle x1="6.95325" y1="13.55725" x2="15.14475" y2="13.56995" layer="21"/>
<rectangle x1="6.97865" y1="13.56995" x2="15.11935" y2="13.58265" layer="21"/>
<rectangle x1="7.00405" y1="13.58265" x2="15.09395" y2="13.59535" layer="21"/>
<rectangle x1="7.02945" y1="13.59535" x2="15.06855" y2="13.60805" layer="21"/>
<rectangle x1="7.05485" y1="13.60805" x2="15.04315" y2="13.62075" layer="21"/>
<rectangle x1="7.08025" y1="13.62075" x2="15.01775" y2="13.63345" layer="21"/>
<rectangle x1="7.10565" y1="13.63345" x2="14.99235" y2="13.64615" layer="21"/>
<rectangle x1="7.13105" y1="13.64615" x2="14.96695" y2="13.65885" layer="21"/>
<rectangle x1="7.15645" y1="13.65885" x2="14.94155" y2="13.67155" layer="21"/>
<rectangle x1="7.18185" y1="13.67155" x2="14.91615" y2="13.68425" layer="21"/>
<rectangle x1="7.20725" y1="13.68425" x2="14.89075" y2="13.69695" layer="21"/>
<rectangle x1="7.23265" y1="13.69695" x2="14.86535" y2="13.70965" layer="21"/>
<rectangle x1="7.25805" y1="13.70965" x2="14.83995" y2="13.72235" layer="21"/>
<rectangle x1="7.28345" y1="13.72235" x2="14.81455" y2="13.73505" layer="21"/>
<rectangle x1="7.30885" y1="13.73505" x2="14.78915" y2="13.74775" layer="21"/>
<rectangle x1="7.33425" y1="13.74775" x2="14.76375" y2="13.76045" layer="21"/>
<rectangle x1="7.35965" y1="13.76045" x2="14.73835" y2="13.77315" layer="21"/>
<rectangle x1="7.38505" y1="13.77315" x2="14.71295" y2="13.78585" layer="21"/>
<rectangle x1="7.41045" y1="13.78585" x2="14.68755" y2="13.79855" layer="21"/>
<rectangle x1="7.43585" y1="13.79855" x2="14.66215" y2="13.81125" layer="21"/>
<rectangle x1="7.46125" y1="13.81125" x2="14.63675" y2="13.82395" layer="21"/>
<rectangle x1="7.48665" y1="13.82395" x2="14.61135" y2="13.83665" layer="21"/>
<rectangle x1="7.51205" y1="13.83665" x2="14.58595" y2="13.84935" layer="21"/>
<rectangle x1="7.53745" y1="13.84935" x2="14.56055" y2="13.86205" layer="21"/>
<rectangle x1="7.56285" y1="13.86205" x2="14.53515" y2="13.87475" layer="21"/>
<rectangle x1="7.58825" y1="13.87475" x2="14.50975" y2="13.88745" layer="21"/>
<rectangle x1="7.61365" y1="13.88745" x2="14.48435" y2="13.90015" layer="21"/>
<rectangle x1="7.63905" y1="13.90015" x2="14.45895" y2="13.91285" layer="21"/>
<rectangle x1="7.66445" y1="13.91285" x2="14.43355" y2="13.92555" layer="21"/>
<rectangle x1="7.68985" y1="13.92555" x2="14.40815" y2="13.93825" layer="21"/>
<rectangle x1="7.71525" y1="13.93825" x2="14.38275" y2="13.95095" layer="21"/>
<rectangle x1="7.74065" y1="13.95095" x2="14.35735" y2="13.96365" layer="21"/>
<rectangle x1="7.76605" y1="13.96365" x2="14.33195" y2="13.97635" layer="21"/>
<rectangle x1="7.79145" y1="13.97635" x2="14.30655" y2="13.98905" layer="21"/>
<rectangle x1="7.81685" y1="13.98905" x2="14.28115" y2="14.00175" layer="21"/>
<rectangle x1="7.84225" y1="14.00175" x2="14.25575" y2="14.01445" layer="21"/>
<rectangle x1="7.86765" y1="14.01445" x2="14.23035" y2="14.02715" layer="21"/>
<rectangle x1="7.89305" y1="14.02715" x2="14.20495" y2="14.03985" layer="21"/>
<rectangle x1="7.91845" y1="14.03985" x2="14.17955" y2="14.05255" layer="21"/>
<rectangle x1="7.94385" y1="14.05255" x2="14.15415" y2="14.06525" layer="21"/>
<rectangle x1="7.96925" y1="14.06525" x2="14.12875" y2="14.07795" layer="21"/>
<rectangle x1="7.99465" y1="14.07795" x2="14.10335" y2="14.09065" layer="21"/>
<rectangle x1="8.02005" y1="14.09065" x2="14.07795" y2="14.10335" layer="21"/>
<rectangle x1="8.04545" y1="14.10335" x2="14.05255" y2="14.11605" layer="21"/>
<rectangle x1="8.07085" y1="14.11605" x2="14.02715" y2="14.12875" layer="21"/>
<rectangle x1="8.09625" y1="14.12875" x2="14.00175" y2="14.14145" layer="21"/>
<rectangle x1="8.12165" y1="14.14145" x2="13.97635" y2="14.15415" layer="21"/>
<rectangle x1="8.14705" y1="14.15415" x2="13.95095" y2="14.16685" layer="21"/>
<rectangle x1="8.17245" y1="14.16685" x2="13.92555" y2="14.17955" layer="21"/>
<rectangle x1="8.19785" y1="14.17955" x2="13.90015" y2="14.19225" layer="21"/>
<rectangle x1="8.22325" y1="14.19225" x2="13.87475" y2="14.20495" layer="21"/>
<rectangle x1="8.24865" y1="14.20495" x2="13.84935" y2="14.21765" layer="21"/>
<rectangle x1="8.27405" y1="14.21765" x2="13.82395" y2="14.23035" layer="21"/>
<rectangle x1="8.29945" y1="14.23035" x2="13.79855" y2="14.24305" layer="21"/>
<rectangle x1="8.32485" y1="14.24305" x2="13.77315" y2="14.25575" layer="21"/>
<rectangle x1="8.35025" y1="14.25575" x2="13.74775" y2="14.26845" layer="21"/>
<rectangle x1="8.37565" y1="14.26845" x2="13.72235" y2="14.28115" layer="21"/>
<rectangle x1="8.40105" y1="14.28115" x2="13.69695" y2="14.29385" layer="21"/>
<rectangle x1="8.42645" y1="14.29385" x2="13.67155" y2="14.30655" layer="21"/>
<rectangle x1="8.45185" y1="14.30655" x2="13.64615" y2="14.31925" layer="21"/>
<rectangle x1="8.47725" y1="14.31925" x2="13.62075" y2="14.33195" layer="21"/>
<rectangle x1="8.50265" y1="14.33195" x2="13.59535" y2="14.34465" layer="21"/>
<rectangle x1="8.52805" y1="14.34465" x2="13.56995" y2="14.35735" layer="21"/>
<rectangle x1="8.55345" y1="14.35735" x2="13.54455" y2="14.37005" layer="21"/>
<rectangle x1="8.57885" y1="14.37005" x2="13.51915" y2="14.38275" layer="21"/>
<rectangle x1="8.60425" y1="14.38275" x2="13.49375" y2="14.39545" layer="21"/>
<rectangle x1="8.62965" y1="14.39545" x2="13.46835" y2="14.40815" layer="21"/>
<rectangle x1="8.65505" y1="14.40815" x2="13.44295" y2="14.42085" layer="21"/>
<rectangle x1="8.68045" y1="14.42085" x2="13.41755" y2="14.43355" layer="21"/>
<rectangle x1="8.70585" y1="14.43355" x2="13.39215" y2="14.44625" layer="21"/>
<rectangle x1="8.73125" y1="14.44625" x2="13.36675" y2="14.45895" layer="21"/>
<rectangle x1="8.75665" y1="14.45895" x2="13.34135" y2="14.47165" layer="21"/>
<rectangle x1="8.78205" y1="14.47165" x2="13.31595" y2="14.48435" layer="21"/>
<rectangle x1="8.80745" y1="14.48435" x2="13.29055" y2="14.49705" layer="21"/>
<rectangle x1="8.83285" y1="14.49705" x2="13.26515" y2="14.50975" layer="21"/>
<rectangle x1="8.85825" y1="14.50975" x2="13.23975" y2="14.52245" layer="21"/>
<rectangle x1="8.88365" y1="14.52245" x2="13.21435" y2="14.53515" layer="21"/>
<rectangle x1="8.90905" y1="14.53515" x2="13.18895" y2="14.54785" layer="21"/>
<rectangle x1="8.93445" y1="14.54785" x2="13.16355" y2="14.56055" layer="21"/>
<rectangle x1="8.95985" y1="14.56055" x2="13.13815" y2="14.57325" layer="21"/>
<rectangle x1="8.98525" y1="14.57325" x2="13.11275" y2="14.58595" layer="21"/>
<rectangle x1="9.01065" y1="14.58595" x2="13.08735" y2="14.59865" layer="21"/>
<rectangle x1="9.03605" y1="14.59865" x2="13.06195" y2="14.61135" layer="21"/>
<rectangle x1="9.06145" y1="14.61135" x2="13.03655" y2="14.62405" layer="21"/>
<rectangle x1="9.08685" y1="14.62405" x2="13.01115" y2="14.63675" layer="21"/>
<rectangle x1="9.11225" y1="14.63675" x2="12.98575" y2="14.64945" layer="21"/>
<rectangle x1="9.13765" y1="14.64945" x2="12.96035" y2="14.66215" layer="21"/>
<rectangle x1="9.16305" y1="14.66215" x2="12.93495" y2="14.67485" layer="21"/>
<rectangle x1="9.18845" y1="14.67485" x2="12.90955" y2="14.68755" layer="21"/>
<rectangle x1="9.21385" y1="14.68755" x2="12.88415" y2="14.70025" layer="21"/>
<rectangle x1="9.23925" y1="14.70025" x2="12.85875" y2="14.71295" layer="21"/>
<rectangle x1="9.26465" y1="14.71295" x2="12.83335" y2="14.72565" layer="21"/>
<rectangle x1="9.29005" y1="14.72565" x2="12.80795" y2="14.73835" layer="21"/>
<rectangle x1="9.31545" y1="14.73835" x2="12.78255" y2="14.75105" layer="21"/>
<rectangle x1="9.34085" y1="14.75105" x2="12.75715" y2="14.76375" layer="21"/>
<rectangle x1="9.36625" y1="14.76375" x2="12.73175" y2="14.77645" layer="21"/>
<rectangle x1="9.39165" y1="14.77645" x2="12.70635" y2="14.78915" layer="21"/>
<rectangle x1="9.41705" y1="14.78915" x2="12.68095" y2="14.80185" layer="21"/>
<rectangle x1="9.44245" y1="14.80185" x2="12.65555" y2="14.81455" layer="21"/>
<rectangle x1="9.46785" y1="14.81455" x2="12.63015" y2="14.82725" layer="21"/>
<rectangle x1="9.49325" y1="14.82725" x2="12.60475" y2="14.83995" layer="21"/>
<rectangle x1="9.51865" y1="14.83995" x2="12.57935" y2="14.85265" layer="21"/>
<rectangle x1="9.54405" y1="14.85265" x2="12.55395" y2="14.86535" layer="21"/>
<rectangle x1="9.56945" y1="14.86535" x2="12.52855" y2="14.87805" layer="21"/>
<rectangle x1="9.59485" y1="14.87805" x2="12.50315" y2="14.89075" layer="21"/>
<rectangle x1="9.62025" y1="14.89075" x2="12.49045" y2="14.90345" layer="21"/>
<rectangle x1="9.64565" y1="14.90345" x2="12.46505" y2="14.91615" layer="21"/>
<rectangle x1="9.67105" y1="14.91615" x2="12.42695" y2="14.92885" layer="21"/>
<rectangle x1="9.69645" y1="14.92885" x2="12.40155" y2="14.94155" layer="21"/>
<rectangle x1="9.72185" y1="14.94155" x2="12.37615" y2="14.95425" layer="21"/>
<rectangle x1="9.74725" y1="14.95425" x2="12.35075" y2="14.96695" layer="21"/>
<rectangle x1="9.77265" y1="14.96695" x2="12.32535" y2="14.97965" layer="21"/>
<rectangle x1="9.79805" y1="14.97965" x2="12.29995" y2="14.99235" layer="21"/>
<rectangle x1="9.82345" y1="14.99235" x2="12.27455" y2="15.00505" layer="21"/>
<rectangle x1="9.84885" y1="15.00505" x2="12.24915" y2="15.01775" layer="21"/>
<rectangle x1="9.87425" y1="15.01775" x2="12.22375" y2="15.03045" layer="21"/>
<rectangle x1="9.89965" y1="15.03045" x2="12.19835" y2="15.04315" layer="21"/>
<rectangle x1="9.92505" y1="15.04315" x2="12.17295" y2="15.05585" layer="21"/>
<rectangle x1="9.95045" y1="15.05585" x2="12.14755" y2="15.06855" layer="21"/>
<rectangle x1="9.96315" y1="15.06855" x2="12.12215" y2="15.08125" layer="21"/>
<rectangle x1="10.00125" y1="15.08125" x2="12.09675" y2="15.09395" layer="21"/>
<rectangle x1="10.02665" y1="15.09395" x2="12.07135" y2="15.10665" layer="21"/>
<rectangle x1="10.05205" y1="15.10665" x2="12.04595" y2="15.11935" layer="21"/>
<rectangle x1="10.07745" y1="15.11935" x2="12.02055" y2="15.13205" layer="21"/>
<rectangle x1="10.10285" y1="15.13205" x2="11.99515" y2="15.14475" layer="21"/>
<rectangle x1="10.12825" y1="15.14475" x2="11.96975" y2="15.15745" layer="21"/>
<rectangle x1="10.15365" y1="15.15745" x2="11.94435" y2="15.17015" layer="21"/>
<rectangle x1="10.17905" y1="15.17015" x2="11.91895" y2="15.18285" layer="21"/>
<rectangle x1="10.20445" y1="15.18285" x2="11.89355" y2="15.19555" layer="21"/>
<rectangle x1="10.22985" y1="15.19555" x2="11.86815" y2="15.20825" layer="21"/>
<rectangle x1="10.25525" y1="15.20825" x2="11.84275" y2="15.22095" layer="21"/>
<rectangle x1="10.28065" y1="15.22095" x2="11.81735" y2="15.23365" layer="21"/>
<rectangle x1="10.30605" y1="15.23365" x2="11.79195" y2="15.24635" layer="21"/>
<rectangle x1="10.33145" y1="15.24635" x2="11.76655" y2="15.25905" layer="21"/>
<rectangle x1="10.35685" y1="15.25905" x2="11.74115" y2="15.27175" layer="21"/>
<rectangle x1="10.38225" y1="15.27175" x2="11.72845" y2="15.28445" layer="21"/>
<rectangle x1="10.40765" y1="15.28445" x2="11.70305" y2="15.29715" layer="21"/>
<rectangle x1="10.43305" y1="15.29715" x2="11.67765" y2="15.30985" layer="21"/>
<rectangle x1="10.45845" y1="15.30985" x2="11.65225" y2="15.32255" layer="21"/>
<rectangle x1="10.48385" y1="15.32255" x2="11.62685" y2="15.33525" layer="21"/>
<rectangle x1="10.50925" y1="15.33525" x2="11.60145" y2="15.34795" layer="21"/>
<rectangle x1="10.53465" y1="15.34795" x2="11.57605" y2="15.36065" layer="21"/>
<rectangle x1="10.56005" y1="15.36065" x2="11.55065" y2="15.37335" layer="21"/>
<rectangle x1="10.58545" y1="15.37335" x2="11.52525" y2="15.38605" layer="21"/>
<rectangle x1="10.61085" y1="15.38605" x2="11.49985" y2="15.39875" layer="21"/>
<rectangle x1="10.63625" y1="15.39875" x2="11.47445" y2="15.41145" layer="21"/>
<rectangle x1="10.66165" y1="15.41145" x2="11.44905" y2="15.42415" layer="21"/>
<rectangle x1="10.68705" y1="15.42415" x2="11.42365" y2="15.43685" layer="21"/>
<rectangle x1="10.71245" y1="15.43685" x2="11.39825" y2="15.44955" layer="21"/>
<rectangle x1="10.73785" y1="15.44955" x2="11.37285" y2="15.46225" layer="21"/>
<rectangle x1="10.76325" y1="15.46225" x2="11.34745" y2="15.47495" layer="21"/>
<rectangle x1="10.78865" y1="15.47495" x2="11.32205" y2="15.48765" layer="21"/>
<rectangle x1="10.81405" y1="15.48765" x2="11.29665" y2="15.50035" layer="21"/>
<rectangle x1="10.83945" y1="15.50035" x2="11.27125" y2="15.51305" layer="21"/>
<rectangle x1="10.86485" y1="15.51305" x2="11.24585" y2="15.52575" layer="21"/>
<rectangle x1="10.89025" y1="15.52575" x2="11.22045" y2="15.53845" layer="21"/>
<rectangle x1="10.91565" y1="15.53845" x2="11.19505" y2="15.55115" layer="21"/>
<rectangle x1="10.94105" y1="15.55115" x2="11.16965" y2="15.56385" layer="21"/>
<rectangle x1="10.96645" y1="15.56385" x2="11.14425" y2="15.57655" layer="21"/>
<rectangle x1="10.99185" y1="15.57655" x2="11.11885" y2="15.58925" layer="21"/>
<rectangle x1="11.01725" y1="15.58925" x2="11.09345" y2="15.60195" layer="21"/>
<rectangle x1="11.04265" y1="15.60195" x2="11.06805" y2="15.61465" layer="21"/>
</package>
<package name="EAGLEXLOGO">
<rectangle x1="1.3081" y1="0.9525" x2="8.9535" y2="0.9779" layer="21"/>
<rectangle x1="17.3609" y1="0.9525" x2="25.2603" y2="0.9779" layer="21"/>
<rectangle x1="1.2573" y1="0.9779" x2="8.9789" y2="1.0033" layer="21"/>
<rectangle x1="17.3101" y1="0.9779" x2="25.3111" y2="1.0033" layer="21"/>
<rectangle x1="1.2319" y1="1.0033" x2="9.0297" y2="1.0287" layer="21"/>
<rectangle x1="17.2847" y1="1.0033" x2="25.3365" y2="1.0287" layer="21"/>
<rectangle x1="1.2065" y1="1.0287" x2="9.0297" y2="1.0541" layer="21"/>
<rectangle x1="17.2593" y1="1.0287" x2="25.3619" y2="1.0541" layer="21"/>
<rectangle x1="1.2065" y1="1.0541" x2="9.0551" y2="1.0795" layer="21"/>
<rectangle x1="17.2593" y1="1.0541" x2="25.3619" y2="1.0795" layer="21"/>
<rectangle x1="1.1811" y1="1.0795" x2="9.0805" y2="1.1049" layer="21"/>
<rectangle x1="17.2339" y1="1.0795" x2="25.3873" y2="1.1049" layer="21"/>
<rectangle x1="1.1811" y1="1.1049" x2="9.0805" y2="1.1303" layer="21"/>
<rectangle x1="17.2085" y1="1.1049" x2="25.3873" y2="1.1303" layer="21"/>
<rectangle x1="1.1811" y1="1.1303" x2="9.1059" y2="1.1557" layer="21"/>
<rectangle x1="17.2085" y1="1.1303" x2="25.3873" y2="1.1557" layer="21"/>
<rectangle x1="1.1811" y1="1.1557" x2="9.1059" y2="1.1811" layer="21"/>
<rectangle x1="17.1831" y1="1.1557" x2="25.3873" y2="1.1811" layer="21"/>
<rectangle x1="1.1811" y1="1.1811" x2="9.1313" y2="1.2065" layer="21"/>
<rectangle x1="17.1831" y1="1.1811" x2="25.3873" y2="1.2065" layer="21"/>
<rectangle x1="1.1811" y1="1.2065" x2="9.1567" y2="1.2319" layer="21"/>
<rectangle x1="17.1577" y1="1.2065" x2="25.3873" y2="1.2319" layer="21"/>
<rectangle x1="1.2065" y1="1.2319" x2="9.1567" y2="1.2573" layer="21"/>
<rectangle x1="17.1323" y1="1.2319" x2="25.3873" y2="1.2573" layer="21"/>
<rectangle x1="1.2065" y1="1.2573" x2="9.1821" y2="1.2827" layer="21"/>
<rectangle x1="17.1323" y1="1.2573" x2="25.3619" y2="1.2827" layer="21"/>
<rectangle x1="1.2319" y1="1.2827" x2="9.2075" y2="1.3081" layer="21"/>
<rectangle x1="17.1069" y1="1.2827" x2="25.3365" y2="1.3081" layer="21"/>
<rectangle x1="1.2573" y1="1.3081" x2="9.2075" y2="1.3335" layer="21"/>
<rectangle x1="17.1069" y1="1.3081" x2="25.3365" y2="1.3335" layer="21"/>
<rectangle x1="1.2573" y1="1.3335" x2="9.2329" y2="1.3589" layer="21"/>
<rectangle x1="17.0815" y1="1.3335" x2="25.3111" y2="1.3589" layer="21"/>
<rectangle x1="1.2827" y1="1.3589" x2="9.2583" y2="1.3843" layer="21"/>
<rectangle x1="17.0561" y1="1.3589" x2="25.2857" y2="1.3843" layer="21"/>
<rectangle x1="1.3081" y1="1.3843" x2="9.2583" y2="1.4097" layer="21"/>
<rectangle x1="17.0561" y1="1.3843" x2="25.2857" y2="1.4097" layer="21"/>
<rectangle x1="1.3081" y1="1.4097" x2="9.2837" y2="1.4351" layer="21"/>
<rectangle x1="17.0307" y1="1.4097" x2="25.2603" y2="1.4351" layer="21"/>
<rectangle x1="1.3335" y1="1.4351" x2="9.2837" y2="1.4605" layer="21"/>
<rectangle x1="17.0307" y1="1.4351" x2="25.2349" y2="1.4605" layer="21"/>
<rectangle x1="1.3589" y1="1.4605" x2="9.3091" y2="1.4859" layer="21"/>
<rectangle x1="17.0053" y1="1.4605" x2="25.2349" y2="1.4859" layer="21"/>
<rectangle x1="1.3589" y1="1.4859" x2="9.3345" y2="1.5113" layer="21"/>
<rectangle x1="16.9799" y1="1.4859" x2="25.2095" y2="1.5113" layer="21"/>
<rectangle x1="1.3843" y1="1.5113" x2="9.3345" y2="1.5367" layer="21"/>
<rectangle x1="16.9799" y1="1.5113" x2="25.1841" y2="1.5367" layer="21"/>
<rectangle x1="1.4097" y1="1.5367" x2="9.3599" y2="1.5621" layer="21"/>
<rectangle x1="16.9545" y1="1.5367" x2="25.1841" y2="1.5621" layer="21"/>
<rectangle x1="1.4097" y1="1.5621" x2="9.3853" y2="1.5875" layer="21"/>
<rectangle x1="16.9545" y1="1.5621" x2="25.1587" y2="1.5875" layer="21"/>
<rectangle x1="1.4351" y1="1.5875" x2="9.3853" y2="1.6129" layer="21"/>
<rectangle x1="16.9291" y1="1.5875" x2="25.1333" y2="1.6129" layer="21"/>
<rectangle x1="1.4605" y1="1.6129" x2="9.4107" y2="1.6383" layer="21"/>
<rectangle x1="16.9291" y1="1.6129" x2="25.1333" y2="1.6383" layer="21"/>
<rectangle x1="1.4605" y1="1.6383" x2="9.4107" y2="1.6637" layer="21"/>
<rectangle x1="16.9037" y1="1.6383" x2="25.1079" y2="1.6637" layer="21"/>
<rectangle x1="1.4859" y1="1.6637" x2="9.4361" y2="1.6891" layer="21"/>
<rectangle x1="16.8783" y1="1.6637" x2="25.0825" y2="1.6891" layer="21"/>
<rectangle x1="1.5113" y1="1.6891" x2="9.4615" y2="1.7145" layer="21"/>
<rectangle x1="16.8783" y1="1.6891" x2="25.0825" y2="1.7145" layer="21"/>
<rectangle x1="1.5113" y1="1.7145" x2="9.4615" y2="1.7399" layer="21"/>
<rectangle x1="16.8529" y1="1.7145" x2="25.0571" y2="1.7399" layer="21"/>
<rectangle x1="1.5367" y1="1.7399" x2="9.4869" y2="1.7653" layer="21"/>
<rectangle x1="16.8529" y1="1.7399" x2="25.0317" y2="1.7653" layer="21"/>
<rectangle x1="1.5621" y1="1.7653" x2="9.5123" y2="1.7907" layer="21"/>
<rectangle x1="16.8275" y1="1.7653" x2="25.0317" y2="1.7907" layer="21"/>
<rectangle x1="1.5621" y1="1.7907" x2="9.5123" y2="1.8161" layer="21"/>
<rectangle x1="16.8021" y1="1.7907" x2="25.0063" y2="1.8161" layer="21"/>
<rectangle x1="1.5875" y1="1.8161" x2="9.5377" y2="1.8415" layer="21"/>
<rectangle x1="16.8021" y1="1.8161" x2="24.9809" y2="1.8415" layer="21"/>
<rectangle x1="1.6129" y1="1.8415" x2="9.5631" y2="1.8669" layer="21"/>
<rectangle x1="16.7767" y1="1.8415" x2="24.9809" y2="1.8669" layer="21"/>
<rectangle x1="1.6129" y1="1.8669" x2="9.5631" y2="1.8923" layer="21"/>
<rectangle x1="16.7767" y1="1.8669" x2="24.9555" y2="1.8923" layer="21"/>
<rectangle x1="1.6383" y1="1.8923" x2="9.5885" y2="1.9177" layer="21"/>
<rectangle x1="16.7513" y1="1.8923" x2="24.9301" y2="1.9177" layer="21"/>
<rectangle x1="1.6637" y1="1.9177" x2="9.5885" y2="1.9431" layer="21"/>
<rectangle x1="16.7259" y1="1.9177" x2="24.9301" y2="1.9431" layer="21"/>
<rectangle x1="1.6637" y1="1.9431" x2="9.6139" y2="1.9685" layer="21"/>
<rectangle x1="16.7259" y1="1.9431" x2="24.9047" y2="1.9685" layer="21"/>
<rectangle x1="1.6891" y1="1.9685" x2="9.6393" y2="1.9939" layer="21"/>
<rectangle x1="16.7005" y1="1.9685" x2="24.8793" y2="1.9939" layer="21"/>
<rectangle x1="1.7145" y1="1.9939" x2="9.6393" y2="2.0193" layer="21"/>
<rectangle x1="16.7005" y1="1.9939" x2="24.8793" y2="2.0193" layer="21"/>
<rectangle x1="1.7145" y1="2.0193" x2="9.6647" y2="2.0447" layer="21"/>
<rectangle x1="16.6751" y1="2.0193" x2="24.8539" y2="2.0447" layer="21"/>
<rectangle x1="1.7399" y1="2.0447" x2="9.6901" y2="2.0701" layer="21"/>
<rectangle x1="16.6497" y1="2.0447" x2="24.8285" y2="2.0701" layer="21"/>
<rectangle x1="1.7653" y1="2.0701" x2="9.6901" y2="2.0955" layer="21"/>
<rectangle x1="16.6497" y1="2.0701" x2="24.8285" y2="2.0955" layer="21"/>
<rectangle x1="1.7907" y1="2.0955" x2="9.7155" y2="2.1209" layer="21"/>
<rectangle x1="16.6243" y1="2.0955" x2="24.8031" y2="2.1209" layer="21"/>
<rectangle x1="1.7907" y1="2.1209" x2="9.7409" y2="2.1463" layer="21"/>
<rectangle x1="16.6243" y1="2.1209" x2="24.7777" y2="2.1463" layer="21"/>
<rectangle x1="1.8161" y1="2.1463" x2="9.7409" y2="2.1717" layer="21"/>
<rectangle x1="16.5989" y1="2.1463" x2="24.7777" y2="2.1717" layer="21"/>
<rectangle x1="1.8415" y1="2.1717" x2="9.7663" y2="2.1971" layer="21"/>
<rectangle x1="16.5735" y1="2.1717" x2="24.7523" y2="2.1971" layer="21"/>
<rectangle x1="1.8415" y1="2.1971" x2="9.7663" y2="2.2225" layer="21"/>
<rectangle x1="16.5735" y1="2.1971" x2="24.7269" y2="2.2225" layer="21"/>
<rectangle x1="1.8669" y1="2.2225" x2="9.7917" y2="2.2479" layer="21"/>
<rectangle x1="16.5481" y1="2.2225" x2="24.7269" y2="2.2479" layer="21"/>
<rectangle x1="1.8923" y1="2.2479" x2="9.8171" y2="2.2733" layer="21"/>
<rectangle x1="16.5481" y1="2.2479" x2="24.7015" y2="2.2733" layer="21"/>
<rectangle x1="1.8923" y1="2.2733" x2="9.8171" y2="2.2987" layer="21"/>
<rectangle x1="16.5227" y1="2.2733" x2="24.6761" y2="2.2987" layer="21"/>
<rectangle x1="1.9177" y1="2.2987" x2="9.8425" y2="2.3241" layer="21"/>
<rectangle x1="16.4973" y1="2.2987" x2="24.6761" y2="2.3241" layer="21"/>
<rectangle x1="1.9431" y1="2.3241" x2="9.8679" y2="2.3495" layer="21"/>
<rectangle x1="16.4973" y1="2.3241" x2="24.6507" y2="2.3495" layer="21"/>
<rectangle x1="1.9431" y1="2.3495" x2="9.8679" y2="2.3749" layer="21"/>
<rectangle x1="16.4719" y1="2.3495" x2="24.6253" y2="2.3749" layer="21"/>
<rectangle x1="1.9685" y1="2.3749" x2="9.8933" y2="2.4003" layer="21"/>
<rectangle x1="16.4719" y1="2.3749" x2="24.6253" y2="2.4003" layer="21"/>
<rectangle x1="1.9939" y1="2.4003" x2="9.8933" y2="2.4257" layer="21"/>
<rectangle x1="16.4465" y1="2.4003" x2="24.5999" y2="2.4257" layer="21"/>
<rectangle x1="1.9939" y1="2.4257" x2="9.9187" y2="2.4511" layer="21"/>
<rectangle x1="16.4465" y1="2.4257" x2="24.5745" y2="2.4511" layer="21"/>
<rectangle x1="2.0193" y1="2.4511" x2="9.9441" y2="2.4765" layer="21"/>
<rectangle x1="16.4211" y1="2.4511" x2="24.5745" y2="2.4765" layer="21"/>
<rectangle x1="2.0447" y1="2.4765" x2="9.9441" y2="2.5019" layer="21"/>
<rectangle x1="16.3957" y1="2.4765" x2="24.5491" y2="2.5019" layer="21"/>
<rectangle x1="2.0447" y1="2.5019" x2="9.9695" y2="2.5273" layer="21"/>
<rectangle x1="16.3957" y1="2.5019" x2="24.5237" y2="2.5273" layer="21"/>
<rectangle x1="2.0701" y1="2.5273" x2="9.9949" y2="2.5527" layer="21"/>
<rectangle x1="16.3703" y1="2.5273" x2="24.5237" y2="2.5527" layer="21"/>
<rectangle x1="2.0955" y1="2.5527" x2="9.9949" y2="2.5781" layer="21"/>
<rectangle x1="16.3703" y1="2.5527" x2="24.4983" y2="2.5781" layer="21"/>
<rectangle x1="2.0955" y1="2.5781" x2="10.0203" y2="2.6035" layer="21"/>
<rectangle x1="16.3449" y1="2.5781" x2="24.4729" y2="2.6035" layer="21"/>
<rectangle x1="2.1209" y1="2.6035" x2="10.0457" y2="2.6289" layer="21"/>
<rectangle x1="16.3195" y1="2.6035" x2="24.4475" y2="2.6289" layer="21"/>
<rectangle x1="2.1463" y1="2.6289" x2="10.0457" y2="2.6543" layer="21"/>
<rectangle x1="16.3195" y1="2.6289" x2="24.4475" y2="2.6543" layer="21"/>
<rectangle x1="2.1463" y1="2.6543" x2="10.0711" y2="2.6797" layer="21"/>
<rectangle x1="16.2941" y1="2.6543" x2="24.4221" y2="2.6797" layer="21"/>
<rectangle x1="2.1717" y1="2.6797" x2="10.0711" y2="2.7051" layer="21"/>
<rectangle x1="16.2941" y1="2.6797" x2="24.3967" y2="2.7051" layer="21"/>
<rectangle x1="2.1971" y1="2.7051" x2="10.0965" y2="2.7305" layer="21"/>
<rectangle x1="16.2687" y1="2.7051" x2="24.3967" y2="2.7305" layer="21"/>
<rectangle x1="2.1971" y1="2.7305" x2="10.1219" y2="2.7559" layer="21"/>
<rectangle x1="16.2433" y1="2.7305" x2="24.3713" y2="2.7559" layer="21"/>
<rectangle x1="2.2225" y1="2.7559" x2="10.1219" y2="2.7813" layer="21"/>
<rectangle x1="16.2433" y1="2.7559" x2="24.3459" y2="2.7813" layer="21"/>
<rectangle x1="2.2479" y1="2.7813" x2="10.1473" y2="2.8067" layer="21"/>
<rectangle x1="16.2179" y1="2.7813" x2="24.3459" y2="2.8067" layer="21"/>
<rectangle x1="2.2479" y1="2.8067" x2="10.1727" y2="2.8321" layer="21"/>
<rectangle x1="16.2179" y1="2.8067" x2="24.3205" y2="2.8321" layer="21"/>
<rectangle x1="2.2733" y1="2.8321" x2="10.1727" y2="2.8575" layer="21"/>
<rectangle x1="16.1925" y1="2.8321" x2="24.2951" y2="2.8575" layer="21"/>
<rectangle x1="2.2987" y1="2.8575" x2="10.1981" y2="2.8829" layer="21"/>
<rectangle x1="16.1671" y1="2.8575" x2="24.2951" y2="2.8829" layer="21"/>
<rectangle x1="2.2987" y1="2.8829" x2="10.1981" y2="2.9083" layer="21"/>
<rectangle x1="16.1671" y1="2.8829" x2="24.2697" y2="2.9083" layer="21"/>
<rectangle x1="2.3241" y1="2.9083" x2="10.2235" y2="2.9337" layer="21"/>
<rectangle x1="16.1417" y1="2.9083" x2="24.2443" y2="2.9337" layer="21"/>
<rectangle x1="2.3495" y1="2.9337" x2="10.2489" y2="2.9591" layer="21"/>
<rectangle x1="16.1417" y1="2.9337" x2="24.2443" y2="2.9591" layer="21"/>
<rectangle x1="2.3495" y1="2.9591" x2="10.2489" y2="2.9845" layer="21"/>
<rectangle x1="16.1163" y1="2.9591" x2="24.2189" y2="2.9845" layer="21"/>
<rectangle x1="2.3749" y1="2.9845" x2="10.2743" y2="3.0099" layer="21"/>
<rectangle x1="16.0909" y1="2.9845" x2="24.1935" y2="3.0099" layer="21"/>
<rectangle x1="2.4003" y1="3.0099" x2="10.2997" y2="3.0353" layer="21"/>
<rectangle x1="16.0909" y1="3.0099" x2="24.1935" y2="3.0353" layer="21"/>
<rectangle x1="2.4257" y1="3.0353" x2="10.2997" y2="3.0607" layer="21"/>
<rectangle x1="16.0655" y1="3.0353" x2="24.1681" y2="3.0607" layer="21"/>
<rectangle x1="2.4257" y1="3.0607" x2="10.3251" y2="3.0861" layer="21"/>
<rectangle x1="16.0655" y1="3.0607" x2="24.1427" y2="3.0861" layer="21"/>
<rectangle x1="2.4511" y1="3.0861" x2="10.3505" y2="3.1115" layer="21"/>
<rectangle x1="16.0401" y1="3.0861" x2="24.1427" y2="3.1115" layer="21"/>
<rectangle x1="2.4765" y1="3.1115" x2="10.3505" y2="3.1369" layer="21"/>
<rectangle x1="16.0401" y1="3.1115" x2="24.1173" y2="3.1369" layer="21"/>
<rectangle x1="2.4765" y1="3.1369" x2="10.3759" y2="3.1623" layer="21"/>
<rectangle x1="16.0147" y1="3.1369" x2="24.0919" y2="3.1623" layer="21"/>
<rectangle x1="2.5019" y1="3.1623" x2="10.3759" y2="3.1877" layer="21"/>
<rectangle x1="15.9893" y1="3.1623" x2="24.0919" y2="3.1877" layer="21"/>
<rectangle x1="2.5273" y1="3.1877" x2="10.4013" y2="3.2131" layer="21"/>
<rectangle x1="15.9893" y1="3.1877" x2="24.0665" y2="3.2131" layer="21"/>
<rectangle x1="2.5273" y1="3.2131" x2="10.4267" y2="3.2385" layer="21"/>
<rectangle x1="15.9639" y1="3.2131" x2="24.0411" y2="3.2385" layer="21"/>
<rectangle x1="2.5527" y1="3.2385" x2="10.4267" y2="3.2639" layer="21"/>
<rectangle x1="15.9639" y1="3.2385" x2="24.0411" y2="3.2639" layer="21"/>
<rectangle x1="2.5781" y1="3.2639" x2="10.4521" y2="3.2893" layer="21"/>
<rectangle x1="15.9385" y1="3.2639" x2="24.0157" y2="3.2893" layer="21"/>
<rectangle x1="2.5781" y1="3.2893" x2="10.4775" y2="3.3147" layer="21"/>
<rectangle x1="15.9131" y1="3.2893" x2="23.9903" y2="3.3147" layer="21"/>
<rectangle x1="2.6035" y1="3.3147" x2="10.4775" y2="3.3401" layer="21"/>
<rectangle x1="15.9131" y1="3.3147" x2="23.9903" y2="3.3401" layer="21"/>
<rectangle x1="2.6289" y1="3.3401" x2="10.5029" y2="3.3655" layer="21"/>
<rectangle x1="15.8877" y1="3.3401" x2="23.9649" y2="3.3655" layer="21"/>
<rectangle x1="2.6289" y1="3.3655" x2="10.5029" y2="3.3909" layer="21"/>
<rectangle x1="15.8877" y1="3.3655" x2="23.9395" y2="3.3909" layer="21"/>
<rectangle x1="2.6543" y1="3.3909" x2="10.5283" y2="3.4163" layer="21"/>
<rectangle x1="15.8623" y1="3.3909" x2="23.9395" y2="3.4163" layer="21"/>
<rectangle x1="2.6797" y1="3.4163" x2="10.5537" y2="3.4417" layer="21"/>
<rectangle x1="15.8369" y1="3.4163" x2="23.9141" y2="3.4417" layer="21"/>
<rectangle x1="2.6797" y1="3.4417" x2="10.5537" y2="3.4671" layer="21"/>
<rectangle x1="15.8369" y1="3.4417" x2="23.8887" y2="3.4671" layer="21"/>
<rectangle x1="2.7051" y1="3.4671" x2="10.5791" y2="3.4925" layer="21"/>
<rectangle x1="15.8115" y1="3.4671" x2="23.8887" y2="3.4925" layer="21"/>
<rectangle x1="2.7305" y1="3.4925" x2="10.6045" y2="3.5179" layer="21"/>
<rectangle x1="15.8115" y1="3.4925" x2="23.8633" y2="3.5179" layer="21"/>
<rectangle x1="2.7305" y1="3.5179" x2="10.6045" y2="3.5433" layer="21"/>
<rectangle x1="15.7861" y1="3.5179" x2="23.8379" y2="3.5433" layer="21"/>
<rectangle x1="2.7559" y1="3.5433" x2="10.6299" y2="3.5687" layer="21"/>
<rectangle x1="15.7607" y1="3.5433" x2="23.8379" y2="3.5687" layer="21"/>
<rectangle x1="2.7813" y1="3.5687" x2="10.6553" y2="3.5941" layer="21"/>
<rectangle x1="15.7607" y1="3.5687" x2="23.8125" y2="3.5941" layer="21"/>
<rectangle x1="2.7813" y1="3.5941" x2="10.6553" y2="3.6195" layer="21"/>
<rectangle x1="15.7353" y1="3.5941" x2="23.7871" y2="3.6195" layer="21"/>
<rectangle x1="2.8067" y1="3.6195" x2="10.6807" y2="3.6449" layer="21"/>
<rectangle x1="15.7353" y1="3.6195" x2="23.7871" y2="3.6449" layer="21"/>
<rectangle x1="2.8321" y1="3.6449" x2="10.6807" y2="3.6703" layer="21"/>
<rectangle x1="15.7099" y1="3.6449" x2="23.7617" y2="3.6703" layer="21"/>
<rectangle x1="2.8321" y1="3.6703" x2="10.7061" y2="3.6957" layer="21"/>
<rectangle x1="15.6845" y1="3.6703" x2="23.7363" y2="3.6957" layer="21"/>
<rectangle x1="2.8575" y1="3.6957" x2="10.7315" y2="3.7211" layer="21"/>
<rectangle x1="15.6845" y1="3.6957" x2="23.7363" y2="3.7211" layer="21"/>
<rectangle x1="2.8829" y1="3.7211" x2="10.7315" y2="3.7465" layer="21"/>
<rectangle x1="15.6591" y1="3.7211" x2="23.7109" y2="3.7465" layer="21"/>
<rectangle x1="2.8829" y1="3.7465" x2="10.7569" y2="3.7719" layer="21"/>
<rectangle x1="15.6591" y1="3.7465" x2="23.6855" y2="3.7719" layer="21"/>
<rectangle x1="2.9083" y1="3.7719" x2="10.7823" y2="3.7973" layer="21"/>
<rectangle x1="15.6337" y1="3.7719" x2="23.6855" y2="3.7973" layer="21"/>
<rectangle x1="2.9337" y1="3.7973" x2="10.7823" y2="3.8227" layer="21"/>
<rectangle x1="15.6337" y1="3.7973" x2="23.6601" y2="3.8227" layer="21"/>
<rectangle x1="2.9337" y1="3.8227" x2="10.8077" y2="3.8481" layer="21"/>
<rectangle x1="15.6083" y1="3.8227" x2="23.6347" y2="3.8481" layer="21"/>
<rectangle x1="2.9591" y1="3.8481" x2="10.8077" y2="3.8735" layer="21"/>
<rectangle x1="15.5829" y1="3.8481" x2="23.6347" y2="3.8735" layer="21"/>
<rectangle x1="2.9845" y1="3.8735" x2="10.8331" y2="3.8989" layer="21"/>
<rectangle x1="15.5829" y1="3.8735" x2="23.6093" y2="3.8989" layer="21"/>
<rectangle x1="2.9845" y1="3.8989" x2="10.8585" y2="3.9243" layer="21"/>
<rectangle x1="15.5575" y1="3.8989" x2="23.5839" y2="3.9243" layer="21"/>
<rectangle x1="3.0099" y1="3.9243" x2="10.8585" y2="3.9497" layer="21"/>
<rectangle x1="15.5575" y1="3.9243" x2="23.5839" y2="3.9497" layer="21"/>
<rectangle x1="3.0353" y1="3.9497" x2="10.8839" y2="3.9751" layer="21"/>
<rectangle x1="15.5321" y1="3.9497" x2="23.5585" y2="3.9751" layer="21"/>
<rectangle x1="3.0353" y1="3.9751" x2="10.9093" y2="4.0005" layer="21"/>
<rectangle x1="15.5067" y1="3.9751" x2="23.5331" y2="4.0005" layer="21"/>
<rectangle x1="3.0607" y1="4.0005" x2="10.9093" y2="4.0259" layer="21"/>
<rectangle x1="15.5067" y1="4.0005" x2="23.5331" y2="4.0259" layer="21"/>
<rectangle x1="3.0861" y1="4.0259" x2="10.9347" y2="4.0513" layer="21"/>
<rectangle x1="15.4813" y1="4.0259" x2="23.5077" y2="4.0513" layer="21"/>
<rectangle x1="3.1115" y1="4.0513" x2="10.9601" y2="4.0767" layer="21"/>
<rectangle x1="15.4813" y1="4.0513" x2="23.4823" y2="4.0767" layer="21"/>
<rectangle x1="3.1115" y1="4.0767" x2="10.9601" y2="4.1021" layer="21"/>
<rectangle x1="15.4559" y1="4.0767" x2="23.4823" y2="4.1021" layer="21"/>
<rectangle x1="3.1369" y1="4.1021" x2="10.9855" y2="4.1275" layer="21"/>
<rectangle x1="15.4305" y1="4.1021" x2="23.4569" y2="4.1275" layer="21"/>
<rectangle x1="3.1623" y1="4.1275" x2="10.9855" y2="4.1529" layer="21"/>
<rectangle x1="15.4305" y1="4.1275" x2="23.4315" y2="4.1529" layer="21"/>
<rectangle x1="3.1623" y1="4.1529" x2="11.0109" y2="4.1783" layer="21"/>
<rectangle x1="15.4051" y1="4.1529" x2="23.4315" y2="4.1783" layer="21"/>
<rectangle x1="3.1877" y1="4.1783" x2="11.0363" y2="4.2037" layer="21"/>
<rectangle x1="15.4051" y1="4.1783" x2="23.4061" y2="4.2037" layer="21"/>
<rectangle x1="3.2131" y1="4.2037" x2="11.0363" y2="4.2291" layer="21"/>
<rectangle x1="15.3797" y1="4.2037" x2="23.3807" y2="4.2291" layer="21"/>
<rectangle x1="3.2131" y1="4.2291" x2="11.0617" y2="4.2545" layer="21"/>
<rectangle x1="15.3543" y1="4.2291" x2="23.3807" y2="4.2545" layer="21"/>
<rectangle x1="3.2385" y1="4.2545" x2="11.0871" y2="4.2799" layer="21"/>
<rectangle x1="15.3543" y1="4.2545" x2="23.3553" y2="4.2799" layer="21"/>
<rectangle x1="3.2639" y1="4.2799" x2="11.0871" y2="4.3053" layer="21"/>
<rectangle x1="15.3289" y1="4.2799" x2="23.3299" y2="4.3053" layer="21"/>
<rectangle x1="3.2639" y1="4.3053" x2="11.1125" y2="4.3307" layer="21"/>
<rectangle x1="15.3289" y1="4.3053" x2="23.3299" y2="4.3307" layer="21"/>
<rectangle x1="3.2893" y1="4.3307" x2="11.1125" y2="4.3561" layer="21"/>
<rectangle x1="15.3035" y1="4.3307" x2="23.3045" y2="4.3561" layer="21"/>
<rectangle x1="3.3147" y1="4.3561" x2="11.1379" y2="4.3815" layer="21"/>
<rectangle x1="15.2781" y1="4.3561" x2="23.2791" y2="4.3815" layer="21"/>
<rectangle x1="3.3147" y1="4.3815" x2="11.1633" y2="4.4069" layer="21"/>
<rectangle x1="15.2781" y1="4.3815" x2="23.2791" y2="4.4069" layer="21"/>
<rectangle x1="3.3401" y1="4.4069" x2="11.1633" y2="4.4323" layer="21"/>
<rectangle x1="15.2527" y1="4.4069" x2="23.2537" y2="4.4323" layer="21"/>
<rectangle x1="3.3655" y1="4.4323" x2="11.1887" y2="4.4577" layer="21"/>
<rectangle x1="15.2527" y1="4.4323" x2="23.2283" y2="4.4577" layer="21"/>
<rectangle x1="3.3655" y1="4.4577" x2="11.2141" y2="4.4831" layer="21"/>
<rectangle x1="15.2273" y1="4.4577" x2="23.2283" y2="4.4831" layer="21"/>
<rectangle x1="3.3909" y1="4.4831" x2="11.2141" y2="4.5085" layer="21"/>
<rectangle x1="15.2019" y1="4.4831" x2="23.2029" y2="4.5085" layer="21"/>
<rectangle x1="3.4163" y1="4.5085" x2="11.2395" y2="4.5339" layer="21"/>
<rectangle x1="15.2019" y1="4.5085" x2="23.1775" y2="4.5339" layer="21"/>
<rectangle x1="3.4163" y1="4.5339" x2="11.2649" y2="4.5593" layer="21"/>
<rectangle x1="15.1765" y1="4.5339" x2="23.1775" y2="4.5593" layer="21"/>
<rectangle x1="3.4417" y1="4.5593" x2="11.2649" y2="4.5847" layer="21"/>
<rectangle x1="15.1765" y1="4.5593" x2="23.1521" y2="4.5847" layer="21"/>
<rectangle x1="3.4671" y1="4.5847" x2="11.2903" y2="4.6101" layer="21"/>
<rectangle x1="15.1511" y1="4.5847" x2="23.1267" y2="4.6101" layer="21"/>
<rectangle x1="3.4671" y1="4.6101" x2="11.2903" y2="4.6355" layer="21"/>
<rectangle x1="15.1511" y1="4.6101" x2="23.1267" y2="4.6355" layer="21"/>
<rectangle x1="3.4925" y1="4.6355" x2="11.3157" y2="4.6609" layer="21"/>
<rectangle x1="15.1257" y1="4.6355" x2="23.1013" y2="4.6609" layer="21"/>
<rectangle x1="3.5179" y1="4.6609" x2="11.3411" y2="4.6863" layer="21"/>
<rectangle x1="15.1003" y1="4.6609" x2="23.0759" y2="4.6863" layer="21"/>
<rectangle x1="3.5179" y1="4.6863" x2="11.3411" y2="4.7117" layer="21"/>
<rectangle x1="15.1003" y1="4.6863" x2="23.0759" y2="4.7117" layer="21"/>
<rectangle x1="3.5433" y1="4.7117" x2="11.3665" y2="4.7371" layer="21"/>
<rectangle x1="15.0749" y1="4.7117" x2="23.0505" y2="4.7371" layer="21"/>
<rectangle x1="3.5687" y1="4.7371" x2="11.3919" y2="4.7625" layer="21"/>
<rectangle x1="15.0749" y1="4.7371" x2="23.0251" y2="4.7625" layer="21"/>
<rectangle x1="3.5687" y1="4.7625" x2="11.3919" y2="4.7879" layer="21"/>
<rectangle x1="15.0495" y1="4.7625" x2="23.0251" y2="4.7879" layer="21"/>
<rectangle x1="3.5941" y1="4.7879" x2="11.4173" y2="4.8133" layer="21"/>
<rectangle x1="15.0241" y1="4.7879" x2="22.9997" y2="4.8133" layer="21"/>
<rectangle x1="3.6195" y1="4.8133" x2="11.4427" y2="4.8387" layer="21"/>
<rectangle x1="15.0241" y1="4.8133" x2="22.9743" y2="4.8387" layer="21"/>
<rectangle x1="3.6195" y1="4.8387" x2="11.4427" y2="4.8641" layer="21"/>
<rectangle x1="14.9987" y1="4.8387" x2="22.9743" y2="4.8641" layer="21"/>
<rectangle x1="3.6449" y1="4.8641" x2="11.4681" y2="4.8895" layer="21"/>
<rectangle x1="14.9987" y1="4.8641" x2="22.9489" y2="4.8895" layer="21"/>
<rectangle x1="3.6703" y1="4.8895" x2="11.4681" y2="4.9149" layer="21"/>
<rectangle x1="14.9733" y1="4.8895" x2="22.9235" y2="4.9149" layer="21"/>
<rectangle x1="3.6703" y1="4.9149" x2="11.4935" y2="4.9403" layer="21"/>
<rectangle x1="14.9479" y1="4.9149" x2="22.9235" y2="4.9403" layer="21"/>
<rectangle x1="3.6957" y1="4.9403" x2="11.5189" y2="4.9657" layer="21"/>
<rectangle x1="14.9479" y1="4.9403" x2="22.8981" y2="4.9657" layer="21"/>
<rectangle x1="3.7211" y1="4.9657" x2="11.5189" y2="4.9911" layer="21"/>
<rectangle x1="14.9225" y1="4.9657" x2="22.8727" y2="4.9911" layer="21"/>
<rectangle x1="3.7211" y1="4.9911" x2="11.5443" y2="5.0165" layer="21"/>
<rectangle x1="14.9225" y1="4.9911" x2="22.8727" y2="5.0165" layer="21"/>
<rectangle x1="3.7465" y1="5.0165" x2="11.5697" y2="5.0419" layer="21"/>
<rectangle x1="14.8971" y1="5.0165" x2="22.8473" y2="5.0419" layer="21"/>
<rectangle x1="3.7719" y1="5.0419" x2="11.5697" y2="5.0673" layer="21"/>
<rectangle x1="14.8717" y1="5.0419" x2="22.8219" y2="5.0673" layer="21"/>
<rectangle x1="3.7973" y1="5.0673" x2="11.5951" y2="5.0927" layer="21"/>
<rectangle x1="14.8717" y1="5.0673" x2="22.7965" y2="5.0927" layer="21"/>
<rectangle x1="3.7973" y1="5.0927" x2="11.5951" y2="5.1181" layer="21"/>
<rectangle x1="14.8463" y1="5.0927" x2="22.7965" y2="5.1181" layer="21"/>
<rectangle x1="3.8227" y1="5.1181" x2="11.6205" y2="5.1435" layer="21"/>
<rectangle x1="14.8463" y1="5.1181" x2="22.7711" y2="5.1435" layer="21"/>
<rectangle x1="3.8481" y1="5.1435" x2="11.6459" y2="5.1689" layer="21"/>
<rectangle x1="14.8209" y1="5.1435" x2="22.7457" y2="5.1689" layer="21"/>
<rectangle x1="3.8481" y1="5.1689" x2="11.6459" y2="5.1943" layer="21"/>
<rectangle x1="14.7955" y1="5.1689" x2="22.7457" y2="5.1943" layer="21"/>
<rectangle x1="3.8735" y1="5.1943" x2="11.6713" y2="5.2197" layer="21"/>
<rectangle x1="14.7955" y1="5.1943" x2="22.7203" y2="5.2197" layer="21"/>
<rectangle x1="3.8989" y1="5.2197" x2="11.6967" y2="5.2451" layer="21"/>
<rectangle x1="14.7701" y1="5.2197" x2="22.6949" y2="5.2451" layer="21"/>
<rectangle x1="3.8989" y1="5.2451" x2="11.6967" y2="5.2705" layer="21"/>
<rectangle x1="14.7701" y1="5.2451" x2="22.6949" y2="5.2705" layer="21"/>
<rectangle x1="3.9243" y1="5.2705" x2="11.7221" y2="5.2959" layer="21"/>
<rectangle x1="14.7447" y1="5.2705" x2="22.6695" y2="5.2959" layer="21"/>
<rectangle x1="3.9497" y1="5.2959" x2="11.7475" y2="5.3213" layer="21"/>
<rectangle x1="14.7447" y1="5.2959" x2="22.6441" y2="5.3213" layer="21"/>
<rectangle x1="3.9497" y1="5.3213" x2="11.7475" y2="5.3467" layer="21"/>
<rectangle x1="14.7193" y1="5.3213" x2="22.6441" y2="5.3467" layer="21"/>
<rectangle x1="3.9751" y1="5.3467" x2="11.7729" y2="5.3721" layer="21"/>
<rectangle x1="14.6939" y1="5.3467" x2="22.6187" y2="5.3721" layer="21"/>
<rectangle x1="4.0005" y1="5.3721" x2="11.7729" y2="5.3975" layer="21"/>
<rectangle x1="14.6939" y1="5.3721" x2="22.5933" y2="5.3975" layer="21"/>
<rectangle x1="4.0005" y1="5.3975" x2="11.7983" y2="5.4229" layer="21"/>
<rectangle x1="14.6685" y1="5.3975" x2="22.5933" y2="5.4229" layer="21"/>
<rectangle x1="4.0259" y1="5.4229" x2="11.8237" y2="5.4483" layer="21"/>
<rectangle x1="14.6685" y1="5.4229" x2="22.5679" y2="5.4483" layer="21"/>
<rectangle x1="4.0513" y1="5.4483" x2="11.8237" y2="5.4737" layer="21"/>
<rectangle x1="14.6431" y1="5.4483" x2="22.5425" y2="5.4737" layer="21"/>
<rectangle x1="4.0513" y1="5.4737" x2="11.8491" y2="5.4991" layer="21"/>
<rectangle x1="14.6177" y1="5.4737" x2="22.5425" y2="5.4991" layer="21"/>
<rectangle x1="4.0767" y1="5.4991" x2="11.8745" y2="5.5245" layer="21"/>
<rectangle x1="14.6177" y1="5.4991" x2="22.5171" y2="5.5245" layer="21"/>
<rectangle x1="4.1021" y1="5.5245" x2="11.8745" y2="5.5499" layer="21"/>
<rectangle x1="14.5923" y1="5.5245" x2="22.4917" y2="5.5499" layer="21"/>
<rectangle x1="4.1021" y1="5.5499" x2="11.8999" y2="5.5753" layer="21"/>
<rectangle x1="14.5923" y1="5.5499" x2="22.4917" y2="5.5753" layer="21"/>
<rectangle x1="4.1275" y1="5.5753" x2="11.8999" y2="5.6007" layer="21"/>
<rectangle x1="14.5669" y1="5.5753" x2="22.4663" y2="5.6007" layer="21"/>
<rectangle x1="4.1529" y1="5.6007" x2="11.9253" y2="5.6261" layer="21"/>
<rectangle x1="14.5415" y1="5.6007" x2="22.4409" y2="5.6261" layer="21"/>
<rectangle x1="4.1529" y1="5.6261" x2="11.9507" y2="5.6515" layer="21"/>
<rectangle x1="14.5415" y1="5.6261" x2="22.4409" y2="5.6515" layer="21"/>
<rectangle x1="4.1783" y1="5.6515" x2="11.9507" y2="5.6769" layer="21"/>
<rectangle x1="14.5161" y1="5.6515" x2="22.4155" y2="5.6769" layer="21"/>
<rectangle x1="4.2037" y1="5.6769" x2="11.9761" y2="5.7023" layer="21"/>
<rectangle x1="14.5161" y1="5.6769" x2="22.3901" y2="5.7023" layer="21"/>
<rectangle x1="4.2037" y1="5.7023" x2="12.0015" y2="5.7277" layer="21"/>
<rectangle x1="14.4907" y1="5.7023" x2="22.3901" y2="5.7277" layer="21"/>
<rectangle x1="4.2291" y1="5.7277" x2="12.0015" y2="5.7531" layer="21"/>
<rectangle x1="14.4653" y1="5.7277" x2="22.3647" y2="5.7531" layer="21"/>
<rectangle x1="4.2545" y1="5.7531" x2="12.0269" y2="5.7785" layer="21"/>
<rectangle x1="14.4653" y1="5.7531" x2="22.3393" y2="5.7785" layer="21"/>
<rectangle x1="4.2545" y1="5.7785" x2="12.0523" y2="5.8039" layer="21"/>
<rectangle x1="14.4399" y1="5.7785" x2="22.3393" y2="5.8039" layer="21"/>
<rectangle x1="4.2799" y1="5.8039" x2="12.0523" y2="5.8293" layer="21"/>
<rectangle x1="14.4399" y1="5.8039" x2="22.3139" y2="5.8293" layer="21"/>
<rectangle x1="4.3053" y1="5.8293" x2="12.0777" y2="5.8547" layer="21"/>
<rectangle x1="14.4145" y1="5.8293" x2="22.2885" y2="5.8547" layer="21"/>
<rectangle x1="4.3053" y1="5.8547" x2="12.0777" y2="5.8801" layer="21"/>
<rectangle x1="14.3891" y1="5.8547" x2="22.2885" y2="5.8801" layer="21"/>
<rectangle x1="4.3307" y1="5.8801" x2="12.1031" y2="5.9055" layer="21"/>
<rectangle x1="14.3891" y1="5.8801" x2="22.2631" y2="5.9055" layer="21"/>
<rectangle x1="4.3561" y1="5.9055" x2="12.1285" y2="5.9309" layer="21"/>
<rectangle x1="14.3637" y1="5.9055" x2="22.2377" y2="5.9309" layer="21"/>
<rectangle x1="4.3561" y1="5.9309" x2="12.1285" y2="5.9563" layer="21"/>
<rectangle x1="14.3637" y1="5.9309" x2="22.2377" y2="5.9563" layer="21"/>
<rectangle x1="4.3815" y1="5.9563" x2="12.1539" y2="5.9817" layer="21"/>
<rectangle x1="14.3383" y1="5.9563" x2="22.2123" y2="5.9817" layer="21"/>
<rectangle x1="4.4069" y1="5.9817" x2="12.1793" y2="6.0071" layer="21"/>
<rectangle x1="14.3129" y1="5.9817" x2="22.1869" y2="6.0071" layer="21"/>
<rectangle x1="4.4069" y1="6.0071" x2="12.1793" y2="6.0325" layer="21"/>
<rectangle x1="14.3129" y1="6.0071" x2="22.1869" y2="6.0325" layer="21"/>
<rectangle x1="4.4323" y1="6.0325" x2="12.2047" y2="6.0579" layer="21"/>
<rectangle x1="14.2875" y1="6.0325" x2="22.1615" y2="6.0579" layer="21"/>
<rectangle x1="4.4577" y1="6.0579" x2="12.2047" y2="6.0833" layer="21"/>
<rectangle x1="14.2875" y1="6.0579" x2="22.1361" y2="6.0833" layer="21"/>
<rectangle x1="4.4831" y1="6.0833" x2="12.2301" y2="6.1087" layer="21"/>
<rectangle x1="14.2621" y1="6.0833" x2="22.1361" y2="6.1087" layer="21"/>
<rectangle x1="4.4831" y1="6.1087" x2="12.2555" y2="6.1341" layer="21"/>
<rectangle x1="14.2621" y1="6.1087" x2="22.1107" y2="6.1341" layer="21"/>
<rectangle x1="4.5085" y1="6.1341" x2="12.2555" y2="6.1595" layer="21"/>
<rectangle x1="14.2367" y1="6.1341" x2="22.0853" y2="6.1595" layer="21"/>
<rectangle x1="4.5339" y1="6.1595" x2="12.2809" y2="6.1849" layer="21"/>
<rectangle x1="14.2113" y1="6.1595" x2="22.0853" y2="6.1849" layer="21"/>
<rectangle x1="4.5339" y1="6.1849" x2="12.3063" y2="6.2103" layer="21"/>
<rectangle x1="14.2113" y1="6.1849" x2="22.0599" y2="6.2103" layer="21"/>
<rectangle x1="4.5593" y1="6.2103" x2="12.3063" y2="6.2357" layer="21"/>
<rectangle x1="14.1859" y1="6.2103" x2="22.0345" y2="6.2357" layer="21"/>
<rectangle x1="4.5847" y1="6.2357" x2="12.3317" y2="6.2611" layer="21"/>
<rectangle x1="14.1859" y1="6.2357" x2="22.0345" y2="6.2611" layer="21"/>
<rectangle x1="4.5847" y1="6.2611" x2="12.3571" y2="6.2865" layer="21"/>
<rectangle x1="14.1605" y1="6.2611" x2="22.0091" y2="6.2865" layer="21"/>
<rectangle x1="4.6101" y1="6.2865" x2="12.3571" y2="6.3119" layer="21"/>
<rectangle x1="14.1351" y1="6.2865" x2="21.9837" y2="6.3119" layer="21"/>
<rectangle x1="4.6355" y1="6.3119" x2="12.3825" y2="6.3373" layer="21"/>
<rectangle x1="14.1351" y1="6.3119" x2="21.9837" y2="6.3373" layer="21"/>
<rectangle x1="4.6355" y1="6.3373" x2="12.3825" y2="6.3627" layer="21"/>
<rectangle x1="14.1097" y1="6.3373" x2="21.9583" y2="6.3627" layer="21"/>
<rectangle x1="4.6609" y1="6.3627" x2="12.4079" y2="6.3881" layer="21"/>
<rectangle x1="14.1097" y1="6.3627" x2="21.9329" y2="6.3881" layer="21"/>
<rectangle x1="4.6863" y1="6.3881" x2="12.4333" y2="6.4135" layer="21"/>
<rectangle x1="14.0843" y1="6.3881" x2="21.9329" y2="6.4135" layer="21"/>
<rectangle x1="4.6863" y1="6.4135" x2="12.4333" y2="6.4389" layer="21"/>
<rectangle x1="14.0589" y1="6.4135" x2="21.9075" y2="6.4389" layer="21"/>
<rectangle x1="4.7117" y1="6.4389" x2="12.4587" y2="6.4643" layer="21"/>
<rectangle x1="14.0589" y1="6.4389" x2="21.8821" y2="6.4643" layer="21"/>
<rectangle x1="4.7371" y1="6.4643" x2="12.4841" y2="6.4897" layer="21"/>
<rectangle x1="14.0335" y1="6.4643" x2="21.8821" y2="6.4897" layer="21"/>
<rectangle x1="4.7371" y1="6.4897" x2="12.4841" y2="6.5151" layer="21"/>
<rectangle x1="14.0335" y1="6.4897" x2="21.8567" y2="6.5151" layer="21"/>
<rectangle x1="4.7625" y1="6.5151" x2="12.5095" y2="6.5405" layer="21"/>
<rectangle x1="14.0081" y1="6.5151" x2="21.8313" y2="6.5405" layer="21"/>
<rectangle x1="4.7879" y1="6.5405" x2="12.5095" y2="6.5659" layer="21"/>
<rectangle x1="13.9827" y1="6.5405" x2="21.8313" y2="6.5659" layer="21"/>
<rectangle x1="4.7879" y1="6.5659" x2="12.5349" y2="6.5913" layer="21"/>
<rectangle x1="13.9827" y1="6.5659" x2="21.8059" y2="6.5913" layer="21"/>
<rectangle x1="4.8133" y1="6.5913" x2="12.5603" y2="6.6167" layer="21"/>
<rectangle x1="13.9573" y1="6.5913" x2="21.7805" y2="6.6167" layer="21"/>
<rectangle x1="4.8387" y1="6.6167" x2="12.5603" y2="6.6421" layer="21"/>
<rectangle x1="13.9573" y1="6.6167" x2="21.7805" y2="6.6421" layer="21"/>
<rectangle x1="4.8641" y1="6.6421" x2="12.5857" y2="6.6675" layer="21"/>
<rectangle x1="13.9319" y1="6.6421" x2="21.7551" y2="6.6675" layer="21"/>
<rectangle x1="4.8641" y1="6.6675" x2="12.6111" y2="6.6929" layer="21"/>
<rectangle x1="13.9065" y1="6.6675" x2="21.7297" y2="6.6929" layer="21"/>
<rectangle x1="4.8895" y1="6.6929" x2="12.6111" y2="6.7183" layer="21"/>
<rectangle x1="13.9065" y1="6.6929" x2="21.7297" y2="6.7183" layer="21"/>
<rectangle x1="4.9149" y1="6.7183" x2="12.6365" y2="6.7437" layer="21"/>
<rectangle x1="13.8811" y1="6.7183" x2="21.7043" y2="6.7437" layer="21"/>
<rectangle x1="4.9403" y1="6.7437" x2="12.6619" y2="6.7691" layer="21"/>
<rectangle x1="13.8811" y1="6.7437" x2="21.6789" y2="6.7691" layer="21"/>
<rectangle x1="4.9403" y1="6.7691" x2="12.6619" y2="6.7945" layer="21"/>
<rectangle x1="13.8557" y1="6.7691" x2="21.6789" y2="6.7945" layer="21"/>
<rectangle x1="4.9657" y1="6.7945" x2="12.6873" y2="6.8199" layer="21"/>
<rectangle x1="13.8557" y1="6.7945" x2="21.6535" y2="6.8199" layer="21"/>
<rectangle x1="4.9911" y1="6.8199" x2="12.6873" y2="6.8453" layer="21"/>
<rectangle x1="13.8303" y1="6.8199" x2="21.6281" y2="6.8453" layer="21"/>
<rectangle x1="5.0165" y1="6.8453" x2="12.7127" y2="6.8707" layer="21"/>
<rectangle x1="13.8049" y1="6.8453" x2="21.6281" y2="6.8707" layer="21"/>
<rectangle x1="5.0419" y1="6.8707" x2="12.7381" y2="6.8961" layer="21"/>
<rectangle x1="13.8049" y1="6.8707" x2="21.6027" y2="6.8961" layer="21"/>
<rectangle x1="5.0419" y1="6.8961" x2="12.7381" y2="6.9215" layer="21"/>
<rectangle x1="13.7795" y1="6.8961" x2="21.5773" y2="6.9215" layer="21"/>
<rectangle x1="5.0673" y1="6.9215" x2="12.7635" y2="6.9469" layer="21"/>
<rectangle x1="13.7795" y1="6.9215" x2="21.5773" y2="6.9469" layer="21"/>
<rectangle x1="5.0927" y1="6.9469" x2="12.7889" y2="6.9723" layer="21"/>
<rectangle x1="13.7541" y1="6.9469" x2="21.5519" y2="6.9723" layer="21"/>
<rectangle x1="5.1181" y1="6.9723" x2="12.7889" y2="6.9977" layer="21"/>
<rectangle x1="13.7287" y1="6.9723" x2="21.5265" y2="6.9977" layer="21"/>
<rectangle x1="5.1435" y1="6.9977" x2="12.8143" y2="7.0231" layer="21"/>
<rectangle x1="13.7287" y1="6.9977" x2="21.5265" y2="7.0231" layer="21"/>
<rectangle x1="5.1435" y1="7.0231" x2="12.8143" y2="7.0485" layer="21"/>
<rectangle x1="13.7033" y1="7.0231" x2="21.5011" y2="7.0485" layer="21"/>
<rectangle x1="5.1689" y1="7.0485" x2="12.8397" y2="7.0739" layer="21"/>
<rectangle x1="13.7033" y1="7.0485" x2="21.4757" y2="7.0739" layer="21"/>
<rectangle x1="5.1943" y1="7.0739" x2="12.8651" y2="7.0993" layer="21"/>
<rectangle x1="13.6779" y1="7.0739" x2="21.4757" y2="7.0993" layer="21"/>
<rectangle x1="5.2197" y1="7.0993" x2="12.8651" y2="7.1247" layer="21"/>
<rectangle x1="13.6525" y1="7.0993" x2="21.4503" y2="7.1247" layer="21"/>
<rectangle x1="5.2451" y1="7.1247" x2="12.8905" y2="7.1501" layer="21"/>
<rectangle x1="13.6525" y1="7.1247" x2="21.4249" y2="7.1501" layer="21"/>
<rectangle x1="5.2705" y1="7.1501" x2="12.9159" y2="7.1755" layer="21"/>
<rectangle x1="13.6271" y1="7.1501" x2="21.4249" y2="7.1755" layer="21"/>
<rectangle x1="5.2705" y1="7.1755" x2="12.9159" y2="7.2009" layer="21"/>
<rectangle x1="13.6271" y1="7.1755" x2="21.3995" y2="7.2009" layer="21"/>
<rectangle x1="5.2959" y1="7.2009" x2="12.9413" y2="7.2263" layer="21"/>
<rectangle x1="13.6017" y1="7.2009" x2="21.3741" y2="7.2263" layer="21"/>
<rectangle x1="5.3213" y1="7.2263" x2="12.9667" y2="7.2517" layer="21"/>
<rectangle x1="13.5763" y1="7.2263" x2="21.3741" y2="7.2517" layer="21"/>
<rectangle x1="5.3467" y1="7.2517" x2="12.9667" y2="7.2771" layer="21"/>
<rectangle x1="13.5763" y1="7.2517" x2="21.3487" y2="7.2771" layer="21"/>
<rectangle x1="5.3721" y1="7.2771" x2="12.9921" y2="7.3025" layer="21"/>
<rectangle x1="13.5509" y1="7.2771" x2="21.3233" y2="7.3025" layer="21"/>
<rectangle x1="5.3975" y1="7.3025" x2="12.9921" y2="7.3279" layer="21"/>
<rectangle x1="13.5509" y1="7.3025" x2="21.3233" y2="7.3279" layer="21"/>
<rectangle x1="5.4229" y1="7.3279" x2="13.0175" y2="7.3533" layer="21"/>
<rectangle x1="13.5255" y1="7.3279" x2="21.2979" y2="7.3533" layer="21"/>
<rectangle x1="5.4483" y1="7.3533" x2="13.0429" y2="7.3787" layer="21"/>
<rectangle x1="13.5001" y1="7.3533" x2="21.2725" y2="7.3787" layer="21"/>
<rectangle x1="5.4483" y1="7.3787" x2="13.0429" y2="7.4041" layer="21"/>
<rectangle x1="13.5001" y1="7.3787" x2="21.2725" y2="7.4041" layer="21"/>
<rectangle x1="5.4737" y1="7.4041" x2="13.0683" y2="7.4295" layer="21"/>
<rectangle x1="13.4747" y1="7.4041" x2="21.2471" y2="7.4295" layer="21"/>
<rectangle x1="5.4991" y1="7.4295" x2="13.0937" y2="7.4549" layer="21"/>
<rectangle x1="13.4747" y1="7.4295" x2="21.2217" y2="7.4549" layer="21"/>
<rectangle x1="5.5245" y1="7.4549" x2="13.0937" y2="7.4803" layer="21"/>
<rectangle x1="13.4493" y1="7.4549" x2="21.1963" y2="7.4803" layer="21"/>
<rectangle x1="5.5499" y1="7.4803" x2="13.1191" y2="7.5057" layer="21"/>
<rectangle x1="13.4239" y1="7.4803" x2="21.1963" y2="7.5057" layer="21"/>
<rectangle x1="5.5753" y1="7.5057" x2="13.1445" y2="7.5311" layer="21"/>
<rectangle x1="13.3985" y1="7.5057" x2="21.1709" y2="7.5311" layer="21"/>
<rectangle x1="5.6007" y1="7.5311" x2="13.1699" y2="7.5565" layer="21"/>
<rectangle x1="13.3731" y1="7.5311" x2="21.1455" y2="7.5565" layer="21"/>
<rectangle x1="5.6261" y1="7.5565" x2="13.2461" y2="7.5819" layer="21"/>
<rectangle x1="13.2969" y1="7.5565" x2="21.1455" y2="7.5819" layer="21"/>
<rectangle x1="5.6515" y1="7.5819" x2="21.1201" y2="7.6073" layer="21"/>
<rectangle x1="5.6769" y1="7.6073" x2="21.0947" y2="7.6327" layer="21"/>
<rectangle x1="5.7023" y1="7.6327" x2="21.0947" y2="7.6581" layer="21"/>
<rectangle x1="5.7277" y1="7.6581" x2="21.0693" y2="7.6835" layer="21"/>
<rectangle x1="5.7531" y1="7.6835" x2="21.0439" y2="7.7089" layer="21"/>
<rectangle x1="5.7785" y1="7.7089" x2="21.0439" y2="7.7343" layer="21"/>
<rectangle x1="5.8039" y1="7.7343" x2="21.0185" y2="7.7597" layer="21"/>
<rectangle x1="5.8293" y1="7.7597" x2="20.9931" y2="7.7851" layer="21"/>
<rectangle x1="5.8547" y1="7.7851" x2="20.9931" y2="7.8105" layer="21"/>
<rectangle x1="5.8801" y1="7.8105" x2="20.9677" y2="7.8359" layer="21"/>
<rectangle x1="5.9055" y1="7.8359" x2="20.9423" y2="7.8613" layer="21"/>
<rectangle x1="5.9309" y1="7.8613" x2="20.9423" y2="7.8867" layer="21"/>
<rectangle x1="5.9563" y1="7.8867" x2="20.9169" y2="7.9121" layer="21"/>
<rectangle x1="5.9817" y1="7.9121" x2="20.8915" y2="7.9375" layer="21"/>
<rectangle x1="6.0071" y1="7.9375" x2="20.8915" y2="7.9629" layer="21"/>
<rectangle x1="6.0325" y1="7.9629" x2="20.8661" y2="7.9883" layer="21"/>
<rectangle x1="6.0579" y1="7.9883" x2="20.8407" y2="8.0137" layer="21"/>
<rectangle x1="6.0833" y1="8.0137" x2="20.8407" y2="8.0391" layer="21"/>
<rectangle x1="6.1087" y1="8.0391" x2="20.8153" y2="8.0645" layer="21"/>
<rectangle x1="6.1341" y1="8.0645" x2="20.7899" y2="8.0899" layer="21"/>
<rectangle x1="6.1595" y1="8.0899" x2="20.7899" y2="8.1153" layer="21"/>
<rectangle x1="6.1849" y1="8.1153" x2="20.7645" y2="8.1407" layer="21"/>
<rectangle x1="6.2103" y1="8.1407" x2="20.7391" y2="8.1661" layer="21"/>
<rectangle x1="6.2357" y1="8.1661" x2="20.7391" y2="8.1915" layer="21"/>
<rectangle x1="6.2865" y1="8.1915" x2="20.7137" y2="8.2169" layer="21"/>
<rectangle x1="6.3119" y1="8.2169" x2="20.6883" y2="8.2423" layer="21"/>
<rectangle x1="6.3373" y1="8.2423" x2="20.6883" y2="8.2677" layer="21"/>
<rectangle x1="6.3627" y1="8.2677" x2="20.6629" y2="8.2931" layer="21"/>
<rectangle x1="6.3881" y1="8.2931" x2="20.6375" y2="8.3185" layer="21"/>
<rectangle x1="6.4135" y1="8.3185" x2="20.6375" y2="8.3439" layer="21"/>
<rectangle x1="6.4389" y1="8.3439" x2="20.6121" y2="8.3693" layer="21"/>
<rectangle x1="6.4897" y1="8.3693" x2="20.5867" y2="8.3947" layer="21"/>
<rectangle x1="6.5151" y1="8.3947" x2="20.5867" y2="8.4201" layer="21"/>
<rectangle x1="6.5405" y1="8.4201" x2="20.5613" y2="8.4455" layer="21"/>
<rectangle x1="6.5659" y1="8.4455" x2="20.5359" y2="8.4709" layer="21"/>
<rectangle x1="6.5913" y1="8.4709" x2="20.5359" y2="8.4963" layer="21"/>
<rectangle x1="6.6167" y1="8.4963" x2="20.5105" y2="8.5217" layer="21"/>
<rectangle x1="6.6675" y1="8.5217" x2="20.4851" y2="8.5471" layer="21"/>
<rectangle x1="6.6929" y1="8.5471" x2="20.4851" y2="8.5725" layer="21"/>
<rectangle x1="6.7183" y1="8.5725" x2="20.4597" y2="8.5979" layer="21"/>
<rectangle x1="6.7437" y1="8.5979" x2="20.4343" y2="8.6233" layer="21"/>
<rectangle x1="6.7691" y1="8.6233" x2="20.4343" y2="8.6487" layer="21"/>
<rectangle x1="6.8199" y1="8.6487" x2="20.4089" y2="8.6741" layer="21"/>
<rectangle x1="6.8453" y1="8.6741" x2="20.3835" y2="8.6995" layer="21"/>
<rectangle x1="6.8707" y1="8.6995" x2="20.3835" y2="8.7249" layer="21"/>
<rectangle x1="6.8961" y1="8.7249" x2="20.3581" y2="8.7503" layer="21"/>
<rectangle x1="6.9469" y1="8.7503" x2="20.3327" y2="8.7757" layer="21"/>
<rectangle x1="6.9723" y1="8.7757" x2="20.3327" y2="8.8011" layer="21"/>
<rectangle x1="6.9977" y1="8.8011" x2="20.3073" y2="8.8265" layer="21"/>
<rectangle x1="7.0485" y1="8.8265" x2="20.2819" y2="8.8519" layer="21"/>
<rectangle x1="7.0739" y1="8.8519" x2="20.2819" y2="8.8773" layer="21"/>
<rectangle x1="7.0993" y1="8.8773" x2="20.2565" y2="8.9027" layer="21"/>
<rectangle x1="7.1247" y1="8.9027" x2="20.2311" y2="8.9281" layer="21"/>
<rectangle x1="7.1755" y1="8.9281" x2="20.2311" y2="8.9535" layer="21"/>
<rectangle x1="7.2009" y1="8.9535" x2="20.2057" y2="8.9789" layer="21"/>
<rectangle x1="7.2263" y1="8.9789" x2="20.1803" y2="9.0043" layer="21"/>
<rectangle x1="7.2771" y1="9.0043" x2="20.1803" y2="9.0297" layer="21"/>
<rectangle x1="7.3025" y1="9.0297" x2="20.1549" y2="9.0551" layer="21"/>
<rectangle x1="7.3279" y1="9.0551" x2="20.1295" y2="9.0805" layer="21"/>
<rectangle x1="7.3787" y1="9.0805" x2="20.1295" y2="9.1059" layer="21"/>
<rectangle x1="7.4041" y1="9.1059" x2="20.1041" y2="9.1313" layer="21"/>
<rectangle x1="7.4549" y1="9.1313" x2="20.0787" y2="9.1567" layer="21"/>
<rectangle x1="7.4803" y1="9.1567" x2="20.0787" y2="9.1821" layer="21"/>
<rectangle x1="7.5057" y1="9.1821" x2="20.0533" y2="9.2075" layer="21"/>
<rectangle x1="7.5565" y1="9.2075" x2="20.0279" y2="9.2329" layer="21"/>
<rectangle x1="7.5819" y1="9.2329" x2="20.0279" y2="9.2583" layer="21"/>
<rectangle x1="7.6327" y1="9.2583" x2="20.0025" y2="9.2837" layer="21"/>
<rectangle x1="7.6581" y1="9.2837" x2="19.9771" y2="9.3091" layer="21"/>
<rectangle x1="7.6835" y1="9.3091" x2="19.9771" y2="9.3345" layer="21"/>
<rectangle x1="7.7343" y1="9.3345" x2="19.9517" y2="9.3599" layer="21"/>
<rectangle x1="7.7597" y1="9.3599" x2="19.9263" y2="9.3853" layer="21"/>
<rectangle x1="7.8105" y1="9.3853" x2="19.9263" y2="9.4107" layer="21"/>
<rectangle x1="7.8359" y1="9.4107" x2="19.9009" y2="9.4361" layer="21"/>
<rectangle x1="7.8867" y1="9.4361" x2="19.8755" y2="9.4615" layer="21"/>
<rectangle x1="7.9121" y1="9.4615" x2="19.8755" y2="9.4869" layer="21"/>
<rectangle x1="7.9629" y1="9.4869" x2="19.8501" y2="9.5123" layer="21"/>
<rectangle x1="7.9883" y1="9.5123" x2="19.8247" y2="9.5377" layer="21"/>
<rectangle x1="8.0391" y1="9.5377" x2="19.8247" y2="9.5631" layer="21"/>
<rectangle x1="8.0645" y1="9.5631" x2="19.7993" y2="9.5885" layer="21"/>
<rectangle x1="8.1153" y1="9.5885" x2="19.7739" y2="9.6139" layer="21"/>
<rectangle x1="8.1407" y1="9.6139" x2="19.7739" y2="9.6393" layer="21"/>
<rectangle x1="8.1915" y1="9.6393" x2="19.7485" y2="9.6647" layer="21"/>
<rectangle x1="8.2169" y1="9.6647" x2="19.7231" y2="9.6901" layer="21"/>
<rectangle x1="8.2677" y1="9.6901" x2="19.7231" y2="9.7155" layer="21"/>
<rectangle x1="8.3185" y1="9.7155" x2="19.6977" y2="9.7409" layer="21"/>
<rectangle x1="8.3439" y1="9.7409" x2="19.6723" y2="9.7663" layer="21"/>
<rectangle x1="8.3947" y1="9.7663" x2="19.6723" y2="9.7917" layer="21"/>
<rectangle x1="8.4201" y1="9.7917" x2="19.6469" y2="9.8171" layer="21"/>
<rectangle x1="8.4709" y1="9.8171" x2="19.6215" y2="9.8425" layer="21"/>
<rectangle x1="8.5217" y1="9.8425" x2="19.6215" y2="9.8679" layer="21"/>
<rectangle x1="8.5471" y1="9.8679" x2="19.5961" y2="9.8933" layer="21"/>
<rectangle x1="8.5979" y1="9.8933" x2="19.5707" y2="9.9187" layer="21"/>
<rectangle x1="8.6233" y1="9.9187" x2="19.5453" y2="9.9441" layer="21"/>
<rectangle x1="8.6741" y1="9.9441" x2="19.5453" y2="9.9695" layer="21"/>
<rectangle x1="8.7249" y1="9.9695" x2="19.5199" y2="9.9949" layer="21"/>
<rectangle x1="8.7503" y1="9.9949" x2="19.4945" y2="10.0203" layer="21"/>
<rectangle x1="8.8011" y1="10.0203" x2="19.4945" y2="10.0457" layer="21"/>
<rectangle x1="8.8519" y1="10.0457" x2="19.4691" y2="10.0711" layer="21"/>
<rectangle x1="8.9027" y1="10.0711" x2="19.4437" y2="10.0965" layer="21"/>
<rectangle x1="8.9281" y1="10.0965" x2="19.4437" y2="10.1219" layer="21"/>
<rectangle x1="8.9789" y1="10.1219" x2="19.4183" y2="10.1473" layer="21"/>
<rectangle x1="9.0297" y1="10.1473" x2="19.3929" y2="10.1727" layer="21"/>
<rectangle x1="9.0551" y1="10.1727" x2="19.3929" y2="10.1981" layer="21"/>
<rectangle x1="9.1059" y1="10.1981" x2="19.3675" y2="10.2235" layer="21"/>
<rectangle x1="9.1567" y1="10.2235" x2="19.3421" y2="10.2489" layer="21"/>
<rectangle x1="9.2075" y1="10.2489" x2="19.3421" y2="10.2743" layer="21"/>
<rectangle x1="9.2583" y1="10.2743" x2="19.3167" y2="10.2997" layer="21"/>
<rectangle x1="9.2837" y1="10.2997" x2="19.2913" y2="10.3251" layer="21"/>
<rectangle x1="9.3345" y1="10.3251" x2="19.2913" y2="10.3505" layer="21"/>
<rectangle x1="9.3853" y1="10.3505" x2="19.2659" y2="10.3759" layer="21"/>
<rectangle x1="9.4361" y1="10.3759" x2="19.2405" y2="10.4013" layer="21"/>
<rectangle x1="9.4869" y1="10.4013" x2="19.2405" y2="10.4267" layer="21"/>
<rectangle x1="9.5123" y1="10.4267" x2="19.2151" y2="10.4521" layer="21"/>
<rectangle x1="9.5631" y1="10.4521" x2="19.1897" y2="10.4775" layer="21"/>
<rectangle x1="9.6139" y1="10.4775" x2="19.1897" y2="10.5029" layer="21"/>
<rectangle x1="9.6647" y1="10.5029" x2="19.1643" y2="10.5283" layer="21"/>
<rectangle x1="9.7155" y1="10.5283" x2="19.1389" y2="10.5537" layer="21"/>
<rectangle x1="9.7663" y1="10.5537" x2="19.1389" y2="10.5791" layer="21"/>
<rectangle x1="9.8171" y1="10.5791" x2="19.1135" y2="10.6045" layer="21"/>
<rectangle x1="9.8679" y1="10.6045" x2="19.0881" y2="10.6299" layer="21"/>
<rectangle x1="9.9187" y1="10.6299" x2="19.0881" y2="10.6553" layer="21"/>
<rectangle x1="9.9695" y1="10.6553" x2="19.0627" y2="10.6807" layer="21"/>
<rectangle x1="10.0203" y1="10.6807" x2="19.0373" y2="10.7061" layer="21"/>
<rectangle x1="10.0711" y1="10.7061" x2="19.0373" y2="10.7315" layer="21"/>
<rectangle x1="10.1219" y1="10.7315" x2="19.0119" y2="10.7569" layer="21"/>
<rectangle x1="10.1727" y1="10.7569" x2="18.9865" y2="10.7823" layer="21"/>
<rectangle x1="10.2235" y1="10.7823" x2="18.9865" y2="10.8077" layer="21"/>
<rectangle x1="10.2743" y1="10.8077" x2="18.9611" y2="10.8331" layer="21"/>
<rectangle x1="10.3251" y1="10.8331" x2="18.9357" y2="10.8585" layer="21"/>
<rectangle x1="10.3759" y1="10.8585" x2="18.9357" y2="10.8839" layer="21"/>
<rectangle x1="10.4267" y1="10.8839" x2="18.9103" y2="10.9093" layer="21"/>
<rectangle x1="10.4775" y1="10.9093" x2="18.8849" y2="10.9347" layer="21"/>
<rectangle x1="10.5283" y1="10.9347" x2="18.8849" y2="10.9601" layer="21"/>
<rectangle x1="10.5791" y1="10.9601" x2="18.8595" y2="10.9855" layer="21"/>
<rectangle x1="10.6299" y1="10.9855" x2="18.8341" y2="11.0109" layer="21"/>
<rectangle x1="10.6807" y1="11.0109" x2="18.8341" y2="11.0363" layer="21"/>
<rectangle x1="10.7315" y1="11.0363" x2="18.8087" y2="11.0617" layer="21"/>
<rectangle x1="10.7823" y1="11.0617" x2="18.7833" y2="11.0871" layer="21"/>
<rectangle x1="10.8585" y1="11.0871" x2="18.7833" y2="11.1125" layer="21"/>
<rectangle x1="10.9093" y1="11.1125" x2="18.7579" y2="11.1379" layer="21"/>
<rectangle x1="10.9601" y1="11.1379" x2="18.7325" y2="11.1633" layer="21"/>
<rectangle x1="11.0109" y1="11.1633" x2="18.7325" y2="11.1887" layer="21"/>
<rectangle x1="11.0617" y1="11.1887" x2="18.7071" y2="11.2141" layer="21"/>
<rectangle x1="11.1379" y1="11.2141" x2="18.6817" y2="11.2395" layer="21"/>
<rectangle x1="11.1887" y1="11.2395" x2="18.6817" y2="11.2649" layer="21"/>
<rectangle x1="11.2395" y1="11.2649" x2="18.6563" y2="11.2903" layer="21"/>
<rectangle x1="11.2903" y1="11.2903" x2="18.6309" y2="11.3157" layer="21"/>
<rectangle x1="11.3665" y1="11.3157" x2="18.6309" y2="11.3411" layer="21"/>
<rectangle x1="11.4173" y1="11.3411" x2="18.6055" y2="11.3665" layer="21"/>
<rectangle x1="11.4681" y1="11.3665" x2="18.5801" y2="11.3919" layer="21"/>
<rectangle x1="11.5443" y1="11.3919" x2="18.5801" y2="11.4173" layer="21"/>
<rectangle x1="11.5951" y1="11.4173" x2="18.5547" y2="11.4427" layer="21"/>
<rectangle x1="11.6459" y1="11.4427" x2="18.5293" y2="11.4681" layer="21"/>
<rectangle x1="11.7221" y1="11.4681" x2="18.5293" y2="11.4935" layer="21"/>
<rectangle x1="11.7729" y1="11.4935" x2="18.5039" y2="11.5189" layer="21"/>
<rectangle x1="11.8491" y1="11.5189" x2="18.4785" y2="11.5443" layer="21"/>
<rectangle x1="11.8999" y1="11.5443" x2="18.4785" y2="11.5697" layer="21"/>
<rectangle x1="11.9507" y1="11.5697" x2="18.4531" y2="11.5951" layer="21"/>
<rectangle x1="12.0269" y1="11.5951" x2="18.4277" y2="11.6205" layer="21"/>
<rectangle x1="12.0777" y1="11.6205" x2="18.4277" y2="11.6459" layer="21"/>
<rectangle x1="12.1539" y1="11.6459" x2="18.4023" y2="11.6713" layer="21"/>
<rectangle x1="12.2047" y1="11.6713" x2="18.3769" y2="11.6967" layer="21"/>
<rectangle x1="12.2809" y1="11.6967" x2="18.3769" y2="11.7221" layer="21"/>
<rectangle x1="12.3317" y1="11.7221" x2="18.3515" y2="11.7475" layer="21"/>
<rectangle x1="12.4079" y1="11.7475" x2="18.3261" y2="11.7729" layer="21"/>
<rectangle x1="12.4841" y1="11.7729" x2="18.3261" y2="11.7983" layer="21"/>
<rectangle x1="12.5349" y1="11.7983" x2="18.3007" y2="11.8237" layer="21"/>
<rectangle x1="12.6111" y1="11.8237" x2="18.2753" y2="11.8491" layer="21"/>
<rectangle x1="12.6619" y1="11.8491" x2="18.2753" y2="11.8745" layer="21"/>
<rectangle x1="12.7381" y1="11.8745" x2="18.2499" y2="11.8999" layer="21"/>
<rectangle x1="12.8143" y1="11.8999" x2="18.2245" y2="11.9253" layer="21"/>
<rectangle x1="12.8905" y1="11.9253" x2="18.2245" y2="11.9507" layer="21"/>
<rectangle x1="12.9413" y1="11.9507" x2="18.1991" y2="11.9761" layer="21"/>
<rectangle x1="13.0175" y1="11.9761" x2="18.1737" y2="12.0015" layer="21"/>
<rectangle x1="13.0937" y1="12.0015" x2="18.1737" y2="12.0269" layer="21"/>
<rectangle x1="13.1699" y1="12.0269" x2="18.1483" y2="12.0523" layer="21"/>
<rectangle x1="13.2207" y1="12.0523" x2="18.1229" y2="12.0777" layer="21"/>
<rectangle x1="13.2969" y1="12.0777" x2="18.1229" y2="12.1031" layer="21"/>
<rectangle x1="10.7569" y1="12.1031" x2="10.8331" y2="12.1285" layer="21"/>
<rectangle x1="13.3731" y1="12.1031" x2="18.0975" y2="12.1285" layer="21"/>
<rectangle x1="10.8077" y1="12.1285" x2="10.9601" y2="12.1539" layer="21"/>
<rectangle x1="13.4493" y1="12.1285" x2="18.0721" y2="12.1539" layer="21"/>
<rectangle x1="10.8331" y1="12.1539" x2="11.0871" y2="12.1793" layer="21"/>
<rectangle x1="13.5255" y1="12.1539" x2="18.0721" y2="12.1793" layer="21"/>
<rectangle x1="10.8839" y1="12.1793" x2="11.2141" y2="12.2047" layer="21"/>
<rectangle x1="13.6017" y1="12.1793" x2="18.0467" y2="12.2047" layer="21"/>
<rectangle x1="10.9347" y1="12.2047" x2="11.3665" y2="12.2301" layer="21"/>
<rectangle x1="13.6779" y1="12.2047" x2="18.0213" y2="12.2301" layer="21"/>
<rectangle x1="10.9601" y1="12.2301" x2="11.4935" y2="12.2555" layer="21"/>
<rectangle x1="13.7541" y1="12.2301" x2="18.0213" y2="12.2555" layer="21"/>
<rectangle x1="11.0109" y1="12.2555" x2="11.6459" y2="12.2809" layer="21"/>
<rectangle x1="13.8303" y1="12.2555" x2="17.9959" y2="12.2809" layer="21"/>
<rectangle x1="11.0617" y1="12.2809" x2="11.7729" y2="12.3063" layer="21"/>
<rectangle x1="13.9065" y1="12.2809" x2="17.9705" y2="12.3063" layer="21"/>
<rectangle x1="11.0871" y1="12.3063" x2="11.9253" y2="12.3317" layer="21"/>
<rectangle x1="13.9827" y1="12.3063" x2="17.9705" y2="12.3317" layer="21"/>
<rectangle x1="11.1379" y1="12.3317" x2="12.0523" y2="12.3571" layer="21"/>
<rectangle x1="14.0589" y1="12.3317" x2="17.9451" y2="12.3571" layer="21"/>
<rectangle x1="11.1887" y1="12.3571" x2="12.2047" y2="12.3825" layer="21"/>
<rectangle x1="14.1351" y1="12.3571" x2="17.9197" y2="12.3825" layer="21"/>
<rectangle x1="11.2395" y1="12.3825" x2="12.3571" y2="12.4079" layer="21"/>
<rectangle x1="14.2367" y1="12.3825" x2="17.8943" y2="12.4079" layer="21"/>
<rectangle x1="11.2649" y1="12.4079" x2="12.5095" y2="12.4333" layer="21"/>
<rectangle x1="14.3129" y1="12.4079" x2="17.8943" y2="12.4333" layer="21"/>
<rectangle x1="11.3157" y1="12.4333" x2="12.6873" y2="12.4587" layer="21"/>
<rectangle x1="14.3891" y1="12.4333" x2="17.8689" y2="12.4587" layer="21"/>
<rectangle x1="11.3665" y1="12.4587" x2="12.8397" y2="12.4841" layer="21"/>
<rectangle x1="14.4907" y1="12.4587" x2="17.8435" y2="12.4841" layer="21"/>
<rectangle x1="11.3919" y1="12.4841" x2="13.0175" y2="12.5095" layer="21"/>
<rectangle x1="14.5669" y1="12.4841" x2="17.8435" y2="12.5095" layer="21"/>
<rectangle x1="11.4427" y1="12.5095" x2="13.1953" y2="12.5349" layer="21"/>
<rectangle x1="14.6431" y1="12.5095" x2="17.8181" y2="12.5349" layer="21"/>
<rectangle x1="11.4935" y1="12.5349" x2="13.3731" y2="12.5603" layer="21"/>
<rectangle x1="14.7447" y1="12.5349" x2="17.8435" y2="12.5603" layer="21"/>
<rectangle x1="11.5443" y1="12.5603" x2="13.5763" y2="12.5857" layer="21"/>
<rectangle x1="14.8209" y1="12.5603" x2="17.9197" y2="12.5857" layer="21"/>
<rectangle x1="11.5951" y1="12.5857" x2="13.7795" y2="12.6111" layer="21"/>
<rectangle x1="14.9225" y1="12.5857" x2="17.9705" y2="12.6111" layer="21"/>
<rectangle x1="11.6205" y1="12.6111" x2="14.0081" y2="12.6365" layer="21"/>
<rectangle x1="15.0241" y1="12.6111" x2="18.0467" y2="12.6365" layer="21"/>
<rectangle x1="11.6713" y1="12.6365" x2="14.3129" y2="12.6619" layer="21"/>
<rectangle x1="15.0749" y1="12.6365" x2="18.0975" y2="12.6619" layer="21"/>
<rectangle x1="11.7221" y1="12.6619" x2="18.1737" y2="12.6873" layer="21"/>
<rectangle x1="11.7729" y1="12.6873" x2="18.2245" y2="12.7127" layer="21"/>
<rectangle x1="11.8237" y1="12.7127" x2="18.3007" y2="12.7381" layer="21"/>
<rectangle x1="11.8491" y1="12.7381" x2="18.3769" y2="12.7635" layer="21"/>
<rectangle x1="11.8999" y1="12.7635" x2="18.4277" y2="12.7889" layer="21"/>
<rectangle x1="11.9507" y1="12.7889" x2="18.5039" y2="12.8143" layer="21"/>
<rectangle x1="12.0015" y1="12.8143" x2="18.5801" y2="12.8397" layer="21"/>
<rectangle x1="12.0523" y1="12.8397" x2="18.6563" y2="12.8651" layer="21"/>
<rectangle x1="12.1031" y1="12.8651" x2="18.7325" y2="12.8905" layer="21"/>
<rectangle x1="12.1539" y1="12.8905" x2="18.8087" y2="12.9159" layer="21"/>
<rectangle x1="12.1793" y1="12.9159" x2="18.8849" y2="12.9413" layer="21"/>
<rectangle x1="12.2301" y1="12.9413" x2="18.9865" y2="12.9667" layer="21"/>
<rectangle x1="12.2809" y1="12.9667" x2="19.0627" y2="12.9921" layer="21"/>
<rectangle x1="12.3317" y1="12.9921" x2="19.1389" y2="13.0175" layer="21"/>
<rectangle x1="21.6535" y1="12.9921" x2="21.7043" y2="13.0175" layer="21"/>
<rectangle x1="12.3825" y1="13.0175" x2="19.2405" y2="13.0429" layer="21"/>
<rectangle x1="21.6281" y1="13.0175" x2="21.7297" y2="13.0429" layer="21"/>
<rectangle x1="12.4333" y1="13.0429" x2="19.3421" y2="13.0683" layer="21"/>
<rectangle x1="21.5773" y1="13.0429" x2="21.7297" y2="13.0683" layer="21"/>
<rectangle x1="12.4841" y1="13.0683" x2="19.4437" y2="13.0937" layer="21"/>
<rectangle x1="21.5011" y1="13.0683" x2="21.7297" y2="13.0937" layer="21"/>
<rectangle x1="12.5349" y1="13.0937" x2="19.5707" y2="13.1191" layer="21"/>
<rectangle x1="21.4503" y1="13.0937" x2="21.7551" y2="13.1191" layer="21"/>
<rectangle x1="12.5857" y1="13.1191" x2="19.6723" y2="13.1445" layer="21"/>
<rectangle x1="21.3741" y1="13.1191" x2="21.7551" y2="13.1445" layer="21"/>
<rectangle x1="12.6365" y1="13.1445" x2="19.8247" y2="13.1699" layer="21"/>
<rectangle x1="21.2725" y1="13.1445" x2="21.7805" y2="13.1699" layer="21"/>
<rectangle x1="12.6873" y1="13.1699" x2="19.9771" y2="13.1953" layer="21"/>
<rectangle x1="21.1709" y1="13.1699" x2="21.7805" y2="13.1953" layer="21"/>
<rectangle x1="12.7381" y1="13.1953" x2="20.1803" y2="13.2207" layer="21"/>
<rectangle x1="21.0185" y1="13.1953" x2="21.7805" y2="13.2207" layer="21"/>
<rectangle x1="12.7889" y1="13.2207" x2="20.5359" y2="13.2461" layer="21"/>
<rectangle x1="20.7137" y1="13.2207" x2="21.8059" y2="13.2461" layer="21"/>
<rectangle x1="12.8397" y1="13.2461" x2="21.8059" y2="13.2715" layer="21"/>
<rectangle x1="12.8905" y1="13.2715" x2="21.8059" y2="13.2969" layer="21"/>
<rectangle x1="12.9413" y1="13.2969" x2="21.8313" y2="13.3223" layer="21"/>
<rectangle x1="12.9921" y1="13.3223" x2="21.8313" y2="13.3477" layer="21"/>
<rectangle x1="13.0429" y1="13.3477" x2="21.8313" y2="13.3731" layer="21"/>
<rectangle x1="13.0937" y1="13.3731" x2="21.8567" y2="13.3985" layer="21"/>
<rectangle x1="13.1699" y1="13.3985" x2="21.8567" y2="13.4239" layer="21"/>
<rectangle x1="13.2207" y1="13.4239" x2="21.8567" y2="13.4493" layer="21"/>
<rectangle x1="13.2715" y1="13.4493" x2="21.8821" y2="13.4747" layer="21"/>
<rectangle x1="13.3223" y1="13.4747" x2="21.8821" y2="13.5001" layer="21"/>
<rectangle x1="13.3731" y1="13.5001" x2="21.8821" y2="13.5255" layer="21"/>
<rectangle x1="13.4239" y1="13.5255" x2="21.9075" y2="13.5509" layer="21"/>
<rectangle x1="13.5001" y1="13.5509" x2="21.9075" y2="13.5763" layer="21"/>
<rectangle x1="13.5509" y1="13.5763" x2="21.9075" y2="13.6017" layer="21"/>
<rectangle x1="13.6017" y1="13.6017" x2="21.9075" y2="13.6271" layer="21"/>
<rectangle x1="13.6525" y1="13.6271" x2="21.9075" y2="13.6525" layer="21"/>
<rectangle x1="13.7287" y1="13.6525" x2="21.9329" y2="13.6779" layer="21"/>
<rectangle x1="13.7795" y1="13.6779" x2="21.9329" y2="13.7033" layer="21"/>
<rectangle x1="13.8303" y1="13.7033" x2="21.9329" y2="13.7287" layer="21"/>
<rectangle x1="13.9065" y1="13.7287" x2="21.9329" y2="13.7541" layer="21"/>
<rectangle x1="13.9573" y1="13.7541" x2="21.9329" y2="13.7795" layer="21"/>
<rectangle x1="14.0081" y1="13.7795" x2="21.9583" y2="13.8049" layer="21"/>
<rectangle x1="14.0843" y1="13.8049" x2="21.5265" y2="13.8303" layer="21"/>
<rectangle x1="21.5519" y1="13.8049" x2="21.9583" y2="13.8303" layer="21"/>
<rectangle x1="14.1351" y1="13.8303" x2="21.5011" y2="13.8557" layer="21"/>
<rectangle x1="21.5773" y1="13.8303" x2="21.9583" y2="13.8557" layer="21"/>
<rectangle x1="14.2113" y1="13.8557" x2="21.5011" y2="13.8811" layer="21"/>
<rectangle x1="21.5773" y1="13.8557" x2="21.9583" y2="13.8811" layer="21"/>
<rectangle x1="14.2621" y1="13.8811" x2="21.4757" y2="13.9065" layer="21"/>
<rectangle x1="21.5773" y1="13.8811" x2="21.9583" y2="13.9065" layer="21"/>
<rectangle x1="14.3383" y1="13.9065" x2="21.4249" y2="13.9319" layer="21"/>
<rectangle x1="21.5773" y1="13.9065" x2="21.9583" y2="13.9319" layer="21"/>
<rectangle x1="14.3891" y1="13.9319" x2="21.3995" y2="13.9573" layer="21"/>
<rectangle x1="21.5773" y1="13.9319" x2="21.9583" y2="13.9573" layer="21"/>
<rectangle x1="14.4653" y1="13.9573" x2="21.3741" y2="13.9827" layer="21"/>
<rectangle x1="21.5773" y1="13.9573" x2="21.9583" y2="13.9827" layer="21"/>
<rectangle x1="14.5415" y1="13.9827" x2="21.3487" y2="14.0081" layer="21"/>
<rectangle x1="21.5773" y1="13.9827" x2="21.9837" y2="14.0081" layer="21"/>
<rectangle x1="14.5923" y1="14.0081" x2="21.3233" y2="14.0335" layer="21"/>
<rectangle x1="21.6027" y1="14.0081" x2="21.9837" y2="14.0335" layer="21"/>
<rectangle x1="14.6685" y1="14.0335" x2="21.2725" y2="14.0589" layer="21"/>
<rectangle x1="21.6027" y1="14.0335" x2="21.9837" y2="14.0589" layer="21"/>
<rectangle x1="14.7447" y1="14.0589" x2="21.2471" y2="14.0843" layer="21"/>
<rectangle x1="21.6027" y1="14.0589" x2="21.9837" y2="14.0843" layer="21"/>
<rectangle x1="14.7955" y1="14.0843" x2="21.1963" y2="14.1097" layer="21"/>
<rectangle x1="21.6027" y1="14.0843" x2="21.9837" y2="14.1097" layer="21"/>
<rectangle x1="14.8717" y1="14.1097" x2="21.1709" y2="14.1351" layer="21"/>
<rectangle x1="21.6027" y1="14.1097" x2="21.9837" y2="14.1351" layer="21"/>
<rectangle x1="14.9479" y1="14.1351" x2="21.1201" y2="14.1605" layer="21"/>
<rectangle x1="21.6027" y1="14.1351" x2="21.9837" y2="14.1605" layer="21"/>
<rectangle x1="15.0241" y1="14.1605" x2="21.0947" y2="14.1859" layer="21"/>
<rectangle x1="21.6027" y1="14.1605" x2="21.9837" y2="14.1859" layer="21"/>
<rectangle x1="15.1003" y1="14.1859" x2="21.0439" y2="14.2113" layer="21"/>
<rectangle x1="21.6027" y1="14.1859" x2="21.9837" y2="14.2113" layer="21"/>
<rectangle x1="15.1765" y1="14.2113" x2="20.9931" y2="14.2367" layer="21"/>
<rectangle x1="21.6027" y1="14.2113" x2="21.9837" y2="14.2367" layer="21"/>
<rectangle x1="15.2527" y1="14.2367" x2="20.9423" y2="14.2621" layer="21"/>
<rectangle x1="21.5773" y1="14.2367" x2="21.9837" y2="14.2621" layer="21"/>
<rectangle x1="15.3289" y1="14.2621" x2="20.8915" y2="14.2875" layer="21"/>
<rectangle x1="21.5773" y1="14.2621" x2="21.9583" y2="14.2875" layer="21"/>
<rectangle x1="15.4051" y1="14.2875" x2="20.8407" y2="14.3129" layer="21"/>
<rectangle x1="21.5773" y1="14.2875" x2="21.9583" y2="14.3129" layer="21"/>
<rectangle x1="15.4813" y1="14.3129" x2="20.7899" y2="14.3383" layer="21"/>
<rectangle x1="21.5773" y1="14.3129" x2="21.9583" y2="14.3383" layer="21"/>
<rectangle x1="15.5575" y1="14.3383" x2="20.7391" y2="14.3637" layer="21"/>
<rectangle x1="21.5773" y1="14.3383" x2="21.9583" y2="14.3637" layer="21"/>
<rectangle x1="15.6337" y1="14.3637" x2="20.6883" y2="14.3891" layer="21"/>
<rectangle x1="21.5773" y1="14.3637" x2="21.9583" y2="14.3891" layer="21"/>
<rectangle x1="15.7353" y1="14.3891" x2="20.6375" y2="14.4145" layer="21"/>
<rectangle x1="21.5519" y1="14.3891" x2="21.9583" y2="14.4145" layer="21"/>
<rectangle x1="15.8115" y1="14.4145" x2="20.5867" y2="14.4399" layer="21"/>
<rectangle x1="21.5519" y1="14.4145" x2="21.9583" y2="14.4399" layer="21"/>
<rectangle x1="15.9131" y1="14.4399" x2="20.5105" y2="14.4653" layer="21"/>
<rectangle x1="21.5519" y1="14.4399" x2="21.9583" y2="14.4653" layer="21"/>
<rectangle x1="16.0147" y1="14.4653" x2="20.4597" y2="14.4907" layer="21"/>
<rectangle x1="21.5519" y1="14.4653" x2="21.9329" y2="14.4907" layer="21"/>
<rectangle x1="16.0909" y1="14.4907" x2="20.3835" y2="14.5161" layer="21"/>
<rectangle x1="21.5265" y1="14.4907" x2="21.9329" y2="14.5161" layer="21"/>
<rectangle x1="16.1925" y1="14.5161" x2="20.3327" y2="14.5415" layer="21"/>
<rectangle x1="21.5265" y1="14.5161" x2="21.9329" y2="14.5415" layer="21"/>
<rectangle x1="16.2941" y1="14.5415" x2="20.2565" y2="14.5669" layer="21"/>
<rectangle x1="21.5265" y1="14.5415" x2="21.9329" y2="14.5669" layer="21"/>
<rectangle x1="16.3957" y1="14.5669" x2="20.1803" y2="14.5923" layer="21"/>
<rectangle x1="21.5011" y1="14.5669" x2="21.9075" y2="14.5923" layer="21"/>
<rectangle x1="16.5227" y1="14.5923" x2="20.1041" y2="14.6177" layer="21"/>
<rectangle x1="21.5011" y1="14.5923" x2="21.9075" y2="14.6177" layer="21"/>
<rectangle x1="16.6243" y1="14.6177" x2="20.0279" y2="14.6431" layer="21"/>
<rectangle x1="21.5011" y1="14.6177" x2="21.9075" y2="14.6431" layer="21"/>
<rectangle x1="16.7513" y1="14.6431" x2="19.9517" y2="14.6685" layer="21"/>
<rectangle x1="21.4757" y1="14.6431" x2="21.8821" y2="14.6685" layer="21"/>
<rectangle x1="16.8783" y1="14.6685" x2="19.8501" y2="14.6939" layer="21"/>
<rectangle x1="21.4757" y1="14.6685" x2="21.8821" y2="14.6939" layer="21"/>
<rectangle x1="17.0307" y1="14.6939" x2="19.7485" y2="14.7193" layer="21"/>
<rectangle x1="21.4503" y1="14.6939" x2="21.8821" y2="14.7193" layer="21"/>
<rectangle x1="17.1831" y1="14.7193" x2="19.6215" y2="14.7447" layer="21"/>
<rectangle x1="21.4503" y1="14.7193" x2="21.8567" y2="14.7447" layer="21"/>
<rectangle x1="17.3355" y1="14.7447" x2="19.4945" y2="14.7701" layer="21"/>
<rectangle x1="21.4503" y1="14.7447" x2="21.8567" y2="14.7701" layer="21"/>
<rectangle x1="17.5387" y1="14.7701" x2="19.3421" y2="14.7955" layer="21"/>
<rectangle x1="21.4249" y1="14.7701" x2="21.8313" y2="14.7955" layer="21"/>
<rectangle x1="17.7673" y1="14.7955" x2="19.1389" y2="14.8209" layer="21"/>
<rectangle x1="21.4249" y1="14.7955" x2="21.8313" y2="14.8209" layer="21"/>
<rectangle x1="18.1483" y1="14.8209" x2="18.7833" y2="14.8463" layer="21"/>
<rectangle x1="21.3995" y1="14.8209" x2="21.8059" y2="14.8463" layer="21"/>
<rectangle x1="21.3741" y1="14.8463" x2="21.8059" y2="14.8717" layer="21"/>
<rectangle x1="21.3741" y1="14.8717" x2="21.7805" y2="14.8971" layer="21"/>
<rectangle x1="21.3487" y1="14.8971" x2="21.7805" y2="14.9225" layer="21"/>
<rectangle x1="21.3233" y1="14.9225" x2="21.7551" y2="14.9479" layer="21"/>
<rectangle x1="21.3233" y1="14.9479" x2="21.7297" y2="14.9733" layer="21"/>
<rectangle x1="21.2979" y1="14.9733" x2="21.7297" y2="14.9987" layer="21"/>
<rectangle x1="21.2725" y1="14.9987" x2="21.7043" y2="15.0241" layer="21"/>
<rectangle x1="21.2725" y1="15.0241" x2="21.6789" y2="15.0495" layer="21"/>
<rectangle x1="21.2471" y1="15.0495" x2="21.6535" y2="15.0749" layer="21"/>
<rectangle x1="21.2217" y1="15.0749" x2="21.6281" y2="15.1003" layer="21"/>
<rectangle x1="21.1963" y1="15.1003" x2="21.6027" y2="15.1257" layer="21"/>
<rectangle x1="21.1709" y1="15.1257" x2="21.5773" y2="15.1511" layer="21"/>
<rectangle x1="21.1455" y1="15.1511" x2="21.5519" y2="15.1765" layer="21"/>
<rectangle x1="21.1201" y1="15.1765" x2="21.5265" y2="15.2019" layer="21"/>
<rectangle x1="21.0947" y1="15.2019" x2="21.5011" y2="15.2273" layer="21"/>
<rectangle x1="21.0693" y1="15.2273" x2="21.4757" y2="15.2527" layer="21"/>
<rectangle x1="21.0439" y1="15.2527" x2="21.4503" y2="15.2781" layer="21"/>
<rectangle x1="21.0185" y1="15.2781" x2="21.4249" y2="15.3035" layer="21"/>
<rectangle x1="20.9931" y1="15.3035" x2="21.3741" y2="15.3289" layer="21"/>
<rectangle x1="20.9677" y1="15.3289" x2="21.3487" y2="15.3543" layer="21"/>
<rectangle x1="20.9169" y1="15.3543" x2="21.2979" y2="15.3797" layer="21"/>
<rectangle x1="20.8915" y1="15.3797" x2="21.2725" y2="15.4051" layer="21"/>
<rectangle x1="20.8661" y1="15.4051" x2="21.2217" y2="15.4305" layer="21"/>
<rectangle x1="20.8153" y1="15.4305" x2="21.1709" y2="15.4559" layer="21"/>
<rectangle x1="20.7899" y1="15.4559" x2="21.1201" y2="15.4813" layer="21"/>
<rectangle x1="20.7391" y1="15.4813" x2="21.0693" y2="15.5067" layer="21"/>
<rectangle x1="20.6883" y1="15.5067" x2="21.0185" y2="15.5321" layer="21"/>
<rectangle x1="20.6629" y1="15.5321" x2="20.9423" y2="15.5575" layer="21"/>
<rectangle x1="20.6121" y1="15.5575" x2="20.8915" y2="15.5829" layer="21"/>
<rectangle x1="20.5613" y1="15.5829" x2="20.8153" y2="15.6083" layer="21"/>
<rectangle x1="20.5105" y1="15.6083" x2="20.7137" y2="15.6337" layer="21"/>
<rectangle x1="20.4343" y1="15.6337" x2="20.6375" y2="15.6591" layer="21"/>
<rectangle x1="20.3835" y1="15.6591" x2="20.5105" y2="15.6845" layer="21"/>
<rectangle x1="17.5387" y1="15.8623" x2="17.9197" y2="15.8877" layer="21"/>
<rectangle x1="17.4371" y1="15.8877" x2="17.9959" y2="15.9131" layer="21"/>
<rectangle x1="17.3355" y1="15.9131" x2="18.0467" y2="15.9385" layer="21"/>
<rectangle x1="17.2593" y1="15.9385" x2="18.0975" y2="15.9639" layer="21"/>
<rectangle x1="17.2085" y1="15.9639" x2="18.1229" y2="15.9893" layer="21"/>
<rectangle x1="17.1577" y1="15.9893" x2="18.1483" y2="16.0147" layer="21"/>
<rectangle x1="17.0815" y1="16.0147" x2="18.1991" y2="16.0401" layer="21"/>
<rectangle x1="17.0561" y1="16.0401" x2="18.2245" y2="16.0655" layer="21"/>
<rectangle x1="17.0053" y1="16.0655" x2="18.2245" y2="16.0909" layer="21"/>
<rectangle x1="16.9545" y1="16.0909" x2="18.2499" y2="16.1163" layer="21"/>
<rectangle x1="16.9291" y1="16.1163" x2="18.2753" y2="16.1417" layer="21"/>
<rectangle x1="16.9037" y1="16.1417" x2="18.3007" y2="16.1671" layer="21"/>
<rectangle x1="16.8529" y1="16.1671" x2="18.3007" y2="16.1925" layer="21"/>
<rectangle x1="16.8275" y1="16.1925" x2="18.3261" y2="16.2179" layer="21"/>
<rectangle x1="16.8021" y1="16.2179" x2="18.3261" y2="16.2433" layer="21"/>
<rectangle x1="16.7767" y1="16.2433" x2="18.3515" y2="16.2687" layer="21"/>
<rectangle x1="16.7259" y1="16.2687" x2="18.3515" y2="16.2941" layer="21"/>
<rectangle x1="16.7005" y1="16.2941" x2="18.3515" y2="16.3195" layer="21"/>
<rectangle x1="16.6497" y1="16.3195" x2="18.3007" y2="16.3449" layer="21"/>
<rectangle x1="16.6243" y1="16.3449" x2="18.2499" y2="16.3703" layer="21"/>
<rectangle x1="16.5735" y1="16.3703" x2="18.1991" y2="16.3957" layer="21"/>
<rectangle x1="16.5481" y1="16.3957" x2="18.1229" y2="16.4211" layer="21"/>
<rectangle x1="16.4973" y1="16.4211" x2="18.0721" y2="16.4465" layer="21"/>
<rectangle x1="16.4465" y1="16.4465" x2="18.0213" y2="16.4719" layer="21"/>
<rectangle x1="16.3957" y1="16.4719" x2="17.9451" y2="16.4973" layer="21"/>
<rectangle x1="16.3703" y1="16.4973" x2="17.8943" y2="16.5227" layer="21"/>
<rectangle x1="16.3195" y1="16.5227" x2="17.8181" y2="16.5481" layer="21"/>
<rectangle x1="16.2687" y1="16.5481" x2="17.7419" y2="16.5735" layer="21"/>
<rectangle x1="16.2179" y1="16.5735" x2="17.6657" y2="16.5989" layer="21"/>
<rectangle x1="16.1671" y1="16.5989" x2="17.5895" y2="16.6243" layer="21"/>
<rectangle x1="16.1163" y1="16.6243" x2="17.4879" y2="16.6497" layer="21"/>
<rectangle x1="16.0401" y1="16.6497" x2="17.4117" y2="16.6751" layer="21"/>
<rectangle x1="15.9893" y1="16.6751" x2="17.3101" y2="16.7005" layer="21"/>
<rectangle x1="15.9385" y1="16.7005" x2="17.1831" y2="16.7259" layer="21"/>
<rectangle x1="15.8877" y1="16.7259" x2="17.0561" y2="16.7513" layer="21"/>
<rectangle x1="15.8369" y1="16.7513" x2="16.9037" y2="16.7767" layer="21"/>
<rectangle x1="15.7607" y1="16.7767" x2="16.7259" y2="16.8021" layer="21"/>
<rectangle x1="15.7099" y1="16.8021" x2="16.4719" y2="16.8275" layer="21"/>
<rectangle x1="19.4945" y1="16.8783" x2="19.5453" y2="16.9037" layer="21"/>
<rectangle x1="19.4437" y1="16.9037" x2="19.5707" y2="16.9291" layer="21"/>
<rectangle x1="19.3929" y1="16.9291" x2="19.5707" y2="16.9545" layer="21"/>
<rectangle x1="19.3421" y1="16.9545" x2="19.5961" y2="16.9799" layer="21"/>
<rectangle x1="19.2913" y1="16.9799" x2="19.6215" y2="17.0053" layer="21"/>
<rectangle x1="19.2405" y1="17.0053" x2="19.6215" y2="17.0307" layer="21"/>
<rectangle x1="19.1897" y1="17.0307" x2="19.6469" y2="17.0561" layer="21"/>
<rectangle x1="19.1389" y1="17.0561" x2="19.6723" y2="17.0815" layer="21"/>
<rectangle x1="19.0881" y1="17.0815" x2="19.6723" y2="17.1069" layer="21"/>
<rectangle x1="19.0119" y1="17.1069" x2="19.6977" y2="17.1323" layer="21"/>
<rectangle x1="18.9611" y1="17.1323" x2="19.7231" y2="17.1577" layer="21"/>
<rectangle x1="18.8849" y1="17.1577" x2="19.7231" y2="17.1831" layer="21"/>
<rectangle x1="18.8341" y1="17.1831" x2="19.7485" y2="17.2085" layer="21"/>
<rectangle x1="18.7579" y1="17.2085" x2="19.7739" y2="17.2339" layer="21"/>
<rectangle x1="18.7071" y1="17.2339" x2="19.7739" y2="17.2593" layer="21"/>
<rectangle x1="6.6421" y1="17.2593" x2="6.6929" y2="17.2847" layer="21"/>
<rectangle x1="18.6309" y1="17.2593" x2="19.7993" y2="17.2847" layer="21"/>
<rectangle x1="6.6167" y1="17.2847" x2="6.8453" y2="17.3101" layer="21"/>
<rectangle x1="18.5801" y1="17.2847" x2="19.8247" y2="17.3101" layer="21"/>
<rectangle x1="6.5913" y1="17.3101" x2="6.9723" y2="17.3355" layer="21"/>
<rectangle x1="18.5039" y1="17.3101" x2="19.8501" y2="17.3355" layer="21"/>
<rectangle x1="6.5913" y1="17.3355" x2="7.1247" y2="17.3609" layer="21"/>
<rectangle x1="18.4277" y1="17.3355" x2="19.8501" y2="17.3609" layer="21"/>
<rectangle x1="6.5659" y1="17.3609" x2="7.2517" y2="17.3863" layer="21"/>
<rectangle x1="18.3515" y1="17.3609" x2="19.8755" y2="17.3863" layer="21"/>
<rectangle x1="6.5405" y1="17.3863" x2="7.4041" y2="17.4117" layer="21"/>
<rectangle x1="18.2753" y1="17.3863" x2="19.9009" y2="17.4117" layer="21"/>
<rectangle x1="6.5405" y1="17.4117" x2="7.5565" y2="17.4371" layer="21"/>
<rectangle x1="18.1991" y1="17.4117" x2="19.9009" y2="17.4371" layer="21"/>
<rectangle x1="6.5151" y1="17.4371" x2="7.6835" y2="17.4625" layer="21"/>
<rectangle x1="18.1229" y1="17.4371" x2="19.9263" y2="17.4625" layer="21"/>
<rectangle x1="6.4897" y1="17.4625" x2="7.8359" y2="17.4879" layer="21"/>
<rectangle x1="18.0467" y1="17.4625" x2="19.9517" y2="17.4879" layer="21"/>
<rectangle x1="6.4897" y1="17.4879" x2="7.9629" y2="17.5133" layer="21"/>
<rectangle x1="17.9705" y1="17.4879" x2="19.9517" y2="17.5133" layer="21"/>
<rectangle x1="6.4643" y1="17.5133" x2="8.1153" y2="17.5387" layer="21"/>
<rectangle x1="17.8689" y1="17.5133" x2="19.9771" y2="17.5387" layer="21"/>
<rectangle x1="6.4389" y1="17.5387" x2="8.2423" y2="17.5641" layer="21"/>
<rectangle x1="17.7927" y1="17.5387" x2="20.0025" y2="17.5641" layer="21"/>
<rectangle x1="6.4389" y1="17.5641" x2="8.3947" y2="17.5895" layer="21"/>
<rectangle x1="17.6911" y1="17.5641" x2="20.0025" y2="17.5895" layer="21"/>
<rectangle x1="6.4135" y1="17.5895" x2="8.5471" y2="17.6149" layer="21"/>
<rectangle x1="17.6149" y1="17.5895" x2="20.0279" y2="17.6149" layer="21"/>
<rectangle x1="6.3881" y1="17.6149" x2="8.6741" y2="17.6403" layer="21"/>
<rectangle x1="17.5133" y1="17.6149" x2="20.0533" y2="17.6403" layer="21"/>
<rectangle x1="6.3881" y1="17.6403" x2="8.7249" y2="17.6657" layer="21"/>
<rectangle x1="17.4117" y1="17.6403" x2="20.0533" y2="17.6657" layer="21"/>
<rectangle x1="6.3627" y1="17.6657" x2="8.6487" y2="17.6911" layer="21"/>
<rectangle x1="17.3101" y1="17.6657" x2="20.0787" y2="17.6911" layer="21"/>
<rectangle x1="6.3373" y1="17.6911" x2="8.5979" y2="17.7165" layer="21"/>
<rectangle x1="17.2085" y1="17.6911" x2="20.1041" y2="17.7165" layer="21"/>
<rectangle x1="6.3373" y1="17.7165" x2="8.5471" y2="17.7419" layer="21"/>
<rectangle x1="17.1069" y1="17.7165" x2="20.1041" y2="17.7419" layer="21"/>
<rectangle x1="6.3119" y1="17.7419" x2="8.4963" y2="17.7673" layer="21"/>
<rectangle x1="17.0053" y1="17.7419" x2="20.1295" y2="17.7673" layer="21"/>
<rectangle x1="6.2865" y1="17.7673" x2="8.4455" y2="17.7927" layer="21"/>
<rectangle x1="16.8783" y1="17.7673" x2="20.1549" y2="17.7927" layer="21"/>
<rectangle x1="6.2865" y1="17.7927" x2="8.3947" y2="17.8181" layer="21"/>
<rectangle x1="16.7513" y1="17.7927" x2="20.1549" y2="17.8181" layer="21"/>
<rectangle x1="6.2611" y1="17.8181" x2="8.3439" y2="17.8435" layer="21"/>
<rectangle x1="16.6243" y1="17.8181" x2="20.1803" y2="17.8435" layer="21"/>
<rectangle x1="6.2357" y1="17.8435" x2="8.2931" y2="17.8689" layer="21"/>
<rectangle x1="16.4973" y1="17.8435" x2="20.2057" y2="17.8689" layer="21"/>
<rectangle x1="6.2357" y1="17.8689" x2="8.2423" y2="17.8943" layer="21"/>
<rectangle x1="16.3703" y1="17.8689" x2="20.2057" y2="17.8943" layer="21"/>
<rectangle x1="6.2103" y1="17.8943" x2="8.1915" y2="17.9197" layer="21"/>
<rectangle x1="16.2179" y1="17.8943" x2="20.2311" y2="17.9197" layer="21"/>
<rectangle x1="6.1849" y1="17.9197" x2="8.1661" y2="17.9451" layer="21"/>
<rectangle x1="16.0655" y1="17.9197" x2="20.2565" y2="17.9451" layer="21"/>
<rectangle x1="6.1849" y1="17.9451" x2="8.1153" y2="17.9705" layer="21"/>
<rectangle x1="15.8877" y1="17.9451" x2="20.2565" y2="17.9705" layer="21"/>
<rectangle x1="6.1595" y1="17.9705" x2="8.0899" y2="17.9959" layer="21"/>
<rectangle x1="15.7099" y1="17.9705" x2="20.2819" y2="17.9959" layer="21"/>
<rectangle x1="6.1341" y1="17.9959" x2="8.0391" y2="18.0213" layer="21"/>
<rectangle x1="15.5321" y1="17.9959" x2="20.3073" y2="18.0213" layer="21"/>
<rectangle x1="6.1341" y1="18.0213" x2="8.0137" y2="18.0467" layer="21"/>
<rectangle x1="15.3035" y1="18.0213" x2="20.3327" y2="18.0467" layer="21"/>
<rectangle x1="6.1087" y1="18.0467" x2="7.9629" y2="18.0721" layer="21"/>
<rectangle x1="15.0749" y1="18.0467" x2="20.3327" y2="18.0721" layer="21"/>
<rectangle x1="6.0833" y1="18.0721" x2="7.9375" y2="18.0975" layer="21"/>
<rectangle x1="9.0551" y1="18.0721" x2="11.0871" y2="18.0975" layer="21"/>
<rectangle x1="14.7701" y1="18.0721" x2="20.3581" y2="18.0975" layer="21"/>
<rectangle x1="6.0833" y1="18.0975" x2="7.8867" y2="18.1229" layer="21"/>
<rectangle x1="8.5217" y1="18.0975" x2="12.0523" y2="18.1229" layer="21"/>
<rectangle x1="14.4145" y1="18.0975" x2="20.3835" y2="18.1229" layer="21"/>
<rectangle x1="6.0579" y1="18.1229" x2="7.8613" y2="18.1483" layer="21"/>
<rectangle x1="8.2169" y1="18.1229" x2="12.7381" y2="18.1483" layer="21"/>
<rectangle x1="13.7795" y1="18.1229" x2="20.3835" y2="18.1483" layer="21"/>
<rectangle x1="6.0325" y1="18.1483" x2="7.8359" y2="18.1737" layer="21"/>
<rectangle x1="7.9883" y1="18.1483" x2="20.4089" y2="18.1737" layer="21"/>
<rectangle x1="6.0325" y1="18.1737" x2="7.8105" y2="18.1991" layer="21"/>
<rectangle x1="7.8613" y1="18.1737" x2="20.4343" y2="18.1991" layer="21"/>
<rectangle x1="6.0071" y1="18.1991" x2="20.4343" y2="18.2245" layer="21"/>
<rectangle x1="5.9817" y1="18.2245" x2="20.4597" y2="18.2499" layer="21"/>
<rectangle x1="5.9817" y1="18.2499" x2="20.4851" y2="18.2753" layer="21"/>
<rectangle x1="5.9563" y1="18.2753" x2="20.4851" y2="18.3007" layer="21"/>
<rectangle x1="5.9309" y1="18.3007" x2="20.5105" y2="18.3261" layer="21"/>
<rectangle x1="5.9309" y1="18.3261" x2="20.5359" y2="18.3515" layer="21"/>
<rectangle x1="5.9055" y1="18.3515" x2="20.5359" y2="18.3769" layer="21"/>
<rectangle x1="5.8801" y1="18.3769" x2="20.5613" y2="18.4023" layer="21"/>
<rectangle x1="5.8801" y1="18.4023" x2="20.5867" y2="18.4277" layer="21"/>
<rectangle x1="5.8547" y1="18.4277" x2="20.5867" y2="18.4531" layer="21"/>
<rectangle x1="5.8293" y1="18.4531" x2="20.6121" y2="18.4785" layer="21"/>
<rectangle x1="5.8293" y1="18.4785" x2="20.6375" y2="18.5039" layer="21"/>
<rectangle x1="5.8039" y1="18.5039" x2="20.6375" y2="18.5293" layer="21"/>
<rectangle x1="5.7785" y1="18.5293" x2="20.6629" y2="18.5547" layer="21"/>
<rectangle x1="5.7785" y1="18.5547" x2="20.6883" y2="18.5801" layer="21"/>
<rectangle x1="5.7531" y1="18.5801" x2="20.6883" y2="18.6055" layer="21"/>
<rectangle x1="5.7277" y1="18.6055" x2="20.7137" y2="18.6309" layer="21"/>
<rectangle x1="5.7277" y1="18.6309" x2="20.7391" y2="18.6563" layer="21"/>
<rectangle x1="5.7023" y1="18.6563" x2="20.7391" y2="18.6817" layer="21"/>
<rectangle x1="5.6769" y1="18.6817" x2="20.7645" y2="18.7071" layer="21"/>
<rectangle x1="5.6769" y1="18.7071" x2="20.7899" y2="18.7325" layer="21"/>
<rectangle x1="5.6515" y1="18.7325" x2="20.8153" y2="18.7579" layer="21"/>
<rectangle x1="5.6261" y1="18.7579" x2="20.8153" y2="18.7833" layer="21"/>
<rectangle x1="5.6261" y1="18.7833" x2="20.8407" y2="18.8087" layer="21"/>
<rectangle x1="5.6007" y1="18.8087" x2="20.8661" y2="18.8341" layer="21"/>
<rectangle x1="5.5753" y1="18.8341" x2="20.8661" y2="18.8595" layer="21"/>
<rectangle x1="5.5753" y1="18.8595" x2="20.8915" y2="18.8849" layer="21"/>
<rectangle x1="5.5499" y1="18.8849" x2="13.2715" y2="18.9103" layer="21"/>
<rectangle x1="13.3477" y1="18.8849" x2="20.9169" y2="18.9103" layer="21"/>
<rectangle x1="5.5245" y1="18.9103" x2="13.2207" y2="18.9357" layer="21"/>
<rectangle x1="13.4239" y1="18.9103" x2="20.9169" y2="18.9357" layer="21"/>
<rectangle x1="5.5245" y1="18.9357" x2="13.1699" y2="18.9611" layer="21"/>
<rectangle x1="13.4493" y1="18.9357" x2="20.9423" y2="18.9611" layer="21"/>
<rectangle x1="5.4991" y1="18.9611" x2="13.1445" y2="18.9865" layer="21"/>
<rectangle x1="13.4747" y1="18.9611" x2="20.9677" y2="18.9865" layer="21"/>
<rectangle x1="5.4737" y1="18.9865" x2="13.1445" y2="19.0119" layer="21"/>
<rectangle x1="13.5001" y1="18.9865" x2="20.9677" y2="19.0119" layer="21"/>
<rectangle x1="5.4737" y1="19.0119" x2="13.1191" y2="19.0373" layer="21"/>
<rectangle x1="13.5001" y1="19.0119" x2="20.9931" y2="19.0373" layer="21"/>
<rectangle x1="5.4483" y1="19.0373" x2="13.1191" y2="19.0627" layer="21"/>
<rectangle x1="13.5255" y1="19.0373" x2="21.0185" y2="19.0627" layer="21"/>
<rectangle x1="5.4229" y1="19.0627" x2="13.0937" y2="19.0881" layer="21"/>
<rectangle x1="13.5509" y1="19.0627" x2="21.0185" y2="19.0881" layer="21"/>
<rectangle x1="5.4229" y1="19.0881" x2="13.0683" y2="19.1135" layer="21"/>
<rectangle x1="13.5509" y1="19.0881" x2="21.0439" y2="19.1135" layer="21"/>
<rectangle x1="5.3975" y1="19.1135" x2="13.0683" y2="19.1389" layer="21"/>
<rectangle x1="13.5763" y1="19.1135" x2="21.0693" y2="19.1389" layer="21"/>
<rectangle x1="5.3721" y1="19.1389" x2="13.0429" y2="19.1643" layer="21"/>
<rectangle x1="13.5763" y1="19.1389" x2="21.0693" y2="19.1643" layer="21"/>
<rectangle x1="5.3721" y1="19.1643" x2="13.0429" y2="19.1897" layer="21"/>
<rectangle x1="13.6017" y1="19.1643" x2="21.0947" y2="19.1897" layer="21"/>
<rectangle x1="5.3467" y1="19.1897" x2="13.0175" y2="19.2151" layer="21"/>
<rectangle x1="13.6271" y1="19.1897" x2="21.1201" y2="19.2151" layer="21"/>
<rectangle x1="5.3213" y1="19.2151" x2="12.9921" y2="19.2405" layer="21"/>
<rectangle x1="13.6271" y1="19.2151" x2="21.1201" y2="19.2405" layer="21"/>
<rectangle x1="5.3213" y1="19.2405" x2="12.9921" y2="19.2659" layer="21"/>
<rectangle x1="13.6525" y1="19.2405" x2="21.1455" y2="19.2659" layer="21"/>
<rectangle x1="5.2959" y1="19.2659" x2="12.9667" y2="19.2913" layer="21"/>
<rectangle x1="13.6525" y1="19.2659" x2="21.1709" y2="19.2913" layer="21"/>
<rectangle x1="5.2705" y1="19.2913" x2="12.9667" y2="19.3167" layer="21"/>
<rectangle x1="13.6779" y1="19.2913" x2="21.1709" y2="19.3167" layer="21"/>
<rectangle x1="5.2705" y1="19.3167" x2="12.9413" y2="19.3421" layer="21"/>
<rectangle x1="13.7033" y1="19.3167" x2="21.1963" y2="19.3421" layer="21"/>
<rectangle x1="5.2451" y1="19.3421" x2="12.9413" y2="19.3675" layer="21"/>
<rectangle x1="13.7033" y1="19.3421" x2="21.2217" y2="19.3675" layer="21"/>
<rectangle x1="5.2197" y1="19.3675" x2="12.9159" y2="19.3929" layer="21"/>
<rectangle x1="13.7287" y1="19.3675" x2="21.2217" y2="19.3929" layer="21"/>
<rectangle x1="5.2197" y1="19.3929" x2="12.8905" y2="19.4183" layer="21"/>
<rectangle x1="13.7541" y1="19.3929" x2="21.2471" y2="19.4183" layer="21"/>
<rectangle x1="5.1943" y1="19.4183" x2="12.8905" y2="19.4437" layer="21"/>
<rectangle x1="13.7541" y1="19.4183" x2="21.2725" y2="19.4437" layer="21"/>
<rectangle x1="5.1689" y1="19.4437" x2="12.8651" y2="19.4691" layer="21"/>
<rectangle x1="13.7795" y1="19.4437" x2="21.2725" y2="19.4691" layer="21"/>
<rectangle x1="5.1689" y1="19.4691" x2="12.8651" y2="19.4945" layer="21"/>
<rectangle x1="13.7795" y1="19.4691" x2="21.2979" y2="19.4945" layer="21"/>
<rectangle x1="5.1435" y1="19.4945" x2="12.8397" y2="19.5199" layer="21"/>
<rectangle x1="13.8049" y1="19.4945" x2="21.3233" y2="19.5199" layer="21"/>
<rectangle x1="5.1181" y1="19.5199" x2="12.8143" y2="19.5453" layer="21"/>
<rectangle x1="13.8303" y1="19.5199" x2="21.3487" y2="19.5453" layer="21"/>
<rectangle x1="5.1181" y1="19.5453" x2="12.8143" y2="19.5707" layer="21"/>
<rectangle x1="13.8303" y1="19.5453" x2="21.3487" y2="19.5707" layer="21"/>
<rectangle x1="5.0927" y1="19.5707" x2="12.7889" y2="19.5961" layer="21"/>
<rectangle x1="13.8557" y1="19.5707" x2="21.3741" y2="19.5961" layer="21"/>
<rectangle x1="5.0673" y1="19.5961" x2="12.7889" y2="19.6215" layer="21"/>
<rectangle x1="13.8557" y1="19.5961" x2="21.3995" y2="19.6215" layer="21"/>
<rectangle x1="5.0673" y1="19.6215" x2="12.7635" y2="19.6469" layer="21"/>
<rectangle x1="13.8811" y1="19.6215" x2="21.3995" y2="19.6469" layer="21"/>
<rectangle x1="5.0419" y1="19.6469" x2="12.7381" y2="19.6723" layer="21"/>
<rectangle x1="13.9065" y1="19.6469" x2="21.4249" y2="19.6723" layer="21"/>
<rectangle x1="5.0165" y1="19.6723" x2="12.7381" y2="19.6977" layer="21"/>
<rectangle x1="13.9065" y1="19.6723" x2="21.4503" y2="19.6977" layer="21"/>
<rectangle x1="5.0165" y1="19.6977" x2="12.7127" y2="19.7231" layer="21"/>
<rectangle x1="13.9319" y1="19.6977" x2="21.4503" y2="19.7231" layer="21"/>
<rectangle x1="4.9911" y1="19.7231" x2="12.7127" y2="19.7485" layer="21"/>
<rectangle x1="13.9319" y1="19.7231" x2="21.4757" y2="19.7485" layer="21"/>
<rectangle x1="4.9657" y1="19.7485" x2="12.6873" y2="19.7739" layer="21"/>
<rectangle x1="13.9573" y1="19.7485" x2="21.5011" y2="19.7739" layer="21"/>
<rectangle x1="4.9657" y1="19.7739" x2="12.6873" y2="19.7993" layer="21"/>
<rectangle x1="13.9827" y1="19.7739" x2="21.5011" y2="19.7993" layer="21"/>
<rectangle x1="4.9403" y1="19.7993" x2="12.6619" y2="19.8247" layer="21"/>
<rectangle x1="13.9827" y1="19.7993" x2="21.5265" y2="19.8247" layer="21"/>
<rectangle x1="4.9149" y1="19.8247" x2="12.6365" y2="19.8501" layer="21"/>
<rectangle x1="14.0081" y1="19.8247" x2="21.5519" y2="19.8501" layer="21"/>
<rectangle x1="4.9149" y1="19.8501" x2="12.6365" y2="19.8755" layer="21"/>
<rectangle x1="14.0081" y1="19.8501" x2="21.5519" y2="19.8755" layer="21"/>
<rectangle x1="4.8895" y1="19.8755" x2="12.6111" y2="19.9009" layer="21"/>
<rectangle x1="14.0335" y1="19.8755" x2="21.5773" y2="19.9009" layer="21"/>
<rectangle x1="4.8641" y1="19.9009" x2="12.6111" y2="19.9263" layer="21"/>
<rectangle x1="14.0589" y1="19.9009" x2="21.6027" y2="19.9263" layer="21"/>
<rectangle x1="4.8641" y1="19.9263" x2="12.5857" y2="19.9517" layer="21"/>
<rectangle x1="14.0589" y1="19.9263" x2="21.6027" y2="19.9517" layer="21"/>
<rectangle x1="4.8387" y1="19.9517" x2="12.5603" y2="19.9771" layer="21"/>
<rectangle x1="14.0843" y1="19.9517" x2="21.6281" y2="19.9771" layer="21"/>
<rectangle x1="4.8133" y1="19.9771" x2="12.5603" y2="20.0025" layer="21"/>
<rectangle x1="14.0843" y1="19.9771" x2="21.6535" y2="20.0025" layer="21"/>
<rectangle x1="4.8133" y1="20.0025" x2="12.5349" y2="20.0279" layer="21"/>
<rectangle x1="14.1097" y1="20.0025" x2="21.6535" y2="20.0279" layer="21"/>
<rectangle x1="4.7879" y1="20.0279" x2="12.5349" y2="20.0533" layer="21"/>
<rectangle x1="14.1351" y1="20.0279" x2="21.6789" y2="20.0533" layer="21"/>
<rectangle x1="4.7625" y1="20.0533" x2="12.5095" y2="20.0787" layer="21"/>
<rectangle x1="14.1351" y1="20.0533" x2="21.7043" y2="20.0787" layer="21"/>
<rectangle x1="4.7625" y1="20.0787" x2="12.4841" y2="20.1041" layer="21"/>
<rectangle x1="14.1605" y1="20.0787" x2="21.7043" y2="20.1041" layer="21"/>
<rectangle x1="4.7371" y1="20.1041" x2="12.4841" y2="20.1295" layer="21"/>
<rectangle x1="14.1605" y1="20.1041" x2="21.7297" y2="20.1295" layer="21"/>
<rectangle x1="4.7117" y1="20.1295" x2="12.4587" y2="20.1549" layer="21"/>
<rectangle x1="14.1859" y1="20.1295" x2="21.7551" y2="20.1549" layer="21"/>
<rectangle x1="4.7117" y1="20.1549" x2="12.4587" y2="20.1803" layer="21"/>
<rectangle x1="14.2113" y1="20.1549" x2="21.7551" y2="20.1803" layer="21"/>
<rectangle x1="4.6863" y1="20.1803" x2="12.4333" y2="20.2057" layer="21"/>
<rectangle x1="14.2113" y1="20.1803" x2="21.7805" y2="20.2057" layer="21"/>
<rectangle x1="4.6609" y1="20.2057" x2="12.4079" y2="20.2311" layer="21"/>
<rectangle x1="14.2367" y1="20.2057" x2="21.8059" y2="20.2311" layer="21"/>
<rectangle x1="4.6609" y1="20.2311" x2="12.4079" y2="20.2565" layer="21"/>
<rectangle x1="14.2367" y1="20.2311" x2="21.8313" y2="20.2565" layer="21"/>
<rectangle x1="4.6355" y1="20.2565" x2="12.3825" y2="20.2819" layer="21"/>
<rectangle x1="14.2621" y1="20.2565" x2="21.8313" y2="20.2819" layer="21"/>
<rectangle x1="4.6101" y1="20.2819" x2="12.3825" y2="20.3073" layer="21"/>
<rectangle x1="14.2875" y1="20.2819" x2="21.8567" y2="20.3073" layer="21"/>
<rectangle x1="4.6101" y1="20.3073" x2="12.3571" y2="20.3327" layer="21"/>
<rectangle x1="14.2875" y1="20.3073" x2="21.8821" y2="20.3327" layer="21"/>
<rectangle x1="4.5847" y1="20.3327" x2="12.3571" y2="20.3581" layer="21"/>
<rectangle x1="14.3129" y1="20.3327" x2="21.8821" y2="20.3581" layer="21"/>
<rectangle x1="4.5593" y1="20.3581" x2="12.3317" y2="20.3835" layer="21"/>
<rectangle x1="14.3129" y1="20.3581" x2="21.9075" y2="20.3835" layer="21"/>
<rectangle x1="4.5593" y1="20.3835" x2="12.3063" y2="20.4089" layer="21"/>
<rectangle x1="14.3383" y1="20.3835" x2="21.9329" y2="20.4089" layer="21"/>
<rectangle x1="4.5339" y1="20.4089" x2="12.3063" y2="20.4343" layer="21"/>
<rectangle x1="14.3637" y1="20.4089" x2="21.9329" y2="20.4343" layer="21"/>
<rectangle x1="4.5085" y1="20.4343" x2="12.2809" y2="20.4597" layer="21"/>
<rectangle x1="14.3637" y1="20.4343" x2="21.9583" y2="20.4597" layer="21"/>
<rectangle x1="4.5085" y1="20.4597" x2="12.2809" y2="20.4851" layer="21"/>
<rectangle x1="14.3891" y1="20.4597" x2="21.9837" y2="20.4851" layer="21"/>
<rectangle x1="4.4831" y1="20.4851" x2="12.2555" y2="20.5105" layer="21"/>
<rectangle x1="14.3891" y1="20.4851" x2="21.9837" y2="20.5105" layer="21"/>
<rectangle x1="4.4577" y1="20.5105" x2="12.2301" y2="20.5359" layer="21"/>
<rectangle x1="14.4145" y1="20.5105" x2="22.0091" y2="20.5359" layer="21"/>
<rectangle x1="4.4577" y1="20.5359" x2="12.2301" y2="20.5613" layer="21"/>
<rectangle x1="14.4399" y1="20.5359" x2="22.0345" y2="20.5613" layer="21"/>
<rectangle x1="4.4323" y1="20.5613" x2="12.2047" y2="20.5867" layer="21"/>
<rectangle x1="14.4399" y1="20.5613" x2="22.0345" y2="20.5867" layer="21"/>
<rectangle x1="4.4069" y1="20.5867" x2="12.2047" y2="20.6121" layer="21"/>
<rectangle x1="14.4653" y1="20.5867" x2="22.0599" y2="20.6121" layer="21"/>
<rectangle x1="4.4069" y1="20.6121" x2="12.1793" y2="20.6375" layer="21"/>
<rectangle x1="14.4653" y1="20.6121" x2="22.0853" y2="20.6375" layer="21"/>
<rectangle x1="4.3815" y1="20.6375" x2="12.1539" y2="20.6629" layer="21"/>
<rectangle x1="14.4907" y1="20.6375" x2="22.0853" y2="20.6629" layer="21"/>
<rectangle x1="4.3561" y1="20.6629" x2="12.1539" y2="20.6883" layer="21"/>
<rectangle x1="14.5161" y1="20.6629" x2="22.1107" y2="20.6883" layer="21"/>
<rectangle x1="4.3561" y1="20.6883" x2="12.1285" y2="20.7137" layer="21"/>
<rectangle x1="14.5161" y1="20.6883" x2="22.1361" y2="20.7137" layer="21"/>
<rectangle x1="4.3307" y1="20.7137" x2="12.1285" y2="20.7391" layer="21"/>
<rectangle x1="14.5415" y1="20.7137" x2="22.1361" y2="20.7391" layer="21"/>
<rectangle x1="4.3053" y1="20.7391" x2="12.1031" y2="20.7645" layer="21"/>
<rectangle x1="14.5415" y1="20.7391" x2="22.1615" y2="20.7645" layer="21"/>
<rectangle x1="4.3053" y1="20.7645" x2="12.1031" y2="20.7899" layer="21"/>
<rectangle x1="14.5669" y1="20.7645" x2="22.1869" y2="20.7899" layer="21"/>
<rectangle x1="4.2799" y1="20.7899" x2="12.0777" y2="20.8153" layer="21"/>
<rectangle x1="14.5923" y1="20.7899" x2="22.1869" y2="20.8153" layer="21"/>
<rectangle x1="4.2545" y1="20.8153" x2="12.0523" y2="20.8407" layer="21"/>
<rectangle x1="14.5923" y1="20.8153" x2="22.2123" y2="20.8407" layer="21"/>
<rectangle x1="4.2545" y1="20.8407" x2="12.0523" y2="20.8661" layer="21"/>
<rectangle x1="14.6177" y1="20.8407" x2="22.2377" y2="20.8661" layer="21"/>
<rectangle x1="4.2291" y1="20.8661" x2="12.0269" y2="20.8915" layer="21"/>
<rectangle x1="14.6177" y1="20.8661" x2="22.2377" y2="20.8915" layer="21"/>
<rectangle x1="4.2037" y1="20.8915" x2="12.0269" y2="20.9169" layer="21"/>
<rectangle x1="14.6431" y1="20.8915" x2="22.2631" y2="20.9169" layer="21"/>
<rectangle x1="4.2037" y1="20.9169" x2="12.0015" y2="20.9423" layer="21"/>
<rectangle x1="14.6685" y1="20.9169" x2="22.2885" y2="20.9423" layer="21"/>
<rectangle x1="4.1783" y1="20.9423" x2="11.9761" y2="20.9677" layer="21"/>
<rectangle x1="14.6685" y1="20.9423" x2="22.2885" y2="20.9677" layer="21"/>
<rectangle x1="4.1529" y1="20.9677" x2="11.9761" y2="20.9931" layer="21"/>
<rectangle x1="14.6939" y1="20.9677" x2="22.3139" y2="20.9931" layer="21"/>
<rectangle x1="4.1529" y1="20.9931" x2="11.9507" y2="21.0185" layer="21"/>
<rectangle x1="14.7193" y1="20.9931" x2="22.3393" y2="21.0185" layer="21"/>
<rectangle x1="4.1275" y1="21.0185" x2="11.9507" y2="21.0439" layer="21"/>
<rectangle x1="14.7193" y1="21.0185" x2="22.3647" y2="21.0439" layer="21"/>
<rectangle x1="4.1021" y1="21.0439" x2="11.9253" y2="21.0693" layer="21"/>
<rectangle x1="14.7447" y1="21.0439" x2="22.3647" y2="21.0693" layer="21"/>
<rectangle x1="4.1021" y1="21.0693" x2="11.8999" y2="21.0947" layer="21"/>
<rectangle x1="14.7447" y1="21.0693" x2="22.3901" y2="21.0947" layer="21"/>
<rectangle x1="4.0767" y1="21.0947" x2="11.8999" y2="21.1201" layer="21"/>
<rectangle x1="14.7701" y1="21.0947" x2="22.4155" y2="21.1201" layer="21"/>
<rectangle x1="4.0513" y1="21.1201" x2="11.8745" y2="21.1455" layer="21"/>
<rectangle x1="14.7955" y1="21.1201" x2="22.4155" y2="21.1455" layer="21"/>
<rectangle x1="4.0513" y1="21.1455" x2="11.8745" y2="21.1709" layer="21"/>
<rectangle x1="14.7955" y1="21.1455" x2="22.4409" y2="21.1709" layer="21"/>
<rectangle x1="4.0259" y1="21.1709" x2="11.8491" y2="21.1963" layer="21"/>
<rectangle x1="14.8209" y1="21.1709" x2="22.4663" y2="21.1963" layer="21"/>
<rectangle x1="4.0005" y1="21.1963" x2="11.8491" y2="21.2217" layer="21"/>
<rectangle x1="14.8209" y1="21.1963" x2="22.4663" y2="21.2217" layer="21"/>
<rectangle x1="4.0005" y1="21.2217" x2="11.8237" y2="21.2471" layer="21"/>
<rectangle x1="14.8463" y1="21.2217" x2="22.4917" y2="21.2471" layer="21"/>
<rectangle x1="3.9751" y1="21.2471" x2="11.7983" y2="21.2725" layer="21"/>
<rectangle x1="14.8717" y1="21.2471" x2="22.5171" y2="21.2725" layer="21"/>
<rectangle x1="3.9497" y1="21.2725" x2="11.7983" y2="21.2979" layer="21"/>
<rectangle x1="14.8717" y1="21.2725" x2="22.5171" y2="21.2979" layer="21"/>
<rectangle x1="3.9497" y1="21.2979" x2="11.7729" y2="21.3233" layer="21"/>
<rectangle x1="14.8971" y1="21.2979" x2="22.5425" y2="21.3233" layer="21"/>
<rectangle x1="3.9243" y1="21.3233" x2="11.7729" y2="21.3487" layer="21"/>
<rectangle x1="14.8971" y1="21.3233" x2="22.5679" y2="21.3487" layer="21"/>
<rectangle x1="3.8989" y1="21.3487" x2="11.7475" y2="21.3741" layer="21"/>
<rectangle x1="14.9225" y1="21.3487" x2="22.5679" y2="21.3741" layer="21"/>
<rectangle x1="3.8989" y1="21.3741" x2="11.7221" y2="21.3995" layer="21"/>
<rectangle x1="14.9479" y1="21.3741" x2="22.5933" y2="21.3995" layer="21"/>
<rectangle x1="3.8735" y1="21.3995" x2="11.7221" y2="21.4249" layer="21"/>
<rectangle x1="14.9479" y1="21.3995" x2="22.6187" y2="21.4249" layer="21"/>
<rectangle x1="3.8481" y1="21.4249" x2="11.6967" y2="21.4503" layer="21"/>
<rectangle x1="14.9733" y1="21.4249" x2="22.6187" y2="21.4503" layer="21"/>
<rectangle x1="3.8481" y1="21.4503" x2="11.6967" y2="21.4757" layer="21"/>
<rectangle x1="14.9733" y1="21.4503" x2="22.6441" y2="21.4757" layer="21"/>
<rectangle x1="3.8227" y1="21.4757" x2="11.6713" y2="21.5011" layer="21"/>
<rectangle x1="14.9987" y1="21.4757" x2="22.6695" y2="21.5011" layer="21"/>
<rectangle x1="3.7973" y1="21.5011" x2="11.6459" y2="21.5265" layer="21"/>
<rectangle x1="15.0241" y1="21.5011" x2="22.6695" y2="21.5265" layer="21"/>
<rectangle x1="3.7973" y1="21.5265" x2="11.6459" y2="21.5519" layer="21"/>
<rectangle x1="15.0241" y1="21.5265" x2="22.6949" y2="21.5519" layer="21"/>
<rectangle x1="3.7719" y1="21.5519" x2="11.6205" y2="21.5773" layer="21"/>
<rectangle x1="15.0495" y1="21.5519" x2="22.7203" y2="21.5773" layer="21"/>
<rectangle x1="3.7465" y1="21.5773" x2="11.6205" y2="21.6027" layer="21"/>
<rectangle x1="15.0495" y1="21.5773" x2="22.7203" y2="21.6027" layer="21"/>
<rectangle x1="3.7465" y1="21.6027" x2="11.5951" y2="21.6281" layer="21"/>
<rectangle x1="15.0749" y1="21.6027" x2="22.7457" y2="21.6281" layer="21"/>
<rectangle x1="3.7211" y1="21.6281" x2="11.5697" y2="21.6535" layer="21"/>
<rectangle x1="15.1003" y1="21.6281" x2="22.7711" y2="21.6535" layer="21"/>
<rectangle x1="3.6957" y1="21.6535" x2="11.5697" y2="21.6789" layer="21"/>
<rectangle x1="15.1003" y1="21.6535" x2="22.7711" y2="21.6789" layer="21"/>
<rectangle x1="3.6957" y1="21.6789" x2="11.5443" y2="21.7043" layer="21"/>
<rectangle x1="15.1257" y1="21.6789" x2="22.7965" y2="21.7043" layer="21"/>
<rectangle x1="3.6703" y1="21.7043" x2="11.5443" y2="21.7297" layer="21"/>
<rectangle x1="15.1257" y1="21.7043" x2="22.8219" y2="21.7297" layer="21"/>
<rectangle x1="3.6449" y1="21.7297" x2="11.5189" y2="21.7551" layer="21"/>
<rectangle x1="15.1511" y1="21.7297" x2="22.8473" y2="21.7551" layer="21"/>
<rectangle x1="3.6449" y1="21.7551" x2="11.5189" y2="21.7805" layer="21"/>
<rectangle x1="15.1765" y1="21.7551" x2="22.8473" y2="21.7805" layer="21"/>
<rectangle x1="3.6195" y1="21.7805" x2="11.4935" y2="21.8059" layer="21"/>
<rectangle x1="15.1765" y1="21.7805" x2="22.8727" y2="21.8059" layer="21"/>
<rectangle x1="3.5941" y1="21.8059" x2="11.4681" y2="21.8313" layer="21"/>
<rectangle x1="15.2019" y1="21.8059" x2="22.8981" y2="21.8313" layer="21"/>
<rectangle x1="3.5941" y1="21.8313" x2="11.4681" y2="21.8567" layer="21"/>
<rectangle x1="15.2019" y1="21.8313" x2="22.8981" y2="21.8567" layer="21"/>
<rectangle x1="3.5687" y1="21.8567" x2="11.4427" y2="21.8821" layer="21"/>
<rectangle x1="15.2273" y1="21.8567" x2="22.9235" y2="21.8821" layer="21"/>
<rectangle x1="3.5433" y1="21.8821" x2="11.4427" y2="21.9075" layer="21"/>
<rectangle x1="15.2527" y1="21.8821" x2="22.9489" y2="21.9075" layer="21"/>
<rectangle x1="3.5433" y1="21.9075" x2="11.4173" y2="21.9329" layer="21"/>
<rectangle x1="15.2527" y1="21.9075" x2="22.9489" y2="21.9329" layer="21"/>
<rectangle x1="3.5179" y1="21.9329" x2="11.3919" y2="21.9583" layer="21"/>
<rectangle x1="15.2781" y1="21.9329" x2="22.9743" y2="21.9583" layer="21"/>
<rectangle x1="3.4925" y1="21.9583" x2="11.3919" y2="21.9837" layer="21"/>
<rectangle x1="15.2781" y1="21.9583" x2="22.9997" y2="21.9837" layer="21"/>
<rectangle x1="3.4925" y1="21.9837" x2="11.3665" y2="22.0091" layer="21"/>
<rectangle x1="15.3035" y1="21.9837" x2="22.9997" y2="22.0091" layer="21"/>
<rectangle x1="3.4671" y1="22.0091" x2="11.3665" y2="22.0345" layer="21"/>
<rectangle x1="15.3289" y1="22.0091" x2="23.0251" y2="22.0345" layer="21"/>
<rectangle x1="3.4417" y1="22.0345" x2="11.3411" y2="22.0599" layer="21"/>
<rectangle x1="15.3289" y1="22.0345" x2="23.0505" y2="22.0599" layer="21"/>
<rectangle x1="3.4417" y1="22.0599" x2="11.3157" y2="22.0853" layer="21"/>
<rectangle x1="15.3543" y1="22.0599" x2="23.0505" y2="22.0853" layer="21"/>
<rectangle x1="3.4163" y1="22.0853" x2="11.3157" y2="22.1107" layer="21"/>
<rectangle x1="15.3543" y1="22.0853" x2="23.0759" y2="22.1107" layer="21"/>
<rectangle x1="3.3909" y1="22.1107" x2="11.2903" y2="22.1361" layer="21"/>
<rectangle x1="15.3797" y1="22.1107" x2="23.1013" y2="22.1361" layer="21"/>
<rectangle x1="3.3909" y1="22.1361" x2="11.2903" y2="22.1615" layer="21"/>
<rectangle x1="15.4051" y1="22.1361" x2="23.1013" y2="22.1615" layer="21"/>
<rectangle x1="3.3655" y1="22.1615" x2="11.2649" y2="22.1869" layer="21"/>
<rectangle x1="15.4051" y1="22.1615" x2="23.1267" y2="22.1869" layer="21"/>
<rectangle x1="3.3401" y1="22.1869" x2="11.2649" y2="22.2123" layer="21"/>
<rectangle x1="15.4305" y1="22.1869" x2="23.1521" y2="22.2123" layer="21"/>
<rectangle x1="3.3401" y1="22.2123" x2="11.2395" y2="22.2377" layer="21"/>
<rectangle x1="15.4305" y1="22.2123" x2="23.1521" y2="22.2377" layer="21"/>
<rectangle x1="3.3147" y1="22.2377" x2="11.2141" y2="22.2631" layer="21"/>
<rectangle x1="15.4559" y1="22.2377" x2="23.1775" y2="22.2631" layer="21"/>
<rectangle x1="3.2893" y1="22.2631" x2="11.2141" y2="22.2885" layer="21"/>
<rectangle x1="15.4813" y1="22.2631" x2="23.2029" y2="22.2885" layer="21"/>
<rectangle x1="3.2893" y1="22.2885" x2="11.1887" y2="22.3139" layer="21"/>
<rectangle x1="15.4813" y1="22.2885" x2="23.2029" y2="22.3139" layer="21"/>
<rectangle x1="3.2639" y1="22.3139" x2="11.1887" y2="22.3393" layer="21"/>
<rectangle x1="15.5067" y1="22.3139" x2="23.2283" y2="22.3393" layer="21"/>
<rectangle x1="3.2385" y1="22.3393" x2="11.1633" y2="22.3647" layer="21"/>
<rectangle x1="15.5067" y1="22.3393" x2="23.2537" y2="22.3647" layer="21"/>
<rectangle x1="3.2385" y1="22.3647" x2="11.1379" y2="22.3901" layer="21"/>
<rectangle x1="15.5321" y1="22.3647" x2="23.2537" y2="22.3901" layer="21"/>
<rectangle x1="3.2131" y1="22.3901" x2="11.1379" y2="22.4155" layer="21"/>
<rectangle x1="15.5575" y1="22.3901" x2="23.2791" y2="22.4155" layer="21"/>
<rectangle x1="3.1877" y1="22.4155" x2="11.1125" y2="22.4409" layer="21"/>
<rectangle x1="15.5575" y1="22.4155" x2="23.3045" y2="22.4409" layer="21"/>
<rectangle x1="3.1877" y1="22.4409" x2="11.1125" y2="22.4663" layer="21"/>
<rectangle x1="15.5829" y1="22.4409" x2="23.3299" y2="22.4663" layer="21"/>
<rectangle x1="3.1623" y1="22.4663" x2="11.0871" y2="22.4917" layer="21"/>
<rectangle x1="15.5829" y1="22.4663" x2="23.3299" y2="22.4917" layer="21"/>
<rectangle x1="3.1369" y1="22.4917" x2="11.0617" y2="22.5171" layer="21"/>
<rectangle x1="15.6083" y1="22.4917" x2="23.3553" y2="22.5171" layer="21"/>
<rectangle x1="3.1369" y1="22.5171" x2="11.0617" y2="22.5425" layer="21"/>
<rectangle x1="15.6337" y1="22.5171" x2="23.3807" y2="22.5425" layer="21"/>
<rectangle x1="3.1115" y1="22.5425" x2="11.0363" y2="22.5679" layer="21"/>
<rectangle x1="15.6337" y1="22.5425" x2="23.3807" y2="22.5679" layer="21"/>
<rectangle x1="3.0861" y1="22.5679" x2="11.0363" y2="22.5933" layer="21"/>
<rectangle x1="15.6591" y1="22.5679" x2="23.4061" y2="22.5933" layer="21"/>
<rectangle x1="3.0861" y1="22.5933" x2="11.0109" y2="22.6187" layer="21"/>
<rectangle x1="15.6845" y1="22.5933" x2="23.4315" y2="22.6187" layer="21"/>
<rectangle x1="3.0607" y1="22.6187" x2="11.0109" y2="22.6441" layer="21"/>
<rectangle x1="15.6845" y1="22.6187" x2="23.4315" y2="22.6441" layer="21"/>
<rectangle x1="3.0353" y1="22.6441" x2="10.9855" y2="22.6695" layer="21"/>
<rectangle x1="15.7099" y1="22.6441" x2="23.4569" y2="22.6695" layer="21"/>
<rectangle x1="3.0353" y1="22.6695" x2="10.9601" y2="22.6949" layer="21"/>
<rectangle x1="15.7099" y1="22.6695" x2="23.4823" y2="22.6949" layer="21"/>
<rectangle x1="3.0099" y1="22.6949" x2="10.9601" y2="22.7203" layer="21"/>
<rectangle x1="15.7353" y1="22.6949" x2="23.4823" y2="22.7203" layer="21"/>
<rectangle x1="2.9845" y1="22.7203" x2="10.9347" y2="22.7457" layer="21"/>
<rectangle x1="15.7607" y1="22.7203" x2="23.5077" y2="22.7457" layer="21"/>
<rectangle x1="2.9845" y1="22.7457" x2="10.9347" y2="22.7711" layer="21"/>
<rectangle x1="15.7607" y1="22.7457" x2="23.5331" y2="22.7711" layer="21"/>
<rectangle x1="2.9591" y1="22.7711" x2="10.9093" y2="22.7965" layer="21"/>
<rectangle x1="15.7861" y1="22.7711" x2="23.5331" y2="22.7965" layer="21"/>
<rectangle x1="2.9337" y1="22.7965" x2="10.8839" y2="22.8219" layer="21"/>
<rectangle x1="15.7861" y1="22.7965" x2="23.5585" y2="22.8219" layer="21"/>
<rectangle x1="2.9337" y1="22.8219" x2="10.8839" y2="22.8473" layer="21"/>
<rectangle x1="15.8115" y1="22.8219" x2="23.5839" y2="22.8473" layer="21"/>
<rectangle x1="2.9083" y1="22.8473" x2="10.8585" y2="22.8727" layer="21"/>
<rectangle x1="15.8369" y1="22.8473" x2="23.5839" y2="22.8727" layer="21"/>
<rectangle x1="2.8829" y1="22.8727" x2="10.8585" y2="22.8981" layer="21"/>
<rectangle x1="15.8369" y1="22.8727" x2="23.6093" y2="22.8981" layer="21"/>
<rectangle x1="2.8829" y1="22.8981" x2="10.8331" y2="22.9235" layer="21"/>
<rectangle x1="15.8623" y1="22.8981" x2="23.6347" y2="22.9235" layer="21"/>
<rectangle x1="2.8575" y1="22.9235" x2="10.8077" y2="22.9489" layer="21"/>
<rectangle x1="15.8623" y1="22.9235" x2="23.6347" y2="22.9489" layer="21"/>
<rectangle x1="2.8321" y1="22.9489" x2="10.8077" y2="22.9743" layer="21"/>
<rectangle x1="15.8877" y1="22.9489" x2="23.6601" y2="22.9743" layer="21"/>
<rectangle x1="2.8321" y1="22.9743" x2="10.7823" y2="22.9997" layer="21"/>
<rectangle x1="15.9131" y1="22.9743" x2="23.6855" y2="22.9997" layer="21"/>
<rectangle x1="2.8067" y1="22.9997" x2="10.7823" y2="23.0251" layer="21"/>
<rectangle x1="15.9131" y1="22.9997" x2="23.6855" y2="23.0251" layer="21"/>
<rectangle x1="2.7813" y1="23.0251" x2="10.7569" y2="23.0505" layer="21"/>
<rectangle x1="15.9385" y1="23.0251" x2="23.7109" y2="23.0505" layer="21"/>
<rectangle x1="2.7813" y1="23.0505" x2="10.7315" y2="23.0759" layer="21"/>
<rectangle x1="15.9385" y1="23.0505" x2="23.7363" y2="23.0759" layer="21"/>
<rectangle x1="2.7559" y1="23.0759" x2="10.7315" y2="23.1013" layer="21"/>
<rectangle x1="15.9639" y1="23.0759" x2="23.7363" y2="23.1013" layer="21"/>
<rectangle x1="2.7305" y1="23.1013" x2="10.7061" y2="23.1267" layer="21"/>
<rectangle x1="15.9893" y1="23.1013" x2="23.7617" y2="23.1267" layer="21"/>
<rectangle x1="2.7305" y1="23.1267" x2="10.7061" y2="23.1521" layer="21"/>
<rectangle x1="15.9893" y1="23.1267" x2="23.7871" y2="23.1521" layer="21"/>
<rectangle x1="2.7051" y1="23.1521" x2="10.6807" y2="23.1775" layer="21"/>
<rectangle x1="16.0147" y1="23.1521" x2="23.7871" y2="23.1775" layer="21"/>
<rectangle x1="2.6797" y1="23.1775" x2="10.6807" y2="23.2029" layer="21"/>
<rectangle x1="16.0147" y1="23.1775" x2="23.8125" y2="23.2029" layer="21"/>
<rectangle x1="2.6797" y1="23.2029" x2="10.6553" y2="23.2283" layer="21"/>
<rectangle x1="16.0401" y1="23.2029" x2="23.8379" y2="23.2283" layer="21"/>
<rectangle x1="2.6543" y1="23.2283" x2="10.6299" y2="23.2537" layer="21"/>
<rectangle x1="16.0655" y1="23.2283" x2="23.8633" y2="23.2537" layer="21"/>
<rectangle x1="2.6289" y1="23.2537" x2="10.6299" y2="23.2791" layer="21"/>
<rectangle x1="16.0655" y1="23.2537" x2="23.8633" y2="23.2791" layer="21"/>
<rectangle x1="2.6289" y1="23.2791" x2="10.6045" y2="23.3045" layer="21"/>
<rectangle x1="16.0909" y1="23.2791" x2="23.8887" y2="23.3045" layer="21"/>
<rectangle x1="2.6035" y1="23.3045" x2="10.6045" y2="23.3299" layer="21"/>
<rectangle x1="16.0909" y1="23.3045" x2="23.9141" y2="23.3299" layer="21"/>
<rectangle x1="2.5781" y1="23.3299" x2="10.5791" y2="23.3553" layer="21"/>
<rectangle x1="16.1163" y1="23.3299" x2="23.9141" y2="23.3553" layer="21"/>
<rectangle x1="2.5781" y1="23.3553" x2="10.5537" y2="23.3807" layer="21"/>
<rectangle x1="16.1417" y1="23.3553" x2="23.9395" y2="23.3807" layer="21"/>
<rectangle x1="2.5527" y1="23.3807" x2="10.5537" y2="23.4061" layer="21"/>
<rectangle x1="16.1417" y1="23.3807" x2="23.9649" y2="23.4061" layer="21"/>
<rectangle x1="2.5273" y1="23.4061" x2="10.5283" y2="23.4315" layer="21"/>
<rectangle x1="16.1671" y1="23.4061" x2="23.9649" y2="23.4315" layer="21"/>
<rectangle x1="2.5273" y1="23.4315" x2="10.5283" y2="23.4569" layer="21"/>
<rectangle x1="16.1671" y1="23.4315" x2="23.9903" y2="23.4569" layer="21"/>
<rectangle x1="2.5019" y1="23.4569" x2="10.5029" y2="23.4823" layer="21"/>
<rectangle x1="16.1925" y1="23.4569" x2="24.0157" y2="23.4823" layer="21"/>
<rectangle x1="2.4765" y1="23.4823" x2="10.4775" y2="23.5077" layer="21"/>
<rectangle x1="16.2179" y1="23.4823" x2="24.0157" y2="23.5077" layer="21"/>
<rectangle x1="2.4765" y1="23.5077" x2="10.4775" y2="23.5331" layer="21"/>
<rectangle x1="16.2179" y1="23.5077" x2="24.0411" y2="23.5331" layer="21"/>
<rectangle x1="2.4511" y1="23.5331" x2="10.4521" y2="23.5585" layer="21"/>
<rectangle x1="16.2433" y1="23.5331" x2="24.0665" y2="23.5585" layer="21"/>
<rectangle x1="2.4257" y1="23.5585" x2="10.4521" y2="23.5839" layer="21"/>
<rectangle x1="16.2433" y1="23.5585" x2="24.0665" y2="23.5839" layer="21"/>
<rectangle x1="2.4257" y1="23.5839" x2="10.4267" y2="23.6093" layer="21"/>
<rectangle x1="16.2687" y1="23.5839" x2="24.0919" y2="23.6093" layer="21"/>
<rectangle x1="2.4003" y1="23.6093" x2="10.4267" y2="23.6347" layer="21"/>
<rectangle x1="16.2941" y1="23.6093" x2="24.1173" y2="23.6347" layer="21"/>
<rectangle x1="2.3749" y1="23.6347" x2="10.4013" y2="23.6601" layer="21"/>
<rectangle x1="16.2941" y1="23.6347" x2="24.1173" y2="23.6601" layer="21"/>
<rectangle x1="2.3749" y1="23.6601" x2="10.3759" y2="23.6855" layer="21"/>
<rectangle x1="16.3195" y1="23.6601" x2="24.1427" y2="23.6855" layer="21"/>
<rectangle x1="2.3495" y1="23.6855" x2="10.3759" y2="23.7109" layer="21"/>
<rectangle x1="16.3195" y1="23.6855" x2="24.1681" y2="23.7109" layer="21"/>
<rectangle x1="2.3241" y1="23.7109" x2="10.3505" y2="23.7363" layer="21"/>
<rectangle x1="16.3449" y1="23.7109" x2="24.1681" y2="23.7363" layer="21"/>
<rectangle x1="2.3241" y1="23.7363" x2="10.3505" y2="23.7617" layer="21"/>
<rectangle x1="16.3703" y1="23.7363" x2="24.1935" y2="23.7617" layer="21"/>
<rectangle x1="2.2987" y1="23.7617" x2="10.3251" y2="23.7871" layer="21"/>
<rectangle x1="16.3703" y1="23.7617" x2="24.2189" y2="23.7871" layer="21"/>
<rectangle x1="2.2733" y1="23.7871" x2="10.2997" y2="23.8125" layer="21"/>
<rectangle x1="16.3957" y1="23.7871" x2="24.2189" y2="23.8125" layer="21"/>
<rectangle x1="2.2733" y1="23.8125" x2="10.2997" y2="23.8379" layer="21"/>
<rectangle x1="16.3957" y1="23.8125" x2="24.2443" y2="23.8379" layer="21"/>
<rectangle x1="2.2479" y1="23.8379" x2="10.2743" y2="23.8633" layer="21"/>
<rectangle x1="16.4211" y1="23.8379" x2="24.2697" y2="23.8633" layer="21"/>
<rectangle x1="2.2225" y1="23.8633" x2="10.2743" y2="23.8887" layer="21"/>
<rectangle x1="16.4465" y1="23.8633" x2="24.2697" y2="23.8887" layer="21"/>
<rectangle x1="2.2225" y1="23.8887" x2="10.2489" y2="23.9141" layer="21"/>
<rectangle x1="16.4465" y1="23.8887" x2="24.2951" y2="23.9141" layer="21"/>
<rectangle x1="2.1971" y1="23.9141" x2="10.2235" y2="23.9395" layer="21"/>
<rectangle x1="16.4719" y1="23.9141" x2="24.3205" y2="23.9395" layer="21"/>
<rectangle x1="2.1717" y1="23.9395" x2="10.2235" y2="23.9649" layer="21"/>
<rectangle x1="16.4719" y1="23.9395" x2="24.3459" y2="23.9649" layer="21"/>
<rectangle x1="2.1717" y1="23.9649" x2="10.1981" y2="23.9903" layer="21"/>
<rectangle x1="16.4973" y1="23.9649" x2="24.3459" y2="23.9903" layer="21"/>
<rectangle x1="2.1463" y1="23.9903" x2="10.1981" y2="24.0157" layer="21"/>
<rectangle x1="16.5227" y1="23.9903" x2="24.3713" y2="24.0157" layer="21"/>
<rectangle x1="2.1209" y1="24.0157" x2="10.1727" y2="24.0411" layer="21"/>
<rectangle x1="16.5227" y1="24.0157" x2="24.3967" y2="24.0411" layer="21"/>
<rectangle x1="2.1209" y1="24.0411" x2="10.1727" y2="24.0665" layer="21"/>
<rectangle x1="16.5481" y1="24.0411" x2="24.3967" y2="24.0665" layer="21"/>
<rectangle x1="2.0955" y1="24.0665" x2="10.1473" y2="24.0919" layer="21"/>
<rectangle x1="16.5481" y1="24.0665" x2="24.4221" y2="24.0919" layer="21"/>
<rectangle x1="2.0701" y1="24.0919" x2="10.1219" y2="24.1173" layer="21"/>
<rectangle x1="16.5735" y1="24.0919" x2="24.4475" y2="24.1173" layer="21"/>
<rectangle x1="2.0701" y1="24.1173" x2="10.1219" y2="24.1427" layer="21"/>
<rectangle x1="16.5989" y1="24.1173" x2="24.4475" y2="24.1427" layer="21"/>
<rectangle x1="2.0447" y1="24.1427" x2="10.0965" y2="24.1681" layer="21"/>
<rectangle x1="16.5989" y1="24.1427" x2="24.4729" y2="24.1681" layer="21"/>
<rectangle x1="2.0193" y1="24.1681" x2="10.0965" y2="24.1935" layer="21"/>
<rectangle x1="16.6243" y1="24.1681" x2="24.4983" y2="24.1935" layer="21"/>
<rectangle x1="2.0193" y1="24.1935" x2="10.0711" y2="24.2189" layer="21"/>
<rectangle x1="16.6243" y1="24.1935" x2="24.4983" y2="24.2189" layer="21"/>
<rectangle x1="1.9939" y1="24.2189" x2="10.0457" y2="24.2443" layer="21"/>
<rectangle x1="16.6497" y1="24.2189" x2="24.5237" y2="24.2443" layer="21"/>
<rectangle x1="1.9685" y1="24.2443" x2="10.0457" y2="24.2697" layer="21"/>
<rectangle x1="16.6751" y1="24.2443" x2="24.5491" y2="24.2697" layer="21"/>
<rectangle x1="1.9685" y1="24.2697" x2="10.0203" y2="24.2951" layer="21"/>
<rectangle x1="16.6751" y1="24.2697" x2="24.5491" y2="24.2951" layer="21"/>
<rectangle x1="1.9431" y1="24.2951" x2="10.0203" y2="24.3205" layer="21"/>
<rectangle x1="16.7005" y1="24.2951" x2="24.5745" y2="24.3205" layer="21"/>
<rectangle x1="1.9177" y1="24.3205" x2="9.9949" y2="24.3459" layer="21"/>
<rectangle x1="16.7259" y1="24.3205" x2="24.5999" y2="24.3459" layer="21"/>
<rectangle x1="1.9177" y1="24.3459" x2="9.9695" y2="24.3713" layer="21"/>
<rectangle x1="16.7259" y1="24.3459" x2="24.5999" y2="24.3713" layer="21"/>
<rectangle x1="1.8923" y1="24.3713" x2="9.9695" y2="24.3967" layer="21"/>
<rectangle x1="16.7513" y1="24.3713" x2="24.6253" y2="24.3967" layer="21"/>
<rectangle x1="1.8669" y1="24.3967" x2="9.9441" y2="24.4221" layer="21"/>
<rectangle x1="16.7513" y1="24.3967" x2="24.6507" y2="24.4221" layer="21"/>
<rectangle x1="1.8669" y1="24.4221" x2="9.9441" y2="24.4475" layer="21"/>
<rectangle x1="16.7767" y1="24.4221" x2="24.6507" y2="24.4475" layer="21"/>
<rectangle x1="1.8415" y1="24.4475" x2="9.9187" y2="24.4729" layer="21"/>
<rectangle x1="16.8021" y1="24.4475" x2="24.6761" y2="24.4729" layer="21"/>
<rectangle x1="1.8161" y1="24.4729" x2="9.8933" y2="24.4983" layer="21"/>
<rectangle x1="16.8021" y1="24.4729" x2="24.7015" y2="24.4983" layer="21"/>
<rectangle x1="1.8161" y1="24.4983" x2="9.8933" y2="24.5237" layer="21"/>
<rectangle x1="16.8275" y1="24.4983" x2="24.7015" y2="24.5237" layer="21"/>
<rectangle x1="1.7907" y1="24.5237" x2="9.8679" y2="24.5491" layer="21"/>
<rectangle x1="16.8275" y1="24.5237" x2="24.7269" y2="24.5491" layer="21"/>
<rectangle x1="1.7653" y1="24.5491" x2="9.8679" y2="24.5745" layer="21"/>
<rectangle x1="16.8529" y1="24.5491" x2="24.7523" y2="24.5745" layer="21"/>
<rectangle x1="1.7653" y1="24.5745" x2="9.8425" y2="24.5999" layer="21"/>
<rectangle x1="16.8783" y1="24.5745" x2="24.7523" y2="24.5999" layer="21"/>
<rectangle x1="1.7399" y1="24.5999" x2="9.8425" y2="24.6253" layer="21"/>
<rectangle x1="16.8783" y1="24.5999" x2="24.7777" y2="24.6253" layer="21"/>
<rectangle x1="1.7145" y1="24.6253" x2="9.8171" y2="24.6507" layer="21"/>
<rectangle x1="16.9037" y1="24.6253" x2="24.8031" y2="24.6507" layer="21"/>
<rectangle x1="1.7145" y1="24.6507" x2="9.7917" y2="24.6761" layer="21"/>
<rectangle x1="16.9037" y1="24.6507" x2="24.8031" y2="24.6761" layer="21"/>
<rectangle x1="1.6891" y1="24.6761" x2="9.7917" y2="24.7015" layer="21"/>
<rectangle x1="16.9291" y1="24.6761" x2="24.8285" y2="24.7015" layer="21"/>
<rectangle x1="1.6637" y1="24.7015" x2="9.7663" y2="24.7269" layer="21"/>
<rectangle x1="16.9545" y1="24.7015" x2="24.8539" y2="24.7269" layer="21"/>
<rectangle x1="1.6637" y1="24.7269" x2="9.7663" y2="24.7523" layer="21"/>
<rectangle x1="16.9545" y1="24.7269" x2="24.8793" y2="24.7523" layer="21"/>
<rectangle x1="1.6383" y1="24.7523" x2="9.7409" y2="24.7777" layer="21"/>
<rectangle x1="16.9799" y1="24.7523" x2="24.8793" y2="24.7777" layer="21"/>
<rectangle x1="1.6383" y1="24.7777" x2="9.7155" y2="24.8031" layer="21"/>
<rectangle x1="16.9799" y1="24.7777" x2="24.9047" y2="24.8031" layer="21"/>
<rectangle x1="1.6383" y1="24.8031" x2="9.7155" y2="24.8285" layer="21"/>
<rectangle x1="17.0053" y1="24.8031" x2="24.9047" y2="24.8285" layer="21"/>
<rectangle x1="1.6129" y1="24.8285" x2="9.6901" y2="24.8539" layer="21"/>
<rectangle x1="17.0307" y1="24.8285" x2="24.9047" y2="24.8539" layer="21"/>
<rectangle x1="1.6129" y1="24.8539" x2="9.6901" y2="24.8793" layer="21"/>
<rectangle x1="17.0307" y1="24.8539" x2="24.9047" y2="24.8793" layer="21"/>
<rectangle x1="1.6129" y1="24.8793" x2="9.6647" y2="24.9047" layer="21"/>
<rectangle x1="17.0561" y1="24.8793" x2="24.9047" y2="24.9047" layer="21"/>
<rectangle x1="1.6383" y1="24.9047" x2="9.6393" y2="24.9301" layer="21"/>
<rectangle x1="17.0561" y1="24.9047" x2="24.9047" y2="24.9301" layer="21"/>
<rectangle x1="1.6383" y1="24.9301" x2="9.6393" y2="24.9555" layer="21"/>
<rectangle x1="17.0815" y1="24.9301" x2="24.8793" y2="24.9555" layer="21"/>
<rectangle x1="1.6383" y1="24.9555" x2="9.6139" y2="24.9809" layer="21"/>
<rectangle x1="17.1069" y1="24.9555" x2="24.8793" y2="24.9809" layer="21"/>
<rectangle x1="1.6637" y1="24.9809" x2="9.6139" y2="25.0063" layer="21"/>
<rectangle x1="17.1069" y1="24.9809" x2="24.8539" y2="25.0063" layer="21"/>
<rectangle x1="1.6891" y1="25.0063" x2="9.5885" y2="25.0317" layer="21"/>
<rectangle x1="17.1323" y1="25.0063" x2="24.8285" y2="25.0317" layer="21"/>
<rectangle x1="1.7399" y1="25.0317" x2="9.5377" y2="25.0571" layer="21"/>
<rectangle x1="17.1831" y1="25.0317" x2="24.8031" y2="25.0571" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="AIXLOGO">
<wire x1="-10.16" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="2.54" size="1.778" layer="94">AiX-Logo</text>
</symbol>
<symbol name="EAGLEXLOGO">
<text x="-2.54" y="2.54" size="1.778" layer="94">Eagle X</text>
<wire x1="-5.08" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AIXWARELOGO">
<gates>
<gate name="G$1" symbol="AIXLOGO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AIXLOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EAGLEXLOGO">
<gates>
<gate name="G$1" symbol="EAGLEXLOGO" x="-2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="EAGLEXLOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JST_AIX">
<packages>
<package name="JST2POSVER">
<smd name="P$1" x="-3.4" y="2.25" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P1" x="-1" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P2" x="1" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P$6" x="3.4" y="2.25" dx="3" dy="1.6" layer="1" rot="R90"/>
<text x="-5.94" y="5.67" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.67" y="-6.21" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.975" y1="4.75" x2="3.975" y2="4.75" width="0.127" layer="21"/>
<wire x1="3.975" y1="4.75" x2="3.975" y2="-0.25" width="0.127" layer="21"/>
<wire x1="3.975" y1="-0.25" x2="-3.975" y2="-0.25" width="0.127" layer="21"/>
<wire x1="-3.975" y1="-0.25" x2="-3.975" y2="4.75" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="JST2POSVER">
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<pin name="P1" x="-7.62" y="2.54" length="middle"/>
<pin name="P2" x="-7.62" y="-2.54" length="middle"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="JSTPOS2VERSMT">
<gates>
<gate name="G$1" symbol="JST2POSVER" x="2.54" y="5.08"/>
</gates>
<devices>
<device name="" package="JST2POSVER">
<connects>
<connect gate="G$1" pin="P1" pad="P1"/>
<connect gate="G$1" pin="P2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Conector_AIX">
<packages>
<package name="1437012">
<pad name="EXTRA" x="0" y="0" drill="1" diameter="1.7"/>
<pad name="P5" x="0" y="1.9" drill="1" diameter="1.7"/>
<pad name="P8" x="0" y="4" drill="1" diameter="1.7"/>
<pad name="P4" x="-2.08" y="1.9" drill="1" diameter="1.7"/>
<pad name="P6" x="2.08" y="1.9" drill="1" diameter="1.7"/>
<pad name="P3" x="-2.72" y="4" drill="1" diameter="1.7"/>
<pad name="P7" x="2.72" y="4" drill="1" diameter="1.7"/>
<pad name="P2" x="-1.5" y="6.1" drill="1" diameter="1.7"/>
<pad name="P1" x="1.5" y="6.1" drill="1" diameter="1.7"/>
<wire x1="-10" y1="-3.8" x2="10" y2="-3.8" width="0.127" layer="21"/>
<wire x1="-10" y1="-5.8" x2="10" y2="-5.8" width="0.127" layer="21" style="dashdot"/>
<text x="-1.27" y="-3.81" size="1.27" layer="21">PCB</text>
<wire x1="-10" y1="-3.81" x2="-10" y2="5.4" width="0.127" layer="21"/>
<wire x1="-10" y1="5.4" x2="-5" y2="5.4" width="0.127" layer="21"/>
<wire x1="-5" y1="5.4" x2="-5" y2="8.7" width="0.127" layer="21"/>
<wire x1="-5" y1="8.7" x2="5" y2="8.7" width="0.127" layer="21"/>
<wire x1="5" y1="8.7" x2="5" y2="5.4" width="0.127" layer="21"/>
<wire x1="5" y1="5.4" x2="10" y2="5.4" width="0.127" layer="21"/>
<wire x1="10" y1="5.4" x2="10" y2="-3.81" width="0.127" layer="21"/>
<hole x="-7.5" y="4" drill="2.05"/>
<hole x="7.5" y="4" drill="2.05"/>
<hole x="-7.5" y="0" drill="2.8"/>
<hole x="7.5" y="0" drill="2.8"/>
</package>
<package name="T4145435021-001">
<pad name="EXTRA" x="0" y="0" drill="1.3"/>
<pad name="P5" x="0" y="4" drill="1.3"/>
<pad name="P4" x="-1.77" y="2.23" drill="1.3"/>
<pad name="P3" x="1.77" y="2.23" drill="1.3"/>
<pad name="P2" x="1.77" y="5.77" drill="1.3"/>
<pad name="P1" x="-1.77" y="5.77" drill="1.3"/>
<wire x1="-10.16" y1="-4.5" x2="10.16" y2="-4.5" width="0.127" layer="21" style="dashdot"/>
<text x="11.43" y="-3.81" size="1.27" layer="21">Panel</text>
<hole x="-7.5" y="0" drill="2.8"/>
<hole x="7.5" y="0" drill="2.8"/>
<hole x="-7.5" y="4" drill="2.35"/>
<hole x="7.5" y="4" drill="2.35"/>
<wire x1="-9.75" y1="5.08" x2="-9.75" y2="-3.81" width="0.127" layer="21"/>
<wire x1="9.75" y1="-3.81" x2="9.75" y2="5.08" width="0.127" layer="21"/>
</package>
<package name="53047-0410">
<pad name="3D+" x="0" y="0" drill="0.5"/>
<pad name="4GND" x="-1.25" y="0" drill="0.5"/>
<pad name="2D-" x="1.25" y="0" drill="0.5"/>
<pad name="1+" x="2.5" y="0" drill="0.5"/>
<wire x1="-2.75" y1="1.125" x2="4" y2="1.125" width="0.127" layer="21"/>
<wire x1="4" y1="1.125" x2="4" y2="-2.075" width="0.127" layer="21"/>
<wire x1="4" y1="-2.075" x2="-2.75" y2="-2.075" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-2.075" x2="-2.75" y2="1.125" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="1.905" y="-1.905" size="1.27" layer="21">+</text>
<text x="-1.905" y="-1.905" size="1.27" layer="21">-</text>
</package>
<package name="0533240260">
<pad name="P1" x="0" y="0" drill="0.8"/>
<pad name="P2" x="-2" y="0" drill="0.8"/>
<hole x="2.05" y="1.1" drill="1.4"/>
<wire x1="-4.65" y1="1.7" x2="2.65" y2="1.7" width="0.127" layer="21"/>
<wire x1="2.65" y1="1.7" x2="2.65" y2="-2.85" width="0.127" layer="21"/>
<wire x1="2.65" y1="-2.85" x2="-4.65" y2="-2.85" width="0.127" layer="21"/>
<wire x1="-4.65" y1="-2.85" x2="-4.65" y2="1.7" width="0.127" layer="21"/>
<text x="-5.08" y="2.54" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="0353631060">
<pad name="P1" x="-9" y="0" drill="0.8"/>
<pad name="P2" x="-7" y="0" drill="0.8"/>
<pad name="P3" x="-5" y="0" drill="0.8"/>
<pad name="P4" x="-3" y="0" drill="0.8"/>
<pad name="P5" x="-1" y="0" drill="0.8"/>
<pad name="P6" x="1" y="0" drill="0.8"/>
<pad name="P7" x="3" y="0" drill="0.8"/>
<pad name="P8" x="5" y="0" drill="0.8"/>
<pad name="P9" x="7" y="0" drill="0.8"/>
<pad name="P10" x="9" y="0" drill="0.8"/>
<wire x1="-11.5" y1="8.55" x2="-11.5" y2="0" width="0.127" layer="21"/>
<wire x1="-11.5" y1="0" x2="11.5" y2="0" width="0.127" layer="21"/>
<wire x1="11.5" y1="0" x2="11.5" y2="8.55" width="0.127" layer="21"/>
<wire x1="11.5" y1="8.55" x2="-11.5" y2="8.55" width="0.127" layer="21"/>
<text x="-11.43" y="8.89" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="1437012">
<wire x1="-5.08" y1="10.16" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="-5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-12.7" x2="-5.08" y2="10.16" width="0.254" layer="94"/>
<pin name="P1" x="5.08" y="7.62" length="short" rot="R180"/>
<pin name="P2" x="5.08" y="5.08" length="short" rot="R180"/>
<pin name="P3" x="5.08" y="2.54" length="short" rot="R180"/>
<pin name="P4" x="5.08" y="0" length="short" rot="R180"/>
<pin name="P5" x="5.08" y="-2.54" length="short" rot="R180"/>
<pin name="P6" x="5.08" y="-5.08" length="short" rot="R180"/>
<pin name="P7" x="5.08" y="-7.62" length="short" rot="R180"/>
<pin name="P8" x="5.08" y="-10.16" length="short" rot="R180"/>
<text x="-5.08" y="12.7" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="CON2POS_SIMPLE">
<wire x1="-2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<pin name="P1+" x="-5.08" y="2.54" length="short"/>
<pin name="P2-" x="-5.08" y="-2.54" length="short"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="53047-0410">
<pin name="VCC" x="-2.54" y="5.08" length="short"/>
<pin name="D-" x="-2.54" y="2.54" length="short"/>
<pin name="D+" x="-2.54" y="0" length="short"/>
<pin name="GND" x="-2.54" y="-2.54" length="short"/>
<wire x1="0" y1="7.62" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<text x="0" y="10.16" size="1.27" layer="95">&gt;NAME</text>
<text x="0" y="-7.62" size="1.27" layer="97">USB header</text>
</symbol>
<symbol name="0533240260">
<pin name="P1" x="-2.54" y="2.54" length="short"/>
<pin name="P2" x="-2.54" y="0" length="short"/>
<wire x1="0" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="5.08" width="0.254" layer="94"/>
<text x="0" y="7.62" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="0353631060">
<pin name="P1" x="-2.54" y="10.16" length="short"/>
<pin name="P2" x="-2.54" y="7.62" length="short"/>
<pin name="P3" x="-2.54" y="5.08" length="short"/>
<pin name="P4" x="-2.54" y="2.54" length="short"/>
<pin name="P5" x="-2.54" y="0" length="short"/>
<pin name="P6" x="-2.54" y="-2.54" length="short"/>
<pin name="P7" x="-2.54" y="-5.08" length="short"/>
<pin name="P8" x="-2.54" y="-7.62" length="short"/>
<pin name="P9" x="-2.54" y="-10.16" length="short"/>
<pin name="P10" x="-2.54" y="-12.7" length="short"/>
<wire x1="0" y1="12.7" x2="0" y2="-15.24" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="0" y2="12.7" width="0.254" layer="94"/>
<text x="0" y="15.24" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1437012">
<gates>
<gate name="G$1" symbol="1437012" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1437012">
<connects>
<connect gate="G$1" pin="P1" pad="P1"/>
<connect gate="G$1" pin="P2" pad="P2"/>
<connect gate="G$1" pin="P3" pad="P3"/>
<connect gate="G$1" pin="P4" pad="P4"/>
<connect gate="G$1" pin="P5" pad="P5"/>
<connect gate="G$1" pin="P6" pad="P6"/>
<connect gate="G$1" pin="P7" pad="P7"/>
<connect gate="G$1" pin="P8" pad="P8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="T4145435021-001">
<gates>
<gate name="G$1" symbol="CON2POS_SIMPLE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="T4145435021-001">
<connects>
<connect gate="G$1" pin="P1+" pad="P1"/>
<connect gate="G$1" pin="P2-" pad="P3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="53047-0410">
<gates>
<gate name="G$1" symbol="53047-0410" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="53047-0410">
<connects>
<connect gate="G$1" pin="D+" pad="3D+"/>
<connect gate="G$1" pin="D-" pad="2D-"/>
<connect gate="G$1" pin="GND" pad="4GND"/>
<connect gate="G$1" pin="VCC" pad="1+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0533240260">
<gates>
<gate name="G$1" symbol="0533240260" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="0533240260">
<connects>
<connect gate="G$1" pin="P1" pad="P1"/>
<connect gate="G$1" pin="P2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0353631060">
<gates>
<gate name="G$1" symbol="0353631060" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0353631060">
<connects>
<connect gate="G$1" pin="P1" pad="P1"/>
<connect gate="G$1" pin="P10" pad="P10"/>
<connect gate="G$1" pin="P2" pad="P2"/>
<connect gate="G$1" pin="P3" pad="P3"/>
<connect gate="G$1" pin="P4" pad="P4"/>
<connect gate="G$1" pin="P5" pad="P5"/>
<connect gate="G$1" pin="P6" pad="P6"/>
<connect gate="G$1" pin="P7" pad="P7"/>
<connect gate="G$1" pin="P8" pad="P8"/>
<connect gate="G$1" pin="P9" pad="P9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Misc_AIX">
<packages>
<package name="LOGOTECHMAKE">
<rectangle x1="3.1877" y1="0.4445" x2="3.7973" y2="0.4699" layer="21"/>
<rectangle x1="3.1877" y1="0.4699" x2="3.7973" y2="0.4953" layer="21"/>
<rectangle x1="3.1877" y1="0.4953" x2="3.7973" y2="0.5207" layer="21"/>
<rectangle x1="3.1877" y1="0.5207" x2="3.7973" y2="0.5461" layer="21"/>
<rectangle x1="3.1877" y1="0.5461" x2="3.7973" y2="0.5715" layer="21"/>
<rectangle x1="3.1877" y1="0.5715" x2="3.7973" y2="0.5969" layer="21"/>
<rectangle x1="3.1877" y1="0.5969" x2="3.7973" y2="0.6223" layer="21"/>
<rectangle x1="3.1877" y1="0.6223" x2="3.7973" y2="0.6477" layer="21"/>
<rectangle x1="3.1877" y1="0.6477" x2="3.7973" y2="0.6731" layer="21"/>
<rectangle x1="3.1877" y1="0.6731" x2="3.7973" y2="0.6985" layer="21"/>
<rectangle x1="3.1877" y1="0.6985" x2="3.7973" y2="0.7239" layer="21"/>
<rectangle x1="3.1877" y1="0.7239" x2="3.7973" y2="0.7493" layer="21"/>
<rectangle x1="3.1877" y1="0.7493" x2="3.7973" y2="0.7747" layer="21"/>
<rectangle x1="3.1877" y1="0.7747" x2="3.7973" y2="0.8001" layer="21"/>
<rectangle x1="3.1877" y1="0.8001" x2="3.7973" y2="0.8255" layer="21"/>
<rectangle x1="3.1877" y1="0.8255" x2="3.7973" y2="0.8509" layer="21"/>
<rectangle x1="3.1877" y1="0.8509" x2="3.7973" y2="0.8763" layer="21"/>
<rectangle x1="3.1877" y1="0.8763" x2="3.7973" y2="0.9017" layer="21"/>
<rectangle x1="3.1877" y1="0.9017" x2="3.7973" y2="0.9271" layer="21"/>
<rectangle x1="3.1877" y1="0.9271" x2="3.7973" y2="0.9525" layer="21"/>
<rectangle x1="3.1877" y1="0.9525" x2="3.7973" y2="0.9779" layer="21"/>
<rectangle x1="3.1877" y1="0.9779" x2="3.7973" y2="1.0033" layer="21"/>
<rectangle x1="3.1877" y1="1.0033" x2="3.7973" y2="1.0287" layer="21"/>
<rectangle x1="3.1877" y1="1.0287" x2="3.7973" y2="1.0541" layer="21"/>
<rectangle x1="3.1877" y1="1.0541" x2="3.7973" y2="1.0795" layer="21"/>
<rectangle x1="3.1877" y1="1.0795" x2="3.7973" y2="1.1049" layer="21"/>
<rectangle x1="3.1877" y1="1.1049" x2="3.7973" y2="1.1303" layer="21"/>
<rectangle x1="3.1877" y1="1.1303" x2="3.7973" y2="1.1557" layer="21"/>
<rectangle x1="3.1877" y1="1.1557" x2="3.7973" y2="1.1811" layer="21"/>
<rectangle x1="1.7907" y1="1.1811" x2="2.4257" y2="1.2065" layer="21"/>
<rectangle x1="3.1877" y1="1.1811" x2="3.7973" y2="1.2065" layer="21"/>
<rectangle x1="6.0325" y1="1.1811" x2="6.6421" y2="1.2065" layer="21"/>
<rectangle x1="1.7907" y1="1.2065" x2="2.4257" y2="1.2319" layer="21"/>
<rectangle x1="3.1877" y1="1.2065" x2="3.7973" y2="1.2319" layer="21"/>
<rectangle x1="6.0325" y1="1.2065" x2="6.6421" y2="1.2319" layer="21"/>
<rectangle x1="1.7907" y1="1.2319" x2="2.4257" y2="1.2573" layer="21"/>
<rectangle x1="3.1877" y1="1.2319" x2="3.7973" y2="1.2573" layer="21"/>
<rectangle x1="6.0325" y1="1.2319" x2="6.6421" y2="1.2573" layer="21"/>
<rectangle x1="1.7907" y1="1.2573" x2="2.4257" y2="1.2827" layer="21"/>
<rectangle x1="3.1877" y1="1.2573" x2="3.7973" y2="1.2827" layer="21"/>
<rectangle x1="6.0325" y1="1.2573" x2="6.6421" y2="1.2827" layer="21"/>
<rectangle x1="1.7907" y1="1.2827" x2="2.4257" y2="1.3081" layer="21"/>
<rectangle x1="3.1877" y1="1.2827" x2="3.7973" y2="1.3081" layer="21"/>
<rectangle x1="6.0325" y1="1.2827" x2="6.6421" y2="1.3081" layer="21"/>
<rectangle x1="1.7907" y1="1.3081" x2="2.4257" y2="1.3335" layer="21"/>
<rectangle x1="3.1877" y1="1.3081" x2="3.7973" y2="1.3335" layer="21"/>
<rectangle x1="6.0325" y1="1.3081" x2="6.6421" y2="1.3335" layer="21"/>
<rectangle x1="1.7907" y1="1.3335" x2="2.4257" y2="1.3589" layer="21"/>
<rectangle x1="3.1877" y1="1.3335" x2="3.7973" y2="1.3589" layer="21"/>
<rectangle x1="6.0325" y1="1.3335" x2="6.6421" y2="1.3589" layer="21"/>
<rectangle x1="1.7907" y1="1.3589" x2="2.4257" y2="1.3843" layer="21"/>
<rectangle x1="3.1877" y1="1.3589" x2="3.7973" y2="1.3843" layer="21"/>
<rectangle x1="6.0325" y1="1.3589" x2="6.6421" y2="1.3843" layer="21"/>
<rectangle x1="1.7907" y1="1.3843" x2="2.4257" y2="1.4097" layer="21"/>
<rectangle x1="3.1877" y1="1.3843" x2="3.7973" y2="1.4097" layer="21"/>
<rectangle x1="6.0325" y1="1.3843" x2="6.6421" y2="1.4097" layer="21"/>
<rectangle x1="1.7907" y1="1.4097" x2="2.4257" y2="1.4351" layer="21"/>
<rectangle x1="3.1877" y1="1.4097" x2="3.7973" y2="1.4351" layer="21"/>
<rectangle x1="6.0325" y1="1.4097" x2="6.6421" y2="1.4351" layer="21"/>
<rectangle x1="1.7907" y1="1.4351" x2="2.4257" y2="1.4605" layer="21"/>
<rectangle x1="3.1877" y1="1.4351" x2="3.7973" y2="1.4605" layer="21"/>
<rectangle x1="6.0325" y1="1.4351" x2="6.6421" y2="1.4605" layer="21"/>
<rectangle x1="1.7907" y1="1.4605" x2="2.4257" y2="1.4859" layer="21"/>
<rectangle x1="3.1877" y1="1.4605" x2="3.7973" y2="1.4859" layer="21"/>
<rectangle x1="6.0325" y1="1.4605" x2="6.6421" y2="1.4859" layer="21"/>
<rectangle x1="1.7907" y1="1.4859" x2="2.4257" y2="1.5113" layer="21"/>
<rectangle x1="3.1877" y1="1.4859" x2="3.7973" y2="1.5113" layer="21"/>
<rectangle x1="6.0325" y1="1.4859" x2="6.6421" y2="1.5113" layer="21"/>
<rectangle x1="1.7907" y1="1.5113" x2="2.4257" y2="1.5367" layer="21"/>
<rectangle x1="3.1877" y1="1.5113" x2="3.7973" y2="1.5367" layer="21"/>
<rectangle x1="6.0325" y1="1.5113" x2="6.6421" y2="1.5367" layer="21"/>
<rectangle x1="1.7907" y1="1.5367" x2="2.4257" y2="1.5621" layer="21"/>
<rectangle x1="3.1877" y1="1.5367" x2="3.7973" y2="1.5621" layer="21"/>
<rectangle x1="6.0325" y1="1.5367" x2="6.6421" y2="1.5621" layer="21"/>
<rectangle x1="1.7907" y1="1.5621" x2="2.4257" y2="1.5875" layer="21"/>
<rectangle x1="3.1877" y1="1.5621" x2="3.7973" y2="1.5875" layer="21"/>
<rectangle x1="6.0325" y1="1.5621" x2="6.6421" y2="1.5875" layer="21"/>
<rectangle x1="1.7907" y1="1.5875" x2="2.4257" y2="1.6129" layer="21"/>
<rectangle x1="3.1877" y1="1.5875" x2="3.7973" y2="1.6129" layer="21"/>
<rectangle x1="6.0325" y1="1.5875" x2="6.6421" y2="1.6129" layer="21"/>
<rectangle x1="1.7907" y1="1.6129" x2="2.4257" y2="1.6383" layer="21"/>
<rectangle x1="3.1877" y1="1.6129" x2="3.7973" y2="1.6383" layer="21"/>
<rectangle x1="6.0325" y1="1.6129" x2="6.6421" y2="1.6383" layer="21"/>
<rectangle x1="1.7907" y1="1.6383" x2="2.4257" y2="1.6637" layer="21"/>
<rectangle x1="3.1877" y1="1.6383" x2="3.7973" y2="1.6637" layer="21"/>
<rectangle x1="6.0325" y1="1.6383" x2="6.6421" y2="1.6637" layer="21"/>
<rectangle x1="1.7907" y1="1.6637" x2="2.4257" y2="1.6891" layer="21"/>
<rectangle x1="3.1877" y1="1.6637" x2="3.7973" y2="1.6891" layer="21"/>
<rectangle x1="6.0325" y1="1.6637" x2="6.6421" y2="1.6891" layer="21"/>
<rectangle x1="1.7907" y1="1.6891" x2="2.4257" y2="1.7145" layer="21"/>
<rectangle x1="3.1877" y1="1.6891" x2="3.7973" y2="1.7145" layer="21"/>
<rectangle x1="6.0325" y1="1.6891" x2="6.6421" y2="1.7145" layer="21"/>
<rectangle x1="1.7907" y1="1.7145" x2="2.4257" y2="1.7399" layer="21"/>
<rectangle x1="3.1877" y1="1.7145" x2="3.7973" y2="1.7399" layer="21"/>
<rectangle x1="6.0325" y1="1.7145" x2="6.6421" y2="1.7399" layer="21"/>
<rectangle x1="1.7907" y1="1.7399" x2="2.4257" y2="1.7653" layer="21"/>
<rectangle x1="3.1877" y1="1.7399" x2="3.7973" y2="1.7653" layer="21"/>
<rectangle x1="4.6355" y1="1.7399" x2="5.2451" y2="1.7653" layer="21"/>
<rectangle x1="6.0325" y1="1.7399" x2="6.6421" y2="1.7653" layer="21"/>
<rectangle x1="1.7907" y1="1.7653" x2="2.4257" y2="1.7907" layer="21"/>
<rectangle x1="3.1877" y1="1.7653" x2="3.7973" y2="1.7907" layer="21"/>
<rectangle x1="4.6355" y1="1.7653" x2="5.2451" y2="1.7907" layer="21"/>
<rectangle x1="6.0325" y1="1.7653" x2="6.6421" y2="1.7907" layer="21"/>
<rectangle x1="11.9507" y1="1.7653" x2="12.1793" y2="1.7907" layer="21"/>
<rectangle x1="13.5509" y1="1.7653" x2="13.5763" y2="1.7907" layer="21"/>
<rectangle x1="13.9573" y1="1.7653" x2="14.1859" y2="1.7907" layer="21"/>
<rectangle x1="15.1257" y1="1.7653" x2="15.1511" y2="1.7907" layer="21"/>
<rectangle x1="15.8115" y1="1.7653" x2="16.0401" y2="1.7907" layer="21"/>
<rectangle x1="16.3957" y1="1.7653" x2="16.5989" y2="1.7907" layer="21"/>
<rectangle x1="1.7907" y1="1.7907" x2="2.4257" y2="1.8161" layer="21"/>
<rectangle x1="3.1877" y1="1.7907" x2="3.7973" y2="1.8161" layer="21"/>
<rectangle x1="4.6355" y1="1.7907" x2="5.2451" y2="1.8161" layer="21"/>
<rectangle x1="6.0325" y1="1.7907" x2="6.6421" y2="1.8161" layer="21"/>
<rectangle x1="9.8933" y1="1.7907" x2="10.3759" y2="1.8161" layer="21"/>
<rectangle x1="10.5029" y1="1.7907" x2="10.9855" y2="1.8161" layer="21"/>
<rectangle x1="11.0871" y1="1.7907" x2="11.5951" y2="1.8161" layer="21"/>
<rectangle x1="11.8999" y1="1.7907" x2="12.2809" y2="1.8161" layer="21"/>
<rectangle x1="12.6111" y1="1.7907" x2="12.6873" y2="1.8161" layer="21"/>
<rectangle x1="13.0429" y1="1.7907" x2="13.1191" y2="1.8161" layer="21"/>
<rectangle x1="13.5255" y1="1.7907" x2="13.6017" y2="1.8161" layer="21"/>
<rectangle x1="13.9065" y1="1.7907" x2="14.2621" y2="1.8161" layer="21"/>
<rectangle x1="14.5923" y1="1.7907" x2="14.6685" y2="1.8161" layer="21"/>
<rectangle x1="15.1003" y1="1.7907" x2="15.1765" y2="1.8161" layer="21"/>
<rectangle x1="15.3543" y1="1.7907" x2="15.4051" y2="1.8161" layer="21"/>
<rectangle x1="15.7353" y1="1.7907" x2="16.1417" y2="1.8161" layer="21"/>
<rectangle x1="16.2941" y1="1.7907" x2="16.6751" y2="1.8161" layer="21"/>
<rectangle x1="1.7907" y1="1.8161" x2="2.4257" y2="1.8415" layer="21"/>
<rectangle x1="3.1877" y1="1.8161" x2="3.7973" y2="1.8415" layer="21"/>
<rectangle x1="4.6355" y1="1.8161" x2="5.2451" y2="1.8415" layer="21"/>
<rectangle x1="6.0325" y1="1.8161" x2="6.6421" y2="1.8415" layer="21"/>
<rectangle x1="9.8933" y1="1.8161" x2="10.3759" y2="1.8415" layer="21"/>
<rectangle x1="10.5029" y1="1.8161" x2="10.9855" y2="1.8415" layer="21"/>
<rectangle x1="11.0871" y1="1.8161" x2="11.5951" y2="1.8415" layer="21"/>
<rectangle x1="11.8491" y1="1.8161" x2="12.0015" y2="1.8415" layer="21"/>
<rectangle x1="12.1285" y1="1.8161" x2="12.2809" y2="1.8415" layer="21"/>
<rectangle x1="12.6111" y1="1.8161" x2="12.6873" y2="1.8415" layer="21"/>
<rectangle x1="13.0429" y1="1.8161" x2="13.1191" y2="1.8415" layer="21"/>
<rectangle x1="13.5001" y1="1.8161" x2="13.5763" y2="1.8415" layer="21"/>
<rectangle x1="13.8557" y1="1.8161" x2="14.0081" y2="1.8415" layer="21"/>
<rectangle x1="14.1605" y1="1.8161" x2="14.3129" y2="1.8415" layer="21"/>
<rectangle x1="14.5923" y1="1.8161" x2="14.6685" y2="1.8415" layer="21"/>
<rectangle x1="15.0749" y1="1.8161" x2="15.1765" y2="1.8415" layer="21"/>
<rectangle x1="15.3543" y1="1.8161" x2="15.4051" y2="1.8415" layer="21"/>
<rectangle x1="15.7099" y1="1.8161" x2="15.8623" y2="1.8415" layer="21"/>
<rectangle x1="15.9893" y1="1.8161" x2="16.1417" y2="1.8415" layer="21"/>
<rectangle x1="16.2687" y1="1.8161" x2="16.4211" y2="1.8415" layer="21"/>
<rectangle x1="16.5735" y1="1.8161" x2="16.7005" y2="1.8415" layer="21"/>
<rectangle x1="1.7907" y1="1.8415" x2="2.4257" y2="1.8669" layer="21"/>
<rectangle x1="3.1877" y1="1.8415" x2="3.7973" y2="1.8669" layer="21"/>
<rectangle x1="4.6355" y1="1.8415" x2="5.2451" y2="1.8669" layer="21"/>
<rectangle x1="6.0325" y1="1.8415" x2="6.6421" y2="1.8669" layer="21"/>
<rectangle x1="9.8933" y1="1.8415" x2="9.9441" y2="1.8669" layer="21"/>
<rectangle x1="10.5029" y1="1.8415" x2="10.5791" y2="1.8669" layer="21"/>
<rectangle x1="11.0871" y1="1.8415" x2="11.1633" y2="1.8669" layer="21"/>
<rectangle x1="11.8237" y1="1.8415" x2="11.9253" y2="1.8669" layer="21"/>
<rectangle x1="12.2301" y1="1.8415" x2="12.2809" y2="1.8669" layer="21"/>
<rectangle x1="12.6111" y1="1.8415" x2="12.6873" y2="1.8669" layer="21"/>
<rectangle x1="13.0429" y1="1.8415" x2="13.1191" y2="1.8669" layer="21"/>
<rectangle x1="13.5001" y1="1.8415" x2="13.5763" y2="1.8669" layer="21"/>
<rectangle x1="13.8303" y1="1.8415" x2="13.9319" y2="1.8669" layer="21"/>
<rectangle x1="14.2367" y1="1.8415" x2="14.3383" y2="1.8669" layer="21"/>
<rectangle x1="14.5923" y1="1.8415" x2="14.6685" y2="1.8669" layer="21"/>
<rectangle x1="15.0749" y1="1.8415" x2="15.1765" y2="1.8669" layer="21"/>
<rectangle x1="15.3543" y1="1.8415" x2="15.4051" y2="1.8669" layer="21"/>
<rectangle x1="15.6845" y1="1.8415" x2="15.7861" y2="1.8669" layer="21"/>
<rectangle x1="16.0909" y1="1.8415" x2="16.1417" y2="1.8669" layer="21"/>
<rectangle x1="16.2941" y1="1.8415" x2="16.3449" y2="1.8669" layer="21"/>
<rectangle x1="16.6243" y1="1.8415" x2="16.7259" y2="1.8669" layer="21"/>
<rectangle x1="1.7907" y1="1.8669" x2="2.4257" y2="1.8923" layer="21"/>
<rectangle x1="3.1877" y1="1.8669" x2="3.7973" y2="1.8923" layer="21"/>
<rectangle x1="4.6355" y1="1.8669" x2="5.2451" y2="1.8923" layer="21"/>
<rectangle x1="6.0325" y1="1.8669" x2="6.6421" y2="1.8923" layer="21"/>
<rectangle x1="9.8933" y1="1.8669" x2="9.9441" y2="1.8923" layer="21"/>
<rectangle x1="10.5029" y1="1.8669" x2="10.5791" y2="1.8923" layer="21"/>
<rectangle x1="11.0871" y1="1.8669" x2="11.1633" y2="1.8923" layer="21"/>
<rectangle x1="11.7983" y1="1.8669" x2="11.8999" y2="1.8923" layer="21"/>
<rectangle x1="12.6111" y1="1.8669" x2="12.6873" y2="1.8923" layer="21"/>
<rectangle x1="13.0429" y1="1.8669" x2="13.1191" y2="1.8923" layer="21"/>
<rectangle x1="13.4747" y1="1.8669" x2="13.5509" y2="1.8923" layer="21"/>
<rectangle x1="13.8049" y1="1.8669" x2="13.9065" y2="1.8923" layer="21"/>
<rectangle x1="14.2621" y1="1.8669" x2="14.3637" y2="1.8923" layer="21"/>
<rectangle x1="14.5923" y1="1.8669" x2="14.6685" y2="1.8923" layer="21"/>
<rectangle x1="15.0495" y1="1.8669" x2="15.1765" y2="1.8923" layer="21"/>
<rectangle x1="15.3543" y1="1.8669" x2="15.4051" y2="1.8923" layer="21"/>
<rectangle x1="15.6591" y1="1.8669" x2="15.7353" y2="1.8923" layer="21"/>
<rectangle x1="16.6497" y1="1.8669" x2="16.7513" y2="1.8923" layer="21"/>
<rectangle x1="1.7907" y1="1.8923" x2="2.4257" y2="1.9177" layer="21"/>
<rectangle x1="3.1877" y1="1.8923" x2="3.7973" y2="1.9177" layer="21"/>
<rectangle x1="4.6355" y1="1.8923" x2="5.2451" y2="1.9177" layer="21"/>
<rectangle x1="6.0325" y1="1.8923" x2="6.6421" y2="1.9177" layer="21"/>
<rectangle x1="9.8933" y1="1.8923" x2="9.9441" y2="1.9177" layer="21"/>
<rectangle x1="10.5029" y1="1.8923" x2="10.5791" y2="1.9177" layer="21"/>
<rectangle x1="11.0871" y1="1.8923" x2="11.1633" y2="1.9177" layer="21"/>
<rectangle x1="11.7729" y1="1.8923" x2="11.8745" y2="1.9177" layer="21"/>
<rectangle x1="12.6111" y1="1.8923" x2="12.6873" y2="1.9177" layer="21"/>
<rectangle x1="13.0429" y1="1.8923" x2="13.1191" y2="1.9177" layer="21"/>
<rectangle x1="13.4493" y1="1.8923" x2="13.5255" y2="1.9177" layer="21"/>
<rectangle x1="13.7795" y1="1.8923" x2="13.8811" y2="1.9177" layer="21"/>
<rectangle x1="14.2875" y1="1.8923" x2="14.3891" y2="1.9177" layer="21"/>
<rectangle x1="14.5923" y1="1.8923" x2="14.6685" y2="1.9177" layer="21"/>
<rectangle x1="15.0495" y1="1.8923" x2="15.1765" y2="1.9177" layer="21"/>
<rectangle x1="15.3543" y1="1.8923" x2="15.4051" y2="1.9177" layer="21"/>
<rectangle x1="15.6337" y1="1.8923" x2="15.7099" y2="1.9177" layer="21"/>
<rectangle x1="16.6751" y1="1.8923" x2="16.7513" y2="1.9177" layer="21"/>
<rectangle x1="1.7907" y1="1.9177" x2="2.4257" y2="1.9431" layer="21"/>
<rectangle x1="3.1877" y1="1.9177" x2="3.7973" y2="1.9431" layer="21"/>
<rectangle x1="4.6355" y1="1.9177" x2="5.2451" y2="1.9431" layer="21"/>
<rectangle x1="6.0325" y1="1.9177" x2="6.6421" y2="1.9431" layer="21"/>
<rectangle x1="9.8933" y1="1.9177" x2="9.9441" y2="1.9431" layer="21"/>
<rectangle x1="10.5029" y1="1.9177" x2="10.5791" y2="1.9431" layer="21"/>
<rectangle x1="11.0871" y1="1.9177" x2="11.1633" y2="1.9431" layer="21"/>
<rectangle x1="11.7729" y1="1.9177" x2="11.8491" y2="1.9431" layer="21"/>
<rectangle x1="12.6111" y1="1.9177" x2="12.6873" y2="1.9431" layer="21"/>
<rectangle x1="13.0429" y1="1.9177" x2="13.1191" y2="1.9431" layer="21"/>
<rectangle x1="13.4493" y1="1.9177" x2="13.5255" y2="1.9431" layer="21"/>
<rectangle x1="13.7795" y1="1.9177" x2="13.8557" y2="1.9431" layer="21"/>
<rectangle x1="14.3129" y1="1.9177" x2="14.3891" y2="1.9431" layer="21"/>
<rectangle x1="14.5923" y1="1.9177" x2="14.6685" y2="1.9431" layer="21"/>
<rectangle x1="15.0241" y1="1.9177" x2="15.1003" y2="1.9431" layer="21"/>
<rectangle x1="15.1257" y1="1.9177" x2="15.1765" y2="1.9431" layer="21"/>
<rectangle x1="15.3543" y1="1.9177" x2="15.4051" y2="1.9431" layer="21"/>
<rectangle x1="15.6083" y1="1.9177" x2="15.6845" y2="1.9431" layer="21"/>
<rectangle x1="16.7005" y1="1.9177" x2="16.7513" y2="1.9431" layer="21"/>
<rectangle x1="1.7907" y1="1.9431" x2="2.4257" y2="1.9685" layer="21"/>
<rectangle x1="3.1877" y1="1.9431" x2="3.7973" y2="1.9685" layer="21"/>
<rectangle x1="4.6355" y1="1.9431" x2="5.2451" y2="1.9685" layer="21"/>
<rectangle x1="6.0325" y1="1.9431" x2="6.6421" y2="1.9685" layer="21"/>
<rectangle x1="9.8933" y1="1.9431" x2="9.9441" y2="1.9685" layer="21"/>
<rectangle x1="10.5029" y1="1.9431" x2="10.5791" y2="1.9685" layer="21"/>
<rectangle x1="11.0871" y1="1.9431" x2="11.1633" y2="1.9685" layer="21"/>
<rectangle x1="11.7475" y1="1.9431" x2="11.8237" y2="1.9685" layer="21"/>
<rectangle x1="12.6111" y1="1.9431" x2="12.6873" y2="1.9685" layer="21"/>
<rectangle x1="13.0429" y1="1.9431" x2="13.1191" y2="1.9685" layer="21"/>
<rectangle x1="13.4239" y1="1.9431" x2="13.5001" y2="1.9685" layer="21"/>
<rectangle x1="13.7541" y1="1.9431" x2="13.8303" y2="1.9685" layer="21"/>
<rectangle x1="14.3383" y1="1.9431" x2="14.4145" y2="1.9685" layer="21"/>
<rectangle x1="14.5923" y1="1.9431" x2="14.6685" y2="1.9685" layer="21"/>
<rectangle x1="14.9987" y1="1.9431" x2="15.0749" y2="1.9685" layer="21"/>
<rectangle x1="15.1257" y1="1.9431" x2="15.1765" y2="1.9685" layer="21"/>
<rectangle x1="15.3543" y1="1.9431" x2="15.4051" y2="1.9685" layer="21"/>
<rectangle x1="15.6083" y1="1.9431" x2="15.6845" y2="1.9685" layer="21"/>
<rectangle x1="16.7005" y1="1.9431" x2="16.7513" y2="1.9685" layer="21"/>
<rectangle x1="1.7907" y1="1.9685" x2="2.4257" y2="1.9939" layer="21"/>
<rectangle x1="3.1877" y1="1.9685" x2="3.7973" y2="1.9939" layer="21"/>
<rectangle x1="4.6355" y1="1.9685" x2="5.2451" y2="1.9939" layer="21"/>
<rectangle x1="6.0325" y1="1.9685" x2="6.6421" y2="1.9939" layer="21"/>
<rectangle x1="9.8933" y1="1.9685" x2="9.9441" y2="1.9939" layer="21"/>
<rectangle x1="10.5029" y1="1.9685" x2="10.5791" y2="1.9939" layer="21"/>
<rectangle x1="11.0871" y1="1.9685" x2="11.1633" y2="1.9939" layer="21"/>
<rectangle x1="11.7475" y1="1.9685" x2="11.8237" y2="1.9939" layer="21"/>
<rectangle x1="12.6111" y1="1.9685" x2="12.6873" y2="1.9939" layer="21"/>
<rectangle x1="13.0429" y1="1.9685" x2="13.1191" y2="1.9939" layer="21"/>
<rectangle x1="13.4239" y1="1.9685" x2="13.5001" y2="1.9939" layer="21"/>
<rectangle x1="13.7541" y1="1.9685" x2="13.8303" y2="1.9939" layer="21"/>
<rectangle x1="14.3383" y1="1.9685" x2="14.4145" y2="1.9939" layer="21"/>
<rectangle x1="14.5923" y1="1.9685" x2="14.6685" y2="1.9939" layer="21"/>
<rectangle x1="14.9987" y1="1.9685" x2="15.0749" y2="1.9939" layer="21"/>
<rectangle x1="15.1257" y1="1.9685" x2="15.1765" y2="1.9939" layer="21"/>
<rectangle x1="15.3543" y1="1.9685" x2="15.4051" y2="1.9939" layer="21"/>
<rectangle x1="15.5829" y1="1.9685" x2="15.6591" y2="1.9939" layer="21"/>
<rectangle x1="16.7005" y1="1.9685" x2="16.7767" y2="1.9939" layer="21"/>
<rectangle x1="1.7907" y1="1.9939" x2="2.4257" y2="2.0193" layer="21"/>
<rectangle x1="3.1877" y1="1.9939" x2="3.7973" y2="2.0193" layer="21"/>
<rectangle x1="4.6355" y1="1.9939" x2="5.2451" y2="2.0193" layer="21"/>
<rectangle x1="6.0325" y1="1.9939" x2="6.6421" y2="2.0193" layer="21"/>
<rectangle x1="9.8933" y1="1.9939" x2="9.9441" y2="2.0193" layer="21"/>
<rectangle x1="10.5029" y1="1.9939" x2="10.5791" y2="2.0193" layer="21"/>
<rectangle x1="11.0871" y1="1.9939" x2="11.1633" y2="2.0193" layer="21"/>
<rectangle x1="11.7221" y1="1.9939" x2="11.7983" y2="2.0193" layer="21"/>
<rectangle x1="12.6111" y1="1.9939" x2="12.6873" y2="2.0193" layer="21"/>
<rectangle x1="13.0429" y1="1.9939" x2="13.1191" y2="2.0193" layer="21"/>
<rectangle x1="13.3985" y1="1.9939" x2="13.4747" y2="2.0193" layer="21"/>
<rectangle x1="13.7541" y1="1.9939" x2="13.8049" y2="2.0193" layer="21"/>
<rectangle x1="14.3637" y1="1.9939" x2="14.4145" y2="2.0193" layer="21"/>
<rectangle x1="14.5923" y1="1.9939" x2="14.6685" y2="2.0193" layer="21"/>
<rectangle x1="14.9733" y1="1.9939" x2="15.0495" y2="2.0193" layer="21"/>
<rectangle x1="15.1257" y1="1.9939" x2="15.1765" y2="2.0193" layer="21"/>
<rectangle x1="15.3543" y1="1.9939" x2="15.4051" y2="2.0193" layer="21"/>
<rectangle x1="15.5829" y1="1.9939" x2="15.6591" y2="2.0193" layer="21"/>
<rectangle x1="16.7005" y1="1.9939" x2="16.7513" y2="2.0193" layer="21"/>
<rectangle x1="1.7907" y1="2.0193" x2="2.4257" y2="2.0447" layer="21"/>
<rectangle x1="3.1877" y1="2.0193" x2="3.7973" y2="2.0447" layer="21"/>
<rectangle x1="4.6355" y1="2.0193" x2="5.2451" y2="2.0447" layer="21"/>
<rectangle x1="6.0325" y1="2.0193" x2="6.6421" y2="2.0447" layer="21"/>
<rectangle x1="9.8933" y1="2.0193" x2="9.9441" y2="2.0447" layer="21"/>
<rectangle x1="10.5029" y1="2.0193" x2="10.5791" y2="2.0447" layer="21"/>
<rectangle x1="11.0871" y1="2.0193" x2="11.1633" y2="2.0447" layer="21"/>
<rectangle x1="11.7221" y1="2.0193" x2="11.7983" y2="2.0447" layer="21"/>
<rectangle x1="12.6111" y1="2.0193" x2="12.6873" y2="2.0447" layer="21"/>
<rectangle x1="13.0429" y1="2.0193" x2="13.1191" y2="2.0447" layer="21"/>
<rectangle x1="13.3731" y1="2.0193" x2="13.4493" y2="2.0447" layer="21"/>
<rectangle x1="13.7287" y1="2.0193" x2="13.8049" y2="2.0447" layer="21"/>
<rectangle x1="14.3637" y1="2.0193" x2="14.4399" y2="2.0447" layer="21"/>
<rectangle x1="14.5923" y1="2.0193" x2="14.6685" y2="2.0447" layer="21"/>
<rectangle x1="14.9479" y1="2.0193" x2="15.0241" y2="2.0447" layer="21"/>
<rectangle x1="15.1257" y1="2.0193" x2="15.1765" y2="2.0447" layer="21"/>
<rectangle x1="15.3543" y1="2.0193" x2="15.4051" y2="2.0447" layer="21"/>
<rectangle x1="15.5829" y1="2.0193" x2="15.6337" y2="2.0447" layer="21"/>
<rectangle x1="16.7005" y1="2.0193" x2="16.7513" y2="2.0447" layer="21"/>
<rectangle x1="1.7907" y1="2.0447" x2="2.4257" y2="2.0701" layer="21"/>
<rectangle x1="3.1877" y1="2.0447" x2="3.7973" y2="2.0701" layer="21"/>
<rectangle x1="4.6355" y1="2.0447" x2="5.2451" y2="2.0701" layer="21"/>
<rectangle x1="6.0325" y1="2.0447" x2="6.6421" y2="2.0701" layer="21"/>
<rectangle x1="9.8933" y1="2.0447" x2="9.9441" y2="2.0701" layer="21"/>
<rectangle x1="10.5029" y1="2.0447" x2="10.5791" y2="2.0701" layer="21"/>
<rectangle x1="11.0871" y1="2.0447" x2="11.1633" y2="2.0701" layer="21"/>
<rectangle x1="11.7221" y1="2.0447" x2="11.7983" y2="2.0701" layer="21"/>
<rectangle x1="12.6111" y1="2.0447" x2="12.6873" y2="2.0701" layer="21"/>
<rectangle x1="13.0429" y1="2.0447" x2="13.1191" y2="2.0701" layer="21"/>
<rectangle x1="13.3731" y1="2.0447" x2="13.4493" y2="2.0701" layer="21"/>
<rectangle x1="13.7287" y1="2.0447" x2="13.8049" y2="2.0701" layer="21"/>
<rectangle x1="14.3637" y1="2.0447" x2="14.4399" y2="2.0701" layer="21"/>
<rectangle x1="14.5923" y1="2.0447" x2="14.6685" y2="2.0701" layer="21"/>
<rectangle x1="14.9479" y1="2.0447" x2="15.0241" y2="2.0701" layer="21"/>
<rectangle x1="15.1257" y1="2.0447" x2="15.1765" y2="2.0701" layer="21"/>
<rectangle x1="15.3543" y1="2.0447" x2="15.4051" y2="2.0701" layer="21"/>
<rectangle x1="15.5829" y1="2.0447" x2="15.6337" y2="2.0701" layer="21"/>
<rectangle x1="16.6751" y1="2.0447" x2="16.7513" y2="2.0701" layer="21"/>
<rectangle x1="1.7907" y1="2.0701" x2="2.4257" y2="2.0955" layer="21"/>
<rectangle x1="3.1877" y1="2.0701" x2="3.7973" y2="2.0955" layer="21"/>
<rectangle x1="4.6355" y1="2.0701" x2="5.2451" y2="2.0955" layer="21"/>
<rectangle x1="6.0325" y1="2.0701" x2="6.6421" y2="2.0955" layer="21"/>
<rectangle x1="9.8933" y1="2.0701" x2="9.9441" y2="2.0955" layer="21"/>
<rectangle x1="10.5029" y1="2.0701" x2="10.5791" y2="2.0955" layer="21"/>
<rectangle x1="11.0871" y1="2.0701" x2="11.1633" y2="2.0955" layer="21"/>
<rectangle x1="11.7221" y1="2.0701" x2="11.7729" y2="2.0955" layer="21"/>
<rectangle x1="12.6111" y1="2.0701" x2="12.6873" y2="2.0955" layer="21"/>
<rectangle x1="13.0429" y1="2.0701" x2="13.1191" y2="2.0955" layer="21"/>
<rectangle x1="13.3477" y1="2.0701" x2="13.4239" y2="2.0955" layer="21"/>
<rectangle x1="13.7287" y1="2.0701" x2="13.7795" y2="2.0955" layer="21"/>
<rectangle x1="14.3637" y1="2.0701" x2="14.4399" y2="2.0955" layer="21"/>
<rectangle x1="14.5923" y1="2.0701" x2="14.6685" y2="2.0955" layer="21"/>
<rectangle x1="14.9225" y1="2.0701" x2="14.9987" y2="2.0955" layer="21"/>
<rectangle x1="15.1257" y1="2.0701" x2="15.1765" y2="2.0955" layer="21"/>
<rectangle x1="15.3543" y1="2.0701" x2="15.4051" y2="2.0955" layer="21"/>
<rectangle x1="15.5829" y1="2.0701" x2="15.6337" y2="2.0955" layer="21"/>
<rectangle x1="16.6497" y1="2.0701" x2="16.7259" y2="2.0955" layer="21"/>
<rectangle x1="1.7907" y1="2.0955" x2="2.4257" y2="2.1209" layer="21"/>
<rectangle x1="3.1877" y1="2.0955" x2="3.7973" y2="2.1209" layer="21"/>
<rectangle x1="4.6355" y1="2.0955" x2="5.2451" y2="2.1209" layer="21"/>
<rectangle x1="6.0325" y1="2.0955" x2="6.6421" y2="2.1209" layer="21"/>
<rectangle x1="9.8933" y1="2.0955" x2="9.9441" y2="2.1209" layer="21"/>
<rectangle x1="10.5029" y1="2.0955" x2="10.5791" y2="2.1209" layer="21"/>
<rectangle x1="11.0871" y1="2.0955" x2="11.1633" y2="2.1209" layer="21"/>
<rectangle x1="11.7221" y1="2.0955" x2="11.7729" y2="2.1209" layer="21"/>
<rectangle x1="12.6111" y1="2.0955" x2="12.6873" y2="2.1209" layer="21"/>
<rectangle x1="13.0429" y1="2.0955" x2="13.3985" y2="2.1209" layer="21"/>
<rectangle x1="13.7287" y1="2.0955" x2="13.7795" y2="2.1209" layer="21"/>
<rectangle x1="14.3891" y1="2.0955" x2="14.4399" y2="2.1209" layer="21"/>
<rectangle x1="14.5923" y1="2.0955" x2="14.6685" y2="2.1209" layer="21"/>
<rectangle x1="14.8971" y1="2.0955" x2="14.9733" y2="2.1209" layer="21"/>
<rectangle x1="15.1257" y1="2.0955" x2="15.1765" y2="2.1209" layer="21"/>
<rectangle x1="15.3543" y1="2.0955" x2="15.4051" y2="2.1209" layer="21"/>
<rectangle x1="15.5575" y1="2.0955" x2="15.6337" y2="2.1209" layer="21"/>
<rectangle x1="16.6243" y1="2.0955" x2="16.7259" y2="2.1209" layer="21"/>
<rectangle x1="1.7907" y1="2.1209" x2="2.4257" y2="2.1463" layer="21"/>
<rectangle x1="3.1877" y1="2.1209" x2="3.7973" y2="2.1463" layer="21"/>
<rectangle x1="4.6355" y1="2.1209" x2="5.2451" y2="2.1463" layer="21"/>
<rectangle x1="6.0325" y1="2.1209" x2="6.6421" y2="2.1463" layer="21"/>
<rectangle x1="9.8933" y1="2.1209" x2="9.9441" y2="2.1463" layer="21"/>
<rectangle x1="10.5029" y1="2.1209" x2="10.5791" y2="2.1463" layer="21"/>
<rectangle x1="11.0871" y1="2.1209" x2="11.1633" y2="2.1463" layer="21"/>
<rectangle x1="11.7221" y1="2.1209" x2="11.7729" y2="2.1463" layer="21"/>
<rectangle x1="12.6111" y1="2.1209" x2="12.6873" y2="2.1463" layer="21"/>
<rectangle x1="13.0429" y1="2.1209" x2="13.4239" y2="2.1463" layer="21"/>
<rectangle x1="13.7287" y1="2.1209" x2="13.7795" y2="2.1463" layer="21"/>
<rectangle x1="14.3891" y1="2.1209" x2="14.4399" y2="2.1463" layer="21"/>
<rectangle x1="14.5923" y1="2.1209" x2="14.6685" y2="2.1463" layer="21"/>
<rectangle x1="14.8971" y1="2.1209" x2="14.9733" y2="2.1463" layer="21"/>
<rectangle x1="15.1257" y1="2.1209" x2="15.1765" y2="2.1463" layer="21"/>
<rectangle x1="15.3543" y1="2.1209" x2="15.4051" y2="2.1463" layer="21"/>
<rectangle x1="15.5575" y1="2.1209" x2="15.6337" y2="2.1463" layer="21"/>
<rectangle x1="16.5735" y1="2.1209" x2="16.7005" y2="2.1463" layer="21"/>
<rectangle x1="1.7907" y1="2.1463" x2="2.4257" y2="2.1717" layer="21"/>
<rectangle x1="3.1877" y1="2.1463" x2="3.7973" y2="2.1717" layer="21"/>
<rectangle x1="4.6355" y1="2.1463" x2="5.2451" y2="2.1717" layer="21"/>
<rectangle x1="6.0325" y1="2.1463" x2="6.6421" y2="2.1717" layer="21"/>
<rectangle x1="9.8933" y1="2.1463" x2="10.3251" y2="2.1717" layer="21"/>
<rectangle x1="10.5029" y1="2.1463" x2="10.5791" y2="2.1717" layer="21"/>
<rectangle x1="11.0871" y1="2.1463" x2="11.5189" y2="2.1717" layer="21"/>
<rectangle x1="11.7221" y1="2.1463" x2="11.7729" y2="2.1717" layer="21"/>
<rectangle x1="12.6111" y1="2.1463" x2="12.6873" y2="2.1717" layer="21"/>
<rectangle x1="13.0429" y1="2.1463" x2="13.1699" y2="2.1717" layer="21"/>
<rectangle x1="13.2969" y1="2.1463" x2="13.4747" y2="2.1717" layer="21"/>
<rectangle x1="13.7287" y1="2.1463" x2="13.7795" y2="2.1717" layer="21"/>
<rectangle x1="14.3891" y1="2.1463" x2="14.4399" y2="2.1717" layer="21"/>
<rectangle x1="14.5923" y1="2.1463" x2="14.6685" y2="2.1717" layer="21"/>
<rectangle x1="14.8717" y1="2.1463" x2="14.9479" y2="2.1717" layer="21"/>
<rectangle x1="15.1257" y1="2.1463" x2="15.1765" y2="2.1717" layer="21"/>
<rectangle x1="15.3543" y1="2.1463" x2="15.4051" y2="2.1717" layer="21"/>
<rectangle x1="15.5575" y1="2.1463" x2="15.6337" y2="2.1717" layer="21"/>
<rectangle x1="16.4973" y1="2.1463" x2="16.6497" y2="2.1717" layer="21"/>
<rectangle x1="1.7907" y1="2.1717" x2="2.4257" y2="2.1971" layer="21"/>
<rectangle x1="3.1877" y1="2.1717" x2="3.7973" y2="2.1971" layer="21"/>
<rectangle x1="4.6355" y1="2.1717" x2="5.2451" y2="2.1971" layer="21"/>
<rectangle x1="6.0325" y1="2.1717" x2="6.6421" y2="2.1971" layer="21"/>
<rectangle x1="9.8933" y1="2.1717" x2="10.3251" y2="2.1971" layer="21"/>
<rectangle x1="10.5029" y1="2.1717" x2="10.5791" y2="2.1971" layer="21"/>
<rectangle x1="11.0871" y1="2.1717" x2="11.5443" y2="2.1971" layer="21"/>
<rectangle x1="11.7221" y1="2.1717" x2="11.7729" y2="2.1971" layer="21"/>
<rectangle x1="12.6111" y1="2.1717" x2="12.6873" y2="2.1971" layer="21"/>
<rectangle x1="13.0429" y1="2.1717" x2="13.1191" y2="2.1971" layer="21"/>
<rectangle x1="13.3731" y1="2.1717" x2="13.5001" y2="2.1971" layer="21"/>
<rectangle x1="13.7287" y1="2.1717" x2="13.7795" y2="2.1971" layer="21"/>
<rectangle x1="14.3891" y1="2.1717" x2="14.4399" y2="2.1971" layer="21"/>
<rectangle x1="14.5923" y1="2.1717" x2="14.6685" y2="2.1971" layer="21"/>
<rectangle x1="14.8463" y1="2.1717" x2="14.9225" y2="2.1971" layer="21"/>
<rectangle x1="15.1257" y1="2.1717" x2="15.1765" y2="2.1971" layer="21"/>
<rectangle x1="15.3543" y1="2.1717" x2="15.4051" y2="2.1971" layer="21"/>
<rectangle x1="15.5575" y1="2.1717" x2="15.6337" y2="2.1971" layer="21"/>
<rectangle x1="16.4465" y1="2.1717" x2="16.5989" y2="2.1971" layer="21"/>
<rectangle x1="1.7907" y1="2.1971" x2="2.4257" y2="2.2225" layer="21"/>
<rectangle x1="3.1877" y1="2.1971" x2="3.7973" y2="2.2225" layer="21"/>
<rectangle x1="4.6355" y1="2.1971" x2="5.2451" y2="2.2225" layer="21"/>
<rectangle x1="6.0325" y1="2.1971" x2="6.6421" y2="2.2225" layer="21"/>
<rectangle x1="9.8933" y1="2.1971" x2="9.9695" y2="2.2225" layer="21"/>
<rectangle x1="10.5029" y1="2.1971" x2="10.5791" y2="2.2225" layer="21"/>
<rectangle x1="11.0871" y1="2.1971" x2="11.1633" y2="2.2225" layer="21"/>
<rectangle x1="11.7221" y1="2.1971" x2="11.7729" y2="2.2225" layer="21"/>
<rectangle x1="12.6111" y1="2.1971" x2="12.6873" y2="2.2225" layer="21"/>
<rectangle x1="13.0429" y1="2.1971" x2="13.1191" y2="2.2225" layer="21"/>
<rectangle x1="13.4239" y1="2.1971" x2="13.5255" y2="2.2225" layer="21"/>
<rectangle x1="13.7287" y1="2.1971" x2="13.7795" y2="2.2225" layer="21"/>
<rectangle x1="14.3891" y1="2.1971" x2="14.4399" y2="2.2225" layer="21"/>
<rectangle x1="14.5923" y1="2.1971" x2="14.6685" y2="2.2225" layer="21"/>
<rectangle x1="14.8463" y1="2.1971" x2="14.9225" y2="2.2225" layer="21"/>
<rectangle x1="15.1257" y1="2.1971" x2="15.1765" y2="2.2225" layer="21"/>
<rectangle x1="15.3543" y1="2.1971" x2="15.4051" y2="2.2225" layer="21"/>
<rectangle x1="15.5575" y1="2.1971" x2="15.6337" y2="2.2225" layer="21"/>
<rectangle x1="16.3957" y1="2.1971" x2="16.5481" y2="2.2225" layer="21"/>
<rectangle x1="1.7907" y1="2.2225" x2="2.4257" y2="2.2479" layer="21"/>
<rectangle x1="3.1877" y1="2.2225" x2="3.7973" y2="2.2479" layer="21"/>
<rectangle x1="4.6355" y1="2.2225" x2="5.2451" y2="2.2479" layer="21"/>
<rectangle x1="6.0325" y1="2.2225" x2="6.6421" y2="2.2479" layer="21"/>
<rectangle x1="9.8933" y1="2.2225" x2="9.9441" y2="2.2479" layer="21"/>
<rectangle x1="10.5029" y1="2.2225" x2="10.5791" y2="2.2479" layer="21"/>
<rectangle x1="11.0871" y1="2.2225" x2="11.1633" y2="2.2479" layer="21"/>
<rectangle x1="11.7221" y1="2.2225" x2="11.7729" y2="2.2479" layer="21"/>
<rectangle x1="12.6111" y1="2.2225" x2="12.6873" y2="2.2479" layer="21"/>
<rectangle x1="13.0429" y1="2.2225" x2="13.1191" y2="2.2479" layer="21"/>
<rectangle x1="13.4493" y1="2.2225" x2="13.5255" y2="2.2479" layer="21"/>
<rectangle x1="13.7287" y1="2.2225" x2="13.7795" y2="2.2479" layer="21"/>
<rectangle x1="14.3891" y1="2.2225" x2="14.4399" y2="2.2479" layer="21"/>
<rectangle x1="14.5923" y1="2.2225" x2="14.6685" y2="2.2479" layer="21"/>
<rectangle x1="14.8209" y1="2.2225" x2="14.8971" y2="2.2479" layer="21"/>
<rectangle x1="15.1257" y1="2.2225" x2="15.1765" y2="2.2479" layer="21"/>
<rectangle x1="15.3543" y1="2.2225" x2="15.4051" y2="2.2479" layer="21"/>
<rectangle x1="15.5575" y1="2.2225" x2="15.6337" y2="2.2479" layer="21"/>
<rectangle x1="16.3703" y1="2.2225" x2="16.4973" y2="2.2479" layer="21"/>
<rectangle x1="1.7907" y1="2.2479" x2="2.4257" y2="2.2733" layer="21"/>
<rectangle x1="3.1877" y1="2.2479" x2="3.7973" y2="2.2733" layer="21"/>
<rectangle x1="4.6355" y1="2.2479" x2="5.2451" y2="2.2733" layer="21"/>
<rectangle x1="6.0325" y1="2.2479" x2="6.6421" y2="2.2733" layer="21"/>
<rectangle x1="9.8933" y1="2.2479" x2="9.9441" y2="2.2733" layer="21"/>
<rectangle x1="10.5029" y1="2.2479" x2="10.5791" y2="2.2733" layer="21"/>
<rectangle x1="11.0871" y1="2.2479" x2="11.1633" y2="2.2733" layer="21"/>
<rectangle x1="11.7221" y1="2.2479" x2="11.7729" y2="2.2733" layer="21"/>
<rectangle x1="12.6111" y1="2.2479" x2="12.6873" y2="2.2733" layer="21"/>
<rectangle x1="13.0429" y1="2.2479" x2="13.1191" y2="2.2733" layer="21"/>
<rectangle x1="13.4747" y1="2.2479" x2="13.5509" y2="2.2733" layer="21"/>
<rectangle x1="13.7287" y1="2.2479" x2="13.7795" y2="2.2733" layer="21"/>
<rectangle x1="14.3891" y1="2.2479" x2="14.4399" y2="2.2733" layer="21"/>
<rectangle x1="14.5923" y1="2.2479" x2="14.6685" y2="2.2733" layer="21"/>
<rectangle x1="14.7955" y1="2.2479" x2="14.8717" y2="2.2733" layer="21"/>
<rectangle x1="15.1257" y1="2.2479" x2="15.1765" y2="2.2733" layer="21"/>
<rectangle x1="15.3543" y1="2.2479" x2="15.4051" y2="2.2733" layer="21"/>
<rectangle x1="15.5575" y1="2.2479" x2="15.6337" y2="2.2733" layer="21"/>
<rectangle x1="16.3195" y1="2.2479" x2="16.4465" y2="2.2733" layer="21"/>
<rectangle x1="1.7907" y1="2.2733" x2="2.4257" y2="2.2987" layer="21"/>
<rectangle x1="3.1877" y1="2.2733" x2="3.7973" y2="2.2987" layer="21"/>
<rectangle x1="4.6355" y1="2.2733" x2="5.2451" y2="2.2987" layer="21"/>
<rectangle x1="6.0325" y1="2.2733" x2="6.6421" y2="2.2987" layer="21"/>
<rectangle x1="9.8933" y1="2.2733" x2="9.9441" y2="2.2987" layer="21"/>
<rectangle x1="10.5029" y1="2.2733" x2="10.5791" y2="2.2987" layer="21"/>
<rectangle x1="11.0871" y1="2.2733" x2="11.1633" y2="2.2987" layer="21"/>
<rectangle x1="11.7221" y1="2.2733" x2="11.7729" y2="2.2987" layer="21"/>
<rectangle x1="12.6111" y1="2.2733" x2="12.6873" y2="2.2987" layer="21"/>
<rectangle x1="13.0429" y1="2.2733" x2="13.1191" y2="2.2987" layer="21"/>
<rectangle x1="13.4747" y1="2.2733" x2="13.5509" y2="2.2987" layer="21"/>
<rectangle x1="13.7287" y1="2.2733" x2="13.7795" y2="2.2987" layer="21"/>
<rectangle x1="14.3891" y1="2.2733" x2="14.4399" y2="2.2987" layer="21"/>
<rectangle x1="14.5923" y1="2.2733" x2="14.6685" y2="2.2987" layer="21"/>
<rectangle x1="14.7955" y1="2.2733" x2="14.8717" y2="2.2987" layer="21"/>
<rectangle x1="15.1257" y1="2.2733" x2="15.1765" y2="2.2987" layer="21"/>
<rectangle x1="15.3543" y1="2.2733" x2="15.4051" y2="2.2987" layer="21"/>
<rectangle x1="15.5829" y1="2.2733" x2="15.6337" y2="2.2987" layer="21"/>
<rectangle x1="16.2941" y1="2.2733" x2="16.3957" y2="2.2987" layer="21"/>
<rectangle x1="1.7907" y1="2.2987" x2="2.4257" y2="2.3241" layer="21"/>
<rectangle x1="3.1877" y1="2.2987" x2="3.7973" y2="2.3241" layer="21"/>
<rectangle x1="4.6355" y1="2.2987" x2="5.2451" y2="2.3241" layer="21"/>
<rectangle x1="6.0325" y1="2.2987" x2="6.6421" y2="2.3241" layer="21"/>
<rectangle x1="9.8933" y1="2.2987" x2="9.9441" y2="2.3241" layer="21"/>
<rectangle x1="10.5029" y1="2.2987" x2="10.5791" y2="2.3241" layer="21"/>
<rectangle x1="11.0871" y1="2.2987" x2="11.1633" y2="2.3241" layer="21"/>
<rectangle x1="11.7221" y1="2.2987" x2="11.7729" y2="2.3241" layer="21"/>
<rectangle x1="12.6111" y1="2.2987" x2="12.6873" y2="2.3241" layer="21"/>
<rectangle x1="13.0429" y1="2.2987" x2="13.1191" y2="2.3241" layer="21"/>
<rectangle x1="13.5001" y1="2.2987" x2="13.5509" y2="2.3241" layer="21"/>
<rectangle x1="13.7287" y1="2.2987" x2="13.8049" y2="2.3241" layer="21"/>
<rectangle x1="14.3637" y1="2.2987" x2="14.4399" y2="2.3241" layer="21"/>
<rectangle x1="14.5923" y1="2.2987" x2="14.6685" y2="2.3241" layer="21"/>
<rectangle x1="14.7701" y1="2.2987" x2="14.8463" y2="2.3241" layer="21"/>
<rectangle x1="15.1257" y1="2.2987" x2="15.1765" y2="2.3241" layer="21"/>
<rectangle x1="15.3543" y1="2.2987" x2="15.4051" y2="2.3241" layer="21"/>
<rectangle x1="15.5829" y1="2.2987" x2="15.6337" y2="2.3241" layer="21"/>
<rectangle x1="16.2941" y1="2.2987" x2="16.3703" y2="2.3241" layer="21"/>
<rectangle x1="1.7907" y1="2.3241" x2="2.4257" y2="2.3495" layer="21"/>
<rectangle x1="3.1877" y1="2.3241" x2="3.7973" y2="2.3495" layer="21"/>
<rectangle x1="4.6355" y1="2.3241" x2="5.2451" y2="2.3495" layer="21"/>
<rectangle x1="6.0325" y1="2.3241" x2="6.6421" y2="2.3495" layer="21"/>
<rectangle x1="9.8933" y1="2.3241" x2="9.9441" y2="2.3495" layer="21"/>
<rectangle x1="10.5029" y1="2.3241" x2="10.5791" y2="2.3495" layer="21"/>
<rectangle x1="11.0871" y1="2.3241" x2="11.1633" y2="2.3495" layer="21"/>
<rectangle x1="11.7221" y1="2.3241" x2="11.7983" y2="2.3495" layer="21"/>
<rectangle x1="12.6111" y1="2.3241" x2="12.6873" y2="2.3495" layer="21"/>
<rectangle x1="13.0429" y1="2.3241" x2="13.1191" y2="2.3495" layer="21"/>
<rectangle x1="13.5001" y1="2.3241" x2="13.5509" y2="2.3495" layer="21"/>
<rectangle x1="13.7287" y1="2.3241" x2="13.8049" y2="2.3495" layer="21"/>
<rectangle x1="14.3637" y1="2.3241" x2="14.4399" y2="2.3495" layer="21"/>
<rectangle x1="14.5923" y1="2.3241" x2="14.6685" y2="2.3495" layer="21"/>
<rectangle x1="14.7447" y1="2.3241" x2="14.8209" y2="2.3495" layer="21"/>
<rectangle x1="15.1257" y1="2.3241" x2="15.1765" y2="2.3495" layer="21"/>
<rectangle x1="15.3543" y1="2.3241" x2="15.4051" y2="2.3495" layer="21"/>
<rectangle x1="15.5829" y1="2.3241" x2="15.6337" y2="2.3495" layer="21"/>
<rectangle x1="16.2687" y1="2.3241" x2="16.3449" y2="2.3495" layer="21"/>
<rectangle x1="1.7907" y1="2.3495" x2="2.4257" y2="2.3749" layer="21"/>
<rectangle x1="3.1877" y1="2.3495" x2="3.7973" y2="2.3749" layer="21"/>
<rectangle x1="4.6355" y1="2.3495" x2="5.2451" y2="2.3749" layer="21"/>
<rectangle x1="6.0325" y1="2.3495" x2="6.6421" y2="2.3749" layer="21"/>
<rectangle x1="9.8933" y1="2.3495" x2="9.9441" y2="2.3749" layer="21"/>
<rectangle x1="10.5029" y1="2.3495" x2="10.5791" y2="2.3749" layer="21"/>
<rectangle x1="11.0871" y1="2.3495" x2="11.1633" y2="2.3749" layer="21"/>
<rectangle x1="11.7221" y1="2.3495" x2="11.7983" y2="2.3749" layer="21"/>
<rectangle x1="12.6111" y1="2.3495" x2="12.6873" y2="2.3749" layer="21"/>
<rectangle x1="13.0429" y1="2.3495" x2="13.1191" y2="2.3749" layer="21"/>
<rectangle x1="13.5001" y1="2.3495" x2="13.5509" y2="2.3749" layer="21"/>
<rectangle x1="13.7541" y1="2.3495" x2="13.8049" y2="2.3749" layer="21"/>
<rectangle x1="14.3637" y1="2.3495" x2="14.4145" y2="2.3749" layer="21"/>
<rectangle x1="14.5923" y1="2.3495" x2="14.6685" y2="2.3749" layer="21"/>
<rectangle x1="14.7447" y1="2.3495" x2="14.8209" y2="2.3749" layer="21"/>
<rectangle x1="15.1257" y1="2.3495" x2="15.1765" y2="2.3749" layer="21"/>
<rectangle x1="15.3543" y1="2.3495" x2="15.4051" y2="2.3749" layer="21"/>
<rectangle x1="15.5829" y1="2.3495" x2="15.6591" y2="2.3749" layer="21"/>
<rectangle x1="16.2687" y1="2.3495" x2="16.3195" y2="2.3749" layer="21"/>
<rectangle x1="1.7907" y1="2.3749" x2="2.4257" y2="2.4003" layer="21"/>
<rectangle x1="3.1877" y1="2.3749" x2="3.7973" y2="2.4003" layer="21"/>
<rectangle x1="4.6355" y1="2.3749" x2="5.2451" y2="2.4003" layer="21"/>
<rectangle x1="6.0325" y1="2.3749" x2="6.6421" y2="2.4003" layer="21"/>
<rectangle x1="9.8933" y1="2.3749" x2="9.9441" y2="2.4003" layer="21"/>
<rectangle x1="10.5029" y1="2.3749" x2="10.5791" y2="2.4003" layer="21"/>
<rectangle x1="11.0871" y1="2.3749" x2="11.1633" y2="2.4003" layer="21"/>
<rectangle x1="11.7475" y1="2.3749" x2="11.8237" y2="2.4003" layer="21"/>
<rectangle x1="12.6111" y1="2.3749" x2="12.6873" y2="2.4003" layer="21"/>
<rectangle x1="13.0429" y1="2.3749" x2="13.1191" y2="2.4003" layer="21"/>
<rectangle x1="13.5001" y1="2.3749" x2="13.5509" y2="2.4003" layer="21"/>
<rectangle x1="13.7541" y1="2.3749" x2="13.8303" y2="2.4003" layer="21"/>
<rectangle x1="14.3383" y1="2.3749" x2="14.4145" y2="2.4003" layer="21"/>
<rectangle x1="14.5923" y1="2.3749" x2="14.6685" y2="2.4003" layer="21"/>
<rectangle x1="14.7193" y1="2.3749" x2="14.7955" y2="2.4003" layer="21"/>
<rectangle x1="15.1257" y1="2.3749" x2="15.1765" y2="2.4003" layer="21"/>
<rectangle x1="15.3543" y1="2.3749" x2="15.4051" y2="2.4003" layer="21"/>
<rectangle x1="15.5829" y1="2.3749" x2="15.6591" y2="2.4003" layer="21"/>
<rectangle x1="16.2687" y1="2.3749" x2="16.3195" y2="2.4003" layer="21"/>
<rectangle x1="1.7907" y1="2.4003" x2="2.4257" y2="2.4257" layer="21"/>
<rectangle x1="3.1877" y1="2.4003" x2="3.7973" y2="2.4257" layer="21"/>
<rectangle x1="4.6355" y1="2.4003" x2="5.2451" y2="2.4257" layer="21"/>
<rectangle x1="6.0325" y1="2.4003" x2="6.6421" y2="2.4257" layer="21"/>
<rectangle x1="9.8933" y1="2.4003" x2="9.9441" y2="2.4257" layer="21"/>
<rectangle x1="10.5029" y1="2.4003" x2="10.5791" y2="2.4257" layer="21"/>
<rectangle x1="11.0871" y1="2.4003" x2="11.1633" y2="2.4257" layer="21"/>
<rectangle x1="11.7475" y1="2.4003" x2="11.8237" y2="2.4257" layer="21"/>
<rectangle x1="12.6111" y1="2.4003" x2="12.6873" y2="2.4257" layer="21"/>
<rectangle x1="13.0429" y1="2.4003" x2="13.1191" y2="2.4257" layer="21"/>
<rectangle x1="13.4747" y1="2.4003" x2="13.5509" y2="2.4257" layer="21"/>
<rectangle x1="13.7541" y1="2.4003" x2="13.8303" y2="2.4257" layer="21"/>
<rectangle x1="14.3383" y1="2.4003" x2="14.4145" y2="2.4257" layer="21"/>
<rectangle x1="14.5923" y1="2.4003" x2="14.6685" y2="2.4257" layer="21"/>
<rectangle x1="14.6939" y1="2.4003" x2="14.7701" y2="2.4257" layer="21"/>
<rectangle x1="15.1257" y1="2.4003" x2="15.1765" y2="2.4257" layer="21"/>
<rectangle x1="15.3543" y1="2.4003" x2="15.4051" y2="2.4257" layer="21"/>
<rectangle x1="15.6083" y1="2.4003" x2="15.6845" y2="2.4257" layer="21"/>
<rectangle x1="16.2687" y1="2.4003" x2="16.3195" y2="2.4257" layer="21"/>
<rectangle x1="1.7907" y1="2.4257" x2="2.4257" y2="2.4511" layer="21"/>
<rectangle x1="3.1877" y1="2.4257" x2="3.7973" y2="2.4511" layer="21"/>
<rectangle x1="4.6355" y1="2.4257" x2="5.2451" y2="2.4511" layer="21"/>
<rectangle x1="6.0325" y1="2.4257" x2="6.6421" y2="2.4511" layer="21"/>
<rectangle x1="9.8933" y1="2.4257" x2="9.9441" y2="2.4511" layer="21"/>
<rectangle x1="10.5029" y1="2.4257" x2="10.5791" y2="2.4511" layer="21"/>
<rectangle x1="11.0871" y1="2.4257" x2="11.1633" y2="2.4511" layer="21"/>
<rectangle x1="11.7729" y1="2.4257" x2="11.8491" y2="2.4511" layer="21"/>
<rectangle x1="12.6111" y1="2.4257" x2="12.6873" y2="2.4511" layer="21"/>
<rectangle x1="13.0429" y1="2.4257" x2="13.1191" y2="2.4511" layer="21"/>
<rectangle x1="13.4747" y1="2.4257" x2="13.5509" y2="2.4511" layer="21"/>
<rectangle x1="13.7795" y1="2.4257" x2="13.8557" y2="2.4511" layer="21"/>
<rectangle x1="14.3129" y1="2.4257" x2="14.3891" y2="2.4511" layer="21"/>
<rectangle x1="14.5923" y1="2.4257" x2="14.6685" y2="2.4511" layer="21"/>
<rectangle x1="14.6939" y1="2.4257" x2="14.7701" y2="2.4511" layer="21"/>
<rectangle x1="15.1257" y1="2.4257" x2="15.1765" y2="2.4511" layer="21"/>
<rectangle x1="15.3543" y1="2.4257" x2="15.4051" y2="2.4511" layer="21"/>
<rectangle x1="15.6083" y1="2.4257" x2="15.6845" y2="2.4511" layer="21"/>
<rectangle x1="16.2687" y1="2.4257" x2="16.3195" y2="2.4511" layer="21"/>
<rectangle x1="1.7907" y1="2.4511" x2="2.4257" y2="2.4765" layer="21"/>
<rectangle x1="3.1877" y1="2.4511" x2="3.7973" y2="2.4765" layer="21"/>
<rectangle x1="4.6355" y1="2.4511" x2="5.2451" y2="2.4765" layer="21"/>
<rectangle x1="6.0325" y1="2.4511" x2="6.6421" y2="2.4765" layer="21"/>
<rectangle x1="9.8933" y1="2.4511" x2="9.9441" y2="2.4765" layer="21"/>
<rectangle x1="10.5029" y1="2.4511" x2="10.5791" y2="2.4765" layer="21"/>
<rectangle x1="11.0871" y1="2.4511" x2="11.1633" y2="2.4765" layer="21"/>
<rectangle x1="11.7729" y1="2.4511" x2="11.8745" y2="2.4765" layer="21"/>
<rectangle x1="12.6111" y1="2.4511" x2="12.6873" y2="2.4765" layer="21"/>
<rectangle x1="13.0429" y1="2.4511" x2="13.1191" y2="2.4765" layer="21"/>
<rectangle x1="13.4493" y1="2.4511" x2="13.5255" y2="2.4765" layer="21"/>
<rectangle x1="13.7795" y1="2.4511" x2="13.8811" y2="2.4765" layer="21"/>
<rectangle x1="14.2875" y1="2.4511" x2="14.3891" y2="2.4765" layer="21"/>
<rectangle x1="14.5923" y1="2.4511" x2="14.7447" y2="2.4765" layer="21"/>
<rectangle x1="15.1257" y1="2.4511" x2="15.1765" y2="2.4765" layer="21"/>
<rectangle x1="15.3543" y1="2.4511" x2="15.4051" y2="2.4765" layer="21"/>
<rectangle x1="15.6337" y1="2.4511" x2="15.7099" y2="2.4765" layer="21"/>
<rectangle x1="16.2687" y1="2.4511" x2="16.3449" y2="2.4765" layer="21"/>
<rectangle x1="1.7907" y1="2.4765" x2="2.4257" y2="2.5019" layer="21"/>
<rectangle x1="3.1877" y1="2.4765" x2="3.7973" y2="2.5019" layer="21"/>
<rectangle x1="4.6355" y1="2.4765" x2="5.2451" y2="2.5019" layer="21"/>
<rectangle x1="6.0325" y1="2.4765" x2="6.6421" y2="2.5019" layer="21"/>
<rectangle x1="9.8933" y1="2.4765" x2="9.9441" y2="2.5019" layer="21"/>
<rectangle x1="10.5029" y1="2.4765" x2="10.5791" y2="2.5019" layer="21"/>
<rectangle x1="11.0871" y1="2.4765" x2="11.1633" y2="2.5019" layer="21"/>
<rectangle x1="11.7983" y1="2.4765" x2="11.8999" y2="2.5019" layer="21"/>
<rectangle x1="12.6111" y1="2.4765" x2="12.6873" y2="2.5019" layer="21"/>
<rectangle x1="13.0429" y1="2.4765" x2="13.1191" y2="2.5019" layer="21"/>
<rectangle x1="13.4239" y1="2.4765" x2="13.5255" y2="2.5019" layer="21"/>
<rectangle x1="13.8049" y1="2.4765" x2="13.9065" y2="2.5019" layer="21"/>
<rectangle x1="14.2621" y1="2.4765" x2="14.3637" y2="2.5019" layer="21"/>
<rectangle x1="14.5923" y1="2.4765" x2="14.7193" y2="2.5019" layer="21"/>
<rectangle x1="15.1257" y1="2.4765" x2="15.1765" y2="2.5019" layer="21"/>
<rectangle x1="15.3543" y1="2.4765" x2="15.4051" y2="2.5019" layer="21"/>
<rectangle x1="15.6591" y1="2.4765" x2="15.7353" y2="2.5019" layer="21"/>
<rectangle x1="16.2687" y1="2.4765" x2="16.3703" y2="2.5019" layer="21"/>
<rectangle x1="1.7907" y1="2.5019" x2="2.4257" y2="2.5273" layer="21"/>
<rectangle x1="3.1877" y1="2.5019" x2="3.7973" y2="2.5273" layer="21"/>
<rectangle x1="4.6355" y1="2.5019" x2="5.2451" y2="2.5273" layer="21"/>
<rectangle x1="6.0325" y1="2.5019" x2="6.6421" y2="2.5273" layer="21"/>
<rectangle x1="9.8933" y1="2.5019" x2="9.9441" y2="2.5273" layer="21"/>
<rectangle x1="10.5029" y1="2.5019" x2="10.5791" y2="2.5273" layer="21"/>
<rectangle x1="11.0871" y1="2.5019" x2="11.1633" y2="2.5273" layer="21"/>
<rectangle x1="11.8237" y1="2.5019" x2="11.9253" y2="2.5273" layer="21"/>
<rectangle x1="12.2555" y1="2.5019" x2="12.2809" y2="2.5273" layer="21"/>
<rectangle x1="12.6111" y1="2.5019" x2="12.6873" y2="2.5273" layer="21"/>
<rectangle x1="13.0429" y1="2.5019" x2="13.1191" y2="2.5273" layer="21"/>
<rectangle x1="13.3731" y1="2.5019" x2="13.5001" y2="2.5273" layer="21"/>
<rectangle x1="13.8303" y1="2.5019" x2="13.9319" y2="2.5273" layer="21"/>
<rectangle x1="14.2367" y1="2.5019" x2="14.3383" y2="2.5273" layer="21"/>
<rectangle x1="14.5923" y1="2.5019" x2="14.7193" y2="2.5273" layer="21"/>
<rectangle x1="15.1257" y1="2.5019" x2="15.1765" y2="2.5273" layer="21"/>
<rectangle x1="15.3543" y1="2.5019" x2="15.4051" y2="2.5273" layer="21"/>
<rectangle x1="15.6591" y1="2.5019" x2="15.7861" y2="2.5273" layer="21"/>
<rectangle x1="16.0909" y1="2.5019" x2="16.1417" y2="2.5273" layer="21"/>
<rectangle x1="16.2941" y1="2.5019" x2="16.3957" y2="2.5273" layer="21"/>
<rectangle x1="16.7005" y1="2.5019" x2="16.7259" y2="2.5273" layer="21"/>
<rectangle x1="1.7907" y1="2.5273" x2="2.4257" y2="2.5527" layer="21"/>
<rectangle x1="3.1877" y1="2.5273" x2="3.7973" y2="2.5527" layer="21"/>
<rectangle x1="4.6355" y1="2.5273" x2="5.2451" y2="2.5527" layer="21"/>
<rectangle x1="6.0325" y1="2.5273" x2="6.6421" y2="2.5527" layer="21"/>
<rectangle x1="9.8933" y1="2.5273" x2="10.3759" y2="2.5527" layer="21"/>
<rectangle x1="10.5029" y1="2.5273" x2="10.5791" y2="2.5527" layer="21"/>
<rectangle x1="11.0871" y1="2.5273" x2="11.5951" y2="2.5527" layer="21"/>
<rectangle x1="11.8491" y1="2.5273" x2="12.0015" y2="2.5527" layer="21"/>
<rectangle x1="12.1539" y1="2.5273" x2="12.2809" y2="2.5527" layer="21"/>
<rectangle x1="12.3571" y1="2.5273" x2="12.9667" y2="2.5527" layer="21"/>
<rectangle x1="13.0429" y1="2.5273" x2="13.4747" y2="2.5527" layer="21"/>
<rectangle x1="13.8557" y1="2.5273" x2="14.0081" y2="2.5527" layer="21"/>
<rectangle x1="14.1605" y1="2.5273" x2="14.3129" y2="2.5527" layer="21"/>
<rectangle x1="14.5923" y1="2.5273" x2="14.6939" y2="2.5527" layer="21"/>
<rectangle x1="15.1257" y1="2.5273" x2="15.1765" y2="2.5527" layer="21"/>
<rectangle x1="15.3543" y1="2.5273" x2="15.4051" y2="2.5527" layer="21"/>
<rectangle x1="15.7099" y1="2.5273" x2="15.8369" y2="2.5527" layer="21"/>
<rectangle x1="16.0147" y1="2.5273" x2="16.1417" y2="2.5527" layer="21"/>
<rectangle x1="16.3195" y1="2.5273" x2="16.4465" y2="2.5527" layer="21"/>
<rectangle x1="16.5989" y1="2.5273" x2="16.7259" y2="2.5527" layer="21"/>
<rectangle x1="1.7907" y1="2.5527" x2="2.4257" y2="2.5781" layer="21"/>
<rectangle x1="3.1877" y1="2.5527" x2="3.8227" y2="2.5781" layer="21"/>
<rectangle x1="4.6355" y1="2.5527" x2="5.2705" y2="2.5781" layer="21"/>
<rectangle x1="6.0325" y1="2.5527" x2="6.6675" y2="2.5781" layer="21"/>
<rectangle x1="9.8933" y1="2.5527" x2="10.3759" y2="2.5781" layer="21"/>
<rectangle x1="10.5029" y1="2.5527" x2="10.5791" y2="2.5781" layer="21"/>
<rectangle x1="11.0871" y1="2.5527" x2="11.5951" y2="2.5781" layer="21"/>
<rectangle x1="11.8999" y1="2.5527" x2="12.2809" y2="2.5781" layer="21"/>
<rectangle x1="12.3317" y1="2.5527" x2="12.9667" y2="2.5781" layer="21"/>
<rectangle x1="13.0429" y1="2.5527" x2="13.4239" y2="2.5781" layer="21"/>
<rectangle x1="13.8811" y1="2.5527" x2="14.2875" y2="2.5781" layer="21"/>
<rectangle x1="14.5923" y1="2.5527" x2="14.6939" y2="2.5781" layer="21"/>
<rectangle x1="15.1257" y1="2.5527" x2="15.1765" y2="2.5781" layer="21"/>
<rectangle x1="15.3543" y1="2.5527" x2="15.4051" y2="2.5781" layer="21"/>
<rectangle x1="15.7353" y1="2.5527" x2="16.1417" y2="2.5781" layer="21"/>
<rectangle x1="16.3449" y1="2.5527" x2="16.7259" y2="2.5781" layer="21"/>
<rectangle x1="0.6223" y1="2.5781" x2="7.8613" y2="2.6035" layer="21"/>
<rectangle x1="9.8933" y1="2.5781" x2="10.3759" y2="2.6035" layer="21"/>
<rectangle x1="10.5029" y1="2.5781" x2="10.5537" y2="2.6035" layer="21"/>
<rectangle x1="11.1125" y1="2.5781" x2="11.5951" y2="2.6035" layer="21"/>
<rectangle x1="11.9507" y1="2.5781" x2="12.2047" y2="2.6035" layer="21"/>
<rectangle x1="12.3571" y1="2.5781" x2="12.9667" y2="2.6035" layer="21"/>
<rectangle x1="13.0429" y1="2.5781" x2="13.3223" y2="2.6035" layer="21"/>
<rectangle x1="13.9573" y1="2.5781" x2="14.2113" y2="2.6035" layer="21"/>
<rectangle x1="14.6177" y1="2.5781" x2="14.6685" y2="2.6035" layer="21"/>
<rectangle x1="15.1257" y1="2.5781" x2="15.1765" y2="2.6035" layer="21"/>
<rectangle x1="15.3543" y1="2.5781" x2="15.4051" y2="2.6035" layer="21"/>
<rectangle x1="15.8115" y1="2.5781" x2="16.0655" y2="2.6035" layer="21"/>
<rectangle x1="16.3957" y1="2.5781" x2="16.6497" y2="2.6035" layer="21"/>
<rectangle x1="0.6223" y1="2.6035" x2="7.8613" y2="2.6289" layer="21"/>
<rectangle x1="0.6223" y1="2.6289" x2="7.8613" y2="2.6543" layer="21"/>
<rectangle x1="0.6223" y1="2.6543" x2="7.8613" y2="2.6797" layer="21"/>
<rectangle x1="0.6223" y1="2.6797" x2="7.8613" y2="2.7051" layer="21"/>
<rectangle x1="0.6223" y1="2.7051" x2="7.8613" y2="2.7305" layer="21"/>
<rectangle x1="0.6223" y1="2.7305" x2="7.8613" y2="2.7559" layer="21"/>
<rectangle x1="0.6223" y1="2.7559" x2="7.8613" y2="2.7813" layer="21"/>
<rectangle x1="0.6223" y1="2.7813" x2="7.8613" y2="2.8067" layer="21"/>
<rectangle x1="0.6223" y1="2.8067" x2="7.8613" y2="2.8321" layer="21"/>
<rectangle x1="0.6223" y1="2.8321" x2="7.8613" y2="2.8575" layer="21"/>
<rectangle x1="0.6223" y1="2.8575" x2="7.8613" y2="2.8829" layer="21"/>
<rectangle x1="0.6223" y1="2.8829" x2="7.8613" y2="2.9083" layer="21"/>
<rectangle x1="0.6223" y1="2.9083" x2="7.8613" y2="2.9337" layer="21"/>
<rectangle x1="0.6223" y1="2.9337" x2="3.0861" y2="2.9591" layer="21"/>
<rectangle x1="3.4671" y1="2.9337" x2="4.9657" y2="2.9591" layer="21"/>
<rectangle x1="5.2959" y1="2.9337" x2="7.8613" y2="2.9591" layer="21"/>
<rectangle x1="16.7767" y1="2.9337" x2="16.9291" y2="2.9591" layer="21"/>
<rectangle x1="0.6223" y1="2.9591" x2="1.4859" y2="2.9845" layer="21"/>
<rectangle x1="1.9177" y1="2.9591" x2="2.9591" y2="2.9845" layer="21"/>
<rectangle x1="3.6449" y1="2.9591" x2="4.8641" y2="2.9845" layer="21"/>
<rectangle x1="5.4229" y1="2.9591" x2="7.8613" y2="2.9845" layer="21"/>
<rectangle x1="11.6713" y1="2.9591" x2="12.3063" y2="2.9845" layer="21"/>
<rectangle x1="15.5321" y1="2.9591" x2="16.1163" y2="2.9845" layer="21"/>
<rectangle x1="16.7259" y1="2.9591" x2="16.9799" y2="2.9845" layer="21"/>
<rectangle x1="0.6223" y1="2.9845" x2="1.4097" y2="3.0099" layer="21"/>
<rectangle x1="1.9939" y1="2.9845" x2="2.8575" y2="3.0099" layer="21"/>
<rectangle x1="3.7719" y1="2.9845" x2="4.7879" y2="3.0099" layer="21"/>
<rectangle x1="5.4991" y1="2.9845" x2="5.8801" y2="3.0099" layer="21"/>
<rectangle x1="6.4643" y1="2.9845" x2="6.9215" y2="3.0099" layer="21"/>
<rectangle x1="7.5311" y1="2.9845" x2="7.8613" y2="3.0099" layer="21"/>
<rectangle x1="8.1661" y1="2.9845" x2="8.7249" y2="3.0099" layer="21"/>
<rectangle x1="9.2075" y1="2.9845" x2="9.7663" y2="3.0099" layer="21"/>
<rectangle x1="10.2489" y1="2.9845" x2="10.8077" y2="3.0099" layer="21"/>
<rectangle x1="11.5443" y1="2.9845" x2="12.4333" y2="3.0099" layer="21"/>
<rectangle x1="13.0683" y1="2.9845" x2="13.6271" y2="3.0099" layer="21"/>
<rectangle x1="14.2113" y1="2.9845" x2="14.8463" y2="3.0099" layer="21"/>
<rectangle x1="15.4305" y1="2.9845" x2="16.2433" y2="3.0099" layer="21"/>
<rectangle x1="16.7005" y1="2.9845" x2="16.8021" y2="3.0099" layer="21"/>
<rectangle x1="16.9037" y1="2.9845" x2="17.0053" y2="3.0099" layer="21"/>
<rectangle x1="0.6223" y1="3.0099" x2="1.3589" y2="3.0353" layer="21"/>
<rectangle x1="2.0447" y1="3.0099" x2="2.8067" y2="3.0353" layer="21"/>
<rectangle x1="3.8735" y1="3.0099" x2="4.7371" y2="3.0353" layer="21"/>
<rectangle x1="5.5753" y1="3.0099" x2="5.8801" y2="3.0353" layer="21"/>
<rectangle x1="6.4643" y1="3.0099" x2="6.9215" y2="3.0353" layer="21"/>
<rectangle x1="7.5311" y1="3.0099" x2="7.8613" y2="3.0353" layer="21"/>
<rectangle x1="8.1407" y1="3.0099" x2="8.7249" y2="3.0353" layer="21"/>
<rectangle x1="9.2075" y1="3.0099" x2="9.7663" y2="3.0353" layer="21"/>
<rectangle x1="10.2489" y1="3.0099" x2="10.8077" y2="3.0353" layer="21"/>
<rectangle x1="11.4681" y1="3.0099" x2="12.5349" y2="3.0353" layer="21"/>
<rectangle x1="13.0683" y1="3.0099" x2="13.6271" y2="3.0353" layer="21"/>
<rectangle x1="14.2113" y1="3.0099" x2="14.8463" y2="3.0353" layer="21"/>
<rectangle x1="15.3543" y1="3.0099" x2="16.3449" y2="3.0353" layer="21"/>
<rectangle x1="16.6751" y1="3.0099" x2="16.7513" y2="3.0353" layer="21"/>
<rectangle x1="16.9291" y1="3.0099" x2="17.0307" y2="3.0353" layer="21"/>
<rectangle x1="0.6223" y1="3.0353" x2="1.3335" y2="3.0607" layer="21"/>
<rectangle x1="2.0447" y1="3.0353" x2="2.7559" y2="3.0607" layer="21"/>
<rectangle x1="3.9497" y1="3.0353" x2="4.6863" y2="3.0607" layer="21"/>
<rectangle x1="5.6007" y1="3.0353" x2="5.8801" y2="3.0607" layer="21"/>
<rectangle x1="6.4643" y1="3.0353" x2="6.9215" y2="3.0607" layer="21"/>
<rectangle x1="7.5311" y1="3.0353" x2="7.8613" y2="3.0607" layer="21"/>
<rectangle x1="8.1407" y1="3.0353" x2="8.7249" y2="3.0607" layer="21"/>
<rectangle x1="9.2075" y1="3.0353" x2="9.7663" y2="3.0607" layer="21"/>
<rectangle x1="10.2489" y1="3.0353" x2="10.8077" y2="3.0607" layer="21"/>
<rectangle x1="11.4173" y1="3.0353" x2="12.6365" y2="3.0607" layer="21"/>
<rectangle x1="13.0683" y1="3.0353" x2="13.6271" y2="3.0607" layer="21"/>
<rectangle x1="14.1859" y1="3.0353" x2="14.8209" y2="3.0607" layer="21"/>
<rectangle x1="15.3035" y1="3.0353" x2="16.4465" y2="3.0607" layer="21"/>
<rectangle x1="16.6751" y1="3.0353" x2="16.7259" y2="3.0607" layer="21"/>
<rectangle x1="16.7767" y1="3.0353" x2="16.8275" y2="3.0607" layer="21"/>
<rectangle x1="16.8783" y1="3.0353" x2="16.9545" y2="3.0607" layer="21"/>
<rectangle x1="16.9799" y1="3.0353" x2="17.0307" y2="3.0607" layer="21"/>
<rectangle x1="0.6223" y1="3.0607" x2="1.2827" y2="3.0861" layer="21"/>
<rectangle x1="2.0447" y1="3.0607" x2="2.7051" y2="3.0861" layer="21"/>
<rectangle x1="4.0005" y1="3.0607" x2="4.6355" y2="3.0861" layer="21"/>
<rectangle x1="5.6515" y1="3.0607" x2="5.8801" y2="3.0861" layer="21"/>
<rectangle x1="6.4643" y1="3.0607" x2="6.9215" y2="3.0861" layer="21"/>
<rectangle x1="7.5311" y1="3.0607" x2="7.8613" y2="3.0861" layer="21"/>
<rectangle x1="8.1407" y1="3.0607" x2="8.7249" y2="3.0861" layer="21"/>
<rectangle x1="9.2075" y1="3.0607" x2="9.7663" y2="3.0861" layer="21"/>
<rectangle x1="10.2489" y1="3.0607" x2="10.8077" y2="3.0861" layer="21"/>
<rectangle x1="11.3665" y1="3.0607" x2="12.6873" y2="3.0861" layer="21"/>
<rectangle x1="13.0683" y1="3.0607" x2="13.6271" y2="3.0861" layer="21"/>
<rectangle x1="14.1605" y1="3.0607" x2="14.8209" y2="3.0861" layer="21"/>
<rectangle x1="15.2527" y1="3.0607" x2="16.4973" y2="3.0861" layer="21"/>
<rectangle x1="16.6497" y1="3.0607" x2="16.7259" y2="3.0861" layer="21"/>
<rectangle x1="16.7767" y1="3.0607" x2="16.8275" y2="3.0861" layer="21"/>
<rectangle x1="16.8783" y1="3.0607" x2="16.9291" y2="3.0861" layer="21"/>
<rectangle x1="16.9799" y1="3.0607" x2="17.0561" y2="3.0861" layer="21"/>
<rectangle x1="0.6223" y1="3.0861" x2="1.2573" y2="3.1115" layer="21"/>
<rectangle x1="2.0447" y1="3.0861" x2="2.6543" y2="3.1115" layer="21"/>
<rectangle x1="4.0005" y1="3.0861" x2="4.6101" y2="3.1115" layer="21"/>
<rectangle x1="5.6261" y1="3.0861" x2="5.8801" y2="3.1115" layer="21"/>
<rectangle x1="6.4643" y1="3.0861" x2="6.9215" y2="3.1115" layer="21"/>
<rectangle x1="7.5311" y1="3.0861" x2="7.8613" y2="3.1115" layer="21"/>
<rectangle x1="8.1407" y1="3.0861" x2="8.7249" y2="3.1115" layer="21"/>
<rectangle x1="9.2075" y1="3.0861" x2="9.7663" y2="3.1115" layer="21"/>
<rectangle x1="10.2489" y1="3.0861" x2="10.8077" y2="3.1115" layer="21"/>
<rectangle x1="11.3157" y1="3.0861" x2="12.7127" y2="3.1115" layer="21"/>
<rectangle x1="13.0683" y1="3.0861" x2="13.6271" y2="3.1115" layer="21"/>
<rectangle x1="14.1605" y1="3.0861" x2="14.7955" y2="3.1115" layer="21"/>
<rectangle x1="15.2019" y1="3.0861" x2="16.5227" y2="3.1115" layer="21"/>
<rectangle x1="16.6497" y1="3.0861" x2="16.7259" y2="3.1115" layer="21"/>
<rectangle x1="16.7767" y1="3.0861" x2="16.9291" y2="3.1115" layer="21"/>
<rectangle x1="16.9799" y1="3.0861" x2="17.0561" y2="3.1115" layer="21"/>
<rectangle x1="0.6223" y1="3.1115" x2="1.2319" y2="3.1369" layer="21"/>
<rectangle x1="2.0447" y1="3.1115" x2="2.6289" y2="3.1369" layer="21"/>
<rectangle x1="4.0005" y1="3.1115" x2="4.5593" y2="3.1369" layer="21"/>
<rectangle x1="5.6261" y1="3.1115" x2="5.8801" y2="3.1369" layer="21"/>
<rectangle x1="6.4643" y1="3.1115" x2="6.9215" y2="3.1369" layer="21"/>
<rectangle x1="7.5311" y1="3.1115" x2="7.8613" y2="3.1369" layer="21"/>
<rectangle x1="8.1407" y1="3.1115" x2="8.7249" y2="3.1369" layer="21"/>
<rectangle x1="9.2075" y1="3.1115" x2="9.7663" y2="3.1369" layer="21"/>
<rectangle x1="10.2489" y1="3.1115" x2="10.8077" y2="3.1369" layer="21"/>
<rectangle x1="11.2903" y1="3.1115" x2="12.7127" y2="3.1369" layer="21"/>
<rectangle x1="13.0683" y1="3.1115" x2="13.6271" y2="3.1369" layer="21"/>
<rectangle x1="14.1351" y1="3.1115" x2="14.7701" y2="3.1369" layer="21"/>
<rectangle x1="15.1765" y1="3.1115" x2="16.5227" y2="3.1369" layer="21"/>
<rectangle x1="16.6497" y1="3.1115" x2="16.7259" y2="3.1369" layer="21"/>
<rectangle x1="16.7767" y1="3.1115" x2="16.9037" y2="3.1369" layer="21"/>
<rectangle x1="17.0053" y1="3.1115" x2="17.0561" y2="3.1369" layer="21"/>
<rectangle x1="0.6223" y1="3.1369" x2="1.2319" y2="3.1623" layer="21"/>
<rectangle x1="2.0447" y1="3.1369" x2="2.6035" y2="3.1623" layer="21"/>
<rectangle x1="3.9751" y1="3.1369" x2="4.5339" y2="3.1623" layer="21"/>
<rectangle x1="5.6261" y1="3.1369" x2="5.8801" y2="3.1623" layer="21"/>
<rectangle x1="6.4643" y1="3.1369" x2="6.9215" y2="3.1623" layer="21"/>
<rectangle x1="7.5311" y1="3.1369" x2="7.8613" y2="3.1623" layer="21"/>
<rectangle x1="8.1407" y1="3.1369" x2="8.7249" y2="3.1623" layer="21"/>
<rectangle x1="9.2075" y1="3.1369" x2="9.7663" y2="3.1623" layer="21"/>
<rectangle x1="10.2489" y1="3.1369" x2="10.8077" y2="3.1623" layer="21"/>
<rectangle x1="11.2649" y1="3.1369" x2="12.7127" y2="3.1623" layer="21"/>
<rectangle x1="13.0683" y1="3.1369" x2="13.6271" y2="3.1623" layer="21"/>
<rectangle x1="14.1097" y1="3.1369" x2="14.7701" y2="3.1623" layer="21"/>
<rectangle x1="15.1511" y1="3.1369" x2="16.4973" y2="3.1623" layer="21"/>
<rectangle x1="16.6497" y1="3.1369" x2="16.7259" y2="3.1623" layer="21"/>
<rectangle x1="16.7767" y1="3.1369" x2="16.9291" y2="3.1623" layer="21"/>
<rectangle x1="16.9799" y1="3.1369" x2="17.0561" y2="3.1623" layer="21"/>
<rectangle x1="0.6223" y1="3.1623" x2="1.2065" y2="3.1877" layer="21"/>
<rectangle x1="2.0447" y1="3.1623" x2="2.5527" y2="3.1877" layer="21"/>
<rectangle x1="3.9751" y1="3.1623" x2="4.5085" y2="3.1877" layer="21"/>
<rectangle x1="5.6007" y1="3.1623" x2="5.8801" y2="3.1877" layer="21"/>
<rectangle x1="6.4643" y1="3.1623" x2="6.9215" y2="3.1877" layer="21"/>
<rectangle x1="7.5311" y1="3.1623" x2="7.8613" y2="3.1877" layer="21"/>
<rectangle x1="8.1407" y1="3.1623" x2="8.7249" y2="3.1877" layer="21"/>
<rectangle x1="9.2075" y1="3.1623" x2="9.7663" y2="3.1877" layer="21"/>
<rectangle x1="10.2489" y1="3.1623" x2="10.8077" y2="3.1877" layer="21"/>
<rectangle x1="11.2395" y1="3.1623" x2="12.7127" y2="3.1877" layer="21"/>
<rectangle x1="13.0683" y1="3.1623" x2="13.6271" y2="3.1877" layer="21"/>
<rectangle x1="14.1097" y1="3.1623" x2="14.7447" y2="3.1877" layer="21"/>
<rectangle x1="15.1003" y1="3.1623" x2="16.4973" y2="3.1877" layer="21"/>
<rectangle x1="16.6497" y1="3.1623" x2="16.7259" y2="3.1877" layer="21"/>
<rectangle x1="16.7767" y1="3.1623" x2="16.8275" y2="3.1877" layer="21"/>
<rectangle x1="16.8783" y1="3.1623" x2="16.9291" y2="3.1877" layer="21"/>
<rectangle x1="16.9799" y1="3.1623" x2="17.0561" y2="3.1877" layer="21"/>
<rectangle x1="0.6223" y1="3.1877" x2="1.2065" y2="3.2131" layer="21"/>
<rectangle x1="2.0447" y1="3.1877" x2="2.5273" y2="3.2131" layer="21"/>
<rectangle x1="3.9751" y1="3.1877" x2="4.4831" y2="3.2131" layer="21"/>
<rectangle x1="5.6007" y1="3.1877" x2="5.8801" y2="3.2131" layer="21"/>
<rectangle x1="6.4643" y1="3.1877" x2="6.9215" y2="3.2131" layer="21"/>
<rectangle x1="7.5311" y1="3.1877" x2="7.8613" y2="3.2131" layer="21"/>
<rectangle x1="8.1407" y1="3.1877" x2="8.7249" y2="3.2131" layer="21"/>
<rectangle x1="9.2075" y1="3.1877" x2="9.7663" y2="3.2131" layer="21"/>
<rectangle x1="10.2489" y1="3.1877" x2="10.8077" y2="3.2131" layer="21"/>
<rectangle x1="11.2141" y1="3.1877" x2="12.7127" y2="3.2131" layer="21"/>
<rectangle x1="13.0683" y1="3.1877" x2="13.6271" y2="3.2131" layer="21"/>
<rectangle x1="14.0843" y1="3.1877" x2="14.7193" y2="3.2131" layer="21"/>
<rectangle x1="15.0749" y1="3.1877" x2="16.4719" y2="3.2131" layer="21"/>
<rectangle x1="16.6751" y1="3.1877" x2="16.7259" y2="3.2131" layer="21"/>
<rectangle x1="16.7767" y1="3.1877" x2="16.9291" y2="3.2131" layer="21"/>
<rectangle x1="16.9799" y1="3.1877" x2="17.0307" y2="3.2131" layer="21"/>
<rectangle x1="0.6223" y1="3.2131" x2="1.1811" y2="3.2385" layer="21"/>
<rectangle x1="2.0447" y1="3.2131" x2="2.5019" y2="3.2385" layer="21"/>
<rectangle x1="3.9497" y1="3.2131" x2="4.4577" y2="3.2385" layer="21"/>
<rectangle x1="5.5753" y1="3.2131" x2="5.8801" y2="3.2385" layer="21"/>
<rectangle x1="6.4643" y1="3.2131" x2="6.9215" y2="3.2385" layer="21"/>
<rectangle x1="7.5311" y1="3.2131" x2="7.8613" y2="3.2385" layer="21"/>
<rectangle x1="8.1407" y1="3.2131" x2="8.7249" y2="3.2385" layer="21"/>
<rectangle x1="9.2075" y1="3.2131" x2="9.7663" y2="3.2385" layer="21"/>
<rectangle x1="10.2489" y1="3.2131" x2="10.8077" y2="3.2385" layer="21"/>
<rectangle x1="11.1887" y1="3.2131" x2="12.7127" y2="3.2385" layer="21"/>
<rectangle x1="13.0683" y1="3.2131" x2="13.6271" y2="3.2385" layer="21"/>
<rectangle x1="14.0843" y1="3.2131" x2="14.7193" y2="3.2385" layer="21"/>
<rectangle x1="15.0495" y1="3.2131" x2="16.4719" y2="3.2385" layer="21"/>
<rectangle x1="16.6751" y1="3.2131" x2="16.7513" y2="3.2385" layer="21"/>
<rectangle x1="16.7767" y1="3.2131" x2="16.9037" y2="3.2385" layer="21"/>
<rectangle x1="16.9545" y1="3.2131" x2="17.0307" y2="3.2385" layer="21"/>
<rectangle x1="0.6223" y1="3.2385" x2="1.1811" y2="3.2639" layer="21"/>
<rectangle x1="2.0447" y1="3.2385" x2="2.5019" y2="3.2639" layer="21"/>
<rectangle x1="3.9497" y1="3.2385" x2="4.4577" y2="3.2639" layer="21"/>
<rectangle x1="5.5753" y1="3.2385" x2="5.8801" y2="3.2639" layer="21"/>
<rectangle x1="6.4643" y1="3.2385" x2="6.9215" y2="3.2639" layer="21"/>
<rectangle x1="7.5311" y1="3.2385" x2="7.8613" y2="3.2639" layer="21"/>
<rectangle x1="8.1407" y1="3.2385" x2="8.7249" y2="3.2639" layer="21"/>
<rectangle x1="9.2075" y1="3.2385" x2="9.7663" y2="3.2639" layer="21"/>
<rectangle x1="10.2489" y1="3.2385" x2="10.8077" y2="3.2639" layer="21"/>
<rectangle x1="11.1633" y1="3.2385" x2="12.7127" y2="3.2639" layer="21"/>
<rectangle x1="13.0683" y1="3.2385" x2="13.6271" y2="3.2639" layer="21"/>
<rectangle x1="14.0589" y1="3.2385" x2="14.6939" y2="3.2639" layer="21"/>
<rectangle x1="15.0241" y1="3.2385" x2="16.4719" y2="3.2639" layer="21"/>
<rectangle x1="16.7005" y1="3.2385" x2="16.8021" y2="3.2639" layer="21"/>
<rectangle x1="16.9291" y1="3.2385" x2="17.0053" y2="3.2639" layer="21"/>
<rectangle x1="0.6223" y1="3.2639" x2="1.1557" y2="3.2893" layer="21"/>
<rectangle x1="2.0447" y1="3.2639" x2="2.4765" y2="3.2893" layer="21"/>
<rectangle x1="3.9243" y1="3.2639" x2="4.4323" y2="3.2893" layer="21"/>
<rectangle x1="5.5753" y1="3.2639" x2="5.8801" y2="3.2893" layer="21"/>
<rectangle x1="6.4643" y1="3.2639" x2="6.9215" y2="3.2893" layer="21"/>
<rectangle x1="7.5311" y1="3.2639" x2="7.8613" y2="3.2893" layer="21"/>
<rectangle x1="8.1407" y1="3.2639" x2="8.7249" y2="3.2893" layer="21"/>
<rectangle x1="9.2075" y1="3.2639" x2="9.7663" y2="3.2893" layer="21"/>
<rectangle x1="10.2489" y1="3.2639" x2="10.8077" y2="3.2893" layer="21"/>
<rectangle x1="11.1633" y1="3.2639" x2="12.7127" y2="3.2893" layer="21"/>
<rectangle x1="13.0683" y1="3.2639" x2="13.6271" y2="3.2893" layer="21"/>
<rectangle x1="14.0335" y1="3.2639" x2="14.6939" y2="3.2893" layer="21"/>
<rectangle x1="15.0241" y1="3.2639" x2="16.4465" y2="3.2893" layer="21"/>
<rectangle x1="16.7259" y1="3.2639" x2="16.9799" y2="3.2893" layer="21"/>
<rectangle x1="0.6223" y1="3.2893" x2="1.1557" y2="3.3147" layer="21"/>
<rectangle x1="2.0447" y1="3.2893" x2="2.4511" y2="3.3147" layer="21"/>
<rectangle x1="3.9243" y1="3.2893" x2="4.4069" y2="3.3147" layer="21"/>
<rectangle x1="5.5499" y1="3.2893" x2="5.8801" y2="3.3147" layer="21"/>
<rectangle x1="6.4643" y1="3.2893" x2="6.9215" y2="3.3147" layer="21"/>
<rectangle x1="7.5311" y1="3.2893" x2="7.8613" y2="3.3147" layer="21"/>
<rectangle x1="8.1407" y1="3.2893" x2="8.7249" y2="3.3147" layer="21"/>
<rectangle x1="9.2075" y1="3.2893" x2="9.7663" y2="3.3147" layer="21"/>
<rectangle x1="10.2489" y1="3.2893" x2="10.8077" y2="3.3147" layer="21"/>
<rectangle x1="11.1379" y1="3.2893" x2="12.7127" y2="3.3147" layer="21"/>
<rectangle x1="13.0683" y1="3.2893" x2="13.6271" y2="3.3147" layer="21"/>
<rectangle x1="14.0335" y1="3.2893" x2="14.6685" y2="3.3147" layer="21"/>
<rectangle x1="14.9987" y1="3.2893" x2="16.4465" y2="3.3147" layer="21"/>
<rectangle x1="16.7513" y1="3.2893" x2="16.9545" y2="3.3147" layer="21"/>
<rectangle x1="0.6223" y1="3.3147" x2="1.1557" y2="3.3401" layer="21"/>
<rectangle x1="2.0447" y1="3.3147" x2="2.4257" y2="3.3401" layer="21"/>
<rectangle x1="3.9243" y1="3.3147" x2="4.3815" y2="3.3401" layer="21"/>
<rectangle x1="5.5499" y1="3.3147" x2="5.8801" y2="3.3401" layer="21"/>
<rectangle x1="6.4643" y1="3.3147" x2="6.9215" y2="3.3401" layer="21"/>
<rectangle x1="7.5311" y1="3.3147" x2="7.8613" y2="3.3401" layer="21"/>
<rectangle x1="8.1407" y1="3.3147" x2="8.7249" y2="3.3401" layer="21"/>
<rectangle x1="9.2075" y1="3.3147" x2="9.7663" y2="3.3401" layer="21"/>
<rectangle x1="10.2489" y1="3.3147" x2="10.8077" y2="3.3401" layer="21"/>
<rectangle x1="11.1379" y1="3.3147" x2="12.7127" y2="3.3401" layer="21"/>
<rectangle x1="13.0683" y1="3.3147" x2="13.6271" y2="3.3401" layer="21"/>
<rectangle x1="14.0081" y1="3.3147" x2="14.6431" y2="3.3401" layer="21"/>
<rectangle x1="14.9733" y1="3.3147" x2="16.4211" y2="3.3401" layer="21"/>
<rectangle x1="0.6223" y1="3.3401" x2="1.1557" y2="3.3655" layer="21"/>
<rectangle x1="2.0447" y1="3.3401" x2="2.4257" y2="3.3655" layer="21"/>
<rectangle x1="3.8989" y1="3.3401" x2="4.3815" y2="3.3655" layer="21"/>
<rectangle x1="5.5245" y1="3.3401" x2="5.8801" y2="3.3655" layer="21"/>
<rectangle x1="6.4643" y1="3.3401" x2="6.9215" y2="3.3655" layer="21"/>
<rectangle x1="7.5311" y1="3.3401" x2="7.8613" y2="3.3655" layer="21"/>
<rectangle x1="8.1407" y1="3.3401" x2="8.7249" y2="3.3655" layer="21"/>
<rectangle x1="9.2075" y1="3.3401" x2="9.7663" y2="3.3655" layer="21"/>
<rectangle x1="10.2489" y1="3.3401" x2="10.8077" y2="3.3655" layer="21"/>
<rectangle x1="11.1125" y1="3.3401" x2="11.8745" y2="3.3655" layer="21"/>
<rectangle x1="12.0523" y1="3.3401" x2="12.7127" y2="3.3655" layer="21"/>
<rectangle x1="13.0683" y1="3.3401" x2="13.6271" y2="3.3655" layer="21"/>
<rectangle x1="14.0081" y1="3.3401" x2="14.6431" y2="3.3655" layer="21"/>
<rectangle x1="14.9733" y1="3.3401" x2="16.4211" y2="3.3655" layer="21"/>
<rectangle x1="0.6223" y1="3.3655" x2="1.1303" y2="3.3909" layer="21"/>
<rectangle x1="2.0447" y1="3.3655" x2="2.4003" y2="3.3909" layer="21"/>
<rectangle x1="3.8989" y1="3.3655" x2="4.3561" y2="3.3909" layer="21"/>
<rectangle x1="5.5245" y1="3.3655" x2="5.8801" y2="3.3909" layer="21"/>
<rectangle x1="6.4643" y1="3.3655" x2="6.9215" y2="3.3909" layer="21"/>
<rectangle x1="7.5311" y1="3.3655" x2="7.8613" y2="3.3909" layer="21"/>
<rectangle x1="8.1407" y1="3.3655" x2="8.7249" y2="3.3909" layer="21"/>
<rectangle x1="9.2075" y1="3.3655" x2="9.7663" y2="3.3909" layer="21"/>
<rectangle x1="10.2489" y1="3.3655" x2="10.8077" y2="3.3909" layer="21"/>
<rectangle x1="11.1125" y1="3.3655" x2="11.7729" y2="3.3909" layer="21"/>
<rectangle x1="12.1539" y1="3.3655" x2="12.7127" y2="3.3909" layer="21"/>
<rectangle x1="13.0683" y1="3.3655" x2="13.6271" y2="3.3909" layer="21"/>
<rectangle x1="13.9827" y1="3.3655" x2="14.6177" y2="3.3909" layer="21"/>
<rectangle x1="14.9479" y1="3.3655" x2="16.4211" y2="3.3909" layer="21"/>
<rectangle x1="0.6223" y1="3.3909" x2="1.1303" y2="3.4163" layer="21"/>
<rectangle x1="2.0447" y1="3.3909" x2="2.4003" y2="3.4163" layer="21"/>
<rectangle x1="3.8989" y1="3.3909" x2="4.3561" y2="3.4163" layer="21"/>
<rectangle x1="5.4991" y1="3.3909" x2="5.8801" y2="3.4163" layer="21"/>
<rectangle x1="6.4643" y1="3.3909" x2="6.9215" y2="3.4163" layer="21"/>
<rectangle x1="7.5311" y1="3.3909" x2="7.8613" y2="3.4163" layer="21"/>
<rectangle x1="8.1407" y1="3.3909" x2="8.7249" y2="3.4163" layer="21"/>
<rectangle x1="9.2075" y1="3.3909" x2="9.7663" y2="3.4163" layer="21"/>
<rectangle x1="10.2489" y1="3.3909" x2="10.8077" y2="3.4163" layer="21"/>
<rectangle x1="11.1125" y1="3.3909" x2="11.7221" y2="3.4163" layer="21"/>
<rectangle x1="12.1793" y1="3.3909" x2="12.7127" y2="3.4163" layer="21"/>
<rectangle x1="13.0683" y1="3.3909" x2="13.6271" y2="3.4163" layer="21"/>
<rectangle x1="13.9573" y1="3.3909" x2="14.5923" y2="3.4163" layer="21"/>
<rectangle x1="14.9225" y1="3.3909" x2="15.7353" y2="3.4163" layer="21"/>
<rectangle x1="15.9639" y1="3.3909" x2="16.3957" y2="3.4163" layer="21"/>
<rectangle x1="0.6223" y1="3.4163" x2="1.1303" y2="3.4417" layer="21"/>
<rectangle x1="2.0447" y1="3.4163" x2="2.3749" y2="3.4417" layer="21"/>
<rectangle x1="3.1369" y1="3.4163" x2="3.5179" y2="3.4417" layer="21"/>
<rectangle x1="3.8735" y1="3.4163" x2="4.3307" y2="3.4417" layer="21"/>
<rectangle x1="5.4991" y1="3.4163" x2="5.8801" y2="3.4417" layer="21"/>
<rectangle x1="6.4643" y1="3.4163" x2="6.9215" y2="3.4417" layer="21"/>
<rectangle x1="7.5311" y1="3.4163" x2="7.8613" y2="3.4417" layer="21"/>
<rectangle x1="8.1407" y1="3.4163" x2="8.7249" y2="3.4417" layer="21"/>
<rectangle x1="9.2075" y1="3.4163" x2="9.7663" y2="3.4417" layer="21"/>
<rectangle x1="10.2489" y1="3.4163" x2="10.8077" y2="3.4417" layer="21"/>
<rectangle x1="11.1125" y1="3.4163" x2="11.6967" y2="3.4417" layer="21"/>
<rectangle x1="12.1793" y1="3.4163" x2="12.7127" y2="3.4417" layer="21"/>
<rectangle x1="13.0683" y1="3.4163" x2="13.6271" y2="3.4417" layer="21"/>
<rectangle x1="13.9573" y1="3.4163" x2="14.5923" y2="3.4417" layer="21"/>
<rectangle x1="14.9225" y1="3.4163" x2="15.6337" y2="3.4417" layer="21"/>
<rectangle x1="16.1163" y1="3.4163" x2="16.3957" y2="3.4417" layer="21"/>
<rectangle x1="0.6223" y1="3.4417" x2="1.1303" y2="3.4671" layer="21"/>
<rectangle x1="2.0447" y1="3.4417" x2="2.3749" y2="3.4671" layer="21"/>
<rectangle x1="3.0607" y1="3.4417" x2="3.6449" y2="3.4671" layer="21"/>
<rectangle x1="3.8735" y1="3.4417" x2="4.3307" y2="3.4671" layer="21"/>
<rectangle x1="5.1181" y1="3.4417" x2="5.3213" y2="3.4671" layer="21"/>
<rectangle x1="5.4991" y1="3.4417" x2="5.8801" y2="3.4671" layer="21"/>
<rectangle x1="6.4643" y1="3.4417" x2="6.9215" y2="3.4671" layer="21"/>
<rectangle x1="7.5311" y1="3.4417" x2="7.8613" y2="3.4671" layer="21"/>
<rectangle x1="8.1407" y1="3.4417" x2="8.7249" y2="3.4671" layer="21"/>
<rectangle x1="9.2075" y1="3.4417" x2="9.7663" y2="3.4671" layer="21"/>
<rectangle x1="10.2489" y1="3.4417" x2="10.8077" y2="3.4671" layer="21"/>
<rectangle x1="11.1125" y1="3.4417" x2="11.6713" y2="3.4671" layer="21"/>
<rectangle x1="12.1793" y1="3.4417" x2="12.7127" y2="3.4671" layer="21"/>
<rectangle x1="13.0683" y1="3.4417" x2="13.6271" y2="3.4671" layer="21"/>
<rectangle x1="13.9319" y1="3.4417" x2="14.5669" y2="3.4671" layer="21"/>
<rectangle x1="14.8971" y1="3.4417" x2="15.5575" y2="3.4671" layer="21"/>
<rectangle x1="16.2179" y1="3.4417" x2="16.3957" y2="3.4671" layer="21"/>
<rectangle x1="0.6223" y1="3.4671" x2="1.1303" y2="3.4925" layer="21"/>
<rectangle x1="1.8415" y1="3.4671" x2="1.9177" y2="3.4925" layer="21"/>
<rectangle x1="2.0447" y1="3.4671" x2="2.3495" y2="3.4925" layer="21"/>
<rectangle x1="3.0099" y1="3.4671" x2="3.7211" y2="3.4925" layer="21"/>
<rectangle x1="3.8481" y1="3.4671" x2="4.3307" y2="3.4925" layer="21"/>
<rectangle x1="5.0673" y1="3.4671" x2="5.3975" y2="3.4925" layer="21"/>
<rectangle x1="5.4737" y1="3.4671" x2="5.8801" y2="3.4925" layer="21"/>
<rectangle x1="6.4643" y1="3.4671" x2="6.9215" y2="3.4925" layer="21"/>
<rectangle x1="7.5311" y1="3.4671" x2="7.8613" y2="3.4925" layer="21"/>
<rectangle x1="8.1407" y1="3.4671" x2="8.7249" y2="3.4925" layer="21"/>
<rectangle x1="9.2075" y1="3.4671" x2="9.7663" y2="3.4925" layer="21"/>
<rectangle x1="10.2489" y1="3.4671" x2="10.8077" y2="3.4925" layer="21"/>
<rectangle x1="11.0871" y1="3.4671" x2="11.6459" y2="3.4925" layer="21"/>
<rectangle x1="12.1793" y1="3.4671" x2="12.7127" y2="3.4925" layer="21"/>
<rectangle x1="13.0683" y1="3.4671" x2="13.6271" y2="3.4925" layer="21"/>
<rectangle x1="13.9065" y1="3.4671" x2="14.5669" y2="3.4925" layer="21"/>
<rectangle x1="14.8971" y1="3.4671" x2="15.5321" y2="3.4925" layer="21"/>
<rectangle x1="16.2941" y1="3.4671" x2="16.3703" y2="3.4925" layer="21"/>
<rectangle x1="0.6223" y1="3.4925" x2="1.1303" y2="3.5179" layer="21"/>
<rectangle x1="1.7653" y1="3.4925" x2="2.3495" y2="3.5179" layer="21"/>
<rectangle x1="2.9845" y1="3.4925" x2="3.7973" y2="3.5179" layer="21"/>
<rectangle x1="3.8481" y1="3.4925" x2="4.3053" y2="3.5179" layer="21"/>
<rectangle x1="5.0419" y1="3.4925" x2="5.4483" y2="3.5179" layer="21"/>
<rectangle x1="5.4737" y1="3.4925" x2="5.8801" y2="3.5179" layer="21"/>
<rectangle x1="6.4643" y1="3.4925" x2="6.9215" y2="3.5179" layer="21"/>
<rectangle x1="7.5311" y1="3.4925" x2="7.8613" y2="3.5179" layer="21"/>
<rectangle x1="8.1407" y1="3.4925" x2="8.7249" y2="3.5179" layer="21"/>
<rectangle x1="9.2075" y1="3.4925" x2="9.7663" y2="3.5179" layer="21"/>
<rectangle x1="10.2489" y1="3.4925" x2="10.8077" y2="3.5179" layer="21"/>
<rectangle x1="11.0871" y1="3.4925" x2="11.6459" y2="3.5179" layer="21"/>
<rectangle x1="12.1793" y1="3.4925" x2="12.7127" y2="3.5179" layer="21"/>
<rectangle x1="13.0683" y1="3.4925" x2="13.6271" y2="3.5179" layer="21"/>
<rectangle x1="13.9065" y1="3.4925" x2="14.5415" y2="3.5179" layer="21"/>
<rectangle x1="14.8971" y1="3.4925" x2="15.5067" y2="3.5179" layer="21"/>
<rectangle x1="16.3449" y1="3.4925" x2="16.3703" y2="3.5179" layer="21"/>
<rectangle x1="0.6223" y1="3.5179" x2="1.1303" y2="3.5433" layer="21"/>
<rectangle x1="1.7399" y1="3.5179" x2="2.3241" y2="3.5433" layer="21"/>
<rectangle x1="2.9591" y1="3.5179" x2="4.3053" y2="3.5433" layer="21"/>
<rectangle x1="4.9911" y1="3.5179" x2="5.8801" y2="3.5433" layer="21"/>
<rectangle x1="6.4643" y1="3.5179" x2="6.9215" y2="3.5433" layer="21"/>
<rectangle x1="7.5311" y1="3.5179" x2="7.8613" y2="3.5433" layer="21"/>
<rectangle x1="8.1407" y1="3.5179" x2="8.7249" y2="3.5433" layer="21"/>
<rectangle x1="9.2075" y1="3.5179" x2="9.7663" y2="3.5433" layer="21"/>
<rectangle x1="10.2489" y1="3.5179" x2="10.8077" y2="3.5433" layer="21"/>
<rectangle x1="11.0871" y1="3.5179" x2="11.6205" y2="3.5433" layer="21"/>
<rectangle x1="12.1793" y1="3.5179" x2="12.7127" y2="3.5433" layer="21"/>
<rectangle x1="13.0683" y1="3.5179" x2="13.6271" y2="3.5433" layer="21"/>
<rectangle x1="13.8811" y1="3.5179" x2="14.5161" y2="3.5433" layer="21"/>
<rectangle x1="14.8717" y1="3.5179" x2="15.4813" y2="3.5433" layer="21"/>
<rectangle x1="0.6223" y1="3.5433" x2="1.1303" y2="3.5687" layer="21"/>
<rectangle x1="1.7145" y1="3.5433" x2="2.3241" y2="3.5687" layer="21"/>
<rectangle x1="2.9337" y1="3.5433" x2="4.3053" y2="3.5687" layer="21"/>
<rectangle x1="4.9911" y1="3.5433" x2="5.8801" y2="3.5687" layer="21"/>
<rectangle x1="6.4643" y1="3.5433" x2="6.9215" y2="3.5687" layer="21"/>
<rectangle x1="7.5311" y1="3.5433" x2="7.8613" y2="3.5687" layer="21"/>
<rectangle x1="8.1407" y1="3.5433" x2="8.7249" y2="3.5687" layer="21"/>
<rectangle x1="9.2075" y1="3.5433" x2="9.7663" y2="3.5687" layer="21"/>
<rectangle x1="10.2489" y1="3.5433" x2="10.8077" y2="3.5687" layer="21"/>
<rectangle x1="11.0871" y1="3.5433" x2="11.6205" y2="3.5687" layer="21"/>
<rectangle x1="12.1793" y1="3.5433" x2="12.7127" y2="3.5687" layer="21"/>
<rectangle x1="13.0683" y1="3.5433" x2="13.6271" y2="3.5687" layer="21"/>
<rectangle x1="13.8811" y1="3.5433" x2="14.5161" y2="3.5687" layer="21"/>
<rectangle x1="14.8717" y1="3.5433" x2="15.4559" y2="3.5687" layer="21"/>
<rectangle x1="0.6223" y1="3.5687" x2="1.1303" y2="3.5941" layer="21"/>
<rectangle x1="1.7145" y1="3.5687" x2="2.3241" y2="3.5941" layer="21"/>
<rectangle x1="2.9083" y1="3.5687" x2="4.2799" y2="3.5941" layer="21"/>
<rectangle x1="4.9657" y1="3.5687" x2="5.8801" y2="3.5941" layer="21"/>
<rectangle x1="6.4643" y1="3.5687" x2="6.9215" y2="3.5941" layer="21"/>
<rectangle x1="7.5311" y1="3.5687" x2="7.8613" y2="3.5941" layer="21"/>
<rectangle x1="8.1407" y1="3.5687" x2="8.7249" y2="3.5941" layer="21"/>
<rectangle x1="9.2075" y1="3.5687" x2="9.7663" y2="3.5941" layer="21"/>
<rectangle x1="10.2489" y1="3.5687" x2="10.8077" y2="3.5941" layer="21"/>
<rectangle x1="11.0871" y1="3.5687" x2="11.6205" y2="3.5941" layer="21"/>
<rectangle x1="12.1793" y1="3.5687" x2="12.7127" y2="3.5941" layer="21"/>
<rectangle x1="13.0683" y1="3.5687" x2="13.6271" y2="3.5941" layer="21"/>
<rectangle x1="13.8557" y1="3.5687" x2="14.4907" y2="3.5941" layer="21"/>
<rectangle x1="14.8717" y1="3.5687" x2="15.4305" y2="3.5941" layer="21"/>
<rectangle x1="0.6223" y1="3.5941" x2="1.1303" y2="3.6195" layer="21"/>
<rectangle x1="1.7145" y1="3.5941" x2="2.2987" y2="3.6195" layer="21"/>
<rectangle x1="2.8829" y1="3.5941" x2="4.2799" y2="3.6195" layer="21"/>
<rectangle x1="4.9403" y1="3.5941" x2="5.8801" y2="3.6195" layer="21"/>
<rectangle x1="6.4643" y1="3.5941" x2="6.9215" y2="3.6195" layer="21"/>
<rectangle x1="7.5311" y1="3.5941" x2="7.8613" y2="3.6195" layer="21"/>
<rectangle x1="8.1407" y1="3.5941" x2="8.7249" y2="3.6195" layer="21"/>
<rectangle x1="9.2075" y1="3.5941" x2="9.7663" y2="3.6195" layer="21"/>
<rectangle x1="10.2489" y1="3.5941" x2="10.8077" y2="3.6195" layer="21"/>
<rectangle x1="11.0871" y1="3.5941" x2="11.6205" y2="3.6195" layer="21"/>
<rectangle x1="12.1793" y1="3.5941" x2="12.7127" y2="3.6195" layer="21"/>
<rectangle x1="13.0683" y1="3.5941" x2="13.6271" y2="3.6195" layer="21"/>
<rectangle x1="13.8303" y1="3.5941" x2="14.4653" y2="3.6195" layer="21"/>
<rectangle x1="14.8463" y1="3.5941" x2="15.4051" y2="3.6195" layer="21"/>
<rectangle x1="0.6223" y1="3.6195" x2="1.1303" y2="3.6449" layer="21"/>
<rectangle x1="1.7145" y1="3.6195" x2="2.2987" y2="3.6449" layer="21"/>
<rectangle x1="2.8829" y1="3.6195" x2="4.2799" y2="3.6449" layer="21"/>
<rectangle x1="4.9403" y1="3.6195" x2="5.8801" y2="3.6449" layer="21"/>
<rectangle x1="6.4643" y1="3.6195" x2="6.9215" y2="3.6449" layer="21"/>
<rectangle x1="7.5311" y1="3.6195" x2="7.8613" y2="3.6449" layer="21"/>
<rectangle x1="8.1407" y1="3.6195" x2="8.7249" y2="3.6449" layer="21"/>
<rectangle x1="9.2075" y1="3.6195" x2="9.7663" y2="3.6449" layer="21"/>
<rectangle x1="10.2489" y1="3.6195" x2="10.8077" y2="3.6449" layer="21"/>
<rectangle x1="11.0871" y1="3.6195" x2="11.6205" y2="3.6449" layer="21"/>
<rectangle x1="12.1793" y1="3.6195" x2="12.7127" y2="3.6449" layer="21"/>
<rectangle x1="13.0683" y1="3.6195" x2="13.6271" y2="3.6449" layer="21"/>
<rectangle x1="13.8303" y1="3.6195" x2="14.4653" y2="3.6449" layer="21"/>
<rectangle x1="14.8463" y1="3.6195" x2="15.4051" y2="3.6449" layer="21"/>
<rectangle x1="0.6223" y1="3.6449" x2="1.1303" y2="3.6703" layer="21"/>
<rectangle x1="1.7145" y1="3.6449" x2="2.2987" y2="3.6703" layer="21"/>
<rectangle x1="2.8829" y1="3.6449" x2="4.2799" y2="3.6703" layer="21"/>
<rectangle x1="4.9149" y1="3.6449" x2="5.8801" y2="3.6703" layer="21"/>
<rectangle x1="6.4643" y1="3.6449" x2="6.9215" y2="3.6703" layer="21"/>
<rectangle x1="7.5311" y1="3.6449" x2="7.8613" y2="3.6703" layer="21"/>
<rectangle x1="8.1407" y1="3.6449" x2="8.7249" y2="3.6703" layer="21"/>
<rectangle x1="9.2075" y1="3.6449" x2="9.7663" y2="3.6703" layer="21"/>
<rectangle x1="10.2489" y1="3.6449" x2="10.8077" y2="3.6703" layer="21"/>
<rectangle x1="11.0871" y1="3.6449" x2="11.6459" y2="3.6703" layer="21"/>
<rectangle x1="12.1793" y1="3.6449" x2="12.7127" y2="3.6703" layer="21"/>
<rectangle x1="13.0683" y1="3.6449" x2="13.6271" y2="3.6703" layer="21"/>
<rectangle x1="13.8049" y1="3.6449" x2="14.4399" y2="3.6703" layer="21"/>
<rectangle x1="14.8463" y1="3.6449" x2="15.3797" y2="3.6703" layer="21"/>
<rectangle x1="0.6223" y1="3.6703" x2="1.1303" y2="3.6957" layer="21"/>
<rectangle x1="1.7145" y1="3.6703" x2="2.2987" y2="3.6957" layer="21"/>
<rectangle x1="2.8575" y1="3.6703" x2="4.2545" y2="3.6957" layer="21"/>
<rectangle x1="4.9149" y1="3.6703" x2="5.8801" y2="3.6957" layer="21"/>
<rectangle x1="6.4643" y1="3.6703" x2="6.9215" y2="3.6957" layer="21"/>
<rectangle x1="7.5311" y1="3.6703" x2="7.8613" y2="3.6957" layer="21"/>
<rectangle x1="8.1407" y1="3.6703" x2="8.7249" y2="3.6957" layer="21"/>
<rectangle x1="9.2075" y1="3.6703" x2="9.7663" y2="3.6957" layer="21"/>
<rectangle x1="10.2489" y1="3.6703" x2="10.8077" y2="3.6957" layer="21"/>
<rectangle x1="11.1125" y1="3.6703" x2="11.6459" y2="3.6957" layer="21"/>
<rectangle x1="12.1793" y1="3.6703" x2="12.7127" y2="3.6957" layer="21"/>
<rectangle x1="13.0683" y1="3.6703" x2="13.6271" y2="3.6957" layer="21"/>
<rectangle x1="13.8049" y1="3.6703" x2="14.4145" y2="3.6957" layer="21"/>
<rectangle x1="14.8463" y1="3.6703" x2="15.3797" y2="3.6957" layer="21"/>
<rectangle x1="0.6223" y1="3.6957" x2="1.1303" y2="3.7211" layer="21"/>
<rectangle x1="1.7145" y1="3.6957" x2="2.2987" y2="3.7211" layer="21"/>
<rectangle x1="3.0099" y1="3.6957" x2="4.2545" y2="3.7211" layer="21"/>
<rectangle x1="4.8895" y1="3.6957" x2="5.8801" y2="3.7211" layer="21"/>
<rectangle x1="6.4643" y1="3.6957" x2="6.9215" y2="3.7211" layer="21"/>
<rectangle x1="7.5311" y1="3.6957" x2="7.8613" y2="3.7211" layer="21"/>
<rectangle x1="8.1407" y1="3.6957" x2="8.7249" y2="3.7211" layer="21"/>
<rectangle x1="9.2075" y1="3.6957" x2="9.7663" y2="3.7211" layer="21"/>
<rectangle x1="10.2489" y1="3.6957" x2="10.8077" y2="3.7211" layer="21"/>
<rectangle x1="11.1125" y1="3.6957" x2="11.6713" y2="3.7211" layer="21"/>
<rectangle x1="12.1793" y1="3.6957" x2="12.7127" y2="3.7211" layer="21"/>
<rectangle x1="13.0683" y1="3.6957" x2="13.6271" y2="3.7211" layer="21"/>
<rectangle x1="13.7795" y1="3.6957" x2="14.4145" y2="3.7211" layer="21"/>
<rectangle x1="14.8209" y1="3.6957" x2="15.4305" y2="3.7211" layer="21"/>
<rectangle x1="0.6223" y1="3.7211" x2="1.1303" y2="3.7465" layer="21"/>
<rectangle x1="1.7145" y1="3.7211" x2="2.2733" y2="3.7465" layer="21"/>
<rectangle x1="3.1877" y1="3.7211" x2="4.2545" y2="3.7465" layer="21"/>
<rectangle x1="4.8895" y1="3.7211" x2="5.8801" y2="3.7465" layer="21"/>
<rectangle x1="6.4643" y1="3.7211" x2="6.9215" y2="3.7465" layer="21"/>
<rectangle x1="7.5311" y1="3.7211" x2="7.8613" y2="3.7465" layer="21"/>
<rectangle x1="8.1407" y1="3.7211" x2="8.7249" y2="3.7465" layer="21"/>
<rectangle x1="9.2075" y1="3.7211" x2="9.7663" y2="3.7465" layer="21"/>
<rectangle x1="10.2489" y1="3.7211" x2="10.8077" y2="3.7465" layer="21"/>
<rectangle x1="11.1125" y1="3.7211" x2="11.6967" y2="3.7465" layer="21"/>
<rectangle x1="12.1793" y1="3.7211" x2="12.7127" y2="3.7465" layer="21"/>
<rectangle x1="13.0683" y1="3.7211" x2="13.6271" y2="3.7465" layer="21"/>
<rectangle x1="13.7541" y1="3.7211" x2="14.3891" y2="3.7465" layer="21"/>
<rectangle x1="14.8209" y1="3.7211" x2="15.6337" y2="3.7465" layer="21"/>
<rectangle x1="0.6223" y1="3.7465" x2="1.1303" y2="3.7719" layer="21"/>
<rectangle x1="1.7145" y1="3.7465" x2="2.2733" y2="3.7719" layer="21"/>
<rectangle x1="3.3909" y1="3.7465" x2="4.2545" y2="3.7719" layer="21"/>
<rectangle x1="4.8895" y1="3.7465" x2="5.8801" y2="3.7719" layer="21"/>
<rectangle x1="6.4643" y1="3.7465" x2="6.9215" y2="3.7719" layer="21"/>
<rectangle x1="7.5311" y1="3.7465" x2="7.8613" y2="3.7719" layer="21"/>
<rectangle x1="8.1407" y1="3.7465" x2="8.7249" y2="3.7719" layer="21"/>
<rectangle x1="9.2075" y1="3.7465" x2="9.7663" y2="3.7719" layer="21"/>
<rectangle x1="10.2489" y1="3.7465" x2="10.8077" y2="3.7719" layer="21"/>
<rectangle x1="11.1125" y1="3.7465" x2="11.7221" y2="3.7719" layer="21"/>
<rectangle x1="12.1793" y1="3.7465" x2="12.7127" y2="3.7719" layer="21"/>
<rectangle x1="13.0683" y1="3.7465" x2="13.6271" y2="3.7719" layer="21"/>
<rectangle x1="13.7541" y1="3.7465" x2="14.3891" y2="3.7719" layer="21"/>
<rectangle x1="14.8209" y1="3.7465" x2="15.8115" y2="3.7719" layer="21"/>
<rectangle x1="0.6223" y1="3.7719" x2="1.1303" y2="3.7973" layer="21"/>
<rectangle x1="1.7145" y1="3.7719" x2="2.2733" y2="3.7973" layer="21"/>
<rectangle x1="3.5687" y1="3.7719" x2="4.2545" y2="3.7973" layer="21"/>
<rectangle x1="4.8895" y1="3.7719" x2="5.8801" y2="3.7973" layer="21"/>
<rectangle x1="6.4643" y1="3.7719" x2="6.9215" y2="3.7973" layer="21"/>
<rectangle x1="7.5311" y1="3.7719" x2="7.8613" y2="3.7973" layer="21"/>
<rectangle x1="8.1407" y1="3.7719" x2="8.7249" y2="3.7973" layer="21"/>
<rectangle x1="9.2075" y1="3.7719" x2="9.7663" y2="3.7973" layer="21"/>
<rectangle x1="10.2489" y1="3.7719" x2="10.8077" y2="3.7973" layer="21"/>
<rectangle x1="11.1379" y1="3.7719" x2="11.7475" y2="3.7973" layer="21"/>
<rectangle x1="12.1793" y1="3.7719" x2="12.7127" y2="3.7973" layer="21"/>
<rectangle x1="13.0683" y1="3.7719" x2="13.6271" y2="3.7973" layer="21"/>
<rectangle x1="13.7287" y1="3.7719" x2="14.3637" y2="3.7973" layer="21"/>
<rectangle x1="14.8209" y1="3.7719" x2="16.0147" y2="3.7973" layer="21"/>
<rectangle x1="0.6223" y1="3.7973" x2="1.1303" y2="3.8227" layer="21"/>
<rectangle x1="1.7145" y1="3.7973" x2="2.2733" y2="3.8227" layer="21"/>
<rectangle x1="3.7719" y1="3.7973" x2="4.2545" y2="3.8227" layer="21"/>
<rectangle x1="4.8641" y1="3.7973" x2="5.8801" y2="3.8227" layer="21"/>
<rectangle x1="6.4643" y1="3.7973" x2="6.9215" y2="3.8227" layer="21"/>
<rectangle x1="7.5311" y1="3.7973" x2="7.8613" y2="3.8227" layer="21"/>
<rectangle x1="8.1407" y1="3.7973" x2="8.7249" y2="3.8227" layer="21"/>
<rectangle x1="9.2075" y1="3.7973" x2="9.7663" y2="3.8227" layer="21"/>
<rectangle x1="10.2489" y1="3.7973" x2="10.8077" y2="3.8227" layer="21"/>
<rectangle x1="11.1379" y1="3.7973" x2="11.8237" y2="3.8227" layer="21"/>
<rectangle x1="12.1793" y1="3.7973" x2="12.7127" y2="3.8227" layer="21"/>
<rectangle x1="13.0683" y1="3.7973" x2="13.6271" y2="3.8227" layer="21"/>
<rectangle x1="13.7033" y1="3.7973" x2="14.3383" y2="3.8227" layer="21"/>
<rectangle x1="14.8209" y1="3.7973" x2="16.2179" y2="3.8227" layer="21"/>
<rectangle x1="0.6223" y1="3.8227" x2="1.1303" y2="3.8481" layer="21"/>
<rectangle x1="1.7145" y1="3.8227" x2="2.2733" y2="3.8481" layer="21"/>
<rectangle x1="3.9751" y1="3.8227" x2="4.2545" y2="3.8481" layer="21"/>
<rectangle x1="4.8641" y1="3.8227" x2="5.8801" y2="3.8481" layer="21"/>
<rectangle x1="6.4643" y1="3.8227" x2="6.9215" y2="3.8481" layer="21"/>
<rectangle x1="7.5311" y1="3.8227" x2="7.8613" y2="3.8481" layer="21"/>
<rectangle x1="8.1407" y1="3.8227" x2="8.7249" y2="3.8481" layer="21"/>
<rectangle x1="9.2075" y1="3.8227" x2="9.7663" y2="3.8481" layer="21"/>
<rectangle x1="10.2489" y1="3.8227" x2="10.8077" y2="3.8481" layer="21"/>
<rectangle x1="11.1633" y1="3.8227" x2="11.9507" y2="3.8481" layer="21"/>
<rectangle x1="12.1793" y1="3.8227" x2="12.7127" y2="3.8481" layer="21"/>
<rectangle x1="13.0683" y1="3.8227" x2="13.6271" y2="3.8481" layer="21"/>
<rectangle x1="13.7033" y1="3.8227" x2="14.3383" y2="3.8481" layer="21"/>
<rectangle x1="14.8209" y1="3.8227" x2="16.3957" y2="3.8481" layer="21"/>
<rectangle x1="0.6223" y1="3.8481" x2="1.1303" y2="3.8735" layer="21"/>
<rectangle x1="1.7145" y1="3.8481" x2="2.2733" y2="3.8735" layer="21"/>
<rectangle x1="4.0259" y1="3.8481" x2="4.2545" y2="3.8735" layer="21"/>
<rectangle x1="4.8641" y1="3.8481" x2="5.8801" y2="3.8735" layer="21"/>
<rectangle x1="6.4643" y1="3.8481" x2="6.9215" y2="3.8735" layer="21"/>
<rectangle x1="7.5311" y1="3.8481" x2="7.8613" y2="3.8735" layer="21"/>
<rectangle x1="8.1407" y1="3.8481" x2="8.7249" y2="3.8735" layer="21"/>
<rectangle x1="9.2075" y1="3.8481" x2="9.7663" y2="3.8735" layer="21"/>
<rectangle x1="10.2489" y1="3.8481" x2="10.8077" y2="3.8735" layer="21"/>
<rectangle x1="11.1633" y1="3.8481" x2="12.7127" y2="3.8735" layer="21"/>
<rectangle x1="13.0683" y1="3.8481" x2="13.6271" y2="3.8735" layer="21"/>
<rectangle x1="13.6779" y1="3.8481" x2="14.3129" y2="3.8735" layer="21"/>
<rectangle x1="14.8209" y1="3.8481" x2="16.5481" y2="3.8735" layer="21"/>
<rectangle x1="0.6223" y1="3.8735" x2="1.1303" y2="3.8989" layer="21"/>
<rectangle x1="1.7145" y1="3.8735" x2="2.2733" y2="3.8989" layer="21"/>
<rectangle x1="4.0259" y1="3.8735" x2="4.2545" y2="3.8989" layer="21"/>
<rectangle x1="4.8641" y1="3.8735" x2="5.8801" y2="3.8989" layer="21"/>
<rectangle x1="6.4643" y1="3.8735" x2="6.9215" y2="3.8989" layer="21"/>
<rectangle x1="7.5311" y1="3.8735" x2="7.8613" y2="3.8989" layer="21"/>
<rectangle x1="8.1407" y1="3.8735" x2="8.7249" y2="3.8989" layer="21"/>
<rectangle x1="9.2075" y1="3.8735" x2="9.7663" y2="3.8989" layer="21"/>
<rectangle x1="10.2489" y1="3.8735" x2="10.8077" y2="3.8989" layer="21"/>
<rectangle x1="11.1887" y1="3.8735" x2="12.7127" y2="3.8989" layer="21"/>
<rectangle x1="13.0683" y1="3.8735" x2="13.6271" y2="3.8989" layer="21"/>
<rectangle x1="13.6779" y1="3.8735" x2="14.2875" y2="3.8989" layer="21"/>
<rectangle x1="14.8209" y1="3.8735" x2="16.5481" y2="3.8989" layer="21"/>
<rectangle x1="0.6223" y1="3.8989" x2="1.1303" y2="3.9243" layer="21"/>
<rectangle x1="1.7145" y1="3.8989" x2="2.2733" y2="3.9243" layer="21"/>
<rectangle x1="4.0259" y1="3.8989" x2="4.2545" y2="3.9243" layer="21"/>
<rectangle x1="4.8641" y1="3.8989" x2="5.8801" y2="3.9243" layer="21"/>
<rectangle x1="6.4643" y1="3.8989" x2="6.9215" y2="3.9243" layer="21"/>
<rectangle x1="7.5311" y1="3.8989" x2="7.8613" y2="3.9243" layer="21"/>
<rectangle x1="8.1407" y1="3.8989" x2="8.7249" y2="3.9243" layer="21"/>
<rectangle x1="9.2075" y1="3.8989" x2="9.7663" y2="3.9243" layer="21"/>
<rectangle x1="10.2489" y1="3.8989" x2="10.8077" y2="3.9243" layer="21"/>
<rectangle x1="11.2141" y1="3.8989" x2="12.7127" y2="3.9243" layer="21"/>
<rectangle x1="13.0683" y1="3.8989" x2="14.2875" y2="3.9243" layer="21"/>
<rectangle x1="14.8209" y1="3.8989" x2="16.5481" y2="3.9243" layer="21"/>
<rectangle x1="0.6223" y1="3.9243" x2="1.1303" y2="3.9497" layer="21"/>
<rectangle x1="1.7145" y1="3.9243" x2="2.2733" y2="3.9497" layer="21"/>
<rectangle x1="4.0259" y1="3.9243" x2="4.2545" y2="3.9497" layer="21"/>
<rectangle x1="4.8641" y1="3.9243" x2="5.8801" y2="3.9497" layer="21"/>
<rectangle x1="6.4643" y1="3.9243" x2="6.9215" y2="3.9497" layer="21"/>
<rectangle x1="7.5311" y1="3.9243" x2="7.8613" y2="3.9497" layer="21"/>
<rectangle x1="8.1407" y1="3.9243" x2="8.7249" y2="3.9497" layer="21"/>
<rectangle x1="9.2075" y1="3.9243" x2="9.7663" y2="3.9497" layer="21"/>
<rectangle x1="10.2489" y1="3.9243" x2="10.8077" y2="3.9497" layer="21"/>
<rectangle x1="11.2141" y1="3.9243" x2="12.7127" y2="3.9497" layer="21"/>
<rectangle x1="13.0683" y1="3.9243" x2="14.2621" y2="3.9497" layer="21"/>
<rectangle x1="14.8209" y1="3.9243" x2="16.5481" y2="3.9497" layer="21"/>
<rectangle x1="0.6223" y1="3.9497" x2="1.1303" y2="3.9751" layer="21"/>
<rectangle x1="1.7145" y1="3.9497" x2="2.2733" y2="3.9751" layer="21"/>
<rectangle x1="4.0259" y1="3.9497" x2="4.2545" y2="3.9751" layer="21"/>
<rectangle x1="4.8641" y1="3.9497" x2="5.8801" y2="3.9751" layer="21"/>
<rectangle x1="6.4643" y1="3.9497" x2="6.9215" y2="3.9751" layer="21"/>
<rectangle x1="7.5311" y1="3.9497" x2="7.8613" y2="3.9751" layer="21"/>
<rectangle x1="8.1407" y1="3.9497" x2="8.7249" y2="3.9751" layer="21"/>
<rectangle x1="9.2075" y1="3.9497" x2="9.7663" y2="3.9751" layer="21"/>
<rectangle x1="10.2489" y1="3.9497" x2="10.8077" y2="3.9751" layer="21"/>
<rectangle x1="11.2649" y1="3.9497" x2="12.7127" y2="3.9751" layer="21"/>
<rectangle x1="13.0683" y1="3.9497" x2="14.2621" y2="3.9751" layer="21"/>
<rectangle x1="14.8209" y1="3.9497" x2="16.5481" y2="3.9751" layer="21"/>
<rectangle x1="0.6223" y1="3.9751" x2="1.1303" y2="4.0005" layer="21"/>
<rectangle x1="1.7145" y1="3.9751" x2="2.2733" y2="4.0005" layer="21"/>
<rectangle x1="4.0259" y1="3.9751" x2="4.2545" y2="4.0005" layer="21"/>
<rectangle x1="4.8641" y1="3.9751" x2="5.8801" y2="4.0005" layer="21"/>
<rectangle x1="6.4643" y1="3.9751" x2="6.9215" y2="4.0005" layer="21"/>
<rectangle x1="7.5311" y1="3.9751" x2="7.8613" y2="4.0005" layer="21"/>
<rectangle x1="8.1407" y1="3.9751" x2="8.7249" y2="4.0005" layer="21"/>
<rectangle x1="9.2075" y1="3.9751" x2="9.7663" y2="4.0005" layer="21"/>
<rectangle x1="10.2489" y1="3.9751" x2="10.8077" y2="4.0005" layer="21"/>
<rectangle x1="11.2903" y1="3.9751" x2="12.7127" y2="4.0005" layer="21"/>
<rectangle x1="13.0683" y1="3.9751" x2="14.2367" y2="4.0005" layer="21"/>
<rectangle x1="14.8209" y1="3.9751" x2="16.5481" y2="4.0005" layer="21"/>
<rectangle x1="0.6223" y1="4.0005" x2="1.1303" y2="4.0259" layer="21"/>
<rectangle x1="1.7145" y1="4.0005" x2="2.2733" y2="4.0259" layer="21"/>
<rectangle x1="4.0259" y1="4.0005" x2="4.2545" y2="4.0259" layer="21"/>
<rectangle x1="4.8641" y1="4.0005" x2="5.8801" y2="4.0259" layer="21"/>
<rectangle x1="6.4643" y1="4.0005" x2="6.9215" y2="4.0259" layer="21"/>
<rectangle x1="7.5311" y1="4.0005" x2="7.8613" y2="4.0259" layer="21"/>
<rectangle x1="8.1407" y1="4.0005" x2="8.7249" y2="4.0259" layer="21"/>
<rectangle x1="9.2075" y1="4.0005" x2="9.7663" y2="4.0259" layer="21"/>
<rectangle x1="10.2489" y1="4.0005" x2="10.8077" y2="4.0259" layer="21"/>
<rectangle x1="11.3157" y1="4.0005" x2="12.7127" y2="4.0259" layer="21"/>
<rectangle x1="13.0683" y1="4.0005" x2="14.2367" y2="4.0259" layer="21"/>
<rectangle x1="14.8209" y1="4.0005" x2="16.5481" y2="4.0259" layer="21"/>
<rectangle x1="0.6223" y1="4.0259" x2="1.1303" y2="4.0513" layer="21"/>
<rectangle x1="1.7145" y1="4.0259" x2="2.2733" y2="4.0513" layer="21"/>
<rectangle x1="4.0259" y1="4.0259" x2="4.2545" y2="4.0513" layer="21"/>
<rectangle x1="4.8641" y1="4.0259" x2="5.8801" y2="4.0513" layer="21"/>
<rectangle x1="6.4643" y1="4.0259" x2="6.9215" y2="4.0513" layer="21"/>
<rectangle x1="7.5311" y1="4.0259" x2="7.8613" y2="4.0513" layer="21"/>
<rectangle x1="8.1407" y1="4.0259" x2="8.7249" y2="4.0513" layer="21"/>
<rectangle x1="9.2075" y1="4.0259" x2="9.7663" y2="4.0513" layer="21"/>
<rectangle x1="10.2489" y1="4.0259" x2="10.8077" y2="4.0513" layer="21"/>
<rectangle x1="11.3665" y1="4.0259" x2="12.7127" y2="4.0513" layer="21"/>
<rectangle x1="13.0683" y1="4.0259" x2="14.2621" y2="4.0513" layer="21"/>
<rectangle x1="14.8209" y1="4.0259" x2="16.5481" y2="4.0513" layer="21"/>
<rectangle x1="0.6223" y1="4.0513" x2="1.1303" y2="4.0767" layer="21"/>
<rectangle x1="1.7145" y1="4.0513" x2="2.2733" y2="4.0767" layer="21"/>
<rectangle x1="4.0259" y1="4.0513" x2="4.2545" y2="4.0767" layer="21"/>
<rectangle x1="4.8641" y1="4.0513" x2="5.8801" y2="4.0767" layer="21"/>
<rectangle x1="6.4643" y1="4.0513" x2="6.9215" y2="4.0767" layer="21"/>
<rectangle x1="7.5311" y1="4.0513" x2="7.8613" y2="4.0767" layer="21"/>
<rectangle x1="8.1407" y1="4.0513" x2="8.7249" y2="4.0767" layer="21"/>
<rectangle x1="9.2075" y1="4.0513" x2="9.7663" y2="4.0767" layer="21"/>
<rectangle x1="10.2489" y1="4.0513" x2="10.8077" y2="4.0767" layer="21"/>
<rectangle x1="11.4173" y1="4.0513" x2="12.7127" y2="4.0767" layer="21"/>
<rectangle x1="13.0683" y1="4.0513" x2="14.2875" y2="4.0767" layer="21"/>
<rectangle x1="14.8209" y1="4.0513" x2="15.3543" y2="4.0767" layer="21"/>
<rectangle x1="15.3797" y1="4.0513" x2="16.5481" y2="4.0767" layer="21"/>
<rectangle x1="0.6223" y1="4.0767" x2="1.1303" y2="4.1021" layer="21"/>
<rectangle x1="1.7145" y1="4.0767" x2="2.2733" y2="4.1021" layer="21"/>
<rectangle x1="2.8321" y1="4.0767" x2="2.9591" y2="4.1021" layer="21"/>
<rectangle x1="4.0259" y1="4.0767" x2="4.2545" y2="4.1021" layer="21"/>
<rectangle x1="4.8895" y1="4.0767" x2="5.8801" y2="4.1021" layer="21"/>
<rectangle x1="6.4643" y1="4.0767" x2="6.9215" y2="4.1021" layer="21"/>
<rectangle x1="7.5311" y1="4.0767" x2="7.8613" y2="4.1021" layer="21"/>
<rectangle x1="8.1407" y1="4.0767" x2="8.7249" y2="4.1021" layer="21"/>
<rectangle x1="9.2075" y1="4.0767" x2="9.7663" y2="4.1021" layer="21"/>
<rectangle x1="10.2489" y1="4.0767" x2="10.8077" y2="4.1021" layer="21"/>
<rectangle x1="11.4681" y1="4.0767" x2="12.7127" y2="4.1021" layer="21"/>
<rectangle x1="13.0683" y1="4.0767" x2="13.6271" y2="4.1021" layer="21"/>
<rectangle x1="13.6525" y1="4.0767" x2="14.2875" y2="4.1021" layer="21"/>
<rectangle x1="14.8209" y1="4.0767" x2="15.3289" y2="4.1021" layer="21"/>
<rectangle x1="15.5829" y1="4.0767" x2="16.5481" y2="4.1021" layer="21"/>
<rectangle x1="0.6223" y1="4.1021" x2="1.1303" y2="4.1275" layer="21"/>
<rectangle x1="1.7145" y1="4.1021" x2="2.2733" y2="4.1275" layer="21"/>
<rectangle x1="2.8321" y1="4.1021" x2="3.1623" y2="4.1275" layer="21"/>
<rectangle x1="4.0259" y1="4.1021" x2="4.2545" y2="4.1275" layer="21"/>
<rectangle x1="4.8895" y1="4.1021" x2="5.8801" y2="4.1275" layer="21"/>
<rectangle x1="6.4643" y1="4.1021" x2="6.9215" y2="4.1275" layer="21"/>
<rectangle x1="7.5311" y1="4.1021" x2="7.8613" y2="4.1275" layer="21"/>
<rectangle x1="8.1407" y1="4.1021" x2="8.7249" y2="4.1275" layer="21"/>
<rectangle x1="9.2075" y1="4.1021" x2="9.7663" y2="4.1275" layer="21"/>
<rectangle x1="10.2489" y1="4.1021" x2="10.8077" y2="4.1275" layer="21"/>
<rectangle x1="11.5443" y1="4.1021" x2="12.7127" y2="4.1275" layer="21"/>
<rectangle x1="13.0683" y1="4.1021" x2="13.6271" y2="4.1275" layer="21"/>
<rectangle x1="13.6779" y1="4.1021" x2="14.3129" y2="4.1275" layer="21"/>
<rectangle x1="14.8209" y1="4.1021" x2="15.3289" y2="4.1275" layer="21"/>
<rectangle x1="15.7861" y1="4.1021" x2="16.5481" y2="4.1275" layer="21"/>
<rectangle x1="0.6223" y1="4.1275" x2="1.1303" y2="4.1529" layer="21"/>
<rectangle x1="1.7145" y1="4.1275" x2="2.2987" y2="4.1529" layer="21"/>
<rectangle x1="2.8321" y1="4.1275" x2="3.3655" y2="4.1529" layer="21"/>
<rectangle x1="4.0259" y1="4.1275" x2="4.2545" y2="4.1529" layer="21"/>
<rectangle x1="4.8895" y1="4.1275" x2="5.8801" y2="4.1529" layer="21"/>
<rectangle x1="6.4643" y1="4.1275" x2="6.9215" y2="4.1529" layer="21"/>
<rectangle x1="7.5311" y1="4.1275" x2="7.8613" y2="4.1529" layer="21"/>
<rectangle x1="8.1407" y1="4.1275" x2="8.7249" y2="4.1529" layer="21"/>
<rectangle x1="9.2075" y1="4.1275" x2="9.7663" y2="4.1529" layer="21"/>
<rectangle x1="10.2489" y1="4.1275" x2="10.8077" y2="4.1529" layer="21"/>
<rectangle x1="11.6205" y1="4.1275" x2="12.7127" y2="4.1529" layer="21"/>
<rectangle x1="13.0683" y1="4.1275" x2="13.6271" y2="4.1529" layer="21"/>
<rectangle x1="13.7033" y1="4.1275" x2="14.3383" y2="4.1529" layer="21"/>
<rectangle x1="14.8209" y1="4.1275" x2="15.3543" y2="4.1529" layer="21"/>
<rectangle x1="15.9893" y1="4.1275" x2="16.5481" y2="4.1529" layer="21"/>
<rectangle x1="0.6223" y1="4.1529" x2="1.1303" y2="4.1783" layer="21"/>
<rectangle x1="1.7145" y1="4.1529" x2="2.2987" y2="4.1783" layer="21"/>
<rectangle x1="2.8321" y1="4.1529" x2="3.4671" y2="4.1783" layer="21"/>
<rectangle x1="4.0259" y1="4.1529" x2="4.2545" y2="4.1783" layer="21"/>
<rectangle x1="4.8895" y1="4.1529" x2="5.8801" y2="4.1783" layer="21"/>
<rectangle x1="6.4643" y1="4.1529" x2="6.9215" y2="4.1783" layer="21"/>
<rectangle x1="7.5311" y1="4.1529" x2="7.8613" y2="4.1783" layer="21"/>
<rectangle x1="8.1407" y1="4.1529" x2="8.7249" y2="4.1783" layer="21"/>
<rectangle x1="9.2075" y1="4.1529" x2="9.7663" y2="4.1783" layer="21"/>
<rectangle x1="10.2489" y1="4.1529" x2="10.8077" y2="4.1783" layer="21"/>
<rectangle x1="11.7221" y1="4.1529" x2="12.7127" y2="4.1783" layer="21"/>
<rectangle x1="13.0683" y1="4.1529" x2="13.6271" y2="4.1783" layer="21"/>
<rectangle x1="13.7033" y1="4.1529" x2="14.3383" y2="4.1783" layer="21"/>
<rectangle x1="14.8463" y1="4.1529" x2="15.3543" y2="4.1783" layer="21"/>
<rectangle x1="15.9893" y1="4.1529" x2="16.5481" y2="4.1783" layer="21"/>
<rectangle x1="0.6223" y1="4.1783" x2="1.1303" y2="4.2037" layer="21"/>
<rectangle x1="1.7145" y1="4.1783" x2="2.2987" y2="4.2037" layer="21"/>
<rectangle x1="2.8321" y1="4.1783" x2="3.4417" y2="4.2037" layer="21"/>
<rectangle x1="4.0259" y1="4.1783" x2="4.2545" y2="4.2037" layer="21"/>
<rectangle x1="4.9149" y1="4.1783" x2="5.8801" y2="4.2037" layer="21"/>
<rectangle x1="6.4643" y1="4.1783" x2="6.9215" y2="4.2037" layer="21"/>
<rectangle x1="7.5311" y1="4.1783" x2="7.8613" y2="4.2037" layer="21"/>
<rectangle x1="8.1407" y1="4.1783" x2="8.7249" y2="4.2037" layer="21"/>
<rectangle x1="9.2075" y1="4.1783" x2="9.7663" y2="4.2037" layer="21"/>
<rectangle x1="10.2489" y1="4.1783" x2="10.8077" y2="4.2037" layer="21"/>
<rectangle x1="11.8999" y1="4.1783" x2="12.7127" y2="4.2037" layer="21"/>
<rectangle x1="13.0683" y1="4.1783" x2="13.6271" y2="4.2037" layer="21"/>
<rectangle x1="13.7287" y1="4.1783" x2="14.3637" y2="4.2037" layer="21"/>
<rectangle x1="14.8463" y1="4.1783" x2="15.3543" y2="4.2037" layer="21"/>
<rectangle x1="15.9893" y1="4.1783" x2="16.5481" y2="4.2037" layer="21"/>
<rectangle x1="0.6223" y1="4.2037" x2="1.1303" y2="4.2291" layer="21"/>
<rectangle x1="1.7145" y1="4.2037" x2="2.2987" y2="4.2291" layer="21"/>
<rectangle x1="2.8321" y1="4.2037" x2="3.4417" y2="4.2291" layer="21"/>
<rectangle x1="4.0259" y1="4.2037" x2="4.2799" y2="4.2291" layer="21"/>
<rectangle x1="4.9149" y1="4.2037" x2="5.8801" y2="4.2291" layer="21"/>
<rectangle x1="6.4643" y1="4.2037" x2="6.9215" y2="4.2291" layer="21"/>
<rectangle x1="7.5311" y1="4.2037" x2="7.8613" y2="4.2291" layer="21"/>
<rectangle x1="8.1407" y1="4.2037" x2="8.7249" y2="4.2291" layer="21"/>
<rectangle x1="9.2075" y1="4.2037" x2="9.7663" y2="4.2291" layer="21"/>
<rectangle x1="10.2489" y1="4.2037" x2="10.8077" y2="4.2291" layer="21"/>
<rectangle x1="12.1539" y1="4.2037" x2="12.7127" y2="4.2291" layer="21"/>
<rectangle x1="13.0683" y1="4.2037" x2="13.6271" y2="4.2291" layer="21"/>
<rectangle x1="13.7541" y1="4.2037" x2="14.3891" y2="4.2291" layer="21"/>
<rectangle x1="14.8463" y1="4.2037" x2="15.3543" y2="4.2291" layer="21"/>
<rectangle x1="15.9893" y1="4.2037" x2="16.5481" y2="4.2291" layer="21"/>
<rectangle x1="0.6223" y1="4.2291" x2="1.1303" y2="4.2545" layer="21"/>
<rectangle x1="1.7145" y1="4.2291" x2="2.2987" y2="4.2545" layer="21"/>
<rectangle x1="2.8575" y1="4.2291" x2="3.4417" y2="4.2545" layer="21"/>
<rectangle x1="4.0259" y1="4.2291" x2="4.2799" y2="4.2545" layer="21"/>
<rectangle x1="4.9149" y1="4.2291" x2="5.8801" y2="4.2545" layer="21"/>
<rectangle x1="6.4643" y1="4.2291" x2="6.9215" y2="4.2545" layer="21"/>
<rectangle x1="7.5311" y1="4.2291" x2="7.8613" y2="4.2545" layer="21"/>
<rectangle x1="8.1407" y1="4.2291" x2="8.7249" y2="4.2545" layer="21"/>
<rectangle x1="9.2075" y1="4.2291" x2="9.7663" y2="4.2545" layer="21"/>
<rectangle x1="10.2489" y1="4.2291" x2="10.8077" y2="4.2545" layer="21"/>
<rectangle x1="12.1793" y1="4.2291" x2="12.7127" y2="4.2545" layer="21"/>
<rectangle x1="13.0683" y1="4.2291" x2="13.6271" y2="4.2545" layer="21"/>
<rectangle x1="13.7541" y1="4.2291" x2="14.3891" y2="4.2545" layer="21"/>
<rectangle x1="14.8463" y1="4.2291" x2="15.3543" y2="4.2545" layer="21"/>
<rectangle x1="15.9893" y1="4.2291" x2="16.5481" y2="4.2545" layer="21"/>
<rectangle x1="0.6223" y1="4.2545" x2="1.1303" y2="4.2799" layer="21"/>
<rectangle x1="1.7145" y1="4.2545" x2="2.3241" y2="4.2799" layer="21"/>
<rectangle x1="2.8575" y1="4.2545" x2="3.4417" y2="4.2799" layer="21"/>
<rectangle x1="4.0259" y1="4.2545" x2="4.2799" y2="4.2799" layer="21"/>
<rectangle x1="4.9403" y1="4.2545" x2="5.8801" y2="4.2799" layer="21"/>
<rectangle x1="6.4643" y1="4.2545" x2="6.9215" y2="4.2799" layer="21"/>
<rectangle x1="7.5311" y1="4.2545" x2="7.8613" y2="4.2799" layer="21"/>
<rectangle x1="8.1407" y1="4.2545" x2="8.7249" y2="4.2799" layer="21"/>
<rectangle x1="9.1821" y1="4.2545" x2="9.7663" y2="4.2799" layer="21"/>
<rectangle x1="10.2489" y1="4.2545" x2="10.8077" y2="4.2799" layer="21"/>
<rectangle x1="12.1793" y1="4.2545" x2="12.7127" y2="4.2799" layer="21"/>
<rectangle x1="13.0683" y1="4.2545" x2="13.6271" y2="4.2799" layer="21"/>
<rectangle x1="13.7795" y1="4.2545" x2="14.4145" y2="4.2799" layer="21"/>
<rectangle x1="14.8463" y1="4.2545" x2="15.3797" y2="4.2799" layer="21"/>
<rectangle x1="15.9893" y1="4.2545" x2="16.5227" y2="4.2799" layer="21"/>
<rectangle x1="0.6223" y1="4.2799" x2="1.1303" y2="4.3053" layer="21"/>
<rectangle x1="1.7145" y1="4.2799" x2="2.3241" y2="4.3053" layer="21"/>
<rectangle x1="2.8575" y1="4.2799" x2="3.4417" y2="4.3053" layer="21"/>
<rectangle x1="4.0005" y1="4.2799" x2="4.2799" y2="4.3053" layer="21"/>
<rectangle x1="4.9657" y1="4.2799" x2="5.8801" y2="4.3053" layer="21"/>
<rectangle x1="6.4643" y1="4.2799" x2="6.9215" y2="4.3053" layer="21"/>
<rectangle x1="7.5311" y1="4.2799" x2="7.8613" y2="4.3053" layer="21"/>
<rectangle x1="8.1407" y1="4.2799" x2="8.7249" y2="4.3053" layer="21"/>
<rectangle x1="9.1821" y1="4.2799" x2="9.7663" y2="4.3053" layer="21"/>
<rectangle x1="10.2235" y1="4.2799" x2="10.8077" y2="4.3053" layer="21"/>
<rectangle x1="12.1793" y1="4.2799" x2="12.7127" y2="4.3053" layer="21"/>
<rectangle x1="13.0683" y1="4.2799" x2="13.6271" y2="4.3053" layer="21"/>
<rectangle x1="13.7795" y1="4.2799" x2="14.4399" y2="4.3053" layer="21"/>
<rectangle x1="14.8717" y1="4.2799" x2="15.3797" y2="4.3053" layer="21"/>
<rectangle x1="15.9893" y1="4.2799" x2="16.5227" y2="4.3053" layer="21"/>
<rectangle x1="0.6223" y1="4.3053" x2="1.1303" y2="4.3307" layer="21"/>
<rectangle x1="1.7145" y1="4.3053" x2="2.3241" y2="4.3307" layer="21"/>
<rectangle x1="2.8829" y1="4.3053" x2="3.4163" y2="4.3307" layer="21"/>
<rectangle x1="4.0005" y1="4.3053" x2="4.3053" y2="4.3307" layer="21"/>
<rectangle x1="4.9657" y1="4.3053" x2="5.8801" y2="4.3307" layer="21"/>
<rectangle x1="6.4643" y1="4.3053" x2="6.8961" y2="4.3307" layer="21"/>
<rectangle x1="7.5311" y1="4.3053" x2="7.8613" y2="4.3307" layer="21"/>
<rectangle x1="8.1407" y1="4.3053" x2="8.7249" y2="4.3307" layer="21"/>
<rectangle x1="9.1821" y1="4.3053" x2="9.7663" y2="4.3307" layer="21"/>
<rectangle x1="10.2235" y1="4.3053" x2="10.8077" y2="4.3307" layer="21"/>
<rectangle x1="12.1539" y1="4.3053" x2="12.7127" y2="4.3307" layer="21"/>
<rectangle x1="13.0683" y1="4.3053" x2="13.6271" y2="4.3307" layer="21"/>
<rectangle x1="13.8049" y1="4.3053" x2="14.4399" y2="4.3307" layer="21"/>
<rectangle x1="14.8717" y1="4.3053" x2="15.4051" y2="4.3307" layer="21"/>
<rectangle x1="15.9639" y1="4.3053" x2="16.5227" y2="4.3307" layer="21"/>
<rectangle x1="0.6223" y1="4.3307" x2="1.1303" y2="4.3561" layer="21"/>
<rectangle x1="1.7145" y1="4.3307" x2="2.3241" y2="4.3561" layer="21"/>
<rectangle x1="2.8829" y1="4.3307" x2="3.4163" y2="4.3561" layer="21"/>
<rectangle x1="4.0005" y1="4.3307" x2="4.3053" y2="4.3561" layer="21"/>
<rectangle x1="4.9911" y1="4.3307" x2="5.8801" y2="4.3561" layer="21"/>
<rectangle x1="6.4643" y1="4.3307" x2="6.8961" y2="4.3561" layer="21"/>
<rectangle x1="7.5311" y1="4.3307" x2="7.8613" y2="4.3561" layer="21"/>
<rectangle x1="8.1407" y1="4.3307" x2="8.7249" y2="4.3561" layer="21"/>
<rectangle x1="9.1567" y1="4.3307" x2="9.7663" y2="4.3561" layer="21"/>
<rectangle x1="10.2235" y1="4.3307" x2="10.8077" y2="4.3561" layer="21"/>
<rectangle x1="12.1539" y1="4.3307" x2="12.7127" y2="4.3561" layer="21"/>
<rectangle x1="13.0683" y1="4.3307" x2="13.6271" y2="4.3561" layer="21"/>
<rectangle x1="13.8303" y1="4.3307" x2="14.4653" y2="4.3561" layer="21"/>
<rectangle x1="14.8717" y1="4.3307" x2="15.4051" y2="4.3561" layer="21"/>
<rectangle x1="15.9639" y1="4.3307" x2="16.5227" y2="4.3561" layer="21"/>
<rectangle x1="0.6223" y1="4.3561" x2="1.1303" y2="4.3815" layer="21"/>
<rectangle x1="1.7145" y1="4.3561" x2="2.3495" y2="4.3815" layer="21"/>
<rectangle x1="2.9083" y1="4.3561" x2="3.3909" y2="4.3815" layer="21"/>
<rectangle x1="4.0005" y1="4.3561" x2="4.3053" y2="4.3815" layer="21"/>
<rectangle x1="5.0165" y1="4.3561" x2="5.4483" y2="4.3815" layer="21"/>
<rectangle x1="5.4737" y1="4.3561" x2="5.8801" y2="4.3815" layer="21"/>
<rectangle x1="6.4643" y1="4.3561" x2="6.8707" y2="4.3815" layer="21"/>
<rectangle x1="7.5311" y1="4.3561" x2="7.8613" y2="4.3815" layer="21"/>
<rectangle x1="8.1407" y1="4.3561" x2="8.7249" y2="4.3815" layer="21"/>
<rectangle x1="9.1567" y1="4.3561" x2="9.7663" y2="4.3815" layer="21"/>
<rectangle x1="10.1981" y1="4.3561" x2="10.8077" y2="4.3815" layer="21"/>
<rectangle x1="12.1539" y1="4.3561" x2="12.7127" y2="4.3815" layer="21"/>
<rectangle x1="13.0683" y1="4.3561" x2="13.6271" y2="4.3815" layer="21"/>
<rectangle x1="13.8303" y1="4.3561" x2="14.4907" y2="4.3815" layer="21"/>
<rectangle x1="14.8971" y1="4.3561" x2="15.4305" y2="4.3815" layer="21"/>
<rectangle x1="15.9385" y1="4.3561" x2="16.4973" y2="4.3815" layer="21"/>
<rectangle x1="0.6223" y1="4.3815" x2="1.1303" y2="4.4069" layer="21"/>
<rectangle x1="1.7145" y1="4.3815" x2="2.3495" y2="4.4069" layer="21"/>
<rectangle x1="2.9337" y1="4.3815" x2="3.3909" y2="4.4069" layer="21"/>
<rectangle x1="3.9751" y1="4.3815" x2="4.3307" y2="4.4069" layer="21"/>
<rectangle x1="5.0673" y1="4.3815" x2="5.3975" y2="4.4069" layer="21"/>
<rectangle x1="5.4737" y1="4.3815" x2="5.8801" y2="4.4069" layer="21"/>
<rectangle x1="6.4643" y1="4.3815" x2="6.8453" y2="4.4069" layer="21"/>
<rectangle x1="7.5311" y1="4.3815" x2="7.8613" y2="4.4069" layer="21"/>
<rectangle x1="8.1407" y1="4.3815" x2="8.7249" y2="4.4069" layer="21"/>
<rectangle x1="9.1313" y1="4.3815" x2="9.7663" y2="4.4069" layer="21"/>
<rectangle x1="10.1727" y1="4.3815" x2="10.8077" y2="4.4069" layer="21"/>
<rectangle x1="12.1539" y1="4.3815" x2="12.6873" y2="4.4069" layer="21"/>
<rectangle x1="13.0683" y1="4.3815" x2="13.6271" y2="4.4069" layer="21"/>
<rectangle x1="13.8557" y1="4.3815" x2="14.4907" y2="4.4069" layer="21"/>
<rectangle x1="14.8971" y1="4.3815" x2="15.4559" y2="4.4069" layer="21"/>
<rectangle x1="15.9385" y1="4.3815" x2="16.4973" y2="4.4069" layer="21"/>
<rectangle x1="0.6223" y1="4.4069" x2="1.1303" y2="4.4323" layer="21"/>
<rectangle x1="1.7145" y1="4.4069" x2="2.3749" y2="4.4323" layer="21"/>
<rectangle x1="2.9591" y1="4.4069" x2="3.3655" y2="4.4323" layer="21"/>
<rectangle x1="3.9751" y1="4.4069" x2="4.3307" y2="4.4323" layer="21"/>
<rectangle x1="5.1181" y1="4.4069" x2="5.3213" y2="4.4323" layer="21"/>
<rectangle x1="5.4991" y1="4.4069" x2="5.8801" y2="4.4323" layer="21"/>
<rectangle x1="6.5405" y1="4.4069" x2="6.7945" y2="4.4323" layer="21"/>
<rectangle x1="7.5057" y1="4.4069" x2="7.8613" y2="4.4323" layer="21"/>
<rectangle x1="8.1407" y1="4.4069" x2="8.7249" y2="4.4323" layer="21"/>
<rectangle x1="9.1059" y1="4.4069" x2="9.8171" y2="4.4323" layer="21"/>
<rectangle x1="10.1473" y1="4.4069" x2="10.8077" y2="4.4323" layer="21"/>
<rectangle x1="11.3157" y1="4.4069" x2="11.4173" y2="4.4323" layer="21"/>
<rectangle x1="12.1285" y1="4.4069" x2="12.6873" y2="4.4323" layer="21"/>
<rectangle x1="13.0683" y1="4.4069" x2="13.6271" y2="4.4323" layer="21"/>
<rectangle x1="13.8811" y1="4.4069" x2="14.5161" y2="4.4323" layer="21"/>
<rectangle x1="14.9225" y1="4.4069" x2="15.4813" y2="4.4323" layer="21"/>
<rectangle x1="15.9131" y1="4.4069" x2="16.4973" y2="4.4323" layer="21"/>
<rectangle x1="0.6223" y1="4.4323" x2="0.9017" y2="4.4577" layer="21"/>
<rectangle x1="2.0447" y1="4.4323" x2="2.3749" y2="4.4577" layer="21"/>
<rectangle x1="2.9845" y1="4.4323" x2="3.3401" y2="4.4577" layer="21"/>
<rectangle x1="3.9497" y1="4.4323" x2="4.3561" y2="4.4577" layer="21"/>
<rectangle x1="5.4991" y1="4.4323" x2="5.8801" y2="4.4577" layer="21"/>
<rectangle x1="7.5057" y1="4.4323" x2="7.8613" y2="4.4577" layer="21"/>
<rectangle x1="8.1407" y1="4.4323" x2="8.8011" y2="4.4577" layer="21"/>
<rectangle x1="9.0297" y1="4.4323" x2="9.8679" y2="4.4577" layer="21"/>
<rectangle x1="10.0965" y1="4.4323" x2="10.8077" y2="4.4577" layer="21"/>
<rectangle x1="11.3157" y1="4.4323" x2="11.4935" y2="4.4577" layer="21"/>
<rectangle x1="12.1031" y1="4.4323" x2="12.6873" y2="4.4577" layer="21"/>
<rectangle x1="13.0683" y1="4.4323" x2="13.6271" y2="4.4577" layer="21"/>
<rectangle x1="13.8811" y1="4.4323" x2="14.5415" y2="4.4577" layer="21"/>
<rectangle x1="14.9225" y1="4.4323" x2="15.5067" y2="4.4577" layer="21"/>
<rectangle x1="15.8877" y1="4.4323" x2="16.4719" y2="4.4577" layer="21"/>
<rectangle x1="0.6223" y1="4.4577" x2="0.9017" y2="4.4831" layer="21"/>
<rectangle x1="2.0447" y1="4.4577" x2="2.4003" y2="4.4831" layer="21"/>
<rectangle x1="3.0353" y1="4.4577" x2="3.2893" y2="4.4831" layer="21"/>
<rectangle x1="3.9497" y1="4.4577" x2="4.3561" y2="4.4831" layer="21"/>
<rectangle x1="5.4991" y1="4.4577" x2="5.8801" y2="4.4831" layer="21"/>
<rectangle x1="7.5057" y1="4.4577" x2="7.8613" y2="4.4831" layer="21"/>
<rectangle x1="8.1407" y1="4.4577" x2="10.8077" y2="4.4831" layer="21"/>
<rectangle x1="11.3157" y1="4.4577" x2="11.5951" y2="4.4831" layer="21"/>
<rectangle x1="12.0523" y1="4.4577" x2="12.6873" y2="4.4831" layer="21"/>
<rectangle x1="13.0683" y1="4.4577" x2="13.6271" y2="4.4831" layer="21"/>
<rectangle x1="13.9065" y1="4.4577" x2="14.5415" y2="4.4831" layer="21"/>
<rectangle x1="14.9479" y1="4.4577" x2="15.5575" y2="4.4831" layer="21"/>
<rectangle x1="15.8369" y1="4.4577" x2="16.4719" y2="4.4831" layer="21"/>
<rectangle x1="0.6223" y1="4.4831" x2="0.9017" y2="4.5085" layer="21"/>
<rectangle x1="2.0447" y1="4.4831" x2="2.4003" y2="4.5085" layer="21"/>
<rectangle x1="3.9497" y1="4.4831" x2="4.3815" y2="4.5085" layer="21"/>
<rectangle x1="5.5245" y1="4.4831" x2="5.8801" y2="4.5085" layer="21"/>
<rectangle x1="7.5057" y1="4.4831" x2="7.8613" y2="4.5085" layer="21"/>
<rectangle x1="8.1407" y1="4.4831" x2="10.7823" y2="4.5085" layer="21"/>
<rectangle x1="11.2903" y1="4.4831" x2="11.7221" y2="4.5085" layer="21"/>
<rectangle x1="11.9761" y1="4.4831" x2="12.6873" y2="4.5085" layer="21"/>
<rectangle x1="13.0683" y1="4.4831" x2="13.6271" y2="4.5085" layer="21"/>
<rectangle x1="13.9319" y1="4.4831" x2="14.5669" y2="4.5085" layer="21"/>
<rectangle x1="14.9479" y1="4.4831" x2="15.6083" y2="4.5085" layer="21"/>
<rectangle x1="15.7861" y1="4.4831" x2="16.4465" y2="4.5085" layer="21"/>
<rectangle x1="0.6223" y1="4.5085" x2="0.9017" y2="4.5339" layer="21"/>
<rectangle x1="2.0447" y1="4.5085" x2="2.4257" y2="4.5339" layer="21"/>
<rectangle x1="3.9243" y1="4.5085" x2="4.3815" y2="4.5339" layer="21"/>
<rectangle x1="5.5245" y1="4.5085" x2="5.8801" y2="4.5339" layer="21"/>
<rectangle x1="7.4803" y1="4.5085" x2="7.8613" y2="4.5339" layer="21"/>
<rectangle x1="8.1407" y1="4.5085" x2="10.7823" y2="4.5339" layer="21"/>
<rectangle x1="11.2903" y1="4.5085" x2="12.6619" y2="4.5339" layer="21"/>
<rectangle x1="13.0683" y1="4.5085" x2="13.6271" y2="4.5339" layer="21"/>
<rectangle x1="13.9319" y1="4.5085" x2="14.5923" y2="4.5339" layer="21"/>
<rectangle x1="14.9733" y1="4.5085" x2="16.4465" y2="4.5339" layer="21"/>
<rectangle x1="0.6223" y1="4.5339" x2="0.9017" y2="4.5593" layer="21"/>
<rectangle x1="2.0447" y1="4.5339" x2="2.4257" y2="4.5593" layer="21"/>
<rectangle x1="3.8989" y1="4.5339" x2="4.4069" y2="4.5593" layer="21"/>
<rectangle x1="5.5499" y1="4.5339" x2="5.8801" y2="4.5593" layer="21"/>
<rectangle x1="7.4803" y1="4.5339" x2="7.8613" y2="4.5593" layer="21"/>
<rectangle x1="8.1407" y1="4.5339" x2="10.7823" y2="4.5593" layer="21"/>
<rectangle x1="11.2903" y1="4.5339" x2="12.6619" y2="4.5593" layer="21"/>
<rectangle x1="13.0683" y1="4.5339" x2="13.6271" y2="4.5593" layer="21"/>
<rectangle x1="13.9573" y1="4.5339" x2="14.5923" y2="4.5593" layer="21"/>
<rectangle x1="14.9733" y1="4.5339" x2="16.4211" y2="4.5593" layer="21"/>
<rectangle x1="0.6223" y1="4.5593" x2="0.9017" y2="4.5847" layer="21"/>
<rectangle x1="2.0447" y1="4.5593" x2="2.4511" y2="4.5847" layer="21"/>
<rectangle x1="3.8989" y1="4.5593" x2="4.4069" y2="4.5847" layer="21"/>
<rectangle x1="5.5499" y1="4.5593" x2="5.8801" y2="4.5847" layer="21"/>
<rectangle x1="7.4803" y1="4.5593" x2="7.8613" y2="4.5847" layer="21"/>
<rectangle x1="8.1407" y1="4.5593" x2="10.7569" y2="4.5847" layer="21"/>
<rectangle x1="11.2649" y1="4.5593" x2="12.6365" y2="4.5847" layer="21"/>
<rectangle x1="13.0683" y1="4.5593" x2="13.6271" y2="4.5847" layer="21"/>
<rectangle x1="13.9827" y1="4.5593" x2="14.6177" y2="4.5847" layer="21"/>
<rectangle x1="14.9987" y1="4.5593" x2="16.4211" y2="4.5847" layer="21"/>
<rectangle x1="0.6223" y1="4.5847" x2="0.9017" y2="4.6101" layer="21"/>
<rectangle x1="2.0447" y1="4.5847" x2="2.4765" y2="4.6101" layer="21"/>
<rectangle x1="3.8735" y1="4.5847" x2="4.4323" y2="4.6101" layer="21"/>
<rectangle x1="5.5753" y1="4.5847" x2="5.8801" y2="4.6101" layer="21"/>
<rectangle x1="7.4549" y1="4.5847" x2="7.8613" y2="4.6101" layer="21"/>
<rectangle x1="8.1407" y1="4.5847" x2="10.7569" y2="4.6101" layer="21"/>
<rectangle x1="11.2649" y1="4.5847" x2="12.6365" y2="4.6101" layer="21"/>
<rectangle x1="13.0683" y1="4.5847" x2="13.6271" y2="4.6101" layer="21"/>
<rectangle x1="13.9827" y1="4.5847" x2="14.6431" y2="4.6101" layer="21"/>
<rectangle x1="15.0241" y1="4.5847" x2="16.3957" y2="4.6101" layer="21"/>
<rectangle x1="0.6223" y1="4.6101" x2="0.9017" y2="4.6355" layer="21"/>
<rectangle x1="2.0447" y1="4.6101" x2="2.5019" y2="4.6355" layer="21"/>
<rectangle x1="3.8481" y1="4.6101" x2="4.4577" y2="4.6355" layer="21"/>
<rectangle x1="5.5753" y1="4.6101" x2="5.8801" y2="4.6355" layer="21"/>
<rectangle x1="7.4549" y1="4.6101" x2="7.8613" y2="4.6355" layer="21"/>
<rectangle x1="8.1407" y1="4.6101" x2="10.7315" y2="4.6355" layer="21"/>
<rectangle x1="11.2395" y1="4.6101" x2="12.6111" y2="4.6355" layer="21"/>
<rectangle x1="13.0683" y1="4.6101" x2="13.6271" y2="4.6355" layer="21"/>
<rectangle x1="14.0081" y1="4.6101" x2="14.6431" y2="4.6355" layer="21"/>
<rectangle x1="15.0495" y1="4.6101" x2="16.3703" y2="4.6355" layer="21"/>
<rectangle x1="0.6223" y1="4.6355" x2="0.9017" y2="4.6609" layer="21"/>
<rectangle x1="2.0447" y1="4.6355" x2="2.5019" y2="4.6609" layer="21"/>
<rectangle x1="3.8481" y1="4.6355" x2="4.4831" y2="4.6609" layer="21"/>
<rectangle x1="5.6007" y1="4.6355" x2="5.8801" y2="4.6609" layer="21"/>
<rectangle x1="7.4295" y1="4.6355" x2="7.8613" y2="4.6609" layer="21"/>
<rectangle x1="8.1407" y1="4.6355" x2="10.7315" y2="4.6609" layer="21"/>
<rectangle x1="11.2395" y1="4.6355" x2="12.6111" y2="4.6609" layer="21"/>
<rectangle x1="13.0683" y1="4.6355" x2="13.6271" y2="4.6609" layer="21"/>
<rectangle x1="14.0081" y1="4.6355" x2="14.6685" y2="4.6609" layer="21"/>
<rectangle x1="15.0495" y1="4.6355" x2="16.3449" y2="4.6609" layer="21"/>
<rectangle x1="0.6223" y1="4.6609" x2="0.9017" y2="4.6863" layer="21"/>
<rectangle x1="2.0447" y1="4.6609" x2="2.5273" y2="4.6863" layer="21"/>
<rectangle x1="3.8227" y1="4.6609" x2="4.5085" y2="4.6863" layer="21"/>
<rectangle x1="5.6007" y1="4.6609" x2="5.8801" y2="4.6863" layer="21"/>
<rectangle x1="7.4041" y1="4.6609" x2="7.8613" y2="4.6863" layer="21"/>
<rectangle x1="8.1407" y1="4.6609" x2="10.7061" y2="4.6863" layer="21"/>
<rectangle x1="11.2395" y1="4.6609" x2="12.5857" y2="4.6863" layer="21"/>
<rectangle x1="13.0683" y1="4.6609" x2="13.6271" y2="4.6863" layer="21"/>
<rectangle x1="14.0335" y1="4.6609" x2="14.6939" y2="4.6863" layer="21"/>
<rectangle x1="15.0749" y1="4.6609" x2="16.3449" y2="4.6863" layer="21"/>
<rectangle x1="0.6223" y1="4.6863" x2="0.9017" y2="4.7117" layer="21"/>
<rectangle x1="2.0447" y1="4.6863" x2="2.5527" y2="4.7117" layer="21"/>
<rectangle x1="3.7973" y1="4.6863" x2="4.5339" y2="4.7117" layer="21"/>
<rectangle x1="5.6007" y1="4.6863" x2="5.8801" y2="4.7117" layer="21"/>
<rectangle x1="7.3787" y1="4.6863" x2="7.8613" y2="4.7117" layer="21"/>
<rectangle x1="8.1407" y1="4.6863" x2="10.6807" y2="4.7117" layer="21"/>
<rectangle x1="11.2141" y1="4.6863" x2="12.5603" y2="4.7117" layer="21"/>
<rectangle x1="13.0683" y1="4.6863" x2="13.6271" y2="4.7117" layer="21"/>
<rectangle x1="14.0589" y1="4.6863" x2="14.6939" y2="4.7117" layer="21"/>
<rectangle x1="15.1003" y1="4.6863" x2="16.3195" y2="4.7117" layer="21"/>
<rectangle x1="0.6223" y1="4.7117" x2="0.9017" y2="4.7371" layer="21"/>
<rectangle x1="2.0447" y1="4.7117" x2="2.6035" y2="4.7371" layer="21"/>
<rectangle x1="3.7719" y1="4.7117" x2="4.5593" y2="4.7371" layer="21"/>
<rectangle x1="5.6261" y1="4.7117" x2="5.8801" y2="4.7371" layer="21"/>
<rectangle x1="7.3787" y1="4.7117" x2="7.8613" y2="4.7371" layer="21"/>
<rectangle x1="8.1407" y1="4.7117" x2="10.6553" y2="4.7371" layer="21"/>
<rectangle x1="11.2141" y1="4.7117" x2="12.5603" y2="4.7371" layer="21"/>
<rectangle x1="13.0683" y1="4.7117" x2="13.6271" y2="4.7371" layer="21"/>
<rectangle x1="14.0589" y1="4.7117" x2="14.7193" y2="4.7371" layer="21"/>
<rectangle x1="15.1511" y1="4.7117" x2="16.2687" y2="4.7371" layer="21"/>
<rectangle x1="0.6223" y1="4.7371" x2="0.9017" y2="4.7625" layer="21"/>
<rectangle x1="2.0447" y1="4.7371" x2="2.6289" y2="4.7625" layer="21"/>
<rectangle x1="3.7465" y1="4.7371" x2="4.5847" y2="4.7625" layer="21"/>
<rectangle x1="5.6261" y1="4.7371" x2="5.8801" y2="4.7625" layer="21"/>
<rectangle x1="7.3533" y1="4.7371" x2="7.8613" y2="4.7625" layer="21"/>
<rectangle x1="8.1407" y1="4.7371" x2="9.4869" y2="4.7625" layer="21"/>
<rectangle x1="9.5123" y1="4.7371" x2="10.6299" y2="4.7625" layer="21"/>
<rectangle x1="11.2141" y1="4.7371" x2="12.5095" y2="4.7625" layer="21"/>
<rectangle x1="13.0683" y1="4.7371" x2="13.6271" y2="4.7625" layer="21"/>
<rectangle x1="14.0843" y1="4.7371" x2="14.7447" y2="4.7625" layer="21"/>
<rectangle x1="15.1765" y1="4.7371" x2="16.2433" y2="4.7625" layer="21"/>
<rectangle x1="0.6223" y1="4.7625" x2="0.9017" y2="4.7879" layer="21"/>
<rectangle x1="2.0447" y1="4.7625" x2="2.6543" y2="4.7879" layer="21"/>
<rectangle x1="3.6957" y1="4.7625" x2="4.6101" y2="4.7879" layer="21"/>
<rectangle x1="5.6515" y1="4.7625" x2="5.8801" y2="4.7879" layer="21"/>
<rectangle x1="7.3025" y1="4.7625" x2="7.8613" y2="4.7879" layer="21"/>
<rectangle x1="8.1661" y1="4.7625" x2="9.4361" y2="4.7879" layer="21"/>
<rectangle x1="9.5631" y1="4.7625" x2="10.6045" y2="4.7879" layer="21"/>
<rectangle x1="11.2141" y1="4.7625" x2="12.4841" y2="4.7879" layer="21"/>
<rectangle x1="13.0683" y1="4.7625" x2="13.6271" y2="4.7879" layer="21"/>
<rectangle x1="14.1097" y1="4.7625" x2="14.7447" y2="4.7879" layer="21"/>
<rectangle x1="15.2019" y1="4.7625" x2="16.2179" y2="4.7879" layer="21"/>
<rectangle x1="0.6223" y1="4.7879" x2="0.9017" y2="4.8133" layer="21"/>
<rectangle x1="2.0447" y1="4.7879" x2="2.7051" y2="4.8133" layer="21"/>
<rectangle x1="3.6703" y1="4.7879" x2="4.6609" y2="4.8133" layer="21"/>
<rectangle x1="5.6261" y1="4.7879" x2="5.8801" y2="4.8133" layer="21"/>
<rectangle x1="7.2771" y1="4.7879" x2="7.8613" y2="4.8133" layer="21"/>
<rectangle x1="8.2169" y1="4.7879" x2="9.4107" y2="4.8133" layer="21"/>
<rectangle x1="9.6139" y1="4.7879" x2="10.5791" y2="4.8133" layer="21"/>
<rectangle x1="11.2649" y1="4.7879" x2="12.4587" y2="4.8133" layer="21"/>
<rectangle x1="13.0683" y1="4.7879" x2="13.6271" y2="4.8133" layer="21"/>
<rectangle x1="14.1097" y1="4.7879" x2="14.7701" y2="4.8133" layer="21"/>
<rectangle x1="15.2527" y1="4.7879" x2="16.1671" y2="4.8133" layer="21"/>
<rectangle x1="0.6223" y1="4.8133" x2="0.9017" y2="4.8387" layer="21"/>
<rectangle x1="2.0447" y1="4.8133" x2="2.7559" y2="4.8387" layer="21"/>
<rectangle x1="3.6195" y1="4.8133" x2="4.7117" y2="4.8387" layer="21"/>
<rectangle x1="5.5753" y1="4.8133" x2="5.8801" y2="4.8387" layer="21"/>
<rectangle x1="7.2263" y1="4.8133" x2="7.8613" y2="4.8387" layer="21"/>
<rectangle x1="8.2931" y1="4.8133" x2="9.3599" y2="4.8387" layer="21"/>
<rectangle x1="9.6647" y1="4.8133" x2="10.5283" y2="4.8387" layer="21"/>
<rectangle x1="11.3411" y1="4.8133" x2="12.4079" y2="4.8387" layer="21"/>
<rectangle x1="13.0683" y1="4.8133" x2="13.6271" y2="4.8387" layer="21"/>
<rectangle x1="14.1351" y1="4.8133" x2="14.7955" y2="4.8387" layer="21"/>
<rectangle x1="15.3035" y1="4.8133" x2="16.1163" y2="4.8387" layer="21"/>
<rectangle x1="0.6223" y1="4.8387" x2="0.9017" y2="4.8641" layer="21"/>
<rectangle x1="2.0447" y1="4.8387" x2="2.8067" y2="4.8641" layer="21"/>
<rectangle x1="3.5687" y1="4.8387" x2="4.7625" y2="4.8641" layer="21"/>
<rectangle x1="5.5245" y1="4.8387" x2="5.8801" y2="4.8641" layer="21"/>
<rectangle x1="6.4643" y1="4.8387" x2="6.4897" y2="4.8641" layer="21"/>
<rectangle x1="7.1755" y1="4.8387" x2="7.8613" y2="4.8641" layer="21"/>
<rectangle x1="8.3947" y1="4.8387" x2="9.2837" y2="4.8641" layer="21"/>
<rectangle x1="9.7409" y1="4.8387" x2="10.4775" y2="4.8641" layer="21"/>
<rectangle x1="11.4427" y1="4.8387" x2="12.3571" y2="4.8641" layer="21"/>
<rectangle x1="13.0683" y1="4.8387" x2="13.6271" y2="4.8641" layer="21"/>
<rectangle x1="14.1605" y1="4.8387" x2="14.7955" y2="4.8641" layer="21"/>
<rectangle x1="15.3543" y1="4.8387" x2="16.0655" y2="4.8641" layer="21"/>
<rectangle x1="0.6223" y1="4.8641" x2="0.9017" y2="4.8895" layer="21"/>
<rectangle x1="2.0193" y1="4.8641" x2="2.8575" y2="4.8895" layer="21"/>
<rectangle x1="3.4925" y1="4.8641" x2="4.8133" y2="4.8895" layer="21"/>
<rectangle x1="5.4737" y1="4.8641" x2="5.8801" y2="4.8895" layer="21"/>
<rectangle x1="6.4643" y1="4.8641" x2="6.5659" y2="4.8895" layer="21"/>
<rectangle x1="7.0993" y1="4.8641" x2="7.8613" y2="4.8895" layer="21"/>
<rectangle x1="8.5217" y1="4.8641" x2="9.2075" y2="4.8895" layer="21"/>
<rectangle x1="9.8425" y1="4.8641" x2="10.4013" y2="4.8895" layer="21"/>
<rectangle x1="11.5697" y1="4.8641" x2="12.2555" y2="4.8895" layer="21"/>
<rectangle x1="13.0683" y1="4.8641" x2="13.6271" y2="4.8895" layer="21"/>
<rectangle x1="15.4305" y1="4.8641" x2="15.9893" y2="4.8895" layer="21"/>
<rectangle x1="0.6223" y1="4.8895" x2="1.1303" y2="4.9149" layer="21"/>
<rectangle x1="1.7145" y1="4.8895" x2="2.9591" y2="4.9149" layer="21"/>
<rectangle x1="3.4163" y1="4.8895" x2="4.9149" y2="4.9149" layer="21"/>
<rectangle x1="5.3721" y1="4.8895" x2="5.8801" y2="4.9149" layer="21"/>
<rectangle x1="6.4643" y1="4.8895" x2="6.6929" y2="4.9149" layer="21"/>
<rectangle x1="6.9977" y1="4.8895" x2="7.8613" y2="4.9149" layer="21"/>
<rectangle x1="8.6995" y1="4.8895" x2="9.0551" y2="4.9149" layer="21"/>
<rectangle x1="9.9949" y1="4.8895" x2="10.2743" y2="4.9149" layer="21"/>
<rectangle x1="11.7475" y1="4.8895" x2="12.1031" y2="4.9149" layer="21"/>
<rectangle x1="13.0683" y1="4.8895" x2="13.6271" y2="4.9149" layer="21"/>
<rectangle x1="15.5829" y1="4.8895" x2="15.8623" y2="4.9149" layer="21"/>
<rectangle x1="0.6223" y1="4.9149" x2="1.1303" y2="4.9403" layer="21"/>
<rectangle x1="1.7145" y1="4.9149" x2="5.8801" y2="4.9403" layer="21"/>
<rectangle x1="6.4643" y1="4.9149" x2="7.8613" y2="4.9403" layer="21"/>
<rectangle x1="13.0683" y1="4.9149" x2="13.6271" y2="4.9403" layer="21"/>
<rectangle x1="0.6223" y1="4.9403" x2="1.1303" y2="4.9657" layer="21"/>
<rectangle x1="1.7145" y1="4.9403" x2="5.8801" y2="4.9657" layer="21"/>
<rectangle x1="6.4643" y1="4.9403" x2="7.8613" y2="4.9657" layer="21"/>
<rectangle x1="13.0683" y1="4.9403" x2="13.6271" y2="4.9657" layer="21"/>
<rectangle x1="0.6223" y1="4.9657" x2="1.1303" y2="4.9911" layer="21"/>
<rectangle x1="1.7145" y1="4.9657" x2="5.8801" y2="4.9911" layer="21"/>
<rectangle x1="6.4643" y1="4.9657" x2="7.8613" y2="4.9911" layer="21"/>
<rectangle x1="13.0683" y1="4.9657" x2="13.6271" y2="4.9911" layer="21"/>
<rectangle x1="0.6223" y1="4.9911" x2="1.1303" y2="5.0165" layer="21"/>
<rectangle x1="1.7145" y1="4.9911" x2="5.8801" y2="5.0165" layer="21"/>
<rectangle x1="6.4643" y1="4.9911" x2="7.8613" y2="5.0165" layer="21"/>
<rectangle x1="13.0683" y1="4.9911" x2="13.6271" y2="5.0165" layer="21"/>
<rectangle x1="0.6223" y1="5.0165" x2="1.1303" y2="5.0419" layer="21"/>
<rectangle x1="1.7145" y1="5.0165" x2="5.8801" y2="5.0419" layer="21"/>
<rectangle x1="6.4643" y1="5.0165" x2="7.8613" y2="5.0419" layer="21"/>
<rectangle x1="13.0683" y1="5.0165" x2="13.6271" y2="5.0419" layer="21"/>
<rectangle x1="0.6223" y1="5.0419" x2="1.1303" y2="5.0673" layer="21"/>
<rectangle x1="1.7145" y1="5.0419" x2="5.8801" y2="5.0673" layer="21"/>
<rectangle x1="6.4643" y1="5.0419" x2="7.8613" y2="5.0673" layer="21"/>
<rectangle x1="13.0683" y1="5.0419" x2="13.6271" y2="5.0673" layer="21"/>
<rectangle x1="0.6223" y1="5.0673" x2="1.1303" y2="5.0927" layer="21"/>
<rectangle x1="1.7145" y1="5.0673" x2="5.8801" y2="5.0927" layer="21"/>
<rectangle x1="6.4643" y1="5.0673" x2="7.8613" y2="5.0927" layer="21"/>
<rectangle x1="13.0683" y1="5.0673" x2="13.6271" y2="5.0927" layer="21"/>
<rectangle x1="0.6223" y1="5.0927" x2="1.1303" y2="5.1181" layer="21"/>
<rectangle x1="1.7145" y1="5.0927" x2="5.8801" y2="5.1181" layer="21"/>
<rectangle x1="6.4643" y1="5.0927" x2="7.8613" y2="5.1181" layer="21"/>
<rectangle x1="13.0683" y1="5.0927" x2="13.6271" y2="5.1181" layer="21"/>
<rectangle x1="0.6223" y1="5.1181" x2="1.1303" y2="5.1435" layer="21"/>
<rectangle x1="1.7145" y1="5.1181" x2="5.8801" y2="5.1435" layer="21"/>
<rectangle x1="6.4643" y1="5.1181" x2="7.8613" y2="5.1435" layer="21"/>
<rectangle x1="13.0683" y1="5.1181" x2="13.6271" y2="5.1435" layer="21"/>
<rectangle x1="0.6223" y1="5.1435" x2="1.1303" y2="5.1689" layer="21"/>
<rectangle x1="1.7145" y1="5.1435" x2="5.8801" y2="5.1689" layer="21"/>
<rectangle x1="6.4643" y1="5.1435" x2="7.8613" y2="5.1689" layer="21"/>
<rectangle x1="13.0683" y1="5.1435" x2="13.6271" y2="5.1689" layer="21"/>
<rectangle x1="0.6223" y1="5.1689" x2="1.1303" y2="5.1943" layer="21"/>
<rectangle x1="1.7145" y1="5.1689" x2="5.8801" y2="5.1943" layer="21"/>
<rectangle x1="6.4643" y1="5.1689" x2="7.8613" y2="5.1943" layer="21"/>
<rectangle x1="13.0683" y1="5.1689" x2="13.6271" y2="5.1943" layer="21"/>
<rectangle x1="0.6223" y1="5.1943" x2="1.1303" y2="5.2197" layer="21"/>
<rectangle x1="1.7145" y1="5.1943" x2="5.8801" y2="5.2197" layer="21"/>
<rectangle x1="6.4643" y1="5.1943" x2="7.8613" y2="5.2197" layer="21"/>
<rectangle x1="13.0683" y1="5.1943" x2="13.6271" y2="5.2197" layer="21"/>
<rectangle x1="0.6223" y1="5.2197" x2="1.1303" y2="5.2451" layer="21"/>
<rectangle x1="1.7145" y1="5.2197" x2="5.8801" y2="5.2451" layer="21"/>
<rectangle x1="6.4643" y1="5.2197" x2="7.8613" y2="5.2451" layer="21"/>
<rectangle x1="13.0683" y1="5.2197" x2="13.6271" y2="5.2451" layer="21"/>
<rectangle x1="0.6223" y1="5.2451" x2="1.1303" y2="5.2705" layer="21"/>
<rectangle x1="1.7145" y1="5.2451" x2="5.8801" y2="5.2705" layer="21"/>
<rectangle x1="6.4643" y1="5.2451" x2="7.8613" y2="5.2705" layer="21"/>
<rectangle x1="13.0683" y1="5.2451" x2="13.6271" y2="5.2705" layer="21"/>
<rectangle x1="0.6223" y1="5.2705" x2="1.1303" y2="5.2959" layer="21"/>
<rectangle x1="1.7145" y1="5.2705" x2="5.8801" y2="5.2959" layer="21"/>
<rectangle x1="6.4643" y1="5.2705" x2="7.8613" y2="5.2959" layer="21"/>
<rectangle x1="13.0683" y1="5.2705" x2="13.6271" y2="5.2959" layer="21"/>
<rectangle x1="0.6223" y1="5.2959" x2="1.2319" y2="5.3213" layer="21"/>
<rectangle x1="1.7145" y1="5.2959" x2="5.8801" y2="5.3213" layer="21"/>
<rectangle x1="6.4643" y1="5.2959" x2="7.8613" y2="5.3213" layer="21"/>
<rectangle x1="13.0683" y1="5.2959" x2="13.6271" y2="5.3213" layer="21"/>
<rectangle x1="0.6223" y1="5.3213" x2="1.3081" y2="5.3467" layer="21"/>
<rectangle x1="1.7145" y1="5.3213" x2="5.8801" y2="5.3467" layer="21"/>
<rectangle x1="6.4643" y1="5.3213" x2="7.8613" y2="5.3467" layer="21"/>
<rectangle x1="13.0683" y1="5.3213" x2="13.6271" y2="5.3467" layer="21"/>
<rectangle x1="0.6223" y1="5.3467" x2="1.4097" y2="5.3721" layer="21"/>
<rectangle x1="1.7145" y1="5.3467" x2="5.8801" y2="5.3721" layer="21"/>
<rectangle x1="6.4643" y1="5.3467" x2="7.8613" y2="5.3721" layer="21"/>
<rectangle x1="13.0683" y1="5.3467" x2="13.6271" y2="5.3721" layer="21"/>
<rectangle x1="0.6223" y1="5.3721" x2="1.4859" y2="5.3975" layer="21"/>
<rectangle x1="1.7145" y1="5.3721" x2="5.8801" y2="5.3975" layer="21"/>
<rectangle x1="6.4643" y1="5.3721" x2="7.8613" y2="5.3975" layer="21"/>
<rectangle x1="13.0683" y1="5.3721" x2="13.6271" y2="5.3975" layer="21"/>
<rectangle x1="0.6223" y1="5.3975" x2="1.5875" y2="5.4229" layer="21"/>
<rectangle x1="1.7145" y1="5.3975" x2="5.8801" y2="5.4229" layer="21"/>
<rectangle x1="6.4643" y1="5.3975" x2="7.8613" y2="5.4229" layer="21"/>
<rectangle x1="13.0683" y1="5.3975" x2="13.6271" y2="5.4229" layer="21"/>
<rectangle x1="0.6223" y1="5.4229" x2="1.6891" y2="5.4483" layer="21"/>
<rectangle x1="1.7145" y1="5.4229" x2="5.8801" y2="5.4483" layer="21"/>
<rectangle x1="6.4643" y1="5.4229" x2="7.8613" y2="5.4483" layer="21"/>
<rectangle x1="13.0683" y1="5.4229" x2="13.6271" y2="5.4483" layer="21"/>
<rectangle x1="0.6223" y1="5.4483" x2="5.8801" y2="5.4737" layer="21"/>
<rectangle x1="6.4643" y1="5.4483" x2="7.8613" y2="5.4737" layer="21"/>
<rectangle x1="13.0683" y1="5.4483" x2="13.6271" y2="5.4737" layer="21"/>
<rectangle x1="0.6223" y1="5.4737" x2="5.8801" y2="5.4991" layer="21"/>
<rectangle x1="6.4643" y1="5.4737" x2="7.8613" y2="5.4991" layer="21"/>
<rectangle x1="13.0683" y1="5.4737" x2="13.6271" y2="5.4991" layer="21"/>
<rectangle x1="0.6223" y1="5.4991" x2="5.8801" y2="5.5245" layer="21"/>
<rectangle x1="6.4643" y1="5.4991" x2="7.8613" y2="5.5245" layer="21"/>
<rectangle x1="13.0683" y1="5.4991" x2="13.6271" y2="5.5245" layer="21"/>
<rectangle x1="0.6223" y1="5.5245" x2="5.8801" y2="5.5499" layer="21"/>
<rectangle x1="6.4643" y1="5.5245" x2="7.8613" y2="5.5499" layer="21"/>
<rectangle x1="13.0683" y1="5.5245" x2="13.6271" y2="5.5499" layer="21"/>
<rectangle x1="0.6223" y1="5.5499" x2="5.8801" y2="5.5753" layer="21"/>
<rectangle x1="6.4643" y1="5.5499" x2="7.8613" y2="5.5753" layer="21"/>
<rectangle x1="13.0683" y1="5.5499" x2="13.6271" y2="5.5753" layer="21"/>
<rectangle x1="0.6223" y1="5.5753" x2="5.8801" y2="5.6007" layer="21"/>
<rectangle x1="6.4643" y1="5.5753" x2="7.8613" y2="5.6007" layer="21"/>
<rectangle x1="13.0683" y1="5.5753" x2="13.6271" y2="5.6007" layer="21"/>
<rectangle x1="0.6223" y1="5.6007" x2="5.8801" y2="5.6261" layer="21"/>
<rectangle x1="6.4643" y1="5.6007" x2="7.8613" y2="5.6261" layer="21"/>
<rectangle x1="13.0683" y1="5.6007" x2="13.6271" y2="5.6261" layer="21"/>
<rectangle x1="0.6223" y1="5.6261" x2="5.8801" y2="5.6515" layer="21"/>
<rectangle x1="6.4643" y1="5.6261" x2="7.8613" y2="5.6515" layer="21"/>
<rectangle x1="13.0683" y1="5.6261" x2="13.6271" y2="5.6515" layer="21"/>
<rectangle x1="0.6223" y1="5.6515" x2="5.8801" y2="5.6769" layer="21"/>
<rectangle x1="6.4643" y1="5.6515" x2="7.8613" y2="5.6769" layer="21"/>
<rectangle x1="13.0683" y1="5.6515" x2="13.6271" y2="5.6769" layer="21"/>
<rectangle x1="0.6223" y1="5.6769" x2="5.8801" y2="5.7023" layer="21"/>
<rectangle x1="6.4643" y1="5.6769" x2="7.8613" y2="5.7023" layer="21"/>
<rectangle x1="13.0683" y1="5.6769" x2="13.6271" y2="5.7023" layer="21"/>
<rectangle x1="0.6223" y1="5.7023" x2="5.8801" y2="5.7277" layer="21"/>
<rectangle x1="6.4643" y1="5.7023" x2="7.8613" y2="5.7277" layer="21"/>
<rectangle x1="13.0683" y1="5.7023" x2="13.6271" y2="5.7277" layer="21"/>
<rectangle x1="0.6223" y1="5.7277" x2="7.8613" y2="5.7531" layer="21"/>
<rectangle x1="0.6223" y1="5.7531" x2="7.8613" y2="5.7785" layer="21"/>
<rectangle x1="0.6223" y1="5.7785" x2="7.8613" y2="5.8039" layer="21"/>
<rectangle x1="0.6223" y1="5.8039" x2="7.8613" y2="5.8293" layer="21"/>
<rectangle x1="0.6223" y1="5.8293" x2="7.8613" y2="5.8547" layer="21"/>
<rectangle x1="0.6223" y1="5.8547" x2="7.8613" y2="5.8801" layer="21"/>
<rectangle x1="0.6223" y1="5.8801" x2="7.8613" y2="5.9055" layer="21"/>
<rectangle x1="0.6223" y1="5.9055" x2="7.8613" y2="5.9309" layer="21"/>
<rectangle x1="0.6223" y1="5.9309" x2="7.8613" y2="5.9563" layer="21"/>
<rectangle x1="0.6223" y1="5.9563" x2="7.8613" y2="5.9817" layer="21"/>
<rectangle x1="0.6223" y1="5.9817" x2="7.8613" y2="6.0071" layer="21"/>
<rectangle x1="0.6223" y1="6.0071" x2="7.8613" y2="6.0325" layer="21"/>
<rectangle x1="1.7907" y1="6.0325" x2="2.4257" y2="6.0579" layer="21"/>
<rectangle x1="3.1877" y1="6.0325" x2="3.8227" y2="6.0579" layer="21"/>
<rectangle x1="4.6355" y1="6.0325" x2="5.2705" y2="6.0579" layer="21"/>
<rectangle x1="6.0325" y1="6.0325" x2="6.6675" y2="6.0579" layer="21"/>
<rectangle x1="1.7907" y1="6.0579" x2="2.4257" y2="6.0833" layer="21"/>
<rectangle x1="3.1877" y1="6.0579" x2="3.7973" y2="6.0833" layer="21"/>
<rectangle x1="4.6355" y1="6.0579" x2="5.2451" y2="6.0833" layer="21"/>
<rectangle x1="6.0325" y1="6.0579" x2="6.6421" y2="6.0833" layer="21"/>
<rectangle x1="1.7907" y1="6.0833" x2="2.4257" y2="6.1087" layer="21"/>
<rectangle x1="3.1877" y1="6.0833" x2="3.7973" y2="6.1087" layer="21"/>
<rectangle x1="4.6355" y1="6.0833" x2="5.2451" y2="6.1087" layer="21"/>
<rectangle x1="6.0325" y1="6.0833" x2="6.6421" y2="6.1087" layer="21"/>
<rectangle x1="1.7907" y1="6.1087" x2="2.4257" y2="6.1341" layer="21"/>
<rectangle x1="3.1877" y1="6.1087" x2="3.7973" y2="6.1341" layer="21"/>
<rectangle x1="4.6355" y1="6.1087" x2="5.2451" y2="6.1341" layer="21"/>
<rectangle x1="6.0325" y1="6.1087" x2="6.6421" y2="6.1341" layer="21"/>
<rectangle x1="1.7907" y1="6.1341" x2="2.4257" y2="6.1595" layer="21"/>
<rectangle x1="3.1877" y1="6.1341" x2="3.7973" y2="6.1595" layer="21"/>
<rectangle x1="4.6355" y1="6.1341" x2="5.2451" y2="6.1595" layer="21"/>
<rectangle x1="6.0325" y1="6.1341" x2="6.6421" y2="6.1595" layer="21"/>
<rectangle x1="1.7907" y1="6.1595" x2="2.4257" y2="6.1849" layer="21"/>
<rectangle x1="3.1877" y1="6.1595" x2="3.7973" y2="6.1849" layer="21"/>
<rectangle x1="4.6355" y1="6.1595" x2="5.2451" y2="6.1849" layer="21"/>
<rectangle x1="6.0325" y1="6.1595" x2="6.6421" y2="6.1849" layer="21"/>
<rectangle x1="1.7907" y1="6.1849" x2="2.4257" y2="6.2103" layer="21"/>
<rectangle x1="3.1877" y1="6.1849" x2="3.7973" y2="6.2103" layer="21"/>
<rectangle x1="4.6355" y1="6.1849" x2="5.2451" y2="6.2103" layer="21"/>
<rectangle x1="6.0325" y1="6.1849" x2="6.6421" y2="6.2103" layer="21"/>
<rectangle x1="1.7907" y1="6.2103" x2="2.4257" y2="6.2357" layer="21"/>
<rectangle x1="3.1877" y1="6.2103" x2="3.7973" y2="6.2357" layer="21"/>
<rectangle x1="4.6355" y1="6.2103" x2="5.2451" y2="6.2357" layer="21"/>
<rectangle x1="6.0325" y1="6.2103" x2="6.6421" y2="6.2357" layer="21"/>
<rectangle x1="1.7907" y1="6.2357" x2="2.4257" y2="6.2611" layer="21"/>
<rectangle x1="3.1877" y1="6.2357" x2="3.7973" y2="6.2611" layer="21"/>
<rectangle x1="4.6355" y1="6.2357" x2="5.2451" y2="6.2611" layer="21"/>
<rectangle x1="6.0325" y1="6.2357" x2="6.6421" y2="6.2611" layer="21"/>
<rectangle x1="1.7907" y1="6.2611" x2="2.4257" y2="6.2865" layer="21"/>
<rectangle x1="3.1877" y1="6.2611" x2="3.7973" y2="6.2865" layer="21"/>
<rectangle x1="4.6355" y1="6.2611" x2="5.2451" y2="6.2865" layer="21"/>
<rectangle x1="6.0325" y1="6.2611" x2="6.6421" y2="6.2865" layer="21"/>
<rectangle x1="1.7907" y1="6.2865" x2="2.4257" y2="6.3119" layer="21"/>
<rectangle x1="3.1877" y1="6.2865" x2="3.7973" y2="6.3119" layer="21"/>
<rectangle x1="4.6355" y1="6.2865" x2="5.2451" y2="6.3119" layer="21"/>
<rectangle x1="6.0325" y1="6.2865" x2="6.6421" y2="6.3119" layer="21"/>
<rectangle x1="1.7907" y1="6.3119" x2="2.4257" y2="6.3373" layer="21"/>
<rectangle x1="3.1877" y1="6.3119" x2="3.7973" y2="6.3373" layer="21"/>
<rectangle x1="4.6355" y1="6.3119" x2="5.2451" y2="6.3373" layer="21"/>
<rectangle x1="6.0325" y1="6.3119" x2="6.6421" y2="6.3373" layer="21"/>
<rectangle x1="1.7907" y1="6.3373" x2="2.4257" y2="6.3627" layer="21"/>
<rectangle x1="3.1877" y1="6.3373" x2="3.7973" y2="6.3627" layer="21"/>
<rectangle x1="4.6355" y1="6.3373" x2="5.2451" y2="6.3627" layer="21"/>
<rectangle x1="6.0325" y1="6.3373" x2="6.6421" y2="6.3627" layer="21"/>
<rectangle x1="1.7907" y1="6.3627" x2="2.4257" y2="6.3881" layer="21"/>
<rectangle x1="3.1877" y1="6.3627" x2="3.7973" y2="6.3881" layer="21"/>
<rectangle x1="4.6355" y1="6.3627" x2="5.2451" y2="6.3881" layer="21"/>
<rectangle x1="6.0325" y1="6.3627" x2="6.6421" y2="6.3881" layer="21"/>
<rectangle x1="1.7907" y1="6.3881" x2="2.4257" y2="6.4135" layer="21"/>
<rectangle x1="3.1877" y1="6.3881" x2="3.7973" y2="6.4135" layer="21"/>
<rectangle x1="4.6355" y1="6.3881" x2="5.2451" y2="6.4135" layer="21"/>
<rectangle x1="6.0325" y1="6.3881" x2="6.6421" y2="6.4135" layer="21"/>
<rectangle x1="1.7907" y1="6.4135" x2="2.4257" y2="6.4389" layer="21"/>
<rectangle x1="3.1877" y1="6.4135" x2="3.7973" y2="6.4389" layer="21"/>
<rectangle x1="4.6355" y1="6.4135" x2="5.2451" y2="6.4389" layer="21"/>
<rectangle x1="6.0325" y1="6.4135" x2="6.6421" y2="6.4389" layer="21"/>
<rectangle x1="1.7907" y1="6.4389" x2="2.4257" y2="6.4643" layer="21"/>
<rectangle x1="3.1877" y1="6.4389" x2="3.7973" y2="6.4643" layer="21"/>
<rectangle x1="4.6355" y1="6.4389" x2="5.2451" y2="6.4643" layer="21"/>
<rectangle x1="6.0325" y1="6.4389" x2="6.6421" y2="6.4643" layer="21"/>
<rectangle x1="1.7907" y1="6.4643" x2="2.4257" y2="6.4897" layer="21"/>
<rectangle x1="3.1877" y1="6.4643" x2="3.7973" y2="6.4897" layer="21"/>
<rectangle x1="4.6355" y1="6.4643" x2="5.2451" y2="6.4897" layer="21"/>
<rectangle x1="6.0325" y1="6.4643" x2="6.6421" y2="6.4897" layer="21"/>
<rectangle x1="1.7907" y1="6.4897" x2="2.4257" y2="6.5151" layer="21"/>
<rectangle x1="3.1877" y1="6.4897" x2="3.7973" y2="6.5151" layer="21"/>
<rectangle x1="4.6355" y1="6.4897" x2="5.2451" y2="6.5151" layer="21"/>
<rectangle x1="6.0325" y1="6.4897" x2="6.6421" y2="6.5151" layer="21"/>
<rectangle x1="1.7907" y1="6.5151" x2="2.4257" y2="6.5405" layer="21"/>
<rectangle x1="3.1877" y1="6.5151" x2="3.7973" y2="6.5405" layer="21"/>
<rectangle x1="4.6355" y1="6.5151" x2="5.2451" y2="6.5405" layer="21"/>
<rectangle x1="6.0325" y1="6.5151" x2="6.6421" y2="6.5405" layer="21"/>
<rectangle x1="1.7907" y1="6.5405" x2="2.4257" y2="6.5659" layer="21"/>
<rectangle x1="3.1877" y1="6.5405" x2="3.7973" y2="6.5659" layer="21"/>
<rectangle x1="4.6355" y1="6.5405" x2="5.2451" y2="6.5659" layer="21"/>
<rectangle x1="6.0325" y1="6.5405" x2="6.6421" y2="6.5659" layer="21"/>
<rectangle x1="1.7907" y1="6.5659" x2="2.4257" y2="6.5913" layer="21"/>
<rectangle x1="3.1877" y1="6.5659" x2="3.7973" y2="6.5913" layer="21"/>
<rectangle x1="4.6355" y1="6.5659" x2="5.2451" y2="6.5913" layer="21"/>
<rectangle x1="6.0325" y1="6.5659" x2="6.6421" y2="6.5913" layer="21"/>
<rectangle x1="1.7907" y1="6.5913" x2="2.4257" y2="6.6167" layer="21"/>
<rectangle x1="3.1877" y1="6.5913" x2="3.7973" y2="6.6167" layer="21"/>
<rectangle x1="4.6355" y1="6.5913" x2="5.2451" y2="6.6167" layer="21"/>
<rectangle x1="6.0325" y1="6.5913" x2="6.6421" y2="6.6167" layer="21"/>
<rectangle x1="1.7907" y1="6.6167" x2="2.4257" y2="6.6421" layer="21"/>
<rectangle x1="3.1877" y1="6.6167" x2="3.7973" y2="6.6421" layer="21"/>
<rectangle x1="4.6355" y1="6.6167" x2="5.2451" y2="6.6421" layer="21"/>
<rectangle x1="6.0325" y1="6.6167" x2="6.6421" y2="6.6421" layer="21"/>
<rectangle x1="1.7907" y1="6.6421" x2="2.4257" y2="6.6675" layer="21"/>
<rectangle x1="3.1877" y1="6.6421" x2="3.7973" y2="6.6675" layer="21"/>
<rectangle x1="4.6355" y1="6.6421" x2="5.2451" y2="6.6675" layer="21"/>
<rectangle x1="6.0325" y1="6.6421" x2="6.6421" y2="6.6675" layer="21"/>
<rectangle x1="1.7907" y1="6.6675" x2="2.4257" y2="6.6929" layer="21"/>
<rectangle x1="3.1877" y1="6.6675" x2="3.7973" y2="6.6929" layer="21"/>
<rectangle x1="4.6355" y1="6.6675" x2="5.2451" y2="6.6929" layer="21"/>
<rectangle x1="6.0325" y1="6.6675" x2="6.6421" y2="6.6929" layer="21"/>
<rectangle x1="1.7907" y1="6.6929" x2="2.4257" y2="6.7183" layer="21"/>
<rectangle x1="3.1877" y1="6.6929" x2="3.7973" y2="6.7183" layer="21"/>
<rectangle x1="4.6355" y1="6.6929" x2="5.2451" y2="6.7183" layer="21"/>
<rectangle x1="6.0325" y1="6.6929" x2="6.6421" y2="6.7183" layer="21"/>
<rectangle x1="1.7907" y1="6.7183" x2="2.4257" y2="6.7437" layer="21"/>
<rectangle x1="3.1877" y1="6.7183" x2="3.7973" y2="6.7437" layer="21"/>
<rectangle x1="4.6355" y1="6.7183" x2="5.2451" y2="6.7437" layer="21"/>
<rectangle x1="6.0325" y1="6.7183" x2="6.6421" y2="6.7437" layer="21"/>
<rectangle x1="1.7907" y1="6.7437" x2="2.4257" y2="6.7691" layer="21"/>
<rectangle x1="3.1877" y1="6.7437" x2="3.7973" y2="6.7691" layer="21"/>
<rectangle x1="4.6355" y1="6.7437" x2="5.2451" y2="6.7691" layer="21"/>
<rectangle x1="6.0325" y1="6.7437" x2="6.6421" y2="6.7691" layer="21"/>
<rectangle x1="1.7907" y1="6.7691" x2="2.4257" y2="6.7945" layer="21"/>
<rectangle x1="3.1877" y1="6.7691" x2="3.7973" y2="6.7945" layer="21"/>
<rectangle x1="4.6355" y1="6.7691" x2="5.2451" y2="6.7945" layer="21"/>
<rectangle x1="6.0325" y1="6.7691" x2="6.6421" y2="6.7945" layer="21"/>
<rectangle x1="1.7907" y1="6.7945" x2="2.4257" y2="6.8199" layer="21"/>
<rectangle x1="3.1877" y1="6.7945" x2="3.7973" y2="6.8199" layer="21"/>
<rectangle x1="4.6355" y1="6.7945" x2="5.2451" y2="6.8199" layer="21"/>
<rectangle x1="6.0325" y1="6.7945" x2="6.6421" y2="6.8199" layer="21"/>
<rectangle x1="1.7907" y1="6.8199" x2="2.4257" y2="6.8453" layer="21"/>
<rectangle x1="3.1877" y1="6.8199" x2="3.7973" y2="6.8453" layer="21"/>
<rectangle x1="4.6355" y1="6.8199" x2="5.2451" y2="6.8453" layer="21"/>
<rectangle x1="6.0325" y1="6.8199" x2="6.6421" y2="6.8453" layer="21"/>
<rectangle x1="1.7907" y1="6.8453" x2="2.4257" y2="6.8707" layer="21"/>
<rectangle x1="3.1877" y1="6.8453" x2="3.7973" y2="6.8707" layer="21"/>
<rectangle x1="4.6355" y1="6.8453" x2="5.2451" y2="6.8707" layer="21"/>
<rectangle x1="6.0325" y1="6.8453" x2="6.6421" y2="6.8707" layer="21"/>
<rectangle x1="1.7907" y1="6.8707" x2="2.4257" y2="6.8961" layer="21"/>
<rectangle x1="3.1877" y1="6.8707" x2="3.7973" y2="6.8961" layer="21"/>
<rectangle x1="4.6355" y1="6.8707" x2="5.2451" y2="6.8961" layer="21"/>
<rectangle x1="6.0325" y1="6.8707" x2="6.6421" y2="6.8961" layer="21"/>
<rectangle x1="1.7907" y1="6.8961" x2="2.4257" y2="6.9215" layer="21"/>
<rectangle x1="3.1877" y1="6.8961" x2="3.7973" y2="6.9215" layer="21"/>
<rectangle x1="4.6355" y1="6.8961" x2="5.2451" y2="6.9215" layer="21"/>
<rectangle x1="6.0325" y1="6.8961" x2="6.6421" y2="6.9215" layer="21"/>
<rectangle x1="1.7907" y1="6.9215" x2="2.4257" y2="6.9469" layer="21"/>
<rectangle x1="3.1877" y1="6.9215" x2="3.7973" y2="6.9469" layer="21"/>
<rectangle x1="4.6355" y1="6.9215" x2="5.2451" y2="6.9469" layer="21"/>
<rectangle x1="6.0325" y1="6.9215" x2="6.6421" y2="6.9469" layer="21"/>
<rectangle x1="1.7907" y1="6.9469" x2="2.4257" y2="6.9723" layer="21"/>
<rectangle x1="3.1877" y1="6.9469" x2="3.7973" y2="6.9723" layer="21"/>
<rectangle x1="4.6355" y1="6.9469" x2="5.2451" y2="6.9723" layer="21"/>
<rectangle x1="6.0325" y1="6.9469" x2="6.6421" y2="6.9723" layer="21"/>
<rectangle x1="1.7907" y1="6.9723" x2="2.4257" y2="6.9977" layer="21"/>
<rectangle x1="3.1877" y1="6.9723" x2="3.7973" y2="6.9977" layer="21"/>
<rectangle x1="4.6355" y1="6.9723" x2="5.2451" y2="6.9977" layer="21"/>
<rectangle x1="6.0325" y1="6.9723" x2="6.6421" y2="6.9977" layer="21"/>
<rectangle x1="1.7907" y1="6.9977" x2="2.4257" y2="7.0231" layer="21"/>
<rectangle x1="4.6355" y1="6.9977" x2="5.2451" y2="7.0231" layer="21"/>
<rectangle x1="6.0325" y1="6.9977" x2="6.6421" y2="7.0231" layer="21"/>
<rectangle x1="1.7907" y1="7.0231" x2="2.4257" y2="7.0485" layer="21"/>
<rectangle x1="4.6355" y1="7.0231" x2="5.2451" y2="7.0485" layer="21"/>
<rectangle x1="6.0325" y1="7.0231" x2="6.6421" y2="7.0485" layer="21"/>
<rectangle x1="1.7907" y1="7.0485" x2="2.4257" y2="7.0739" layer="21"/>
<rectangle x1="4.6355" y1="7.0485" x2="5.2451" y2="7.0739" layer="21"/>
<rectangle x1="6.0325" y1="7.0485" x2="6.6421" y2="7.0739" layer="21"/>
<rectangle x1="1.7907" y1="7.0739" x2="2.4257" y2="7.0993" layer="21"/>
<rectangle x1="4.6355" y1="7.0739" x2="5.2451" y2="7.0993" layer="21"/>
<rectangle x1="6.0325" y1="7.0739" x2="6.6421" y2="7.0993" layer="21"/>
<rectangle x1="1.7907" y1="7.0993" x2="2.4257" y2="7.1247" layer="21"/>
<rectangle x1="4.6355" y1="7.0993" x2="5.2451" y2="7.1247" layer="21"/>
<rectangle x1="6.0325" y1="7.0993" x2="6.6421" y2="7.1247" layer="21"/>
<rectangle x1="1.7907" y1="7.1247" x2="2.4257" y2="7.1501" layer="21"/>
<rectangle x1="4.6355" y1="7.1247" x2="5.2451" y2="7.1501" layer="21"/>
<rectangle x1="6.0325" y1="7.1247" x2="6.6421" y2="7.1501" layer="21"/>
<rectangle x1="1.7907" y1="7.1501" x2="2.4257" y2="7.1755" layer="21"/>
<rectangle x1="4.6355" y1="7.1501" x2="5.2451" y2="7.1755" layer="21"/>
<rectangle x1="6.0325" y1="7.1501" x2="6.6421" y2="7.1755" layer="21"/>
<rectangle x1="1.7907" y1="7.1755" x2="2.4257" y2="7.2009" layer="21"/>
<rectangle x1="4.6355" y1="7.1755" x2="5.2451" y2="7.2009" layer="21"/>
<rectangle x1="6.0325" y1="7.1755" x2="6.6421" y2="7.2009" layer="21"/>
<rectangle x1="1.7907" y1="7.2009" x2="2.4257" y2="7.2263" layer="21"/>
<rectangle x1="4.6355" y1="7.2009" x2="5.2451" y2="7.2263" layer="21"/>
<rectangle x1="6.0325" y1="7.2009" x2="6.6421" y2="7.2263" layer="21"/>
<rectangle x1="1.7907" y1="7.2263" x2="2.4257" y2="7.2517" layer="21"/>
<rectangle x1="4.6355" y1="7.2263" x2="5.2451" y2="7.2517" layer="21"/>
<rectangle x1="6.0325" y1="7.2263" x2="6.6421" y2="7.2517" layer="21"/>
<rectangle x1="1.7907" y1="7.2517" x2="2.4257" y2="7.2771" layer="21"/>
<rectangle x1="4.6355" y1="7.2517" x2="5.2451" y2="7.2771" layer="21"/>
<rectangle x1="6.0325" y1="7.2517" x2="6.6421" y2="7.2771" layer="21"/>
<rectangle x1="1.7907" y1="7.2771" x2="2.4257" y2="7.3025" layer="21"/>
<rectangle x1="4.6355" y1="7.2771" x2="5.2451" y2="7.3025" layer="21"/>
<rectangle x1="6.0325" y1="7.2771" x2="6.6421" y2="7.3025" layer="21"/>
<rectangle x1="1.7907" y1="7.3025" x2="2.4257" y2="7.3279" layer="21"/>
<rectangle x1="4.6355" y1="7.3025" x2="5.2451" y2="7.3279" layer="21"/>
<rectangle x1="6.0325" y1="7.3025" x2="6.6421" y2="7.3279" layer="21"/>
<rectangle x1="1.7907" y1="7.3279" x2="2.4257" y2="7.3533" layer="21"/>
<rectangle x1="4.6355" y1="7.3279" x2="5.2451" y2="7.3533" layer="21"/>
<rectangle x1="6.0325" y1="7.3279" x2="6.6421" y2="7.3533" layer="21"/>
<rectangle x1="1.7907" y1="7.3533" x2="2.4257" y2="7.3787" layer="21"/>
<rectangle x1="4.6355" y1="7.3533" x2="5.2451" y2="7.3787" layer="21"/>
<rectangle x1="6.0325" y1="7.3533" x2="6.6421" y2="7.3787" layer="21"/>
<rectangle x1="1.7907" y1="7.3787" x2="2.4257" y2="7.4041" layer="21"/>
<rectangle x1="4.6355" y1="7.3787" x2="5.2451" y2="7.4041" layer="21"/>
<rectangle x1="6.0325" y1="7.3787" x2="6.6421" y2="7.4041" layer="21"/>
<rectangle x1="1.7907" y1="7.4041" x2="2.4257" y2="7.4295" layer="21"/>
<rectangle x1="4.6355" y1="7.4041" x2="5.2451" y2="7.4295" layer="21"/>
<rectangle x1="6.0325" y1="7.4041" x2="6.6421" y2="7.4295" layer="21"/>
<rectangle x1="1.7907" y1="7.4295" x2="2.4257" y2="7.4549" layer="21"/>
<rectangle x1="4.6355" y1="7.4295" x2="5.2451" y2="7.4549" layer="21"/>
<rectangle x1="6.0325" y1="7.4295" x2="6.6421" y2="7.4549" layer="21"/>
<rectangle x1="1.7907" y1="7.4549" x2="2.4257" y2="7.4803" layer="21"/>
<rectangle x1="4.6355" y1="7.4549" x2="5.2451" y2="7.4803" layer="21"/>
<rectangle x1="6.0325" y1="7.4549" x2="6.6421" y2="7.4803" layer="21"/>
<rectangle x1="1.7907" y1="7.4803" x2="2.4257" y2="7.5057" layer="21"/>
<rectangle x1="4.6355" y1="7.4803" x2="5.2451" y2="7.5057" layer="21"/>
<rectangle x1="6.0325" y1="7.4803" x2="6.6421" y2="7.5057" layer="21"/>
<rectangle x1="1.7907" y1="7.5057" x2="2.4257" y2="7.5311" layer="21"/>
<rectangle x1="4.6355" y1="7.5057" x2="5.2451" y2="7.5311" layer="21"/>
<rectangle x1="6.0325" y1="7.5057" x2="6.6421" y2="7.5311" layer="21"/>
<rectangle x1="1.7907" y1="7.5311" x2="2.4257" y2="7.5565" layer="21"/>
<rectangle x1="4.6355" y1="7.5311" x2="5.2451" y2="7.5565" layer="21"/>
<rectangle x1="6.0325" y1="7.5311" x2="6.6421" y2="7.5565" layer="21"/>
<rectangle x1="1.7907" y1="7.5565" x2="2.4257" y2="7.5819" layer="21"/>
<rectangle x1="4.6355" y1="7.5565" x2="5.2451" y2="7.5819" layer="21"/>
<rectangle x1="6.0325" y1="7.5565" x2="6.6421" y2="7.5819" layer="21"/>
<rectangle x1="1.7907" y1="7.5819" x2="2.4257" y2="7.6073" layer="21"/>
<rectangle x1="4.6355" y1="7.5819" x2="5.2451" y2="7.6073" layer="21"/>
<rectangle x1="6.0325" y1="7.5819" x2="6.6421" y2="7.6073" layer="21"/>
<rectangle x1="1.7907" y1="7.6073" x2="2.4257" y2="7.6327" layer="21"/>
<rectangle x1="4.6355" y1="7.6073" x2="5.2451" y2="7.6327" layer="21"/>
<rectangle x1="6.0325" y1="7.6073" x2="6.6421" y2="7.6327" layer="21"/>
<rectangle x1="1.7907" y1="7.6327" x2="2.4257" y2="7.6581" layer="21"/>
<rectangle x1="4.6355" y1="7.6327" x2="5.2451" y2="7.6581" layer="21"/>
<rectangle x1="6.0325" y1="7.6327" x2="6.6421" y2="7.6581" layer="21"/>
<rectangle x1="1.7907" y1="7.6581" x2="2.4257" y2="7.6835" layer="21"/>
<rectangle x1="4.6355" y1="7.6581" x2="5.2451" y2="7.6835" layer="21"/>
<rectangle x1="6.0325" y1="7.6581" x2="6.6421" y2="7.6835" layer="21"/>
<rectangle x1="1.7907" y1="7.6835" x2="2.4257" y2="7.7089" layer="21"/>
<rectangle x1="4.6355" y1="7.6835" x2="5.2451" y2="7.7089" layer="21"/>
<rectangle x1="6.0325" y1="7.6835" x2="6.6421" y2="7.7089" layer="21"/>
<rectangle x1="1.7907" y1="7.7089" x2="2.4257" y2="7.7343" layer="21"/>
<rectangle x1="4.6355" y1="7.7089" x2="5.2451" y2="7.7343" layer="21"/>
<rectangle x1="6.0325" y1="7.7089" x2="6.6421" y2="7.7343" layer="21"/>
<rectangle x1="4.6355" y1="7.7343" x2="5.2451" y2="7.7597" layer="21"/>
<rectangle x1="4.6355" y1="7.7597" x2="5.2451" y2="7.7851" layer="21"/>
<rectangle x1="4.6355" y1="7.7851" x2="5.2451" y2="7.8105" layer="21"/>
<rectangle x1="4.6355" y1="7.8105" x2="5.2451" y2="7.8359" layer="21"/>
<rectangle x1="4.6355" y1="7.8359" x2="5.2451" y2="7.8613" layer="21"/>
<rectangle x1="4.6355" y1="7.8613" x2="5.2451" y2="7.8867" layer="21"/>
<rectangle x1="4.6355" y1="7.8867" x2="5.2451" y2="7.9121" layer="21"/>
<rectangle x1="4.6355" y1="7.9121" x2="5.2451" y2="7.9375" layer="21"/>
<rectangle x1="4.6355" y1="7.9375" x2="5.2451" y2="7.9629" layer="21"/>
<rectangle x1="4.6355" y1="7.9629" x2="5.2451" y2="7.9883" layer="21"/>
<rectangle x1="4.6355" y1="7.9883" x2="5.2451" y2="8.0137" layer="21"/>
<rectangle x1="4.6355" y1="8.0137" x2="5.2451" y2="8.0391" layer="21"/>
<rectangle x1="4.6355" y1="8.0391" x2="5.2451" y2="8.0645" layer="21"/>
<rectangle x1="4.6355" y1="8.0645" x2="5.2451" y2="8.0899" layer="21"/>
<rectangle x1="4.6355" y1="8.0899" x2="5.2451" y2="8.1153" layer="21"/>
<rectangle x1="4.6355" y1="8.1153" x2="5.2451" y2="8.1407" layer="21"/>
<rectangle x1="4.6355" y1="8.1407" x2="5.2451" y2="8.1661" layer="21"/>
<rectangle x1="4.6355" y1="8.1661" x2="5.2451" y2="8.1915" layer="21"/>
<rectangle x1="4.6355" y1="8.1915" x2="5.2451" y2="8.2169" layer="21"/>
<rectangle x1="4.6355" y1="8.2169" x2="5.2451" y2="8.2423" layer="21"/>
<rectangle x1="4.6355" y1="8.2423" x2="5.2451" y2="8.2677" layer="21"/>
<rectangle x1="4.6355" y1="8.2677" x2="5.2451" y2="8.2931" layer="21"/>
<rectangle x1="4.6355" y1="8.2931" x2="5.2451" y2="8.3185" layer="21"/>
<rectangle x1="4.6355" y1="8.3185" x2="5.2451" y2="8.3439" layer="21"/>
<rectangle x1="4.6355" y1="8.3439" x2="5.2451" y2="8.3693" layer="21"/>
<rectangle x1="4.6355" y1="8.3693" x2="5.2451" y2="8.3947" layer="21"/>
<rectangle x1="4.6355" y1="8.3947" x2="5.2451" y2="8.4201" layer="21"/>
<rectangle x1="4.6355" y1="8.4201" x2="5.2451" y2="8.4455" layer="21"/>
<rectangle x1="4.6355" y1="8.4455" x2="5.2451" y2="8.4709" layer="21"/>
<rectangle x1="4.6355" y1="8.4709" x2="5.2451" y2="8.4963" layer="21"/>
<rectangle x1="4.6355" y1="8.4963" x2="5.2451" y2="8.5217" layer="21"/>
<rectangle x1="4.6355" y1="8.5217" x2="5.2451" y2="8.5471" layer="21"/>
<rectangle x1="4.6355" y1="8.5471" x2="5.2451" y2="8.5725" layer="21"/>
<rectangle x1="4.6355" y1="8.5725" x2="5.2451" y2="8.5979" layer="21"/>
</package>
<package name="FUS_AUTO">
<pad name="P$1" x="0" y="-7.62" drill="2.0828"/>
<pad name="P1" x="-3.81" y="0" drill="1.27"/>
<pad name="P2" x="3.81" y="0" drill="1.27"/>
<wire x1="-8.636" y1="2.6416" x2="8.636" y2="2.6416" width="0.127" layer="21"/>
<wire x1="8.636" y1="2.6416" x2="8.636" y2="-10.2616" width="0.127" layer="21"/>
<wire x1="8.636" y1="-10.2616" x2="-8.636" y2="-10.2616" width="0.127" layer="21"/>
<wire x1="-8.636" y1="-10.2616" x2="-8.636" y2="2.6416" width="0.127" layer="21"/>
<text x="-8.89" y="3.81" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="LOGOTECHMAKE">
<wire x1="-12.7" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<text x="-10.16" y="2.54" size="1.778" layer="94">Techmake Logo</text>
</symbol>
<symbol name="FUS_AUTO">
<wire x1="-2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<pin name="P1" x="-7.62" y="2.54" length="middle"/>
<pin name="P2" x="-7.62" y="-2.54" length="middle"/>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TECHMAKELOGO">
<gates>
<gate name="G$1" symbol="LOGOTECHMAKE" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="LOGOTECHMAKE">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUS_AUTO">
<gates>
<gate name="G$1" symbol="FUS_AUTO" x="-1.016" y="0"/>
</gates>
<devices>
<device name="" package="FUS_AUTO">
<connects>
<connect gate="G$1" pin="P1" pad="P1"/>
<connect gate="G$1" pin="P2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Aix_IC">
<packages>
<package name="SP3222EUEY-L/TR">
<smd name="EN" x="-2.97" y="2.925" dx="1.78" dy="0.42" layer="1"/>
<smd name="C1+" x="-2.97" y="2.275" dx="1.78" dy="0.42" layer="1"/>
<smd name="V+" x="-2.97" y="1.625" dx="1.78" dy="0.42" layer="1"/>
<smd name="C1-" x="-2.97" y="0.975" dx="1.78" dy="0.42" layer="1"/>
<smd name="C2+" x="-2.97" y="0.325" dx="1.78" dy="0.42" layer="1"/>
<smd name="C2-" x="-2.97" y="-0.325" dx="1.78" dy="0.42" layer="1"/>
<smd name="V-" x="-2.97" y="-0.975" dx="1.78" dy="0.42" layer="1"/>
<smd name="T2OUT" x="-2.97" y="-1.625" dx="1.78" dy="0.42" layer="1"/>
<smd name="R2IN" x="-2.97" y="-2.275" dx="1.78" dy="0.42" layer="1"/>
<smd name="R2OUT" x="-2.97" y="-2.925" dx="1.78" dy="0.42" layer="1"/>
<smd name="SHDN" x="2.97" y="2.925" dx="1.78" dy="0.42" layer="1"/>
<smd name="VCC" x="2.97" y="2.275" dx="1.78" dy="0.42" layer="1"/>
<smd name="GND" x="2.97" y="1.625" dx="1.78" dy="0.42" layer="1"/>
<smd name="T1OUT" x="2.97" y="0.975" dx="1.78" dy="0.42" layer="1"/>
<smd name="R1IN" x="2.97" y="0.325" dx="1.78" dy="0.42" layer="1"/>
<smd name="N.C." x="2.97" y="-0.975" dx="1.78" dy="0.42" layer="1"/>
<smd name="R1OUT" x="2.97" y="-0.325" dx="1.78" dy="0.42" layer="1"/>
<smd name="T1IN" x="2.97" y="-1.625" dx="1.78" dy="0.42" layer="1"/>
<smd name="T2IN" x="2.97" y="-2.275" dx="1.78" dy="0.42" layer="1"/>
<smd name="N.C.2" x="2.97" y="-2.925" dx="1.78" dy="0.42" layer="1"/>
<wire x1="-2.2" y1="-3.25" x2="2.2" y2="-3.25" width="0.127" layer="21"/>
<wire x1="2.2" y1="-3.25" x2="2.2" y2="3.25" width="0.127" layer="21"/>
<wire x1="2.2" y1="3.25" x2="-2.2" y2="3.25" width="0.127" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.127" layer="21"/>
<text x="-3.81" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="LP38693MP-3.3">
<smd name="GND" x="0" y="6.3" dx="3.3" dy="1.5" layer="1"/>
<smd name="EN" x="-2.25" y="0" dx="1.5" dy="1" layer="1" rot="R90"/>
<smd name="NC" x="-0.75" y="0" dx="1.5" dy="1" layer="1" rot="R90"/>
<smd name="OUT" x="0.75" y="0" dx="1.5" dy="1" layer="1" rot="R90"/>
<smd name="IN" x="2.25" y="0" dx="1.5" dy="1" layer="1" rot="R90"/>
<text x="-3.81" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-3.25" y1="5.56" x2="3.25" y2="5.56" width="0.127" layer="21"/>
<wire x1="3.25" y1="5.56" x2="3.25" y2="2" width="0.127" layer="21"/>
<wire x1="3.25" y1="2" x2="-3.25" y2="2" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2" x2="-3.25" y2="5.56" width="0.127" layer="21"/>
</package>
<package name="MIC2026">
<smd name="ENA" x="-2.465" y="1.905" dx="1.98" dy="0.53" layer="1"/>
<smd name="FLGA" x="-2.465" y="0.635" dx="1.98" dy="0.53" layer="1"/>
<smd name="FLGB" x="-2.465" y="-0.635" dx="1.98" dy="0.53" layer="1"/>
<smd name="ENB" x="-2.465" y="-1.905" dx="1.98" dy="0.53" layer="1"/>
<smd name="OUTA" x="2.465" y="1.905" dx="1.98" dy="0.53" layer="1"/>
<smd name="IN" x="2.465" y="0.635" dx="1.98" dy="0.53" layer="1"/>
<smd name="GND" x="2.465" y="-0.635" dx="1.98" dy="0.53" layer="1"/>
<smd name="OUTB" x="2.465" y="-1.905" dx="1.98" dy="0.53" layer="1"/>
<wire x1="-1.965" y1="2.465" x2="1.965" y2="2.465" width="0.127" layer="21"/>
<wire x1="1.965" y1="2.465" x2="1.965" y2="-2.465" width="0.127" layer="21"/>
<wire x1="1.965" y1="-2.465" x2="-1.965" y2="-2.465" width="0.127" layer="21"/>
<wire x1="-1.965" y1="-2.465" x2="-1.965" y2="2.465" width="0.127" layer="21"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<circle x="-0.8" y="1.27" radius="0.4" width="0.127" layer="21"/>
</package>
<package name="MIC29150-12WU-TR">
<smd name="IN" x="-2.54" y="0" dx="4.318" dy="1.524" layer="1" rot="R90"/>
<smd name="GND" x="0" y="0" dx="4.318" dy="1.524" layer="1" rot="R90"/>
<smd name="OUT" x="2.54" y="0" dx="4.318" dy="1.524" layer="1" rot="R90"/>
<smd name="GND2" x="0" y="9.144" dx="9.906" dy="11.176" layer="1" rot="R90"/>
<text x="-5.08" y="16.51" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5.334" y1="4.5593" x2="5.334" y2="4.5593" width="0.127" layer="21"/>
<wire x1="5.334" y1="4.5593" x2="5.334" y2="13.7287" width="0.127" layer="21"/>
<wire x1="5.334" y1="13.7287" x2="-5.334" y2="13.7287" width="0.127" layer="21"/>
<wire x1="-5.334" y1="13.7287" x2="-5.334" y2="4.5593" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SP3222EUEY-L/TR">
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<pin name="EN" x="-15.24" y="12.7" length="middle"/>
<pin name="C1+" x="-15.24" y="10.16" length="middle"/>
<pin name="V+" x="-15.24" y="7.62" length="middle"/>
<pin name="C1-" x="-15.24" y="5.08" length="middle"/>
<pin name="C2+" x="-15.24" y="2.54" length="middle"/>
<pin name="C2-" x="-15.24" y="0" length="middle"/>
<pin name="V-" x="-15.24" y="-2.54" length="middle"/>
<pin name="T2OUT" x="-15.24" y="-5.08" length="middle"/>
<pin name="R2IN" x="-15.24" y="-7.62" length="middle"/>
<pin name="R2OUT" x="-15.24" y="-10.16" length="middle"/>
<pin name="SHDN" x="15.24" y="12.7" length="middle" rot="R180"/>
<pin name="VCC" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="GND" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="T1OUT" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="R1IN" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="R1OUT" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="N.C." x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="T1IN" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="T2IN" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="N.C.2" x="15.24" y="-10.16" length="middle" rot="R180"/>
<text x="-10.16" y="17.78" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LP38693MP-3.3">
<wire x1="-5.08" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<pin name="GND" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="EN" x="-2.54" y="-7.62" length="short" rot="R90"/>
<pin name="NC" x="0" y="-7.62" length="short" rot="R90"/>
<pin name="OUT" x="2.54" y="-7.62" length="short" rot="R90"/>
<pin name="IN" x="5.08" y="-7.62" length="short" rot="R90"/>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="MIC2026">
<pin name="ENA" x="-10.16" y="5.08" length="short"/>
<pin name="FLGA" x="-10.16" y="2.54" length="short"/>
<pin name="FLGB" x="-10.16" y="0" length="short"/>
<pin name="ENB" x="-10.16" y="-2.54" length="short"/>
<pin name="OUTB" x="10.16" y="-2.54" length="short" rot="R180"/>
<pin name="GND" x="10.16" y="0" length="short" rot="R180"/>
<pin name="IN" x="10.16" y="2.54" length="short" rot="R180"/>
<pin name="OUTA" x="10.16" y="5.08" length="short" rot="R180"/>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-7.62" y="10.16" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="MIC29150-12WU-TR">
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<pin name="OUT" x="7.62" y="2.54" length="middle" rot="R180"/>
<pin name="GND" x="7.62" y="0" length="middle" rot="R180"/>
<pin name="IN" x="7.62" y="-2.54" length="middle" rot="R180"/>
<text x="-7.62" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SP3222EUEY-L/TR">
<gates>
<gate name="G$1" symbol="SP3222EUEY-L/TR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SP3222EUEY-L/TR">
<connects>
<connect gate="G$1" pin="C1+" pad="C1+"/>
<connect gate="G$1" pin="C1-" pad="C1-"/>
<connect gate="G$1" pin="C2+" pad="C2+"/>
<connect gate="G$1" pin="C2-" pad="C2-"/>
<connect gate="G$1" pin="EN" pad="EN"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="N.C." pad="N.C."/>
<connect gate="G$1" pin="N.C.2" pad="N.C.2"/>
<connect gate="G$1" pin="R1IN" pad="R1IN"/>
<connect gate="G$1" pin="R1OUT" pad="R1OUT"/>
<connect gate="G$1" pin="R2IN" pad="R2IN"/>
<connect gate="G$1" pin="R2OUT" pad="R2OUT"/>
<connect gate="G$1" pin="SHDN" pad="SHDN"/>
<connect gate="G$1" pin="T1IN" pad="T1IN"/>
<connect gate="G$1" pin="T1OUT" pad="T1OUT"/>
<connect gate="G$1" pin="T2IN" pad="T2IN"/>
<connect gate="G$1" pin="T2OUT" pad="T2OUT"/>
<connect gate="G$1" pin="V+" pad="V+"/>
<connect gate="G$1" pin="V-" pad="V-"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LP38693MP-3.3">
<gates>
<gate name="G$1" symbol="LP38693MP-3.3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LP38693MP-3.3">
<connects>
<connect gate="G$1" pin="EN" pad="EN"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="IN" pad="IN"/>
<connect gate="G$1" pin="NC" pad="NC"/>
<connect gate="G$1" pin="OUT" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MIC2026-1YM-TR">
<gates>
<gate name="G$1" symbol="MIC2026" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MIC2026">
<connects>
<connect gate="G$1" pin="ENA" pad="ENA"/>
<connect gate="G$1" pin="ENB" pad="ENB"/>
<connect gate="G$1" pin="FLGA" pad="FLGA"/>
<connect gate="G$1" pin="FLGB" pad="FLGB"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="IN" pad="IN"/>
<connect gate="G$1" pin="OUTA" pad="OUTA"/>
<connect gate="G$1" pin="OUTB" pad="OUTB"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MIC29150-12WU-TR">
<gates>
<gate name="G$1" symbol="MIC29150-12WU-TR" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="MIC29150-12WU-TR">
<connects>
<connect gate="G$1" pin="GND" pad="GND GND2"/>
<connect gate="G$1" pin="IN" pad="IN"/>
<connect gate="G$1" pin="OUT" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;map name="nav_main"&gt;
&lt;area shape="rect" coords="0,1,140,23" href="../military_specs.asp" title=""&gt;
&lt;area shape="rect" coords="0,24,140,51" href="../about.asp" title=""&gt;
&lt;area shape="rect" coords="1,52,140,77" href="../rfq.asp" title=""&gt;
&lt;area shape="rect" coords="0,78,139,103" href="../products.asp" title=""&gt;
&lt;area shape="rect" coords="1,102,138,128" href="../excess_inventory.asp" title=""&gt;
&lt;area shape="rect" coords="1,129,138,150" href="../edge.asp" title=""&gt;
&lt;area shape="rect" coords="1,151,139,178" href="../industry_links.asp" title=""&gt;
&lt;area shape="rect" coords="0,179,139,201" href="../comments.asp" title=""&gt;
&lt;area shape="rect" coords="1,203,138,231" href="../directory.asp" title=""&gt;
&lt;area shape="default" nohref&gt;
&lt;/map&gt;

&lt;html&gt;

&lt;title&gt;&lt;/title&gt;

 &lt;LINK REL="StyleSheet" TYPE="text/css" HREF="style-sheet.css"&gt;

&lt;body bgcolor="#ffffff" text="#000000" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0"&gt;
&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0 height="55%"&gt;
&lt;tr valign="top"&gt;

&lt;/td&gt;
&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/BODY&gt;&lt;/HTML&gt;</description>
<packages>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.683" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.826" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.032" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.683" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.826" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="C-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-US" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1005" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="raspberrypi_cm">
<packages>
<package name="QFN64-9X9">
<description>&lt;b&gt;QFN64-9X9&lt;/b&gt;&lt;p&gt;
Source: http://www.gennum.com/video/pdf/36655DOC.pdf</description>
<wire x1="-4.3984" y1="-4.3984" x2="4.3984" y2="-4.3984" width="0.2032" layer="51"/>
<wire x1="4.3984" y1="-4.3984" x2="4.3984" y2="4.3984" width="0.2032" layer="51"/>
<wire x1="4.3984" y1="4.3984" x2="-4.3984" y2="4.3984" width="0.2032" layer="51"/>
<wire x1="-4.3984" y1="4.3984" x2="-4.3984" y2="-4.3984" width="0.2032" layer="51"/>
<smd name="1" x="-4.477" y="3.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="2" x="-4.477" y="3.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="3" x="-4.477" y="2.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="4" x="-4.477" y="2.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="5" x="-4.477" y="1.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="6" x="-4.477" y="1.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="7" x="-4.477" y="0.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="8" x="-4.477" y="0.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="9" x="-4.477" y="-0.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="10" x="-4.477" y="-0.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="11" x="-4.477" y="-1.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="12" x="-4.477" y="-1.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="13" x="-4.477" y="-2.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="14" x="-4.477" y="-2.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="15" x="-4.477" y="-3.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="16" x="-4.477" y="-3.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="17" x="-3.75" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="18" x="-3.25" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="19" x="-2.75" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="20" x="-2.25" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="21" x="-1.75" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="22" x="-1.25" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="23" x="-0.75" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="24" x="-0.25" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="25" x="0.25" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="26" x="0.75" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="27" x="1.25" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="28" x="1.75" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="29" x="2.25" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="30" x="2.75" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="31" x="3.25" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="32" x="3.75" y="-4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="33" x="4.477" y="-3.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="34" x="4.477" y="-3.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="35" x="4.477" y="-2.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="36" x="4.477" y="-2.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="37" x="4.477" y="-1.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="38" x="4.477" y="-1.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="39" x="4.477" y="-0.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="40" x="4.477" y="-0.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="41" x="4.477" y="0.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="42" x="4.477" y="0.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="43" x="4.477" y="1.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="44" x="4.477" y="1.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="45" x="4.477" y="2.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="46" x="4.477" y="2.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="47" x="4.477" y="3.25" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="48" x="4.477" y="3.75" dx="0.55" dy="0.25" layer="1" roundness="100"/>
<smd name="49" x="3.75" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="50" x="3.25" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="51" x="2.75" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="52" x="2.25" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="53" x="1.75" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="54" x="1.25" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="55" x="0.75" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="56" x="0.25" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="57" x="-0.25" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="58" x="-0.75" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="59" x="-1.25" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="60" x="-1.75" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="61" x="-2.25" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="62" x="-2.75" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="63" x="-3.25" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="64" x="-3.75" y="4.477" dx="0.25" dy="0.55" layer="1" roundness="100"/>
<smd name="EXP" x="0" y="0" dx="7.1" dy="7.1" layer="1" stop="no" cream="no"/>
<text x="-4.5" y="5.135" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.5" y="-6.405" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.35" y1="0.3" x2="-0.3" y2="4.35" layer="51"/>
<polygon width="0.2032" layer="29">
<vertex x="-3.2" y="3.55"/>
<vertex x="3.55" y="3.55"/>
<vertex x="3.55" y="-3.55"/>
<vertex x="-3.55" y="-3.55"/>
<vertex x="-3.55" y="3.2"/>
</polygon>
<polygon width="0.2032" layer="31">
<vertex x="-3.35" y="3.1"/>
<vertex x="-3.1" y="3.35"/>
<vertex x="3.35" y="3.35"/>
<vertex x="3.35" y="-3.35"/>
<vertex x="-3.35" y="-3.35"/>
</polygon>
<text x="-5.588" y="3.556" size="1.27" layer="21">*</text>
</package>
</packages>
<symbols>
<symbol name="LAN9512">
<pin name="USBDM2" x="-17.78" y="-40.64" length="middle" rot="R90"/>
<pin name="USBDP2" x="-15.24" y="-40.64" length="middle" rot="R90"/>
<pin name="USBDM3" x="-12.7" y="-40.64" length="middle" rot="R90"/>
<pin name="USBDP3" x="-10.16" y="-40.64" length="middle" rot="R90"/>
<pin name="VDD33A@1" x="-7.62" y="-40.64" length="middle" rot="R90"/>
<pin name="NC@1" x="-5.08" y="-40.64" length="middle" rot="R90"/>
<pin name="NC@2" x="-2.54" y="-40.64" length="middle" rot="R90"/>
<pin name="NC@3" x="0" y="-40.64" length="middle" rot="R90"/>
<pin name="NC@4" x="2.54" y="-40.64" length="middle" rot="R90"/>
<pin name="VDD33A@2" x="5.08" y="-40.64" length="middle" rot="R90"/>
<pin name="VBUS_DET" x="7.62" y="-40.64" length="middle" rot="R90"/>
<pin name="NRESET" x="10.16" y="-40.64" length="middle" rot="R90"/>
<pin name="TEST1" x="12.7" y="-40.64" length="middle" rot="R90"/>
<pin name="PRTCTL2" x="15.24" y="-40.64" length="middle" rot="R90"/>
<pin name="VDD18CORE@1" x="17.78" y="-40.64" length="middle" rot="R90"/>
<pin name="PRTCTL3" x="20.32" y="-40.64" length="middle" rot="R90"/>
<pin name="NC@5" x="40.64" y="-20.32" length="middle" rot="R180"/>
<pin name="NC@6" x="40.64" y="-17.78" length="middle" rot="R180"/>
<pin name="VDD33IO@1" x="40.64" y="-15.24" length="middle" rot="R180"/>
<pin name="NFDX_LED/GPIO0" x="40.64" y="-12.7" length="middle" rot="R180"/>
<pin name="NLNKA_LED/GPIO1" x="40.64" y="-10.16" length="middle" rot="R180"/>
<pin name="NSPD_LED/GPIO2" x="40.64" y="-7.62" length="middle" rot="R180"/>
<pin name="EECLK" x="40.64" y="-5.08" length="middle" rot="R180"/>
<pin name="EECS" x="40.64" y="-2.54" length="middle" rot="R180"/>
<pin name="EEDO" x="40.64" y="0" length="middle" rot="R180"/>
<pin name="EEDI" x="40.64" y="2.54" length="middle" rot="R180"/>
<pin name="VDD33IO@2" x="40.64" y="5.08" length="middle" rot="R180"/>
<pin name="NTRST" x="40.64" y="7.62" length="middle" rot="R180"/>
<pin name="TMS" x="40.64" y="10.16" length="middle" rot="R180"/>
<pin name="TDI" x="40.64" y="12.7" length="middle" rot="R180"/>
<pin name="TDO" x="40.64" y="15.24" length="middle" rot="R180"/>
<pin name="TCK" x="40.64" y="17.78" length="middle" rot="R180"/>
<pin name="VDD33IO@3" x="20.32" y="38.1" length="middle" rot="R270"/>
<pin name="TEST2" x="17.78" y="38.1" length="middle" rot="R270"/>
<pin name="GPIO3" x="15.24" y="38.1" length="middle" rot="R270"/>
<pin name="GPIO4" x="12.7" y="38.1" length="middle" rot="R270"/>
<pin name="GPIO5" x="10.16" y="38.1" length="middle" rot="R270"/>
<pin name="VDD18CORE@2" x="7.62" y="38.1" length="middle" rot="R270"/>
<pin name="VDD33IO@4" x="5.08" y="38.1" length="middle" rot="R270"/>
<pin name="TEST3" x="2.54" y="38.1" length="middle" rot="R270"/>
<pin name="AUTOMDXI_EN" x="0" y="38.1" length="middle" rot="R270"/>
<pin name="GPIO6" x="-2.54" y="38.1" length="middle" rot="R270"/>
<pin name="GPIO7" x="-5.08" y="38.1" length="middle" rot="R270"/>
<pin name="CLK24_EN" x="-7.62" y="38.1" length="middle" rot="R270"/>
<pin name="CLK24_OUT" x="-10.16" y="38.1" length="middle" rot="R270"/>
<pin name="VDD33IO@5" x="-12.7" y="38.1" length="middle" rot="R270"/>
<pin name="TEST4" x="-15.24" y="38.1" length="middle" rot="R270"/>
<pin name="VDD18ETHPLL" x="-17.78" y="38.1" length="middle" rot="R270"/>
<pin name="VDD33A@3" x="-38.1" y="17.78" length="middle"/>
<pin name="EXRES" x="-38.1" y="15.24" length="middle"/>
<pin name="VDD33A@4" x="-38.1" y="12.7" length="middle"/>
<pin name="RXP" x="-38.1" y="10.16" length="middle"/>
<pin name="RXN" x="-38.1" y="7.62" length="middle"/>
<pin name="VDD33A@5" x="-38.1" y="5.08" length="middle"/>
<pin name="TXP" x="-38.1" y="2.54" length="middle"/>
<pin name="TXN" x="-38.1" y="0" length="middle"/>
<pin name="VDD33A@6" x="-38.1" y="-2.54" length="middle"/>
<pin name="USBDM0" x="-38.1" y="-5.08" length="middle"/>
<pin name="USBDP0" x="-38.1" y="-7.62" length="middle"/>
<pin name="XO" x="-38.1" y="-10.16" length="middle"/>
<pin name="XI" x="-38.1" y="-12.7" length="middle"/>
<pin name="VDD18USBPLL" x="-38.1" y="-15.24" length="middle"/>
<pin name="USBRBIAS" x="-38.1" y="-17.78" length="middle"/>
<pin name="VDD33A@7" x="-38.1" y="-20.32" length="middle"/>
<wire x1="35.56" y1="-35.56" x2="-33.02" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-33.02" y1="-35.56" x2="-33.02" y2="33.02" width="0.254" layer="94"/>
<wire x1="-33.02" y1="33.02" x2="35.56" y2="33.02" width="0.254" layer="94"/>
<wire x1="35.56" y1="33.02" x2="35.56" y2="-35.56" width="0.254" layer="94"/>
<text x="-33.02" y="35.56" size="1.778" layer="95">&gt;Name</text>
<text x="-33.02" y="-40.64" size="1.778" layer="95">&gt;Value</text>
<pin name="GND" x="40.64" y="-30.48" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LAN9512" prefix="IC">
<description>LAN9512&lt;br /&gt;
&lt;a href="http://www.hasseb.fi"&gt;www.hasseb.fi&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="LAN9512" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN64-9X9">
<connects>
<connect gate="G$1" pin="AUTOMDXI_EN" pad="41"/>
<connect gate="G$1" pin="CLK24_EN" pad="44"/>
<connect gate="G$1" pin="CLK24_OUT" pad="45"/>
<connect gate="G$1" pin="EECLK" pad="23"/>
<connect gate="G$1" pin="EECS" pad="24"/>
<connect gate="G$1" pin="EEDI" pad="26"/>
<connect gate="G$1" pin="EEDO" pad="25"/>
<connect gate="G$1" pin="EXRES" pad="50"/>
<connect gate="G$1" pin="GND" pad="EXP"/>
<connect gate="G$1" pin="GPIO3" pad="35"/>
<connect gate="G$1" pin="GPIO4" pad="36"/>
<connect gate="G$1" pin="GPIO5" pad="37"/>
<connect gate="G$1" pin="GPIO6" pad="42"/>
<connect gate="G$1" pin="GPIO7" pad="43"/>
<connect gate="G$1" pin="NC@1" pad="6"/>
<connect gate="G$1" pin="NC@2" pad="7"/>
<connect gate="G$1" pin="NC@3" pad="8"/>
<connect gate="G$1" pin="NC@4" pad="9"/>
<connect gate="G$1" pin="NC@5" pad="17"/>
<connect gate="G$1" pin="NC@6" pad="18"/>
<connect gate="G$1" pin="NFDX_LED/GPIO0" pad="20"/>
<connect gate="G$1" pin="NLNKA_LED/GPIO1" pad="21"/>
<connect gate="G$1" pin="NRESET" pad="12"/>
<connect gate="G$1" pin="NSPD_LED/GPIO2" pad="22"/>
<connect gate="G$1" pin="NTRST" pad="28"/>
<connect gate="G$1" pin="PRTCTL2" pad="14"/>
<connect gate="G$1" pin="PRTCTL3" pad="16"/>
<connect gate="G$1" pin="RXN" pad="53"/>
<connect gate="G$1" pin="RXP" pad="52"/>
<connect gate="G$1" pin="TCK" pad="32"/>
<connect gate="G$1" pin="TDI" pad="30"/>
<connect gate="G$1" pin="TDO" pad="31"/>
<connect gate="G$1" pin="TEST1" pad="13"/>
<connect gate="G$1" pin="TEST2" pad="34"/>
<connect gate="G$1" pin="TEST3" pad="40"/>
<connect gate="G$1" pin="TEST4" pad="47"/>
<connect gate="G$1" pin="TMS" pad="29"/>
<connect gate="G$1" pin="TXN" pad="56"/>
<connect gate="G$1" pin="TXP" pad="55"/>
<connect gate="G$1" pin="USBDM0" pad="58"/>
<connect gate="G$1" pin="USBDM2" pad="1"/>
<connect gate="G$1" pin="USBDM3" pad="3"/>
<connect gate="G$1" pin="USBDP0" pad="59"/>
<connect gate="G$1" pin="USBDP2" pad="2"/>
<connect gate="G$1" pin="USBDP3" pad="4"/>
<connect gate="G$1" pin="USBRBIAS" pad="63"/>
<connect gate="G$1" pin="VBUS_DET" pad="11"/>
<connect gate="G$1" pin="VDD18CORE@1" pad="15"/>
<connect gate="G$1" pin="VDD18CORE@2" pad="38"/>
<connect gate="G$1" pin="VDD18ETHPLL" pad="48"/>
<connect gate="G$1" pin="VDD18USBPLL" pad="62"/>
<connect gate="G$1" pin="VDD33A@1" pad="5"/>
<connect gate="G$1" pin="VDD33A@2" pad="10"/>
<connect gate="G$1" pin="VDD33A@3" pad="49"/>
<connect gate="G$1" pin="VDD33A@4" pad="51"/>
<connect gate="G$1" pin="VDD33A@5" pad="54"/>
<connect gate="G$1" pin="VDD33A@6" pad="57"/>
<connect gate="G$1" pin="VDD33A@7" pad="64"/>
<connect gate="G$1" pin="VDD33IO@1" pad="19"/>
<connect gate="G$1" pin="VDD33IO@2" pad="27"/>
<connect gate="G$1" pin="VDD33IO@3" pad="33"/>
<connect gate="G$1" pin="VDD33IO@4" pad="39"/>
<connect gate="G$1" pin="VDD33IO@5" pad="46"/>
<connect gate="G$1" pin="XI" pad="61"/>
<connect gate="G$1" pin="XO" pad="60"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microbuilder">
<description>&lt;h2&gt;&lt;b&gt;microBuilder.eu&lt;/b&gt; Eagle Footprint Library&lt;/h2&gt;

&lt;p&gt;Footprints for common components used in our projects and products.  This is the same library that we use internally, and it is regularly updated.  The newest version can always be found at &lt;b&gt;www.microBuilder.eu&lt;/b&gt;.  If you find this library useful, please feel free to purchase something from our online store. Please also note that all holes are optimised for metric drill bits!&lt;/p&gt;

&lt;h3&gt;Obligatory Warning&lt;/h3&gt;
&lt;p&gt;While it probably goes without saying, there are no guarantees that the footprints or schematic symbols in this library are flawless, and we make no promises of fitness for production, prototyping or any other purpose. These libraries are provided for information puposes only, and are used at your own discretion.  While we make every effort to produce accurate footprints, and many of the items found in this library have be proven in production, we can't make any promises of suitability for a specific purpose. If you do find any errors, though, please feel free to contact us at www.microbuilder.eu to let us know about it so that we can update the library accordingly!&lt;/p&gt;

&lt;h3&gt;License&lt;/h3&gt;
&lt;p&gt;This work is placed in the public domain, and may be freely used for commercial and non-commercial work with the following conditions:&lt;/p&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
&lt;/p&gt;</description>
<packages>
<package name="0603">
<description>0603 (1608 Metric)</description>
<wire x1="-1.473" y1="0.729" x2="1.473" y2="0.729" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.729" x2="1.473" y2="-0.729" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.729" x2="-1.473" y2="-0.729" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.729" x2="-1.473" y2="0.729" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.2032" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="1.778" y="-0.127" size="0.8128" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="1.778" y="-0.762" size="0.4064" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0805">
<description>0805 (2012 Metric)</description>
<wire x1="-1.873" y1="0.883" x2="1.873" y2="0.883" width="0.0508" layer="39"/>
<wire x1="1.873" y1="-0.883" x2="-1.873" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="-1.873" y1="-0.883" x2="-1.873" y2="0.883" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.873" y1="0.883" x2="1.873" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="1.8" y1="0.9" x2="1.8" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="-1.8" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="-1.8" y2="0.9" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="0.9" x2="1.8" y2="0.9" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="2.032" y="-0.127" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="2.032" y="-0.762" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="_0402">
<description>&lt;b&gt; 0402&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="-1.0573" y1="0.5557" x2="1.0573" y2="0.5557" width="0.2032" layer="21"/>
<wire x1="1.0573" y1="0.5557" x2="1.0573" y2="-0.5556" width="0.2032" layer="21"/>
<wire x1="1.0573" y1="-0.5556" x2="-1.0573" y2="-0.5557" width="0.2032" layer="21"/>
<wire x1="-1.0573" y1="-0.5557" x2="-1.0573" y2="0.5557" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.6" dy="0.6" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.6" dy="0.6" layer="1"/>
<text x="-0.9525" y="0.7939" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.9525" y="-1.3336" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.0794" y1="-0.2381" x2="0.0794" y2="0.2381" layer="35"/>
<rectangle x1="0.25" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
<rectangle x1="-0.5" y1="-0.25" x2="-0.25" y2="0.25" layer="51"/>
</package>
<package name="_0402MP">
<description>&lt;b&gt;0402 MicroPitch&lt;p&gt;</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="0" y1="0.127" x2="0" y2="-0.127" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<text x="-0.635" y="0.4763" size="0.6096" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.635" y="-0.7938" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.2" x2="0.1" y2="0.2" layer="35"/>
<rectangle x1="-0.5" y1="-0.25" x2="-0.254" y2="0.25" layer="51"/>
<rectangle x1="0.2588" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="_0603">
<description>&lt;b&gt;0603&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="-1.4605" y1="0.635" x2="1.4605" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.4605" y1="0.635" x2="1.4605" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.4605" y1="-0.635" x2="-1.4605" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.4605" y1="-0.635" x2="-1.4605" y2="0.635" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.9" dy="0.8" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.9" dy="0.8" layer="1"/>
<text x="-1.27" y="0.9525" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.4923" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8382" y2="0.4" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="_0603MP">
<description>&lt;b&gt;0603 MicroPitch&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="0" y1="0.254" x2="0" y2="-0.254" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.9525" y="0.635" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.9525" y="-0.9525" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<rectangle x1="-0.8" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.25" x2="0.1999" y2="0.25" layer="35"/>
</package>
<package name="_0805">
<description>&lt;b&gt;0805&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.585" x2="0.41" y2="0.585" width="0.1016" layer="51"/>
<wire x1="-0.41" y1="-0.585" x2="0.41" y2="-0.585" width="0.1016" layer="51"/>
<wire x1="-1.905" y1="0.889" x2="1.905" y2="0.889" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.889" x2="1.905" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-0.889" x2="-1.905" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-0.889" x2="-1.905" y2="0.889" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<text x="-1.5875" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.5874" y="-1.651" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1.0564" y2="0.65" layer="51"/>
<rectangle x1="-1.0668" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="_0805MP">
<description>&lt;b&gt;0805 MicroPitch&lt;/b&gt;</description>
<wire x1="-0.51" y1="0.535" x2="0.51" y2="0.535" width="0.1016" layer="51"/>
<wire x1="-0.51" y1="-0.535" x2="0.51" y2="-0.535" width="0.1016" layer="51"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<text x="-1.5875" y="0.9525" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.5875" y="-1.27" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1" y2="0.65" layer="51"/>
<rectangle x1="-1" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="FERRITE">
<text x="-1.27" y="1.905" size="1.27" layer="95">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="95">&gt;VALUE</text>
<pin name="P$1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="P$2" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="-1.27" y1="0.9525" x2="1.27" y2="0.9525" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0.9525" x2="1.27" y2="-0.9525" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-0.9525" x2="-1.27" y2="-0.9525" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-0.9525" x2="-1.27" y2="0.9525" width="0.4064" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FERRITE" prefix="FB" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;Ferrite Bead&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0603&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;MMZ1608B121C - 120 Ohm @ 100MHz, 600mA, 0.15 Ohm DC Resistance - Digikey: 445-2164-1-ND&lt;/li&gt;
&lt;li&gt;BK1608HW121-T - 120 Ohm, 600mA Ferrite Chip - Digikey: 587-1876-2-ND&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;0805&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;HZ0805B272R-10 - 2.7K Ohm @ 100MHz, 200mA, 0.8 Ohm DC Resistance - Digikey: 240-2504-1-ND (see also Murata BLM21BD272SN1L)&lt;/li&gt;
&lt;li&gt;MMZ2012Y152B - 1.5K Ohm @ 100MHz, 500mA, 0.4 Ohm DC Resistance - Digikey: 445-1560-1-ND - Mainly for high frequency (80-400MHz)&lt;/li&gt;
&lt;li&gt;MMZ2012R102A - 1K Ohm @ 100MHz, 500mA, 0.3 Ohm DC Resistance - Digikey: 445-1555-2-ND - More general purpose (10-200MHz)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="FERRITE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402" package="_0402">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402MP" package="_0402MP">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="_0603">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603MP" package="_0603MP">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="_0805">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805MP" package="_0805MP">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="crystals">
<packages>
<package name="HC49USTHROUGH">
<pad name="P1" x="-2.44" y="0" drill="0.49"/>
<pad name="P2" x="2.44" y="0" drill="0.49"/>
<wire x1="-5.75" y1="2.33" x2="5.75" y2="2.33" width="0.127" layer="21"/>
<wire x1="5.75" y1="2.33" x2="5.75" y2="-2.33" width="0.127" layer="21"/>
<wire x1="5.75" y1="-2.33" x2="-5.75" y2="-2.33" width="0.127" layer="21"/>
<wire x1="-5.75" y1="-2.33" x2="-5.75" y2="2.33" width="0.127" layer="21"/>
<text x="-6.35" y="2.54" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="CRYSTAL_2_HC49/US">
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<pin name="P1" x="-10.16" y="0" length="middle"/>
<pin name="P2" x="10.16" y="0" length="middle" rot="R180"/>
<text x="-5.08" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HC49USTHROUGH">
<gates>
<gate name="G$1" symbol="CRYSTAL_2_HC49/US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HC49USTHROUGH">
<connects>
<connect gate="G$1" pin="P1" pad="P1"/>
<connect gate="G$1" pin="P2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jumper">
<description>&lt;b&gt;Jumpers&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SJ">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="SJW">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.905" y1="-1.524" x2="-1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.524" x2="2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="1.27" x2="-1.905" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-1.27" x2="-1.905" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="1.905" y1="-1.524" x2="2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.159" y1="-1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-1.27" x2="-2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.524" x2="1.905" y2="1.524" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.524" y1="0" x2="2.032" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0" x2="-2.032" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.1524" layer="51" curve="-180"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="51" curve="180"/>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="-2.159" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="0.762" y1="-0.762" x2="1.016" y2="0.762" layer="51"/>
<rectangle x1="1.016" y1="-0.635" x2="1.27" y2="0.635" layer="51"/>
<rectangle x1="1.27" y1="-0.508" x2="1.397" y2="0.508" layer="51"/>
<rectangle x1="1.397" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.016" y1="-0.762" x2="-0.762" y2="0.762" layer="51"/>
<rectangle x1="-1.27" y1="-0.635" x2="-1.016" y2="0.635" layer="51"/>
<rectangle x1="-1.397" y1="-0.508" x2="-1.27" y2="0.508" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.397" y2="0.254" layer="51"/>
<rectangle x1="0.9652" y1="-0.7112" x2="1.0922" y2="-0.5842" layer="51"/>
<rectangle x1="1.3462" y1="-0.3556" x2="1.4732" y2="-0.2286" layer="51"/>
<rectangle x1="1.3462" y1="0.2032" x2="1.4732" y2="0.3302" layer="51"/>
<rectangle x1="0.9652" y1="0.5842" x2="1.0922" y2="0.7112" layer="51"/>
<rectangle x1="-1.0922" y1="-0.7112" x2="-0.9652" y2="-0.5842" layer="51"/>
<rectangle x1="-1.4478" y1="-0.3302" x2="-1.3208" y2="-0.2032" layer="51"/>
<rectangle x1="-1.4732" y1="0.2032" x2="-1.3462" y2="0.3302" layer="51"/>
<rectangle x1="-1.1176" y1="0.5842" x2="-0.9906" y2="0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="SJ">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SJ" prefix="SJ" uservalue="yes">
<description>SMD solder &lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="SJ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJ">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="W" package="SJW">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.2032" drill="0">
</class>
</classes>
<parts>
<part name="X1" library="con-jst" deviceset="?M06B-SRSS-TB" device="S"/>
<part name="D1" library="diode" deviceset="5KPXX" device=""/>
<part name="R5" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="3k"/>
<part name="P+1" library="supply1" deviceset="VCC" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="L1" library="SparkFun" deviceset="LED" device="1206" value="yellow"/>
<part name="L2" library="SparkFun" deviceset="LED" device="1206" value="green"/>
<part name="R8" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="330"/>
<part name="R9" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="330"/>
<part name="C6" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C7" library="resistor" deviceset="C-US" device="C1206" value="10uF"/>
<part name="GND6" library="SparkFun" deviceset="GND" device=""/>
<part name="U1" library="SparkFun" deviceset="FT232RL" device="SSOP" value="FT232R"/>
<part name="GND11" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND12" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R10" library="SparkFun-Resistors" deviceset="330OHM1/10W1%(0603)" device="" value="330"/>
<part name="R11" library="SparkFun-Resistors" deviceset="330OHM1/10W1%(0603)" device="" value="330"/>
<part name="L6" library="SparkFun-LED" deviceset="LED" device="1206" value="blue"/>
<part name="L7" library="SparkFun-LED" deviceset="LED" device="1206" value="yellow"/>
<part name="U$1" library="AiXWare" deviceset="AIXWARELOGO" device=""/>
<part name="J4" library="JST_AIX" deviceset="JSTPOS2VERSMT" device=""/>
<part name="GND10" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="U$2" library="Misc_AIX" deviceset="TECHMAKELOGO" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="P+8" library="supply1" deviceset="VCC" device=""/>
<part name="L5" library="SparkFun" deviceset="LED" device="1206" value="green"/>
<part name="F1" library="Misc_AIX" deviceset="FUS_AUTO" device=""/>
<part name="Z1" library="Aix_IC" deviceset="SP3222EUEY-L/TR" device=""/>
<part name="C8" library="resistor" deviceset="C-US" device="C0805K" value="0.1uF"/>
<part name="C9" library="resistor" deviceset="C-US" device="C0805K" value="0.1uF"/>
<part name="C10" library="resistor" deviceset="C-US" device="C0805K" value="0.1uF"/>
<part name="C11" library="resistor" deviceset="C-US" device="C0805K" value="0.1uF"/>
<part name="C12" library="resistor" deviceset="C-US" device="C0805K" value="0.1uF"/>
<part name="GND14" library="SparkFun" deviceset="GND" device=""/>
<part name="GND17" library="SparkFun" deviceset="GND" device=""/>
<part name="GND19" library="SparkFun" deviceset="GND" device=""/>
<part name="J2" library="Conector_AIX" deviceset="1437012" device=""/>
<part name="J8" library="Conector_AIX" deviceset="T4145435021-001" device=""/>
<part name="IC1" library="raspberrypi_cm" deviceset="LAN9512" device=""/>
<part name="GND3" library="SparkFun" deviceset="GND" device=""/>
<part name="U3" library="Aix_IC" deviceset="LP38693MP-3.3" device=""/>
<part name="FB1" library="microbuilder" deviceset="FERRITE" device="_0603" value="120ohm"/>
<part name="FB2" library="microbuilder" deviceset="FERRITE" device="_0603" value="120hm"/>
<part name="FB3" library="microbuilder" deviceset="FERRITE" device="_0603" value="120ohm"/>
<part name="U2" library="Aix_IC" deviceset="MIC2026-1YM-TR" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="C2" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="1uF"/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="C3" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C4" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C13" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="VDD33" library="supply1" deviceset="VDD" device=""/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="C15" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C16" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C17" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C18" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C19" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="+3V4" library="supply1" deviceset="+3V3" device=""/>
<part name="C20" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C21" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C22" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C23" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C24" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="4.7uF"/>
<part name="C25" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C26" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="+3V5" library="supply1" deviceset="+3V3" device=""/>
<part name="C27" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="1uF"/>
<part name="C28" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="R1" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="39.2k"/>
<part name="R2" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="61.9k"/>
<part name="C29" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="GND23" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R3" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="12k"/>
<part name="GND24" library="SparkFun" deviceset="GND" device=""/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="C30" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="1uF"/>
<part name="L4" library="SparkFun" deviceset="LED" device="1206" value="green"/>
<part name="L3" library="SparkFun" deviceset="LED" device="1206" value="green"/>
<part name="R4" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="511omhs"/>
<part name="R12" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="511ohms"/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="R13" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="12.4k"/>
<part name="GND28" library="SparkFun" deviceset="GND" device=""/>
<part name="R14" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="10k"/>
<part name="+3V6" library="supply1" deviceset="+3V3" device=""/>
<part name="GND29" library="SparkFun" deviceset="GND" device=""/>
<part name="R15" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="10k"/>
<part name="C31" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="0.01uF"/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="U4" library="crystals" deviceset="HC49USTHROUGH" device="" value="HC49US-25.000MABJ-UB"/>
<part name="C32" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="33pF"/>
<part name="C33" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="33pF"/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="R16" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="1M"/>
<part name="+3V7" library="supply1" deviceset="+3V3" device=""/>
<part name="VDD1" library="supply1" deviceset="VDD" device=""/>
<part name="VDD2" library="supply1" deviceset="VDD" device=""/>
<part name="VDD3" library="supply1" deviceset="VDD" device=""/>
<part name="J6" library="Conector_AIX" deviceset="53047-0410" device=""/>
<part name="J5" library="Conector_AIX" deviceset="53047-0410" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="GND32" library="SparkFun" deviceset="GND" device=""/>
<part name="L8" library="SparkFun" deviceset="LED" device="1206" value="green"/>
<part name="R6" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="330"/>
<part name="L9" library="SparkFun" deviceset="LED" device="1206" value="green"/>
<part name="R7" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="330"/>
<part name="L10" library="SparkFun" deviceset="LED" device="1206" value="yellow"/>
<part name="R17" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="330"/>
<part name="+3V8" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V9" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V10" library="supply1" deviceset="+3V3" device=""/>
<part name="C5" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="1uF"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="C34" library="resistor" deviceset="C-US" device="C1206" value="150uF"/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="C35" library="resistor" deviceset="C-US" device="C1206" value="150uF"/>
<part name="GND34" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND35" library="supply1" deviceset="GND" device=""/>
<part name="J1" library="JST_AIX" deviceset="JSTPOS2VERSMT" device=""/>
<part name="J7" library="Conector_AIX" deviceset="0533240260" device=""/>
<part name="J9" library="Conector_AIX" deviceset="0533240260" device=""/>
<part name="J3" library="Conector_AIX" deviceset="0353631060" device=""/>
<part name="SJ1" library="jumper" deviceset="SJ" device=""/>
<part name="SJ2" library="jumper" deviceset="SJ" device=""/>
<part name="C1" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="1uF"/>
<part name="GND36" library="supply1" deviceset="GND" device=""/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
<part name="J10" library="Conector_AIX" deviceset="0533240260" device=""/>
<part name="GND38" library="supply1" deviceset="GND" device=""/>
<part name="U$3" library="AiXWare" deviceset="EAGLEXLOGO" device=""/>
<part name="R18" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="49.9 ohms"/>
<part name="R19" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="49.9 ohms"/>
<part name="R20" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="49.9 ohms"/>
<part name="R21" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="49.9 ohms"/>
<part name="R22" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="825 ohms"/>
<part name="+3V11" library="supply1" deviceset="+3V3" device=""/>
<part name="U5" library="Aix_IC" deviceset="MIC29150-12WU-TR" device=""/>
<part name="C14" library="resistor" deviceset="C-US" device="C0805K" value="0.1uF"/>
<part name="C36" library="resistor" deviceset="C-US" device="C1206" value="10 uF"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="13.97" y="115.57" size="2.54" layer="97">GPS: AQUI ENTRA LA INFORMACION DEL GPS</text>
<wire x1="2.54" y1="187.96" x2="287.02" y2="187.96" width="0.1524" layer="97" style="longdash"/>
<wire x1="287.02" y1="187.96" x2="287.02" y2="2.54" width="0.1524" layer="97" style="longdash"/>
<wire x1="287.02" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="97" style="longdash"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="187.96" width="0.1524" layer="97" style="longdash"/>
<text x="398.78" y="10.16" size="1.778" layer="97">Note: The internal regulator can only source 50mA
when running at 3.3V. Keep this in mind when
attempting to power 3.3V devices off of the 3.3V pin. </text>
<wire x1="12.7" y1="175.26" x2="78.74" y2="175.26" width="0.1524" layer="97" style="longdash"/>
<wire x1="78.74" y1="175.26" x2="78.74" y2="132.08" width="0.1524" layer="97" style="longdash"/>
<wire x1="78.74" y1="132.08" x2="12.7" y2="132.08" width="0.1524" layer="97" style="longdash"/>
<wire x1="12.7" y1="132.08" x2="12.7" y2="175.26" width="0.1524" layer="97" style="longdash"/>
<wire x1="337.82" y1="78.74" x2="591.82" y2="78.74" width="0.1524" layer="97" style="longdash"/>
<wire x1="591.82" y1="78.74" x2="591.82" y2="5.08" width="0.1524" layer="97" style="longdash"/>
<wire x1="591.82" y1="5.08" x2="337.82" y2="5.08" width="0.1524" layer="97" style="longdash"/>
<wire x1="337.82" y1="5.08" x2="337.82" y2="78.74" width="0.1524" layer="97" style="longdash"/>
<text x="345.44" y="76.2" size="1.778" layer="97">FTDI</text>
<text x="14.224" y="171.958" size="1.778" layer="97">RS232</text>
<text x="340.36" y="45.72" size="1.778" layer="97">Entrada: Datos del USB (PC)
D+
D-</text>
<text x="558.8" y="43.18" size="1.778" layer="97">Salida: Datos hacia el Inercial
TX_INERTIAL
RX_INERTIAL</text>
<text x="83.82" y="236.22" size="2.54" layer="97">Inertial Connector</text>
<wire x1="5.08" y1="-12.7" x2="287.02" y2="-12.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="287.02" y1="-12.7" x2="287.02" y2="-294.64" width="0.1524" layer="97" style="longdash"/>
<wire x1="287.02" y1="-294.64" x2="5.08" y2="-294.64" width="0.1524" layer="97" style="longdash"/>
<wire x1="5.08" y1="-294.64" x2="5.08" y2="-12.7" width="0.1524" layer="97" style="longdash"/>
<text x="10.16" y="-27.94" size="2.54" layer="97">Ethernet to USB</text>
<text x="27.94" y="-48.26" size="2.54" layer="97">Filtering</text>
<text x="17.78" y="-86.36" size="2.54" layer="97">Filtering</text>
<text x="15.24" y="-35.56" size="2.54" layer="97">Power</text>
<wire x1="12.7" y1="-81.28" x2="109.22" y2="-81.28" width="0.1524" layer="97" style="longdash"/>
<wire x1="109.22" y1="-81.28" x2="109.22" y2="-139.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="109.22" y1="-139.7" x2="12.7" y2="-139.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="12.7" y1="-139.7" x2="12.7" y2="-81.28" width="0.1524" layer="97" style="longdash"/>
<wire x1="10.16" y1="-30.48" x2="109.22" y2="-30.48" width="0.1524" layer="97" style="longdash"/>
<wire x1="109.22" y1="-30.48" x2="109.22" y2="-73.66" width="0.1524" layer="97" style="longdash"/>
<wire x1="109.22" y1="-73.66" x2="10.16" y2="-73.66" width="0.1524" layer="97" style="longdash"/>
<wire x1="10.16" y1="-73.66" x2="10.16" y2="-30.48" width="0.1524" layer="97" style="longdash"/>
<text x="17.78" y="-226.06" size="2.54" layer="97">Power switching</text>
<text x="116.84" y="-226.06" size="2.54" layer="97">USB Downstream</text>
<text x="193.04" y="-226.06" size="2.54" layer="97">USB Upstream</text>
<text x="7.62" y="205.74" size="1.27" layer="97">Conector Energia hacia PC</text>
<text x="5.08" y="241.3" size="2.54" layer="97">Conector de energia</text>
<wire x1="12.7" y1="-149.86" x2="109.22" y2="-149.86" width="0.1524" layer="97" style="longdash"/>
<wire x1="109.22" y1="-149.86" x2="109.22" y2="-213.36" width="0.1524" layer="97" style="longdash"/>
<wire x1="109.22" y1="-213.36" x2="12.7" y2="-213.36" width="0.1524" layer="97" style="longdash"/>
<wire x1="12.7" y1="-213.36" x2="12.7" y2="-149.86" width="0.1524" layer="97" style="longdash"/>
<text x="17.78" y="-154.94" size="2.54" layer="97">Osc</text>
<wire x1="12.7" y1="-220.98" x2="109.22" y2="-220.98" width="0.1524" layer="97" style="longdash"/>
<wire x1="109.22" y1="-220.98" x2="109.22" y2="-279.4" width="0.1524" layer="97" style="longdash"/>
<wire x1="109.22" y1="-279.4" x2="12.7" y2="-279.4" width="0.1524" layer="97" style="longdash"/>
<wire x1="12.7" y1="-279.4" x2="12.7" y2="-220.98" width="0.1524" layer="97" style="longdash"/>
<wire x1="114.3" y1="-220.98" x2="114.3" y2="-279.4" width="0.1524" layer="97" style="longdash"/>
<wire x1="114.3" y1="-279.4" x2="185.42" y2="-279.4" width="0.1524" layer="97" style="longdash"/>
<wire x1="185.42" y1="-279.4" x2="185.42" y2="-220.98" width="0.1524" layer="97" style="longdash"/>
<wire x1="185.42" y1="-220.98" x2="114.3" y2="-220.98" width="0.1524" layer="97" style="longdash"/>
<wire x1="190.5" y1="-220.98" x2="190.5" y2="-279.4" width="0.1524" layer="97" style="longdash"/>
<wire x1="190.5" y1="-279.4" x2="274.32" y2="-279.4" width="0.1524" layer="97" style="longdash"/>
<wire x1="274.32" y1="-279.4" x2="274.32" y2="-220.98" width="0.1524" layer="97" style="longdash"/>
<wire x1="274.32" y1="-220.98" x2="190.5" y2="-220.98" width="0.1524" layer="97" style="longdash"/>
<wire x1="114.3" y1="-149.86" x2="114.3" y2="-213.36" width="0.1524" layer="97" style="longdash"/>
<wire x1="114.3" y1="-213.36" x2="162.56" y2="-213.36" width="0.1524" layer="97" style="longdash"/>
<wire x1="162.56" y1="-213.36" x2="162.56" y2="-149.86" width="0.1524" layer="97" style="longdash"/>
<wire x1="162.56" y1="-149.86" x2="114.3" y2="-149.86" width="0.1524" layer="97" style="longdash"/>
<text x="119.38" y="-154.94" size="2.54" layer="97">Ethernet Leds</text>
<wire x1="2.54" y1="271.78" x2="2.54" y2="193.04" width="0.1524" layer="97" style="longdash"/>
<wire x1="2.54" y1="193.04" x2="121.92" y2="190.5" width="0.1524" layer="97" style="longdash"/>
<wire x1="121.92" y1="190.5" x2="121.92" y2="271.78" width="0.1524" layer="97" style="longdash"/>
<wire x1="121.92" y1="271.78" x2="2.54" y2="271.78" width="0.1524" layer="97" style="longdash"/>
<text x="14.224" y="171.958" size="1.778" layer="97">RS232</text>
<text x="7.62" y="266.7" size="2.54" layer="97">Energy</text>
<wire x1="12.7" y1="121.92" x2="99.06" y2="121.92" width="0.1524" layer="97" style="longdash"/>
<wire x1="99.06" y1="121.92" x2="99.06" y2="73.66" width="0.1524" layer="97" style="longdash"/>
<wire x1="99.06" y1="73.66" x2="12.7" y2="73.66" width="0.1524" layer="97" style="longdash"/>
<wire x1="12.7" y1="73.66" x2="12.7" y2="121.92" width="0.1524" layer="97" style="longdash"/>
<text x="149.352" y="61.976" size="2.54" layer="97">Velodyne Connector</text>
<text x="66.802" y="47.752" size="1.778" layer="97">GPS Rx</text>
<text x="10.16" y="66.04" size="1.778" layer="97">Se conecta GPS Pulse de velodyne con PIn 6 (Cafe) de Inercial y se conecta GPS RX de velodyne con PIn 7 (Blanco) de Inercial</text>
<text x="84.074" y="92.964" size="1.778" layer="97">GPS Pulse</text>
<text x="79.502" y="101.092" size="1.778" layer="97">GPS Rx</text>
<text x="30.48" y="259.08" size="2.54" layer="97">Switch</text>
<text x="40.64" y="208.28" size="1.27" layer="97">Conector Led Boton</text>
</plain>
<instances>
<instance part="X1" gate="-1" x="40.64" y="93.98" rot="R180"/>
<instance part="X1" gate="-2" x="40.64" y="96.52" rot="R180"/>
<instance part="X1" gate="-3" x="40.64" y="99.06" rot="R180"/>
<instance part="X1" gate="-4" x="40.64" y="101.6" rot="R180"/>
<instance part="X1" gate="-5" x="40.64" y="104.14" rot="R180"/>
<instance part="X1" gate="-6" x="40.64" y="106.68" rot="R180"/>
<instance part="D1" gate="1" x="121.92" y="40.64" rot="R90"/>
<instance part="R5" gate="G$1" x="48.26" y="86.36" rot="R90"/>
<instance part="P+1" gate="VCC" x="53.34" y="241.3"/>
<instance part="GND1" gate="1" x="25.4" y="226.06"/>
<instance part="GND2" gate="1" x="33.02" y="200.66"/>
<instance part="GND4" gate="1" x="81.28" y="213.36" rot="R270"/>
<instance part="L1" gate="G$1" x="43.18" y="160.02" rot="R180"/>
<instance part="L2" gate="G$1" x="53.34" y="160.02" rot="R180"/>
<instance part="R8" gate="G$1" x="43.18" y="149.86" rot="R90"/>
<instance part="R9" gate="G$1" x="53.34" y="149.86" rot="R90"/>
<instance part="C6" gate="G$1" x="381" y="50.8"/>
<instance part="C7" gate="G$1" x="391.16" y="53.34"/>
<instance part="GND6" gate="1" x="398.78" y="25.4"/>
<instance part="U1" gate="G$1" x="416.56" y="48.26"/>
<instance part="GND11" gate="1" x="381" y="40.64"/>
<instance part="GND12" gate="1" x="391.16" y="40.64"/>
<instance part="R10" gate="G$1" x="447.04" y="60.96" rot="R90"/>
<instance part="R11" gate="G$1" x="457.2" y="60.96" rot="R90"/>
<instance part="L6" gate="G$1" x="447.04" y="50.8"/>
<instance part="L7" gate="G$1" x="457.2" y="50.8"/>
<instance part="U$1" gate="G$1" x="341.757" y="172.1612"/>
<instance part="J4" gate="G$1" x="337.82" y="-228.6" rot="R90"/>
<instance part="GND10" gate="1" x="340.36" y="-241.3"/>
<instance part="U$2" gate="G$1" x="381.254" y="169.672"/>
<instance part="GND13" gate="1" x="17.78" y="22.86"/>
<instance part="P+8" gate="VCC" x="15.24" y="48.26" rot="R90"/>
<instance part="L5" gate="G$1" x="58.42" y="81.28" rot="R90"/>
<instance part="F1" gate="G$1" x="33.02" y="58.42" rot="R90"/>
<instance part="Z1" gate="G$1" x="520.7" y="53.34"/>
<instance part="C8" gate="G$1" x="492.76" y="68.58"/>
<instance part="C9" gate="G$1" x="480.06" y="58.42"/>
<instance part="C10" gate="G$1" x="543.56" y="66.04" rot="R90"/>
<instance part="C11" gate="G$1" x="485.14" y="43.18"/>
<instance part="C12" gate="G$1" x="487.68" y="53.34"/>
<instance part="GND14" gate="1" x="472.44" y="53.34" rot="R270"/>
<instance part="GND17" gate="1" x="561.34" y="60.96" rot="R90"/>
<instance part="GND19" gate="1" x="502.92" y="71.12" rot="R270"/>
<instance part="J2" gate="G$1" x="162.56" y="48.26" rot="R180"/>
<instance part="J8" gate="G$1" x="17.78" y="231.14" rot="MR0"/>
<instance part="IC1" gate="G$1" x="193.04" y="-88.9" rot="R270"/>
<instance part="GND3" gate="1" x="162.56" y="-137.16"/>
<instance part="U3" gate="G$1" x="53.34" y="-45.72" rot="R270"/>
<instance part="FB1" gate="G$1" x="25.4" y="-116.84"/>
<instance part="FB2" gate="G$1" x="33.02" y="-185.42"/>
<instance part="FB3" gate="G$1" x="233.68" y="-246.38" rot="R270"/>
<instance part="U2" gate="G$1" x="48.26" y="-254"/>
<instance part="GND5" gate="1" x="60.96" y="-48.26"/>
<instance part="GND15" gate="1" x="50.8" y="-68.58"/>
<instance part="C2" gate="G$1" x="50.8" y="-60.96"/>
<instance part="+3V1" gate="G$1" x="17.78" y="-45.72"/>
<instance part="+3V2" gate="G$1" x="20.32" y="-111.76"/>
<instance part="C3" gate="G$1" x="30.48" y="-124.46"/>
<instance part="C4" gate="G$1" x="40.64" y="-124.46"/>
<instance part="C13" gate="G$1" x="50.8" y="-124.46"/>
<instance part="GND16" gate="1" x="38.1" y="-132.08"/>
<instance part="VDD33" gate="G$1" x="99.06" y="-114.3"/>
<instance part="+3V3" gate="G$1" x="17.78" y="-91.44" rot="R90"/>
<instance part="GND18" gate="1" x="20.32" y="-101.6"/>
<instance part="C15" gate="G$1" x="25.4" y="-96.52"/>
<instance part="C16" gate="G$1" x="35.56" y="-96.52"/>
<instance part="C17" gate="G$1" x="45.72" y="-96.52"/>
<instance part="C18" gate="G$1" x="55.88" y="-96.52"/>
<instance part="C19" gate="G$1" x="63.5" y="-96.52"/>
<instance part="+3V4" gate="G$1" x="127" y="-81.28"/>
<instance part="C20" gate="G$1" x="60.96" y="-124.46"/>
<instance part="C21" gate="G$1" x="71.12" y="-124.46"/>
<instance part="C22" gate="G$1" x="81.28" y="-124.46"/>
<instance part="C23" gate="G$1" x="91.44" y="-124.46"/>
<instance part="C24" gate="G$1" x="78.74" y="-96.52"/>
<instance part="C25" gate="G$1" x="86.36" y="-96.52"/>
<instance part="C26" gate="G$1" x="93.98" y="-96.52"/>
<instance part="GND21" gate="1" x="78.74" y="-101.6"/>
<instance part="+3V5" gate="G$1" x="177.8" y="-154.94" rot="R180"/>
<instance part="C27" gate="G$1" x="25.4" y="-193.04"/>
<instance part="C28" gate="G$1" x="40.64" y="-193.04"/>
<instance part="GND22" gate="1" x="33.02" y="-200.66"/>
<instance part="R1" gate="G$1" x="220.98" y="-248.92"/>
<instance part="R2" gate="G$1" x="220.98" y="-261.62" rot="R90"/>
<instance part="C29" gate="G$1" x="208.28" y="-261.62" rot="R180"/>
<instance part="GND23" gate="1" x="215.9" y="-271.78"/>
<instance part="R3" gate="G$1" x="172.72" y="-35.56" rot="R270"/>
<instance part="GND24" gate="1" x="172.72" y="-25.4" rot="R180"/>
<instance part="GND25" gate="1" x="101.6" y="-254" rot="R90"/>
<instance part="C30" gate="G$1" x="96.52" y="-243.84" rot="R90"/>
<instance part="L4" gate="G$1" x="66.04" y="-248.92" rot="R90"/>
<instance part="L3" gate="G$1" x="66.04" y="-256.54" rot="R90"/>
<instance part="R4" gate="G$1" x="78.74" y="-248.92"/>
<instance part="R12" gate="G$1" x="78.74" y="-256.54"/>
<instance part="GND26" gate="1" x="88.9" y="-248.92" rot="R90"/>
<instance part="GND27" gate="1" x="88.9" y="-256.54" rot="R90"/>
<instance part="R13" gate="G$1" x="208.28" y="-45.72" rot="R90"/>
<instance part="GND28" gate="1" x="223.52" y="-45.72" rot="R90"/>
<instance part="R14" gate="G$1" x="238.76" y="-88.9" rot="R180"/>
<instance part="+3V6" gate="G$1" x="251.46" y="-83.82"/>
<instance part="GND29" gate="1" x="246.38" y="-106.68" rot="R90"/>
<instance part="R15" gate="G$1" x="127" y="-93.98" rot="R270"/>
<instance part="C31" gate="G$1" x="116.84" y="-93.98"/>
<instance part="GND20" gate="1" x="116.84" y="-81.28" rot="R180"/>
<instance part="U4" gate="G$1" x="78.74" y="-185.42"/>
<instance part="C32" gate="G$1" x="66.04" y="-195.58" rot="R180"/>
<instance part="C33" gate="G$1" x="99.06" y="-195.58" rot="R180"/>
<instance part="GND30" gate="1" x="81.28" y="-205.74"/>
<instance part="R16" gate="G$1" x="78.74" y="-175.26"/>
<instance part="+3V7" gate="G$1" x="203.2" y="-134.62" rot="R180"/>
<instance part="VDD1" gate="G$1" x="132.08" y="-81.28" rot="R90"/>
<instance part="VDD2" gate="G$1" x="144.78" y="-93.98" rot="R90"/>
<instance part="VDD3" gate="G$1" x="238.76" y="-50.8" rot="R270"/>
<instance part="J6" gate="G$1" x="254" y="-254"/>
<instance part="J5" gate="G$1" x="162.56" y="-251.46"/>
<instance part="GND9" gate="1" x="149.86" y="-256.54"/>
<instance part="GND31" gate="1" x="241.3" y="-259.08"/>
<instance part="GND32" gate="1" x="243.84" y="-81.28" rot="R90"/>
<instance part="L8" gate="G$1" x="152.4" y="-187.96"/>
<instance part="R6" gate="G$1" x="152.4" y="-177.8" rot="R90"/>
<instance part="L9" gate="G$1" x="139.7" y="-187.96"/>
<instance part="R7" gate="G$1" x="139.7" y="-177.8" rot="R90"/>
<instance part="L10" gate="G$1" x="124.46" y="-187.96"/>
<instance part="R17" gate="G$1" x="124.46" y="-177.8" rot="R90"/>
<instance part="+3V8" gate="G$1" x="124.46" y="-167.64"/>
<instance part="+3V9" gate="G$1" x="139.7" y="-167.64"/>
<instance part="+3V10" gate="G$1" x="152.4" y="-167.64"/>
<instance part="C5" gate="G$1" x="241.3" y="-236.22" rot="R90"/>
<instance part="GND7" gate="1" x="149.86" y="-256.54"/>
<instance part="GND8" gate="1" x="251.46" y="-236.22" rot="R90"/>
<instance part="C34" gate="G$1" x="132.08" y="-248.92"/>
<instance part="GND33" gate="1" x="132.08" y="-259.08"/>
<instance part="C35" gate="G$1" x="314.96" y="66.04"/>
<instance part="GND34" gate="1" x="314.96" y="53.34"/>
<instance part="GND35" gate="1" x="68.58" y="81.28" rot="R90"/>
<instance part="J1" gate="G$1" x="337.82" y="-210.82" rot="R90"/>
<instance part="J7" gate="G$1" x="43.18" y="248.92" rot="R90"/>
<instance part="J9" gate="G$1" x="30.48" y="213.36" rot="R180"/>
<instance part="J3" gate="G$1" x="101.6" y="218.44"/>
<instance part="SJ1" gate="1" x="129.54" y="121.92" rot="R90"/>
<instance part="SJ2" gate="1" x="137.16" y="121.92" rot="R90"/>
<instance part="C1" gate="G$1" x="35.56" y="-58.42"/>
<instance part="GND36" gate="1" x="43.18" y="170.18" rot="R180"/>
<instance part="GND37" gate="1" x="53.34" y="170.18" rot="R180"/>
<instance part="J10" gate="G$1" x="55.88" y="215.9" rot="R180"/>
<instance part="GND38" gate="1" x="58.42" y="200.66"/>
<instance part="U$3" gate="G$1" x="403.86" y="170.18"/>
<instance part="R18" gate="G$1" x="223.52" y="-22.86" rot="R180"/>
<instance part="R19" gate="G$1" x="223.52" y="-27.94" rot="R180"/>
<instance part="R20" gate="G$1" x="223.52" y="-33.02" rot="R180"/>
<instance part="R21" gate="G$1" x="223.52" y="-38.1" rot="R180"/>
<instance part="R22" gate="G$1" x="215.9" y="-231.14"/>
<instance part="+3V11" gate="G$1" x="226.06" y="-226.06"/>
<instance part="U5" gate="G$1" x="93.98" y="55.88" rot="R270"/>
<instance part="C14" gate="G$1" x="76.2" y="40.64"/>
<instance part="C36" gate="G$1" x="109.22" y="40.64"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$2" class="0">
<segment>
<pinref part="F1" gate="G$1" pin="P2"/>
<wire x1="35.56" y1="48.26" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="IN"/>
<wire x1="91.44" y1="48.26" x2="76.2" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="76.2" y1="48.26" x2="35.56" y2="48.26" width="0.1524" layer="91"/>
<wire x1="76.2" y1="43.18" x2="76.2" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="48.26" y1="81.28" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="A"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="X1" gate="-3" pin="S"/>
<wire x1="43.18" y1="99.06" x2="58.42" y2="99.06" width="0.1524" layer="91"/>
<label x="58.42" y="99.06" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-5" pin="S"/>
<wire x1="43.18" y1="104.14" x2="58.42" y2="104.14" width="0.1524" layer="91"/>
<label x="58.42" y="104.14" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="33.02" y1="210.82" x2="33.02" y2="203.2" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$1" pin="P1"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="J3" gate="G$1" pin="P7"/>
<wire x1="83.82" y1="213.36" x2="99.06" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="401.32" y1="40.64" x2="398.78" y2="40.64" width="0.1524" layer="91"/>
<wire x1="398.78" y1="40.64" x2="398.78" y2="38.1" width="0.1524" layer="91"/>
<wire x1="398.78" y1="38.1" x2="401.32" y2="38.1" width="0.1524" layer="91"/>
<wire x1="398.78" y1="38.1" x2="398.78" y2="35.56" width="0.1524" layer="91"/>
<wire x1="398.78" y1="35.56" x2="401.32" y2="35.56" width="0.1524" layer="91"/>
<wire x1="401.32" y1="33.02" x2="398.78" y2="33.02" width="0.1524" layer="91"/>
<wire x1="398.78" y1="33.02" x2="398.78" y2="35.56" width="0.1524" layer="91"/>
<wire x1="401.32" y1="30.48" x2="398.78" y2="30.48" width="0.1524" layer="91"/>
<wire x1="398.78" y1="30.48" x2="398.78" y2="33.02" width="0.1524" layer="91"/>
<wire x1="398.78" y1="27.94" x2="398.78" y2="30.48" width="0.1524" layer="91"/>
<junction x="398.78" y="38.1"/>
<junction x="398.78" y="35.56"/>
<junction x="398.78" y="33.02"/>
<junction x="398.78" y="30.48"/>
<pinref part="U1" gate="G$1" pin="TEST"/>
<pinref part="U1" gate="G$1" pin="AGND"/>
<pinref part="U1" gate="G$1" pin="GND7"/>
<pinref part="U1" gate="G$1" pin="GND18"/>
<pinref part="U1" gate="G$1" pin="GND21"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="381" y1="48.26" x2="381" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="391.16" y1="43.18" x2="391.16" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="P2"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="340.36" y1="-236.22" x2="340.36" y2="-238.76" width="0.1524" layer="91"/>
<wire x1="340.36" y1="-236.22" x2="340.36" y2="-218.44" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="P2"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="480.06" y1="53.34" x2="474.98" y2="53.34" width="0.1524" layer="91"/>
<wire x1="485.14" y1="45.72" x2="480.06" y2="45.72" width="0.1524" layer="91"/>
<wire x1="480.06" y1="45.72" x2="480.06" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="GND"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="535.94" y1="60.96" x2="548.64" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="548.64" y1="60.96" x2="558.8" y2="60.96" width="0.1524" layer="91"/>
<wire x1="548.64" y1="66.04" x2="548.64" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="EN"/>
<wire x1="505.46" y1="66.04" x2="505.46" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND19" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="25.4" y1="228.6" x2="22.86" y2="228.6" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="P2-"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="162.56" y1="-134.62" x2="162.56" y2="-129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="60.96" y1="-45.72" x2="58.42" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="50.8" y1="-63.5" x2="50.8" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="50.8" y1="-66.04" x2="35.56" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-66.04" x2="35.56" y2="-60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="30.48" y1="-127" x2="38.1" y2="-127" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="38.1" y1="-127" x2="40.64" y2="-127" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-127" x2="50.8" y2="-127" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="38.1" y1="-129.54" x2="38.1" y2="-127" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="50.8" y1="-127" x2="60.96" y2="-127" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="60.96" y1="-127" x2="71.12" y2="-127" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="71.12" y1="-127" x2="81.28" y2="-127" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="81.28" y1="-127" x2="91.44" y2="-127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="63.5" y1="-99.06" x2="55.88" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="55.88" y1="-99.06" x2="45.72" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="45.72" y1="-99.06" x2="35.56" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="35.56" y1="-99.06" x2="25.4" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="25.4" y1="-99.06" x2="20.32" y2="-99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="93.98" y1="-99.06" x2="86.36" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="86.36" y1="-99.06" x2="78.74" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="GND21" gate="1" pin="GND"/>
<junction x="78.74" y="-99.06"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="40.64" y1="-195.58" x2="33.02" y2="-195.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-195.58" x2="33.02" y2="-198.12" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="33.02" y1="-195.58" x2="25.4" y2="-195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="208.28" y1="-266.7" x2="215.9" y2="-266.7" width="0.1524" layer="91"/>
<wire x1="215.9" y1="-266.7" x2="220.98" y2="-266.7" width="0.1524" layer="91"/>
<wire x1="215.9" y1="-269.24" x2="215.9" y2="-266.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="172.72" y1="-30.48" x2="172.72" y2="-27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="58.42" y1="-254" x2="99.06" y2="-254" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="99.06" y1="-243.84" x2="99.06" y2="-254" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="86.36" y1="-248.92" x2="83.82" y2="-248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="86.36" y1="-256.54" x2="83.82" y2="-256.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="208.28" y1="-40.64" x2="220.98" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="220.98" y1="-40.64" x2="220.98" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="TEST2"/>
<wire x1="231.14" y1="-106.68" x2="243.84" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="GND29" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="1"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="116.84" y1="-88.9" x2="116.84" y2="-83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="1"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="66.04" y1="-200.66" x2="81.28" y2="-200.66" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-200.66" x2="99.06" y2="-200.66" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-200.66" x2="81.28" y2="-203.2" width="0.1524" layer="91"/>
<pinref part="GND30" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="GND"/>
<wire x1="160.02" y1="-254" x2="149.86" y2="-254" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="GND7" gate="1" pin="GND"/>
<junction x="149.86" y="-254"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="GND"/>
<wire x1="251.46" y1="-256.54" x2="241.3" y2="-256.54" width="0.1524" layer="91"/>
<pinref part="GND31" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="CLK24_EN"/>
<wire x1="231.14" y1="-81.28" x2="241.3" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="GND32" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="243.84" y1="-236.22" x2="248.92" y2="-236.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="132.08" y1="-256.54" x2="132.08" y2="-254" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="314.96" y1="55.88" x2="314.96" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L5" gate="G$1" pin="C"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="66.04" y1="81.28" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="162.56" y="25.4" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="76.2" y1="25.4" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
<wire x1="93.98" y1="25.4" x2="109.22" y2="25.4" width="0.1524" layer="91"/>
<wire x1="109.22" y1="25.4" x2="121.92" y2="25.4" width="0.1524" layer="91"/>
<wire x1="121.92" y1="25.4" x2="162.56" y2="25.4" width="0.1524" layer="91"/>
<wire x1="162.56" y1="25.4" x2="162.56" y2="58.42" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="P8"/>
<wire x1="162.56" y1="58.42" x2="157.48" y2="58.42" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="17.78" y1="25.4" x2="76.2" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="GND"/>
<wire x1="93.98" y1="48.26" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="76.2" y1="35.56" x2="76.2" y2="25.4" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="109.22" y1="35.56" x2="109.22" y2="25.4" width="0.1524" layer="91"/>
<pinref part="D1" gate="1" pin="A"/>
<wire x1="121.92" y1="38.1" x2="121.92" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND36" gate="1" pin="GND"/>
<pinref part="L1" gate="G$1" pin="C"/>
<wire x1="43.18" y1="167.64" x2="43.18" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND37" gate="1" pin="GND"/>
<pinref part="L2" gate="G$1" pin="C"/>
<wire x1="53.34" y1="167.64" x2="53.34" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="P1"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="58.42" y1="213.36" x2="58.42" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TP2" class="0">
<segment>
<pinref part="X1" gate="-6" pin="S"/>
<wire x1="43.18" y1="106.68" x2="50.8" y2="106.68" width="0.1524" layer="91"/>
<label x="50.8" y="106.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ETHERNET_RX-" class="0">
<segment>
<label x="149.098" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J2" gate="G$1" pin="P1"/>
<wire x1="149.098" y1="40.64" x2="157.48" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="RXN"/>
<wire x1="200.66" y1="-50.8" x2="200.66" y2="-33.02" width="0.1524" layer="91"/>
<label x="200.66" y="-20.32" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="200.66" y1="-33.02" x2="200.66" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="200.66" y1="-33.02" x2="218.44" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ETHERNET_RX+" class="0">
<segment>
<label x="149.098" y="43.18" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="149.098" y1="43.18" x2="157.48" y2="43.18" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="P2"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="RXP"/>
<wire x1="203.2" y1="-50.8" x2="203.2" y2="-38.1" width="0.1524" layer="91"/>
<label x="203.2" y="-20.32" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="203.2" y1="-38.1" x2="203.2" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="203.2" y1="-38.1" x2="218.44" y2="-38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ETHERNET_TX-" class="0">
<segment>
<label x="149.098" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="149.098" y1="45.72" x2="157.48" y2="45.72" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="P3"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="TXN"/>
<wire x1="193.04" y1="-50.8" x2="193.04" y2="-22.86" width="0.1524" layer="91"/>
<label x="193.04" y="-20.32" size="1.27" layer="95" rot="R90" xref="yes"/>
<wire x1="193.04" y1="-22.86" x2="193.04" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="193.04" y1="-22.86" x2="218.44" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
</segment>
</net>
<net name="ETHERNET_TX+" class="0">
<segment>
<label x="149.098" y="48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J2" gate="G$1" pin="P4"/>
<wire x1="149.098" y1="48.26" x2="157.48" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="TXP"/>
<wire x1="195.58" y1="-50.8" x2="195.58" y2="-27.94" width="0.1524" layer="91"/>
<label x="195.58" y="-20.32" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="195.58" y1="-27.94" x2="195.58" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-27.94" x2="218.44" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="P+8" gate="VCC" pin="VCC"/>
<junction x="17.78" y="48.26"/>
<pinref part="F1" gate="G$1" pin="P1"/>
<wire x1="17.78" y1="48.26" x2="30.48" y2="48.26" width="0.1524" layer="91"/>
<wire x1="30.48" y1="50.8" x2="30.48" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="43.18" y1="233.68" x2="53.34" y2="233.68" width="0.1524" layer="91"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="53.34" y1="238.76" x2="53.34" y2="233.68" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="P2"/>
<wire x1="43.18" y1="233.68" x2="43.18" y2="246.38" width="0.1524" layer="91"/>
<wire x1="53.34" y1="233.68" x2="58.42" y2="233.68" width="0.1524" layer="91"/>
<wire x1="58.42" y1="233.68" x2="68.58" y2="233.68" width="0.1524" layer="91"/>
<wire x1="68.58" y1="233.68" x2="68.58" y2="210.82" width="0.1524" layer="91"/>
<wire x1="68.58" y1="210.82" x2="99.06" y2="210.82" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="P8"/>
<pinref part="J10" gate="G$1" pin="P2"/>
<wire x1="58.42" y1="215.9" x2="58.42" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS_OUT" class="0">
<segment>
<label x="91.44" y="220.98" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="P4"/>
<wire x1="91.44" y1="220.98" x2="99.06" y2="220.98" width="0.1524" layer="91"/>
<pinref part="SJ2" gate="1" pin="2"/>
<wire x1="99.06" y1="220.98" x2="137.16" y2="220.98" width="0.1524" layer="91"/>
<wire x1="137.16" y1="220.98" x2="137.16" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RTS" class="0">
<segment>
<wire x1="431.8" y1="55.88" x2="436.88" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RTS"/>
<label x="436.88" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="R2OUT"/>
<wire x1="505.46" y1="43.18" x2="505.46" y2="30.48" width="0.1524" layer="91"/>
<label x="505.46" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="CTS" class="0">
<segment>
<wire x1="431.8" y1="58.42" x2="436.88" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="CTS"/>
<label x="436.88" y="58.42" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="T2IN"/>
<wire x1="535.94" y1="45.72" x2="543.56" y2="45.72" width="0.1524" layer="91"/>
<label x="543.56" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="DTR" class="0">
<segment>
<wire x1="431.8" y1="53.34" x2="436.88" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="DTR"/>
<label x="436.88" y="53.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="DSR" class="0">
<segment>
<wire x1="431.8" y1="50.8" x2="436.88" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="DSR"/>
<label x="436.88" y="50.8" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="DCD" class="0">
<segment>
<wire x1="431.8" y1="48.26" x2="436.88" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="DCD"/>
<label x="436.88" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RI" class="0">
<segment>
<wire x1="431.8" y1="45.72" x2="436.88" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RI"/>
<label x="436.88" y="45.72" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TXLED" class="0">
<segment>
<wire x1="431.8" y1="40.64" x2="447.04" y2="40.64" width="0.1524" layer="91"/>
<wire x1="447.04" y1="40.64" x2="447.04" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="TXLED"/>
<pinref part="L6" gate="G$1" pin="C"/>
</segment>
</net>
<net name="RXLED" class="0">
<segment>
<wire x1="431.8" y1="38.1" x2="457.2" y2="38.1" width="0.1524" layer="91"/>
<wire x1="457.2" y1="38.1" x2="457.2" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RXLED"/>
<pinref part="L7" gate="G$1" pin="C"/>
</segment>
</net>
<net name="PWREN" class="0">
<segment>
<wire x1="431.8" y1="35.56" x2="436.88" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PWREN"/>
<label x="436.88" y="35.56" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TXDEN" class="0">
<segment>
<wire x1="431.8" y1="33.02" x2="436.88" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="TXDEN"/>
<label x="436.88" y="33.02" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SLEEP" class="0">
<segment>
<wire x1="431.8" y1="30.48" x2="436.88" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="SLEEP"/>
<label x="436.88" y="30.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="447.04" y1="55.88" x2="447.04" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="L6" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="457.2" y1="55.88" x2="457.2" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="L7" gate="G$1" pin="A"/>
</segment>
</net>
<net name="RX_FTDI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RXD"/>
<wire x1="431.8" y1="63.5" x2="436.88" y2="63.5" width="0.1524" layer="91"/>
<label x="434.34" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="R1OUT"/>
<wire x1="535.94" y1="53.34" x2="553.72" y2="53.34" width="0.1524" layer="91"/>
<label x="543.56" y="53.34" size="1.778" layer="95"/>
<label x="553.72" y="53.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TX_FTDI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="TXD"/>
<wire x1="431.8" y1="66.04" x2="436.88" y2="66.04" width="0.1524" layer="91"/>
<label x="434.34" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="T1IN"/>
<wire x1="535.94" y1="48.26" x2="543.56" y2="48.26" width="0.1524" layer="91"/>
<label x="543.56" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="C1+"/>
<wire x1="505.46" y1="63.5" x2="497.84" y2="63.5" width="0.1524" layer="91"/>
<wire x1="497.84" y1="63.5" x2="497.84" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="497.84" y1="71.12" x2="492.76" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="V+"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="505.46" y1="60.96" x2="480.06" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="C1-"/>
<wire x1="505.46" y1="58.42" x2="492.76" y2="58.42" width="0.1524" layer="91"/>
<wire x1="492.76" y1="58.42" x2="492.76" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="C2+"/>
<wire x1="505.46" y1="55.88" x2="487.68" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="C2-"/>
<wire x1="505.46" y1="53.34" x2="492.76" y2="53.34" width="0.1524" layer="91"/>
<wire x1="492.76" y1="53.34" x2="492.76" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="492.76" y1="48.26" x2="487.68" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="V-"/>
<wire x1="505.46" y1="50.8" x2="495.3" y2="50.8" width="0.1524" layer="91"/>
<wire x1="495.3" y1="50.8" x2="495.3" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="495.3" y1="38.1" x2="485.14" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="40.64" y1="233.68" x2="35.56" y2="233.68" width="0.1524" layer="91"/>
<wire x1="35.56" y1="233.68" x2="35.56" y2="213.36" width="0.1524" layer="91"/>
<wire x1="35.56" y1="213.36" x2="33.02" y2="213.36" width="0.1524" layer="91"/>
<wire x1="35.56" y1="233.68" x2="22.86" y2="233.68" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="P1+"/>
<pinref part="J7" gate="G$1" pin="P1"/>
<wire x1="40.64" y1="246.38" x2="40.64" y2="233.68" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$1" pin="P2"/>
</segment>
</net>
<net name="RX-INERTIAL" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="T1OUT"/>
<wire x1="535.94" y1="58.42" x2="596.9" y2="58.42" width="0.1524" layer="91"/>
<label x="543.56" y="58.42" size="1.778" layer="95"/>
<label x="596.9" y="58.42" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<label x="91.44" y="226.06" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="P2"/>
<wire x1="99.06" y1="226.06" x2="91.44" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX-INERTIAL" class="0">
<segment>
<label x="91.44" y="223.52" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="P3"/>
<wire x1="91.44" y1="223.52" x2="99.06" y2="223.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="R1IN"/>
<wire x1="535.94" y1="55.88" x2="596.9" y2="55.88" width="0.1524" layer="91"/>
<label x="543.56" y="55.88" size="1.778" layer="95"/>
<label x="596.9" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RTS-INERTIAL" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="R2IN"/>
<wire x1="505.46" y1="45.72" x2="500.38" y2="45.72" width="0.1524" layer="91"/>
<wire x1="500.38" y1="45.72" x2="500.38" y2="30.48" width="0.1524" layer="91"/>
<label x="500.38" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<label x="88.9" y="205.74" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="88.9" y1="205.74" x2="99.06" y2="205.74" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="P10"/>
</segment>
</net>
<net name="RS_IN_CTS-INERTIAL" class="0">
<segment>
<label x="91.44" y="218.44" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="P5"/>
<wire x1="99.06" y1="218.44" x2="91.44" y2="218.44" width="0.1524" layer="91"/>
<pinref part="SJ1" gate="1" pin="2"/>
<wire x1="99.06" y1="218.44" x2="129.54" y2="218.44" width="0.1524" layer="91"/>
<wire x1="129.54" y1="218.44" x2="129.54" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="T2OUT"/>
<wire x1="505.46" y1="48.26" x2="497.84" y2="48.26" width="0.1524" layer="91"/>
<wire x1="497.84" y1="48.26" x2="497.84" y2="22.86" width="0.1524" layer="91"/>
<label x="497.84" y="22.86" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="OUT"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="45.72" y1="-48.26" x2="35.56" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="35.56" y1="-48.26" x2="17.78" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-53.34" x2="35.56" y2="-48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="FB1" gate="G$1" pin="P$1"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="22.86" y1="-116.84" x2="20.32" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-116.84" x2="20.32" y2="-114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="C19" gate="G$1" pin="1"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="63.5" y1="-91.44" x2="55.88" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="55.88" y1="-91.44" x2="45.72" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="45.72" y1="-91.44" x2="35.56" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="35.56" y1="-91.44" x2="25.4" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-91.44" x2="20.32" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VDD33IO@1"/>
<wire x1="177.8" y1="-129.54" x2="177.8" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VDD33IO@2"/>
<wire x1="198.12" y1="-121.92" x2="177.8" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VDD33IO@3"/>
<wire x1="231.14" y1="-109.22" x2="198.12" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-109.22" x2="198.12" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VDD33IO@4"/>
<wire x1="198.12" y1="-121.92" x2="198.12" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-93.98" x2="198.12" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-93.98" x2="198.12" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VDD33IO@5"/>
<wire x1="231.14" y1="-76.2" x2="198.12" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-76.2" x2="198.12" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="177.8" y1="-152.4" x2="177.8" y2="-129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="251.46" y1="-86.36" x2="251.46" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="243.84" y1="-88.9" x2="251.46" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="TEST3"/>
<wire x1="231.14" y1="-91.44" x2="251.46" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="251.46" y1="-91.44" x2="251.46" y2="-88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<wire x1="127" y1="-88.9" x2="127" y2="-83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="NTRST"/>
<pinref part="IC1" gate="G$1" pin="TMS"/>
<wire x1="200.66" y1="-129.54" x2="203.2" y2="-129.54" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="TDI"/>
<wire x1="203.2" y1="-129.54" x2="205.74" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-129.54" x2="205.74" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="TCK"/>
<wire x1="205.74" y1="-121.92" x2="210.82" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-121.92" x2="210.82" y2="-129.54" width="0.1524" layer="91"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<wire x1="203.2" y1="-129.54" x2="203.2" y2="-132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="124.46" y1="-172.72" x2="124.46" y2="-170.18" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="139.7" y1="-172.72" x2="139.7" y2="-170.18" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="152.4" y1="-172.72" x2="152.4" y2="-170.18" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="220.98" y1="-231.14" x2="226.06" y2="-231.14" width="0.1524" layer="91"/>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
<wire x1="226.06" y1="-228.6" x2="226.06" y2="-231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="FB1" gate="G$1" pin="P$2"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="27.94" y1="-116.84" x2="30.48" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-116.84" x2="30.48" y2="-119.38" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="30.48" y1="-116.84" x2="40.64" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-116.84" x2="40.64" y2="-119.38" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="40.64" y1="-116.84" x2="50.8" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-116.84" x2="50.8" y2="-119.38" width="0.1524" layer="91"/>
<pinref part="VDD33" gate="G$1" pin="VDD"/>
<wire x1="50.8" y1="-116.84" x2="60.96" y2="-116.84" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="60.96" y1="-116.84" x2="71.12" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-116.84" x2="81.28" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-116.84" x2="91.44" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-116.84" x2="99.06" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-119.38" x2="60.96" y2="-116.84" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="71.12" y1="-119.38" x2="71.12" y2="-116.84" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="81.28" y1="-119.38" x2="81.28" y2="-116.84" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="91.44" y1="-119.38" x2="91.44" y2="-116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VDD33A@1"/>
<wire x1="152.4" y1="-81.28" x2="134.62" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="VDD1" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VDD33A@2"/>
<wire x1="152.4" y1="-93.98" x2="147.32" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="VDD2" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="VDD3" gate="G$1" pin="VDD"/>
<pinref part="IC1" gate="G$1" pin="VDD33A@3"/>
<wire x1="236.22" y1="-50.8" x2="231.14" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VDD33A@4"/>
<wire x1="231.14" y1="-50.8" x2="210.82" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-50.8" x2="205.74" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-58.42" x2="210.82" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-58.42" x2="210.82" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VDD33A@5"/>
<wire x1="198.12" y1="-50.8" x2="198.12" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-58.42" x2="205.74" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VDD33A@6"/>
<wire x1="190.5" y1="-50.8" x2="190.5" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-58.42" x2="198.12" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VDD33A@7"/>
<wire x1="172.72" y1="-50.8" x2="172.72" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-58.42" x2="190.5" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="228.6" y1="-38.1" x2="231.14" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-38.1" x2="231.14" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="228.6" y1="-33.02" x2="231.14" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-33.02" x2="231.14" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="228.6" y1="-27.94" x2="231.14" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-27.94" x2="231.14" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="228.6" y1="-22.86" x2="231.14" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-22.86" x2="231.14" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDD18CORE" class="0">
<segment>
<pinref part="C26" gate="G$1" pin="1"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-91.44" x2="86.36" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="86.36" y1="-91.44" x2="78.74" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-88.9" x2="71.12" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-91.44" x2="71.12" y2="-91.44" width="0.1524" layer="91"/>
<label x="71.12" y="-88.9" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VDD18CORE@1"/>
<wire x1="152.4" y1="-106.68" x2="142.24" y2="-106.68" width="0.1524" layer="91"/>
<label x="142.24" y="-106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VDD18CORE@2"/>
<wire x1="231.14" y1="-96.52" x2="233.68" y2="-96.52" width="0.1524" layer="91"/>
<label x="233.68" y="-96.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VDD18ETHPLL" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VDD18ETHPLL"/>
<wire x1="231.14" y1="-71.12" x2="233.68" y2="-71.12" width="0.1524" layer="91"/>
<label x="233.68" y="-71.12" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="FB2" gate="G$1" pin="P$2"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="35.56" y1="-185.42" x2="40.64" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-185.42" x2="40.64" y2="-187.96" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-185.42" x2="40.64" y2="-182.88" width="0.1524" layer="91"/>
<label x="40.64" y="-182.88" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="VDD18USBPLL" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VDD18USBPLL"/>
<wire x1="177.8" y1="-50.8" x2="177.8" y2="-43.18" width="0.1524" layer="91"/>
<label x="177.8" y="-43.18" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="FB2" gate="G$1" pin="P$1"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="30.48" y1="-185.42" x2="25.4" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-185.42" x2="25.4" y2="-187.96" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-185.42" x2="25.4" y2="-182.88" width="0.1524" layer="91"/>
<label x="25.4" y="-182.88" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="VBUS_DET" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VBUS_DET"/>
<wire x1="152.4" y1="-96.52" x2="144.78" y2="-96.52" width="0.1524" layer="91"/>
<label x="148.336" y="-94.488" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<label x="205.74" y="-231.14" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="210.82" y1="-231.14" x2="205.74" y2="-231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="USBRBIAS"/>
<wire x1="175.26" y1="-50.8" x2="175.26" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="175.26" y1="-40.64" x2="172.72" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PRTCTL2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PRTCTL2"/>
<wire x1="152.4" y1="-104.14" x2="132.08" y2="-104.14" width="0.1524" layer="91"/>
<label x="132.08" y="-104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="ENA"/>
<wire x1="38.1" y1="-248.92" x2="35.56" y2="-248.92" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="FLGA"/>
<wire x1="35.56" y1="-248.92" x2="33.02" y2="-248.92" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-251.46" x2="35.56" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-251.46" x2="35.56" y2="-248.92" width="0.1524" layer="91"/>
<label x="33.02" y="-248.92" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PRTCTL3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PRTCTL3"/>
<wire x1="152.4" y1="-109.22" x2="132.08" y2="-109.22" width="0.1524" layer="91"/>
<label x="132.08" y="-109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="FLGB"/>
<pinref part="U2" gate="G$1" pin="ENB"/>
<wire x1="38.1" y1="-254" x2="38.1" y2="-256.54" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-256.54" x2="33.02" y2="-256.54" width="0.1524" layer="91"/>
<label x="33.02" y="-256.54" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWR2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUTA"/>
<pinref part="L4" gate="G$1" pin="A"/>
<wire x1="58.42" y1="-248.92" x2="60.96" y2="-248.92" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-248.92" x2="63.5" y2="-248.92" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-248.92" x2="60.96" y2="-241.3" width="0.1524" layer="91"/>
<label x="60.96" y="-241.3" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="309.88" y="68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="314.96" y1="68.58" x2="309.88" y2="68.58" width="0.1524" layer="91"/>
<wire x1="381" y1="68.58" x2="391.16" y2="68.58" width="0.1524" layer="91"/>
<wire x1="391.16" y1="68.58" x2="408.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="408.94" y1="68.58" x2="408.94" y2="58.42" width="0.1524" layer="91"/>
<wire x1="408.94" y1="58.42" x2="401.32" y2="58.42" width="0.1524" layer="91"/>
<wire x1="391.16" y1="55.88" x2="391.16" y2="68.58" width="0.1524" layer="91"/>
<wire x1="381" y1="55.88" x2="381" y2="68.58" width="0.1524" layer="91"/>
<junction x="391.16" y="68.58"/>
<junction x="381" y="68.58"/>
<label x="396.24" y="58.42" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="VCC"/>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="408.94" y1="58.42" x2="408.94" y2="53.34" width="0.1524" layer="91" style="longdash"/>
<pinref part="U1" gate="G$1" pin="VCCIO"/>
<wire x1="408.94" y1="53.34" x2="401.32" y2="53.34" width="0.1524" layer="91" style="longdash"/>
<wire x1="314.96" y1="68.58" x2="381" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="447.04" y1="71.12" x2="447.04" y2="66.04" width="0.1524" layer="91"/>
<label x="447.04" y="71.12" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="457.2" y1="66.04" x2="457.2" y2="71.12" width="0.1524" layer="91"/>
<label x="457.2" y="71.12" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="SHDN"/>
<wire x1="535.94" y1="66.04" x2="535.94" y2="71.12" width="0.1524" layer="91"/>
<pinref part="Z1" gate="G$1" pin="VCC"/>
<wire x1="535.94" y1="63.5" x2="541.02" y2="63.5" width="0.1524" layer="91"/>
<wire x1="541.02" y1="63.5" x2="541.02" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="535.94" y1="63.5" x2="535.94" y2="66.04" width="0.1524" layer="91"/>
<label x="535.94" y="71.12" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="PWR3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUTB"/>
<pinref part="L3" gate="G$1" pin="A"/>
<wire x1="58.42" y1="-256.54" x2="60.96" y2="-256.54" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-256.54" x2="63.5" y2="-256.54" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-256.54" x2="60.96" y2="-261.62" width="0.1524" layer="91"/>
<label x="60.96" y="-261.62" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="VCC"/>
<label x="129.54" y="-246.38" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="160.02" y1="-246.38" x2="132.08" y2="-246.38" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-246.38" x2="129.54" y2="-246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="C"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="71.12" y1="-248.92" x2="73.66" y2="-248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="C"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="71.12" y1="-256.54" x2="73.66" y2="-256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="EXRES"/>
<pinref part="R13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="AUTOMDXI_EN"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="231.14" y1="-88.9" x2="233.68" y2="-88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="NRESET"/>
<wire x1="152.4" y1="-99.06" x2="127" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="127" y1="-99.06" x2="116.84" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-99.06" x2="116.84" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="XI" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="P1"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="68.58" y1="-185.42" x2="66.04" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-185.42" x2="66.04" y2="-193.04" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="73.66" y1="-175.26" x2="66.04" y2="-175.26" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-175.26" x2="66.04" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-175.26" x2="66.04" y2="-170.18" width="0.1524" layer="91"/>
<label x="66.04" y="-170.18" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="XI"/>
<wire x1="180.34" y1="-50.8" x2="180.34" y2="-43.18" width="0.1524" layer="91"/>
<label x="180.34" y="-43.18" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="XO" class="0">
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="99.06" y1="-193.04" x2="99.06" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="P2"/>
<wire x1="99.06" y1="-185.42" x2="88.9" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="83.82" y1="-175.26" x2="99.06" y2="-175.26" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-175.26" x2="99.06" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-175.26" x2="99.06" y2="-172.72" width="0.1524" layer="91"/>
<label x="99.06" y="-172.72" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="XO"/>
<wire x1="182.88" y1="-50.8" x2="182.88" y2="-43.18" width="0.1524" layer="91"/>
<label x="182.88" y="-43.18" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="USBDM2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="USBDM2"/>
<wire x1="152.4" y1="-71.12" x2="144.78" y2="-71.12" width="0.1524" layer="91"/>
<label x="144.78" y="-71.12" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="401.32" y1="66.04" x2="335.28" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="USBDM"/>
<label x="381" y="66.04" size="1.778" layer="95"/>
<label x="335.28" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="USBDP2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="USBDP2"/>
<wire x1="152.4" y1="-73.66" x2="144.78" y2="-73.66" width="0.1524" layer="91"/>
<label x="144.78" y="-73.66" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="401.32" y1="63.5" x2="335.28" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="USBDP"/>
<label x="381" y="63.5" size="1.778" layer="95"/>
<label x="335.28" y="63.5" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="USBDM3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="USBDM3"/>
<wire x1="152.4" y1="-76.2" x2="144.78" y2="-76.2" width="0.1524" layer="91"/>
<label x="144.78" y="-76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="D-"/>
<wire x1="160.02" y1="-248.92" x2="149.86" y2="-248.92" width="0.1524" layer="91"/>
<label x="149.86" y="-248.92" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="USBDP3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="USBDP3"/>
<wire x1="152.4" y1="-78.74" x2="144.78" y2="-78.74" width="0.1524" layer="91"/>
<label x="144.78" y="-78.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="D+"/>
<wire x1="160.02" y1="-251.46" x2="149.86" y2="-251.46" width="0.1524" layer="91"/>
<label x="149.86" y="-251.46" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="USBDM0" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="D-"/>
<wire x1="251.46" y1="-251.46" x2="241.3" y2="-251.46" width="0.1524" layer="91"/>
<label x="241.3" y="-251.46" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="USBDM0"/>
<wire x1="187.96" y1="-50.8" x2="187.96" y2="-43.18" width="0.1524" layer="91"/>
<label x="187.96" y="-43.18" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="USBDP0" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="D+"/>
<wire x1="251.46" y1="-254" x2="241.3" y2="-254" width="0.1524" layer="91"/>
<label x="241.3" y="-254" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="USBDP0"/>
<wire x1="185.42" y1="-50.8" x2="185.42" y2="-43.18" width="0.1524" layer="91"/>
<label x="185.42" y="-43.18" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="FDX_LED" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="NFDX_LED/GPIO0"/>
<wire x1="180.34" y1="-129.54" x2="180.34" y2="-137.16" width="0.1524" layer="91"/>
<label x="180.34" y="-137.16" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="152.4" y1="-193.04" x2="152.4" y2="-198.12" width="0.1524" layer="91"/>
<pinref part="L8" gate="G$1" pin="C"/>
<label x="152.4" y="-198.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="LNKA_LED" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="NLNKA_LED/GPIO1"/>
<wire x1="182.88" y1="-129.54" x2="182.88" y2="-137.16" width="0.1524" layer="91"/>
<label x="182.88" y="-137.16" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="139.7" y1="-193.04" x2="139.7" y2="-198.12" width="0.1524" layer="91"/>
<pinref part="L9" gate="G$1" pin="C"/>
<label x="139.7" y="-198.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="SPD_LED" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="NSPD_LED/GPIO2"/>
<wire x1="185.42" y1="-129.54" x2="185.42" y2="-137.16" width="0.1524" layer="91"/>
<label x="185.42" y="-137.16" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="124.46" y1="-193.04" x2="124.46" y2="-198.12" width="0.1524" layer="91"/>
<pinref part="L10" gate="G$1" pin="C"/>
<label x="124.46" y="-198.12" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="152.4" y1="-185.42" x2="152.4" y2="-182.88" width="0.1524" layer="91"/>
<pinref part="L8" gate="G$1" pin="A"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="139.7" y1="-185.42" x2="139.7" y2="-182.88" width="0.1524" layer="91"/>
<pinref part="L9" gate="G$1" pin="A"/>
<pinref part="R7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="124.46" y1="-185.42" x2="124.46" y2="-182.88" width="0.1524" layer="91"/>
<pinref part="L10" gate="G$1" pin="A"/>
<pinref part="R17" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="VCC"/>
<pinref part="FB3" gate="G$1" pin="P$2"/>
<wire x1="251.46" y1="-248.92" x2="233.68" y2="-248.92" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="226.06" y1="-248.92" x2="233.68" y2="-248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_USB" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="FB3" gate="G$1" pin="P$1"/>
<wire x1="236.22" y1="-236.22" x2="233.68" y2="-236.22" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-236.22" x2="233.68" y2="-243.84" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-236.22" x2="231.14" y2="-236.22" width="0.1524" layer="91"/>
<label x="231.14" y="-236.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="50.8" y1="-35.56" x2="50.8" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="EN"/>
<wire x1="50.8" y1="-43.18" x2="45.72" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="IN"/>
<wire x1="45.72" y1="-50.8" x2="50.8" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-50.8" x2="50.8" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="50.8" y1="-55.88" x2="50.8" y2="-50.8" width="0.1524" layer="91"/>
<label x="50.8" y="-35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-2" pin="S"/>
<wire x1="43.18" y1="96.52" x2="48.26" y2="96.52" width="0.1524" layer="91"/>
<label x="50.8" y="96.52" size="1.27" layer="95" xref="yes"/>
<wire x1="48.26" y1="96.52" x2="50.8" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="48.26" y1="96.52" x2="48.26" y2="91.44" width="0.1524" layer="91"/>
<junction x="48.26" y="96.52"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="P1"/>
<pinref part="J4" gate="G$1" pin="P1"/>
<wire x1="335.28" y1="-236.22" x2="335.28" y2="-218.44" width="0.1524" layer="91"/>
<wire x1="335.28" y1="-236.22" x2="335.28" y2="-243.84" width="0.1524" layer="91"/>
<label x="335.28" y="-243.84" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="IN"/>
<wire x1="58.42" y1="-251.46" x2="91.44" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-251.46" x2="91.44" y2="-243.84" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="91.44" y1="-243.84" x2="91.44" y2="-238.76" width="0.1524" layer="91"/>
<junction x="91.44" y="-243.84"/>
<label x="91.44" y="-238.76" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="GPS_RX" class="0">
<segment>
<pinref part="X1" gate="-4" pin="S"/>
<wire x1="43.18" y1="101.6" x2="68.58" y2="101.6" width="0.1524" layer="91"/>
<label x="68.58" y="101.6" size="1.27" layer="95" xref="yes"/>
<wire x1="68.58" y1="101.6" x2="68.58" y2="104.14" width="0.1524" layer="91"/>
<pinref part="SJ2" gate="1" pin="1"/>
<wire x1="68.58" y1="104.14" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<wire x1="137.16" y1="104.14" x2="137.16" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="157.48" y1="50.8" x2="148.844" y2="50.8" width="0.1524" layer="91"/>
<label x="148.844" y="50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J2" gate="G$1" pin="P5"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="53.34" y1="144.78" x2="53.34" y2="139.7" width="0.1524" layer="91"/>
<label x="53.34" y="139.7" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GPS_PULSE" class="0">
<segment>
<pinref part="X1" gate="-1" pin="S"/>
<wire x1="43.18" y1="93.98" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
<label x="60.96" y="93.98" size="1.27" layer="95" xref="yes"/>
<pinref part="SJ1" gate="1" pin="1"/>
<wire x1="129.54" y1="116.84" x2="129.54" y2="91.44" width="0.1524" layer="91"/>
<wire x1="129.54" y1="91.44" x2="60.96" y2="91.44" width="0.1524" layer="91"/>
<wire x1="60.96" y1="91.44" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="148.59" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J2" gate="G$1" pin="P6"/>
<wire x1="148.59" y1="53.34" x2="157.48" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="43.18" y1="144.78" x2="43.18" y2="139.7" width="0.1524" layer="91"/>
<label x="43.18" y="139.7" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="A"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="43.18" y1="157.48" x2="43.18" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="A"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="53.34" y1="157.48" x2="53.34" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBUS_DET2" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="208.28" y1="-259.08" x2="208.28" y2="-256.54" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="208.28" y1="-256.54" x2="215.9" y2="-256.54" width="0.1524" layer="91"/>
<wire x1="215.9" y1="-256.54" x2="220.98" y2="-256.54" width="0.1524" layer="91"/>
<wire x1="215.9" y1="-256.54" x2="215.9" y2="-248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OUT"/>
<wire x1="96.52" y1="48.26" x2="109.22" y2="48.26" width="0.1524" layer="91"/>
<wire x1="109.22" y1="48.26" x2="121.92" y2="48.26" width="0.1524" layer="91"/>
<wire x1="121.92" y1="48.26" x2="124.46" y2="48.26" width="0.1524" layer="91"/>
<wire x1="124.46" y1="48.26" x2="124.46" y2="55.88" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="P7"/>
<wire x1="124.46" y1="55.88" x2="157.48" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="109.22" y1="43.18" x2="109.22" y2="48.26" width="0.1524" layer="91"/>
<pinref part="D1" gate="1" pin="C"/>
<wire x1="121.92" y1="43.18" x2="121.92" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
