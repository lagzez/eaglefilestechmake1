<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="adafruit">
<packages>
<package name="1X2-3.5MM">
<wire x1="-3.4" y1="3.4" x2="-3.4" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="-3.4" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-3.6" x2="3.6" y2="-3.6" width="0.127" layer="21"/>
<wire x1="3.6" y1="-3.6" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<wire x1="3.6" y1="-2.2" x2="3.6" y2="3.4" width="0.127" layer="21"/>
<wire x1="3.6" y1="3.4" x2="-3.4" y2="3.4" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<pad name="1" x="1.8" y="0" drill="1" diameter="2.1844"/>
<pad name="2" x="-1.7" y="0" drill="1" diameter="2.1844"/>
<text x="3" y="5" size="1.27" layer="25" rot="R180">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="1X2">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="2.54" visible="pin" length="middle" direction="pas"/>
<pin name="2" x="-5.08" y="0" visible="pin" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1X2" prefix="J">
<description>3.5mm Terminal block
&lt;p&gt;http://www.ladyada.net/library/pcb/eaglelibrary.html&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="1X2" x="0" y="0"/>
</gates>
<devices>
<device name="-3.5MM" package="1X2-3.5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun">
<packages>
<package name="SOT23-6">
<wire x1="1.4224" y1="0.8104" x2="1.4224" y2="-0.8104" width="0.2032" layer="21"/>
<wire x1="-1.4224" y1="-0.8104" x2="-1.4224" y2="0.8104" width="0.2032" layer="21"/>
<wire x1="-1.3276" y1="-0.8104" x2="-1.4224" y2="-0.8104" width="0.2032" layer="21"/>
<wire x1="1.4224" y1="-0.8104" x2="1.3276" y2="-0.8104" width="0.2032" layer="21"/>
<wire x1="1.3276" y1="0.8104" x2="1.4224" y2="0.8104" width="0.2032" layer="21"/>
<wire x1="-1.4224" y1="0.8104" x2="-1.3276" y2="0.8104" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-0.5" x2="-0.5" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-0.5" x2="-0.5" y2="-0.79" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-0.5" x2="0.5" y2="-0.8" width="0.2032" layer="21"/>
<smd name="3" x="-0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="1" x="0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="6" x="0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="-0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="0" y="1.3001" dx="0.55" dy="1.2" layer="1" rot="R180"/>
<text x="-0.8255" y="2.159" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9525" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762"/>
<pad name="2" x="2.5" y="0" drill="0.762"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="EIA3216">
<wire x1="-1" y1="-1.2" x2="-2.5" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.2" x2="-2.5" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.2" x2="2.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-1.2" x2="2.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.8" x2="2.5" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.8" x2="2.1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="0.381" y1="1.016" x2="0.381" y2="-1.016" width="0.127" layer="21"/>
<smd name="C" x="-1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="A" x="1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<text x="-2.54" y="1.381" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.408" y="1.332" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="EIA3528">
<wire x1="-0.9" y1="-1.6" x2="-2.6" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-1.6" x2="-2.6" y2="1.55" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="1.55" x2="-0.9" y2="1.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.55" x2="2.2" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-1.55" x2="2.6" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-1.2" x2="2.6" y2="1.25" width="0.2032" layer="21"/>
<wire x1="2.6" y1="1.25" x2="2.2" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="0.609" y1="1.311" x2="0.609" y2="-1.286" width="0.2032" layer="21" style="longdash"/>
<smd name="C" x="-1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<smd name="A" x="1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<text x="-2.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.24" y="-1.37" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="CPOL-RADIAL-100UF-25V">
<wire x1="-0.635" y1="1.27" x2="-1.905" y2="1.27" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.25" width="0.2032" layer="21"/>
<pad name="2" x="-1.27" y="0" drill="0.7" diameter="1.651"/>
<pad name="1" x="1.27" y="0" drill="0.7" diameter="1.651" shape="square"/>
<text x="-1.905" y="-4.318" size="0.8128" layer="27">&gt;Value</text>
<text x="-0.762" y="1.651" size="0.4064" layer="25">&gt;Name</text>
</package>
<package name="CPOL-RADIAL-10UF-25V">
<wire x1="-0.762" y1="1.397" x2="-1.778" y2="1.397" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2.5" width="0.2032" layer="21"/>
<pad name="1" x="1.27" y="0" drill="0.7" diameter="1.651" shape="square"/>
<pad name="2" x="-1.27" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.889" y="1.524" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.905" y="-3.683" size="0.8128" layer="27">&gt;Value</text>
</package>
<package name="EIA7343">
<wire x1="-5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="-1.5" x2="4" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-2.5" x2="2" y2="-2.5" width="0.2032" layer="21"/>
<smd name="C" x="-3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<smd name="A" x="3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="PANASONIC_G">
<description>&lt;b&gt;Panasonic Aluminium Electrolytic Capacitor VS-Serie Package G&lt;/b&gt;</description>
<wire x1="-5.1" y1="5.1" x2="2.8" y2="5.1" width="0.1016" layer="51"/>
<wire x1="2.8" y1="5.1" x2="5.1" y2="2.8" width="0.1016" layer="51"/>
<wire x1="5.1" y1="2.8" x2="5.1" y2="-2.8" width="0.1016" layer="51"/>
<wire x1="5.1" y1="-2.8" x2="2.8" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="2.8" y1="-5.1" x2="-5.1" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="-5.1" y1="-5.1" x2="-5.1" y2="5.1" width="0.1016" layer="51"/>
<wire x1="-5.1" y1="1" x2="-5.1" y2="5.1" width="0.2032" layer="21"/>
<wire x1="-5.1" y1="5.1" x2="2.8" y2="5.1" width="0.2032" layer="21"/>
<wire x1="2.8" y1="5.1" x2="5.1" y2="2.8" width="0.2032" layer="21"/>
<wire x1="5.1" y1="2.8" x2="5.1" y2="1" width="0.2032" layer="21"/>
<wire x1="5.1" y1="-1" x2="5.1" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.1" y1="-2.8" x2="2.8" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-5.1" x2="-5.1" y2="-5.1" width="0.2032" layer="21"/>
<wire x1="-5.1" y1="-5.1" x2="-5.1" y2="-1" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="-1" x2="4.85" y2="-1" width="0.2032" layer="21" curve="156.699401" cap="flat"/>
<wire x1="-4.85" y1="1" x2="4.85" y2="1" width="0.2032" layer="21" curve="-156.699401" cap="flat"/>
<wire x1="-3.25" y1="3.7" x2="-3.25" y2="-3.65" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="4.95" width="0.1016" layer="51"/>
<smd name="-" x="-4.25" y="0" dx="3.9" dy="1.6" layer="1"/>
<smd name="+" x="4.25" y="0" dx="3.9" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-5.85" y1="-0.45" x2="-4.9" y2="0.45" layer="51"/>
<rectangle x1="4.9" y1="-0.45" x2="5.85" y2="0.45" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-3.3" y="3.6"/>
<vertex x="-4.05" y="2.75"/>
<vertex x="-4.65" y="1.55"/>
<vertex x="-4.85" y="0.45"/>
<vertex x="-4.85" y="-0.45"/>
<vertex x="-4.65" y="-1.55"/>
<vertex x="-4.05" y="-2.75"/>
<vertex x="-3.3" y="-3.6"/>
<vertex x="-3.3" y="3.55"/>
</polygon>
</package>
<package name="PANASONIC_E">
<description>&lt;b&gt;Panasonic Aluminium Electrolytic Capacitor VS-Serie Package E&lt;/b&gt;</description>
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="0.9" x2="-4.1" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.2032" layer="21"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.2032" layer="21"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="0.9" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-0.9" x2="4.1" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.85" y1="0.9" x2="3.85" y2="0.9" width="0.2032" layer="21" curve="-153.684915" cap="flat"/>
<wire x1="-3.85" y1="-0.9" x2="3.85" y2="-0.9" width="0.2032" layer="21" curve="153.684915" cap="flat"/>
<circle x="0" y="0" radius="3.95" width="0.1016" layer="51"/>
<smd name="-" x="-3" y="0" dx="3.8" dy="1.4" layer="1"/>
<smd name="+" x="3" y="0" dx="3.8" dy="1.4" layer="1"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5" y1="-0.35" x2="-3.8" y2="0.35" layer="51"/>
<rectangle x1="3.8" y1="-0.35" x2="4.5" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.25" y="3.2"/>
<vertex x="-3" y="2.5"/>
<vertex x="-3.6" y="1.5"/>
<vertex x="-3.85" y="0.65"/>
<vertex x="-3.85" y="-0.65"/>
<vertex x="-3.55" y="-1.6"/>
<vertex x="-2.95" y="-2.55"/>
<vertex x="-2.25" y="-3.2"/>
<vertex x="-2.25" y="3.15"/>
</polygon>
</package>
<package name="PANASONIC_C">
<description>&lt;b&gt;Panasonic Aluminium Electrolytic Capacitor VS-Serie Package E&lt;/b&gt;</description>
<wire x1="-2.6" y1="2.45" x2="1.6" y2="2.45" width="0.2032" layer="21"/>
<wire x1="1.6" y1="2.45" x2="2.7" y2="1.35" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.75" x2="1.6" y2="-2.85" width="0.2032" layer="21"/>
<wire x1="1.6" y1="-2.85" x2="-2.6" y2="-2.85" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="2.45" x2="1.6" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.6" y1="2.45" x2="2.7" y2="1.35" width="0.1016" layer="51"/>
<wire x1="2.7" y1="-1.75" x2="1.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="1.6" y1="-2.85" x2="-2.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="-2.6" y1="2.45" x2="-2.6" y2="0.35" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-2.85" x2="-2.6" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="2.7" y1="1.35" x2="2.7" y2="0.35" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.75" x2="2.7" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="2.45" x2="-2.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="2.7" y1="1.35" x2="2.7" y2="-1.75" width="0.1016" layer="51"/>
<wire x1="-2.4" y1="0.35" x2="2.45" y2="0.3" width="0.2032" layer="21" curve="-156.699401"/>
<wire x1="2.5" y1="-0.7" x2="-2.4" y2="-0.75" width="0.2032" layer="21" curve="-154.694887"/>
<circle x="0.05" y="-0.2" radius="2.5004" width="0.1016" layer="51"/>
<smd name="-" x="-1.8" y="-0.2" dx="2.2" dy="0.65" layer="1"/>
<smd name="+" x="1.9" y="-0.2" dx="2.2" dy="0.65" layer="1"/>
<text x="-2.6" y="2.7" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.6" y="-3.45" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="NIPPON_F80">
<wire x1="-3.3" y1="3.3" x2="1.7" y2="3.3" width="0.2032" layer="21"/>
<wire x1="1.7" y1="3.3" x2="3.3" y2="2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2" x2="1.7" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="1.7" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="1.7" y2="3.3" width="0.1016" layer="51"/>
<wire x1="1.7" y1="3.3" x2="3.3" y2="2" width="0.1016" layer="51"/>
<wire x1="3.3" y1="-2" x2="1.7" y2="-3.3" width="0.1016" layer="51"/>
<wire x1="1.7" y1="-3.3" x2="-3.3" y2="-3.3" width="0.1016" layer="51"/>
<wire x1="-3.3" y1="3.3" x2="-3.3" y2="0.685" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-0.685" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2" x2="3.3" y2="0.685" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2" x2="3.3" y2="-0.685" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="-3.3" y2="-3.3" width="0.1016" layer="51"/>
<wire x1="3.3" y1="2" x2="3.3" y2="-2" width="0.1016" layer="51"/>
<wire x1="-3.1" y1="0.685" x2="3.1" y2="0.685" width="0.2032" layer="21" curve="-156.500033"/>
<wire x1="3.1" y1="-0.685" x2="-3.1" y2="-0.685" width="0.2032" layer="21" curve="-154.748326"/>
<circle x="0" y="0" radius="3.15" width="0.1016" layer="51"/>
<smd name="-" x="-2.4" y="0" dx="2.95" dy="1" layer="1"/>
<smd name="+" x="2.4" y="0" dx="2.95" dy="1" layer="1"/>
<text x="-3.2" y="3.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.85" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="PANASONIC_D">
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="0.95" x2="-3.25" y2="3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="0.95" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-0.95" x2="3.25" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="2.95" y1="0.95" x2="-2.95" y2="0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="+" x="2.4" y="0" dx="3" dy="1.4" layer="1"/>
<smd name="-" x="-2.4" y="0" dx="3" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="CPOL-RADIAL-1000UF-63V">
<wire x1="-3.175" y1="1.905" x2="-4.445" y2="1.905" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="8.001" width="0.2032" layer="21"/>
<pad name="2" x="-3.81" y="0" drill="1.016" diameter="1.6764"/>
<pad name="1" x="3.81" y="0" drill="1.016" diameter="1.651" shape="square"/>
<text x="-2.54" y="8.89" size="0.8128" layer="27">&gt;Value</text>
<text x="-2.54" y="10.16" size="0.8128" layer="25">&gt;Name</text>
</package>
<package name="CPOL-RADIAL-1000UF-25V">
<wire x1="-1.905" y1="1.27" x2="-3.175" y2="1.27" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="5.461" width="0.2032" layer="21"/>
<pad name="2" x="-2.54" y="0" drill="0.7" diameter="1.651"/>
<pad name="1" x="2.54" y="0" drill="0.7" diameter="1.651" shape="square"/>
<text x="-1.905" y="-4.318" size="0.8128" layer="27">&gt;Value</text>
<text x="-0.762" y="2.921" size="0.4064" layer="25">&gt;Name</text>
</package>
<package name="VISHAY_C">
<wire x1="0" y1="1.27" x2="0" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.0574" y1="4.2926" x2="-2.0574" y2="-4.2926" width="0.127" layer="21"/>
<wire x1="-2.0574" y1="-4.2926" x2="2.0574" y2="-4.2926" width="0.127" layer="21"/>
<wire x1="2.0574" y1="-4.2926" x2="2.0574" y2="4.2926" width="0.127" layer="21"/>
<wire x1="2.0574" y1="4.2926" x2="-2.0574" y2="4.2926" width="0.127" layer="21"/>
<smd name="+" x="0" y="3.048" dx="3.556" dy="1.778" layer="1"/>
<smd name="-" x="0" y="-3.048" dx="3.556" dy="1.778" layer="1"/>
<text x="-1.905" y="4.445" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="PANASONIC_H13">
<wire x1="-6.75" y1="6.75" x2="4" y2="6.75" width="0.1016" layer="51"/>
<wire x1="4" y1="6.75" x2="6.75" y2="4" width="0.1016" layer="51"/>
<wire x1="6.75" y1="4" x2="6.75" y2="-4" width="0.1016" layer="51"/>
<wire x1="6.75" y1="-4" x2="4" y2="-6.75" width="0.1016" layer="51"/>
<wire x1="4" y1="-6.75" x2="-6.75" y2="-6.75" width="0.1016" layer="51"/>
<wire x1="-6.75" y1="-6.75" x2="-6.75" y2="6.75" width="0.1016" layer="51"/>
<wire x1="-6.75" y1="1" x2="-6.75" y2="6.75" width="0.2032" layer="21"/>
<wire x1="-6.75" y1="6.75" x2="4" y2="6.75" width="0.2032" layer="21"/>
<wire x1="4" y1="6.75" x2="6.75" y2="4" width="0.2032" layer="21"/>
<wire x1="6.75" y1="4" x2="6.75" y2="1" width="0.2032" layer="21"/>
<wire x1="6.75" y1="-1" x2="6.75" y2="-4" width="0.2032" layer="21"/>
<wire x1="6.75" y1="-4" x2="4" y2="-6.75" width="0.2032" layer="21"/>
<wire x1="4" y1="-6.75" x2="-6.75" y2="-6.75" width="0.2032" layer="21"/>
<wire x1="-6.75" y1="-6.75" x2="-6.75" y2="-1" width="0.2032" layer="21"/>
<wire x1="-6.55" y1="-1.2" x2="6.45" y2="-1.2" width="0.2032" layer="21" curve="156.692742" cap="flat"/>
<wire x1="-6.55" y1="1.2" x2="6.55" y2="1.2" width="0.2032" layer="21" curve="-156.697982" cap="flat"/>
<wire x1="-5" y1="4.25" x2="-4.95" y2="-4.35" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="6.6" width="0.1016" layer="51"/>
<smd name="-" x="-4.7" y="0" dx="5" dy="1.6" layer="1"/>
<smd name="+" x="4.7" y="0" dx="5" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-7.55" y1="-0.45" x2="-6.6" y2="0.45" layer="51"/>
<rectangle x1="6.6" y1="-0.45" x2="7.55" y2="0.45" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-5" y="4.2"/>
<vertex x="-5.75" y="3.15"/>
<vertex x="-6.25" y="2.05"/>
<vertex x="-6.55" y="0.45"/>
<vertex x="-6.55" y="-0.45"/>
<vertex x="-6.35" y="-1.65"/>
<vertex x="-5.75" y="-3.25"/>
<vertex x="-5" y="-4.2"/>
</polygon>
</package>
<package name="EIA6032">
<wire x1="3.2" y1="-1.6" x2="3.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-1.6" x2="3.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="3.2" y1="1.6" x2="-2.8" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.8" y1="1.6" x2="-3.4" y2="1" width="0.127" layer="21"/>
<wire x1="-3.4" y1="1" x2="-3.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-1.6" x2="-3.4" y2="-1" width="0.127" layer="21"/>
<smd name="P$1" x="-2.3" y="0" dx="1.5" dy="2.4" layer="1"/>
<smd name="P$2" x="2.3" y="0" dx="1.5" dy="2.4" layer="1"/>
</package>
<package name="SMA-DIODE">
<description>&lt;B&gt;Diode&lt;/B&gt;&lt;p&gt;
Basic SMA packaged diode. Good for reverse polarization protection. Common part #: MBRA140</description>
<wire x1="-2.3" y1="1" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2.15" y="0" dx="1.27" dy="1.47" layer="1" rot="R180"/>
<smd name="C" x="2.15" y="0" dx="1.27" dy="1.47" layer="1"/>
<text x="-2.286" y="1.651" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.254" y="1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="DIODE-1N4001">
<wire x1="3.175" y1="1.27" x2="1.905" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="1.27" x2="-3.175" y2="1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.254" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.254" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.175" y2="0" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.175" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.81" y2="0" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.81" y2="0" width="0.254" layer="21"/>
<pad name="A" x="-5.08" y="0" drill="1" diameter="1.9812"/>
<pad name="C" x="5.08" y="0" drill="1" diameter="1.9812"/>
<text x="-2.921" y="1.651" size="0.6096" layer="25">&gt;Name</text>
<text x="-2.921" y="-0.508" size="1.016" layer="21" ratio="12">&gt;Value</text>
</package>
<package name="SOD-323">
<wire x1="-0.9" y1="0.65" x2="-0.5" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.65" x2="0.9" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-0.9" y1="-0.65" x2="-0.5" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-0.65" x2="0.9" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.65" x2="-0.5" y2="-0.65" width="0.2032" layer="21"/>
<smd name="1" x="-1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<smd name="2" x="1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<text x="-0.889" y="1.016" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SOT23-3">
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.1" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1.1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="1" x="-0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<text x="-0.8255" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="DIODE-1N4148">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.2032" layer="21"/>
<pad name="A" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="C" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.032" y="-0.508" size="0.8128" layer="21">&gt;Value</text>
</package>
<package name="SMB-DIODE">
<description>&lt;b&gt;Diode&lt;/b&gt;&lt;p&gt;
Basic small signal diode good up to 200mA. SMB footprint. Common part #: BAS16</description>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.2606" y1="1.905" x2="2.2606" y2="1.905" width="0.1016" layer="21"/>
<wire x1="-2.2606" y1="-1.905" x2="2.2606" y2="-1.905" width="0.1016" layer="21"/>
<wire x1="-2.261" y1="-1.905" x2="-2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="2.261" y1="-1.905" x2="2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="0.643" y1="1" x2="-0.73" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="0" x2="0.643" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.643" y1="-1" x2="0.643" y2="1" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="1" x2="-0.73" y2="-1" width="0.2032" layer="21"/>
<smd name="C" x="-2.2" y="0" dx="2.4" dy="2.4" layer="1"/>
<smd name="A" x="2.2" y="0" dx="2.4" dy="2.4" layer="1"/>
<text x="-2.159" y="2.159" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.0922" x2="-2.2606" y2="1.0922" layer="51"/>
<rectangle x1="2.2606" y1="-1.0922" x2="2.794" y2="1.0922" layer="51"/>
</package>
<package name="DIODE-HV">
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.2606" y1="1.905" x2="2.2606" y2="1.905" width="0.1016" layer="21"/>
<wire x1="-2.2606" y1="-1.905" x2="2.2606" y2="-1.905" width="0.1016" layer="21"/>
<wire x1="-2.261" y1="-1.905" x2="-2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="2.261" y1="-1.905" x2="2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="0.643" y1="1" x2="-0.73" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="0" x2="0.643" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.643" y1="-1" x2="0.643" y2="1" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="1" x2="-0.73" y2="-1" width="0.2032" layer="21"/>
<smd name="C" x="-2.454" y="0" dx="2.2" dy="2.4" layer="1"/>
<smd name="A" x="2.454" y="0" dx="2.2" dy="2.4" layer="1"/>
<text x="-2.159" y="2.159" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.0922" x2="-2.2606" y2="1.0922" layer="51"/>
<rectangle x1="2.2606" y1="-1.0922" x2="2.794" y2="1.0922" layer="51"/>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-1206">
<wire x1="-1" y1="1" x2="-2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1" x2="1" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="0" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="0.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0.6" x2="-0.3" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="0" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="-0.889" y="1.397" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.778" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LED-0603">
<wire x1="0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="-0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="0" y1="0.17" x2="0.2338" y2="-0.14" width="0.2032" layer="21"/>
<wire x1="-0.0254" y1="0.1546" x2="-0.2184" y2="-0.14" width="0.2032" layer="21"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.6985" y="-0.889" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="1.0795" y="-1.016" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="LED10MM">
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7" shape="square"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
</package>
<package name="PTC">
<wire x1="-3.81" y1="1.524" x2="3.81" y2="1.524" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.524" x2="3.81" y2="-1.524" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.524" x2="-3.81" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.524" x2="-3.81" y2="1.524" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="0.8"/>
<pad name="P$2" x="2.54" y="0" drill="0.8"/>
</package>
<package name="PTC-1206">
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="0.635" y1="-0.762" x2="-0.635" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="0.635" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.143" y1="-1.016" x2="0.254" y2="1.016" width="0.127" layer="51"/>
<wire x1="0.254" y1="1.016" x2="1.143" y2="1.016" width="0.127" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1" dy="1.8" layer="1"/>
<text x="-1.524" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.524" y="-1.651" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="0603">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="SSOP28DB">
<description>&lt;b&gt;Small Shrink Outline Package&lt;/b&gt;</description>
<wire x1="-5.2" y1="2.925" x2="5.2" y2="2.925" width="0.1524" layer="21"/>
<wire x1="5.2" y1="2.925" x2="5.2" y2="-2.925" width="0.1524" layer="21"/>
<wire x1="5.2" y1="-2.925" x2="-5.2" y2="-2.925" width="0.1524" layer="21"/>
<wire x1="-5.2" y1="-2.925" x2="-5.2" y2="2.925" width="0.1524" layer="21"/>
<wire x1="-5.038" y1="2.763" x2="5.038" y2="2.763" width="0.0508" layer="27"/>
<wire x1="5.038" y1="2.763" x2="5.038" y2="-2.763" width="0.0508" layer="27"/>
<wire x1="5.038" y1="-2.763" x2="-5.038" y2="-2.763" width="0.0508" layer="27"/>
<wire x1="-5.038" y1="-2.763" x2="-5.038" y2="2.763" width="0.0508" layer="27"/>
<circle x="-4.225" y="-1.95" radius="0.4596" width="0.1524" layer="21"/>
<smd name="28" x="-4.225" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="27" x="-3.575" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="26" x="-2.925" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="25" x="-2.275" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="24" x="-1.625" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="23" x="-0.975" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="22" x="-0.325" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="20" x="0.975" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="21" x="0.325" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="19" x="1.625" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="18" x="2.275" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="17" x="2.925" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="16" x="3.575" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="15" x="4.225" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="1" x="-4.225" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="2" x="-3.575" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="3" x="-2.925" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="4" x="-2.275" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="5" x="-1.625" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="6" x="-0.975" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="7" x="-0.325" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="8" x="0.325" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="9" x="0.975" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="10" x="1.625" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="11" x="2.275" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="12" x="2.925" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="13" x="3.575" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="14" x="4.225" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<text x="-3.81" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="0" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-4.3875" y1="2.9656" x2="-4.0625" y2="3.9" layer="51"/>
<rectangle x1="-4.3875" y1="-3.9" x2="-4.0625" y2="-2.9656" layer="51"/>
<rectangle x1="-3.7375" y1="-3.9" x2="-3.4125" y2="-2.9656" layer="51"/>
<rectangle x1="-3.0875" y1="-3.9" x2="-2.7625" y2="-2.9656" layer="51"/>
<rectangle x1="-3.7375" y1="2.9656" x2="-3.4125" y2="3.9" layer="51"/>
<rectangle x1="-3.0875" y1="2.9656" x2="-2.7625" y2="3.9" layer="51"/>
<rectangle x1="-2.4375" y1="2.9656" x2="-2.1125" y2="3.9" layer="51"/>
<rectangle x1="-1.7875" y1="2.9656" x2="-1.4625" y2="3.9" layer="51"/>
<rectangle x1="-1.1375" y1="2.9656" x2="-0.8125" y2="3.9" layer="51"/>
<rectangle x1="-0.4875" y1="2.9656" x2="-0.1625" y2="3.9" layer="51"/>
<rectangle x1="0.1625" y1="2.9656" x2="0.4875" y2="3.9" layer="51"/>
<rectangle x1="0.8125" y1="2.9656" x2="1.1375" y2="3.9" layer="51"/>
<rectangle x1="1.4625" y1="2.9656" x2="1.7875" y2="3.9" layer="51"/>
<rectangle x1="2.1125" y1="2.9656" x2="2.4375" y2="3.9" layer="51"/>
<rectangle x1="2.7625" y1="2.9656" x2="3.0875" y2="3.9" layer="51"/>
<rectangle x1="3.4125" y1="2.9656" x2="3.7375" y2="3.9" layer="51"/>
<rectangle x1="4.0625" y1="2.9656" x2="4.3875" y2="3.9" layer="51"/>
<rectangle x1="-2.4375" y1="-3.9" x2="-2.1125" y2="-2.9656" layer="51"/>
<rectangle x1="-1.7875" y1="-3.9" x2="-1.4625" y2="-2.9656" layer="51"/>
<rectangle x1="-1.1375" y1="-3.9" x2="-0.8125" y2="-2.9656" layer="51"/>
<rectangle x1="-0.4875" y1="-3.9" x2="-0.1625" y2="-2.9656" layer="51"/>
<rectangle x1="0.1625" y1="-3.9" x2="0.4875" y2="-2.9656" layer="51"/>
<rectangle x1="0.8125" y1="-3.9" x2="1.1375" y2="-2.9656" layer="51"/>
<rectangle x1="1.4625" y1="-3.9" x2="1.7875" y2="-2.9656" layer="51"/>
<rectangle x1="2.1125" y1="-3.9" x2="2.4375" y2="-2.9656" layer="51"/>
<rectangle x1="2.7625" y1="-3.9" x2="3.0875" y2="-2.9656" layer="51"/>
<rectangle x1="3.4125" y1="-3.9" x2="3.7375" y2="-2.9656" layer="51"/>
<rectangle x1="4.0625" y1="-3.9" x2="4.3875" y2="-2.9656" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC2">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="XN04312">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-3.81" x2="0" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="1.778" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="1.27" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.286" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-4.318" x2="1.778" y2="-4.826" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-4.826" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="4.064" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="4.064" y1="-5.08" x2="7.62" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.1524" layer="94"/>
<wire x1="6.604" y1="0" x2="6.35" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-0.508" x2="6.096" y2="0.508" width="0.1524" layer="94"/>
<wire x1="6.096" y1="0.508" x2="5.842" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="5.842" y1="-0.508" x2="5.588" y2="0.508" width="0.1524" layer="94"/>
<wire x1="5.588" y1="0.508" x2="5.334" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="5.334" y1="-0.508" x2="5.08" y2="0.508" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.508" x2="4.826" y2="0" width="0.1524" layer="94"/>
<wire x1="4.826" y1="0" x2="4.064" y2="0" width="0.1524" layer="94"/>
<wire x1="4.064" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="4.064" y1="0" x2="4.064" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="4.064" y1="-1.524" x2="4.572" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="4.572" y1="-1.778" x2="3.556" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="3.556" y1="-2.032" x2="4.572" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="4.572" y1="-2.286" x2="3.556" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.556" y1="-2.54" x2="4.572" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="4.572" y1="-2.794" x2="3.556" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="3.556" y1="-3.048" x2="4.064" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="4.064" y1="-3.302" x2="4.064" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="7.62" y1="5.08" x2="2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="1.778" y1="3.81" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-2.032" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="1.27" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.524" y2="4.572" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="4.572" x2="-2.032" y2="4.064" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="4.064" x2="-1.27" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-4.064" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-4.064" y1="5.08" x2="-7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.1524" layer="94"/>
<wire x1="-6.604" y1="0" x2="-6.35" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="0.508" x2="-6.096" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-6.096" y1="-0.508" x2="-5.842" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-5.842" y1="0.508" x2="-5.588" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-5.588" y1="-0.508" x2="-5.334" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-5.334" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-0.508" x2="-4.826" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.826" y1="0" x2="-4.064" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.064" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-4.064" y1="0" x2="-4.064" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-4.064" y1="3.302" x2="-3.556" y2="3.048" width="0.1524" layer="94"/>
<wire x1="-3.556" y1="3.048" x2="-4.572" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-4.572" y1="2.794" x2="-3.556" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-3.556" y1="2.54" x2="-4.572" y2="2.286" width="0.1524" layer="94"/>
<wire x1="-4.572" y1="2.286" x2="-3.556" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-3.556" y1="2.032" x2="-4.572" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-4.572" y1="1.778" x2="-4.064" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-4.064" y1="3.302" x2="-4.064" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="3.81" width="0.1524" layer="94"/>
<text x="-7.62" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="E2" x="-10.16" y="5.08" visible="off" length="short"/>
<pin name="B2" x="-10.16" y="0" visible="off" length="short"/>
<pin name="C1" x="-10.16" y="-5.08" visible="off" length="short"/>
<pin name="E1" x="10.16" y="-5.08" visible="off" length="short" rot="R180"/>
<pin name="B1" x="10.16" y="0" visible="off" length="short" rot="R180"/>
<pin name="C2" x="10.16" y="5.08" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CAP_POL">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="PTC">
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="3.048" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.302" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="FT232R">
<wire x1="-10.16" y1="20.32" x2="10.16" y2="20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="20.32" x2="10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="-20.32" x2="-10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-20.32" x2="-10.16" y2="20.32" width="0.254" layer="94"/>
<text x="-7.62" y="20.828" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<pin name="RESET" x="-15.24" y="-5.08" length="middle" direction="in" function="dot"/>
<pin name="OSCI" x="-15.24" y="0" length="middle" direction="in"/>
<pin name="OSCO" x="-15.24" y="-2.54" length="middle" direction="out"/>
<pin name="DSR" x="15.24" y="2.54" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="DCD" x="15.24" y="0" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="RI" x="15.24" y="-2.54" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="3V3OUT" x="-15.24" y="7.62" length="middle" direction="out"/>
<pin name="USBDM" x="-15.24" y="17.78" length="middle"/>
<pin name="USBDP" x="-15.24" y="15.24" length="middle"/>
<pin name="GND7" x="-15.24" y="-12.7" length="middle" direction="pwr"/>
<pin name="GND18" x="-15.24" y="-15.24" length="middle" direction="pwr"/>
<pin name="GND21" x="-15.24" y="-17.78" length="middle" direction="pwr"/>
<pin name="TXD" x="15.24" y="17.78" length="middle" direction="out" rot="R180"/>
<pin name="RXD" x="15.24" y="15.24" length="middle" direction="in" rot="R180"/>
<pin name="VCCIO" x="-15.24" y="5.08" length="middle" direction="pwr"/>
<pin name="AGND" x="-15.24" y="-10.16" length="middle" direction="pwr"/>
<pin name="TEST" x="-15.24" y="-7.62" length="middle" direction="in"/>
<pin name="VCC" x="-15.24" y="10.16" length="middle" direction="pwr"/>
<pin name="TXLED" x="15.24" y="-7.62" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="RXLED" x="15.24" y="-10.16" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="RTS" x="15.24" y="7.62" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="CTS" x="15.24" y="10.16" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="DTR" x="15.24" y="5.08" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="PWREN" x="15.24" y="-12.7" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="TXDEN" x="15.24" y="-15.24" length="middle" direction="in" rot="R180"/>
<pin name="SLEEP" x="15.24" y="-17.78" length="middle" direction="in" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="VCC2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XN04312">
<description>&lt;b&gt;NPN/PNP Transistor Array&lt;/b&gt;
Handy dandy transistor array for level shifting and inverting signals. Spark Fun Electronics SKU : COM-00588</description>
<gates>
<gate name="G$1" symbol="XN04312" x="0" y="0"/>
</gates>
<devices>
<device name="SOT" package="SOT23-6">
<connects>
<connect gate="G$1" pin="B1" pad="5"/>
<connect gate="G$1" pin="B2" pad="2"/>
<connect gate="G$1" pin="C1" pad="1"/>
<connect gate="G$1" pin="C2" pad="4"/>
<connect gate="G$1" pin="E1" pad="6"/>
<connect gate="G$1" pin="E2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_POL" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor Polarized&lt;/b&gt;
These are standard SMD and PTH capacitors. Normally 10uF, 47uF, and 100uF in electrolytic and tantalum varieties. Always verify the external diameter of the through hole cap, it varies with capacity, voltage, and manufacturer. The EIA devices should be standard.</description>
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="EIA3216">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3528" package="EIA3528">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CPOL-RADIAL-100UF-25V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CPOL-RADIAL-10UF-25V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7343" package="EIA7343">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="G" package="PANASONIC_G">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E" package="PANASONIC_E">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="PANASONIC_C">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="F80" package="NIPPON_F80">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="PANASONIC_D">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CPOL-RADIAL-1000UF-63V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH4" package="CPOL-RADIAL-1000UF-25V">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="VISHAY_C">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="H13" package="PANASONIC_H13">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6032" package="EIA6032">
<connects>
<connect gate="G$1" pin="+" pad="P$1"/>
<connect gate="G$1" pin="-" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;b&gt;Diode&lt;/b&gt;
These are standard reverse protection diodes and small signal diodes. SMA package can handle up to about 1A. SOD-323 can handle about 200mA. What the SOD-323 package when ordering, there are some mfgs out there that are 5-pin packages.</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="SMA-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH" package="DIODE-1N4001">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1N4148" package="DIODE-1N4148">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="SMB-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HV" package="DIODE-HV">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LEDs&lt;/b&gt;
Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. 5mm - Spark Fun Electronics SKU : COM-00529 (and others)</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PTC" prefix="F">
<description>&lt;b&gt;Resettable Fuse PTC&lt;/b&gt;
Resettable Fuse. Spark Fun Electronics SKU : COM-08357</description>
<gates>
<gate name="G$1" symbol="PTC" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="PTC">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="PTC-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FT232RL" prefix="IC">
<description>&lt;b&gt;USB UART&lt;/b&gt;
FT232RL 4&lt;sup&gt;th&lt;/sup&gt; Generation USB UART (USB &amp;lt;-&amp;gt; Serial) Controller. Spark Fun Electronics SKU : COM-00650</description>
<gates>
<gate name="G$1" symbol="FT232R" x="0" y="0"/>
</gates>
<devices>
<device name="SSOP" package="SSOP28DB">
<connects>
<connect gate="G$1" pin="3V3OUT" pad="17"/>
<connect gate="G$1" pin="AGND" pad="25"/>
<connect gate="G$1" pin="CTS" pad="11"/>
<connect gate="G$1" pin="DCD" pad="10"/>
<connect gate="G$1" pin="DSR" pad="9"/>
<connect gate="G$1" pin="DTR" pad="2"/>
<connect gate="G$1" pin="GND18" pad="18"/>
<connect gate="G$1" pin="GND21" pad="21"/>
<connect gate="G$1" pin="GND7" pad="7"/>
<connect gate="G$1" pin="OSCI" pad="27"/>
<connect gate="G$1" pin="OSCO" pad="28"/>
<connect gate="G$1" pin="PWREN" pad="14"/>
<connect gate="G$1" pin="RESET" pad="19"/>
<connect gate="G$1" pin="RI" pad="6"/>
<connect gate="G$1" pin="RTS" pad="3"/>
<connect gate="G$1" pin="RXD" pad="5"/>
<connect gate="G$1" pin="RXLED" pad="22"/>
<connect gate="G$1" pin="SLEEP" pad="12"/>
<connect gate="G$1" pin="TEST" pad="26"/>
<connect gate="G$1" pin="TXD" pad="1"/>
<connect gate="G$1" pin="TXDEN" pad="13"/>
<connect gate="G$1" pin="TXLED" pad="23"/>
<connect gate="G$1" pin="USBDM" pad="16"/>
<connect gate="G$1" pin="USBDP" pad="15"/>
<connect gate="G$1" pin="VCC" pad="20"/>
<connect gate="G$1" pin="VCCIO" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="330OHM1/10W1%(0603)" prefix="R" uservalue="yes">
<description>RES-00818</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-00818"/>
<attribute name="VALUE" value="330" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find discrete LEDs for illumination or indication, but no displays.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED-0603">
<wire x1="0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="-0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="0" y1="0.17" x2="0.2338" y2="-0.14" width="0.2032" layer="21"/>
<wire x1="-0.0254" y1="0.1546" x2="-0.2184" y2="-0.14" width="0.2032" layer="21"/>
<smd name="C" x="0" y="0.877" dx="1" dy="1" layer="1" roundness="30"/>
<smd name="A" x="0" y="-0.877" dx="1" dy="1" layer="1" roundness="30"/>
<text x="-0.6985" y="-0.889" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="1.0795" y="-1.016" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="LED-1206">
<wire x1="-1" y1="1" x2="-2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1" x2="1" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="0" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="0.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0.6" x2="-0.3" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="0" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="-0.889" y="1.397" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.778" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LED-1206-BOTTOM">
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<wire x1="1.016" y1="1.016" x2="2.7686" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.7686" y1="-1.016" x2="1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="3.175" y1="0" x2="3.3528" y2="0" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-1.016" x2="-2.7686" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.7686" y1="-1.016" x2="-2.7686" y2="1.016" width="0.127" layer="21"/>
<wire x1="-2.7686" y1="1.016" x2="-1.016" y2="1.016" width="0.127" layer="21"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.175" y1="0" x2="-3.81" y2="0" width="0.254" layer="21"/>
<wire x1="-3.48741875" y1="-0.368296875" x2="-3.48741875" y2="0.3556" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.81" y2="0" width="0.254" layer="21"/>
<wire x1="-3.489959375" y1="0.37591875" x2="-3.48741875" y2="0.373378125" width="0.254" layer="21"/>
<wire x1="-3.48741875" y1="0.373378125" x2="-3.48741875" y2="-0.370840625" width="0.254" layer="21"/>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED10MM">
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7" shape="square"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
</package>
<package name="FKIT-LED-1206">
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="0.5" x2="-0.55" y2="0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LED3MM-NS">
<description>&lt;h3&gt;LED 3MM - No Silk&lt;/h3&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM-KIT">
<description>&lt;h3&gt;LED5MM-KIT&lt;/h3&gt;
5MM Through-hole LED&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D" uservalue="yes">
<description>&lt;b&gt;LEDs&lt;/b&gt;
Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. 5mm - Spark Fun Electronics SKU : COM-00529 (and others)</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08794" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.027940625" x2="0" y2="-0.027940625" width="0.381" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="EIA3216">
<wire x1="-1" y1="-1.2" x2="-2.5" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.2" x2="-2.5" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.2" x2="2.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-1.2" x2="2.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.8" x2="2.5" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.8" x2="2.1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="0.381" y1="1.016" x2="0.381" y2="-1.016" width="0.127" layer="21"/>
<smd name="C" x="-1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="A" x="1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<text x="-2.54" y="1.381" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.408" y="1.332" size="0.4064" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="CAP_POL">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0.1UF-25V(+80/-20%)(0603)" prefix="C" uservalue="yes">
<description>CAP-00810&lt;br&gt;
Ceramic&lt;br&gt;
Standard decoupling cap</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00810"/>
<attribute name="VALUE" value="0.1uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="10UF-16V-10%(TANT)" prefix="C" uservalue="yes">
<description>CAP-00811&lt;BR&gt;
1206/EIA-3216 Tantalum capacitor</description>
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EIA3216">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00811"/>
<attribute name="VALUE" value="10uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AiXWare">
<packages>
<package name="AIXLOGO">
<rectangle x1="11.02995" y1="3.04165" x2="11.05535" y2="3.05435" layer="21"/>
<rectangle x1="11.00455" y1="3.05435" x2="11.08075" y2="3.06705" layer="21"/>
<rectangle x1="10.97915" y1="3.06705" x2="11.10615" y2="3.07975" layer="21"/>
<rectangle x1="10.95375" y1="3.07975" x2="11.13155" y2="3.09245" layer="21"/>
<rectangle x1="10.92835" y1="3.09245" x2="11.15695" y2="3.10515" layer="21"/>
<rectangle x1="10.90295" y1="3.10515" x2="11.18235" y2="3.11785" layer="21"/>
<rectangle x1="10.87755" y1="3.11785" x2="11.22045" y2="3.13055" layer="21"/>
<rectangle x1="10.85215" y1="3.13055" x2="11.24585" y2="3.14325" layer="21"/>
<rectangle x1="10.82675" y1="3.14325" x2="11.25855" y2="3.15595" layer="21"/>
<rectangle x1="10.80135" y1="3.15595" x2="11.28395" y2="3.16865" layer="21"/>
<rectangle x1="10.77595" y1="3.16865" x2="11.30935" y2="3.18135" layer="21"/>
<rectangle x1="10.75055" y1="3.18135" x2="11.33475" y2="3.19405" layer="21"/>
<rectangle x1="10.72515" y1="3.19405" x2="11.36015" y2="3.20675" layer="21"/>
<rectangle x1="10.69975" y1="3.20675" x2="11.38555" y2="3.21945" layer="21"/>
<rectangle x1="10.67435" y1="3.21945" x2="11.41095" y2="3.23215" layer="21"/>
<rectangle x1="10.64895" y1="3.23215" x2="11.43635" y2="3.24485" layer="21"/>
<rectangle x1="10.62355" y1="3.24485" x2="11.46175" y2="3.25755" layer="21"/>
<rectangle x1="10.59815" y1="3.25755" x2="11.48715" y2="3.27025" layer="21"/>
<rectangle x1="10.57275" y1="3.27025" x2="11.52525" y2="3.28295" layer="21"/>
<rectangle x1="10.54735" y1="3.28295" x2="11.53795" y2="3.29565" layer="21"/>
<rectangle x1="10.52195" y1="3.29565" x2="11.56335" y2="3.30835" layer="21"/>
<rectangle x1="10.49655" y1="3.30835" x2="11.58875" y2="3.32105" layer="21"/>
<rectangle x1="10.47115" y1="3.32105" x2="11.61415" y2="3.33375" layer="21"/>
<rectangle x1="10.44575" y1="3.33375" x2="11.63955" y2="3.34645" layer="21"/>
<rectangle x1="10.42035" y1="3.34645" x2="11.66495" y2="3.35915" layer="21"/>
<rectangle x1="10.39495" y1="3.35915" x2="11.69035" y2="3.37185" layer="21"/>
<rectangle x1="10.36955" y1="3.37185" x2="11.71575" y2="3.38455" layer="21"/>
<rectangle x1="10.34415" y1="3.38455" x2="11.74115" y2="3.39725" layer="21"/>
<rectangle x1="10.31875" y1="3.39725" x2="11.76655" y2="3.40995" layer="21"/>
<rectangle x1="10.30605" y1="3.40995" x2="11.79195" y2="3.42265" layer="21"/>
<rectangle x1="10.28065" y1="3.42265" x2="11.81735" y2="3.43535" layer="21"/>
<rectangle x1="10.25525" y1="3.43535" x2="11.84275" y2="3.44805" layer="21"/>
<rectangle x1="10.22985" y1="3.44805" x2="11.86815" y2="3.46075" layer="21"/>
<rectangle x1="10.19175" y1="3.46075" x2="11.89355" y2="3.47345" layer="21"/>
<rectangle x1="10.16635" y1="3.47345" x2="11.91895" y2="3.48615" layer="21"/>
<rectangle x1="10.15365" y1="3.48615" x2="11.94435" y2="3.49885" layer="21"/>
<rectangle x1="10.12825" y1="3.49885" x2="11.96975" y2="3.51155" layer="21"/>
<rectangle x1="10.10285" y1="3.51155" x2="11.99515" y2="3.52425" layer="21"/>
<rectangle x1="10.07745" y1="3.52425" x2="12.02055" y2="3.53695" layer="21"/>
<rectangle x1="10.05205" y1="3.53695" x2="12.04595" y2="3.54965" layer="21"/>
<rectangle x1="10.02665" y1="3.54965" x2="12.07135" y2="3.56235" layer="21"/>
<rectangle x1="10.00125" y1="3.56235" x2="12.09675" y2="3.57505" layer="21"/>
<rectangle x1="9.97585" y1="3.57505" x2="12.12215" y2="3.58775" layer="21"/>
<rectangle x1="9.95045" y1="3.58775" x2="12.14755" y2="3.60045" layer="21"/>
<rectangle x1="9.92505" y1="3.60045" x2="12.17295" y2="3.61315" layer="21"/>
<rectangle x1="9.89965" y1="3.61315" x2="12.19835" y2="3.62585" layer="21"/>
<rectangle x1="9.87425" y1="3.62585" x2="12.22375" y2="3.63855" layer="21"/>
<rectangle x1="9.84885" y1="3.63855" x2="12.24915" y2="3.65125" layer="21"/>
<rectangle x1="9.82345" y1="3.65125" x2="12.27455" y2="3.66395" layer="21"/>
<rectangle x1="9.79805" y1="3.66395" x2="12.29995" y2="3.67665" layer="21"/>
<rectangle x1="9.77265" y1="3.67665" x2="12.32535" y2="3.68935" layer="21"/>
<rectangle x1="9.74725" y1="3.68935" x2="12.35075" y2="3.70205" layer="21"/>
<rectangle x1="9.72185" y1="3.70205" x2="12.37615" y2="3.71475" layer="21"/>
<rectangle x1="9.69645" y1="3.71475" x2="12.40155" y2="3.72745" layer="21"/>
<rectangle x1="9.67105" y1="3.72745" x2="12.42695" y2="3.74015" layer="21"/>
<rectangle x1="9.64565" y1="3.74015" x2="12.45235" y2="3.75285" layer="21"/>
<rectangle x1="9.62025" y1="3.75285" x2="12.47775" y2="3.76555" layer="21"/>
<rectangle x1="9.59485" y1="3.76555" x2="12.50315" y2="3.77825" layer="21"/>
<rectangle x1="9.56945" y1="3.77825" x2="12.52855" y2="3.79095" layer="21"/>
<rectangle x1="9.54405" y1="3.79095" x2="12.55395" y2="3.80365" layer="21"/>
<rectangle x1="9.51865" y1="3.80365" x2="12.57935" y2="3.81635" layer="21"/>
<rectangle x1="9.49325" y1="3.81635" x2="12.60475" y2="3.82905" layer="21"/>
<rectangle x1="9.46785" y1="3.82905" x2="12.63015" y2="3.84175" layer="21"/>
<rectangle x1="9.44245" y1="3.84175" x2="12.65555" y2="3.85445" layer="21"/>
<rectangle x1="9.41705" y1="3.85445" x2="12.68095" y2="3.86715" layer="21"/>
<rectangle x1="9.39165" y1="3.86715" x2="12.70635" y2="3.87985" layer="21"/>
<rectangle x1="9.36625" y1="3.87985" x2="12.73175" y2="3.89255" layer="21"/>
<rectangle x1="9.34085" y1="3.89255" x2="12.75715" y2="3.90525" layer="21"/>
<rectangle x1="9.31545" y1="3.90525" x2="12.78255" y2="3.91795" layer="21"/>
<rectangle x1="9.29005" y1="3.91795" x2="12.80795" y2="3.93065" layer="21"/>
<rectangle x1="9.26465" y1="3.93065" x2="12.83335" y2="3.94335" layer="21"/>
<rectangle x1="9.23925" y1="3.94335" x2="12.85875" y2="3.95605" layer="21"/>
<rectangle x1="9.21385" y1="3.95605" x2="12.88415" y2="3.96875" layer="21"/>
<rectangle x1="9.18845" y1="3.96875" x2="12.90955" y2="3.98145" layer="21"/>
<rectangle x1="9.16305" y1="3.98145" x2="12.93495" y2="3.99415" layer="21"/>
<rectangle x1="9.13765" y1="3.99415" x2="12.96035" y2="4.00685" layer="21"/>
<rectangle x1="9.11225" y1="4.00685" x2="12.98575" y2="4.01955" layer="21"/>
<rectangle x1="9.08685" y1="4.01955" x2="13.01115" y2="4.03225" layer="21"/>
<rectangle x1="9.06145" y1="4.03225" x2="13.03655" y2="4.04495" layer="21"/>
<rectangle x1="9.03605" y1="4.04495" x2="13.06195" y2="4.05765" layer="21"/>
<rectangle x1="9.01065" y1="4.05765" x2="13.08735" y2="4.07035" layer="21"/>
<rectangle x1="8.98525" y1="4.07035" x2="13.11275" y2="4.08305" layer="21"/>
<rectangle x1="8.95985" y1="4.08305" x2="13.13815" y2="4.09575" layer="21"/>
<rectangle x1="8.93445" y1="4.09575" x2="13.16355" y2="4.10845" layer="21"/>
<rectangle x1="8.90905" y1="4.10845" x2="13.18895" y2="4.12115" layer="21"/>
<rectangle x1="8.88365" y1="4.12115" x2="13.21435" y2="4.13385" layer="21"/>
<rectangle x1="8.85825" y1="4.13385" x2="13.23975" y2="4.14655" layer="21"/>
<rectangle x1="8.83285" y1="4.14655" x2="13.26515" y2="4.15925" layer="21"/>
<rectangle x1="8.80745" y1="4.15925" x2="13.29055" y2="4.17195" layer="21"/>
<rectangle x1="8.78205" y1="4.17195" x2="13.31595" y2="4.18465" layer="21"/>
<rectangle x1="8.75665" y1="4.18465" x2="13.34135" y2="4.19735" layer="21"/>
<rectangle x1="8.73125" y1="4.19735" x2="13.36675" y2="4.21005" layer="21"/>
<rectangle x1="8.70585" y1="4.21005" x2="13.39215" y2="4.22275" layer="21"/>
<rectangle x1="8.68045" y1="4.22275" x2="13.41755" y2="4.23545" layer="21"/>
<rectangle x1="8.65505" y1="4.23545" x2="13.44295" y2="4.24815" layer="21"/>
<rectangle x1="8.62965" y1="4.24815" x2="13.46835" y2="4.26085" layer="21"/>
<rectangle x1="8.60425" y1="4.26085" x2="13.49375" y2="4.27355" layer="21"/>
<rectangle x1="8.57885" y1="4.27355" x2="13.51915" y2="4.28625" layer="21"/>
<rectangle x1="8.55345" y1="4.28625" x2="13.54455" y2="4.29895" layer="21"/>
<rectangle x1="8.52805" y1="4.29895" x2="13.56995" y2="4.31165" layer="21"/>
<rectangle x1="8.50265" y1="4.31165" x2="13.59535" y2="4.32435" layer="21"/>
<rectangle x1="8.47725" y1="4.32435" x2="13.62075" y2="4.33705" layer="21"/>
<rectangle x1="8.45185" y1="4.33705" x2="13.64615" y2="4.34975" layer="21"/>
<rectangle x1="8.42645" y1="4.34975" x2="13.67155" y2="4.36245" layer="21"/>
<rectangle x1="8.40105" y1="4.36245" x2="13.69695" y2="4.37515" layer="21"/>
<rectangle x1="8.37565" y1="4.37515" x2="13.72235" y2="4.38785" layer="21"/>
<rectangle x1="8.35025" y1="4.38785" x2="13.74775" y2="4.40055" layer="21"/>
<rectangle x1="8.32485" y1="4.40055" x2="13.77315" y2="4.41325" layer="21"/>
<rectangle x1="8.29945" y1="4.41325" x2="13.79855" y2="4.42595" layer="21"/>
<rectangle x1="8.27405" y1="4.42595" x2="13.82395" y2="4.43865" layer="21"/>
<rectangle x1="8.24865" y1="4.43865" x2="13.84935" y2="4.45135" layer="21"/>
<rectangle x1="8.22325" y1="4.45135" x2="13.87475" y2="4.46405" layer="21"/>
<rectangle x1="8.19785" y1="4.46405" x2="13.90015" y2="4.47675" layer="21"/>
<rectangle x1="8.17245" y1="4.47675" x2="13.92555" y2="4.48945" layer="21"/>
<rectangle x1="8.14705" y1="4.48945" x2="13.95095" y2="4.50215" layer="21"/>
<rectangle x1="8.12165" y1="4.50215" x2="13.97635" y2="4.51485" layer="21"/>
<rectangle x1="8.09625" y1="4.51485" x2="14.00175" y2="4.52755" layer="21"/>
<rectangle x1="8.07085" y1="4.52755" x2="14.02715" y2="4.54025" layer="21"/>
<rectangle x1="8.04545" y1="4.54025" x2="14.05255" y2="4.55295" layer="21"/>
<rectangle x1="8.02005" y1="4.55295" x2="14.07795" y2="4.56565" layer="21"/>
<rectangle x1="7.99465" y1="4.56565" x2="14.10335" y2="4.57835" layer="21"/>
<rectangle x1="7.96925" y1="4.57835" x2="14.12875" y2="4.59105" layer="21"/>
<rectangle x1="7.94385" y1="4.59105" x2="14.15415" y2="4.60375" layer="21"/>
<rectangle x1="7.91845" y1="4.60375" x2="14.17955" y2="4.61645" layer="21"/>
<rectangle x1="7.89305" y1="4.61645" x2="14.20495" y2="4.62915" layer="21"/>
<rectangle x1="7.86765" y1="4.62915" x2="14.23035" y2="4.64185" layer="21"/>
<rectangle x1="7.84225" y1="4.64185" x2="14.25575" y2="4.65455" layer="21"/>
<rectangle x1="7.81685" y1="4.65455" x2="14.28115" y2="4.66725" layer="21"/>
<rectangle x1="7.79145" y1="4.66725" x2="14.30655" y2="4.67995" layer="21"/>
<rectangle x1="7.76605" y1="4.67995" x2="14.33195" y2="4.69265" layer="21"/>
<rectangle x1="7.74065" y1="4.69265" x2="14.35735" y2="4.70535" layer="21"/>
<rectangle x1="7.71525" y1="4.70535" x2="14.38275" y2="4.71805" layer="21"/>
<rectangle x1="7.68985" y1="4.71805" x2="14.40815" y2="4.73075" layer="21"/>
<rectangle x1="7.66445" y1="4.73075" x2="14.43355" y2="4.74345" layer="21"/>
<rectangle x1="7.63905" y1="4.74345" x2="14.45895" y2="4.75615" layer="21"/>
<rectangle x1="7.61365" y1="4.75615" x2="14.48435" y2="4.76885" layer="21"/>
<rectangle x1="7.58825" y1="4.76885" x2="14.50975" y2="4.78155" layer="21"/>
<rectangle x1="7.56285" y1="4.78155" x2="14.53515" y2="4.79425" layer="21"/>
<rectangle x1="7.53745" y1="4.79425" x2="14.56055" y2="4.80695" layer="21"/>
<rectangle x1="7.51205" y1="4.80695" x2="14.58595" y2="4.81965" layer="21"/>
<rectangle x1="7.48665" y1="4.81965" x2="14.61135" y2="4.83235" layer="21"/>
<rectangle x1="7.46125" y1="4.83235" x2="14.63675" y2="4.84505" layer="21"/>
<rectangle x1="7.43585" y1="4.84505" x2="14.66215" y2="4.85775" layer="21"/>
<rectangle x1="7.41045" y1="4.85775" x2="14.68755" y2="4.87045" layer="21"/>
<rectangle x1="7.38505" y1="4.87045" x2="14.71295" y2="4.88315" layer="21"/>
<rectangle x1="7.35965" y1="4.88315" x2="14.73835" y2="4.89585" layer="21"/>
<rectangle x1="7.33425" y1="4.89585" x2="14.76375" y2="4.90855" layer="21"/>
<rectangle x1="7.30885" y1="4.90855" x2="14.78915" y2="4.92125" layer="21"/>
<rectangle x1="7.28345" y1="4.92125" x2="14.81455" y2="4.93395" layer="21"/>
<rectangle x1="7.25805" y1="4.93395" x2="14.83995" y2="4.94665" layer="21"/>
<rectangle x1="7.23265" y1="4.94665" x2="14.86535" y2="4.95935" layer="21"/>
<rectangle x1="7.20725" y1="4.95935" x2="14.89075" y2="4.97205" layer="21"/>
<rectangle x1="7.18185" y1="4.97205" x2="14.91615" y2="4.98475" layer="21"/>
<rectangle x1="7.15645" y1="4.98475" x2="14.94155" y2="4.99745" layer="21"/>
<rectangle x1="7.13105" y1="4.99745" x2="14.96695" y2="5.01015" layer="21"/>
<rectangle x1="7.10565" y1="5.01015" x2="14.99235" y2="5.02285" layer="21"/>
<rectangle x1="7.08025" y1="5.02285" x2="15.01775" y2="5.03555" layer="21"/>
<rectangle x1="7.05485" y1="5.03555" x2="15.04315" y2="5.04825" layer="21"/>
<rectangle x1="7.02945" y1="5.04825" x2="15.06855" y2="5.06095" layer="21"/>
<rectangle x1="7.00405" y1="5.06095" x2="15.09395" y2="5.07365" layer="21"/>
<rectangle x1="6.97865" y1="5.07365" x2="15.11935" y2="5.08635" layer="21"/>
<rectangle x1="6.95325" y1="5.08635" x2="15.14475" y2="5.09905" layer="21"/>
<rectangle x1="6.92785" y1="5.09905" x2="15.17015" y2="5.11175" layer="21"/>
<rectangle x1="6.90245" y1="5.11175" x2="15.19555" y2="5.12445" layer="21"/>
<rectangle x1="6.87705" y1="5.12445" x2="15.22095" y2="5.13715" layer="21"/>
<rectangle x1="6.85165" y1="5.13715" x2="15.24635" y2="5.14985" layer="21"/>
<rectangle x1="6.82625" y1="5.14985" x2="15.27175" y2="5.16255" layer="21"/>
<rectangle x1="6.80085" y1="5.16255" x2="15.29715" y2="5.17525" layer="21"/>
<rectangle x1="6.77545" y1="5.17525" x2="15.32255" y2="5.18795" layer="21"/>
<rectangle x1="6.75005" y1="5.18795" x2="15.34795" y2="5.20065" layer="21"/>
<rectangle x1="6.72465" y1="5.20065" x2="15.37335" y2="5.21335" layer="21"/>
<rectangle x1="6.69925" y1="5.21335" x2="15.39875" y2="5.22605" layer="21"/>
<rectangle x1="6.67385" y1="5.22605" x2="15.42415" y2="5.23875" layer="21"/>
<rectangle x1="6.64845" y1="5.23875" x2="15.44955" y2="5.25145" layer="21"/>
<rectangle x1="6.62305" y1="5.25145" x2="15.47495" y2="5.26415" layer="21"/>
<rectangle x1="6.59765" y1="5.26415" x2="15.50035" y2="5.27685" layer="21"/>
<rectangle x1="6.57225" y1="5.27685" x2="15.52575" y2="5.28955" layer="21"/>
<rectangle x1="6.54685" y1="5.28955" x2="15.55115" y2="5.30225" layer="21"/>
<rectangle x1="6.52145" y1="5.30225" x2="15.57655" y2="5.31495" layer="21"/>
<rectangle x1="6.49605" y1="5.31495" x2="15.60195" y2="5.32765" layer="21"/>
<rectangle x1="6.47065" y1="5.32765" x2="15.62735" y2="5.34035" layer="21"/>
<rectangle x1="6.44525" y1="5.34035" x2="15.65275" y2="5.35305" layer="21"/>
<rectangle x1="6.41985" y1="5.35305" x2="15.67815" y2="5.36575" layer="21"/>
<rectangle x1="6.39445" y1="5.36575" x2="15.70355" y2="5.37845" layer="21"/>
<rectangle x1="6.36905" y1="5.37845" x2="15.72895" y2="5.39115" layer="21"/>
<rectangle x1="6.34365" y1="5.39115" x2="15.75435" y2="5.40385" layer="21"/>
<rectangle x1="6.31825" y1="5.40385" x2="15.77975" y2="5.41655" layer="21"/>
<rectangle x1="6.29285" y1="5.41655" x2="15.80515" y2="5.42925" layer="21"/>
<rectangle x1="6.26745" y1="5.42925" x2="15.83055" y2="5.44195" layer="21"/>
<rectangle x1="6.24205" y1="5.44195" x2="15.85595" y2="5.45465" layer="21"/>
<rectangle x1="6.21665" y1="5.45465" x2="15.88135" y2="5.46735" layer="21"/>
<rectangle x1="6.19125" y1="5.46735" x2="15.90675" y2="5.48005" layer="21"/>
<rectangle x1="6.16585" y1="5.48005" x2="15.93215" y2="5.49275" layer="21"/>
<rectangle x1="6.14045" y1="5.49275" x2="15.95755" y2="5.50545" layer="21"/>
<rectangle x1="6.11505" y1="5.50545" x2="15.98295" y2="5.51815" layer="21"/>
<rectangle x1="6.08965" y1="5.51815" x2="16.00835" y2="5.53085" layer="21"/>
<rectangle x1="6.06425" y1="5.53085" x2="16.03375" y2="5.54355" layer="21"/>
<rectangle x1="6.03885" y1="5.54355" x2="16.05915" y2="5.55625" layer="21"/>
<rectangle x1="6.01345" y1="5.55625" x2="16.08455" y2="5.56895" layer="21"/>
<rectangle x1="5.98805" y1="5.56895" x2="16.10995" y2="5.58165" layer="21"/>
<rectangle x1="5.96265" y1="5.58165" x2="16.13535" y2="5.59435" layer="21"/>
<rectangle x1="5.93725" y1="5.59435" x2="16.16075" y2="5.60705" layer="21"/>
<rectangle x1="5.91185" y1="5.60705" x2="16.18615" y2="5.61975" layer="21"/>
<rectangle x1="5.88645" y1="5.61975" x2="16.21155" y2="5.63245" layer="21"/>
<rectangle x1="5.86105" y1="5.63245" x2="16.23695" y2="5.64515" layer="21"/>
<rectangle x1="5.83565" y1="5.64515" x2="16.26235" y2="5.65785" layer="21"/>
<rectangle x1="5.81025" y1="5.65785" x2="16.28775" y2="5.67055" layer="21"/>
<rectangle x1="5.78485" y1="5.67055" x2="16.31315" y2="5.68325" layer="21"/>
<rectangle x1="5.75945" y1="5.68325" x2="16.33855" y2="5.69595" layer="21"/>
<rectangle x1="5.73405" y1="5.69595" x2="16.36395" y2="5.70865" layer="21"/>
<rectangle x1="5.70865" y1="5.70865" x2="16.38935" y2="5.72135" layer="21"/>
<rectangle x1="5.68325" y1="5.72135" x2="16.41475" y2="5.73405" layer="21"/>
<rectangle x1="5.65785" y1="5.73405" x2="16.44015" y2="5.74675" layer="21"/>
<rectangle x1="5.63245" y1="5.74675" x2="16.46555" y2="5.75945" layer="21"/>
<rectangle x1="5.60705" y1="5.75945" x2="16.49095" y2="5.77215" layer="21"/>
<rectangle x1="5.58165" y1="5.77215" x2="16.51635" y2="5.78485" layer="21"/>
<rectangle x1="5.55625" y1="5.78485" x2="16.54175" y2="5.79755" layer="21"/>
<rectangle x1="5.53085" y1="5.79755" x2="16.56715" y2="5.81025" layer="21"/>
<rectangle x1="5.50545" y1="5.81025" x2="16.59255" y2="5.82295" layer="21"/>
<rectangle x1="5.48005" y1="5.82295" x2="16.61795" y2="5.83565" layer="21"/>
<rectangle x1="5.45465" y1="5.83565" x2="16.64335" y2="5.84835" layer="21"/>
<rectangle x1="5.42925" y1="5.84835" x2="16.66875" y2="5.86105" layer="21"/>
<rectangle x1="5.40385" y1="5.86105" x2="16.69415" y2="5.87375" layer="21"/>
<rectangle x1="5.37845" y1="5.87375" x2="16.71955" y2="5.88645" layer="21"/>
<rectangle x1="5.35305" y1="5.88645" x2="16.74495" y2="5.89915" layer="21"/>
<rectangle x1="5.32765" y1="5.89915" x2="16.77035" y2="5.91185" layer="21"/>
<rectangle x1="5.30225" y1="5.91185" x2="16.79575" y2="5.92455" layer="21"/>
<rectangle x1="5.27685" y1="5.92455" x2="16.82115" y2="5.93725" layer="21"/>
<rectangle x1="5.25145" y1="5.93725" x2="16.84655" y2="5.94995" layer="21"/>
<rectangle x1="5.22605" y1="5.94995" x2="16.87195" y2="5.96265" layer="21"/>
<rectangle x1="5.20065" y1="5.96265" x2="16.89735" y2="5.97535" layer="21"/>
<rectangle x1="5.17525" y1="5.97535" x2="16.92275" y2="5.98805" layer="21"/>
<rectangle x1="5.14985" y1="5.98805" x2="16.94815" y2="6.00075" layer="21"/>
<rectangle x1="5.12445" y1="6.00075" x2="16.97355" y2="6.01345" layer="21"/>
<rectangle x1="5.09905" y1="6.01345" x2="16.99895" y2="6.02615" layer="21"/>
<rectangle x1="5.07365" y1="6.02615" x2="17.02435" y2="6.03885" layer="21"/>
<rectangle x1="5.04825" y1="6.03885" x2="17.04975" y2="6.05155" layer="21"/>
<rectangle x1="5.02285" y1="6.05155" x2="17.07515" y2="6.06425" layer="21"/>
<rectangle x1="4.99745" y1="6.06425" x2="17.10055" y2="6.07695" layer="21"/>
<rectangle x1="4.97205" y1="6.07695" x2="17.12595" y2="6.08965" layer="21"/>
<rectangle x1="4.94665" y1="6.08965" x2="17.15135" y2="6.10235" layer="21"/>
<rectangle x1="4.92125" y1="6.10235" x2="17.17675" y2="6.11505" layer="21"/>
<rectangle x1="4.89585" y1="6.11505" x2="17.20215" y2="6.12775" layer="21"/>
<rectangle x1="4.87045" y1="6.12775" x2="17.22755" y2="6.14045" layer="21"/>
<rectangle x1="4.84505" y1="6.14045" x2="17.25295" y2="6.15315" layer="21"/>
<rectangle x1="4.81965" y1="6.15315" x2="17.27835" y2="6.16585" layer="21"/>
<rectangle x1="4.79425" y1="6.16585" x2="17.30375" y2="6.17855" layer="21"/>
<rectangle x1="4.76885" y1="6.17855" x2="17.32915" y2="6.19125" layer="21"/>
<rectangle x1="4.76885" y1="6.19125" x2="17.32915" y2="6.20395" layer="21"/>
<rectangle x1="4.76885" y1="6.20395" x2="17.32915" y2="6.21665" layer="21"/>
<rectangle x1="4.76885" y1="6.21665" x2="17.32915" y2="6.22935" layer="21"/>
<rectangle x1="4.76885" y1="6.22935" x2="17.32915" y2="6.24205" layer="21"/>
<rectangle x1="4.76885" y1="6.24205" x2="17.32915" y2="6.25475" layer="21"/>
<rectangle x1="4.76885" y1="6.25475" x2="17.32915" y2="6.26745" layer="21"/>
<rectangle x1="4.76885" y1="6.26745" x2="17.32915" y2="6.28015" layer="21"/>
<rectangle x1="4.76885" y1="6.28015" x2="17.32915" y2="6.29285" layer="21"/>
<rectangle x1="4.76885" y1="6.29285" x2="17.32915" y2="6.30555" layer="21"/>
<rectangle x1="4.76885" y1="6.30555" x2="17.32915" y2="6.31825" layer="21"/>
<rectangle x1="4.76885" y1="6.31825" x2="17.32915" y2="6.33095" layer="21"/>
<rectangle x1="4.76885" y1="6.33095" x2="17.32915" y2="6.34365" layer="21"/>
<rectangle x1="4.76885" y1="6.34365" x2="17.32915" y2="6.35635" layer="21"/>
<rectangle x1="4.76885" y1="6.35635" x2="17.32915" y2="6.36905" layer="21"/>
<rectangle x1="4.76885" y1="6.36905" x2="17.32915" y2="6.38175" layer="21"/>
<rectangle x1="4.76885" y1="6.38175" x2="17.32915" y2="6.39445" layer="21"/>
<rectangle x1="4.76885" y1="6.39445" x2="17.32915" y2="6.40715" layer="21"/>
<rectangle x1="4.76885" y1="6.40715" x2="17.32915" y2="6.41985" layer="21"/>
<rectangle x1="4.76885" y1="6.41985" x2="17.32915" y2="6.43255" layer="21"/>
<rectangle x1="4.76885" y1="6.43255" x2="17.32915" y2="6.44525" layer="21"/>
<rectangle x1="4.76885" y1="6.44525" x2="17.32915" y2="6.45795" layer="21"/>
<rectangle x1="4.76885" y1="6.45795" x2="17.32915" y2="6.47065" layer="21"/>
<rectangle x1="4.76885" y1="6.47065" x2="17.32915" y2="6.48335" layer="21"/>
<rectangle x1="4.76885" y1="6.48335" x2="17.32915" y2="6.49605" layer="21"/>
<rectangle x1="4.76885" y1="6.49605" x2="17.32915" y2="6.50875" layer="21"/>
<rectangle x1="4.76885" y1="6.50875" x2="17.32915" y2="6.52145" layer="21"/>
<rectangle x1="4.76885" y1="6.52145" x2="17.32915" y2="6.53415" layer="21"/>
<rectangle x1="4.76885" y1="6.53415" x2="17.32915" y2="6.54685" layer="21"/>
<rectangle x1="4.76885" y1="6.54685" x2="17.32915" y2="6.55955" layer="21"/>
<rectangle x1="4.76885" y1="6.55955" x2="17.32915" y2="6.57225" layer="21"/>
<rectangle x1="4.76885" y1="6.57225" x2="17.32915" y2="6.58495" layer="21"/>
<rectangle x1="4.76885" y1="6.58495" x2="17.32915" y2="6.59765" layer="21"/>
<rectangle x1="4.76885" y1="6.59765" x2="17.32915" y2="6.61035" layer="21"/>
<rectangle x1="4.76885" y1="6.61035" x2="17.32915" y2="6.62305" layer="21"/>
<rectangle x1="4.76885" y1="6.62305" x2="17.32915" y2="6.63575" layer="21"/>
<rectangle x1="4.76885" y1="6.63575" x2="17.32915" y2="6.64845" layer="21"/>
<rectangle x1="4.76885" y1="6.64845" x2="17.32915" y2="6.66115" layer="21"/>
<rectangle x1="4.76885" y1="6.66115" x2="17.32915" y2="6.67385" layer="21"/>
<rectangle x1="4.76885" y1="6.67385" x2="17.32915" y2="6.68655" layer="21"/>
<rectangle x1="4.76885" y1="6.68655" x2="17.32915" y2="6.69925" layer="21"/>
<rectangle x1="4.76885" y1="6.69925" x2="17.32915" y2="6.71195" layer="21"/>
<rectangle x1="4.76885" y1="6.71195" x2="17.32915" y2="6.72465" layer="21"/>
<rectangle x1="4.76885" y1="6.72465" x2="17.32915" y2="6.73735" layer="21"/>
<rectangle x1="4.76885" y1="6.73735" x2="17.32915" y2="6.75005" layer="21"/>
<rectangle x1="4.76885" y1="6.75005" x2="17.32915" y2="6.76275" layer="21"/>
<rectangle x1="4.76885" y1="6.76275" x2="17.32915" y2="6.77545" layer="21"/>
<rectangle x1="4.76885" y1="6.77545" x2="17.32915" y2="6.78815" layer="21"/>
<rectangle x1="4.76885" y1="6.78815" x2="17.32915" y2="6.80085" layer="21"/>
<rectangle x1="4.76885" y1="6.80085" x2="17.32915" y2="6.81355" layer="21"/>
<rectangle x1="4.76885" y1="6.81355" x2="17.32915" y2="6.82625" layer="21"/>
<rectangle x1="4.76885" y1="6.82625" x2="17.32915" y2="6.83895" layer="21"/>
<rectangle x1="4.76885" y1="6.83895" x2="17.32915" y2="6.85165" layer="21"/>
<rectangle x1="4.76885" y1="6.85165" x2="17.32915" y2="6.86435" layer="21"/>
<rectangle x1="4.76885" y1="6.86435" x2="17.32915" y2="6.87705" layer="21"/>
<rectangle x1="4.76885" y1="6.87705" x2="17.32915" y2="6.88975" layer="21"/>
<rectangle x1="4.76885" y1="6.88975" x2="17.32915" y2="6.90245" layer="21"/>
<rectangle x1="4.76885" y1="6.90245" x2="17.32915" y2="6.91515" layer="21"/>
<rectangle x1="4.76885" y1="6.91515" x2="17.32915" y2="6.92785" layer="21"/>
<rectangle x1="4.76885" y1="6.92785" x2="17.32915" y2="6.94055" layer="21"/>
<rectangle x1="4.76885" y1="6.94055" x2="17.32915" y2="6.95325" layer="21"/>
<rectangle x1="4.76885" y1="6.95325" x2="17.32915" y2="6.96595" layer="21"/>
<rectangle x1="4.76885" y1="6.96595" x2="17.32915" y2="6.97865" layer="21"/>
<rectangle x1="4.76885" y1="6.97865" x2="17.32915" y2="6.99135" layer="21"/>
<rectangle x1="4.76885" y1="6.99135" x2="17.32915" y2="7.00405" layer="21"/>
<rectangle x1="4.76885" y1="7.00405" x2="17.32915" y2="7.01675" layer="21"/>
<rectangle x1="4.76885" y1="7.01675" x2="17.32915" y2="7.02945" layer="21"/>
<rectangle x1="4.76885" y1="7.02945" x2="17.32915" y2="7.04215" layer="21"/>
<rectangle x1="4.76885" y1="7.04215" x2="17.32915" y2="7.05485" layer="21"/>
<rectangle x1="4.76885" y1="7.05485" x2="17.32915" y2="7.06755" layer="21"/>
<rectangle x1="4.76885" y1="7.06755" x2="17.32915" y2="7.08025" layer="21"/>
<rectangle x1="4.76885" y1="7.08025" x2="17.32915" y2="7.09295" layer="21"/>
<rectangle x1="4.76885" y1="7.09295" x2="17.32915" y2="7.10565" layer="21"/>
<rectangle x1="4.76885" y1="7.10565" x2="17.32915" y2="7.11835" layer="21"/>
<rectangle x1="4.76885" y1="7.11835" x2="17.32915" y2="7.13105" layer="21"/>
<rectangle x1="4.76885" y1="7.13105" x2="17.32915" y2="7.14375" layer="21"/>
<rectangle x1="4.76885" y1="7.14375" x2="17.32915" y2="7.15645" layer="21"/>
<rectangle x1="4.76885" y1="7.15645" x2="17.32915" y2="7.16915" layer="21"/>
<rectangle x1="4.76885" y1="7.16915" x2="17.32915" y2="7.18185" layer="21"/>
<rectangle x1="4.76885" y1="7.18185" x2="17.32915" y2="7.19455" layer="21"/>
<rectangle x1="4.76885" y1="7.19455" x2="17.32915" y2="7.20725" layer="21"/>
<rectangle x1="4.76885" y1="7.20725" x2="17.32915" y2="7.21995" layer="21"/>
<rectangle x1="4.76885" y1="7.21995" x2="17.32915" y2="7.23265" layer="21"/>
<rectangle x1="4.76885" y1="7.23265" x2="17.32915" y2="7.24535" layer="21"/>
<rectangle x1="4.76885" y1="7.24535" x2="17.32915" y2="7.25805" layer="21"/>
<rectangle x1="4.76885" y1="7.25805" x2="17.32915" y2="7.27075" layer="21"/>
<rectangle x1="4.76885" y1="7.27075" x2="17.32915" y2="7.28345" layer="21"/>
<rectangle x1="4.76885" y1="7.28345" x2="17.32915" y2="7.29615" layer="21"/>
<rectangle x1="4.76885" y1="7.29615" x2="13.53185" y2="7.30885" layer="21"/>
<rectangle x1="13.73505" y1="7.29615" x2="17.32915" y2="7.30885" layer="21"/>
<rectangle x1="4.76885" y1="7.30885" x2="13.46835" y2="7.32155" layer="21"/>
<rectangle x1="13.78585" y1="7.30885" x2="17.32915" y2="7.32155" layer="21"/>
<rectangle x1="4.76885" y1="7.32155" x2="13.43025" y2="7.33425" layer="21"/>
<rectangle x1="13.82395" y1="7.32155" x2="17.32915" y2="7.33425" layer="21"/>
<rectangle x1="4.76885" y1="7.33425" x2="13.40485" y2="7.34695" layer="21"/>
<rectangle x1="13.84935" y1="7.33425" x2="17.32915" y2="7.34695" layer="21"/>
<rectangle x1="4.76885" y1="7.34695" x2="13.37945" y2="7.35965" layer="21"/>
<rectangle x1="13.87475" y1="7.34695" x2="17.32915" y2="7.35965" layer="21"/>
<rectangle x1="4.76885" y1="7.35965" x2="13.36675" y2="7.37235" layer="21"/>
<rectangle x1="13.88745" y1="7.35965" x2="17.32915" y2="7.37235" layer="21"/>
<rectangle x1="4.76885" y1="7.37235" x2="13.34135" y2="7.38505" layer="21"/>
<rectangle x1="13.91285" y1="7.37235" x2="17.32915" y2="7.38505" layer="21"/>
<rectangle x1="4.76885" y1="7.38505" x2="13.32865" y2="7.39775" layer="21"/>
<rectangle x1="13.53185" y1="7.38505" x2="13.72235" y2="7.39775" layer="21"/>
<rectangle x1="13.92555" y1="7.38505" x2="17.32915" y2="7.39775" layer="21"/>
<rectangle x1="4.76885" y1="7.39775" x2="13.31595" y2="7.41045" layer="21"/>
<rectangle x1="13.48105" y1="7.39775" x2="13.77315" y2="7.41045" layer="21"/>
<rectangle x1="13.93825" y1="7.39775" x2="17.32915" y2="7.41045" layer="21"/>
<rectangle x1="4.76885" y1="7.41045" x2="13.30325" y2="7.42315" layer="21"/>
<rectangle x1="13.45565" y1="7.41045" x2="13.79855" y2="7.42315" layer="21"/>
<rectangle x1="13.95095" y1="7.41045" x2="17.32915" y2="7.42315" layer="21"/>
<rectangle x1="4.76885" y1="7.42315" x2="13.29055" y2="7.43585" layer="21"/>
<rectangle x1="13.43025" y1="7.42315" x2="13.82395" y2="7.43585" layer="21"/>
<rectangle x1="13.96365" y1="7.42315" x2="17.32915" y2="7.43585" layer="21"/>
<rectangle x1="4.76885" y1="7.43585" x2="13.27785" y2="7.44855" layer="21"/>
<rectangle x1="13.41755" y1="7.43585" x2="13.83665" y2="7.44855" layer="21"/>
<rectangle x1="13.96365" y1="7.43585" x2="17.32915" y2="7.44855" layer="21"/>
<rectangle x1="4.76885" y1="7.44855" x2="13.27785" y2="7.46125" layer="21"/>
<rectangle x1="13.39215" y1="7.44855" x2="13.84935" y2="7.46125" layer="21"/>
<rectangle x1="13.97635" y1="7.44855" x2="17.32915" y2="7.46125" layer="21"/>
<rectangle x1="4.76885" y1="7.46125" x2="13.26515" y2="7.47395" layer="21"/>
<rectangle x1="13.37945" y1="7.46125" x2="13.86205" y2="7.47395" layer="21"/>
<rectangle x1="13.98905" y1="7.46125" x2="17.32915" y2="7.47395" layer="21"/>
<rectangle x1="4.76885" y1="7.47395" x2="13.25245" y2="7.48665" layer="21"/>
<rectangle x1="13.36675" y1="7.47395" x2="13.87475" y2="7.48665" layer="21"/>
<rectangle x1="13.98905" y1="7.47395" x2="17.32915" y2="7.48665" layer="21"/>
<rectangle x1="4.76885" y1="7.48665" x2="13.25245" y2="7.49935" layer="21"/>
<rectangle x1="13.36675" y1="7.48665" x2="13.88745" y2="7.49935" layer="21"/>
<rectangle x1="14.00175" y1="7.48665" x2="17.32915" y2="7.49935" layer="21"/>
<rectangle x1="4.76885" y1="7.49935" x2="13.23975" y2="7.51205" layer="21"/>
<rectangle x1="13.35405" y1="7.49935" x2="13.90015" y2="7.51205" layer="21"/>
<rectangle x1="14.00175" y1="7.49935" x2="17.32915" y2="7.51205" layer="21"/>
<rectangle x1="4.76885" y1="7.51205" x2="13.23975" y2="7.52475" layer="21"/>
<rectangle x1="13.34135" y1="7.51205" x2="13.90015" y2="7.52475" layer="21"/>
<rectangle x1="14.01445" y1="7.51205" x2="17.32915" y2="7.52475" layer="21"/>
<rectangle x1="4.76885" y1="7.52475" x2="13.23975" y2="7.53745" layer="21"/>
<rectangle x1="13.34135" y1="7.52475" x2="13.91285" y2="7.53745" layer="21"/>
<rectangle x1="14.01445" y1="7.52475" x2="17.32915" y2="7.53745" layer="21"/>
<rectangle x1="4.76885" y1="7.53745" x2="13.22705" y2="7.55015" layer="21"/>
<rectangle x1="13.32865" y1="7.53745" x2="13.91285" y2="7.55015" layer="21"/>
<rectangle x1="14.01445" y1="7.53745" x2="17.32915" y2="7.55015" layer="21"/>
<rectangle x1="4.76885" y1="7.55015" x2="13.22705" y2="7.56285" layer="21"/>
<rectangle x1="13.32865" y1="7.55015" x2="13.92555" y2="7.56285" layer="21"/>
<rectangle x1="14.02715" y1="7.55015" x2="17.32915" y2="7.56285" layer="21"/>
<rectangle x1="4.76885" y1="7.56285" x2="13.22705" y2="7.57555" layer="21"/>
<rectangle x1="13.32865" y1="7.56285" x2="13.92555" y2="7.57555" layer="21"/>
<rectangle x1="14.02715" y1="7.56285" x2="17.32915" y2="7.57555" layer="21"/>
<rectangle x1="4.76885" y1="7.57555" x2="13.22705" y2="7.58825" layer="21"/>
<rectangle x1="13.31595" y1="7.57555" x2="13.92555" y2="7.58825" layer="21"/>
<rectangle x1="14.02715" y1="7.57555" x2="17.32915" y2="7.58825" layer="21"/>
<rectangle x1="4.76885" y1="7.58825" x2="13.93825" y2="7.60095" layer="21"/>
<rectangle x1="14.02715" y1="7.58825" x2="17.32915" y2="7.60095" layer="21"/>
<rectangle x1="4.76885" y1="7.60095" x2="13.93825" y2="7.61365" layer="21"/>
<rectangle x1="14.02715" y1="7.60095" x2="17.32915" y2="7.61365" layer="21"/>
<rectangle x1="4.76885" y1="7.61365" x2="13.93825" y2="7.62635" layer="21"/>
<rectangle x1="14.02715" y1="7.61365" x2="17.32915" y2="7.62635" layer="21"/>
<rectangle x1="4.76885" y1="7.62635" x2="13.93825" y2="7.63905" layer="21"/>
<rectangle x1="14.02715" y1="7.62635" x2="17.32915" y2="7.63905" layer="21"/>
<rectangle x1="4.76885" y1="7.63905" x2="13.93825" y2="7.65175" layer="21"/>
<rectangle x1="14.02715" y1="7.63905" x2="17.32915" y2="7.65175" layer="21"/>
<rectangle x1="4.76885" y1="7.65175" x2="13.93825" y2="7.66445" layer="21"/>
<rectangle x1="14.02715" y1="7.65175" x2="17.32915" y2="7.66445" layer="21"/>
<rectangle x1="4.76885" y1="7.66445" x2="13.93825" y2="7.67715" layer="21"/>
<rectangle x1="14.02715" y1="7.66445" x2="17.32915" y2="7.67715" layer="21"/>
<rectangle x1="4.76885" y1="7.67715" x2="13.93825" y2="7.68985" layer="21"/>
<rectangle x1="14.02715" y1="7.67715" x2="17.32915" y2="7.68985" layer="21"/>
<rectangle x1="4.76885" y1="7.68985" x2="13.93825" y2="7.70255" layer="21"/>
<rectangle x1="14.02715" y1="7.68985" x2="17.32915" y2="7.70255" layer="21"/>
<rectangle x1="4.76885" y1="7.70255" x2="6.85165" y2="7.71525" layer="21"/>
<rectangle x1="7.01675" y1="7.70255" x2="7.88035" y2="7.71525" layer="21"/>
<rectangle x1="8.04545" y1="7.70255" x2="10.96645" y2="7.71525" layer="21"/>
<rectangle x1="11.13155" y1="7.70255" x2="12.46505" y2="7.71525" layer="21"/>
<rectangle x1="12.63015" y1="7.70255" x2="13.93825" y2="7.71525" layer="21"/>
<rectangle x1="14.02715" y1="7.70255" x2="15.09395" y2="7.71525" layer="21"/>
<rectangle x1="15.25905" y1="7.70255" x2="16.04645" y2="7.71525" layer="21"/>
<rectangle x1="16.24965" y1="7.70255" x2="17.32915" y2="7.71525" layer="21"/>
<rectangle x1="4.76885" y1="7.71525" x2="6.78815" y2="7.72795" layer="21"/>
<rectangle x1="7.08025" y1="7.71525" x2="7.82955" y2="7.72795" layer="21"/>
<rectangle x1="8.09625" y1="7.71525" x2="10.91565" y2="7.72795" layer="21"/>
<rectangle x1="11.18235" y1="7.71525" x2="12.41425" y2="7.72795" layer="21"/>
<rectangle x1="12.68095" y1="7.71525" x2="13.55725" y2="7.72795" layer="21"/>
<rectangle x1="13.67155" y1="7.71525" x2="13.93825" y2="7.72795" layer="21"/>
<rectangle x1="14.02715" y1="7.71525" x2="15.03045" y2="7.72795" layer="21"/>
<rectangle x1="15.32255" y1="7.71525" x2="15.99565" y2="7.72795" layer="21"/>
<rectangle x1="16.30045" y1="7.71525" x2="17.32915" y2="7.72795" layer="21"/>
<rectangle x1="4.76885" y1="7.72795" x2="6.02615" y2="7.74065" layer="21"/>
<rectangle x1="6.12775" y1="7.72795" x2="6.76275" y2="7.74065" layer="21"/>
<rectangle x1="7.10565" y1="7.72795" x2="7.79145" y2="7.74065" layer="21"/>
<rectangle x1="8.13435" y1="7.72795" x2="8.60425" y2="7.74065" layer="21"/>
<rectangle x1="8.69315" y1="7.72795" x2="9.26465" y2="7.74065" layer="21"/>
<rectangle x1="9.35355" y1="7.72795" x2="9.63295" y2="7.74065" layer="21"/>
<rectangle x1="9.72185" y1="7.72795" x2="10.29335" y2="7.74065" layer="21"/>
<rectangle x1="10.38225" y1="7.72795" x2="10.87755" y2="7.74065" layer="21"/>
<rectangle x1="11.22045" y1="7.72795" x2="11.75385" y2="7.74065" layer="21"/>
<rectangle x1="11.84275" y1="7.72795" x2="12.37615" y2="7.74065" layer="21"/>
<rectangle x1="12.71905" y1="7.72795" x2="13.49375" y2="7.74065" layer="21"/>
<rectangle x1="13.74775" y1="7.72795" x2="13.93825" y2="7.74065" layer="21"/>
<rectangle x1="14.02715" y1="7.72795" x2="14.37005" y2="7.74065" layer="21"/>
<rectangle x1="14.45895" y1="7.72795" x2="15.00505" y2="7.74065" layer="21"/>
<rectangle x1="15.34795" y1="7.72795" x2="15.95755" y2="7.74065" layer="21"/>
<rectangle x1="16.33855" y1="7.72795" x2="17.32915" y2="7.74065" layer="21"/>
<rectangle x1="4.76885" y1="7.74065" x2="6.02615" y2="7.75335" layer="21"/>
<rectangle x1="6.12775" y1="7.74065" x2="6.73735" y2="7.75335" layer="21"/>
<rectangle x1="7.14375" y1="7.74065" x2="7.76605" y2="7.75335" layer="21"/>
<rectangle x1="8.15975" y1="7.74065" x2="8.60425" y2="7.75335" layer="21"/>
<rectangle x1="8.69315" y1="7.74065" x2="9.26465" y2="7.75335" layer="21"/>
<rectangle x1="9.35355" y1="7.74065" x2="9.63295" y2="7.75335" layer="21"/>
<rectangle x1="9.72185" y1="7.74065" x2="10.29335" y2="7.75335" layer="21"/>
<rectangle x1="10.38225" y1="7.74065" x2="10.85215" y2="7.75335" layer="21"/>
<rectangle x1="11.24585" y1="7.74065" x2="11.75385" y2="7.75335" layer="21"/>
<rectangle x1="11.84275" y1="7.74065" x2="12.35075" y2="7.75335" layer="21"/>
<rectangle x1="12.74445" y1="7.74065" x2="13.45565" y2="7.75335" layer="21"/>
<rectangle x1="13.78585" y1="7.74065" x2="13.93825" y2="7.75335" layer="21"/>
<rectangle x1="14.02715" y1="7.74065" x2="14.37005" y2="7.75335" layer="21"/>
<rectangle x1="14.45895" y1="7.74065" x2="14.97965" y2="7.75335" layer="21"/>
<rectangle x1="15.38605" y1="7.74065" x2="15.93215" y2="7.75335" layer="21"/>
<rectangle x1="16.36395" y1="7.74065" x2="17.32915" y2="7.75335" layer="21"/>
<rectangle x1="4.76885" y1="7.75335" x2="6.02615" y2="7.76605" layer="21"/>
<rectangle x1="6.12775" y1="7.75335" x2="6.71195" y2="7.76605" layer="21"/>
<rectangle x1="7.15645" y1="7.75335" x2="7.74065" y2="7.76605" layer="21"/>
<rectangle x1="8.18515" y1="7.75335" x2="8.60425" y2="7.76605" layer="21"/>
<rectangle x1="8.69315" y1="7.75335" x2="9.26465" y2="7.76605" layer="21"/>
<rectangle x1="9.35355" y1="7.75335" x2="9.63295" y2="7.76605" layer="21"/>
<rectangle x1="9.72185" y1="7.75335" x2="10.29335" y2="7.76605" layer="21"/>
<rectangle x1="10.38225" y1="7.75335" x2="10.82675" y2="7.76605" layer="21"/>
<rectangle x1="11.27125" y1="7.75335" x2="11.75385" y2="7.76605" layer="21"/>
<rectangle x1="11.84275" y1="7.75335" x2="12.32535" y2="7.76605" layer="21"/>
<rectangle x1="12.76985" y1="7.75335" x2="13.43025" y2="7.76605" layer="21"/>
<rectangle x1="13.82395" y1="7.75335" x2="13.93825" y2="7.76605" layer="21"/>
<rectangle x1="14.02715" y1="7.75335" x2="14.37005" y2="7.76605" layer="21"/>
<rectangle x1="14.45895" y1="7.75335" x2="14.95425" y2="7.76605" layer="21"/>
<rectangle x1="15.39875" y1="7.75335" x2="15.90675" y2="7.76605" layer="21"/>
<rectangle x1="16.38935" y1="7.75335" x2="17.32915" y2="7.76605" layer="21"/>
<rectangle x1="4.76885" y1="7.76605" x2="6.02615" y2="7.77875" layer="21"/>
<rectangle x1="6.12775" y1="7.76605" x2="6.68655" y2="7.77875" layer="21"/>
<rectangle x1="7.18185" y1="7.76605" x2="7.72795" y2="7.77875" layer="21"/>
<rectangle x1="8.19785" y1="7.76605" x2="8.60425" y2="7.77875" layer="21"/>
<rectangle x1="8.69315" y1="7.76605" x2="9.26465" y2="7.77875" layer="21"/>
<rectangle x1="9.35355" y1="7.76605" x2="9.63295" y2="7.77875" layer="21"/>
<rectangle x1="9.72185" y1="7.76605" x2="10.29335" y2="7.77875" layer="21"/>
<rectangle x1="10.38225" y1="7.76605" x2="10.80135" y2="7.77875" layer="21"/>
<rectangle x1="11.28395" y1="7.76605" x2="11.75385" y2="7.77875" layer="21"/>
<rectangle x1="11.84275" y1="7.76605" x2="12.29995" y2="7.77875" layer="21"/>
<rectangle x1="12.78255" y1="7.76605" x2="13.40485" y2="7.77875" layer="21"/>
<rectangle x1="13.84935" y1="7.76605" x2="13.93825" y2="7.77875" layer="21"/>
<rectangle x1="14.02715" y1="7.76605" x2="14.37005" y2="7.77875" layer="21"/>
<rectangle x1="14.45895" y1="7.76605" x2="14.92885" y2="7.77875" layer="21"/>
<rectangle x1="15.42415" y1="7.76605" x2="15.88135" y2="7.77875" layer="21"/>
<rectangle x1="16.40205" y1="7.76605" x2="17.32915" y2="7.77875" layer="21"/>
<rectangle x1="4.76885" y1="7.77875" x2="6.02615" y2="7.79145" layer="21"/>
<rectangle x1="6.12775" y1="7.77875" x2="6.67385" y2="7.79145" layer="21"/>
<rectangle x1="7.19455" y1="7.77875" x2="7.70255" y2="7.79145" layer="21"/>
<rectangle x1="8.22325" y1="7.77875" x2="8.60425" y2="7.79145" layer="21"/>
<rectangle x1="8.69315" y1="7.77875" x2="9.26465" y2="7.79145" layer="21"/>
<rectangle x1="9.35355" y1="7.77875" x2="9.63295" y2="7.79145" layer="21"/>
<rectangle x1="9.72185" y1="7.77875" x2="10.29335" y2="7.79145" layer="21"/>
<rectangle x1="10.38225" y1="7.77875" x2="10.78865" y2="7.79145" layer="21"/>
<rectangle x1="11.30935" y1="7.77875" x2="11.75385" y2="7.79145" layer="21"/>
<rectangle x1="11.84275" y1="7.77875" x2="12.28725" y2="7.79145" layer="21"/>
<rectangle x1="12.80795" y1="7.77875" x2="13.39215" y2="7.79145" layer="21"/>
<rectangle x1="13.86205" y1="7.77875" x2="13.93825" y2="7.79145" layer="21"/>
<rectangle x1="14.02715" y1="7.77875" x2="14.37005" y2="7.79145" layer="21"/>
<rectangle x1="14.45895" y1="7.77875" x2="14.91615" y2="7.79145" layer="21"/>
<rectangle x1="15.43685" y1="7.77875" x2="15.86865" y2="7.79145" layer="21"/>
<rectangle x1="16.12265" y1="7.77875" x2="16.18615" y2="7.79145" layer="21"/>
<rectangle x1="16.41475" y1="7.77875" x2="17.32915" y2="7.79145" layer="21"/>
<rectangle x1="4.76885" y1="7.79145" x2="6.02615" y2="7.80415" layer="21"/>
<rectangle x1="6.12775" y1="7.79145" x2="6.66115" y2="7.80415" layer="21"/>
<rectangle x1="6.85165" y1="7.79145" x2="7.01675" y2="7.80415" layer="21"/>
<rectangle x1="7.20725" y1="7.79145" x2="7.68985" y2="7.80415" layer="21"/>
<rectangle x1="7.89305" y1="7.79145" x2="8.04545" y2="7.80415" layer="21"/>
<rectangle x1="8.23595" y1="7.79145" x2="8.60425" y2="7.80415" layer="21"/>
<rectangle x1="8.69315" y1="7.79145" x2="9.26465" y2="7.80415" layer="21"/>
<rectangle x1="9.35355" y1="7.79145" x2="9.63295" y2="7.80415" layer="21"/>
<rectangle x1="9.72185" y1="7.79145" x2="10.29335" y2="7.80415" layer="21"/>
<rectangle x1="10.38225" y1="7.79145" x2="10.76325" y2="7.80415" layer="21"/>
<rectangle x1="10.96645" y1="7.79145" x2="11.11885" y2="7.80415" layer="21"/>
<rectangle x1="11.32205" y1="7.79145" x2="11.75385" y2="7.80415" layer="21"/>
<rectangle x1="11.84275" y1="7.79145" x2="12.26185" y2="7.80415" layer="21"/>
<rectangle x1="12.46505" y1="7.79145" x2="12.61745" y2="7.80415" layer="21"/>
<rectangle x1="12.82065" y1="7.79145" x2="13.36675" y2="7.80415" layer="21"/>
<rectangle x1="13.88745" y1="7.79145" x2="13.93825" y2="7.80415" layer="21"/>
<rectangle x1="14.02715" y1="7.79145" x2="14.37005" y2="7.80415" layer="21"/>
<rectangle x1="14.45895" y1="7.79145" x2="14.90345" y2="7.80415" layer="21"/>
<rectangle x1="15.09395" y1="7.79145" x2="15.25905" y2="7.80415" layer="21"/>
<rectangle x1="15.44955" y1="7.79145" x2="15.85595" y2="7.80415" layer="21"/>
<rectangle x1="16.03375" y1="7.79145" x2="16.26235" y2="7.80415" layer="21"/>
<rectangle x1="16.42745" y1="7.79145" x2="17.32915" y2="7.80415" layer="21"/>
<rectangle x1="4.76885" y1="7.80415" x2="6.02615" y2="7.81685" layer="21"/>
<rectangle x1="6.12775" y1="7.80415" x2="6.64845" y2="7.81685" layer="21"/>
<rectangle x1="6.81355" y1="7.80415" x2="7.06755" y2="7.81685" layer="21"/>
<rectangle x1="7.23265" y1="7.80415" x2="7.67715" y2="7.81685" layer="21"/>
<rectangle x1="7.84225" y1="7.80415" x2="8.08355" y2="7.81685" layer="21"/>
<rectangle x1="8.24865" y1="7.80415" x2="8.60425" y2="7.81685" layer="21"/>
<rectangle x1="8.69315" y1="7.80415" x2="9.26465" y2="7.81685" layer="21"/>
<rectangle x1="9.35355" y1="7.80415" x2="9.63295" y2="7.81685" layer="21"/>
<rectangle x1="9.72185" y1="7.80415" x2="10.29335" y2="7.81685" layer="21"/>
<rectangle x1="10.38225" y1="7.80415" x2="10.75055" y2="7.81685" layer="21"/>
<rectangle x1="10.92835" y1="7.80415" x2="11.16965" y2="7.81685" layer="21"/>
<rectangle x1="11.33475" y1="7.80415" x2="11.75385" y2="7.81685" layer="21"/>
<rectangle x1="11.84275" y1="7.80415" x2="12.24915" y2="7.81685" layer="21"/>
<rectangle x1="12.42695" y1="7.80415" x2="12.66825" y2="7.81685" layer="21"/>
<rectangle x1="12.83335" y1="7.80415" x2="13.35405" y2="7.81685" layer="21"/>
<rectangle x1="13.54455" y1="7.80415" x2="13.70965" y2="7.81685" layer="21"/>
<rectangle x1="13.90015" y1="7.80415" x2="13.93825" y2="7.81685" layer="21"/>
<rectangle x1="14.02715" y1="7.80415" x2="14.37005" y2="7.81685" layer="21"/>
<rectangle x1="14.45895" y1="7.80415" x2="14.89075" y2="7.81685" layer="21"/>
<rectangle x1="15.05585" y1="7.80415" x2="15.30985" y2="7.81685" layer="21"/>
<rectangle x1="15.47495" y1="7.80415" x2="15.84325" y2="7.81685" layer="21"/>
<rectangle x1="15.99565" y1="7.80415" x2="16.30045" y2="7.81685" layer="21"/>
<rectangle x1="16.44015" y1="7.80415" x2="17.32915" y2="7.81685" layer="21"/>
<rectangle x1="4.76885" y1="7.81685" x2="6.02615" y2="7.82955" layer="21"/>
<rectangle x1="6.12775" y1="7.81685" x2="6.62305" y2="7.82955" layer="21"/>
<rectangle x1="6.77545" y1="7.81685" x2="7.09295" y2="7.82955" layer="21"/>
<rectangle x1="7.23265" y1="7.81685" x2="7.66445" y2="7.82955" layer="21"/>
<rectangle x1="7.81685" y1="7.81685" x2="8.10895" y2="7.82955" layer="21"/>
<rectangle x1="8.26135" y1="7.81685" x2="8.60425" y2="7.82955" layer="21"/>
<rectangle x1="8.69315" y1="7.81685" x2="9.26465" y2="7.82955" layer="21"/>
<rectangle x1="9.35355" y1="7.81685" x2="9.63295" y2="7.82955" layer="21"/>
<rectangle x1="9.72185" y1="7.81685" x2="10.29335" y2="7.82955" layer="21"/>
<rectangle x1="10.38225" y1="7.81685" x2="10.73785" y2="7.82955" layer="21"/>
<rectangle x1="10.90295" y1="7.81685" x2="11.19505" y2="7.82955" layer="21"/>
<rectangle x1="11.34745" y1="7.81685" x2="11.75385" y2="7.82955" layer="21"/>
<rectangle x1="11.84275" y1="7.81685" x2="12.23645" y2="7.82955" layer="21"/>
<rectangle x1="12.40155" y1="7.81685" x2="12.69365" y2="7.82955" layer="21"/>
<rectangle x1="12.84605" y1="7.81685" x2="13.34135" y2="7.82955" layer="21"/>
<rectangle x1="13.50645" y1="7.81685" x2="13.74775" y2="7.82955" layer="21"/>
<rectangle x1="13.92555" y1="7.81685" x2="13.93825" y2="7.82955" layer="21"/>
<rectangle x1="14.02715" y1="7.81685" x2="14.37005" y2="7.82955" layer="21"/>
<rectangle x1="14.45895" y1="7.81685" x2="14.86535" y2="7.82955" layer="21"/>
<rectangle x1="15.01775" y1="7.81685" x2="15.33525" y2="7.82955" layer="21"/>
<rectangle x1="15.47495" y1="7.81685" x2="15.83055" y2="7.82955" layer="21"/>
<rectangle x1="15.97025" y1="7.81685" x2="16.32585" y2="7.82955" layer="21"/>
<rectangle x1="16.45285" y1="7.81685" x2="17.32915" y2="7.82955" layer="21"/>
<rectangle x1="4.76885" y1="7.82955" x2="6.02615" y2="7.84225" layer="21"/>
<rectangle x1="6.12775" y1="7.82955" x2="6.62305" y2="7.84225" layer="21"/>
<rectangle x1="6.76275" y1="7.82955" x2="7.11835" y2="7.84225" layer="21"/>
<rectangle x1="7.24535" y1="7.82955" x2="7.65175" y2="7.84225" layer="21"/>
<rectangle x1="7.79145" y1="7.82955" x2="8.13435" y2="7.84225" layer="21"/>
<rectangle x1="8.27405" y1="7.82955" x2="8.60425" y2="7.84225" layer="21"/>
<rectangle x1="8.69315" y1="7.82955" x2="9.26465" y2="7.84225" layer="21"/>
<rectangle x1="9.35355" y1="7.82955" x2="9.63295" y2="7.84225" layer="21"/>
<rectangle x1="9.72185" y1="7.82955" x2="10.29335" y2="7.84225" layer="21"/>
<rectangle x1="10.38225" y1="7.82955" x2="10.72515" y2="7.84225" layer="21"/>
<rectangle x1="10.87755" y1="7.82955" x2="11.22045" y2="7.84225" layer="21"/>
<rectangle x1="11.36015" y1="7.82955" x2="11.75385" y2="7.84225" layer="21"/>
<rectangle x1="11.84275" y1="7.82955" x2="12.22375" y2="7.84225" layer="21"/>
<rectangle x1="12.37615" y1="7.82955" x2="12.71905" y2="7.84225" layer="21"/>
<rectangle x1="12.85875" y1="7.82955" x2="13.32865" y2="7.84225" layer="21"/>
<rectangle x1="13.48105" y1="7.82955" x2="13.78585" y2="7.84225" layer="21"/>
<rectangle x1="14.02715" y1="7.82955" x2="14.37005" y2="7.84225" layer="21"/>
<rectangle x1="14.45895" y1="7.82955" x2="14.86535" y2="7.84225" layer="21"/>
<rectangle x1="15.00505" y1="7.82955" x2="15.36065" y2="7.84225" layer="21"/>
<rectangle x1="15.48765" y1="7.82955" x2="15.81785" y2="7.84225" layer="21"/>
<rectangle x1="15.94485" y1="7.82955" x2="16.33855" y2="7.84225" layer="21"/>
<rectangle x1="16.46555" y1="7.82955" x2="17.32915" y2="7.84225" layer="21"/>
<rectangle x1="4.76885" y1="7.84225" x2="6.02615" y2="7.85495" layer="21"/>
<rectangle x1="6.12775" y1="7.84225" x2="6.61035" y2="7.85495" layer="21"/>
<rectangle x1="6.73735" y1="7.84225" x2="7.13105" y2="7.85495" layer="21"/>
<rectangle x1="7.25805" y1="7.84225" x2="7.63905" y2="7.85495" layer="21"/>
<rectangle x1="7.76605" y1="7.84225" x2="8.14705" y2="7.85495" layer="21"/>
<rectangle x1="8.27405" y1="7.84225" x2="8.60425" y2="7.85495" layer="21"/>
<rectangle x1="8.69315" y1="7.84225" x2="9.26465" y2="7.85495" layer="21"/>
<rectangle x1="9.35355" y1="7.84225" x2="9.63295" y2="7.85495" layer="21"/>
<rectangle x1="9.72185" y1="7.84225" x2="10.29335" y2="7.85495" layer="21"/>
<rectangle x1="10.38225" y1="7.84225" x2="10.71245" y2="7.85495" layer="21"/>
<rectangle x1="10.85215" y1="7.84225" x2="11.24585" y2="7.85495" layer="21"/>
<rectangle x1="11.37285" y1="7.84225" x2="11.75385" y2="7.85495" layer="21"/>
<rectangle x1="11.84275" y1="7.84225" x2="12.21105" y2="7.85495" layer="21"/>
<rectangle x1="12.35075" y1="7.84225" x2="12.74445" y2="7.85495" layer="21"/>
<rectangle x1="12.87145" y1="7.84225" x2="13.31595" y2="7.85495" layer="21"/>
<rectangle x1="13.45565" y1="7.84225" x2="13.81125" y2="7.85495" layer="21"/>
<rectangle x1="14.02715" y1="7.84225" x2="14.37005" y2="7.85495" layer="21"/>
<rectangle x1="14.45895" y1="7.84225" x2="14.85265" y2="7.85495" layer="21"/>
<rectangle x1="14.97965" y1="7.84225" x2="15.37335" y2="7.85495" layer="21"/>
<rectangle x1="15.50035" y1="7.84225" x2="15.81785" y2="7.85495" layer="21"/>
<rectangle x1="15.93215" y1="7.84225" x2="16.36395" y2="7.85495" layer="21"/>
<rectangle x1="16.46555" y1="7.84225" x2="17.32915" y2="7.85495" layer="21"/>
<rectangle x1="4.76885" y1="7.85495" x2="6.02615" y2="7.86765" layer="21"/>
<rectangle x1="6.12775" y1="7.85495" x2="6.59765" y2="7.86765" layer="21"/>
<rectangle x1="6.72465" y1="7.85495" x2="7.14375" y2="7.86765" layer="21"/>
<rectangle x1="7.27075" y1="7.85495" x2="7.62635" y2="7.86765" layer="21"/>
<rectangle x1="7.75335" y1="7.85495" x2="8.17245" y2="7.86765" layer="21"/>
<rectangle x1="8.28675" y1="7.85495" x2="8.60425" y2="7.86765" layer="21"/>
<rectangle x1="8.69315" y1="7.85495" x2="9.26465" y2="7.86765" layer="21"/>
<rectangle x1="9.35355" y1="7.85495" x2="9.63295" y2="7.86765" layer="21"/>
<rectangle x1="9.72185" y1="7.85495" x2="10.29335" y2="7.86765" layer="21"/>
<rectangle x1="10.38225" y1="7.85495" x2="10.69975" y2="7.86765" layer="21"/>
<rectangle x1="10.83945" y1="7.85495" x2="11.25855" y2="7.86765" layer="21"/>
<rectangle x1="11.38555" y1="7.85495" x2="11.75385" y2="7.86765" layer="21"/>
<rectangle x1="11.84275" y1="7.85495" x2="12.19835" y2="7.86765" layer="21"/>
<rectangle x1="12.33805" y1="7.85495" x2="12.75715" y2="7.86765" layer="21"/>
<rectangle x1="12.88415" y1="7.85495" x2="13.30325" y2="7.86765" layer="21"/>
<rectangle x1="13.44295" y1="7.85495" x2="13.83665" y2="7.86765" layer="21"/>
<rectangle x1="14.02715" y1="7.85495" x2="14.37005" y2="7.86765" layer="21"/>
<rectangle x1="14.45895" y1="7.85495" x2="14.83995" y2="7.86765" layer="21"/>
<rectangle x1="14.96695" y1="7.85495" x2="15.38605" y2="7.86765" layer="21"/>
<rectangle x1="15.51305" y1="7.85495" x2="15.80515" y2="7.86765" layer="21"/>
<rectangle x1="15.91945" y1="7.85495" x2="16.36395" y2="7.86765" layer="21"/>
<rectangle x1="16.47825" y1="7.85495" x2="17.32915" y2="7.86765" layer="21"/>
<rectangle x1="4.76885" y1="7.86765" x2="6.02615" y2="7.88035" layer="21"/>
<rectangle x1="6.12775" y1="7.86765" x2="6.58495" y2="7.88035" layer="21"/>
<rectangle x1="6.71195" y1="7.86765" x2="7.15645" y2="7.88035" layer="21"/>
<rectangle x1="7.27075" y1="7.86765" x2="7.61365" y2="7.88035" layer="21"/>
<rectangle x1="7.74065" y1="7.86765" x2="8.18515" y2="7.88035" layer="21"/>
<rectangle x1="8.29945" y1="7.86765" x2="8.60425" y2="7.88035" layer="21"/>
<rectangle x1="8.69315" y1="7.86765" x2="9.26465" y2="7.88035" layer="21"/>
<rectangle x1="9.35355" y1="7.86765" x2="9.63295" y2="7.88035" layer="21"/>
<rectangle x1="9.72185" y1="7.86765" x2="10.29335" y2="7.88035" layer="21"/>
<rectangle x1="10.38225" y1="7.86765" x2="10.69975" y2="7.88035" layer="21"/>
<rectangle x1="10.82675" y1="7.86765" x2="11.27125" y2="7.88035" layer="21"/>
<rectangle x1="11.39825" y1="7.86765" x2="11.75385" y2="7.88035" layer="21"/>
<rectangle x1="11.84275" y1="7.86765" x2="12.19835" y2="7.88035" layer="21"/>
<rectangle x1="12.32535" y1="7.86765" x2="12.76985" y2="7.88035" layer="21"/>
<rectangle x1="12.89685" y1="7.86765" x2="13.29055" y2="7.88035" layer="21"/>
<rectangle x1="13.41755" y1="7.86765" x2="13.84935" y2="7.88035" layer="21"/>
<rectangle x1="14.02715" y1="7.86765" x2="14.37005" y2="7.88035" layer="21"/>
<rectangle x1="14.45895" y1="7.86765" x2="14.82725" y2="7.88035" layer="21"/>
<rectangle x1="14.95425" y1="7.86765" x2="15.39875" y2="7.88035" layer="21"/>
<rectangle x1="15.51305" y1="7.86765" x2="15.79245" y2="7.88035" layer="21"/>
<rectangle x1="15.90675" y1="7.86765" x2="16.37665" y2="7.88035" layer="21"/>
<rectangle x1="16.47825" y1="7.86765" x2="17.32915" y2="7.88035" layer="21"/>
<rectangle x1="4.76885" y1="7.88035" x2="6.02615" y2="7.89305" layer="21"/>
<rectangle x1="6.12775" y1="7.88035" x2="6.57225" y2="7.89305" layer="21"/>
<rectangle x1="6.69925" y1="7.88035" x2="7.16915" y2="7.89305" layer="21"/>
<rectangle x1="7.28345" y1="7.88035" x2="7.61365" y2="7.89305" layer="21"/>
<rectangle x1="7.72795" y1="7.88035" x2="8.19785" y2="7.89305" layer="21"/>
<rectangle x1="8.29945" y1="7.88035" x2="8.60425" y2="7.89305" layer="21"/>
<rectangle x1="8.69315" y1="7.88035" x2="9.26465" y2="7.89305" layer="21"/>
<rectangle x1="9.35355" y1="7.88035" x2="9.63295" y2="7.89305" layer="21"/>
<rectangle x1="9.72185" y1="7.88035" x2="10.29335" y2="7.89305" layer="21"/>
<rectangle x1="10.38225" y1="7.88035" x2="10.68705" y2="7.89305" layer="21"/>
<rectangle x1="10.81405" y1="7.88035" x2="11.28395" y2="7.89305" layer="21"/>
<rectangle x1="11.41095" y1="7.88035" x2="11.75385" y2="7.89305" layer="21"/>
<rectangle x1="11.84275" y1="7.88035" x2="12.18565" y2="7.89305" layer="21"/>
<rectangle x1="12.31265" y1="7.88035" x2="12.78255" y2="7.89305" layer="21"/>
<rectangle x1="12.90955" y1="7.88035" x2="13.27785" y2="7.89305" layer="21"/>
<rectangle x1="13.40485" y1="7.88035" x2="13.87475" y2="7.89305" layer="21"/>
<rectangle x1="14.02715" y1="7.88035" x2="14.37005" y2="7.89305" layer="21"/>
<rectangle x1="14.45895" y1="7.88035" x2="14.81455" y2="7.89305" layer="21"/>
<rectangle x1="14.94155" y1="7.88035" x2="15.41145" y2="7.89305" layer="21"/>
<rectangle x1="15.52575" y1="7.88035" x2="15.79245" y2="7.89305" layer="21"/>
<rectangle x1="15.89405" y1="7.88035" x2="16.38935" y2="7.89305" layer="21"/>
<rectangle x1="16.47825" y1="7.88035" x2="17.32915" y2="7.89305" layer="21"/>
<rectangle x1="4.76885" y1="7.89305" x2="6.02615" y2="7.90575" layer="21"/>
<rectangle x1="6.12775" y1="7.89305" x2="6.57225" y2="7.90575" layer="21"/>
<rectangle x1="6.68655" y1="7.89305" x2="7.18185" y2="7.90575" layer="21"/>
<rectangle x1="7.28345" y1="7.89305" x2="7.60095" y2="7.90575" layer="21"/>
<rectangle x1="7.71525" y1="7.89305" x2="8.19785" y2="7.90575" layer="21"/>
<rectangle x1="8.31215" y1="7.89305" x2="8.60425" y2="7.90575" layer="21"/>
<rectangle x1="8.69315" y1="7.89305" x2="9.26465" y2="7.90575" layer="21"/>
<rectangle x1="9.35355" y1="7.89305" x2="9.63295" y2="7.90575" layer="21"/>
<rectangle x1="9.72185" y1="7.89305" x2="10.29335" y2="7.90575" layer="21"/>
<rectangle x1="10.38225" y1="7.89305" x2="10.67435" y2="7.90575" layer="21"/>
<rectangle x1="10.80135" y1="7.89305" x2="11.29665" y2="7.90575" layer="21"/>
<rectangle x1="11.41095" y1="7.89305" x2="11.75385" y2="7.90575" layer="21"/>
<rectangle x1="11.84275" y1="7.89305" x2="12.17295" y2="7.90575" layer="21"/>
<rectangle x1="12.29995" y1="7.89305" x2="12.79525" y2="7.90575" layer="21"/>
<rectangle x1="12.90955" y1="7.89305" x2="13.27785" y2="7.90575" layer="21"/>
<rectangle x1="13.39215" y1="7.89305" x2="13.88745" y2="7.90575" layer="21"/>
<rectangle x1="14.02715" y1="7.89305" x2="14.37005" y2="7.90575" layer="21"/>
<rectangle x1="14.45895" y1="7.89305" x2="14.81455" y2="7.90575" layer="21"/>
<rectangle x1="14.92885" y1="7.89305" x2="15.42415" y2="7.90575" layer="21"/>
<rectangle x1="15.52575" y1="7.89305" x2="15.79245" y2="7.90575" layer="21"/>
<rectangle x1="15.89405" y1="7.89305" x2="16.38935" y2="7.90575" layer="21"/>
<rectangle x1="16.47825" y1="7.89305" x2="17.32915" y2="7.90575" layer="21"/>
<rectangle x1="4.76885" y1="7.90575" x2="6.02615" y2="7.91845" layer="21"/>
<rectangle x1="6.12775" y1="7.90575" x2="6.55955" y2="7.91845" layer="21"/>
<rectangle x1="6.67385" y1="7.90575" x2="7.18185" y2="7.91845" layer="21"/>
<rectangle x1="7.29615" y1="7.90575" x2="7.58825" y2="7.91845" layer="21"/>
<rectangle x1="7.70255" y1="7.90575" x2="8.21055" y2="7.91845" layer="21"/>
<rectangle x1="8.31215" y1="7.90575" x2="8.60425" y2="7.91845" layer="21"/>
<rectangle x1="8.69315" y1="7.90575" x2="9.26465" y2="7.91845" layer="21"/>
<rectangle x1="9.35355" y1="7.90575" x2="9.63295" y2="7.91845" layer="21"/>
<rectangle x1="9.72185" y1="7.90575" x2="10.29335" y2="7.91845" layer="21"/>
<rectangle x1="10.38225" y1="7.90575" x2="10.67435" y2="7.91845" layer="21"/>
<rectangle x1="10.78865" y1="7.90575" x2="11.30935" y2="7.91845" layer="21"/>
<rectangle x1="11.42365" y1="7.90575" x2="11.75385" y2="7.91845" layer="21"/>
<rectangle x1="11.84275" y1="7.90575" x2="12.17295" y2="7.91845" layer="21"/>
<rectangle x1="12.28725" y1="7.90575" x2="12.80795" y2="7.91845" layer="21"/>
<rectangle x1="12.92225" y1="7.90575" x2="13.26515" y2="7.91845" layer="21"/>
<rectangle x1="13.37945" y1="7.90575" x2="13.90015" y2="7.91845" layer="21"/>
<rectangle x1="14.02715" y1="7.90575" x2="14.37005" y2="7.91845" layer="21"/>
<rectangle x1="14.45895" y1="7.90575" x2="14.80185" y2="7.91845" layer="21"/>
<rectangle x1="14.91615" y1="7.90575" x2="15.42415" y2="7.91845" layer="21"/>
<rectangle x1="15.53845" y1="7.90575" x2="15.77975" y2="7.91845" layer="21"/>
<rectangle x1="15.88135" y1="7.90575" x2="16.38935" y2="7.91845" layer="21"/>
<rectangle x1="16.49095" y1="7.90575" x2="17.32915" y2="7.91845" layer="21"/>
<rectangle x1="4.76885" y1="7.91845" x2="6.02615" y2="7.93115" layer="21"/>
<rectangle x1="6.12775" y1="7.91845" x2="6.55955" y2="7.93115" layer="21"/>
<rectangle x1="6.66115" y1="7.91845" x2="7.58825" y2="7.93115" layer="21"/>
<rectangle x1="7.70255" y1="7.91845" x2="8.22325" y2="7.93115" layer="21"/>
<rectangle x1="8.32485" y1="7.91845" x2="8.60425" y2="7.93115" layer="21"/>
<rectangle x1="8.69315" y1="7.91845" x2="9.26465" y2="7.93115" layer="21"/>
<rectangle x1="9.35355" y1="7.91845" x2="9.63295" y2="7.93115" layer="21"/>
<rectangle x1="9.72185" y1="7.91845" x2="10.29335" y2="7.93115" layer="21"/>
<rectangle x1="10.38225" y1="7.91845" x2="10.66165" y2="7.93115" layer="21"/>
<rectangle x1="10.77595" y1="7.91845" x2="11.32205" y2="7.93115" layer="21"/>
<rectangle x1="11.43635" y1="7.91845" x2="11.75385" y2="7.93115" layer="21"/>
<rectangle x1="11.84275" y1="7.91845" x2="12.16025" y2="7.93115" layer="21"/>
<rectangle x1="12.27455" y1="7.91845" x2="12.82065" y2="7.93115" layer="21"/>
<rectangle x1="12.93495" y1="7.91845" x2="13.25245" y2="7.93115" layer="21"/>
<rectangle x1="13.36675" y1="7.91845" x2="13.91285" y2="7.93115" layer="21"/>
<rectangle x1="14.02715" y1="7.91845" x2="14.37005" y2="7.93115" layer="21"/>
<rectangle x1="14.45895" y1="7.91845" x2="14.80185" y2="7.93115" layer="21"/>
<rectangle x1="14.90345" y1="7.91845" x2="15.77975" y2="7.93115" layer="21"/>
<rectangle x1="15.86865" y1="7.91845" x2="16.40205" y2="7.93115" layer="21"/>
<rectangle x1="16.49095" y1="7.91845" x2="17.32915" y2="7.93115" layer="21"/>
<rectangle x1="4.76885" y1="7.93115" x2="6.02615" y2="7.94385" layer="21"/>
<rectangle x1="6.12775" y1="7.93115" x2="6.54685" y2="7.94385" layer="21"/>
<rectangle x1="6.66115" y1="7.93115" x2="7.57555" y2="7.94385" layer="21"/>
<rectangle x1="7.68985" y1="7.93115" x2="8.22325" y2="7.94385" layer="21"/>
<rectangle x1="8.32485" y1="7.93115" x2="8.60425" y2="7.94385" layer="21"/>
<rectangle x1="8.69315" y1="7.93115" x2="9.26465" y2="7.94385" layer="21"/>
<rectangle x1="9.35355" y1="7.93115" x2="9.63295" y2="7.94385" layer="21"/>
<rectangle x1="9.72185" y1="7.93115" x2="10.29335" y2="7.94385" layer="21"/>
<rectangle x1="10.38225" y1="7.93115" x2="10.66165" y2="7.94385" layer="21"/>
<rectangle x1="10.76325" y1="7.93115" x2="11.33475" y2="7.94385" layer="21"/>
<rectangle x1="11.43635" y1="7.93115" x2="11.75385" y2="7.94385" layer="21"/>
<rectangle x1="11.84275" y1="7.93115" x2="12.16025" y2="7.94385" layer="21"/>
<rectangle x1="12.26185" y1="7.93115" x2="12.83335" y2="7.94385" layer="21"/>
<rectangle x1="12.93495" y1="7.93115" x2="13.25245" y2="7.94385" layer="21"/>
<rectangle x1="13.36675" y1="7.93115" x2="13.92555" y2="7.94385" layer="21"/>
<rectangle x1="14.02715" y1="7.93115" x2="14.37005" y2="7.94385" layer="21"/>
<rectangle x1="14.45895" y1="7.93115" x2="14.78915" y2="7.94385" layer="21"/>
<rectangle x1="14.90345" y1="7.93115" x2="15.77975" y2="7.94385" layer="21"/>
<rectangle x1="15.86865" y1="7.93115" x2="16.40205" y2="7.94385" layer="21"/>
<rectangle x1="16.49095" y1="7.93115" x2="17.32915" y2="7.94385" layer="21"/>
<rectangle x1="4.76885" y1="7.94385" x2="6.02615" y2="7.95655" layer="21"/>
<rectangle x1="6.12775" y1="7.94385" x2="6.54685" y2="7.95655" layer="21"/>
<rectangle x1="6.64845" y1="7.94385" x2="7.57555" y2="7.95655" layer="21"/>
<rectangle x1="7.67715" y1="7.94385" x2="8.23595" y2="7.95655" layer="21"/>
<rectangle x1="8.33755" y1="7.94385" x2="8.60425" y2="7.95655" layer="21"/>
<rectangle x1="8.69315" y1="7.94385" x2="9.26465" y2="7.95655" layer="21"/>
<rectangle x1="9.35355" y1="7.94385" x2="9.63295" y2="7.95655" layer="21"/>
<rectangle x1="9.72185" y1="7.94385" x2="10.29335" y2="7.95655" layer="21"/>
<rectangle x1="10.38225" y1="7.94385" x2="10.64895" y2="7.95655" layer="21"/>
<rectangle x1="10.76325" y1="7.94385" x2="11.33475" y2="7.95655" layer="21"/>
<rectangle x1="11.44905" y1="7.94385" x2="11.75385" y2="7.95655" layer="21"/>
<rectangle x1="11.84275" y1="7.94385" x2="12.14755" y2="7.95655" layer="21"/>
<rectangle x1="12.26185" y1="7.94385" x2="12.83335" y2="7.95655" layer="21"/>
<rectangle x1="12.94765" y1="7.94385" x2="13.23975" y2="7.95655" layer="21"/>
<rectangle x1="13.35405" y1="7.94385" x2="13.92555" y2="7.95655" layer="21"/>
<rectangle x1="14.02715" y1="7.94385" x2="14.37005" y2="7.95655" layer="21"/>
<rectangle x1="14.45895" y1="7.94385" x2="14.78915" y2="7.95655" layer="21"/>
<rectangle x1="14.89075" y1="7.94385" x2="15.76705" y2="7.95655" layer="21"/>
<rectangle x1="15.86865" y1="7.94385" x2="16.40205" y2="7.95655" layer="21"/>
<rectangle x1="16.49095" y1="7.94385" x2="17.32915" y2="7.95655" layer="21"/>
<rectangle x1="4.76885" y1="7.95655" x2="6.02615" y2="7.96925" layer="21"/>
<rectangle x1="6.12775" y1="7.95655" x2="6.53415" y2="7.96925" layer="21"/>
<rectangle x1="6.63575" y1="7.95655" x2="7.56285" y2="7.96925" layer="21"/>
<rectangle x1="7.67715" y1="7.95655" x2="8.23595" y2="7.96925" layer="21"/>
<rectangle x1="8.33755" y1="7.95655" x2="8.60425" y2="7.96925" layer="21"/>
<rectangle x1="8.69315" y1="7.95655" x2="9.26465" y2="7.96925" layer="21"/>
<rectangle x1="9.35355" y1="7.95655" x2="9.63295" y2="7.96925" layer="21"/>
<rectangle x1="9.72185" y1="7.95655" x2="10.29335" y2="7.96925" layer="21"/>
<rectangle x1="10.38225" y1="7.95655" x2="10.64895" y2="7.96925" layer="21"/>
<rectangle x1="10.75055" y1="7.95655" x2="11.34745" y2="7.96925" layer="21"/>
<rectangle x1="11.44905" y1="7.95655" x2="11.75385" y2="7.96925" layer="21"/>
<rectangle x1="11.84275" y1="7.95655" x2="12.14755" y2="7.96925" layer="21"/>
<rectangle x1="12.24915" y1="7.95655" x2="12.84605" y2="7.96925" layer="21"/>
<rectangle x1="12.94765" y1="7.95655" x2="13.23975" y2="7.96925" layer="21"/>
<rectangle x1="13.34135" y1="7.95655" x2="13.93825" y2="7.96925" layer="21"/>
<rectangle x1="14.02715" y1="7.95655" x2="14.37005" y2="7.96925" layer="21"/>
<rectangle x1="14.45895" y1="7.95655" x2="14.77645" y2="7.96925" layer="21"/>
<rectangle x1="14.87805" y1="7.95655" x2="15.76705" y2="7.96925" layer="21"/>
<rectangle x1="15.85595" y1="7.95655" x2="16.40205" y2="7.96925" layer="21"/>
<rectangle x1="16.49095" y1="7.95655" x2="17.32915" y2="7.96925" layer="21"/>
<rectangle x1="4.76885" y1="7.96925" x2="6.02615" y2="7.98195" layer="21"/>
<rectangle x1="6.12775" y1="7.96925" x2="6.53415" y2="7.98195" layer="21"/>
<rectangle x1="6.63575" y1="7.96925" x2="7.56285" y2="7.98195" layer="21"/>
<rectangle x1="7.66445" y1="7.96925" x2="8.23595" y2="7.98195" layer="21"/>
<rectangle x1="8.33755" y1="7.96925" x2="8.60425" y2="7.98195" layer="21"/>
<rectangle x1="8.69315" y1="7.96925" x2="9.26465" y2="7.98195" layer="21"/>
<rectangle x1="9.35355" y1="7.96925" x2="9.63295" y2="7.98195" layer="21"/>
<rectangle x1="9.72185" y1="7.96925" x2="10.29335" y2="7.98195" layer="21"/>
<rectangle x1="10.38225" y1="7.96925" x2="10.63625" y2="7.98195" layer="21"/>
<rectangle x1="10.73785" y1="7.96925" x2="11.34745" y2="7.98195" layer="21"/>
<rectangle x1="11.44905" y1="7.96925" x2="11.75385" y2="7.98195" layer="21"/>
<rectangle x1="11.84275" y1="7.96925" x2="12.13485" y2="7.98195" layer="21"/>
<rectangle x1="12.23645" y1="7.96925" x2="12.84605" y2="7.98195" layer="21"/>
<rectangle x1="12.94765" y1="7.96925" x2="13.22705" y2="7.98195" layer="21"/>
<rectangle x1="13.34135" y1="7.96925" x2="13.93825" y2="7.98195" layer="21"/>
<rectangle x1="14.02715" y1="7.96925" x2="14.37005" y2="7.98195" layer="21"/>
<rectangle x1="14.45895" y1="7.96925" x2="14.77645" y2="7.98195" layer="21"/>
<rectangle x1="14.87805" y1="7.96925" x2="16.38935" y2="7.98195" layer="21"/>
<rectangle x1="16.49095" y1="7.96925" x2="17.32915" y2="7.98195" layer="21"/>
<rectangle x1="4.76885" y1="7.98195" x2="6.02615" y2="7.99465" layer="21"/>
<rectangle x1="6.12775" y1="7.98195" x2="6.53415" y2="7.99465" layer="21"/>
<rectangle x1="6.63575" y1="7.98195" x2="7.56285" y2="7.99465" layer="21"/>
<rectangle x1="7.66445" y1="7.98195" x2="8.24865" y2="7.99465" layer="21"/>
<rectangle x1="8.33755" y1="7.98195" x2="8.60425" y2="7.99465" layer="21"/>
<rectangle x1="8.69315" y1="7.98195" x2="9.26465" y2="7.99465" layer="21"/>
<rectangle x1="9.35355" y1="7.98195" x2="9.63295" y2="7.99465" layer="21"/>
<rectangle x1="9.72185" y1="7.98195" x2="10.29335" y2="7.99465" layer="21"/>
<rectangle x1="10.38225" y1="7.98195" x2="10.63625" y2="7.99465" layer="21"/>
<rectangle x1="10.73785" y1="7.98195" x2="11.36015" y2="7.99465" layer="21"/>
<rectangle x1="11.46175" y1="7.98195" x2="11.75385" y2="7.99465" layer="21"/>
<rectangle x1="11.84275" y1="7.98195" x2="12.13485" y2="7.99465" layer="21"/>
<rectangle x1="12.23645" y1="7.98195" x2="12.85875" y2="7.99465" layer="21"/>
<rectangle x1="12.96035" y1="7.98195" x2="13.22705" y2="7.99465" layer="21"/>
<rectangle x1="13.32865" y1="7.98195" x2="13.93825" y2="7.99465" layer="21"/>
<rectangle x1="14.02715" y1="7.98195" x2="14.37005" y2="7.99465" layer="21"/>
<rectangle x1="14.45895" y1="7.98195" x2="14.77645" y2="7.99465" layer="21"/>
<rectangle x1="14.87805" y1="7.98195" x2="16.38935" y2="7.99465" layer="21"/>
<rectangle x1="16.49095" y1="7.98195" x2="17.32915" y2="7.99465" layer="21"/>
<rectangle x1="4.76885" y1="7.99465" x2="6.02615" y2="8.00735" layer="21"/>
<rectangle x1="6.12775" y1="7.99465" x2="6.52145" y2="8.00735" layer="21"/>
<rectangle x1="6.62305" y1="7.99465" x2="7.55015" y2="8.00735" layer="21"/>
<rectangle x1="7.65175" y1="7.99465" x2="8.60425" y2="8.00735" layer="21"/>
<rectangle x1="8.69315" y1="7.99465" x2="9.26465" y2="8.00735" layer="21"/>
<rectangle x1="9.35355" y1="7.99465" x2="9.63295" y2="8.00735" layer="21"/>
<rectangle x1="9.72185" y1="7.99465" x2="10.29335" y2="8.00735" layer="21"/>
<rectangle x1="10.38225" y1="7.99465" x2="10.62355" y2="8.00735" layer="21"/>
<rectangle x1="10.73785" y1="7.99465" x2="11.36015" y2="8.00735" layer="21"/>
<rectangle x1="11.46175" y1="7.99465" x2="11.75385" y2="8.00735" layer="21"/>
<rectangle x1="11.84275" y1="7.99465" x2="12.12215" y2="8.00735" layer="21"/>
<rectangle x1="12.23645" y1="7.99465" x2="12.85875" y2="8.00735" layer="21"/>
<rectangle x1="12.96035" y1="7.99465" x2="13.22705" y2="8.00735" layer="21"/>
<rectangle x1="13.32865" y1="7.99465" x2="13.93825" y2="8.00735" layer="21"/>
<rectangle x1="14.02715" y1="7.99465" x2="14.37005" y2="8.00735" layer="21"/>
<rectangle x1="14.45895" y1="7.99465" x2="14.76375" y2="8.00735" layer="21"/>
<rectangle x1="14.86535" y1="7.99465" x2="16.38935" y2="8.00735" layer="21"/>
<rectangle x1="16.47825" y1="7.99465" x2="17.32915" y2="8.00735" layer="21"/>
<rectangle x1="4.76885" y1="8.00735" x2="6.02615" y2="8.02005" layer="21"/>
<rectangle x1="6.12775" y1="8.00735" x2="6.52145" y2="8.02005" layer="21"/>
<rectangle x1="6.62305" y1="8.00735" x2="7.55015" y2="8.02005" layer="21"/>
<rectangle x1="7.65175" y1="8.00735" x2="8.60425" y2="8.02005" layer="21"/>
<rectangle x1="8.69315" y1="8.00735" x2="9.26465" y2="8.02005" layer="21"/>
<rectangle x1="9.35355" y1="8.00735" x2="9.63295" y2="8.02005" layer="21"/>
<rectangle x1="9.72185" y1="8.00735" x2="10.29335" y2="8.02005" layer="21"/>
<rectangle x1="10.38225" y1="8.00735" x2="10.62355" y2="8.02005" layer="21"/>
<rectangle x1="10.72515" y1="8.00735" x2="11.37285" y2="8.02005" layer="21"/>
<rectangle x1="11.46175" y1="8.00735" x2="11.75385" y2="8.02005" layer="21"/>
<rectangle x1="11.84275" y1="8.00735" x2="12.12215" y2="8.02005" layer="21"/>
<rectangle x1="12.22375" y1="8.00735" x2="12.87145" y2="8.02005" layer="21"/>
<rectangle x1="12.96035" y1="8.00735" x2="13.21435" y2="8.02005" layer="21"/>
<rectangle x1="13.31595" y1="8.00735" x2="13.93825" y2="8.02005" layer="21"/>
<rectangle x1="14.02715" y1="8.00735" x2="14.37005" y2="8.02005" layer="21"/>
<rectangle x1="14.45895" y1="8.00735" x2="14.76375" y2="8.02005" layer="21"/>
<rectangle x1="14.86535" y1="8.00735" x2="16.37665" y2="8.02005" layer="21"/>
<rectangle x1="16.47825" y1="8.00735" x2="17.32915" y2="8.02005" layer="21"/>
<rectangle x1="4.76885" y1="8.02005" x2="6.02615" y2="8.03275" layer="21"/>
<rectangle x1="6.12775" y1="8.02005" x2="6.52145" y2="8.03275" layer="21"/>
<rectangle x1="6.62305" y1="8.02005" x2="7.55015" y2="8.03275" layer="21"/>
<rectangle x1="7.65175" y1="8.02005" x2="8.60425" y2="8.03275" layer="21"/>
<rectangle x1="8.69315" y1="8.02005" x2="9.26465" y2="8.03275" layer="21"/>
<rectangle x1="9.35355" y1="8.02005" x2="9.63295" y2="8.03275" layer="21"/>
<rectangle x1="9.72185" y1="8.02005" x2="10.29335" y2="8.03275" layer="21"/>
<rectangle x1="10.38225" y1="8.02005" x2="10.62355" y2="8.03275" layer="21"/>
<rectangle x1="10.72515" y1="8.02005" x2="11.37285" y2="8.03275" layer="21"/>
<rectangle x1="11.47445" y1="8.02005" x2="11.75385" y2="8.03275" layer="21"/>
<rectangle x1="11.84275" y1="8.02005" x2="12.12215" y2="8.03275" layer="21"/>
<rectangle x1="12.22375" y1="8.02005" x2="12.87145" y2="8.03275" layer="21"/>
<rectangle x1="12.97305" y1="8.02005" x2="13.21435" y2="8.03275" layer="21"/>
<rectangle x1="13.31595" y1="8.02005" x2="13.93825" y2="8.03275" layer="21"/>
<rectangle x1="14.02715" y1="8.02005" x2="14.37005" y2="8.03275" layer="21"/>
<rectangle x1="14.45895" y1="8.02005" x2="14.76375" y2="8.03275" layer="21"/>
<rectangle x1="14.86535" y1="8.02005" x2="16.36395" y2="8.03275" layer="21"/>
<rectangle x1="16.47825" y1="8.02005" x2="17.32915" y2="8.03275" layer="21"/>
<rectangle x1="4.76885" y1="8.03275" x2="6.02615" y2="8.04545" layer="21"/>
<rectangle x1="6.12775" y1="8.03275" x2="6.52145" y2="8.04545" layer="21"/>
<rectangle x1="6.61035" y1="8.03275" x2="7.55015" y2="8.04545" layer="21"/>
<rectangle x1="7.63905" y1="8.03275" x2="8.60425" y2="8.04545" layer="21"/>
<rectangle x1="8.69315" y1="8.03275" x2="9.26465" y2="8.04545" layer="21"/>
<rectangle x1="9.35355" y1="8.03275" x2="9.63295" y2="8.04545" layer="21"/>
<rectangle x1="9.72185" y1="8.03275" x2="10.29335" y2="8.04545" layer="21"/>
<rectangle x1="10.38225" y1="8.03275" x2="10.62355" y2="8.04545" layer="21"/>
<rectangle x1="10.72515" y1="8.03275" x2="11.37285" y2="8.04545" layer="21"/>
<rectangle x1="11.47445" y1="8.03275" x2="11.75385" y2="8.04545" layer="21"/>
<rectangle x1="11.84275" y1="8.03275" x2="12.12215" y2="8.04545" layer="21"/>
<rectangle x1="12.22375" y1="8.03275" x2="12.87145" y2="8.04545" layer="21"/>
<rectangle x1="12.97305" y1="8.03275" x2="13.21435" y2="8.04545" layer="21"/>
<rectangle x1="13.31595" y1="8.03275" x2="13.93825" y2="8.04545" layer="21"/>
<rectangle x1="14.02715" y1="8.03275" x2="14.37005" y2="8.04545" layer="21"/>
<rectangle x1="14.45895" y1="8.03275" x2="14.76375" y2="8.04545" layer="21"/>
<rectangle x1="14.85265" y1="8.03275" x2="16.35125" y2="8.04545" layer="21"/>
<rectangle x1="16.46555" y1="8.03275" x2="17.32915" y2="8.04545" layer="21"/>
<rectangle x1="4.76885" y1="8.04545" x2="6.02615" y2="8.05815" layer="21"/>
<rectangle x1="6.12775" y1="8.04545" x2="6.50875" y2="8.05815" layer="21"/>
<rectangle x1="6.61035" y1="8.04545" x2="7.53745" y2="8.05815" layer="21"/>
<rectangle x1="7.63905" y1="8.04545" x2="8.60425" y2="8.05815" layer="21"/>
<rectangle x1="8.69315" y1="8.04545" x2="9.26465" y2="8.05815" layer="21"/>
<rectangle x1="9.35355" y1="8.04545" x2="9.63295" y2="8.05815" layer="21"/>
<rectangle x1="9.72185" y1="8.04545" x2="10.29335" y2="8.05815" layer="21"/>
<rectangle x1="10.38225" y1="8.04545" x2="10.62355" y2="8.05815" layer="21"/>
<rectangle x1="10.71245" y1="8.04545" x2="11.37285" y2="8.05815" layer="21"/>
<rectangle x1="11.47445" y1="8.04545" x2="11.75385" y2="8.05815" layer="21"/>
<rectangle x1="11.84275" y1="8.04545" x2="12.12215" y2="8.05815" layer="21"/>
<rectangle x1="12.21105" y1="8.04545" x2="12.87145" y2="8.05815" layer="21"/>
<rectangle x1="12.97305" y1="8.04545" x2="13.21435" y2="8.05815" layer="21"/>
<rectangle x1="13.30325" y1="8.04545" x2="13.93825" y2="8.05815" layer="21"/>
<rectangle x1="14.02715" y1="8.04545" x2="14.37005" y2="8.05815" layer="21"/>
<rectangle x1="14.45895" y1="8.04545" x2="14.75105" y2="8.05815" layer="21"/>
<rectangle x1="14.85265" y1="8.04545" x2="16.33855" y2="8.05815" layer="21"/>
<rectangle x1="16.46555" y1="8.04545" x2="17.32915" y2="8.05815" layer="21"/>
<rectangle x1="4.76885" y1="8.05815" x2="6.02615" y2="8.07085" layer="21"/>
<rectangle x1="6.12775" y1="8.05815" x2="6.50875" y2="8.07085" layer="21"/>
<rectangle x1="6.61035" y1="8.05815" x2="7.53745" y2="8.07085" layer="21"/>
<rectangle x1="7.63905" y1="8.05815" x2="8.60425" y2="8.07085" layer="21"/>
<rectangle x1="8.69315" y1="8.05815" x2="9.26465" y2="8.07085" layer="21"/>
<rectangle x1="9.35355" y1="8.05815" x2="9.63295" y2="8.07085" layer="21"/>
<rectangle x1="9.72185" y1="8.05815" x2="10.29335" y2="8.07085" layer="21"/>
<rectangle x1="10.38225" y1="8.05815" x2="10.61085" y2="8.07085" layer="21"/>
<rectangle x1="10.71245" y1="8.05815" x2="11.38555" y2="8.07085" layer="21"/>
<rectangle x1="11.47445" y1="8.05815" x2="11.75385" y2="8.07085" layer="21"/>
<rectangle x1="11.84275" y1="8.05815" x2="12.10945" y2="8.07085" layer="21"/>
<rectangle x1="12.21105" y1="8.05815" x2="12.88415" y2="8.07085" layer="21"/>
<rectangle x1="12.97305" y1="8.05815" x2="13.20165" y2="8.07085" layer="21"/>
<rectangle x1="13.30325" y1="8.05815" x2="13.93825" y2="8.07085" layer="21"/>
<rectangle x1="14.02715" y1="8.05815" x2="14.37005" y2="8.07085" layer="21"/>
<rectangle x1="14.45895" y1="8.05815" x2="14.75105" y2="8.07085" layer="21"/>
<rectangle x1="14.85265" y1="8.05815" x2="16.31315" y2="8.07085" layer="21"/>
<rectangle x1="16.45285" y1="8.05815" x2="17.32915" y2="8.07085" layer="21"/>
<rectangle x1="4.76885" y1="8.07085" x2="6.02615" y2="8.08355" layer="21"/>
<rectangle x1="6.12775" y1="8.07085" x2="6.50875" y2="8.08355" layer="21"/>
<rectangle x1="6.61035" y1="8.07085" x2="7.53745" y2="8.08355" layer="21"/>
<rectangle x1="7.63905" y1="8.07085" x2="8.60425" y2="8.08355" layer="21"/>
<rectangle x1="8.69315" y1="8.07085" x2="9.26465" y2="8.08355" layer="21"/>
<rectangle x1="9.35355" y1="8.07085" x2="9.63295" y2="8.08355" layer="21"/>
<rectangle x1="9.72185" y1="8.07085" x2="10.29335" y2="8.08355" layer="21"/>
<rectangle x1="10.38225" y1="8.07085" x2="10.61085" y2="8.08355" layer="21"/>
<rectangle x1="10.71245" y1="8.07085" x2="11.38555" y2="8.08355" layer="21"/>
<rectangle x1="11.47445" y1="8.07085" x2="11.75385" y2="8.08355" layer="21"/>
<rectangle x1="11.84275" y1="8.07085" x2="12.10945" y2="8.08355" layer="21"/>
<rectangle x1="12.21105" y1="8.07085" x2="12.88415" y2="8.08355" layer="21"/>
<rectangle x1="12.97305" y1="8.07085" x2="13.20165" y2="8.08355" layer="21"/>
<rectangle x1="13.30325" y1="8.07085" x2="13.93825" y2="8.08355" layer="21"/>
<rectangle x1="14.02715" y1="8.07085" x2="14.37005" y2="8.08355" layer="21"/>
<rectangle x1="14.45895" y1="8.07085" x2="14.75105" y2="8.08355" layer="21"/>
<rectangle x1="14.85265" y1="8.07085" x2="16.28775" y2="8.08355" layer="21"/>
<rectangle x1="16.44015" y1="8.07085" x2="17.32915" y2="8.08355" layer="21"/>
<rectangle x1="4.76885" y1="8.08355" x2="6.02615" y2="8.09625" layer="21"/>
<rectangle x1="6.12775" y1="8.08355" x2="6.50875" y2="8.09625" layer="21"/>
<rectangle x1="6.61035" y1="8.08355" x2="7.53745" y2="8.09625" layer="21"/>
<rectangle x1="7.63905" y1="8.08355" x2="8.60425" y2="8.09625" layer="21"/>
<rectangle x1="8.69315" y1="8.08355" x2="9.26465" y2="8.09625" layer="21"/>
<rectangle x1="9.35355" y1="8.08355" x2="9.63295" y2="8.09625" layer="21"/>
<rectangle x1="9.72185" y1="8.08355" x2="10.29335" y2="8.09625" layer="21"/>
<rectangle x1="10.38225" y1="8.08355" x2="10.61085" y2="8.09625" layer="21"/>
<rectangle x1="10.71245" y1="8.08355" x2="11.38555" y2="8.09625" layer="21"/>
<rectangle x1="11.47445" y1="8.08355" x2="11.75385" y2="8.09625" layer="21"/>
<rectangle x1="11.84275" y1="8.08355" x2="12.10945" y2="8.09625" layer="21"/>
<rectangle x1="12.21105" y1="8.08355" x2="12.88415" y2="8.09625" layer="21"/>
<rectangle x1="12.97305" y1="8.08355" x2="13.20165" y2="8.09625" layer="21"/>
<rectangle x1="13.30325" y1="8.08355" x2="13.93825" y2="8.09625" layer="21"/>
<rectangle x1="14.02715" y1="8.08355" x2="14.37005" y2="8.09625" layer="21"/>
<rectangle x1="14.45895" y1="8.08355" x2="14.75105" y2="8.09625" layer="21"/>
<rectangle x1="14.85265" y1="8.08355" x2="16.22425" y2="8.09625" layer="21"/>
<rectangle x1="16.44015" y1="8.08355" x2="17.32915" y2="8.09625" layer="21"/>
<rectangle x1="4.76885" y1="8.09625" x2="6.02615" y2="8.10895" layer="21"/>
<rectangle x1="6.12775" y1="8.09625" x2="6.50875" y2="8.10895" layer="21"/>
<rectangle x1="6.61035" y1="8.09625" x2="7.53745" y2="8.10895" layer="21"/>
<rectangle x1="7.63905" y1="8.09625" x2="8.60425" y2="8.10895" layer="21"/>
<rectangle x1="8.69315" y1="8.09625" x2="9.26465" y2="8.10895" layer="21"/>
<rectangle x1="9.35355" y1="8.09625" x2="9.63295" y2="8.10895" layer="21"/>
<rectangle x1="9.72185" y1="8.09625" x2="10.29335" y2="8.10895" layer="21"/>
<rectangle x1="10.38225" y1="8.09625" x2="10.61085" y2="8.10895" layer="21"/>
<rectangle x1="10.71245" y1="8.09625" x2="11.38555" y2="8.10895" layer="21"/>
<rectangle x1="11.47445" y1="8.09625" x2="11.75385" y2="8.10895" layer="21"/>
<rectangle x1="11.84275" y1="8.09625" x2="12.10945" y2="8.10895" layer="21"/>
<rectangle x1="12.21105" y1="8.09625" x2="12.88415" y2="8.10895" layer="21"/>
<rectangle x1="12.97305" y1="8.09625" x2="13.20165" y2="8.10895" layer="21"/>
<rectangle x1="13.30325" y1="8.09625" x2="13.93825" y2="8.10895" layer="21"/>
<rectangle x1="14.02715" y1="8.09625" x2="14.37005" y2="8.10895" layer="21"/>
<rectangle x1="14.45895" y1="8.09625" x2="14.75105" y2="8.10895" layer="21"/>
<rectangle x1="14.85265" y1="8.09625" x2="16.14805" y2="8.10895" layer="21"/>
<rectangle x1="16.42745" y1="8.09625" x2="17.32915" y2="8.10895" layer="21"/>
<rectangle x1="4.76885" y1="8.10895" x2="6.02615" y2="8.12165" layer="21"/>
<rectangle x1="6.12775" y1="8.10895" x2="6.50875" y2="8.12165" layer="21"/>
<rectangle x1="7.33425" y1="8.10895" x2="7.53745" y2="8.12165" layer="21"/>
<rectangle x1="7.63905" y1="8.10895" x2="8.60425" y2="8.12165" layer="21"/>
<rectangle x1="8.69315" y1="8.10895" x2="9.26465" y2="8.12165" layer="21"/>
<rectangle x1="9.35355" y1="8.10895" x2="9.63295" y2="8.12165" layer="21"/>
<rectangle x1="9.72185" y1="8.10895" x2="10.29335" y2="8.12165" layer="21"/>
<rectangle x1="10.38225" y1="8.10895" x2="10.61085" y2="8.12165" layer="21"/>
<rectangle x1="10.71245" y1="8.10895" x2="11.38555" y2="8.12165" layer="21"/>
<rectangle x1="11.47445" y1="8.10895" x2="11.75385" y2="8.12165" layer="21"/>
<rectangle x1="11.84275" y1="8.10895" x2="12.10945" y2="8.12165" layer="21"/>
<rectangle x1="12.21105" y1="8.10895" x2="12.88415" y2="8.12165" layer="21"/>
<rectangle x1="12.97305" y1="8.10895" x2="13.20165" y2="8.12165" layer="21"/>
<rectangle x1="13.30325" y1="8.10895" x2="13.93825" y2="8.12165" layer="21"/>
<rectangle x1="14.02715" y1="8.10895" x2="14.37005" y2="8.12165" layer="21"/>
<rectangle x1="14.45895" y1="8.10895" x2="14.75105" y2="8.12165" layer="21"/>
<rectangle x1="15.57655" y1="8.10895" x2="16.07185" y2="8.12165" layer="21"/>
<rectangle x1="16.40205" y1="8.10895" x2="17.32915" y2="8.12165" layer="21"/>
<rectangle x1="4.76885" y1="8.12165" x2="6.02615" y2="8.13435" layer="21"/>
<rectangle x1="6.12775" y1="8.12165" x2="6.50875" y2="8.13435" layer="21"/>
<rectangle x1="7.33425" y1="8.12165" x2="7.53745" y2="8.13435" layer="21"/>
<rectangle x1="7.62635" y1="8.12165" x2="8.60425" y2="8.13435" layer="21"/>
<rectangle x1="8.69315" y1="8.12165" x2="9.26465" y2="8.13435" layer="21"/>
<rectangle x1="9.35355" y1="8.12165" x2="9.63295" y2="8.13435" layer="21"/>
<rectangle x1="9.72185" y1="8.12165" x2="10.29335" y2="8.13435" layer="21"/>
<rectangle x1="10.38225" y1="8.12165" x2="10.61085" y2="8.13435" layer="21"/>
<rectangle x1="10.71245" y1="8.12165" x2="11.38555" y2="8.13435" layer="21"/>
<rectangle x1="11.47445" y1="8.12165" x2="11.75385" y2="8.13435" layer="21"/>
<rectangle x1="11.84275" y1="8.12165" x2="12.10945" y2="8.13435" layer="21"/>
<rectangle x1="12.19835" y1="8.12165" x2="12.88415" y2="8.13435" layer="21"/>
<rectangle x1="12.97305" y1="8.12165" x2="13.20165" y2="8.13435" layer="21"/>
<rectangle x1="13.30325" y1="8.12165" x2="13.93825" y2="8.13435" layer="21"/>
<rectangle x1="14.02715" y1="8.12165" x2="14.37005" y2="8.13435" layer="21"/>
<rectangle x1="14.45895" y1="8.12165" x2="14.75105" y2="8.13435" layer="21"/>
<rectangle x1="15.57655" y1="8.12165" x2="16.00835" y2="8.13435" layer="21"/>
<rectangle x1="16.38935" y1="8.12165" x2="17.32915" y2="8.13435" layer="21"/>
<rectangle x1="4.76885" y1="8.13435" x2="6.02615" y2="8.14705" layer="21"/>
<rectangle x1="6.12775" y1="8.13435" x2="6.50875" y2="8.14705" layer="21"/>
<rectangle x1="7.33425" y1="8.13435" x2="7.53745" y2="8.14705" layer="21"/>
<rectangle x1="7.63905" y1="8.13435" x2="8.60425" y2="8.14705" layer="21"/>
<rectangle x1="8.69315" y1="8.13435" x2="9.26465" y2="8.14705" layer="21"/>
<rectangle x1="9.35355" y1="8.13435" x2="9.63295" y2="8.14705" layer="21"/>
<rectangle x1="9.72185" y1="8.13435" x2="10.29335" y2="8.14705" layer="21"/>
<rectangle x1="10.38225" y1="8.13435" x2="10.61085" y2="8.14705" layer="21"/>
<rectangle x1="10.71245" y1="8.13435" x2="11.38555" y2="8.14705" layer="21"/>
<rectangle x1="11.47445" y1="8.13435" x2="11.75385" y2="8.14705" layer="21"/>
<rectangle x1="11.84275" y1="8.13435" x2="12.10945" y2="8.14705" layer="21"/>
<rectangle x1="12.21105" y1="8.13435" x2="12.88415" y2="8.14705" layer="21"/>
<rectangle x1="12.97305" y1="8.13435" x2="13.20165" y2="8.14705" layer="21"/>
<rectangle x1="13.29055" y1="8.13435" x2="13.93825" y2="8.14705" layer="21"/>
<rectangle x1="14.02715" y1="8.13435" x2="14.37005" y2="8.14705" layer="21"/>
<rectangle x1="14.45895" y1="8.13435" x2="14.75105" y2="8.14705" layer="21"/>
<rectangle x1="15.57655" y1="8.13435" x2="15.95755" y2="8.14705" layer="21"/>
<rectangle x1="16.36395" y1="8.13435" x2="17.32915" y2="8.14705" layer="21"/>
<rectangle x1="4.76885" y1="8.14705" x2="6.02615" y2="8.15975" layer="21"/>
<rectangle x1="6.12775" y1="8.14705" x2="6.50875" y2="8.15975" layer="21"/>
<rectangle x1="7.33425" y1="8.14705" x2="7.53745" y2="8.15975" layer="21"/>
<rectangle x1="7.63905" y1="8.14705" x2="8.60425" y2="8.15975" layer="21"/>
<rectangle x1="8.69315" y1="8.14705" x2="9.26465" y2="8.15975" layer="21"/>
<rectangle x1="9.35355" y1="8.14705" x2="9.63295" y2="8.15975" layer="21"/>
<rectangle x1="9.72185" y1="8.14705" x2="10.29335" y2="8.15975" layer="21"/>
<rectangle x1="10.38225" y1="8.14705" x2="10.61085" y2="8.15975" layer="21"/>
<rectangle x1="10.71245" y1="8.14705" x2="11.38555" y2="8.15975" layer="21"/>
<rectangle x1="11.47445" y1="8.14705" x2="11.75385" y2="8.15975" layer="21"/>
<rectangle x1="11.84275" y1="8.14705" x2="12.10945" y2="8.15975" layer="21"/>
<rectangle x1="12.21105" y1="8.14705" x2="12.88415" y2="8.15975" layer="21"/>
<rectangle x1="12.97305" y1="8.14705" x2="13.20165" y2="8.15975" layer="21"/>
<rectangle x1="13.30325" y1="8.14705" x2="13.93825" y2="8.15975" layer="21"/>
<rectangle x1="14.02715" y1="8.14705" x2="14.37005" y2="8.15975" layer="21"/>
<rectangle x1="14.45895" y1="8.14705" x2="14.75105" y2="8.15975" layer="21"/>
<rectangle x1="15.57655" y1="8.14705" x2="15.93215" y2="8.15975" layer="21"/>
<rectangle x1="16.32585" y1="8.14705" x2="17.32915" y2="8.15975" layer="21"/>
<rectangle x1="4.76885" y1="8.15975" x2="6.02615" y2="8.17245" layer="21"/>
<rectangle x1="6.12775" y1="8.15975" x2="6.50875" y2="8.17245" layer="21"/>
<rectangle x1="7.33425" y1="8.15975" x2="7.53745" y2="8.17245" layer="21"/>
<rectangle x1="7.63905" y1="8.15975" x2="8.60425" y2="8.17245" layer="21"/>
<rectangle x1="8.69315" y1="8.15975" x2="9.26465" y2="8.17245" layer="21"/>
<rectangle x1="9.35355" y1="8.15975" x2="9.63295" y2="8.17245" layer="21"/>
<rectangle x1="9.72185" y1="8.15975" x2="10.29335" y2="8.17245" layer="21"/>
<rectangle x1="10.38225" y1="8.15975" x2="10.61085" y2="8.17245" layer="21"/>
<rectangle x1="10.71245" y1="8.15975" x2="11.38555" y2="8.17245" layer="21"/>
<rectangle x1="11.47445" y1="8.15975" x2="11.75385" y2="8.17245" layer="21"/>
<rectangle x1="11.84275" y1="8.15975" x2="12.10945" y2="8.17245" layer="21"/>
<rectangle x1="12.21105" y1="8.15975" x2="12.88415" y2="8.17245" layer="21"/>
<rectangle x1="12.97305" y1="8.15975" x2="13.20165" y2="8.17245" layer="21"/>
<rectangle x1="13.30325" y1="8.15975" x2="13.93825" y2="8.17245" layer="21"/>
<rectangle x1="14.02715" y1="8.15975" x2="14.37005" y2="8.17245" layer="21"/>
<rectangle x1="14.45895" y1="8.15975" x2="14.75105" y2="8.17245" layer="21"/>
<rectangle x1="15.57655" y1="8.15975" x2="15.90675" y2="8.17245" layer="21"/>
<rectangle x1="16.28775" y1="8.15975" x2="17.32915" y2="8.17245" layer="21"/>
<rectangle x1="4.76885" y1="8.17245" x2="6.02615" y2="8.18515" layer="21"/>
<rectangle x1="6.12775" y1="8.17245" x2="6.50875" y2="8.18515" layer="21"/>
<rectangle x1="6.61035" y1="8.17245" x2="7.23265" y2="8.18515" layer="21"/>
<rectangle x1="7.33425" y1="8.17245" x2="7.53745" y2="8.18515" layer="21"/>
<rectangle x1="7.63905" y1="8.17245" x2="8.60425" y2="8.18515" layer="21"/>
<rectangle x1="8.69315" y1="8.17245" x2="9.26465" y2="8.18515" layer="21"/>
<rectangle x1="9.35355" y1="8.17245" x2="9.63295" y2="8.18515" layer="21"/>
<rectangle x1="9.72185" y1="8.17245" x2="10.29335" y2="8.18515" layer="21"/>
<rectangle x1="10.38225" y1="8.17245" x2="10.61085" y2="8.18515" layer="21"/>
<rectangle x1="10.71245" y1="8.17245" x2="11.38555" y2="8.18515" layer="21"/>
<rectangle x1="11.47445" y1="8.17245" x2="11.75385" y2="8.18515" layer="21"/>
<rectangle x1="11.84275" y1="8.17245" x2="12.10945" y2="8.18515" layer="21"/>
<rectangle x1="12.21105" y1="8.17245" x2="12.88415" y2="8.18515" layer="21"/>
<rectangle x1="12.97305" y1="8.17245" x2="13.20165" y2="8.18515" layer="21"/>
<rectangle x1="13.30325" y1="8.17245" x2="13.93825" y2="8.18515" layer="21"/>
<rectangle x1="14.02715" y1="8.17245" x2="14.37005" y2="8.18515" layer="21"/>
<rectangle x1="14.45895" y1="8.17245" x2="14.75105" y2="8.18515" layer="21"/>
<rectangle x1="14.85265" y1="8.17245" x2="15.47495" y2="8.18515" layer="21"/>
<rectangle x1="15.57655" y1="8.17245" x2="15.88135" y2="8.18515" layer="21"/>
<rectangle x1="16.21155" y1="8.17245" x2="17.32915" y2="8.18515" layer="21"/>
<rectangle x1="4.76885" y1="8.18515" x2="6.02615" y2="8.19785" layer="21"/>
<rectangle x1="6.12775" y1="8.18515" x2="6.50875" y2="8.19785" layer="21"/>
<rectangle x1="6.61035" y1="8.18515" x2="7.23265" y2="8.19785" layer="21"/>
<rectangle x1="7.33425" y1="8.18515" x2="7.53745" y2="8.19785" layer="21"/>
<rectangle x1="7.63905" y1="8.18515" x2="8.60425" y2="8.19785" layer="21"/>
<rectangle x1="8.69315" y1="8.18515" x2="9.26465" y2="8.19785" layer="21"/>
<rectangle x1="9.35355" y1="8.18515" x2="9.63295" y2="8.19785" layer="21"/>
<rectangle x1="9.72185" y1="8.18515" x2="10.29335" y2="8.19785" layer="21"/>
<rectangle x1="10.38225" y1="8.18515" x2="10.61085" y2="8.19785" layer="21"/>
<rectangle x1="10.71245" y1="8.18515" x2="11.38555" y2="8.19785" layer="21"/>
<rectangle x1="11.47445" y1="8.18515" x2="11.75385" y2="8.19785" layer="21"/>
<rectangle x1="11.84275" y1="8.18515" x2="12.10945" y2="8.19785" layer="21"/>
<rectangle x1="12.21105" y1="8.18515" x2="12.88415" y2="8.19785" layer="21"/>
<rectangle x1="12.97305" y1="8.18515" x2="13.20165" y2="8.19785" layer="21"/>
<rectangle x1="13.30325" y1="8.18515" x2="13.93825" y2="8.19785" layer="21"/>
<rectangle x1="14.02715" y1="8.18515" x2="14.37005" y2="8.19785" layer="21"/>
<rectangle x1="14.45895" y1="8.18515" x2="14.75105" y2="8.19785" layer="21"/>
<rectangle x1="14.85265" y1="8.18515" x2="15.47495" y2="8.19785" layer="21"/>
<rectangle x1="15.57655" y1="8.18515" x2="15.86865" y2="8.19785" layer="21"/>
<rectangle x1="16.13535" y1="8.18515" x2="17.32915" y2="8.19785" layer="21"/>
<rectangle x1="4.76885" y1="8.19785" x2="6.02615" y2="8.21055" layer="21"/>
<rectangle x1="6.12775" y1="8.19785" x2="6.50875" y2="8.21055" layer="21"/>
<rectangle x1="6.61035" y1="8.19785" x2="7.23265" y2="8.21055" layer="21"/>
<rectangle x1="7.32155" y1="8.19785" x2="7.53745" y2="8.21055" layer="21"/>
<rectangle x1="7.63905" y1="8.19785" x2="8.60425" y2="8.21055" layer="21"/>
<rectangle x1="8.69315" y1="8.19785" x2="9.26465" y2="8.21055" layer="21"/>
<rectangle x1="9.35355" y1="8.19785" x2="9.63295" y2="8.21055" layer="21"/>
<rectangle x1="9.72185" y1="8.19785" x2="10.29335" y2="8.21055" layer="21"/>
<rectangle x1="10.38225" y1="8.19785" x2="10.61085" y2="8.21055" layer="21"/>
<rectangle x1="10.71245" y1="8.19785" x2="11.38555" y2="8.21055" layer="21"/>
<rectangle x1="11.47445" y1="8.19785" x2="11.75385" y2="8.21055" layer="21"/>
<rectangle x1="11.84275" y1="8.19785" x2="12.10945" y2="8.21055" layer="21"/>
<rectangle x1="12.21105" y1="8.19785" x2="12.88415" y2="8.21055" layer="21"/>
<rectangle x1="12.97305" y1="8.19785" x2="13.20165" y2="8.21055" layer="21"/>
<rectangle x1="13.30325" y1="8.19785" x2="13.93825" y2="8.21055" layer="21"/>
<rectangle x1="14.02715" y1="8.19785" x2="14.37005" y2="8.21055" layer="21"/>
<rectangle x1="14.45895" y1="8.19785" x2="14.75105" y2="8.21055" layer="21"/>
<rectangle x1="14.85265" y1="8.19785" x2="15.47495" y2="8.21055" layer="21"/>
<rectangle x1="15.56385" y1="8.19785" x2="15.85595" y2="8.21055" layer="21"/>
<rectangle x1="16.05915" y1="8.19785" x2="17.32915" y2="8.21055" layer="21"/>
<rectangle x1="4.76885" y1="8.21055" x2="6.02615" y2="8.22325" layer="21"/>
<rectangle x1="6.12775" y1="8.21055" x2="6.50875" y2="8.22325" layer="21"/>
<rectangle x1="6.61035" y1="8.21055" x2="7.23265" y2="8.22325" layer="21"/>
<rectangle x1="7.32155" y1="8.21055" x2="7.55015" y2="8.22325" layer="21"/>
<rectangle x1="7.63905" y1="8.21055" x2="8.60425" y2="8.22325" layer="21"/>
<rectangle x1="8.69315" y1="8.21055" x2="9.26465" y2="8.22325" layer="21"/>
<rectangle x1="9.35355" y1="8.21055" x2="9.63295" y2="8.22325" layer="21"/>
<rectangle x1="9.72185" y1="8.21055" x2="10.29335" y2="8.22325" layer="21"/>
<rectangle x1="10.38225" y1="8.21055" x2="10.62355" y2="8.22325" layer="21"/>
<rectangle x1="10.71245" y1="8.21055" x2="11.37285" y2="8.22325" layer="21"/>
<rectangle x1="11.47445" y1="8.21055" x2="11.75385" y2="8.22325" layer="21"/>
<rectangle x1="11.84275" y1="8.21055" x2="12.12215" y2="8.22325" layer="21"/>
<rectangle x1="12.21105" y1="8.21055" x2="12.87145" y2="8.22325" layer="21"/>
<rectangle x1="12.97305" y1="8.21055" x2="13.20165" y2="8.22325" layer="21"/>
<rectangle x1="13.30325" y1="8.21055" x2="13.93825" y2="8.22325" layer="21"/>
<rectangle x1="14.02715" y1="8.21055" x2="14.37005" y2="8.22325" layer="21"/>
<rectangle x1="14.45895" y1="8.21055" x2="14.75105" y2="8.22325" layer="21"/>
<rectangle x1="14.85265" y1="8.21055" x2="15.47495" y2="8.22325" layer="21"/>
<rectangle x1="15.56385" y1="8.21055" x2="15.84325" y2="8.22325" layer="21"/>
<rectangle x1="16.00835" y1="8.21055" x2="17.32915" y2="8.22325" layer="21"/>
<rectangle x1="4.76885" y1="8.22325" x2="6.02615" y2="8.23595" layer="21"/>
<rectangle x1="6.12775" y1="8.22325" x2="6.52145" y2="8.23595" layer="21"/>
<rectangle x1="6.61035" y1="8.22325" x2="7.23265" y2="8.23595" layer="21"/>
<rectangle x1="7.32155" y1="8.22325" x2="7.55015" y2="8.23595" layer="21"/>
<rectangle x1="7.65175" y1="8.22325" x2="8.60425" y2="8.23595" layer="21"/>
<rectangle x1="8.69315" y1="8.22325" x2="9.26465" y2="8.23595" layer="21"/>
<rectangle x1="9.35355" y1="8.22325" x2="9.63295" y2="8.23595" layer="21"/>
<rectangle x1="9.72185" y1="8.22325" x2="10.29335" y2="8.23595" layer="21"/>
<rectangle x1="10.38225" y1="8.22325" x2="10.62355" y2="8.23595" layer="21"/>
<rectangle x1="10.72515" y1="8.22325" x2="11.37285" y2="8.23595" layer="21"/>
<rectangle x1="11.47445" y1="8.22325" x2="11.75385" y2="8.23595" layer="21"/>
<rectangle x1="11.84275" y1="8.22325" x2="12.12215" y2="8.23595" layer="21"/>
<rectangle x1="12.22375" y1="8.22325" x2="12.87145" y2="8.23595" layer="21"/>
<rectangle x1="12.97305" y1="8.22325" x2="13.21435" y2="8.23595" layer="21"/>
<rectangle x1="13.30325" y1="8.22325" x2="13.93825" y2="8.23595" layer="21"/>
<rectangle x1="14.02715" y1="8.22325" x2="14.37005" y2="8.23595" layer="21"/>
<rectangle x1="14.45895" y1="8.22325" x2="14.76375" y2="8.23595" layer="21"/>
<rectangle x1="14.85265" y1="8.22325" x2="15.47495" y2="8.23595" layer="21"/>
<rectangle x1="15.56385" y1="8.22325" x2="15.83055" y2="8.23595" layer="21"/>
<rectangle x1="15.98295" y1="8.22325" x2="17.32915" y2="8.23595" layer="21"/>
<rectangle x1="4.76885" y1="8.23595" x2="6.02615" y2="8.24865" layer="21"/>
<rectangle x1="6.12775" y1="8.23595" x2="6.52145" y2="8.24865" layer="21"/>
<rectangle x1="6.62305" y1="8.23595" x2="7.21995" y2="8.24865" layer="21"/>
<rectangle x1="7.32155" y1="8.23595" x2="7.55015" y2="8.24865" layer="21"/>
<rectangle x1="7.65175" y1="8.23595" x2="8.60425" y2="8.24865" layer="21"/>
<rectangle x1="8.69315" y1="8.23595" x2="9.26465" y2="8.24865" layer="21"/>
<rectangle x1="9.35355" y1="8.23595" x2="9.63295" y2="8.24865" layer="21"/>
<rectangle x1="9.72185" y1="8.23595" x2="10.29335" y2="8.24865" layer="21"/>
<rectangle x1="10.38225" y1="8.23595" x2="10.62355" y2="8.24865" layer="21"/>
<rectangle x1="10.72515" y1="8.23595" x2="11.37285" y2="8.24865" layer="21"/>
<rectangle x1="11.47445" y1="8.23595" x2="11.75385" y2="8.24865" layer="21"/>
<rectangle x1="11.84275" y1="8.23595" x2="12.12215" y2="8.24865" layer="21"/>
<rectangle x1="12.22375" y1="8.23595" x2="12.87145" y2="8.24865" layer="21"/>
<rectangle x1="12.97305" y1="8.23595" x2="13.21435" y2="8.24865" layer="21"/>
<rectangle x1="13.31595" y1="8.23595" x2="13.93825" y2="8.24865" layer="21"/>
<rectangle x1="14.02715" y1="8.23595" x2="14.37005" y2="8.24865" layer="21"/>
<rectangle x1="14.45895" y1="8.23595" x2="14.76375" y2="8.24865" layer="21"/>
<rectangle x1="14.86535" y1="8.23595" x2="15.46225" y2="8.24865" layer="21"/>
<rectangle x1="15.56385" y1="8.23595" x2="15.83055" y2="8.24865" layer="21"/>
<rectangle x1="15.95755" y1="8.23595" x2="17.32915" y2="8.24865" layer="21"/>
<rectangle x1="4.76885" y1="8.24865" x2="6.02615" y2="8.26135" layer="21"/>
<rectangle x1="6.12775" y1="8.24865" x2="6.52145" y2="8.26135" layer="21"/>
<rectangle x1="6.62305" y1="8.24865" x2="7.21995" y2="8.26135" layer="21"/>
<rectangle x1="7.32155" y1="8.24865" x2="7.55015" y2="8.26135" layer="21"/>
<rectangle x1="7.65175" y1="8.24865" x2="8.60425" y2="8.26135" layer="21"/>
<rectangle x1="8.69315" y1="8.24865" x2="9.25195" y2="8.26135" layer="21"/>
<rectangle x1="9.35355" y1="8.24865" x2="9.63295" y2="8.26135" layer="21"/>
<rectangle x1="9.72185" y1="8.24865" x2="10.28065" y2="8.26135" layer="21"/>
<rectangle x1="10.38225" y1="8.24865" x2="10.62355" y2="8.26135" layer="21"/>
<rectangle x1="10.72515" y1="8.24865" x2="11.36015" y2="8.26135" layer="21"/>
<rectangle x1="11.46175" y1="8.24865" x2="11.75385" y2="8.26135" layer="21"/>
<rectangle x1="11.84275" y1="8.24865" x2="12.12215" y2="8.26135" layer="21"/>
<rectangle x1="12.22375" y1="8.24865" x2="12.85875" y2="8.26135" layer="21"/>
<rectangle x1="12.96035" y1="8.24865" x2="13.21435" y2="8.26135" layer="21"/>
<rectangle x1="13.31595" y1="8.24865" x2="13.93825" y2="8.26135" layer="21"/>
<rectangle x1="14.02715" y1="8.24865" x2="14.37005" y2="8.26135" layer="21"/>
<rectangle x1="14.45895" y1="8.24865" x2="14.76375" y2="8.26135" layer="21"/>
<rectangle x1="14.86535" y1="8.24865" x2="15.46225" y2="8.26135" layer="21"/>
<rectangle x1="15.56385" y1="8.24865" x2="15.81785" y2="8.26135" layer="21"/>
<rectangle x1="15.94485" y1="8.24865" x2="17.32915" y2="8.26135" layer="21"/>
<rectangle x1="4.76885" y1="8.26135" x2="6.02615" y2="8.27405" layer="21"/>
<rectangle x1="6.12775" y1="8.26135" x2="6.52145" y2="8.27405" layer="21"/>
<rectangle x1="6.62305" y1="8.26135" x2="7.21995" y2="8.27405" layer="21"/>
<rectangle x1="7.30885" y1="8.26135" x2="7.55015" y2="8.27405" layer="21"/>
<rectangle x1="7.65175" y1="8.26135" x2="8.60425" y2="8.27405" layer="21"/>
<rectangle x1="8.69315" y1="8.26135" x2="9.25195" y2="8.27405" layer="21"/>
<rectangle x1="9.35355" y1="8.26135" x2="9.63295" y2="8.27405" layer="21"/>
<rectangle x1="9.72185" y1="8.26135" x2="10.28065" y2="8.27405" layer="21"/>
<rectangle x1="10.38225" y1="8.26135" x2="10.62355" y2="8.27405" layer="21"/>
<rectangle x1="10.73785" y1="8.26135" x2="11.36015" y2="8.27405" layer="21"/>
<rectangle x1="11.46175" y1="8.26135" x2="11.75385" y2="8.27405" layer="21"/>
<rectangle x1="11.84275" y1="8.26135" x2="12.12215" y2="8.27405" layer="21"/>
<rectangle x1="12.23645" y1="8.26135" x2="12.85875" y2="8.27405" layer="21"/>
<rectangle x1="12.96035" y1="8.26135" x2="13.21435" y2="8.27405" layer="21"/>
<rectangle x1="13.31595" y1="8.26135" x2="13.93825" y2="8.27405" layer="21"/>
<rectangle x1="14.02715" y1="8.26135" x2="14.37005" y2="8.27405" layer="21"/>
<rectangle x1="14.45895" y1="8.26135" x2="14.76375" y2="8.27405" layer="21"/>
<rectangle x1="14.86535" y1="8.26135" x2="15.46225" y2="8.27405" layer="21"/>
<rectangle x1="15.55115" y1="8.26135" x2="15.81785" y2="8.27405" layer="21"/>
<rectangle x1="15.93215" y1="8.26135" x2="17.32915" y2="8.27405" layer="21"/>
<rectangle x1="4.76885" y1="8.27405" x2="6.02615" y2="8.28675" layer="21"/>
<rectangle x1="6.12775" y1="8.27405" x2="6.53415" y2="8.28675" layer="21"/>
<rectangle x1="6.63575" y1="8.27405" x2="7.20725" y2="8.28675" layer="21"/>
<rectangle x1="7.30885" y1="8.27405" x2="7.56285" y2="8.28675" layer="21"/>
<rectangle x1="7.66445" y1="8.27405" x2="8.24865" y2="8.28675" layer="21"/>
<rectangle x1="8.33755" y1="8.27405" x2="8.60425" y2="8.28675" layer="21"/>
<rectangle x1="8.69315" y1="8.27405" x2="9.25195" y2="8.28675" layer="21"/>
<rectangle x1="9.35355" y1="8.27405" x2="9.63295" y2="8.28675" layer="21"/>
<rectangle x1="9.72185" y1="8.27405" x2="10.28065" y2="8.28675" layer="21"/>
<rectangle x1="10.38225" y1="8.27405" x2="10.63625" y2="8.28675" layer="21"/>
<rectangle x1="10.73785" y1="8.27405" x2="11.36015" y2="8.28675" layer="21"/>
<rectangle x1="11.46175" y1="8.27405" x2="11.75385" y2="8.28675" layer="21"/>
<rectangle x1="11.84275" y1="8.27405" x2="12.13485" y2="8.28675" layer="21"/>
<rectangle x1="12.23645" y1="8.27405" x2="12.85875" y2="8.28675" layer="21"/>
<rectangle x1="12.96035" y1="8.27405" x2="13.22705" y2="8.28675" layer="21"/>
<rectangle x1="13.32865" y1="8.27405" x2="13.93825" y2="8.28675" layer="21"/>
<rectangle x1="14.02715" y1="8.27405" x2="14.37005" y2="8.28675" layer="21"/>
<rectangle x1="14.45895" y1="8.27405" x2="14.77645" y2="8.28675" layer="21"/>
<rectangle x1="14.87805" y1="8.27405" x2="15.44955" y2="8.28675" layer="21"/>
<rectangle x1="15.55115" y1="8.27405" x2="15.81785" y2="8.28675" layer="21"/>
<rectangle x1="15.91945" y1="8.27405" x2="17.32915" y2="8.28675" layer="21"/>
<rectangle x1="4.76885" y1="8.28675" x2="6.02615" y2="8.29945" layer="21"/>
<rectangle x1="6.12775" y1="8.28675" x2="6.53415" y2="8.29945" layer="21"/>
<rectangle x1="6.63575" y1="8.28675" x2="7.20725" y2="8.29945" layer="21"/>
<rectangle x1="7.30885" y1="8.28675" x2="7.56285" y2="8.29945" layer="21"/>
<rectangle x1="7.66445" y1="8.28675" x2="8.23595" y2="8.29945" layer="21"/>
<rectangle x1="8.33755" y1="8.28675" x2="8.60425" y2="8.29945" layer="21"/>
<rectangle x1="8.69315" y1="8.28675" x2="9.25195" y2="8.29945" layer="21"/>
<rectangle x1="9.34085" y1="8.28675" x2="9.63295" y2="8.29945" layer="21"/>
<rectangle x1="9.72185" y1="8.28675" x2="10.28065" y2="8.29945" layer="21"/>
<rectangle x1="10.36955" y1="8.28675" x2="10.63625" y2="8.29945" layer="21"/>
<rectangle x1="10.75055" y1="8.28675" x2="11.34745" y2="8.29945" layer="21"/>
<rectangle x1="11.44905" y1="8.28675" x2="11.75385" y2="8.29945" layer="21"/>
<rectangle x1="11.84275" y1="8.28675" x2="12.13485" y2="8.29945" layer="21"/>
<rectangle x1="12.24915" y1="8.28675" x2="12.84605" y2="8.29945" layer="21"/>
<rectangle x1="12.94765" y1="8.28675" x2="13.22705" y2="8.29945" layer="21"/>
<rectangle x1="13.32865" y1="8.28675" x2="13.93825" y2="8.29945" layer="21"/>
<rectangle x1="14.02715" y1="8.28675" x2="14.37005" y2="8.29945" layer="21"/>
<rectangle x1="14.45895" y1="8.28675" x2="14.77645" y2="8.29945" layer="21"/>
<rectangle x1="14.87805" y1="8.28675" x2="15.44955" y2="8.29945" layer="21"/>
<rectangle x1="15.55115" y1="8.28675" x2="15.80515" y2="8.29945" layer="21"/>
<rectangle x1="15.90675" y1="8.28675" x2="17.32915" y2="8.29945" layer="21"/>
<rectangle x1="4.76885" y1="8.29945" x2="6.02615" y2="8.31215" layer="21"/>
<rectangle x1="6.12775" y1="8.29945" x2="6.53415" y2="8.31215" layer="21"/>
<rectangle x1="6.63575" y1="8.29945" x2="7.20725" y2="8.31215" layer="21"/>
<rectangle x1="7.30885" y1="8.29945" x2="7.56285" y2="8.31215" layer="21"/>
<rectangle x1="7.67715" y1="8.29945" x2="8.23595" y2="8.31215" layer="21"/>
<rectangle x1="8.33755" y1="8.29945" x2="8.60425" y2="8.31215" layer="21"/>
<rectangle x1="8.69315" y1="8.29945" x2="9.25195" y2="8.31215" layer="21"/>
<rectangle x1="9.34085" y1="8.29945" x2="9.63295" y2="8.31215" layer="21"/>
<rectangle x1="9.72185" y1="8.29945" x2="10.28065" y2="8.31215" layer="21"/>
<rectangle x1="10.36955" y1="8.29945" x2="10.64895" y2="8.31215" layer="21"/>
<rectangle x1="10.75055" y1="8.29945" x2="11.34745" y2="8.31215" layer="21"/>
<rectangle x1="11.44905" y1="8.29945" x2="11.75385" y2="8.31215" layer="21"/>
<rectangle x1="11.84275" y1="8.29945" x2="12.14755" y2="8.31215" layer="21"/>
<rectangle x1="12.24915" y1="8.29945" x2="12.84605" y2="8.31215" layer="21"/>
<rectangle x1="12.94765" y1="8.29945" x2="13.22705" y2="8.31215" layer="21"/>
<rectangle x1="13.34135" y1="8.29945" x2="13.93825" y2="8.31215" layer="21"/>
<rectangle x1="14.02715" y1="8.29945" x2="14.37005" y2="8.31215" layer="21"/>
<rectangle x1="14.45895" y1="8.29945" x2="14.77645" y2="8.31215" layer="21"/>
<rectangle x1="14.87805" y1="8.29945" x2="15.44955" y2="8.31215" layer="21"/>
<rectangle x1="15.55115" y1="8.29945" x2="15.80515" y2="8.31215" layer="21"/>
<rectangle x1="15.90675" y1="8.29945" x2="17.32915" y2="8.31215" layer="21"/>
<rectangle x1="4.76885" y1="8.31215" x2="6.02615" y2="8.32485" layer="21"/>
<rectangle x1="6.12775" y1="8.31215" x2="6.54685" y2="8.32485" layer="21"/>
<rectangle x1="6.64845" y1="8.31215" x2="7.19455" y2="8.32485" layer="21"/>
<rectangle x1="7.29615" y1="8.31215" x2="7.57555" y2="8.32485" layer="21"/>
<rectangle x1="7.67715" y1="8.31215" x2="8.23595" y2="8.32485" layer="21"/>
<rectangle x1="8.33755" y1="8.31215" x2="8.60425" y2="8.32485" layer="21"/>
<rectangle x1="8.69315" y1="8.31215" x2="9.23925" y2="8.32485" layer="21"/>
<rectangle x1="9.34085" y1="8.31215" x2="9.63295" y2="8.32485" layer="21"/>
<rectangle x1="9.72185" y1="8.31215" x2="10.26795" y2="8.32485" layer="21"/>
<rectangle x1="10.36955" y1="8.31215" x2="10.64895" y2="8.32485" layer="21"/>
<rectangle x1="10.76325" y1="8.31215" x2="11.33475" y2="8.32485" layer="21"/>
<rectangle x1="11.43635" y1="8.31215" x2="11.75385" y2="8.32485" layer="21"/>
<rectangle x1="11.84275" y1="8.31215" x2="12.14755" y2="8.32485" layer="21"/>
<rectangle x1="12.26185" y1="8.31215" x2="12.83335" y2="8.32485" layer="21"/>
<rectangle x1="12.93495" y1="8.31215" x2="13.23975" y2="8.32485" layer="21"/>
<rectangle x1="13.34135" y1="8.31215" x2="13.93825" y2="8.32485" layer="21"/>
<rectangle x1="14.02715" y1="8.31215" x2="14.37005" y2="8.32485" layer="21"/>
<rectangle x1="14.45895" y1="8.31215" x2="14.78915" y2="8.32485" layer="21"/>
<rectangle x1="14.89075" y1="8.31215" x2="15.43685" y2="8.32485" layer="21"/>
<rectangle x1="15.53845" y1="8.31215" x2="15.80515" y2="8.32485" layer="21"/>
<rectangle x1="15.90675" y1="8.31215" x2="16.38935" y2="8.32485" layer="21"/>
<rectangle x1="16.47825" y1="8.31215" x2="17.32915" y2="8.32485" layer="21"/>
<rectangle x1="4.76885" y1="8.32485" x2="6.02615" y2="8.33755" layer="21"/>
<rectangle x1="6.12775" y1="8.32485" x2="6.54685" y2="8.33755" layer="21"/>
<rectangle x1="6.66115" y1="8.32485" x2="7.19455" y2="8.33755" layer="21"/>
<rectangle x1="7.29615" y1="8.32485" x2="7.57555" y2="8.33755" layer="21"/>
<rectangle x1="7.68985" y1="8.32485" x2="8.22325" y2="8.33755" layer="21"/>
<rectangle x1="8.32485" y1="8.32485" x2="8.60425" y2="8.33755" layer="21"/>
<rectangle x1="8.69315" y1="8.32485" x2="9.23925" y2="8.33755" layer="21"/>
<rectangle x1="9.34085" y1="8.32485" x2="9.63295" y2="8.33755" layer="21"/>
<rectangle x1="9.72185" y1="8.32485" x2="10.26795" y2="8.33755" layer="21"/>
<rectangle x1="10.36955" y1="8.32485" x2="10.66165" y2="8.33755" layer="21"/>
<rectangle x1="10.76325" y1="8.32485" x2="11.32205" y2="8.33755" layer="21"/>
<rectangle x1="11.43635" y1="8.32485" x2="11.75385" y2="8.33755" layer="21"/>
<rectangle x1="11.84275" y1="8.32485" x2="12.16025" y2="8.33755" layer="21"/>
<rectangle x1="12.26185" y1="8.32485" x2="12.82065" y2="8.33755" layer="21"/>
<rectangle x1="12.93495" y1="8.32485" x2="13.23975" y2="8.33755" layer="21"/>
<rectangle x1="13.35405" y1="8.32485" x2="13.93825" y2="8.33755" layer="21"/>
<rectangle x1="14.02715" y1="8.32485" x2="14.37005" y2="8.33755" layer="21"/>
<rectangle x1="14.45895" y1="8.32485" x2="14.78915" y2="8.33755" layer="21"/>
<rectangle x1="14.90345" y1="8.32485" x2="15.43685" y2="8.33755" layer="21"/>
<rectangle x1="15.53845" y1="8.32485" x2="15.80515" y2="8.33755" layer="21"/>
<rectangle x1="15.90675" y1="8.32485" x2="16.38935" y2="8.33755" layer="21"/>
<rectangle x1="16.47825" y1="8.32485" x2="17.32915" y2="8.33755" layer="21"/>
<rectangle x1="4.76885" y1="8.33755" x2="6.02615" y2="8.35025" layer="21"/>
<rectangle x1="6.12775" y1="8.33755" x2="6.55955" y2="8.35025" layer="21"/>
<rectangle x1="6.66115" y1="8.33755" x2="7.18185" y2="8.35025" layer="21"/>
<rectangle x1="7.28345" y1="8.33755" x2="7.58825" y2="8.35025" layer="21"/>
<rectangle x1="7.70255" y1="8.33755" x2="8.22325" y2="8.35025" layer="21"/>
<rectangle x1="8.32485" y1="8.33755" x2="8.60425" y2="8.35025" layer="21"/>
<rectangle x1="8.70585" y1="8.33755" x2="9.22655" y2="8.35025" layer="21"/>
<rectangle x1="9.34085" y1="8.33755" x2="9.63295" y2="8.35025" layer="21"/>
<rectangle x1="9.73455" y1="8.33755" x2="10.25525" y2="8.35025" layer="21"/>
<rectangle x1="10.36955" y1="8.33755" x2="10.66165" y2="8.35025" layer="21"/>
<rectangle x1="10.77595" y1="8.33755" x2="11.32205" y2="8.35025" layer="21"/>
<rectangle x1="11.42365" y1="8.33755" x2="11.75385" y2="8.35025" layer="21"/>
<rectangle x1="11.84275" y1="8.33755" x2="12.16025" y2="8.35025" layer="21"/>
<rectangle x1="12.27455" y1="8.33755" x2="12.82065" y2="8.35025" layer="21"/>
<rectangle x1="12.92225" y1="8.33755" x2="13.25245" y2="8.35025" layer="21"/>
<rectangle x1="13.35405" y1="8.33755" x2="13.92555" y2="8.35025" layer="21"/>
<rectangle x1="14.02715" y1="8.33755" x2="14.37005" y2="8.35025" layer="21"/>
<rectangle x1="14.45895" y1="8.33755" x2="14.80185" y2="8.35025" layer="21"/>
<rectangle x1="14.90345" y1="8.33755" x2="15.42415" y2="8.35025" layer="21"/>
<rectangle x1="15.52575" y1="8.33755" x2="15.80515" y2="8.35025" layer="21"/>
<rectangle x1="15.90675" y1="8.33755" x2="16.38935" y2="8.35025" layer="21"/>
<rectangle x1="16.47825" y1="8.33755" x2="17.32915" y2="8.35025" layer="21"/>
<rectangle x1="4.76885" y1="8.35025" x2="6.02615" y2="8.36295" layer="21"/>
<rectangle x1="6.12775" y1="8.35025" x2="6.55955" y2="8.36295" layer="21"/>
<rectangle x1="6.67385" y1="8.35025" x2="7.16915" y2="8.36295" layer="21"/>
<rectangle x1="7.28345" y1="8.35025" x2="7.58825" y2="8.36295" layer="21"/>
<rectangle x1="7.70255" y1="8.35025" x2="8.21055" y2="8.36295" layer="21"/>
<rectangle x1="8.31215" y1="8.35025" x2="8.60425" y2="8.36295" layer="21"/>
<rectangle x1="8.71855" y1="8.35025" x2="9.22655" y2="8.36295" layer="21"/>
<rectangle x1="9.32815" y1="8.35025" x2="9.63295" y2="8.36295" layer="21"/>
<rectangle x1="9.74725" y1="8.35025" x2="10.25525" y2="8.36295" layer="21"/>
<rectangle x1="10.35685" y1="8.35025" x2="10.67435" y2="8.36295" layer="21"/>
<rectangle x1="10.78865" y1="8.35025" x2="11.30935" y2="8.36295" layer="21"/>
<rectangle x1="11.42365" y1="8.35025" x2="11.75385" y2="8.36295" layer="21"/>
<rectangle x1="11.84275" y1="8.35025" x2="12.17295" y2="8.36295" layer="21"/>
<rectangle x1="12.28725" y1="8.35025" x2="12.80795" y2="8.36295" layer="21"/>
<rectangle x1="12.92225" y1="8.35025" x2="13.25245" y2="8.36295" layer="21"/>
<rectangle x1="13.36675" y1="8.35025" x2="13.91285" y2="8.36295" layer="21"/>
<rectangle x1="14.02715" y1="8.35025" x2="14.37005" y2="8.36295" layer="21"/>
<rectangle x1="14.45895" y1="8.35025" x2="14.80185" y2="8.36295" layer="21"/>
<rectangle x1="14.91615" y1="8.35025" x2="15.41145" y2="8.36295" layer="21"/>
<rectangle x1="15.52575" y1="8.35025" x2="15.80515" y2="8.36295" layer="21"/>
<rectangle x1="15.90675" y1="8.35025" x2="16.38935" y2="8.36295" layer="21"/>
<rectangle x1="16.47825" y1="8.35025" x2="17.32915" y2="8.36295" layer="21"/>
<rectangle x1="4.76885" y1="8.36295" x2="6.02615" y2="8.37565" layer="21"/>
<rectangle x1="6.12775" y1="8.36295" x2="6.57225" y2="8.37565" layer="21"/>
<rectangle x1="6.68655" y1="8.36295" x2="7.16915" y2="8.37565" layer="21"/>
<rectangle x1="7.27075" y1="8.36295" x2="7.60095" y2="8.37565" layer="21"/>
<rectangle x1="7.71525" y1="8.36295" x2="8.19785" y2="8.37565" layer="21"/>
<rectangle x1="8.31215" y1="8.36295" x2="8.60425" y2="8.37565" layer="21"/>
<rectangle x1="8.73125" y1="8.36295" x2="9.21385" y2="8.37565" layer="21"/>
<rectangle x1="9.32815" y1="8.36295" x2="9.63295" y2="8.37565" layer="21"/>
<rectangle x1="9.75995" y1="8.36295" x2="10.24255" y2="8.37565" layer="21"/>
<rectangle x1="10.35685" y1="8.36295" x2="10.67435" y2="8.37565" layer="21"/>
<rectangle x1="10.80135" y1="8.36295" x2="11.29665" y2="8.37565" layer="21"/>
<rectangle x1="11.41095" y1="8.36295" x2="11.75385" y2="8.37565" layer="21"/>
<rectangle x1="11.84275" y1="8.36295" x2="12.17295" y2="8.37565" layer="21"/>
<rectangle x1="12.29995" y1="8.36295" x2="12.79525" y2="8.37565" layer="21"/>
<rectangle x1="12.90955" y1="8.36295" x2="13.26515" y2="8.37565" layer="21"/>
<rectangle x1="13.37945" y1="8.36295" x2="13.90015" y2="8.37565" layer="21"/>
<rectangle x1="14.02715" y1="8.36295" x2="14.37005" y2="8.37565" layer="21"/>
<rectangle x1="14.45895" y1="8.36295" x2="14.81455" y2="8.37565" layer="21"/>
<rectangle x1="14.92885" y1="8.36295" x2="15.41145" y2="8.37565" layer="21"/>
<rectangle x1="15.51305" y1="8.36295" x2="15.80515" y2="8.37565" layer="21"/>
<rectangle x1="15.90675" y1="8.36295" x2="16.37665" y2="8.37565" layer="21"/>
<rectangle x1="16.47825" y1="8.36295" x2="17.32915" y2="8.37565" layer="21"/>
<rectangle x1="4.76885" y1="8.37565" x2="6.02615" y2="8.38835" layer="21"/>
<rectangle x1="6.12775" y1="8.37565" x2="6.58495" y2="8.38835" layer="21"/>
<rectangle x1="6.69925" y1="8.37565" x2="7.15645" y2="8.38835" layer="21"/>
<rectangle x1="7.27075" y1="8.37565" x2="7.61365" y2="8.38835" layer="21"/>
<rectangle x1="7.72795" y1="8.37565" x2="8.19785" y2="8.38835" layer="21"/>
<rectangle x1="8.29945" y1="8.37565" x2="8.60425" y2="8.38835" layer="21"/>
<rectangle x1="8.74395" y1="8.37565" x2="9.21385" y2="8.38835" layer="21"/>
<rectangle x1="9.31545" y1="8.37565" x2="9.63295" y2="8.38835" layer="21"/>
<rectangle x1="9.77265" y1="8.37565" x2="10.24255" y2="8.38835" layer="21"/>
<rectangle x1="10.34415" y1="8.37565" x2="10.68705" y2="8.38835" layer="21"/>
<rectangle x1="10.81405" y1="8.37565" x2="11.28395" y2="8.38835" layer="21"/>
<rectangle x1="11.41095" y1="8.37565" x2="11.75385" y2="8.38835" layer="21"/>
<rectangle x1="11.84275" y1="8.37565" x2="12.18565" y2="8.38835" layer="21"/>
<rectangle x1="12.31265" y1="8.37565" x2="12.78255" y2="8.38835" layer="21"/>
<rectangle x1="12.90955" y1="8.37565" x2="13.26515" y2="8.38835" layer="21"/>
<rectangle x1="13.39215" y1="8.37565" x2="13.88745" y2="8.38835" layer="21"/>
<rectangle x1="14.02715" y1="8.37565" x2="14.37005" y2="8.38835" layer="21"/>
<rectangle x1="14.45895" y1="8.37565" x2="14.82725" y2="8.38835" layer="21"/>
<rectangle x1="14.94155" y1="8.37565" x2="15.39875" y2="8.38835" layer="21"/>
<rectangle x1="15.51305" y1="8.37565" x2="15.81785" y2="8.38835" layer="21"/>
<rectangle x1="15.90675" y1="8.37565" x2="16.37665" y2="8.38835" layer="21"/>
<rectangle x1="16.46555" y1="8.37565" x2="17.32915" y2="8.38835" layer="21"/>
<rectangle x1="4.76885" y1="8.38835" x2="6.02615" y2="8.40105" layer="21"/>
<rectangle x1="6.12775" y1="8.38835" x2="6.58495" y2="8.40105" layer="21"/>
<rectangle x1="6.71195" y1="8.38835" x2="7.14375" y2="8.40105" layer="21"/>
<rectangle x1="7.25805" y1="8.38835" x2="7.61365" y2="8.40105" layer="21"/>
<rectangle x1="7.74065" y1="8.38835" x2="8.18515" y2="8.40105" layer="21"/>
<rectangle x1="8.29945" y1="8.38835" x2="8.60425" y2="8.40105" layer="21"/>
<rectangle x1="8.75665" y1="8.38835" x2="9.20115" y2="8.40105" layer="21"/>
<rectangle x1="9.31545" y1="8.38835" x2="9.63295" y2="8.40105" layer="21"/>
<rectangle x1="9.78535" y1="8.38835" x2="10.22985" y2="8.40105" layer="21"/>
<rectangle x1="10.34415" y1="8.38835" x2="10.69975" y2="8.40105" layer="21"/>
<rectangle x1="10.82675" y1="8.38835" x2="11.27125" y2="8.40105" layer="21"/>
<rectangle x1="11.39825" y1="8.38835" x2="11.75385" y2="8.40105" layer="21"/>
<rectangle x1="11.84275" y1="8.38835" x2="12.19835" y2="8.40105" layer="21"/>
<rectangle x1="12.32535" y1="8.38835" x2="12.76985" y2="8.40105" layer="21"/>
<rectangle x1="12.89685" y1="8.38835" x2="13.27785" y2="8.40105" layer="21"/>
<rectangle x1="13.40485" y1="8.38835" x2="13.87475" y2="8.40105" layer="21"/>
<rectangle x1="14.02715" y1="8.38835" x2="14.37005" y2="8.40105" layer="21"/>
<rectangle x1="14.45895" y1="8.38835" x2="14.82725" y2="8.40105" layer="21"/>
<rectangle x1="14.95425" y1="8.38835" x2="15.38605" y2="8.40105" layer="21"/>
<rectangle x1="15.50035" y1="8.38835" x2="15.81785" y2="8.40105" layer="21"/>
<rectangle x1="15.91945" y1="8.38835" x2="16.36395" y2="8.40105" layer="21"/>
<rectangle x1="16.46555" y1="8.38835" x2="17.32915" y2="8.40105" layer="21"/>
<rectangle x1="4.76885" y1="8.40105" x2="6.02615" y2="8.41375" layer="21"/>
<rectangle x1="6.12775" y1="8.40105" x2="6.59765" y2="8.41375" layer="21"/>
<rectangle x1="6.72465" y1="8.40105" x2="7.13105" y2="8.41375" layer="21"/>
<rectangle x1="7.24535" y1="8.40105" x2="7.62635" y2="8.41375" layer="21"/>
<rectangle x1="7.75335" y1="8.40105" x2="8.17245" y2="8.41375" layer="21"/>
<rectangle x1="8.28675" y1="8.40105" x2="8.60425" y2="8.41375" layer="21"/>
<rectangle x1="8.78205" y1="8.40105" x2="9.18845" y2="8.41375" layer="21"/>
<rectangle x1="9.30275" y1="8.40105" x2="9.63295" y2="8.41375" layer="21"/>
<rectangle x1="9.81075" y1="8.40105" x2="10.21715" y2="8.41375" layer="21"/>
<rectangle x1="10.33145" y1="8.40105" x2="10.69975" y2="8.41375" layer="21"/>
<rectangle x1="10.83945" y1="8.40105" x2="11.25855" y2="8.41375" layer="21"/>
<rectangle x1="11.38555" y1="8.40105" x2="11.75385" y2="8.41375" layer="21"/>
<rectangle x1="11.84275" y1="8.40105" x2="12.19835" y2="8.41375" layer="21"/>
<rectangle x1="12.33805" y1="8.40105" x2="12.75715" y2="8.41375" layer="21"/>
<rectangle x1="12.88415" y1="8.40105" x2="13.29055" y2="8.41375" layer="21"/>
<rectangle x1="13.41755" y1="8.40105" x2="13.86205" y2="8.41375" layer="21"/>
<rectangle x1="14.02715" y1="8.40105" x2="14.37005" y2="8.41375" layer="21"/>
<rectangle x1="14.45895" y1="8.40105" x2="14.83995" y2="8.41375" layer="21"/>
<rectangle x1="14.96695" y1="8.40105" x2="15.37335" y2="8.41375" layer="21"/>
<rectangle x1="15.48765" y1="8.40105" x2="15.81785" y2="8.41375" layer="21"/>
<rectangle x1="15.91945" y1="8.40105" x2="16.35125" y2="8.41375" layer="21"/>
<rectangle x1="16.46555" y1="8.40105" x2="17.32915" y2="8.41375" layer="21"/>
<rectangle x1="4.76885" y1="8.41375" x2="6.02615" y2="8.42645" layer="21"/>
<rectangle x1="6.12775" y1="8.41375" x2="6.61035" y2="8.42645" layer="21"/>
<rectangle x1="6.73735" y1="8.41375" x2="7.11835" y2="8.42645" layer="21"/>
<rectangle x1="7.24535" y1="8.41375" x2="7.63905" y2="8.42645" layer="21"/>
<rectangle x1="7.76605" y1="8.41375" x2="8.14705" y2="8.42645" layer="21"/>
<rectangle x1="8.27405" y1="8.41375" x2="8.60425" y2="8.42645" layer="21"/>
<rectangle x1="8.79475" y1="8.41375" x2="9.17575" y2="8.42645" layer="21"/>
<rectangle x1="9.30275" y1="8.41375" x2="9.63295" y2="8.42645" layer="21"/>
<rectangle x1="9.82345" y1="8.41375" x2="10.20445" y2="8.42645" layer="21"/>
<rectangle x1="10.33145" y1="8.41375" x2="10.71245" y2="8.42645" layer="21"/>
<rectangle x1="10.85215" y1="8.41375" x2="11.24585" y2="8.42645" layer="21"/>
<rectangle x1="11.37285" y1="8.41375" x2="11.75385" y2="8.42645" layer="21"/>
<rectangle x1="11.84275" y1="8.41375" x2="12.21105" y2="8.42645" layer="21"/>
<rectangle x1="12.35075" y1="8.41375" x2="12.74445" y2="8.42645" layer="21"/>
<rectangle x1="12.87145" y1="8.41375" x2="13.30325" y2="8.42645" layer="21"/>
<rectangle x1="13.43025" y1="8.41375" x2="13.83665" y2="8.42645" layer="21"/>
<rectangle x1="14.02715" y1="8.41375" x2="14.37005" y2="8.42645" layer="21"/>
<rectangle x1="14.45895" y1="8.41375" x2="14.85265" y2="8.42645" layer="21"/>
<rectangle x1="14.97965" y1="8.41375" x2="15.36065" y2="8.42645" layer="21"/>
<rectangle x1="15.48765" y1="8.41375" x2="15.83055" y2="8.42645" layer="21"/>
<rectangle x1="15.93215" y1="8.41375" x2="16.33855" y2="8.42645" layer="21"/>
<rectangle x1="16.45285" y1="8.41375" x2="17.32915" y2="8.42645" layer="21"/>
<rectangle x1="4.76885" y1="8.42645" x2="6.02615" y2="8.43915" layer="21"/>
<rectangle x1="6.12775" y1="8.42645" x2="6.62305" y2="8.43915" layer="21"/>
<rectangle x1="6.75005" y1="8.42645" x2="7.10565" y2="8.43915" layer="21"/>
<rectangle x1="7.23265" y1="8.42645" x2="7.65175" y2="8.43915" layer="21"/>
<rectangle x1="7.79145" y1="8.42645" x2="8.13435" y2="8.43915" layer="21"/>
<rectangle x1="8.27405" y1="8.42645" x2="8.60425" y2="8.43915" layer="21"/>
<rectangle x1="8.82015" y1="8.42645" x2="9.16305" y2="8.43915" layer="21"/>
<rectangle x1="9.29005" y1="8.42645" x2="9.63295" y2="8.43915" layer="21"/>
<rectangle x1="9.84885" y1="8.42645" x2="10.19175" y2="8.43915" layer="21"/>
<rectangle x1="10.31875" y1="8.42645" x2="10.72515" y2="8.43915" layer="21"/>
<rectangle x1="10.87755" y1="8.42645" x2="11.22045" y2="8.43915" layer="21"/>
<rectangle x1="11.36015" y1="8.42645" x2="11.75385" y2="8.43915" layer="21"/>
<rectangle x1="11.84275" y1="8.42645" x2="12.22375" y2="8.43915" layer="21"/>
<rectangle x1="12.37615" y1="8.42645" x2="12.71905" y2="8.43915" layer="21"/>
<rectangle x1="12.85875" y1="8.42645" x2="13.30325" y2="8.43915" layer="21"/>
<rectangle x1="13.45565" y1="8.42645" x2="13.81125" y2="8.43915" layer="21"/>
<rectangle x1="14.02715" y1="8.42645" x2="14.37005" y2="8.43915" layer="21"/>
<rectangle x1="14.45895" y1="8.42645" x2="14.86535" y2="8.43915" layer="21"/>
<rectangle x1="14.99235" y1="8.42645" x2="15.34795" y2="8.43915" layer="21"/>
<rectangle x1="15.47495" y1="8.42645" x2="15.83055" y2="8.43915" layer="21"/>
<rectangle x1="15.94485" y1="8.42645" x2="16.32585" y2="8.43915" layer="21"/>
<rectangle x1="16.44015" y1="8.42645" x2="17.32915" y2="8.43915" layer="21"/>
<rectangle x1="4.76885" y1="8.43915" x2="6.02615" y2="8.45185" layer="21"/>
<rectangle x1="6.12775" y1="8.43915" x2="6.62305" y2="8.45185" layer="21"/>
<rectangle x1="6.77545" y1="8.43915" x2="7.08025" y2="8.45185" layer="21"/>
<rectangle x1="7.21995" y1="8.43915" x2="7.66445" y2="8.45185" layer="21"/>
<rectangle x1="7.81685" y1="8.43915" x2="8.10895" y2="8.45185" layer="21"/>
<rectangle x1="8.26135" y1="8.43915" x2="8.60425" y2="8.45185" layer="21"/>
<rectangle x1="8.84555" y1="8.43915" x2="9.13765" y2="8.45185" layer="21"/>
<rectangle x1="9.27735" y1="8.43915" x2="9.63295" y2="8.45185" layer="21"/>
<rectangle x1="9.87425" y1="8.43915" x2="10.16635" y2="8.45185" layer="21"/>
<rectangle x1="10.30605" y1="8.43915" x2="10.73785" y2="8.45185" layer="21"/>
<rectangle x1="10.90295" y1="8.43915" x2="11.19505" y2="8.45185" layer="21"/>
<rectangle x1="11.34745" y1="8.43915" x2="11.75385" y2="8.45185" layer="21"/>
<rectangle x1="11.84275" y1="8.43915" x2="12.23645" y2="8.45185" layer="21"/>
<rectangle x1="12.40155" y1="8.43915" x2="12.69365" y2="8.45185" layer="21"/>
<rectangle x1="12.84605" y1="8.43915" x2="13.31595" y2="8.45185" layer="21"/>
<rectangle x1="13.46835" y1="8.43915" x2="13.79855" y2="8.45185" layer="21"/>
<rectangle x1="14.02715" y1="8.43915" x2="14.37005" y2="8.45185" layer="21"/>
<rectangle x1="14.45895" y1="8.43915" x2="14.86535" y2="8.45185" layer="21"/>
<rectangle x1="15.01775" y1="8.43915" x2="15.32255" y2="8.45185" layer="21"/>
<rectangle x1="15.46225" y1="8.43915" x2="15.84325" y2="8.45185" layer="21"/>
<rectangle x1="15.95755" y1="8.43915" x2="16.31315" y2="8.45185" layer="21"/>
<rectangle x1="16.44015" y1="8.43915" x2="17.32915" y2="8.45185" layer="21"/>
<rectangle x1="4.76885" y1="8.45185" x2="6.02615" y2="8.46455" layer="21"/>
<rectangle x1="6.12775" y1="8.45185" x2="6.64845" y2="8.46455" layer="21"/>
<rectangle x1="6.80085" y1="8.45185" x2="7.05485" y2="8.46455" layer="21"/>
<rectangle x1="7.20725" y1="8.45185" x2="7.67715" y2="8.46455" layer="21"/>
<rectangle x1="7.84225" y1="8.45185" x2="8.08355" y2="8.46455" layer="21"/>
<rectangle x1="8.24865" y1="8.45185" x2="8.60425" y2="8.46455" layer="21"/>
<rectangle x1="8.69315" y1="8.45185" x2="8.70585" y2="8.46455" layer="21"/>
<rectangle x1="8.87095" y1="8.45185" x2="9.11225" y2="8.46455" layer="21"/>
<rectangle x1="9.27735" y1="8.45185" x2="9.63295" y2="8.46455" layer="21"/>
<rectangle x1="9.72185" y1="8.45185" x2="9.73455" y2="8.46455" layer="21"/>
<rectangle x1="9.89965" y1="8.45185" x2="10.14095" y2="8.46455" layer="21"/>
<rectangle x1="10.30605" y1="8.45185" x2="10.75055" y2="8.46455" layer="21"/>
<rectangle x1="10.92835" y1="8.45185" x2="11.16965" y2="8.46455" layer="21"/>
<rectangle x1="11.33475" y1="8.45185" x2="11.75385" y2="8.46455" layer="21"/>
<rectangle x1="11.84275" y1="8.45185" x2="12.24915" y2="8.46455" layer="21"/>
<rectangle x1="12.42695" y1="8.45185" x2="12.66825" y2="8.46455" layer="21"/>
<rectangle x1="12.83335" y1="8.45185" x2="13.32865" y2="8.46455" layer="21"/>
<rectangle x1="13.50645" y1="8.45185" x2="13.76045" y2="8.46455" layer="21"/>
<rectangle x1="13.92555" y1="8.45185" x2="13.93825" y2="8.46455" layer="21"/>
<rectangle x1="14.02715" y1="8.45185" x2="14.37005" y2="8.46455" layer="21"/>
<rectangle x1="14.45895" y1="8.45185" x2="14.89075" y2="8.46455" layer="21"/>
<rectangle x1="15.04315" y1="8.45185" x2="15.29715" y2="8.46455" layer="21"/>
<rectangle x1="15.44955" y1="8.45185" x2="15.84325" y2="8.46455" layer="21"/>
<rectangle x1="15.98295" y1="8.45185" x2="16.28775" y2="8.46455" layer="21"/>
<rectangle x1="16.42745" y1="8.45185" x2="17.32915" y2="8.46455" layer="21"/>
<rectangle x1="4.76885" y1="8.46455" x2="6.02615" y2="8.47725" layer="21"/>
<rectangle x1="6.12775" y1="8.46455" x2="6.66115" y2="8.47725" layer="21"/>
<rectangle x1="6.83895" y1="8.46455" x2="7.01675" y2="8.47725" layer="21"/>
<rectangle x1="7.19455" y1="8.46455" x2="7.68985" y2="8.47725" layer="21"/>
<rectangle x1="7.89305" y1="8.46455" x2="8.04545" y2="8.47725" layer="21"/>
<rectangle x1="8.23595" y1="8.46455" x2="8.60425" y2="8.47725" layer="21"/>
<rectangle x1="8.69315" y1="8.46455" x2="8.71855" y2="8.47725" layer="21"/>
<rectangle x1="8.92175" y1="8.46455" x2="9.07415" y2="8.47725" layer="21"/>
<rectangle x1="9.26465" y1="8.46455" x2="9.63295" y2="8.47725" layer="21"/>
<rectangle x1="9.72185" y1="8.46455" x2="9.74725" y2="8.47725" layer="21"/>
<rectangle x1="9.95045" y1="8.46455" x2="10.10285" y2="8.47725" layer="21"/>
<rectangle x1="10.29335" y1="8.46455" x2="10.76325" y2="8.47725" layer="21"/>
<rectangle x1="10.96645" y1="8.46455" x2="11.11885" y2="8.47725" layer="21"/>
<rectangle x1="11.32205" y1="8.46455" x2="11.75385" y2="8.47725" layer="21"/>
<rectangle x1="11.84275" y1="8.46455" x2="12.26185" y2="8.47725" layer="21"/>
<rectangle x1="12.46505" y1="8.46455" x2="12.61745" y2="8.47725" layer="21"/>
<rectangle x1="12.82065" y1="8.46455" x2="13.35405" y2="8.47725" layer="21"/>
<rectangle x1="13.53185" y1="8.46455" x2="13.72235" y2="8.47725" layer="21"/>
<rectangle x1="13.91285" y1="8.46455" x2="13.93825" y2="8.47725" layer="21"/>
<rectangle x1="14.02715" y1="8.46455" x2="14.37005" y2="8.47725" layer="21"/>
<rectangle x1="14.45895" y1="8.46455" x2="14.90345" y2="8.47725" layer="21"/>
<rectangle x1="15.08125" y1="8.46455" x2="15.25905" y2="8.47725" layer="21"/>
<rectangle x1="15.43685" y1="8.46455" x2="15.85595" y2="8.47725" layer="21"/>
<rectangle x1="16.02105" y1="8.46455" x2="16.24965" y2="8.47725" layer="21"/>
<rectangle x1="16.41475" y1="8.46455" x2="17.32915" y2="8.47725" layer="21"/>
<rectangle x1="4.76885" y1="8.47725" x2="6.02615" y2="8.48995" layer="21"/>
<rectangle x1="6.12775" y1="8.47725" x2="6.67385" y2="8.48995" layer="21"/>
<rectangle x1="7.18185" y1="8.47725" x2="7.70255" y2="8.48995" layer="21"/>
<rectangle x1="8.22325" y1="8.47725" x2="8.60425" y2="8.48995" layer="21"/>
<rectangle x1="8.69315" y1="8.47725" x2="8.73125" y2="8.48995" layer="21"/>
<rectangle x1="9.25195" y1="8.47725" x2="9.63295" y2="8.48995" layer="21"/>
<rectangle x1="9.72185" y1="8.47725" x2="9.75995" y2="8.48995" layer="21"/>
<rectangle x1="10.28065" y1="8.47725" x2="10.78865" y2="8.48995" layer="21"/>
<rectangle x1="11.30935" y1="8.47725" x2="11.75385" y2="8.48995" layer="21"/>
<rectangle x1="11.84275" y1="8.47725" x2="12.28725" y2="8.48995" layer="21"/>
<rectangle x1="12.80795" y1="8.47725" x2="13.36675" y2="8.48995" layer="21"/>
<rectangle x1="13.88745" y1="8.47725" x2="13.93825" y2="8.48995" layer="21"/>
<rectangle x1="14.02715" y1="8.47725" x2="14.37005" y2="8.48995" layer="21"/>
<rectangle x1="14.45895" y1="8.47725" x2="14.91615" y2="8.48995" layer="21"/>
<rectangle x1="15.42415" y1="8.47725" x2="15.86865" y2="8.48995" layer="21"/>
<rectangle x1="16.08455" y1="8.47725" x2="16.18615" y2="8.48995" layer="21"/>
<rectangle x1="16.40205" y1="8.47725" x2="17.32915" y2="8.48995" layer="21"/>
<rectangle x1="4.76885" y1="8.48995" x2="6.02615" y2="8.50265" layer="21"/>
<rectangle x1="6.12775" y1="8.48995" x2="6.68655" y2="8.50265" layer="21"/>
<rectangle x1="7.15645" y1="8.48995" x2="7.72795" y2="8.50265" layer="21"/>
<rectangle x1="8.19785" y1="8.48995" x2="8.60425" y2="8.50265" layer="21"/>
<rectangle x1="8.69315" y1="8.48995" x2="8.75665" y2="8.50265" layer="21"/>
<rectangle x1="9.22655" y1="8.48995" x2="9.63295" y2="8.50265" layer="21"/>
<rectangle x1="9.72185" y1="8.48995" x2="9.78535" y2="8.50265" layer="21"/>
<rectangle x1="10.25525" y1="8.48995" x2="10.80135" y2="8.50265" layer="21"/>
<rectangle x1="11.28395" y1="8.48995" x2="11.75385" y2="8.50265" layer="21"/>
<rectangle x1="11.84275" y1="8.48995" x2="12.29995" y2="8.50265" layer="21"/>
<rectangle x1="12.78255" y1="8.48995" x2="13.37945" y2="8.50265" layer="21"/>
<rectangle x1="13.87475" y1="8.48995" x2="13.93825" y2="8.50265" layer="21"/>
<rectangle x1="14.02715" y1="8.48995" x2="14.37005" y2="8.50265" layer="21"/>
<rectangle x1="14.45895" y1="8.48995" x2="14.92885" y2="8.50265" layer="21"/>
<rectangle x1="15.39875" y1="8.48995" x2="15.88135" y2="8.50265" layer="21"/>
<rectangle x1="16.38935" y1="8.48995" x2="17.32915" y2="8.50265" layer="21"/>
<rectangle x1="4.76885" y1="8.50265" x2="6.02615" y2="8.51535" layer="21"/>
<rectangle x1="6.12775" y1="8.50265" x2="6.71195" y2="8.51535" layer="21"/>
<rectangle x1="7.14375" y1="8.50265" x2="7.74065" y2="8.51535" layer="21"/>
<rectangle x1="8.18515" y1="8.50265" x2="8.60425" y2="8.51535" layer="21"/>
<rectangle x1="8.69315" y1="8.50265" x2="8.78205" y2="8.51535" layer="21"/>
<rectangle x1="9.21385" y1="8.50265" x2="9.63295" y2="8.51535" layer="21"/>
<rectangle x1="9.72185" y1="8.50265" x2="9.81075" y2="8.51535" layer="21"/>
<rectangle x1="10.24255" y1="8.50265" x2="10.82675" y2="8.51535" layer="21"/>
<rectangle x1="11.27125" y1="8.50265" x2="11.75385" y2="8.51535" layer="21"/>
<rectangle x1="11.84275" y1="8.50265" x2="12.32535" y2="8.51535" layer="21"/>
<rectangle x1="12.76985" y1="8.50265" x2="13.40485" y2="8.51535" layer="21"/>
<rectangle x1="13.84935" y1="8.50265" x2="13.93825" y2="8.51535" layer="21"/>
<rectangle x1="14.02715" y1="8.50265" x2="14.37005" y2="8.51535" layer="21"/>
<rectangle x1="14.45895" y1="8.50265" x2="14.95425" y2="8.51535" layer="21"/>
<rectangle x1="15.38605" y1="8.50265" x2="15.90675" y2="8.51535" layer="21"/>
<rectangle x1="16.36395" y1="8.50265" x2="17.32915" y2="8.51535" layer="21"/>
<rectangle x1="4.76885" y1="8.51535" x2="6.02615" y2="8.52805" layer="21"/>
<rectangle x1="6.12775" y1="8.51535" x2="6.73735" y2="8.52805" layer="21"/>
<rectangle x1="7.11835" y1="8.51535" x2="7.76605" y2="8.52805" layer="21"/>
<rectangle x1="8.15975" y1="8.51535" x2="8.60425" y2="8.52805" layer="21"/>
<rectangle x1="8.69315" y1="8.51535" x2="8.79475" y2="8.52805" layer="21"/>
<rectangle x1="9.18845" y1="8.51535" x2="9.63295" y2="8.52805" layer="21"/>
<rectangle x1="9.72185" y1="8.51535" x2="9.82345" y2="8.52805" layer="21"/>
<rectangle x1="10.21715" y1="8.51535" x2="10.85215" y2="8.52805" layer="21"/>
<rectangle x1="11.24585" y1="8.51535" x2="11.75385" y2="8.52805" layer="21"/>
<rectangle x1="11.84275" y1="8.51535" x2="12.35075" y2="8.52805" layer="21"/>
<rectangle x1="12.74445" y1="8.51535" x2="13.43025" y2="8.52805" layer="21"/>
<rectangle x1="13.82395" y1="8.51535" x2="13.93825" y2="8.52805" layer="21"/>
<rectangle x1="14.02715" y1="8.51535" x2="14.37005" y2="8.52805" layer="21"/>
<rectangle x1="14.45895" y1="8.51535" x2="14.97965" y2="8.52805" layer="21"/>
<rectangle x1="15.36065" y1="8.51535" x2="15.91945" y2="8.52805" layer="21"/>
<rectangle x1="16.33855" y1="8.51535" x2="17.32915" y2="8.52805" layer="21"/>
<rectangle x1="4.76885" y1="8.52805" x2="6.02615" y2="8.54075" layer="21"/>
<rectangle x1="6.12775" y1="8.52805" x2="6.76275" y2="8.54075" layer="21"/>
<rectangle x1="7.09295" y1="8.52805" x2="7.79145" y2="8.54075" layer="21"/>
<rectangle x1="8.13435" y1="8.52805" x2="8.60425" y2="8.54075" layer="21"/>
<rectangle x1="8.69315" y1="8.52805" x2="8.83285" y2="8.54075" layer="21"/>
<rectangle x1="9.16305" y1="8.52805" x2="9.63295" y2="8.54075" layer="21"/>
<rectangle x1="9.72185" y1="8.52805" x2="9.86155" y2="8.54075" layer="21"/>
<rectangle x1="10.19175" y1="8.52805" x2="10.87755" y2="8.54075" layer="21"/>
<rectangle x1="11.22045" y1="8.52805" x2="11.75385" y2="8.54075" layer="21"/>
<rectangle x1="11.84275" y1="8.52805" x2="12.37615" y2="8.54075" layer="21"/>
<rectangle x1="12.71905" y1="8.52805" x2="13.45565" y2="8.54075" layer="21"/>
<rectangle x1="13.79855" y1="8.52805" x2="13.93825" y2="8.54075" layer="21"/>
<rectangle x1="14.03985" y1="8.52805" x2="14.37005" y2="8.54075" layer="21"/>
<rectangle x1="14.45895" y1="8.52805" x2="15.00505" y2="8.54075" layer="21"/>
<rectangle x1="15.33525" y1="8.52805" x2="15.94485" y2="8.54075" layer="21"/>
<rectangle x1="16.31315" y1="8.52805" x2="17.32915" y2="8.54075" layer="21"/>
<rectangle x1="4.76885" y1="8.54075" x2="6.02615" y2="8.55345" layer="21"/>
<rectangle x1="6.12775" y1="8.54075" x2="6.78815" y2="8.55345" layer="21"/>
<rectangle x1="7.05485" y1="8.54075" x2="7.82955" y2="8.55345" layer="21"/>
<rectangle x1="8.09625" y1="8.54075" x2="8.60425" y2="8.55345" layer="21"/>
<rectangle x1="8.69315" y1="8.54075" x2="8.87095" y2="8.55345" layer="21"/>
<rectangle x1="9.13765" y1="8.54075" x2="9.89965" y2="8.55345" layer="21"/>
<rectangle x1="10.16635" y1="8.54075" x2="10.91565" y2="8.55345" layer="21"/>
<rectangle x1="11.18235" y1="8.54075" x2="11.75385" y2="8.55345" layer="21"/>
<rectangle x1="11.84275" y1="8.54075" x2="12.41425" y2="8.55345" layer="21"/>
<rectangle x1="12.68095" y1="8.54075" x2="13.48105" y2="8.55345" layer="21"/>
<rectangle x1="13.76045" y1="8.54075" x2="15.03045" y2="8.55345" layer="21"/>
<rectangle x1="15.29715" y1="8.54075" x2="15.98295" y2="8.55345" layer="21"/>
<rectangle x1="16.28775" y1="8.54075" x2="17.32915" y2="8.55345" layer="21"/>
<rectangle x1="4.76885" y1="8.55345" x2="6.02615" y2="8.56615" layer="21"/>
<rectangle x1="6.12775" y1="8.55345" x2="6.85165" y2="8.56615" layer="21"/>
<rectangle x1="7.00405" y1="8.55345" x2="7.88035" y2="8.56615" layer="21"/>
<rectangle x1="8.04545" y1="8.55345" x2="8.60425" y2="8.56615" layer="21"/>
<rectangle x1="8.69315" y1="8.55345" x2="8.92175" y2="8.56615" layer="21"/>
<rectangle x1="9.08685" y1="8.55345" x2="9.95045" y2="8.56615" layer="21"/>
<rectangle x1="10.11555" y1="8.55345" x2="10.96645" y2="8.56615" layer="21"/>
<rectangle x1="11.13155" y1="8.55345" x2="11.75385" y2="8.56615" layer="21"/>
<rectangle x1="11.84275" y1="8.55345" x2="12.46505" y2="8.56615" layer="21"/>
<rectangle x1="12.63015" y1="8.55345" x2="13.53185" y2="8.56615" layer="21"/>
<rectangle x1="13.69695" y1="8.55345" x2="15.09395" y2="8.56615" layer="21"/>
<rectangle x1="15.24635" y1="8.55345" x2="16.03375" y2="8.56615" layer="21"/>
<rectangle x1="16.22425" y1="8.55345" x2="17.32915" y2="8.56615" layer="21"/>
<rectangle x1="4.76885" y1="8.56615" x2="6.02615" y2="8.57885" layer="21"/>
<rectangle x1="6.12775" y1="8.56615" x2="8.60425" y2="8.57885" layer="21"/>
<rectangle x1="8.69315" y1="8.56615" x2="11.75385" y2="8.57885" layer="21"/>
<rectangle x1="11.84275" y1="8.56615" x2="17.32915" y2="8.57885" layer="21"/>
<rectangle x1="4.76885" y1="8.57885" x2="6.02615" y2="8.59155" layer="21"/>
<rectangle x1="6.12775" y1="8.57885" x2="8.60425" y2="8.59155" layer="21"/>
<rectangle x1="8.69315" y1="8.57885" x2="11.75385" y2="8.59155" layer="21"/>
<rectangle x1="11.84275" y1="8.57885" x2="17.32915" y2="8.59155" layer="21"/>
<rectangle x1="4.76885" y1="8.59155" x2="6.02615" y2="8.60425" layer="21"/>
<rectangle x1="6.12775" y1="8.59155" x2="8.60425" y2="8.60425" layer="21"/>
<rectangle x1="8.69315" y1="8.59155" x2="11.75385" y2="8.60425" layer="21"/>
<rectangle x1="11.84275" y1="8.59155" x2="17.32915" y2="8.60425" layer="21"/>
<rectangle x1="4.76885" y1="8.60425" x2="6.02615" y2="8.61695" layer="21"/>
<rectangle x1="6.12775" y1="8.60425" x2="8.60425" y2="8.61695" layer="21"/>
<rectangle x1="8.69315" y1="8.60425" x2="11.75385" y2="8.61695" layer="21"/>
<rectangle x1="11.84275" y1="8.60425" x2="17.32915" y2="8.61695" layer="21"/>
<rectangle x1="4.76885" y1="8.61695" x2="6.02615" y2="8.62965" layer="21"/>
<rectangle x1="6.12775" y1="8.61695" x2="8.60425" y2="8.62965" layer="21"/>
<rectangle x1="8.69315" y1="8.61695" x2="11.75385" y2="8.62965" layer="21"/>
<rectangle x1="11.84275" y1="8.61695" x2="17.32915" y2="8.62965" layer="21"/>
<rectangle x1="4.76885" y1="8.62965" x2="6.02615" y2="8.64235" layer="21"/>
<rectangle x1="6.12775" y1="8.62965" x2="8.60425" y2="8.64235" layer="21"/>
<rectangle x1="8.69315" y1="8.62965" x2="11.75385" y2="8.64235" layer="21"/>
<rectangle x1="11.84275" y1="8.62965" x2="17.32915" y2="8.64235" layer="21"/>
<rectangle x1="4.76885" y1="8.64235" x2="6.02615" y2="8.65505" layer="21"/>
<rectangle x1="6.12775" y1="8.64235" x2="8.60425" y2="8.65505" layer="21"/>
<rectangle x1="8.69315" y1="8.64235" x2="11.75385" y2="8.65505" layer="21"/>
<rectangle x1="11.84275" y1="8.64235" x2="17.32915" y2="8.65505" layer="21"/>
<rectangle x1="4.76885" y1="8.65505" x2="6.02615" y2="8.66775" layer="21"/>
<rectangle x1="6.12775" y1="8.65505" x2="8.60425" y2="8.66775" layer="21"/>
<rectangle x1="8.69315" y1="8.65505" x2="11.75385" y2="8.66775" layer="21"/>
<rectangle x1="11.84275" y1="8.65505" x2="17.32915" y2="8.66775" layer="21"/>
<rectangle x1="4.76885" y1="8.66775" x2="6.02615" y2="8.68045" layer="21"/>
<rectangle x1="6.12775" y1="8.66775" x2="8.60425" y2="8.68045" layer="21"/>
<rectangle x1="8.69315" y1="8.66775" x2="11.75385" y2="8.68045" layer="21"/>
<rectangle x1="11.84275" y1="8.66775" x2="17.32915" y2="8.68045" layer="21"/>
<rectangle x1="4.76885" y1="8.68045" x2="6.02615" y2="8.69315" layer="21"/>
<rectangle x1="6.12775" y1="8.68045" x2="8.60425" y2="8.69315" layer="21"/>
<rectangle x1="8.69315" y1="8.68045" x2="11.75385" y2="8.69315" layer="21"/>
<rectangle x1="11.84275" y1="8.68045" x2="17.32915" y2="8.69315" layer="21"/>
<rectangle x1="4.76885" y1="8.69315" x2="6.02615" y2="8.70585" layer="21"/>
<rectangle x1="6.12775" y1="8.69315" x2="8.60425" y2="8.70585" layer="21"/>
<rectangle x1="8.69315" y1="8.69315" x2="11.75385" y2="8.70585" layer="21"/>
<rectangle x1="11.84275" y1="8.69315" x2="17.32915" y2="8.70585" layer="21"/>
<rectangle x1="4.76885" y1="8.70585" x2="6.02615" y2="8.71855" layer="21"/>
<rectangle x1="6.12775" y1="8.70585" x2="8.60425" y2="8.71855" layer="21"/>
<rectangle x1="8.69315" y1="8.70585" x2="11.75385" y2="8.71855" layer="21"/>
<rectangle x1="11.84275" y1="8.70585" x2="17.32915" y2="8.71855" layer="21"/>
<rectangle x1="4.76885" y1="8.71855" x2="6.02615" y2="8.73125" layer="21"/>
<rectangle x1="6.12775" y1="8.71855" x2="8.60425" y2="8.73125" layer="21"/>
<rectangle x1="8.69315" y1="8.71855" x2="11.75385" y2="8.73125" layer="21"/>
<rectangle x1="11.84275" y1="8.71855" x2="17.32915" y2="8.73125" layer="21"/>
<rectangle x1="4.76885" y1="8.73125" x2="6.02615" y2="8.74395" layer="21"/>
<rectangle x1="6.12775" y1="8.73125" x2="8.60425" y2="8.74395" layer="21"/>
<rectangle x1="8.69315" y1="8.73125" x2="11.75385" y2="8.74395" layer="21"/>
<rectangle x1="11.84275" y1="8.73125" x2="17.32915" y2="8.74395" layer="21"/>
<rectangle x1="4.76885" y1="8.74395" x2="6.02615" y2="8.75665" layer="21"/>
<rectangle x1="6.12775" y1="8.74395" x2="8.60425" y2="8.75665" layer="21"/>
<rectangle x1="8.69315" y1="8.74395" x2="11.75385" y2="8.75665" layer="21"/>
<rectangle x1="11.84275" y1="8.74395" x2="17.32915" y2="8.75665" layer="21"/>
<rectangle x1="4.76885" y1="8.75665" x2="6.02615" y2="8.76935" layer="21"/>
<rectangle x1="6.12775" y1="8.75665" x2="8.60425" y2="8.76935" layer="21"/>
<rectangle x1="8.69315" y1="8.75665" x2="11.75385" y2="8.76935" layer="21"/>
<rectangle x1="11.84275" y1="8.75665" x2="17.32915" y2="8.76935" layer="21"/>
<rectangle x1="4.76885" y1="8.76935" x2="6.02615" y2="8.78205" layer="21"/>
<rectangle x1="6.12775" y1="8.76935" x2="8.60425" y2="8.78205" layer="21"/>
<rectangle x1="8.69315" y1="8.76935" x2="11.75385" y2="8.78205" layer="21"/>
<rectangle x1="11.84275" y1="8.76935" x2="17.32915" y2="8.78205" layer="21"/>
<rectangle x1="4.76885" y1="8.78205" x2="6.02615" y2="8.79475" layer="21"/>
<rectangle x1="6.12775" y1="8.78205" x2="8.60425" y2="8.79475" layer="21"/>
<rectangle x1="8.69315" y1="8.78205" x2="11.75385" y2="8.79475" layer="21"/>
<rectangle x1="11.84275" y1="8.78205" x2="14.39545" y2="8.79475" layer="21"/>
<rectangle x1="14.44625" y1="8.78205" x2="17.32915" y2="8.79475" layer="21"/>
<rectangle x1="4.76885" y1="8.79475" x2="6.02615" y2="8.80745" layer="21"/>
<rectangle x1="6.12775" y1="8.79475" x2="8.60425" y2="8.80745" layer="21"/>
<rectangle x1="8.69315" y1="8.79475" x2="11.75385" y2="8.80745" layer="21"/>
<rectangle x1="11.84275" y1="8.79475" x2="14.37005" y2="8.80745" layer="21"/>
<rectangle x1="14.45895" y1="8.79475" x2="17.32915" y2="8.80745" layer="21"/>
<rectangle x1="4.76885" y1="8.80745" x2="6.02615" y2="8.82015" layer="21"/>
<rectangle x1="6.12775" y1="8.80745" x2="8.60425" y2="8.82015" layer="21"/>
<rectangle x1="8.69315" y1="8.80745" x2="11.75385" y2="8.82015" layer="21"/>
<rectangle x1="11.84275" y1="8.80745" x2="14.35735" y2="8.82015" layer="21"/>
<rectangle x1="14.47165" y1="8.80745" x2="17.32915" y2="8.82015" layer="21"/>
<rectangle x1="4.76885" y1="8.82015" x2="6.02615" y2="8.83285" layer="21"/>
<rectangle x1="6.12775" y1="8.82015" x2="8.60425" y2="8.83285" layer="21"/>
<rectangle x1="8.69315" y1="8.82015" x2="11.75385" y2="8.83285" layer="21"/>
<rectangle x1="11.84275" y1="8.82015" x2="14.35735" y2="8.83285" layer="21"/>
<rectangle x1="14.48435" y1="8.82015" x2="17.32915" y2="8.83285" layer="21"/>
<rectangle x1="4.76885" y1="8.83285" x2="6.02615" y2="8.84555" layer="21"/>
<rectangle x1="6.12775" y1="8.83285" x2="8.60425" y2="8.84555" layer="21"/>
<rectangle x1="8.69315" y1="8.83285" x2="11.75385" y2="8.84555" layer="21"/>
<rectangle x1="11.84275" y1="8.83285" x2="14.35735" y2="8.84555" layer="21"/>
<rectangle x1="14.48435" y1="8.83285" x2="17.32915" y2="8.84555" layer="21"/>
<rectangle x1="4.76885" y1="8.84555" x2="6.02615" y2="8.85825" layer="21"/>
<rectangle x1="6.12775" y1="8.84555" x2="8.60425" y2="8.85825" layer="21"/>
<rectangle x1="8.69315" y1="8.84555" x2="11.75385" y2="8.85825" layer="21"/>
<rectangle x1="11.84275" y1="8.84555" x2="14.35735" y2="8.85825" layer="21"/>
<rectangle x1="14.48435" y1="8.84555" x2="17.32915" y2="8.85825" layer="21"/>
<rectangle x1="4.76885" y1="8.85825" x2="5.56895" y2="8.87095" layer="21"/>
<rectangle x1="6.58495" y1="8.85825" x2="8.60425" y2="8.87095" layer="21"/>
<rectangle x1="8.69315" y1="8.85825" x2="11.75385" y2="8.87095" layer="21"/>
<rectangle x1="11.84275" y1="8.85825" x2="14.35735" y2="8.87095" layer="21"/>
<rectangle x1="14.48435" y1="8.85825" x2="17.32915" y2="8.87095" layer="21"/>
<rectangle x1="4.76885" y1="8.87095" x2="5.56895" y2="8.88365" layer="21"/>
<rectangle x1="6.58495" y1="8.87095" x2="8.60425" y2="8.88365" layer="21"/>
<rectangle x1="8.69315" y1="8.87095" x2="11.75385" y2="8.88365" layer="21"/>
<rectangle x1="11.84275" y1="8.87095" x2="14.35735" y2="8.88365" layer="21"/>
<rectangle x1="14.48435" y1="8.87095" x2="17.32915" y2="8.88365" layer="21"/>
<rectangle x1="4.76885" y1="8.88365" x2="5.56895" y2="8.89635" layer="21"/>
<rectangle x1="6.58495" y1="8.88365" x2="8.60425" y2="8.89635" layer="21"/>
<rectangle x1="8.69315" y1="8.88365" x2="11.75385" y2="8.89635" layer="21"/>
<rectangle x1="11.84275" y1="8.88365" x2="14.35735" y2="8.89635" layer="21"/>
<rectangle x1="14.48435" y1="8.88365" x2="17.32915" y2="8.89635" layer="21"/>
<rectangle x1="4.76885" y1="8.89635" x2="5.56895" y2="8.90905" layer="21"/>
<rectangle x1="6.58495" y1="8.89635" x2="8.60425" y2="8.90905" layer="21"/>
<rectangle x1="8.69315" y1="8.89635" x2="11.75385" y2="8.90905" layer="21"/>
<rectangle x1="11.84275" y1="8.89635" x2="14.37005" y2="8.90905" layer="21"/>
<rectangle x1="14.47165" y1="8.89635" x2="17.32915" y2="8.90905" layer="21"/>
<rectangle x1="4.76885" y1="8.90905" x2="5.56895" y2="8.92175" layer="21"/>
<rectangle x1="6.58495" y1="8.90905" x2="8.60425" y2="8.92175" layer="21"/>
<rectangle x1="8.69315" y1="8.90905" x2="11.75385" y2="8.92175" layer="21"/>
<rectangle x1="11.84275" y1="8.90905" x2="14.38275" y2="8.92175" layer="21"/>
<rectangle x1="14.44625" y1="8.90905" x2="17.32915" y2="8.92175" layer="21"/>
<rectangle x1="4.76885" y1="8.92175" x2="5.56895" y2="8.93445" layer="21"/>
<rectangle x1="6.58495" y1="8.92175" x2="8.60425" y2="8.93445" layer="21"/>
<rectangle x1="8.69315" y1="8.92175" x2="11.75385" y2="8.93445" layer="21"/>
<rectangle x1="11.84275" y1="8.92175" x2="17.32915" y2="8.93445" layer="21"/>
<rectangle x1="4.76885" y1="8.93445" x2="8.60425" y2="8.94715" layer="21"/>
<rectangle x1="8.69315" y1="8.93445" x2="11.75385" y2="8.94715" layer="21"/>
<rectangle x1="11.84275" y1="8.93445" x2="17.32915" y2="8.94715" layer="21"/>
<rectangle x1="4.76885" y1="8.94715" x2="8.60425" y2="8.95985" layer="21"/>
<rectangle x1="8.69315" y1="8.94715" x2="11.75385" y2="8.95985" layer="21"/>
<rectangle x1="11.84275" y1="8.94715" x2="17.32915" y2="8.95985" layer="21"/>
<rectangle x1="4.76885" y1="8.95985" x2="8.60425" y2="8.97255" layer="21"/>
<rectangle x1="8.69315" y1="8.95985" x2="11.75385" y2="8.97255" layer="21"/>
<rectangle x1="11.84275" y1="8.95985" x2="17.32915" y2="8.97255" layer="21"/>
<rectangle x1="4.76885" y1="8.97255" x2="8.60425" y2="8.98525" layer="21"/>
<rectangle x1="8.69315" y1="8.97255" x2="11.75385" y2="8.98525" layer="21"/>
<rectangle x1="11.84275" y1="8.97255" x2="17.32915" y2="8.98525" layer="21"/>
<rectangle x1="4.76885" y1="8.98525" x2="8.60425" y2="8.99795" layer="21"/>
<rectangle x1="8.68045" y1="8.98525" x2="11.75385" y2="8.99795" layer="21"/>
<rectangle x1="11.83005" y1="8.98525" x2="17.32915" y2="8.99795" layer="21"/>
<rectangle x1="4.76885" y1="8.99795" x2="17.32915" y2="9.01065" layer="21"/>
<rectangle x1="4.76885" y1="9.01065" x2="17.32915" y2="9.02335" layer="21"/>
<rectangle x1="4.76885" y1="9.02335" x2="17.32915" y2="9.03605" layer="21"/>
<rectangle x1="4.76885" y1="9.03605" x2="17.32915" y2="9.04875" layer="21"/>
<rectangle x1="4.76885" y1="9.04875" x2="17.32915" y2="9.06145" layer="21"/>
<rectangle x1="4.76885" y1="9.06145" x2="17.32915" y2="9.07415" layer="21"/>
<rectangle x1="4.76885" y1="9.07415" x2="17.32915" y2="9.08685" layer="21"/>
<rectangle x1="4.76885" y1="9.08685" x2="17.32915" y2="9.09955" layer="21"/>
<rectangle x1="4.76885" y1="9.09955" x2="17.32915" y2="9.11225" layer="21"/>
<rectangle x1="4.76885" y1="9.11225" x2="17.32915" y2="9.12495" layer="21"/>
<rectangle x1="4.76885" y1="9.12495" x2="17.32915" y2="9.13765" layer="21"/>
<rectangle x1="4.76885" y1="9.13765" x2="17.32915" y2="9.15035" layer="21"/>
<rectangle x1="4.76885" y1="9.15035" x2="17.32915" y2="9.16305" layer="21"/>
<rectangle x1="4.76885" y1="9.16305" x2="17.32915" y2="9.17575" layer="21"/>
<rectangle x1="4.76885" y1="9.17575" x2="17.32915" y2="9.18845" layer="21"/>
<rectangle x1="4.76885" y1="9.18845" x2="17.32915" y2="9.20115" layer="21"/>
<rectangle x1="4.76885" y1="9.20115" x2="17.32915" y2="9.21385" layer="21"/>
<rectangle x1="4.76885" y1="9.21385" x2="17.32915" y2="9.22655" layer="21"/>
<rectangle x1="4.76885" y1="9.22655" x2="17.32915" y2="9.23925" layer="21"/>
<rectangle x1="4.76885" y1="9.23925" x2="17.32915" y2="9.25195" layer="21"/>
<rectangle x1="4.76885" y1="9.25195" x2="17.32915" y2="9.26465" layer="21"/>
<rectangle x1="4.76885" y1="9.26465" x2="17.32915" y2="9.27735" layer="21"/>
<rectangle x1="4.76885" y1="9.27735" x2="17.32915" y2="9.29005" layer="21"/>
<rectangle x1="4.76885" y1="9.29005" x2="17.32915" y2="9.30275" layer="21"/>
<rectangle x1="4.76885" y1="9.30275" x2="17.32915" y2="9.31545" layer="21"/>
<rectangle x1="4.76885" y1="9.31545" x2="17.32915" y2="9.32815" layer="21"/>
<rectangle x1="4.76885" y1="9.32815" x2="17.32915" y2="9.34085" layer="21"/>
<rectangle x1="4.76885" y1="9.34085" x2="17.32915" y2="9.35355" layer="21"/>
<rectangle x1="4.76885" y1="9.35355" x2="17.32915" y2="9.36625" layer="21"/>
<rectangle x1="4.76885" y1="9.36625" x2="17.32915" y2="9.37895" layer="21"/>
<rectangle x1="4.76885" y1="9.37895" x2="17.32915" y2="9.39165" layer="21"/>
<rectangle x1="4.76885" y1="9.39165" x2="17.32915" y2="9.40435" layer="21"/>
<rectangle x1="4.76885" y1="9.40435" x2="17.32915" y2="9.41705" layer="21"/>
<rectangle x1="4.76885" y1="9.41705" x2="17.32915" y2="9.42975" layer="21"/>
<rectangle x1="4.76885" y1="9.42975" x2="17.32915" y2="9.44245" layer="21"/>
<rectangle x1="4.76885" y1="9.44245" x2="17.32915" y2="9.45515" layer="21"/>
<rectangle x1="4.76885" y1="9.45515" x2="17.32915" y2="9.46785" layer="21"/>
<rectangle x1="4.76885" y1="9.46785" x2="17.32915" y2="9.48055" layer="21"/>
<rectangle x1="4.76885" y1="9.48055" x2="17.32915" y2="9.49325" layer="21"/>
<rectangle x1="4.76885" y1="9.49325" x2="17.32915" y2="9.50595" layer="21"/>
<rectangle x1="4.76885" y1="9.50595" x2="17.32915" y2="9.51865" layer="21"/>
<rectangle x1="4.76885" y1="9.51865" x2="17.32915" y2="9.53135" layer="21"/>
<rectangle x1="4.76885" y1="9.53135" x2="17.32915" y2="9.54405" layer="21"/>
<rectangle x1="4.76885" y1="9.54405" x2="17.32915" y2="9.55675" layer="21"/>
<rectangle x1="4.76885" y1="9.55675" x2="17.32915" y2="9.56945" layer="21"/>
<rectangle x1="4.76885" y1="9.56945" x2="17.32915" y2="9.58215" layer="21"/>
<rectangle x1="4.76885" y1="9.58215" x2="17.32915" y2="9.59485" layer="21"/>
<rectangle x1="4.76885" y1="9.59485" x2="17.32915" y2="9.60755" layer="21"/>
<rectangle x1="4.76885" y1="9.60755" x2="17.32915" y2="9.62025" layer="21"/>
<rectangle x1="4.76885" y1="9.62025" x2="17.32915" y2="9.63295" layer="21"/>
<rectangle x1="4.76885" y1="9.63295" x2="17.32915" y2="9.64565" layer="21"/>
<rectangle x1="4.76885" y1="9.64565" x2="17.32915" y2="9.65835" layer="21"/>
<rectangle x1="4.76885" y1="9.65835" x2="17.32915" y2="9.67105" layer="21"/>
<rectangle x1="4.76885" y1="9.67105" x2="10.26795" y2="9.68375" layer="21"/>
<rectangle x1="10.36955" y1="9.67105" x2="11.06805" y2="9.68375" layer="21"/>
<rectangle x1="11.16965" y1="9.67105" x2="17.32915" y2="9.68375" layer="21"/>
<rectangle x1="4.76885" y1="9.68375" x2="5.59435" y2="9.69645" layer="21"/>
<rectangle x1="5.69595" y1="9.68375" x2="6.72465" y2="9.69645" layer="21"/>
<rectangle x1="6.83895" y1="9.68375" x2="7.42315" y2="9.69645" layer="21"/>
<rectangle x1="7.51205" y1="9.68375" x2="8.08355" y2="9.69645" layer="21"/>
<rectangle x1="8.21055" y1="9.68375" x2="9.07415" y2="9.69645" layer="21"/>
<rectangle x1="9.18845" y1="9.68375" x2="10.26795" y2="9.69645" layer="21"/>
<rectangle x1="10.38225" y1="9.68375" x2="11.06805" y2="9.69645" layer="21"/>
<rectangle x1="11.18235" y1="9.68375" x2="12.19835" y2="9.69645" layer="21"/>
<rectangle x1="12.29995" y1="9.68375" x2="13.32865" y2="9.69645" layer="21"/>
<rectangle x1="13.44295" y1="9.68375" x2="14.02715" y2="9.69645" layer="21"/>
<rectangle x1="14.11605" y1="9.68375" x2="14.86535" y2="9.69645" layer="21"/>
<rectangle x1="14.99235" y1="9.68375" x2="15.58925" y2="9.69645" layer="21"/>
<rectangle x1="16.45285" y1="9.68375" x2="17.32915" y2="9.69645" layer="21"/>
<rectangle x1="4.76885" y1="9.69645" x2="5.59435" y2="9.70915" layer="21"/>
<rectangle x1="5.70865" y1="9.69645" x2="6.72465" y2="9.70915" layer="21"/>
<rectangle x1="6.82625" y1="9.69645" x2="7.42315" y2="9.70915" layer="21"/>
<rectangle x1="7.51205" y1="9.69645" x2="8.09625" y2="9.70915" layer="21"/>
<rectangle x1="8.22325" y1="9.69645" x2="9.06145" y2="9.70915" layer="21"/>
<rectangle x1="9.17575" y1="9.69645" x2="10.25525" y2="9.70915" layer="21"/>
<rectangle x1="10.38225" y1="9.69645" x2="11.05535" y2="9.70915" layer="21"/>
<rectangle x1="11.18235" y1="9.69645" x2="12.19835" y2="9.70915" layer="21"/>
<rectangle x1="12.31265" y1="9.69645" x2="13.32865" y2="9.70915" layer="21"/>
<rectangle x1="13.43025" y1="9.69645" x2="14.02715" y2="9.70915" layer="21"/>
<rectangle x1="14.11605" y1="9.69645" x2="14.85265" y2="9.70915" layer="21"/>
<rectangle x1="14.97965" y1="9.69645" x2="15.58925" y2="9.70915" layer="21"/>
<rectangle x1="16.45285" y1="9.69645" x2="17.32915" y2="9.70915" layer="21"/>
<rectangle x1="4.76885" y1="9.70915" x2="5.60705" y2="9.72185" layer="21"/>
<rectangle x1="5.70865" y1="9.70915" x2="6.71195" y2="9.72185" layer="21"/>
<rectangle x1="6.82625" y1="9.70915" x2="7.42315" y2="9.72185" layer="21"/>
<rectangle x1="7.51205" y1="9.70915" x2="8.10895" y2="9.72185" layer="21"/>
<rectangle x1="8.22325" y1="9.70915" x2="9.04875" y2="9.72185" layer="21"/>
<rectangle x1="9.16305" y1="9.70915" x2="10.25525" y2="9.72185" layer="21"/>
<rectangle x1="10.39495" y1="9.70915" x2="11.05535" y2="9.72185" layer="21"/>
<rectangle x1="11.19505" y1="9.70915" x2="12.21105" y2="9.72185" layer="21"/>
<rectangle x1="12.31265" y1="9.70915" x2="13.31595" y2="9.72185" layer="21"/>
<rectangle x1="13.43025" y1="9.70915" x2="14.02715" y2="9.72185" layer="21"/>
<rectangle x1="14.11605" y1="9.70915" x2="14.83995" y2="9.72185" layer="21"/>
<rectangle x1="14.96695" y1="9.70915" x2="15.58925" y2="9.72185" layer="21"/>
<rectangle x1="16.45285" y1="9.70915" x2="17.32915" y2="9.72185" layer="21"/>
<rectangle x1="4.76885" y1="9.72185" x2="5.60705" y2="9.73455" layer="21"/>
<rectangle x1="5.72135" y1="9.72185" x2="6.71195" y2="9.73455" layer="21"/>
<rectangle x1="6.81355" y1="9.72185" x2="7.42315" y2="9.73455" layer="21"/>
<rectangle x1="7.51205" y1="9.72185" x2="8.12165" y2="9.73455" layer="21"/>
<rectangle x1="8.23595" y1="9.72185" x2="9.03605" y2="9.73455" layer="21"/>
<rectangle x1="9.16305" y1="9.72185" x2="10.24255" y2="9.73455" layer="21"/>
<rectangle x1="10.39495" y1="9.72185" x2="11.04265" y2="9.73455" layer="21"/>
<rectangle x1="11.19505" y1="9.72185" x2="12.21105" y2="9.73455" layer="21"/>
<rectangle x1="12.32535" y1="9.72185" x2="13.31595" y2="9.73455" layer="21"/>
<rectangle x1="13.41755" y1="9.72185" x2="14.02715" y2="9.73455" layer="21"/>
<rectangle x1="14.11605" y1="9.72185" x2="14.82725" y2="9.73455" layer="21"/>
<rectangle x1="14.96695" y1="9.72185" x2="15.58925" y2="9.73455" layer="21"/>
<rectangle x1="16.45285" y1="9.72185" x2="17.32915" y2="9.73455" layer="21"/>
<rectangle x1="4.76885" y1="9.73455" x2="5.61975" y2="9.74725" layer="21"/>
<rectangle x1="5.72135" y1="9.73455" x2="6.69925" y2="9.74725" layer="21"/>
<rectangle x1="6.81355" y1="9.73455" x2="7.42315" y2="9.74725" layer="21"/>
<rectangle x1="7.51205" y1="9.73455" x2="8.12165" y2="9.74725" layer="21"/>
<rectangle x1="8.24865" y1="9.73455" x2="9.02335" y2="9.74725" layer="21"/>
<rectangle x1="9.15035" y1="9.73455" x2="10.24255" y2="9.74725" layer="21"/>
<rectangle x1="10.39495" y1="9.73455" x2="11.04265" y2="9.74725" layer="21"/>
<rectangle x1="11.19505" y1="9.73455" x2="12.22375" y2="9.74725" layer="21"/>
<rectangle x1="12.32535" y1="9.73455" x2="13.30325" y2="9.74725" layer="21"/>
<rectangle x1="13.41755" y1="9.73455" x2="14.02715" y2="9.74725" layer="21"/>
<rectangle x1="14.11605" y1="9.73455" x2="14.81455" y2="9.74725" layer="21"/>
<rectangle x1="14.95425" y1="9.73455" x2="15.58925" y2="9.74725" layer="21"/>
<rectangle x1="16.45285" y1="9.73455" x2="17.32915" y2="9.74725" layer="21"/>
<rectangle x1="4.76885" y1="9.74725" x2="5.61975" y2="9.75995" layer="21"/>
<rectangle x1="5.73405" y1="9.74725" x2="6.69925" y2="9.75995" layer="21"/>
<rectangle x1="6.81355" y1="9.74725" x2="7.42315" y2="9.75995" layer="21"/>
<rectangle x1="7.51205" y1="9.74725" x2="8.13435" y2="9.75995" layer="21"/>
<rectangle x1="8.26135" y1="9.74725" x2="9.02335" y2="9.75995" layer="21"/>
<rectangle x1="9.13765" y1="9.74725" x2="10.24255" y2="9.75995" layer="21"/>
<rectangle x1="10.40765" y1="9.74725" x2="11.04265" y2="9.75995" layer="21"/>
<rectangle x1="11.20775" y1="9.74725" x2="12.22375" y2="9.75995" layer="21"/>
<rectangle x1="12.33805" y1="9.74725" x2="13.30325" y2="9.75995" layer="21"/>
<rectangle x1="13.41755" y1="9.74725" x2="14.02715" y2="9.75995" layer="21"/>
<rectangle x1="14.11605" y1="9.74725" x2="14.81455" y2="9.75995" layer="21"/>
<rectangle x1="14.94155" y1="9.74725" x2="15.58925" y2="9.75995" layer="21"/>
<rectangle x1="16.45285" y1="9.74725" x2="17.32915" y2="9.75995" layer="21"/>
<rectangle x1="4.76885" y1="9.75995" x2="5.63245" y2="9.77265" layer="21"/>
<rectangle x1="5.73405" y1="9.75995" x2="6.69925" y2="9.77265" layer="21"/>
<rectangle x1="6.80085" y1="9.75995" x2="7.42315" y2="9.77265" layer="21"/>
<rectangle x1="7.51205" y1="9.75995" x2="8.14705" y2="9.77265" layer="21"/>
<rectangle x1="8.27405" y1="9.75995" x2="9.01065" y2="9.77265" layer="21"/>
<rectangle x1="9.12495" y1="9.75995" x2="10.22985" y2="9.77265" layer="21"/>
<rectangle x1="10.40765" y1="9.75995" x2="11.02995" y2="9.77265" layer="21"/>
<rectangle x1="11.20775" y1="9.75995" x2="12.23645" y2="9.77265" layer="21"/>
<rectangle x1="12.33805" y1="9.75995" x2="13.30325" y2="9.77265" layer="21"/>
<rectangle x1="13.40485" y1="9.75995" x2="14.02715" y2="9.77265" layer="21"/>
<rectangle x1="14.11605" y1="9.75995" x2="14.80185" y2="9.77265" layer="21"/>
<rectangle x1="14.92885" y1="9.75995" x2="15.58925" y2="9.77265" layer="21"/>
<rectangle x1="16.45285" y1="9.75995" x2="17.32915" y2="9.77265" layer="21"/>
<rectangle x1="4.76885" y1="9.77265" x2="5.63245" y2="9.78535" layer="21"/>
<rectangle x1="5.74675" y1="9.77265" x2="6.68655" y2="9.78535" layer="21"/>
<rectangle x1="6.80085" y1="9.77265" x2="7.42315" y2="9.78535" layer="21"/>
<rectangle x1="7.51205" y1="9.77265" x2="8.15975" y2="9.78535" layer="21"/>
<rectangle x1="8.27405" y1="9.77265" x2="8.99795" y2="9.78535" layer="21"/>
<rectangle x1="9.11225" y1="9.77265" x2="10.22985" y2="9.78535" layer="21"/>
<rectangle x1="10.42035" y1="9.77265" x2="11.02995" y2="9.78535" layer="21"/>
<rectangle x1="11.22045" y1="9.77265" x2="12.23645" y2="9.78535" layer="21"/>
<rectangle x1="12.35075" y1="9.77265" x2="13.29055" y2="9.78535" layer="21"/>
<rectangle x1="13.40485" y1="9.77265" x2="14.02715" y2="9.78535" layer="21"/>
<rectangle x1="14.11605" y1="9.77265" x2="14.78915" y2="9.78535" layer="21"/>
<rectangle x1="14.91615" y1="9.77265" x2="15.58925" y2="9.78535" layer="21"/>
<rectangle x1="15.67815" y1="9.77265" x2="17.32915" y2="9.78535" layer="21"/>
<rectangle x1="4.76885" y1="9.78535" x2="5.64515" y2="9.79805" layer="21"/>
<rectangle x1="5.74675" y1="9.78535" x2="6.68655" y2="9.79805" layer="21"/>
<rectangle x1="6.78815" y1="9.78535" x2="7.42315" y2="9.79805" layer="21"/>
<rectangle x1="7.51205" y1="9.78535" x2="8.17245" y2="9.79805" layer="21"/>
<rectangle x1="8.28675" y1="9.78535" x2="8.98525" y2="9.79805" layer="21"/>
<rectangle x1="9.11225" y1="9.78535" x2="10.21715" y2="9.79805" layer="21"/>
<rectangle x1="10.42035" y1="9.78535" x2="11.01725" y2="9.79805" layer="21"/>
<rectangle x1="11.22045" y1="9.78535" x2="12.24915" y2="9.79805" layer="21"/>
<rectangle x1="12.35075" y1="9.78535" x2="13.29055" y2="9.79805" layer="21"/>
<rectangle x1="13.39215" y1="9.78535" x2="14.02715" y2="9.79805" layer="21"/>
<rectangle x1="14.11605" y1="9.78535" x2="14.77645" y2="9.79805" layer="21"/>
<rectangle x1="14.90345" y1="9.78535" x2="15.58925" y2="9.79805" layer="21"/>
<rectangle x1="15.67815" y1="9.78535" x2="17.32915" y2="9.79805" layer="21"/>
<rectangle x1="4.76885" y1="9.79805" x2="5.64515" y2="9.81075" layer="21"/>
<rectangle x1="5.75945" y1="9.79805" x2="6.67385" y2="9.81075" layer="21"/>
<rectangle x1="6.78815" y1="9.79805" x2="7.42315" y2="9.81075" layer="21"/>
<rectangle x1="7.51205" y1="9.79805" x2="8.17245" y2="9.81075" layer="21"/>
<rectangle x1="8.29945" y1="9.79805" x2="8.97255" y2="9.81075" layer="21"/>
<rectangle x1="9.09955" y1="9.79805" x2="10.21715" y2="9.81075" layer="21"/>
<rectangle x1="10.43305" y1="9.79805" x2="11.01725" y2="9.81075" layer="21"/>
<rectangle x1="11.23315" y1="9.79805" x2="12.24915" y2="9.81075" layer="21"/>
<rectangle x1="12.36345" y1="9.79805" x2="13.27785" y2="9.81075" layer="21"/>
<rectangle x1="13.39215" y1="9.79805" x2="14.02715" y2="9.81075" layer="21"/>
<rectangle x1="14.11605" y1="9.79805" x2="14.76375" y2="9.81075" layer="21"/>
<rectangle x1="14.89075" y1="9.79805" x2="15.58925" y2="9.81075" layer="21"/>
<rectangle x1="15.67815" y1="9.79805" x2="17.32915" y2="9.81075" layer="21"/>
<rectangle x1="4.76885" y1="9.81075" x2="5.64515" y2="9.82345" layer="21"/>
<rectangle x1="5.75945" y1="9.81075" x2="6.67385" y2="9.82345" layer="21"/>
<rectangle x1="6.77545" y1="9.81075" x2="7.42315" y2="9.82345" layer="21"/>
<rectangle x1="7.51205" y1="9.81075" x2="8.18515" y2="9.82345" layer="21"/>
<rectangle x1="8.31215" y1="9.81075" x2="8.97255" y2="9.82345" layer="21"/>
<rectangle x1="9.08685" y1="9.81075" x2="10.20445" y2="9.82345" layer="21"/>
<rectangle x1="10.31875" y1="9.81075" x2="10.33145" y2="9.82345" layer="21"/>
<rectangle x1="10.43305" y1="9.81075" x2="11.00455" y2="9.82345" layer="21"/>
<rectangle x1="11.11885" y1="9.81075" x2="11.13155" y2="9.82345" layer="21"/>
<rectangle x1="11.23315" y1="9.81075" x2="12.24915" y2="9.82345" layer="21"/>
<rectangle x1="12.36345" y1="9.81075" x2="13.27785" y2="9.82345" layer="21"/>
<rectangle x1="13.37945" y1="9.81075" x2="14.02715" y2="9.82345" layer="21"/>
<rectangle x1="14.11605" y1="9.81075" x2="14.75105" y2="9.82345" layer="21"/>
<rectangle x1="14.89075" y1="9.81075" x2="15.58925" y2="9.82345" layer="21"/>
<rectangle x1="15.67815" y1="9.81075" x2="17.32915" y2="9.82345" layer="21"/>
<rectangle x1="4.76885" y1="9.82345" x2="5.65785" y2="9.83615" layer="21"/>
<rectangle x1="5.75945" y1="9.82345" x2="6.66115" y2="9.83615" layer="21"/>
<rectangle x1="6.77545" y1="9.82345" x2="7.42315" y2="9.83615" layer="21"/>
<rectangle x1="7.51205" y1="9.82345" x2="8.19785" y2="9.83615" layer="21"/>
<rectangle x1="8.32485" y1="9.82345" x2="8.95985" y2="9.83615" layer="21"/>
<rectangle x1="9.07415" y1="9.82345" x2="10.20445" y2="9.83615" layer="21"/>
<rectangle x1="10.30605" y1="9.82345" x2="10.33145" y2="9.83615" layer="21"/>
<rectangle x1="10.44575" y1="9.82345" x2="11.00455" y2="9.83615" layer="21"/>
<rectangle x1="11.10615" y1="9.82345" x2="11.13155" y2="9.83615" layer="21"/>
<rectangle x1="11.24585" y1="9.82345" x2="12.26185" y2="9.83615" layer="21"/>
<rectangle x1="12.36345" y1="9.82345" x2="13.26515" y2="9.83615" layer="21"/>
<rectangle x1="13.37945" y1="9.82345" x2="14.02715" y2="9.83615" layer="21"/>
<rectangle x1="14.11605" y1="9.82345" x2="14.73835" y2="9.83615" layer="21"/>
<rectangle x1="14.87805" y1="9.82345" x2="15.58925" y2="9.83615" layer="21"/>
<rectangle x1="15.67815" y1="9.82345" x2="17.32915" y2="9.83615" layer="21"/>
<rectangle x1="4.76885" y1="9.83615" x2="5.65785" y2="9.84885" layer="21"/>
<rectangle x1="5.77215" y1="9.83615" x2="6.66115" y2="9.84885" layer="21"/>
<rectangle x1="6.76275" y1="9.83615" x2="7.42315" y2="9.84885" layer="21"/>
<rectangle x1="7.51205" y1="9.83615" x2="8.21055" y2="9.84885" layer="21"/>
<rectangle x1="8.32485" y1="9.83615" x2="8.94715" y2="9.84885" layer="21"/>
<rectangle x1="9.07415" y1="9.83615" x2="10.19175" y2="9.84885" layer="21"/>
<rectangle x1="10.30605" y1="9.83615" x2="10.34415" y2="9.84885" layer="21"/>
<rectangle x1="10.44575" y1="9.83615" x2="10.99185" y2="9.84885" layer="21"/>
<rectangle x1="11.10615" y1="9.83615" x2="11.14425" y2="9.84885" layer="21"/>
<rectangle x1="11.24585" y1="9.83615" x2="12.26185" y2="9.84885" layer="21"/>
<rectangle x1="12.37615" y1="9.83615" x2="13.26515" y2="9.84885" layer="21"/>
<rectangle x1="13.36675" y1="9.83615" x2="14.02715" y2="9.84885" layer="21"/>
<rectangle x1="14.11605" y1="9.83615" x2="14.73835" y2="9.84885" layer="21"/>
<rectangle x1="14.86535" y1="9.83615" x2="15.58925" y2="9.84885" layer="21"/>
<rectangle x1="15.67815" y1="9.83615" x2="17.32915" y2="9.84885" layer="21"/>
<rectangle x1="4.76885" y1="9.84885" x2="5.67055" y2="9.86155" layer="21"/>
<rectangle x1="5.77215" y1="9.84885" x2="6.64845" y2="9.86155" layer="21"/>
<rectangle x1="6.76275" y1="9.84885" x2="7.42315" y2="9.86155" layer="21"/>
<rectangle x1="7.51205" y1="9.84885" x2="8.21055" y2="9.86155" layer="21"/>
<rectangle x1="8.33755" y1="9.84885" x2="8.93445" y2="9.86155" layer="21"/>
<rectangle x1="9.06145" y1="9.84885" x2="10.19175" y2="9.86155" layer="21"/>
<rectangle x1="10.29335" y1="9.84885" x2="10.34415" y2="9.86155" layer="21"/>
<rectangle x1="10.45845" y1="9.84885" x2="10.99185" y2="9.86155" layer="21"/>
<rectangle x1="11.09345" y1="9.84885" x2="11.14425" y2="9.86155" layer="21"/>
<rectangle x1="11.25855" y1="9.84885" x2="12.27455" y2="9.86155" layer="21"/>
<rectangle x1="12.37615" y1="9.84885" x2="13.25245" y2="9.86155" layer="21"/>
<rectangle x1="13.36675" y1="9.84885" x2="14.02715" y2="9.86155" layer="21"/>
<rectangle x1="14.11605" y1="9.84885" x2="14.72565" y2="9.86155" layer="21"/>
<rectangle x1="14.85265" y1="9.84885" x2="15.58925" y2="9.86155" layer="21"/>
<rectangle x1="15.67815" y1="9.84885" x2="17.32915" y2="9.86155" layer="21"/>
<rectangle x1="4.76885" y1="9.86155" x2="5.67055" y2="9.87425" layer="21"/>
<rectangle x1="5.78485" y1="9.86155" x2="6.64845" y2="9.87425" layer="21"/>
<rectangle x1="6.75005" y1="9.86155" x2="7.42315" y2="9.87425" layer="21"/>
<rectangle x1="7.51205" y1="9.86155" x2="8.22325" y2="9.87425" layer="21"/>
<rectangle x1="8.35025" y1="9.86155" x2="8.92175" y2="9.87425" layer="21"/>
<rectangle x1="9.04875" y1="9.86155" x2="10.17905" y2="9.87425" layer="21"/>
<rectangle x1="10.29335" y1="9.86155" x2="10.35685" y2="9.87425" layer="21"/>
<rectangle x1="10.45845" y1="9.86155" x2="10.97915" y2="9.87425" layer="21"/>
<rectangle x1="11.09345" y1="9.86155" x2="11.15695" y2="9.87425" layer="21"/>
<rectangle x1="11.25855" y1="9.86155" x2="12.27455" y2="9.87425" layer="21"/>
<rectangle x1="12.38885" y1="9.86155" x2="13.25245" y2="9.87425" layer="21"/>
<rectangle x1="13.35405" y1="9.86155" x2="14.02715" y2="9.87425" layer="21"/>
<rectangle x1="14.11605" y1="9.86155" x2="14.71295" y2="9.87425" layer="21"/>
<rectangle x1="14.83995" y1="9.86155" x2="15.58925" y2="9.87425" layer="21"/>
<rectangle x1="15.67815" y1="9.86155" x2="17.32915" y2="9.87425" layer="21"/>
<rectangle x1="4.76885" y1="9.87425" x2="5.68325" y2="9.88695" layer="21"/>
<rectangle x1="5.78485" y1="9.87425" x2="6.63575" y2="9.88695" layer="21"/>
<rectangle x1="6.75005" y1="9.87425" x2="7.42315" y2="9.88695" layer="21"/>
<rectangle x1="7.51205" y1="9.87425" x2="8.23595" y2="9.88695" layer="21"/>
<rectangle x1="8.36295" y1="9.87425" x2="8.92175" y2="9.88695" layer="21"/>
<rectangle x1="9.03605" y1="9.87425" x2="10.17905" y2="9.88695" layer="21"/>
<rectangle x1="10.29335" y1="9.87425" x2="10.35685" y2="9.88695" layer="21"/>
<rectangle x1="10.47115" y1="9.87425" x2="10.97915" y2="9.88695" layer="21"/>
<rectangle x1="11.09345" y1="9.87425" x2="11.15695" y2="9.88695" layer="21"/>
<rectangle x1="11.27125" y1="9.87425" x2="12.28725" y2="9.88695" layer="21"/>
<rectangle x1="12.38885" y1="9.87425" x2="13.23975" y2="9.88695" layer="21"/>
<rectangle x1="13.35405" y1="9.87425" x2="14.02715" y2="9.88695" layer="21"/>
<rectangle x1="14.11605" y1="9.87425" x2="14.70025" y2="9.88695" layer="21"/>
<rectangle x1="14.82725" y1="9.87425" x2="15.58925" y2="9.88695" layer="21"/>
<rectangle x1="15.67815" y1="9.87425" x2="17.32915" y2="9.88695" layer="21"/>
<rectangle x1="4.76885" y1="9.88695" x2="5.68325" y2="9.89965" layer="21"/>
<rectangle x1="5.79755" y1="9.88695" x2="6.63575" y2="9.89965" layer="21"/>
<rectangle x1="6.75005" y1="9.88695" x2="7.42315" y2="9.89965" layer="21"/>
<rectangle x1="7.51205" y1="9.88695" x2="8.24865" y2="9.89965" layer="21"/>
<rectangle x1="8.37565" y1="9.88695" x2="8.90905" y2="9.89965" layer="21"/>
<rectangle x1="9.02335" y1="9.88695" x2="10.16635" y2="9.89965" layer="21"/>
<rectangle x1="10.28065" y1="9.88695" x2="10.35685" y2="9.89965" layer="21"/>
<rectangle x1="10.47115" y1="9.88695" x2="10.96645" y2="9.89965" layer="21"/>
<rectangle x1="11.08075" y1="9.88695" x2="11.15695" y2="9.89965" layer="21"/>
<rectangle x1="11.27125" y1="9.88695" x2="12.28725" y2="9.89965" layer="21"/>
<rectangle x1="12.40155" y1="9.88695" x2="13.23975" y2="9.89965" layer="21"/>
<rectangle x1="13.35405" y1="9.88695" x2="14.02715" y2="9.89965" layer="21"/>
<rectangle x1="14.11605" y1="9.88695" x2="14.68755" y2="9.89965" layer="21"/>
<rectangle x1="14.81455" y1="9.88695" x2="15.58925" y2="9.89965" layer="21"/>
<rectangle x1="15.67815" y1="9.88695" x2="17.32915" y2="9.89965" layer="21"/>
<rectangle x1="4.76885" y1="9.89965" x2="5.69595" y2="9.91235" layer="21"/>
<rectangle x1="5.79755" y1="9.89965" x2="6.63575" y2="9.91235" layer="21"/>
<rectangle x1="6.73735" y1="9.89965" x2="7.42315" y2="9.91235" layer="21"/>
<rectangle x1="7.51205" y1="9.89965" x2="8.26135" y2="9.91235" layer="21"/>
<rectangle x1="8.37565" y1="9.89965" x2="8.89635" y2="9.91235" layer="21"/>
<rectangle x1="9.02335" y1="9.89965" x2="10.16635" y2="9.91235" layer="21"/>
<rectangle x1="10.28065" y1="9.89965" x2="10.36955" y2="9.91235" layer="21"/>
<rectangle x1="10.47115" y1="9.89965" x2="10.96645" y2="9.91235" layer="21"/>
<rectangle x1="11.08075" y1="9.89965" x2="11.16965" y2="9.91235" layer="21"/>
<rectangle x1="11.27125" y1="9.89965" x2="12.29995" y2="9.91235" layer="21"/>
<rectangle x1="12.40155" y1="9.89965" x2="13.23975" y2="9.91235" layer="21"/>
<rectangle x1="13.34135" y1="9.89965" x2="14.02715" y2="9.91235" layer="21"/>
<rectangle x1="14.11605" y1="9.89965" x2="14.67485" y2="9.91235" layer="21"/>
<rectangle x1="14.81455" y1="9.89965" x2="15.58925" y2="9.91235" layer="21"/>
<rectangle x1="15.67815" y1="9.89965" x2="17.32915" y2="9.91235" layer="21"/>
<rectangle x1="4.76885" y1="9.91235" x2="5.69595" y2="9.92505" layer="21"/>
<rectangle x1="5.81025" y1="9.91235" x2="6.62305" y2="9.92505" layer="21"/>
<rectangle x1="6.73735" y1="9.91235" x2="7.42315" y2="9.92505" layer="21"/>
<rectangle x1="7.51205" y1="9.91235" x2="8.26135" y2="9.92505" layer="21"/>
<rectangle x1="8.38835" y1="9.91235" x2="8.88365" y2="9.92505" layer="21"/>
<rectangle x1="9.01065" y1="9.91235" x2="10.16635" y2="9.92505" layer="21"/>
<rectangle x1="10.26795" y1="9.91235" x2="10.36955" y2="9.92505" layer="21"/>
<rectangle x1="10.48385" y1="9.91235" x2="10.96645" y2="9.92505" layer="21"/>
<rectangle x1="11.06805" y1="9.91235" x2="11.16965" y2="9.92505" layer="21"/>
<rectangle x1="11.28395" y1="9.91235" x2="12.29995" y2="9.92505" layer="21"/>
<rectangle x1="12.41425" y1="9.91235" x2="13.22705" y2="9.92505" layer="21"/>
<rectangle x1="13.34135" y1="9.91235" x2="14.02715" y2="9.92505" layer="21"/>
<rectangle x1="14.11605" y1="9.91235" x2="14.66215" y2="9.92505" layer="21"/>
<rectangle x1="14.80185" y1="9.91235" x2="15.58925" y2="9.92505" layer="21"/>
<rectangle x1="15.67815" y1="9.91235" x2="17.32915" y2="9.92505" layer="21"/>
<rectangle x1="4.76885" y1="9.92505" x2="5.70865" y2="9.93775" layer="21"/>
<rectangle x1="5.81025" y1="9.92505" x2="6.62305" y2="9.93775" layer="21"/>
<rectangle x1="6.72465" y1="9.92505" x2="7.42315" y2="9.93775" layer="21"/>
<rectangle x1="7.51205" y1="9.92505" x2="8.27405" y2="9.93775" layer="21"/>
<rectangle x1="8.40105" y1="9.92505" x2="8.87095" y2="9.93775" layer="21"/>
<rectangle x1="8.99795" y1="9.92505" x2="10.15365" y2="9.93775" layer="21"/>
<rectangle x1="10.26795" y1="9.92505" x2="10.38225" y2="9.93775" layer="21"/>
<rectangle x1="10.48385" y1="9.92505" x2="10.95375" y2="9.93775" layer="21"/>
<rectangle x1="11.06805" y1="9.92505" x2="11.18235" y2="9.93775" layer="21"/>
<rectangle x1="11.28395" y1="9.92505" x2="12.31265" y2="9.93775" layer="21"/>
<rectangle x1="12.41425" y1="9.92505" x2="13.22705" y2="9.93775" layer="21"/>
<rectangle x1="13.32865" y1="9.92505" x2="14.02715" y2="9.93775" layer="21"/>
<rectangle x1="14.11605" y1="9.92505" x2="14.66215" y2="9.93775" layer="21"/>
<rectangle x1="14.78915" y1="9.92505" x2="15.58925" y2="9.93775" layer="21"/>
<rectangle x1="15.67815" y1="9.92505" x2="17.32915" y2="9.93775" layer="21"/>
<rectangle x1="4.76885" y1="9.93775" x2="5.70865" y2="9.95045" layer="21"/>
<rectangle x1="5.82295" y1="9.93775" x2="6.61035" y2="9.95045" layer="21"/>
<rectangle x1="6.72465" y1="9.93775" x2="7.42315" y2="9.95045" layer="21"/>
<rectangle x1="7.51205" y1="9.93775" x2="8.28675" y2="9.95045" layer="21"/>
<rectangle x1="8.41375" y1="9.93775" x2="8.87095" y2="9.95045" layer="21"/>
<rectangle x1="8.98525" y1="9.93775" x2="10.15365" y2="9.95045" layer="21"/>
<rectangle x1="10.25525" y1="9.93775" x2="10.38225" y2="9.95045" layer="21"/>
<rectangle x1="10.49655" y1="9.93775" x2="10.95375" y2="9.95045" layer="21"/>
<rectangle x1="11.05535" y1="9.93775" x2="11.18235" y2="9.95045" layer="21"/>
<rectangle x1="11.29665" y1="9.93775" x2="12.31265" y2="9.95045" layer="21"/>
<rectangle x1="12.42695" y1="9.93775" x2="13.21435" y2="9.95045" layer="21"/>
<rectangle x1="13.32865" y1="9.93775" x2="14.02715" y2="9.95045" layer="21"/>
<rectangle x1="14.11605" y1="9.93775" x2="14.64945" y2="9.95045" layer="21"/>
<rectangle x1="14.77645" y1="9.93775" x2="15.58925" y2="9.95045" layer="21"/>
<rectangle x1="15.67815" y1="9.93775" x2="17.32915" y2="9.95045" layer="21"/>
<rectangle x1="4.76885" y1="9.95045" x2="5.72135" y2="9.96315" layer="21"/>
<rectangle x1="5.82295" y1="9.95045" x2="6.61035" y2="9.96315" layer="21"/>
<rectangle x1="6.71195" y1="9.95045" x2="7.42315" y2="9.96315" layer="21"/>
<rectangle x1="7.51205" y1="9.95045" x2="8.29945" y2="9.96315" layer="21"/>
<rectangle x1="8.42645" y1="9.95045" x2="8.85825" y2="9.96315" layer="21"/>
<rectangle x1="8.98525" y1="9.95045" x2="10.14095" y2="9.96315" layer="21"/>
<rectangle x1="10.25525" y1="9.95045" x2="10.39495" y2="9.96315" layer="21"/>
<rectangle x1="10.49655" y1="9.95045" x2="10.94105" y2="9.96315" layer="21"/>
<rectangle x1="11.05535" y1="9.95045" x2="11.19505" y2="9.96315" layer="21"/>
<rectangle x1="11.29665" y1="9.95045" x2="12.32535" y2="9.96315" layer="21"/>
<rectangle x1="12.42695" y1="9.95045" x2="13.21435" y2="9.96315" layer="21"/>
<rectangle x1="13.31595" y1="9.95045" x2="14.02715" y2="9.96315" layer="21"/>
<rectangle x1="14.11605" y1="9.95045" x2="14.63675" y2="9.96315" layer="21"/>
<rectangle x1="14.76375" y1="9.95045" x2="15.58925" y2="9.96315" layer="21"/>
<rectangle x1="15.67815" y1="9.95045" x2="17.32915" y2="9.96315" layer="21"/>
<rectangle x1="4.76885" y1="9.96315" x2="5.72135" y2="9.97585" layer="21"/>
<rectangle x1="5.82295" y1="9.96315" x2="6.59765" y2="9.97585" layer="21"/>
<rectangle x1="6.71195" y1="9.96315" x2="7.42315" y2="9.97585" layer="21"/>
<rectangle x1="7.51205" y1="9.96315" x2="8.31215" y2="9.97585" layer="21"/>
<rectangle x1="8.42645" y1="9.96315" x2="8.84555" y2="9.97585" layer="21"/>
<rectangle x1="8.97255" y1="9.96315" x2="10.14095" y2="9.97585" layer="21"/>
<rectangle x1="10.24255" y1="9.96315" x2="10.39495" y2="9.97585" layer="21"/>
<rectangle x1="10.50925" y1="9.96315" x2="10.94105" y2="9.97585" layer="21"/>
<rectangle x1="11.04265" y1="9.96315" x2="11.19505" y2="9.97585" layer="21"/>
<rectangle x1="11.30935" y1="9.96315" x2="12.32535" y2="9.97585" layer="21"/>
<rectangle x1="12.42695" y1="9.96315" x2="13.20165" y2="9.97585" layer="21"/>
<rectangle x1="13.31595" y1="9.96315" x2="14.02715" y2="9.97585" layer="21"/>
<rectangle x1="14.11605" y1="9.96315" x2="14.62405" y2="9.97585" layer="21"/>
<rectangle x1="14.75105" y1="9.96315" x2="15.58925" y2="9.97585" layer="21"/>
<rectangle x1="15.67815" y1="9.96315" x2="17.32915" y2="9.97585" layer="21"/>
<rectangle x1="4.76885" y1="9.97585" x2="5.72135" y2="9.98855" layer="21"/>
<rectangle x1="5.83565" y1="9.97585" x2="6.59765" y2="9.98855" layer="21"/>
<rectangle x1="6.69925" y1="9.97585" x2="7.42315" y2="9.98855" layer="21"/>
<rectangle x1="7.51205" y1="9.97585" x2="8.31215" y2="9.98855" layer="21"/>
<rectangle x1="8.43915" y1="9.97585" x2="8.83285" y2="9.98855" layer="21"/>
<rectangle x1="8.95985" y1="9.97585" x2="10.12825" y2="9.98855" layer="21"/>
<rectangle x1="10.24255" y1="9.97585" x2="10.40765" y2="9.98855" layer="21"/>
<rectangle x1="10.50925" y1="9.97585" x2="10.92835" y2="9.98855" layer="21"/>
<rectangle x1="11.04265" y1="9.97585" x2="11.20775" y2="9.98855" layer="21"/>
<rectangle x1="11.30935" y1="9.97585" x2="12.32535" y2="9.98855" layer="21"/>
<rectangle x1="12.43965" y1="9.97585" x2="13.20165" y2="9.98855" layer="21"/>
<rectangle x1="13.30325" y1="9.97585" x2="14.02715" y2="9.98855" layer="21"/>
<rectangle x1="14.11605" y1="9.97585" x2="14.61135" y2="9.98855" layer="21"/>
<rectangle x1="14.73835" y1="9.97585" x2="15.58925" y2="9.98855" layer="21"/>
<rectangle x1="15.67815" y1="9.97585" x2="17.32915" y2="9.98855" layer="21"/>
<rectangle x1="4.76885" y1="9.98855" x2="5.73405" y2="10.00125" layer="21"/>
<rectangle x1="5.83565" y1="9.98855" x2="6.58495" y2="10.00125" layer="21"/>
<rectangle x1="6.69925" y1="9.98855" x2="7.42315" y2="10.00125" layer="21"/>
<rectangle x1="7.51205" y1="9.98855" x2="8.32485" y2="10.00125" layer="21"/>
<rectangle x1="8.45185" y1="9.98855" x2="8.82015" y2="10.00125" layer="21"/>
<rectangle x1="8.94715" y1="9.98855" x2="10.12825" y2="10.00125" layer="21"/>
<rectangle x1="10.22985" y1="9.98855" x2="10.40765" y2="10.00125" layer="21"/>
<rectangle x1="10.52195" y1="9.98855" x2="10.92835" y2="10.00125" layer="21"/>
<rectangle x1="11.02995" y1="9.98855" x2="11.20775" y2="10.00125" layer="21"/>
<rectangle x1="11.32205" y1="9.98855" x2="12.33805" y2="10.00125" layer="21"/>
<rectangle x1="12.43965" y1="9.98855" x2="13.18895" y2="10.00125" layer="21"/>
<rectangle x1="13.30325" y1="9.98855" x2="14.02715" y2="10.00125" layer="21"/>
<rectangle x1="14.11605" y1="9.98855" x2="14.59865" y2="10.00125" layer="21"/>
<rectangle x1="14.73835" y1="9.98855" x2="15.58925" y2="10.00125" layer="21"/>
<rectangle x1="15.67815" y1="9.98855" x2="17.32915" y2="10.00125" layer="21"/>
<rectangle x1="4.76885" y1="10.00125" x2="5.73405" y2="10.01395" layer="21"/>
<rectangle x1="5.84835" y1="10.00125" x2="6.58495" y2="10.01395" layer="21"/>
<rectangle x1="6.68655" y1="10.00125" x2="7.42315" y2="10.01395" layer="21"/>
<rectangle x1="7.51205" y1="10.00125" x2="8.33755" y2="10.01395" layer="21"/>
<rectangle x1="8.46455" y1="10.00125" x2="8.82015" y2="10.01395" layer="21"/>
<rectangle x1="8.93445" y1="10.00125" x2="10.11555" y2="10.01395" layer="21"/>
<rectangle x1="10.22985" y1="10.00125" x2="10.42035" y2="10.01395" layer="21"/>
<rectangle x1="10.52195" y1="10.00125" x2="10.91565" y2="10.01395" layer="21"/>
<rectangle x1="11.02995" y1="10.00125" x2="11.22045" y2="10.01395" layer="21"/>
<rectangle x1="11.32205" y1="10.00125" x2="12.33805" y2="10.01395" layer="21"/>
<rectangle x1="12.45235" y1="10.00125" x2="13.18895" y2="10.01395" layer="21"/>
<rectangle x1="13.29055" y1="10.00125" x2="14.02715" y2="10.01395" layer="21"/>
<rectangle x1="14.11605" y1="10.00125" x2="14.58595" y2="10.01395" layer="21"/>
<rectangle x1="14.72565" y1="10.00125" x2="15.58925" y2="10.01395" layer="21"/>
<rectangle x1="15.67815" y1="10.00125" x2="17.32915" y2="10.01395" layer="21"/>
<rectangle x1="4.76885" y1="10.01395" x2="5.74675" y2="10.02665" layer="21"/>
<rectangle x1="5.84835" y1="10.01395" x2="6.57225" y2="10.02665" layer="21"/>
<rectangle x1="6.68655" y1="10.01395" x2="7.42315" y2="10.02665" layer="21"/>
<rectangle x1="7.51205" y1="10.01395" x2="8.35025" y2="10.02665" layer="21"/>
<rectangle x1="8.47725" y1="10.01395" x2="8.80745" y2="10.02665" layer="21"/>
<rectangle x1="8.93445" y1="10.01395" x2="10.11555" y2="10.02665" layer="21"/>
<rectangle x1="10.21715" y1="10.01395" x2="10.42035" y2="10.02665" layer="21"/>
<rectangle x1="10.53465" y1="10.01395" x2="10.91565" y2="10.02665" layer="21"/>
<rectangle x1="11.01725" y1="10.01395" x2="11.22045" y2="10.02665" layer="21"/>
<rectangle x1="11.33475" y1="10.01395" x2="12.35075" y2="10.02665" layer="21"/>
<rectangle x1="12.45235" y1="10.01395" x2="13.17625" y2="10.02665" layer="21"/>
<rectangle x1="13.29055" y1="10.01395" x2="14.02715" y2="10.02665" layer="21"/>
<rectangle x1="14.11605" y1="10.01395" x2="14.58595" y2="10.02665" layer="21"/>
<rectangle x1="14.71295" y1="10.01395" x2="15.58925" y2="10.02665" layer="21"/>
<rectangle x1="15.67815" y1="10.01395" x2="17.32915" y2="10.02665" layer="21"/>
<rectangle x1="4.76885" y1="10.02665" x2="5.74675" y2="10.03935" layer="21"/>
<rectangle x1="5.86105" y1="10.02665" x2="6.57225" y2="10.03935" layer="21"/>
<rectangle x1="6.67385" y1="10.02665" x2="7.42315" y2="10.03935" layer="21"/>
<rectangle x1="7.51205" y1="10.02665" x2="8.35025" y2="10.03935" layer="21"/>
<rectangle x1="8.47725" y1="10.02665" x2="8.79475" y2="10.03935" layer="21"/>
<rectangle x1="8.92175" y1="10.02665" x2="10.10285" y2="10.03935" layer="21"/>
<rectangle x1="10.21715" y1="10.02665" x2="10.43305" y2="10.03935" layer="21"/>
<rectangle x1="10.53465" y1="10.02665" x2="10.90295" y2="10.03935" layer="21"/>
<rectangle x1="11.01725" y1="10.02665" x2="11.23315" y2="10.03935" layer="21"/>
<rectangle x1="11.33475" y1="10.02665" x2="12.35075" y2="10.03935" layer="21"/>
<rectangle x1="12.46505" y1="10.02665" x2="13.17625" y2="10.03935" layer="21"/>
<rectangle x1="13.27785" y1="10.02665" x2="14.02715" y2="10.03935" layer="21"/>
<rectangle x1="14.11605" y1="10.02665" x2="14.57325" y2="10.03935" layer="21"/>
<rectangle x1="14.70025" y1="10.02665" x2="15.58925" y2="10.03935" layer="21"/>
<rectangle x1="15.67815" y1="10.02665" x2="17.32915" y2="10.03935" layer="21"/>
<rectangle x1="4.76885" y1="10.03935" x2="5.75945" y2="10.05205" layer="21"/>
<rectangle x1="5.86105" y1="10.03935" x2="6.57225" y2="10.05205" layer="21"/>
<rectangle x1="6.67385" y1="10.03935" x2="7.42315" y2="10.05205" layer="21"/>
<rectangle x1="7.51205" y1="10.03935" x2="8.36295" y2="10.05205" layer="21"/>
<rectangle x1="8.48995" y1="10.03935" x2="8.78205" y2="10.05205" layer="21"/>
<rectangle x1="8.90905" y1="10.03935" x2="10.10285" y2="10.05205" layer="21"/>
<rectangle x1="10.21715" y1="10.03935" x2="10.43305" y2="10.05205" layer="21"/>
<rectangle x1="10.54735" y1="10.03935" x2="10.90295" y2="10.05205" layer="21"/>
<rectangle x1="11.01725" y1="10.03935" x2="11.23315" y2="10.05205" layer="21"/>
<rectangle x1="11.34745" y1="10.03935" x2="12.36345" y2="10.05205" layer="21"/>
<rectangle x1="12.46505" y1="10.03935" x2="13.17625" y2="10.05205" layer="21"/>
<rectangle x1="13.27785" y1="10.03935" x2="14.02715" y2="10.05205" layer="21"/>
<rectangle x1="14.11605" y1="10.03935" x2="14.56055" y2="10.05205" layer="21"/>
<rectangle x1="14.68755" y1="10.03935" x2="15.58925" y2="10.05205" layer="21"/>
<rectangle x1="15.67815" y1="10.03935" x2="17.32915" y2="10.05205" layer="21"/>
<rectangle x1="4.76885" y1="10.05205" x2="5.75945" y2="10.06475" layer="21"/>
<rectangle x1="5.87375" y1="10.05205" x2="6.55955" y2="10.06475" layer="21"/>
<rectangle x1="6.67385" y1="10.05205" x2="7.42315" y2="10.06475" layer="21"/>
<rectangle x1="7.51205" y1="10.05205" x2="8.37565" y2="10.06475" layer="21"/>
<rectangle x1="8.50265" y1="10.05205" x2="8.76935" y2="10.06475" layer="21"/>
<rectangle x1="8.89635" y1="10.05205" x2="10.09015" y2="10.06475" layer="21"/>
<rectangle x1="10.20445" y1="10.05205" x2="10.43305" y2="10.06475" layer="21"/>
<rectangle x1="10.54735" y1="10.05205" x2="10.89025" y2="10.06475" layer="21"/>
<rectangle x1="11.00455" y1="10.05205" x2="11.23315" y2="10.06475" layer="21"/>
<rectangle x1="11.34745" y1="10.05205" x2="12.36345" y2="10.06475" layer="21"/>
<rectangle x1="12.47775" y1="10.05205" x2="13.16355" y2="10.06475" layer="21"/>
<rectangle x1="13.27785" y1="10.05205" x2="14.02715" y2="10.06475" layer="21"/>
<rectangle x1="14.11605" y1="10.05205" x2="14.54785" y2="10.06475" layer="21"/>
<rectangle x1="14.67485" y1="10.05205" x2="15.58925" y2="10.06475" layer="21"/>
<rectangle x1="15.67815" y1="10.05205" x2="17.32915" y2="10.06475" layer="21"/>
<rectangle x1="4.76885" y1="10.06475" x2="5.77215" y2="10.07745" layer="21"/>
<rectangle x1="5.87375" y1="10.06475" x2="6.55955" y2="10.07745" layer="21"/>
<rectangle x1="6.66115" y1="10.06475" x2="7.42315" y2="10.07745" layer="21"/>
<rectangle x1="7.51205" y1="10.06475" x2="8.38835" y2="10.07745" layer="21"/>
<rectangle x1="8.51535" y1="10.06475" x2="8.76935" y2="10.07745" layer="21"/>
<rectangle x1="8.88365" y1="10.06475" x2="10.09015" y2="10.07745" layer="21"/>
<rectangle x1="10.20445" y1="10.06475" x2="10.44575" y2="10.07745" layer="21"/>
<rectangle x1="10.54735" y1="10.06475" x2="10.89025" y2="10.07745" layer="21"/>
<rectangle x1="11.00455" y1="10.06475" x2="11.24585" y2="10.07745" layer="21"/>
<rectangle x1="11.34745" y1="10.06475" x2="12.37615" y2="10.07745" layer="21"/>
<rectangle x1="12.47775" y1="10.06475" x2="13.16355" y2="10.07745" layer="21"/>
<rectangle x1="13.26515" y1="10.06475" x2="14.02715" y2="10.07745" layer="21"/>
<rectangle x1="14.11605" y1="10.06475" x2="14.53515" y2="10.07745" layer="21"/>
<rectangle x1="14.66215" y1="10.06475" x2="15.58925" y2="10.07745" layer="21"/>
<rectangle x1="15.67815" y1="10.06475" x2="17.32915" y2="10.07745" layer="21"/>
<rectangle x1="4.76885" y1="10.07745" x2="5.77215" y2="10.09015" layer="21"/>
<rectangle x1="6.66115" y1="10.07745" x2="7.42315" y2="10.09015" layer="21"/>
<rectangle x1="7.51205" y1="10.07745" x2="8.40105" y2="10.09015" layer="21"/>
<rectangle x1="8.52805" y1="10.07745" x2="8.75665" y2="10.09015" layer="21"/>
<rectangle x1="8.88365" y1="10.07745" x2="10.09015" y2="10.09015" layer="21"/>
<rectangle x1="10.19175" y1="10.07745" x2="10.44575" y2="10.09015" layer="21"/>
<rectangle x1="10.56005" y1="10.07745" x2="10.89025" y2="10.09015" layer="21"/>
<rectangle x1="10.99185" y1="10.07745" x2="11.24585" y2="10.09015" layer="21"/>
<rectangle x1="11.36015" y1="10.07745" x2="12.37615" y2="10.09015" layer="21"/>
<rectangle x1="13.26515" y1="10.07745" x2="14.02715" y2="10.09015" layer="21"/>
<rectangle x1="14.11605" y1="10.07745" x2="14.52245" y2="10.09015" layer="21"/>
<rectangle x1="14.66215" y1="10.07745" x2="15.58925" y2="10.09015" layer="21"/>
<rectangle x1="15.67815" y1="10.07745" x2="17.32915" y2="10.09015" layer="21"/>
<rectangle x1="4.76885" y1="10.09015" x2="5.78485" y2="10.10285" layer="21"/>
<rectangle x1="6.64845" y1="10.09015" x2="7.42315" y2="10.10285" layer="21"/>
<rectangle x1="7.51205" y1="10.09015" x2="8.40105" y2="10.10285" layer="21"/>
<rectangle x1="8.52805" y1="10.09015" x2="8.74395" y2="10.10285" layer="21"/>
<rectangle x1="8.87095" y1="10.09015" x2="10.07745" y2="10.10285" layer="21"/>
<rectangle x1="10.19175" y1="10.09015" x2="10.45845" y2="10.10285" layer="21"/>
<rectangle x1="10.56005" y1="10.09015" x2="10.87755" y2="10.10285" layer="21"/>
<rectangle x1="10.99185" y1="10.09015" x2="11.25855" y2="10.10285" layer="21"/>
<rectangle x1="11.36015" y1="10.09015" x2="12.38885" y2="10.10285" layer="21"/>
<rectangle x1="13.25245" y1="10.09015" x2="14.02715" y2="10.10285" layer="21"/>
<rectangle x1="14.11605" y1="10.09015" x2="14.50975" y2="10.10285" layer="21"/>
<rectangle x1="14.64945" y1="10.09015" x2="15.58925" y2="10.10285" layer="21"/>
<rectangle x1="15.67815" y1="10.09015" x2="17.32915" y2="10.10285" layer="21"/>
<rectangle x1="4.76885" y1="10.10285" x2="5.78485" y2="10.11555" layer="21"/>
<rectangle x1="6.64845" y1="10.10285" x2="7.42315" y2="10.11555" layer="21"/>
<rectangle x1="7.51205" y1="10.10285" x2="8.41375" y2="10.11555" layer="21"/>
<rectangle x1="8.54075" y1="10.10285" x2="8.73125" y2="10.11555" layer="21"/>
<rectangle x1="8.85825" y1="10.10285" x2="10.07745" y2="10.11555" layer="21"/>
<rectangle x1="10.17905" y1="10.10285" x2="10.45845" y2="10.11555" layer="21"/>
<rectangle x1="10.57275" y1="10.10285" x2="10.87755" y2="10.11555" layer="21"/>
<rectangle x1="10.97915" y1="10.10285" x2="11.25855" y2="10.11555" layer="21"/>
<rectangle x1="11.37285" y1="10.10285" x2="12.38885" y2="10.11555" layer="21"/>
<rectangle x1="13.25245" y1="10.10285" x2="14.02715" y2="10.11555" layer="21"/>
<rectangle x1="14.11605" y1="10.10285" x2="14.49705" y2="10.11555" layer="21"/>
<rectangle x1="14.63675" y1="10.10285" x2="15.58925" y2="10.11555" layer="21"/>
<rectangle x1="15.67815" y1="10.10285" x2="17.32915" y2="10.11555" layer="21"/>
<rectangle x1="4.76885" y1="10.11555" x2="5.79755" y2="10.12825" layer="21"/>
<rectangle x1="6.63575" y1="10.11555" x2="7.42315" y2="10.12825" layer="21"/>
<rectangle x1="7.51205" y1="10.11555" x2="8.42645" y2="10.12825" layer="21"/>
<rectangle x1="8.55345" y1="10.11555" x2="8.71855" y2="10.12825" layer="21"/>
<rectangle x1="8.84555" y1="10.11555" x2="10.06475" y2="10.12825" layer="21"/>
<rectangle x1="10.17905" y1="10.11555" x2="10.47115" y2="10.12825" layer="21"/>
<rectangle x1="10.57275" y1="10.11555" x2="10.86485" y2="10.12825" layer="21"/>
<rectangle x1="10.97915" y1="10.11555" x2="11.27125" y2="10.12825" layer="21"/>
<rectangle x1="11.37285" y1="10.11555" x2="12.40155" y2="10.12825" layer="21"/>
<rectangle x1="13.23975" y1="10.11555" x2="14.02715" y2="10.12825" layer="21"/>
<rectangle x1="14.11605" y1="10.11555" x2="14.49705" y2="10.12825" layer="21"/>
<rectangle x1="14.62405" y1="10.11555" x2="15.58925" y2="10.12825" layer="21"/>
<rectangle x1="15.67815" y1="10.11555" x2="17.32915" y2="10.12825" layer="21"/>
<rectangle x1="4.76885" y1="10.12825" x2="5.79755" y2="10.14095" layer="21"/>
<rectangle x1="6.63575" y1="10.12825" x2="7.42315" y2="10.14095" layer="21"/>
<rectangle x1="7.51205" y1="10.12825" x2="8.43915" y2="10.14095" layer="21"/>
<rectangle x1="8.56615" y1="10.12825" x2="8.71855" y2="10.14095" layer="21"/>
<rectangle x1="8.83285" y1="10.12825" x2="10.06475" y2="10.14095" layer="21"/>
<rectangle x1="10.16635" y1="10.12825" x2="10.47115" y2="10.14095" layer="21"/>
<rectangle x1="10.58545" y1="10.12825" x2="10.86485" y2="10.14095" layer="21"/>
<rectangle x1="10.96645" y1="10.12825" x2="11.27125" y2="10.14095" layer="21"/>
<rectangle x1="11.38555" y1="10.12825" x2="12.40155" y2="10.14095" layer="21"/>
<rectangle x1="13.23975" y1="10.12825" x2="14.02715" y2="10.14095" layer="21"/>
<rectangle x1="14.11605" y1="10.12825" x2="14.48435" y2="10.14095" layer="21"/>
<rectangle x1="14.61135" y1="10.12825" x2="15.58925" y2="10.14095" layer="21"/>
<rectangle x1="15.67815" y1="10.12825" x2="17.32915" y2="10.14095" layer="21"/>
<rectangle x1="4.76885" y1="10.14095" x2="5.79755" y2="10.15365" layer="21"/>
<rectangle x1="6.62305" y1="10.14095" x2="7.42315" y2="10.15365" layer="21"/>
<rectangle x1="7.51205" y1="10.14095" x2="8.45185" y2="10.15365" layer="21"/>
<rectangle x1="8.57885" y1="10.14095" x2="8.70585" y2="10.15365" layer="21"/>
<rectangle x1="8.83285" y1="10.14095" x2="10.05205" y2="10.15365" layer="21"/>
<rectangle x1="10.16635" y1="10.14095" x2="10.48385" y2="10.15365" layer="21"/>
<rectangle x1="10.58545" y1="10.14095" x2="10.85215" y2="10.15365" layer="21"/>
<rectangle x1="10.96645" y1="10.14095" x2="11.28395" y2="10.15365" layer="21"/>
<rectangle x1="11.38555" y1="10.14095" x2="12.40155" y2="10.15365" layer="21"/>
<rectangle x1="13.22705" y1="10.14095" x2="14.02715" y2="10.15365" layer="21"/>
<rectangle x1="14.11605" y1="10.14095" x2="14.47165" y2="10.15365" layer="21"/>
<rectangle x1="14.59865" y1="10.14095" x2="15.58925" y2="10.15365" layer="21"/>
<rectangle x1="15.67815" y1="10.14095" x2="17.32915" y2="10.15365" layer="21"/>
<rectangle x1="4.76885" y1="10.15365" x2="5.81025" y2="10.16635" layer="21"/>
<rectangle x1="6.62305" y1="10.15365" x2="7.42315" y2="10.16635" layer="21"/>
<rectangle x1="7.51205" y1="10.15365" x2="8.45185" y2="10.16635" layer="21"/>
<rectangle x1="8.57885" y1="10.15365" x2="8.69315" y2="10.16635" layer="21"/>
<rectangle x1="8.82015" y1="10.15365" x2="10.05205" y2="10.16635" layer="21"/>
<rectangle x1="10.15365" y1="10.15365" x2="10.48385" y2="10.16635" layer="21"/>
<rectangle x1="10.59815" y1="10.15365" x2="10.85215" y2="10.16635" layer="21"/>
<rectangle x1="10.95375" y1="10.15365" x2="11.28395" y2="10.16635" layer="21"/>
<rectangle x1="11.39825" y1="10.15365" x2="12.41425" y2="10.16635" layer="21"/>
<rectangle x1="13.22705" y1="10.15365" x2="14.02715" y2="10.16635" layer="21"/>
<rectangle x1="14.59865" y1="10.15365" x2="15.58925" y2="10.16635" layer="21"/>
<rectangle x1="15.67815" y1="10.15365" x2="17.32915" y2="10.16635" layer="21"/>
<rectangle x1="4.76885" y1="10.16635" x2="5.81025" y2="10.17905" layer="21"/>
<rectangle x1="5.92455" y1="10.16635" x2="6.50875" y2="10.17905" layer="21"/>
<rectangle x1="6.61035" y1="10.16635" x2="7.42315" y2="10.17905" layer="21"/>
<rectangle x1="7.51205" y1="10.16635" x2="8.46455" y2="10.17905" layer="21"/>
<rectangle x1="8.59155" y1="10.16635" x2="8.68045" y2="10.17905" layer="21"/>
<rectangle x1="8.80745" y1="10.16635" x2="10.03935" y2="10.17905" layer="21"/>
<rectangle x1="10.15365" y1="10.16635" x2="10.49655" y2="10.17905" layer="21"/>
<rectangle x1="10.59815" y1="10.16635" x2="10.83945" y2="10.17905" layer="21"/>
<rectangle x1="10.95375" y1="10.16635" x2="11.29665" y2="10.17905" layer="21"/>
<rectangle x1="11.39825" y1="10.16635" x2="12.41425" y2="10.17905" layer="21"/>
<rectangle x1="12.52855" y1="10.16635" x2="13.11275" y2="10.17905" layer="21"/>
<rectangle x1="13.21435" y1="10.16635" x2="14.02715" y2="10.17905" layer="21"/>
<rectangle x1="14.64945" y1="10.16635" x2="15.58925" y2="10.17905" layer="21"/>
<rectangle x1="15.67815" y1="10.16635" x2="17.32915" y2="10.17905" layer="21"/>
<rectangle x1="4.76885" y1="10.17905" x2="5.82295" y2="10.19175" layer="21"/>
<rectangle x1="5.92455" y1="10.17905" x2="6.50875" y2="10.19175" layer="21"/>
<rectangle x1="6.61035" y1="10.17905" x2="7.42315" y2="10.19175" layer="21"/>
<rectangle x1="7.51205" y1="10.17905" x2="8.47725" y2="10.19175" layer="21"/>
<rectangle x1="8.60425" y1="10.17905" x2="8.66775" y2="10.19175" layer="21"/>
<rectangle x1="8.79475" y1="10.17905" x2="10.03935" y2="10.19175" layer="21"/>
<rectangle x1="10.14095" y1="10.17905" x2="10.49655" y2="10.19175" layer="21"/>
<rectangle x1="10.61085" y1="10.17905" x2="10.83945" y2="10.19175" layer="21"/>
<rectangle x1="10.94105" y1="10.17905" x2="11.29665" y2="10.19175" layer="21"/>
<rectangle x1="11.41095" y1="10.17905" x2="12.42695" y2="10.19175" layer="21"/>
<rectangle x1="12.52855" y1="10.17905" x2="13.11275" y2="10.19175" layer="21"/>
<rectangle x1="13.21435" y1="10.17905" x2="14.02715" y2="10.19175" layer="21"/>
<rectangle x1="14.68755" y1="10.17905" x2="15.58925" y2="10.19175" layer="21"/>
<rectangle x1="15.67815" y1="10.17905" x2="17.32915" y2="10.19175" layer="21"/>
<rectangle x1="4.76885" y1="10.19175" x2="5.82295" y2="10.20445" layer="21"/>
<rectangle x1="5.93725" y1="10.19175" x2="6.49605" y2="10.20445" layer="21"/>
<rectangle x1="6.61035" y1="10.19175" x2="7.42315" y2="10.20445" layer="21"/>
<rectangle x1="7.51205" y1="10.19175" x2="8.48995" y2="10.20445" layer="21"/>
<rectangle x1="8.61695" y1="10.19175" x2="8.66775" y2="10.20445" layer="21"/>
<rectangle x1="8.79475" y1="10.19175" x2="10.02665" y2="10.20445" layer="21"/>
<rectangle x1="10.14095" y1="10.19175" x2="10.49655" y2="10.20445" layer="21"/>
<rectangle x1="10.61085" y1="10.19175" x2="10.82675" y2="10.20445" layer="21"/>
<rectangle x1="10.94105" y1="10.19175" x2="11.29665" y2="10.20445" layer="21"/>
<rectangle x1="11.41095" y1="10.19175" x2="12.42695" y2="10.20445" layer="21"/>
<rectangle x1="12.54125" y1="10.19175" x2="13.10005" y2="10.20445" layer="21"/>
<rectangle x1="13.21435" y1="10.19175" x2="14.02715" y2="10.20445" layer="21"/>
<rectangle x1="14.72565" y1="10.19175" x2="15.58925" y2="10.20445" layer="21"/>
<rectangle x1="15.67815" y1="10.19175" x2="17.32915" y2="10.20445" layer="21"/>
<rectangle x1="4.76885" y1="10.20445" x2="5.83565" y2="10.21715" layer="21"/>
<rectangle x1="5.93725" y1="10.20445" x2="6.49605" y2="10.21715" layer="21"/>
<rectangle x1="6.59765" y1="10.20445" x2="7.42315" y2="10.21715" layer="21"/>
<rectangle x1="7.51205" y1="10.20445" x2="8.50265" y2="10.21715" layer="21"/>
<rectangle x1="8.62965" y1="10.20445" x2="8.65505" y2="10.21715" layer="21"/>
<rectangle x1="8.78205" y1="10.20445" x2="10.02665" y2="10.21715" layer="21"/>
<rectangle x1="10.14095" y1="10.20445" x2="10.50925" y2="10.21715" layer="21"/>
<rectangle x1="10.62355" y1="10.20445" x2="10.82675" y2="10.21715" layer="21"/>
<rectangle x1="10.94105" y1="10.20445" x2="11.30935" y2="10.21715" layer="21"/>
<rectangle x1="11.42365" y1="10.20445" x2="12.43965" y2="10.21715" layer="21"/>
<rectangle x1="12.54125" y1="10.20445" x2="13.10005" y2="10.21715" layer="21"/>
<rectangle x1="13.20165" y1="10.20445" x2="14.02715" y2="10.21715" layer="21"/>
<rectangle x1="14.75105" y1="10.20445" x2="15.58925" y2="10.21715" layer="21"/>
<rectangle x1="15.67815" y1="10.20445" x2="17.32915" y2="10.21715" layer="21"/>
<rectangle x1="4.76885" y1="10.21715" x2="5.83565" y2="10.22985" layer="21"/>
<rectangle x1="5.94995" y1="10.21715" x2="6.48335" y2="10.22985" layer="21"/>
<rectangle x1="6.59765" y1="10.21715" x2="7.42315" y2="10.22985" layer="21"/>
<rectangle x1="7.51205" y1="10.21715" x2="8.50265" y2="10.22985" layer="21"/>
<rectangle x1="8.62965" y1="10.21715" x2="8.64235" y2="10.22985" layer="21"/>
<rectangle x1="8.76935" y1="10.21715" x2="10.01395" y2="10.22985" layer="21"/>
<rectangle x1="10.12825" y1="10.21715" x2="10.50925" y2="10.22985" layer="21"/>
<rectangle x1="10.62355" y1="10.21715" x2="10.81405" y2="10.22985" layer="21"/>
<rectangle x1="10.92835" y1="10.21715" x2="11.30935" y2="10.22985" layer="21"/>
<rectangle x1="11.42365" y1="10.21715" x2="12.43965" y2="10.22985" layer="21"/>
<rectangle x1="12.55395" y1="10.21715" x2="13.08735" y2="10.22985" layer="21"/>
<rectangle x1="13.20165" y1="10.21715" x2="14.02715" y2="10.22985" layer="21"/>
<rectangle x1="14.77645" y1="10.21715" x2="15.58925" y2="10.22985" layer="21"/>
<rectangle x1="15.67815" y1="10.21715" x2="17.32915" y2="10.22985" layer="21"/>
<rectangle x1="4.76885" y1="10.22985" x2="5.84835" y2="10.24255" layer="21"/>
<rectangle x1="5.94995" y1="10.22985" x2="6.48335" y2="10.24255" layer="21"/>
<rectangle x1="6.58495" y1="10.22985" x2="7.42315" y2="10.24255" layer="21"/>
<rectangle x1="7.51205" y1="10.22985" x2="8.51535" y2="10.24255" layer="21"/>
<rectangle x1="8.75665" y1="10.22985" x2="10.01395" y2="10.24255" layer="21"/>
<rectangle x1="10.12825" y1="10.22985" x2="10.52195" y2="10.24255" layer="21"/>
<rectangle x1="10.62355" y1="10.22985" x2="10.81405" y2="10.24255" layer="21"/>
<rectangle x1="10.92835" y1="10.22985" x2="11.32205" y2="10.24255" layer="21"/>
<rectangle x1="11.42365" y1="10.22985" x2="12.45235" y2="10.24255" layer="21"/>
<rectangle x1="12.55395" y1="10.22985" x2="13.08735" y2="10.24255" layer="21"/>
<rectangle x1="13.18895" y1="10.22985" x2="14.02715" y2="10.24255" layer="21"/>
<rectangle x1="14.78915" y1="10.22985" x2="15.58925" y2="10.24255" layer="21"/>
<rectangle x1="15.67815" y1="10.22985" x2="17.32915" y2="10.24255" layer="21"/>
<rectangle x1="4.76885" y1="10.24255" x2="5.84835" y2="10.25525" layer="21"/>
<rectangle x1="5.94995" y1="10.24255" x2="6.47065" y2="10.25525" layer="21"/>
<rectangle x1="6.58495" y1="10.24255" x2="7.42315" y2="10.25525" layer="21"/>
<rectangle x1="7.51205" y1="10.24255" x2="8.52805" y2="10.25525" layer="21"/>
<rectangle x1="8.74395" y1="10.24255" x2="10.01395" y2="10.25525" layer="21"/>
<rectangle x1="10.11555" y1="10.24255" x2="10.52195" y2="10.25525" layer="21"/>
<rectangle x1="10.63625" y1="10.24255" x2="10.81405" y2="10.25525" layer="21"/>
<rectangle x1="10.91565" y1="10.24255" x2="11.32205" y2="10.25525" layer="21"/>
<rectangle x1="11.43635" y1="10.24255" x2="12.45235" y2="10.25525" layer="21"/>
<rectangle x1="12.55395" y1="10.24255" x2="13.07465" y2="10.25525" layer="21"/>
<rectangle x1="13.18895" y1="10.24255" x2="14.02715" y2="10.25525" layer="21"/>
<rectangle x1="14.11605" y1="10.24255" x2="14.56055" y2="10.25525" layer="21"/>
<rectangle x1="14.81455" y1="10.24255" x2="15.58925" y2="10.25525" layer="21"/>
<rectangle x1="15.67815" y1="10.24255" x2="17.32915" y2="10.25525" layer="21"/>
<rectangle x1="4.76885" y1="10.25525" x2="5.86105" y2="10.26795" layer="21"/>
<rectangle x1="5.96265" y1="10.25525" x2="6.47065" y2="10.26795" layer="21"/>
<rectangle x1="6.57225" y1="10.25525" x2="7.42315" y2="10.26795" layer="21"/>
<rectangle x1="7.51205" y1="10.25525" x2="8.54075" y2="10.26795" layer="21"/>
<rectangle x1="8.74395" y1="10.25525" x2="10.00125" y2="10.26795" layer="21"/>
<rectangle x1="10.11555" y1="10.25525" x2="10.53465" y2="10.26795" layer="21"/>
<rectangle x1="10.63625" y1="10.25525" x2="10.80135" y2="10.26795" layer="21"/>
<rectangle x1="10.91565" y1="10.25525" x2="11.33475" y2="10.26795" layer="21"/>
<rectangle x1="11.43635" y1="10.25525" x2="12.46505" y2="10.26795" layer="21"/>
<rectangle x1="12.56665" y1="10.25525" x2="13.07465" y2="10.26795" layer="21"/>
<rectangle x1="13.17625" y1="10.25525" x2="14.02715" y2="10.26795" layer="21"/>
<rectangle x1="14.11605" y1="10.25525" x2="14.62405" y2="10.26795" layer="21"/>
<rectangle x1="14.82725" y1="10.25525" x2="15.58925" y2="10.26795" layer="21"/>
<rectangle x1="15.67815" y1="10.25525" x2="17.32915" y2="10.26795" layer="21"/>
<rectangle x1="4.76885" y1="10.26795" x2="5.86105" y2="10.28065" layer="21"/>
<rectangle x1="5.96265" y1="10.26795" x2="6.45795" y2="10.28065" layer="21"/>
<rectangle x1="6.57225" y1="10.26795" x2="7.42315" y2="10.28065" layer="21"/>
<rectangle x1="7.51205" y1="10.26795" x2="8.54075" y2="10.28065" layer="21"/>
<rectangle x1="8.73125" y1="10.26795" x2="10.00125" y2="10.28065" layer="21"/>
<rectangle x1="10.10285" y1="10.26795" x2="10.53465" y2="10.28065" layer="21"/>
<rectangle x1="10.64895" y1="10.26795" x2="10.80135" y2="10.28065" layer="21"/>
<rectangle x1="10.90295" y1="10.26795" x2="11.33475" y2="10.28065" layer="21"/>
<rectangle x1="11.44905" y1="10.26795" x2="12.46505" y2="10.28065" layer="21"/>
<rectangle x1="12.56665" y1="10.26795" x2="13.06195" y2="10.28065" layer="21"/>
<rectangle x1="13.17625" y1="10.26795" x2="14.02715" y2="10.28065" layer="21"/>
<rectangle x1="14.11605" y1="10.26795" x2="14.66215" y2="10.28065" layer="21"/>
<rectangle x1="14.83995" y1="10.26795" x2="15.58925" y2="10.28065" layer="21"/>
<rectangle x1="16.37665" y1="10.26795" x2="17.32915" y2="10.28065" layer="21"/>
<rectangle x1="4.76885" y1="10.28065" x2="5.87375" y2="10.29335" layer="21"/>
<rectangle x1="5.97535" y1="10.28065" x2="6.45795" y2="10.29335" layer="21"/>
<rectangle x1="6.55955" y1="10.28065" x2="7.42315" y2="10.29335" layer="21"/>
<rectangle x1="7.51205" y1="10.28065" x2="8.55345" y2="10.29335" layer="21"/>
<rectangle x1="8.71855" y1="10.28065" x2="9.98855" y2="10.29335" layer="21"/>
<rectangle x1="10.10285" y1="10.28065" x2="10.54735" y2="10.29335" layer="21"/>
<rectangle x1="10.64895" y1="10.28065" x2="10.78865" y2="10.29335" layer="21"/>
<rectangle x1="10.90295" y1="10.28065" x2="11.34745" y2="10.29335" layer="21"/>
<rectangle x1="11.44905" y1="10.28065" x2="12.47775" y2="10.29335" layer="21"/>
<rectangle x1="12.57935" y1="10.28065" x2="13.06195" y2="10.29335" layer="21"/>
<rectangle x1="13.16355" y1="10.28065" x2="14.02715" y2="10.29335" layer="21"/>
<rectangle x1="14.11605" y1="10.28065" x2="14.68755" y2="10.29335" layer="21"/>
<rectangle x1="14.85265" y1="10.28065" x2="15.58925" y2="10.29335" layer="21"/>
<rectangle x1="16.37665" y1="10.28065" x2="17.32915" y2="10.29335" layer="21"/>
<rectangle x1="4.76885" y1="10.29335" x2="5.87375" y2="10.30605" layer="21"/>
<rectangle x1="5.97535" y1="10.29335" x2="6.44525" y2="10.30605" layer="21"/>
<rectangle x1="6.55955" y1="10.29335" x2="7.42315" y2="10.30605" layer="21"/>
<rectangle x1="7.51205" y1="10.29335" x2="8.56615" y2="10.30605" layer="21"/>
<rectangle x1="8.70585" y1="10.29335" x2="9.98855" y2="10.30605" layer="21"/>
<rectangle x1="10.09015" y1="10.29335" x2="10.54735" y2="10.30605" layer="21"/>
<rectangle x1="10.66165" y1="10.29335" x2="10.78865" y2="10.30605" layer="21"/>
<rectangle x1="10.89025" y1="10.29335" x2="11.34745" y2="10.30605" layer="21"/>
<rectangle x1="11.46175" y1="10.29335" x2="12.47775" y2="10.30605" layer="21"/>
<rectangle x1="12.57935" y1="10.29335" x2="13.04925" y2="10.30605" layer="21"/>
<rectangle x1="13.16355" y1="10.29335" x2="14.02715" y2="10.30605" layer="21"/>
<rectangle x1="14.11605" y1="10.29335" x2="14.71295" y2="10.30605" layer="21"/>
<rectangle x1="14.86535" y1="10.29335" x2="15.58925" y2="10.30605" layer="21"/>
<rectangle x1="16.37665" y1="10.29335" x2="17.32915" y2="10.30605" layer="21"/>
<rectangle x1="4.76885" y1="10.30605" x2="5.87375" y2="10.31875" layer="21"/>
<rectangle x1="5.98805" y1="10.30605" x2="6.44525" y2="10.31875" layer="21"/>
<rectangle x1="6.54685" y1="10.30605" x2="7.42315" y2="10.31875" layer="21"/>
<rectangle x1="7.51205" y1="10.30605" x2="8.56615" y2="10.31875" layer="21"/>
<rectangle x1="8.70585" y1="10.30605" x2="9.97585" y2="10.31875" layer="21"/>
<rectangle x1="10.09015" y1="10.30605" x2="10.56005" y2="10.31875" layer="21"/>
<rectangle x1="10.66165" y1="10.30605" x2="10.77595" y2="10.31875" layer="21"/>
<rectangle x1="10.89025" y1="10.30605" x2="11.36015" y2="10.31875" layer="21"/>
<rectangle x1="11.46175" y1="10.30605" x2="12.47775" y2="10.31875" layer="21"/>
<rectangle x1="12.59205" y1="10.30605" x2="13.04925" y2="10.31875" layer="21"/>
<rectangle x1="13.15085" y1="10.30605" x2="14.02715" y2="10.31875" layer="21"/>
<rectangle x1="14.11605" y1="10.30605" x2="14.73835" y2="10.31875" layer="21"/>
<rectangle x1="14.87805" y1="10.30605" x2="15.58925" y2="10.31875" layer="21"/>
<rectangle x1="16.37665" y1="10.30605" x2="17.32915" y2="10.31875" layer="21"/>
<rectangle x1="4.76885" y1="10.31875" x2="5.88645" y2="10.33145" layer="21"/>
<rectangle x1="5.98805" y1="10.31875" x2="6.44525" y2="10.33145" layer="21"/>
<rectangle x1="6.54685" y1="10.31875" x2="7.42315" y2="10.33145" layer="21"/>
<rectangle x1="7.51205" y1="10.31875" x2="8.55345" y2="10.33145" layer="21"/>
<rectangle x1="8.71855" y1="10.31875" x2="9.97585" y2="10.33145" layer="21"/>
<rectangle x1="10.07745" y1="10.31875" x2="10.56005" y2="10.33145" layer="21"/>
<rectangle x1="10.67435" y1="10.31875" x2="10.77595" y2="10.33145" layer="21"/>
<rectangle x1="10.87755" y1="10.31875" x2="11.36015" y2="10.33145" layer="21"/>
<rectangle x1="11.47445" y1="10.31875" x2="12.49045" y2="10.33145" layer="21"/>
<rectangle x1="12.59205" y1="10.31875" x2="13.04925" y2="10.33145" layer="21"/>
<rectangle x1="13.15085" y1="10.31875" x2="14.02715" y2="10.33145" layer="21"/>
<rectangle x1="14.11605" y1="10.31875" x2="14.75105" y2="10.33145" layer="21"/>
<rectangle x1="14.89075" y1="10.31875" x2="15.58925" y2="10.33145" layer="21"/>
<rectangle x1="16.37665" y1="10.31875" x2="17.32915" y2="10.33145" layer="21"/>
<rectangle x1="4.76885" y1="10.33145" x2="5.88645" y2="10.34415" layer="21"/>
<rectangle x1="6.00075" y1="10.33145" x2="6.43255" y2="10.34415" layer="21"/>
<rectangle x1="6.53415" y1="10.33145" x2="7.42315" y2="10.34415" layer="21"/>
<rectangle x1="7.51205" y1="10.33145" x2="8.55345" y2="10.34415" layer="21"/>
<rectangle x1="8.73125" y1="10.33145" x2="9.96315" y2="10.34415" layer="21"/>
<rectangle x1="10.07745" y1="10.33145" x2="10.57275" y2="10.34415" layer="21"/>
<rectangle x1="10.67435" y1="10.33145" x2="10.76325" y2="10.34415" layer="21"/>
<rectangle x1="10.87755" y1="10.33145" x2="11.37285" y2="10.34415" layer="21"/>
<rectangle x1="11.47445" y1="10.33145" x2="12.49045" y2="10.34415" layer="21"/>
<rectangle x1="12.60475" y1="10.33145" x2="13.03655" y2="10.34415" layer="21"/>
<rectangle x1="13.13815" y1="10.33145" x2="14.02715" y2="10.34415" layer="21"/>
<rectangle x1="14.11605" y1="10.33145" x2="14.76375" y2="10.34415" layer="21"/>
<rectangle x1="14.89075" y1="10.33145" x2="15.58925" y2="10.34415" layer="21"/>
<rectangle x1="16.37665" y1="10.33145" x2="17.32915" y2="10.34415" layer="21"/>
<rectangle x1="4.76885" y1="10.34415" x2="5.89915" y2="10.35685" layer="21"/>
<rectangle x1="6.00075" y1="10.34415" x2="6.43255" y2="10.35685" layer="21"/>
<rectangle x1="6.53415" y1="10.34415" x2="7.42315" y2="10.35685" layer="21"/>
<rectangle x1="7.51205" y1="10.34415" x2="8.54075" y2="10.35685" layer="21"/>
<rectangle x1="8.74395" y1="10.34415" x2="9.96315" y2="10.35685" layer="21"/>
<rectangle x1="10.06475" y1="10.34415" x2="10.57275" y2="10.35685" layer="21"/>
<rectangle x1="10.68705" y1="10.34415" x2="10.76325" y2="10.35685" layer="21"/>
<rectangle x1="10.86485" y1="10.34415" x2="11.37285" y2="10.35685" layer="21"/>
<rectangle x1="11.48715" y1="10.34415" x2="12.50315" y2="10.35685" layer="21"/>
<rectangle x1="12.60475" y1="10.34415" x2="13.03655" y2="10.35685" layer="21"/>
<rectangle x1="13.13815" y1="10.34415" x2="14.02715" y2="10.35685" layer="21"/>
<rectangle x1="14.11605" y1="10.34415" x2="14.77645" y2="10.35685" layer="21"/>
<rectangle x1="14.90345" y1="10.34415" x2="15.58925" y2="10.35685" layer="21"/>
<rectangle x1="16.36395" y1="10.34415" x2="17.32915" y2="10.35685" layer="21"/>
<rectangle x1="4.76885" y1="10.35685" x2="5.89915" y2="10.36955" layer="21"/>
<rectangle x1="6.01345" y1="10.35685" x2="6.41985" y2="10.36955" layer="21"/>
<rectangle x1="6.53415" y1="10.35685" x2="7.42315" y2="10.36955" layer="21"/>
<rectangle x1="7.51205" y1="10.35685" x2="8.52805" y2="10.36955" layer="21"/>
<rectangle x1="8.74395" y1="10.35685" x2="9.95045" y2="10.36955" layer="21"/>
<rectangle x1="10.06475" y1="10.35685" x2="10.57275" y2="10.36955" layer="21"/>
<rectangle x1="10.68705" y1="10.35685" x2="10.75055" y2="10.36955" layer="21"/>
<rectangle x1="10.86485" y1="10.35685" x2="11.37285" y2="10.36955" layer="21"/>
<rectangle x1="11.48715" y1="10.35685" x2="12.50315" y2="10.36955" layer="21"/>
<rectangle x1="12.61745" y1="10.35685" x2="13.02385" y2="10.36955" layer="21"/>
<rectangle x1="13.13815" y1="10.35685" x2="14.02715" y2="10.36955" layer="21"/>
<rectangle x1="14.11605" y1="10.35685" x2="14.78915" y2="10.36955" layer="21"/>
<rectangle x1="14.90345" y1="10.35685" x2="15.58925" y2="10.36955" layer="21"/>
<rectangle x1="15.67815" y1="10.35685" x2="17.32915" y2="10.36955" layer="21"/>
<rectangle x1="4.76885" y1="10.36955" x2="5.91185" y2="10.38225" layer="21"/>
<rectangle x1="6.01345" y1="10.36955" x2="6.41985" y2="10.38225" layer="21"/>
<rectangle x1="6.52145" y1="10.36955" x2="7.42315" y2="10.38225" layer="21"/>
<rectangle x1="7.51205" y1="10.36955" x2="8.51535" y2="10.38225" layer="21"/>
<rectangle x1="8.75665" y1="10.36955" x2="9.95045" y2="10.38225" layer="21"/>
<rectangle x1="10.06475" y1="10.36955" x2="10.58545" y2="10.38225" layer="21"/>
<rectangle x1="10.69975" y1="10.36955" x2="10.75055" y2="10.38225" layer="21"/>
<rectangle x1="10.86485" y1="10.36955" x2="11.38555" y2="10.38225" layer="21"/>
<rectangle x1="11.49985" y1="10.36955" x2="12.51585" y2="10.38225" layer="21"/>
<rectangle x1="12.61745" y1="10.36955" x2="13.02385" y2="10.38225" layer="21"/>
<rectangle x1="13.12545" y1="10.36955" x2="14.02715" y2="10.38225" layer="21"/>
<rectangle x1="14.11605" y1="10.36955" x2="14.80185" y2="10.38225" layer="21"/>
<rectangle x1="14.91615" y1="10.36955" x2="15.58925" y2="10.38225" layer="21"/>
<rectangle x1="15.67815" y1="10.36955" x2="17.32915" y2="10.38225" layer="21"/>
<rectangle x1="4.76885" y1="10.38225" x2="5.91185" y2="10.39495" layer="21"/>
<rectangle x1="6.01345" y1="10.38225" x2="6.40715" y2="10.39495" layer="21"/>
<rectangle x1="6.52145" y1="10.38225" x2="7.42315" y2="10.39495" layer="21"/>
<rectangle x1="7.51205" y1="10.38225" x2="8.51535" y2="10.39495" layer="21"/>
<rectangle x1="8.76935" y1="10.38225" x2="9.93775" y2="10.39495" layer="21"/>
<rectangle x1="10.05205" y1="10.38225" x2="10.58545" y2="10.39495" layer="21"/>
<rectangle x1="10.69975" y1="10.38225" x2="10.73785" y2="10.39495" layer="21"/>
<rectangle x1="10.85215" y1="10.38225" x2="11.38555" y2="10.39495" layer="21"/>
<rectangle x1="11.49985" y1="10.38225" x2="12.51585" y2="10.39495" layer="21"/>
<rectangle x1="12.61745" y1="10.38225" x2="13.01115" y2="10.39495" layer="21"/>
<rectangle x1="13.12545" y1="10.38225" x2="14.02715" y2="10.39495" layer="21"/>
<rectangle x1="14.11605" y1="10.38225" x2="14.81455" y2="10.39495" layer="21"/>
<rectangle x1="14.91615" y1="10.38225" x2="15.58925" y2="10.39495" layer="21"/>
<rectangle x1="15.67815" y1="10.38225" x2="17.32915" y2="10.39495" layer="21"/>
<rectangle x1="4.76885" y1="10.39495" x2="5.92455" y2="10.40765" layer="21"/>
<rectangle x1="6.02615" y1="10.39495" x2="6.40715" y2="10.40765" layer="21"/>
<rectangle x1="6.50875" y1="10.39495" x2="7.42315" y2="10.40765" layer="21"/>
<rectangle x1="7.51205" y1="10.39495" x2="8.50265" y2="10.40765" layer="21"/>
<rectangle x1="8.62965" y1="10.39495" x2="8.65505" y2="10.40765" layer="21"/>
<rectangle x1="8.78205" y1="10.39495" x2="9.93775" y2="10.40765" layer="21"/>
<rectangle x1="10.05205" y1="10.39495" x2="10.59815" y2="10.40765" layer="21"/>
<rectangle x1="10.69975" y1="10.39495" x2="10.73785" y2="10.40765" layer="21"/>
<rectangle x1="10.85215" y1="10.39495" x2="11.39825" y2="10.40765" layer="21"/>
<rectangle x1="11.49985" y1="10.39495" x2="12.52855" y2="10.40765" layer="21"/>
<rectangle x1="12.63015" y1="10.39495" x2="13.01115" y2="10.40765" layer="21"/>
<rectangle x1="13.11275" y1="10.39495" x2="14.02715" y2="10.40765" layer="21"/>
<rectangle x1="14.11605" y1="10.39495" x2="14.81455" y2="10.40765" layer="21"/>
<rectangle x1="14.92885" y1="10.39495" x2="15.58925" y2="10.40765" layer="21"/>
<rectangle x1="15.67815" y1="10.39495" x2="17.32915" y2="10.40765" layer="21"/>
<rectangle x1="4.76885" y1="10.40765" x2="5.92455" y2="10.42035" layer="21"/>
<rectangle x1="6.02615" y1="10.40765" x2="6.39445" y2="10.42035" layer="21"/>
<rectangle x1="6.50875" y1="10.40765" x2="7.42315" y2="10.42035" layer="21"/>
<rectangle x1="7.51205" y1="10.40765" x2="8.48995" y2="10.42035" layer="21"/>
<rectangle x1="8.61695" y1="10.40765" x2="8.65505" y2="10.42035" layer="21"/>
<rectangle x1="8.78205" y1="10.40765" x2="9.93775" y2="10.42035" layer="21"/>
<rectangle x1="10.03935" y1="10.40765" x2="10.59815" y2="10.42035" layer="21"/>
<rectangle x1="10.71245" y1="10.40765" x2="10.73785" y2="10.42035" layer="21"/>
<rectangle x1="10.83945" y1="10.40765" x2="11.39825" y2="10.42035" layer="21"/>
<rectangle x1="11.51255" y1="10.40765" x2="12.52855" y2="10.42035" layer="21"/>
<rectangle x1="12.63015" y1="10.40765" x2="12.99845" y2="10.42035" layer="21"/>
<rectangle x1="13.11275" y1="10.40765" x2="14.02715" y2="10.42035" layer="21"/>
<rectangle x1="14.11605" y1="10.40765" x2="14.82725" y2="10.42035" layer="21"/>
<rectangle x1="14.92885" y1="10.40765" x2="15.58925" y2="10.42035" layer="21"/>
<rectangle x1="15.67815" y1="10.40765" x2="17.32915" y2="10.42035" layer="21"/>
<rectangle x1="4.76885" y1="10.42035" x2="5.93725" y2="10.43305" layer="21"/>
<rectangle x1="6.03885" y1="10.42035" x2="6.39445" y2="10.43305" layer="21"/>
<rectangle x1="6.49605" y1="10.42035" x2="7.42315" y2="10.43305" layer="21"/>
<rectangle x1="7.51205" y1="10.42035" x2="8.47725" y2="10.43305" layer="21"/>
<rectangle x1="8.60425" y1="10.42035" x2="8.66775" y2="10.43305" layer="21"/>
<rectangle x1="8.79475" y1="10.42035" x2="9.92505" y2="10.43305" layer="21"/>
<rectangle x1="10.03935" y1="10.42035" x2="10.61085" y2="10.43305" layer="21"/>
<rectangle x1="10.71245" y1="10.42035" x2="10.72515" y2="10.43305" layer="21"/>
<rectangle x1="10.83945" y1="10.42035" x2="11.41095" y2="10.43305" layer="21"/>
<rectangle x1="11.51255" y1="10.42035" x2="12.54125" y2="10.43305" layer="21"/>
<rectangle x1="12.64285" y1="10.42035" x2="12.99845" y2="10.43305" layer="21"/>
<rectangle x1="13.10005" y1="10.42035" x2="14.02715" y2="10.43305" layer="21"/>
<rectangle x1="14.11605" y1="10.42035" x2="14.82725" y2="10.43305" layer="21"/>
<rectangle x1="14.92885" y1="10.42035" x2="15.58925" y2="10.43305" layer="21"/>
<rectangle x1="15.67815" y1="10.42035" x2="17.32915" y2="10.43305" layer="21"/>
<rectangle x1="4.76885" y1="10.43305" x2="5.93725" y2="10.44575" layer="21"/>
<rectangle x1="6.03885" y1="10.43305" x2="6.38175" y2="10.44575" layer="21"/>
<rectangle x1="6.49605" y1="10.43305" x2="7.42315" y2="10.44575" layer="21"/>
<rectangle x1="7.51205" y1="10.43305" x2="8.47725" y2="10.44575" layer="21"/>
<rectangle x1="8.59155" y1="10.43305" x2="8.68045" y2="10.44575" layer="21"/>
<rectangle x1="8.80745" y1="10.43305" x2="9.92505" y2="10.44575" layer="21"/>
<rectangle x1="10.02665" y1="10.43305" x2="10.61085" y2="10.44575" layer="21"/>
<rectangle x1="10.82675" y1="10.43305" x2="11.41095" y2="10.44575" layer="21"/>
<rectangle x1="11.52525" y1="10.43305" x2="12.54125" y2="10.44575" layer="21"/>
<rectangle x1="12.64285" y1="10.43305" x2="12.98575" y2="10.44575" layer="21"/>
<rectangle x1="13.10005" y1="10.43305" x2="14.02715" y2="10.44575" layer="21"/>
<rectangle x1="14.11605" y1="10.43305" x2="14.83995" y2="10.44575" layer="21"/>
<rectangle x1="14.94155" y1="10.43305" x2="15.58925" y2="10.44575" layer="21"/>
<rectangle x1="15.67815" y1="10.43305" x2="17.32915" y2="10.44575" layer="21"/>
<rectangle x1="4.76885" y1="10.44575" x2="5.94995" y2="10.45845" layer="21"/>
<rectangle x1="6.05155" y1="10.44575" x2="6.38175" y2="10.45845" layer="21"/>
<rectangle x1="6.48335" y1="10.44575" x2="7.42315" y2="10.45845" layer="21"/>
<rectangle x1="7.51205" y1="10.44575" x2="8.46455" y2="10.45845" layer="21"/>
<rectangle x1="8.59155" y1="10.44575" x2="8.69315" y2="10.45845" layer="21"/>
<rectangle x1="8.82015" y1="10.44575" x2="9.91235" y2="10.45845" layer="21"/>
<rectangle x1="10.02665" y1="10.44575" x2="10.62355" y2="10.45845" layer="21"/>
<rectangle x1="10.82675" y1="10.44575" x2="11.42365" y2="10.45845" layer="21"/>
<rectangle x1="11.52525" y1="10.44575" x2="12.55395" y2="10.45845" layer="21"/>
<rectangle x1="12.65555" y1="10.44575" x2="12.98575" y2="10.45845" layer="21"/>
<rectangle x1="13.08735" y1="10.44575" x2="14.02715" y2="10.45845" layer="21"/>
<rectangle x1="14.11605" y1="10.44575" x2="14.83995" y2="10.45845" layer="21"/>
<rectangle x1="14.94155" y1="10.44575" x2="15.58925" y2="10.45845" layer="21"/>
<rectangle x1="15.67815" y1="10.44575" x2="17.32915" y2="10.45845" layer="21"/>
<rectangle x1="4.76885" y1="10.45845" x2="5.94995" y2="10.47115" layer="21"/>
<rectangle x1="6.05155" y1="10.45845" x2="6.38175" y2="10.47115" layer="21"/>
<rectangle x1="6.48335" y1="10.45845" x2="7.42315" y2="10.47115" layer="21"/>
<rectangle x1="7.51205" y1="10.45845" x2="8.45185" y2="10.47115" layer="21"/>
<rectangle x1="8.57885" y1="10.45845" x2="8.69315" y2="10.47115" layer="21"/>
<rectangle x1="8.82015" y1="10.45845" x2="9.91235" y2="10.47115" layer="21"/>
<rectangle x1="10.01395" y1="10.45845" x2="10.62355" y2="10.47115" layer="21"/>
<rectangle x1="10.81405" y1="10.45845" x2="11.42365" y2="10.47115" layer="21"/>
<rectangle x1="11.53795" y1="10.45845" x2="12.55395" y2="10.47115" layer="21"/>
<rectangle x1="12.65555" y1="10.45845" x2="12.98575" y2="10.47115" layer="21"/>
<rectangle x1="13.08735" y1="10.45845" x2="14.02715" y2="10.47115" layer="21"/>
<rectangle x1="14.11605" y1="10.45845" x2="14.83995" y2="10.47115" layer="21"/>
<rectangle x1="14.94155" y1="10.45845" x2="15.58925" y2="10.47115" layer="21"/>
<rectangle x1="15.67815" y1="10.45845" x2="17.32915" y2="10.47115" layer="21"/>
<rectangle x1="4.76885" y1="10.47115" x2="5.94995" y2="10.48385" layer="21"/>
<rectangle x1="6.06425" y1="10.47115" x2="6.36905" y2="10.48385" layer="21"/>
<rectangle x1="6.47065" y1="10.47115" x2="7.42315" y2="10.48385" layer="21"/>
<rectangle x1="7.51205" y1="10.47115" x2="8.43915" y2="10.48385" layer="21"/>
<rectangle x1="8.56615" y1="10.47115" x2="8.70585" y2="10.48385" layer="21"/>
<rectangle x1="8.83285" y1="10.47115" x2="9.89965" y2="10.48385" layer="21"/>
<rectangle x1="10.01395" y1="10.47115" x2="10.63625" y2="10.48385" layer="21"/>
<rectangle x1="10.81405" y1="10.47115" x2="11.43635" y2="10.48385" layer="21"/>
<rectangle x1="11.53795" y1="10.47115" x2="12.55395" y2="10.48385" layer="21"/>
<rectangle x1="12.66825" y1="10.47115" x2="12.97305" y2="10.48385" layer="21"/>
<rectangle x1="13.07465" y1="10.47115" x2="14.02715" y2="10.48385" layer="21"/>
<rectangle x1="14.11605" y1="10.47115" x2="14.83995" y2="10.48385" layer="21"/>
<rectangle x1="14.94155" y1="10.47115" x2="15.58925" y2="10.48385" layer="21"/>
<rectangle x1="15.67815" y1="10.47115" x2="17.32915" y2="10.48385" layer="21"/>
<rectangle x1="4.76885" y1="10.48385" x2="5.96265" y2="10.49655" layer="21"/>
<rectangle x1="6.06425" y1="10.48385" x2="6.36905" y2="10.49655" layer="21"/>
<rectangle x1="6.47065" y1="10.48385" x2="7.42315" y2="10.49655" layer="21"/>
<rectangle x1="7.51205" y1="10.48385" x2="8.42645" y2="10.49655" layer="21"/>
<rectangle x1="8.56615" y1="10.48385" x2="8.71855" y2="10.49655" layer="21"/>
<rectangle x1="8.84555" y1="10.48385" x2="9.89965" y2="10.49655" layer="21"/>
<rectangle x1="10.00125" y1="10.48385" x2="10.63625" y2="10.49655" layer="21"/>
<rectangle x1="10.80135" y1="10.48385" x2="11.43635" y2="10.49655" layer="21"/>
<rectangle x1="11.55065" y1="10.48385" x2="12.56665" y2="10.49655" layer="21"/>
<rectangle x1="12.66825" y1="10.48385" x2="12.97305" y2="10.49655" layer="21"/>
<rectangle x1="13.07465" y1="10.48385" x2="14.02715" y2="10.49655" layer="21"/>
<rectangle x1="14.11605" y1="10.48385" x2="14.85265" y2="10.49655" layer="21"/>
<rectangle x1="14.95425" y1="10.48385" x2="15.58925" y2="10.49655" layer="21"/>
<rectangle x1="15.67815" y1="10.48385" x2="17.32915" y2="10.49655" layer="21"/>
<rectangle x1="4.76885" y1="10.49655" x2="5.96265" y2="10.50925" layer="21"/>
<rectangle x1="6.07695" y1="10.49655" x2="6.35635" y2="10.50925" layer="21"/>
<rectangle x1="6.45795" y1="10.49655" x2="7.42315" y2="10.50925" layer="21"/>
<rectangle x1="7.51205" y1="10.49655" x2="8.42645" y2="10.50925" layer="21"/>
<rectangle x1="8.55345" y1="10.49655" x2="8.73125" y2="10.50925" layer="21"/>
<rectangle x1="8.85825" y1="10.49655" x2="9.88695" y2="10.50925" layer="21"/>
<rectangle x1="10.00125" y1="10.49655" x2="10.64895" y2="10.50925" layer="21"/>
<rectangle x1="10.80135" y1="10.49655" x2="11.44905" y2="10.50925" layer="21"/>
<rectangle x1="11.55065" y1="10.49655" x2="12.56665" y2="10.50925" layer="21"/>
<rectangle x1="12.68095" y1="10.49655" x2="12.96035" y2="10.50925" layer="21"/>
<rectangle x1="13.06195" y1="10.49655" x2="14.02715" y2="10.50925" layer="21"/>
<rectangle x1="14.11605" y1="10.49655" x2="14.85265" y2="10.50925" layer="21"/>
<rectangle x1="14.95425" y1="10.49655" x2="15.58925" y2="10.50925" layer="21"/>
<rectangle x1="15.67815" y1="10.49655" x2="17.32915" y2="10.50925" layer="21"/>
<rectangle x1="4.76885" y1="10.50925" x2="5.97535" y2="10.52195" layer="21"/>
<rectangle x1="6.07695" y1="10.50925" x2="6.35635" y2="10.52195" layer="21"/>
<rectangle x1="6.45795" y1="10.50925" x2="7.42315" y2="10.52195" layer="21"/>
<rectangle x1="7.51205" y1="10.50925" x2="8.41375" y2="10.52195" layer="21"/>
<rectangle x1="8.54075" y1="10.50925" x2="8.73125" y2="10.52195" layer="21"/>
<rectangle x1="8.85825" y1="10.50925" x2="9.88695" y2="10.52195" layer="21"/>
<rectangle x1="10.00125" y1="10.50925" x2="10.64895" y2="10.52195" layer="21"/>
<rectangle x1="10.80135" y1="10.50925" x2="11.44905" y2="10.52195" layer="21"/>
<rectangle x1="11.56335" y1="10.50925" x2="12.57935" y2="10.52195" layer="21"/>
<rectangle x1="12.68095" y1="10.50925" x2="12.96035" y2="10.52195" layer="21"/>
<rectangle x1="13.06195" y1="10.50925" x2="14.02715" y2="10.52195" layer="21"/>
<rectangle x1="14.11605" y1="10.50925" x2="14.85265" y2="10.52195" layer="21"/>
<rectangle x1="14.95425" y1="10.50925" x2="15.58925" y2="10.52195" layer="21"/>
<rectangle x1="15.67815" y1="10.50925" x2="17.32915" y2="10.52195" layer="21"/>
<rectangle x1="4.76885" y1="10.52195" x2="5.97535" y2="10.53465" layer="21"/>
<rectangle x1="6.07695" y1="10.52195" x2="6.34365" y2="10.53465" layer="21"/>
<rectangle x1="6.45795" y1="10.52195" x2="7.42315" y2="10.53465" layer="21"/>
<rectangle x1="7.51205" y1="10.52195" x2="8.40105" y2="10.53465" layer="21"/>
<rectangle x1="8.52805" y1="10.52195" x2="8.74395" y2="10.53465" layer="21"/>
<rectangle x1="8.87095" y1="10.52195" x2="9.87425" y2="10.53465" layer="21"/>
<rectangle x1="9.98855" y1="10.52195" x2="10.64895" y2="10.53465" layer="21"/>
<rectangle x1="10.78865" y1="10.52195" x2="11.44905" y2="10.53465" layer="21"/>
<rectangle x1="11.56335" y1="10.52195" x2="12.57935" y2="10.53465" layer="21"/>
<rectangle x1="12.68095" y1="10.52195" x2="12.94765" y2="10.53465" layer="21"/>
<rectangle x1="13.06195" y1="10.52195" x2="14.02715" y2="10.53465" layer="21"/>
<rectangle x1="14.11605" y1="10.52195" x2="14.85265" y2="10.53465" layer="21"/>
<rectangle x1="14.95425" y1="10.52195" x2="15.58925" y2="10.53465" layer="21"/>
<rectangle x1="15.67815" y1="10.52195" x2="17.32915" y2="10.53465" layer="21"/>
<rectangle x1="4.76885" y1="10.53465" x2="5.98805" y2="10.54735" layer="21"/>
<rectangle x1="6.08965" y1="10.53465" x2="6.34365" y2="10.54735" layer="21"/>
<rectangle x1="6.44525" y1="10.53465" x2="7.42315" y2="10.54735" layer="21"/>
<rectangle x1="7.51205" y1="10.53465" x2="8.38835" y2="10.54735" layer="21"/>
<rectangle x1="8.51535" y1="10.53465" x2="8.75665" y2="10.54735" layer="21"/>
<rectangle x1="8.88365" y1="10.53465" x2="9.87425" y2="10.54735" layer="21"/>
<rectangle x1="9.98855" y1="10.53465" x2="10.66165" y2="10.54735" layer="21"/>
<rectangle x1="10.78865" y1="10.53465" x2="11.46175" y2="10.54735" layer="21"/>
<rectangle x1="11.57605" y1="10.53465" x2="12.59205" y2="10.54735" layer="21"/>
<rectangle x1="12.69365" y1="10.53465" x2="12.94765" y2="10.54735" layer="21"/>
<rectangle x1="13.04925" y1="10.53465" x2="14.02715" y2="10.54735" layer="21"/>
<rectangle x1="14.11605" y1="10.53465" x2="14.85265" y2="10.54735" layer="21"/>
<rectangle x1="14.95425" y1="10.53465" x2="15.58925" y2="10.54735" layer="21"/>
<rectangle x1="15.67815" y1="10.53465" x2="17.32915" y2="10.54735" layer="21"/>
<rectangle x1="4.76885" y1="10.54735" x2="5.98805" y2="10.56005" layer="21"/>
<rectangle x1="6.08965" y1="10.54735" x2="6.33095" y2="10.56005" layer="21"/>
<rectangle x1="6.44525" y1="10.54735" x2="7.42315" y2="10.56005" layer="21"/>
<rectangle x1="7.51205" y1="10.54735" x2="8.38835" y2="10.56005" layer="21"/>
<rectangle x1="8.51535" y1="10.54735" x2="8.76935" y2="10.56005" layer="21"/>
<rectangle x1="8.89635" y1="10.54735" x2="9.86155" y2="10.56005" layer="21"/>
<rectangle x1="9.97585" y1="10.54735" x2="10.66165" y2="10.56005" layer="21"/>
<rectangle x1="10.77595" y1="10.54735" x2="11.46175" y2="10.56005" layer="21"/>
<rectangle x1="11.57605" y1="10.54735" x2="12.59205" y2="10.56005" layer="21"/>
<rectangle x1="12.69365" y1="10.54735" x2="12.93495" y2="10.56005" layer="21"/>
<rectangle x1="13.04925" y1="10.54735" x2="14.02715" y2="10.56005" layer="21"/>
<rectangle x1="14.11605" y1="10.54735" x2="14.85265" y2="10.56005" layer="21"/>
<rectangle x1="14.95425" y1="10.54735" x2="15.58925" y2="10.56005" layer="21"/>
<rectangle x1="15.67815" y1="10.54735" x2="17.32915" y2="10.56005" layer="21"/>
<rectangle x1="4.76885" y1="10.56005" x2="6.00075" y2="10.57275" layer="21"/>
<rectangle x1="6.10235" y1="10.56005" x2="6.33095" y2="10.57275" layer="21"/>
<rectangle x1="6.43255" y1="10.56005" x2="7.42315" y2="10.57275" layer="21"/>
<rectangle x1="7.51205" y1="10.56005" x2="8.37565" y2="10.57275" layer="21"/>
<rectangle x1="8.50265" y1="10.56005" x2="8.76935" y2="10.57275" layer="21"/>
<rectangle x1="8.89635" y1="10.56005" x2="9.86155" y2="10.57275" layer="21"/>
<rectangle x1="9.97585" y1="10.56005" x2="10.66165" y2="10.57275" layer="21"/>
<rectangle x1="10.77595" y1="10.56005" x2="11.47445" y2="10.57275" layer="21"/>
<rectangle x1="11.57605" y1="10.56005" x2="12.60475" y2="10.57275" layer="21"/>
<rectangle x1="12.70635" y1="10.56005" x2="12.93495" y2="10.57275" layer="21"/>
<rectangle x1="13.03655" y1="10.56005" x2="14.02715" y2="10.57275" layer="21"/>
<rectangle x1="14.11605" y1="10.56005" x2="14.85265" y2="10.57275" layer="21"/>
<rectangle x1="14.95425" y1="10.56005" x2="15.58925" y2="10.57275" layer="21"/>
<rectangle x1="15.67815" y1="10.56005" x2="17.32915" y2="10.57275" layer="21"/>
<rectangle x1="4.76885" y1="10.57275" x2="6.00075" y2="10.58545" layer="21"/>
<rectangle x1="6.10235" y1="10.57275" x2="6.31825" y2="10.58545" layer="21"/>
<rectangle x1="6.43255" y1="10.57275" x2="7.42315" y2="10.58545" layer="21"/>
<rectangle x1="7.51205" y1="10.57275" x2="8.36295" y2="10.58545" layer="21"/>
<rectangle x1="8.48995" y1="10.57275" x2="8.78205" y2="10.58545" layer="21"/>
<rectangle x1="8.90905" y1="10.57275" x2="9.86155" y2="10.58545" layer="21"/>
<rectangle x1="9.96315" y1="10.57275" x2="10.66165" y2="10.58545" layer="21"/>
<rectangle x1="10.78865" y1="10.57275" x2="11.47445" y2="10.58545" layer="21"/>
<rectangle x1="11.58875" y1="10.57275" x2="12.60475" y2="10.58545" layer="21"/>
<rectangle x1="12.70635" y1="10.57275" x2="12.92225" y2="10.58545" layer="21"/>
<rectangle x1="13.03655" y1="10.57275" x2="14.02715" y2="10.58545" layer="21"/>
<rectangle x1="14.11605" y1="10.57275" x2="14.85265" y2="10.58545" layer="21"/>
<rectangle x1="14.95425" y1="10.57275" x2="15.58925" y2="10.58545" layer="21"/>
<rectangle x1="15.67815" y1="10.57275" x2="17.32915" y2="10.58545" layer="21"/>
<rectangle x1="4.76885" y1="10.58545" x2="6.01345" y2="10.59815" layer="21"/>
<rectangle x1="6.11505" y1="10.58545" x2="6.31825" y2="10.59815" layer="21"/>
<rectangle x1="6.41985" y1="10.58545" x2="7.42315" y2="10.59815" layer="21"/>
<rectangle x1="7.51205" y1="10.58545" x2="8.36295" y2="10.59815" layer="21"/>
<rectangle x1="8.47725" y1="10.58545" x2="8.79475" y2="10.59815" layer="21"/>
<rectangle x1="8.92175" y1="10.58545" x2="9.84885" y2="10.59815" layer="21"/>
<rectangle x1="9.96315" y1="10.58545" x2="10.64895" y2="10.59815" layer="21"/>
<rectangle x1="10.78865" y1="10.58545" x2="11.48715" y2="10.59815" layer="21"/>
<rectangle x1="11.58875" y1="10.58545" x2="12.61745" y2="10.59815" layer="21"/>
<rectangle x1="12.71905" y1="10.58545" x2="12.92225" y2="10.59815" layer="21"/>
<rectangle x1="13.02385" y1="10.58545" x2="14.02715" y2="10.59815" layer="21"/>
<rectangle x1="14.11605" y1="10.58545" x2="14.85265" y2="10.59815" layer="21"/>
<rectangle x1="14.95425" y1="10.58545" x2="15.58925" y2="10.59815" layer="21"/>
<rectangle x1="15.67815" y1="10.58545" x2="17.32915" y2="10.59815" layer="21"/>
<rectangle x1="4.76885" y1="10.59815" x2="6.01345" y2="10.61085" layer="21"/>
<rectangle x1="6.11505" y1="10.59815" x2="6.30555" y2="10.61085" layer="21"/>
<rectangle x1="6.41985" y1="10.59815" x2="7.42315" y2="10.61085" layer="21"/>
<rectangle x1="7.51205" y1="10.59815" x2="8.35025" y2="10.61085" layer="21"/>
<rectangle x1="8.47725" y1="10.59815" x2="8.80745" y2="10.61085" layer="21"/>
<rectangle x1="8.93445" y1="10.59815" x2="9.84885" y2="10.61085" layer="21"/>
<rectangle x1="9.95045" y1="10.59815" x2="10.64895" y2="10.61085" layer="21"/>
<rectangle x1="10.80135" y1="10.59815" x2="11.48715" y2="10.61085" layer="21"/>
<rectangle x1="11.60145" y1="10.59815" x2="12.61745" y2="10.61085" layer="21"/>
<rectangle x1="12.71905" y1="10.59815" x2="12.92225" y2="10.61085" layer="21"/>
<rectangle x1="13.02385" y1="10.59815" x2="14.02715" y2="10.61085" layer="21"/>
<rectangle x1="14.11605" y1="10.59815" x2="14.83995" y2="10.61085" layer="21"/>
<rectangle x1="14.94155" y1="10.59815" x2="15.58925" y2="10.61085" layer="21"/>
<rectangle x1="15.67815" y1="10.59815" x2="17.32915" y2="10.61085" layer="21"/>
<rectangle x1="4.76885" y1="10.61085" x2="6.02615" y2="10.62355" layer="21"/>
<rectangle x1="6.12775" y1="10.61085" x2="6.30555" y2="10.62355" layer="21"/>
<rectangle x1="6.40715" y1="10.61085" x2="7.42315" y2="10.62355" layer="21"/>
<rectangle x1="7.51205" y1="10.61085" x2="8.33755" y2="10.62355" layer="21"/>
<rectangle x1="8.46455" y1="10.61085" x2="8.80745" y2="10.62355" layer="21"/>
<rectangle x1="8.93445" y1="10.61085" x2="9.83615" y2="10.62355" layer="21"/>
<rectangle x1="9.95045" y1="10.61085" x2="10.63625" y2="10.62355" layer="21"/>
<rectangle x1="10.80135" y1="10.61085" x2="11.49985" y2="10.62355" layer="21"/>
<rectangle x1="11.60145" y1="10.61085" x2="12.61745" y2="10.62355" layer="21"/>
<rectangle x1="12.73175" y1="10.61085" x2="12.90955" y2="10.62355" layer="21"/>
<rectangle x1="13.01115" y1="10.61085" x2="14.02715" y2="10.62355" layer="21"/>
<rectangle x1="14.11605" y1="10.61085" x2="14.83995" y2="10.62355" layer="21"/>
<rectangle x1="14.94155" y1="10.61085" x2="15.58925" y2="10.62355" layer="21"/>
<rectangle x1="15.67815" y1="10.61085" x2="17.32915" y2="10.62355" layer="21"/>
<rectangle x1="4.76885" y1="10.62355" x2="6.02615" y2="10.63625" layer="21"/>
<rectangle x1="6.12775" y1="10.62355" x2="6.30555" y2="10.63625" layer="21"/>
<rectangle x1="6.40715" y1="10.62355" x2="7.42315" y2="10.63625" layer="21"/>
<rectangle x1="7.51205" y1="10.62355" x2="8.32485" y2="10.63625" layer="21"/>
<rectangle x1="8.45185" y1="10.62355" x2="8.82015" y2="10.63625" layer="21"/>
<rectangle x1="8.94715" y1="10.62355" x2="9.83615" y2="10.63625" layer="21"/>
<rectangle x1="9.93775" y1="10.62355" x2="10.63625" y2="10.63625" layer="21"/>
<rectangle x1="10.81405" y1="10.62355" x2="11.49985" y2="10.63625" layer="21"/>
<rectangle x1="11.61415" y1="10.62355" x2="12.63015" y2="10.63625" layer="21"/>
<rectangle x1="12.73175" y1="10.62355" x2="12.90955" y2="10.63625" layer="21"/>
<rectangle x1="13.01115" y1="10.62355" x2="14.02715" y2="10.63625" layer="21"/>
<rectangle x1="14.11605" y1="10.62355" x2="14.83995" y2="10.63625" layer="21"/>
<rectangle x1="14.94155" y1="10.62355" x2="15.58925" y2="10.63625" layer="21"/>
<rectangle x1="15.67815" y1="10.62355" x2="17.32915" y2="10.63625" layer="21"/>
<rectangle x1="4.76885" y1="10.63625" x2="6.02615" y2="10.64895" layer="21"/>
<rectangle x1="6.14045" y1="10.63625" x2="6.29285" y2="10.64895" layer="21"/>
<rectangle x1="6.39445" y1="10.63625" x2="7.42315" y2="10.64895" layer="21"/>
<rectangle x1="7.51205" y1="10.63625" x2="8.32485" y2="10.64895" layer="21"/>
<rectangle x1="8.43915" y1="10.63625" x2="8.83285" y2="10.64895" layer="21"/>
<rectangle x1="8.95985" y1="10.63625" x2="9.82345" y2="10.64895" layer="21"/>
<rectangle x1="9.93775" y1="10.63625" x2="10.62355" y2="10.64895" layer="21"/>
<rectangle x1="10.81405" y1="10.63625" x2="11.51255" y2="10.64895" layer="21"/>
<rectangle x1="11.61415" y1="10.63625" x2="12.63015" y2="10.64895" layer="21"/>
<rectangle x1="12.74445" y1="10.63625" x2="12.89685" y2="10.64895" layer="21"/>
<rectangle x1="12.99845" y1="10.63625" x2="14.02715" y2="10.64895" layer="21"/>
<rectangle x1="14.11605" y1="10.63625" x2="14.83995" y2="10.64895" layer="21"/>
<rectangle x1="14.94155" y1="10.63625" x2="15.58925" y2="10.64895" layer="21"/>
<rectangle x1="15.67815" y1="10.63625" x2="17.32915" y2="10.64895" layer="21"/>
<rectangle x1="4.76885" y1="10.64895" x2="6.03885" y2="10.66165" layer="21"/>
<rectangle x1="6.14045" y1="10.64895" x2="6.29285" y2="10.66165" layer="21"/>
<rectangle x1="6.39445" y1="10.64895" x2="7.42315" y2="10.66165" layer="21"/>
<rectangle x1="7.51205" y1="10.64895" x2="8.31215" y2="10.66165" layer="21"/>
<rectangle x1="8.43915" y1="10.64895" x2="8.84555" y2="10.66165" layer="21"/>
<rectangle x1="8.97255" y1="10.64895" x2="9.82345" y2="10.66165" layer="21"/>
<rectangle x1="9.92505" y1="10.64895" x2="10.62355" y2="10.66165" layer="21"/>
<rectangle x1="10.82675" y1="10.64895" x2="11.51255" y2="10.66165" layer="21"/>
<rectangle x1="11.62685" y1="10.64895" x2="12.64285" y2="10.66165" layer="21"/>
<rectangle x1="12.74445" y1="10.64895" x2="12.89685" y2="10.66165" layer="21"/>
<rectangle x1="12.99845" y1="10.64895" x2="14.02715" y2="10.66165" layer="21"/>
<rectangle x1="14.11605" y1="10.64895" x2="14.82725" y2="10.66165" layer="21"/>
<rectangle x1="14.94155" y1="10.64895" x2="15.58925" y2="10.66165" layer="21"/>
<rectangle x1="15.67815" y1="10.64895" x2="17.32915" y2="10.66165" layer="21"/>
<rectangle x1="4.76885" y1="10.66165" x2="6.03885" y2="10.67435" layer="21"/>
<rectangle x1="6.15315" y1="10.66165" x2="6.28015" y2="10.67435" layer="21"/>
<rectangle x1="6.39445" y1="10.66165" x2="7.42315" y2="10.67435" layer="21"/>
<rectangle x1="7.51205" y1="10.66165" x2="8.29945" y2="10.67435" layer="21"/>
<rectangle x1="8.42645" y1="10.66165" x2="8.84555" y2="10.67435" layer="21"/>
<rectangle x1="8.97255" y1="10.66165" x2="9.81075" y2="10.67435" layer="21"/>
<rectangle x1="9.92505" y1="10.66165" x2="10.61085" y2="10.67435" layer="21"/>
<rectangle x1="10.82675" y1="10.66165" x2="11.52525" y2="10.67435" layer="21"/>
<rectangle x1="11.62685" y1="10.66165" x2="12.64285" y2="10.67435" layer="21"/>
<rectangle x1="12.75715" y1="10.66165" x2="12.88415" y2="10.67435" layer="21"/>
<rectangle x1="12.99845" y1="10.66165" x2="14.02715" y2="10.67435" layer="21"/>
<rectangle x1="14.11605" y1="10.66165" x2="14.82725" y2="10.67435" layer="21"/>
<rectangle x1="14.92885" y1="10.66165" x2="15.58925" y2="10.67435" layer="21"/>
<rectangle x1="15.67815" y1="10.66165" x2="17.32915" y2="10.67435" layer="21"/>
<rectangle x1="4.76885" y1="10.67435" x2="6.05155" y2="10.68705" layer="21"/>
<rectangle x1="6.15315" y1="10.67435" x2="6.28015" y2="10.68705" layer="21"/>
<rectangle x1="6.38175" y1="10.67435" x2="7.42315" y2="10.68705" layer="21"/>
<rectangle x1="7.51205" y1="10.67435" x2="8.28675" y2="10.68705" layer="21"/>
<rectangle x1="8.41375" y1="10.67435" x2="8.85825" y2="10.68705" layer="21"/>
<rectangle x1="8.98525" y1="10.67435" x2="9.81075" y2="10.68705" layer="21"/>
<rectangle x1="9.92505" y1="10.67435" x2="10.61085" y2="10.68705" layer="21"/>
<rectangle x1="10.83945" y1="10.67435" x2="11.52525" y2="10.68705" layer="21"/>
<rectangle x1="11.63955" y1="10.67435" x2="12.65555" y2="10.68705" layer="21"/>
<rectangle x1="12.75715" y1="10.67435" x2="12.88415" y2="10.68705" layer="21"/>
<rectangle x1="12.98575" y1="10.67435" x2="14.02715" y2="10.68705" layer="21"/>
<rectangle x1="14.11605" y1="10.67435" x2="14.81455" y2="10.68705" layer="21"/>
<rectangle x1="14.92885" y1="10.67435" x2="15.58925" y2="10.68705" layer="21"/>
<rectangle x1="15.67815" y1="10.67435" x2="17.32915" y2="10.68705" layer="21"/>
<rectangle x1="4.76885" y1="10.68705" x2="6.05155" y2="10.69975" layer="21"/>
<rectangle x1="6.15315" y1="10.68705" x2="6.26745" y2="10.69975" layer="21"/>
<rectangle x1="6.38175" y1="10.68705" x2="7.42315" y2="10.69975" layer="21"/>
<rectangle x1="7.51205" y1="10.68705" x2="8.27405" y2="10.69975" layer="21"/>
<rectangle x1="8.40105" y1="10.68705" x2="8.87095" y2="10.69975" layer="21"/>
<rectangle x1="8.99795" y1="10.68705" x2="9.79805" y2="10.69975" layer="21"/>
<rectangle x1="9.91235" y1="10.68705" x2="10.59815" y2="10.69975" layer="21"/>
<rectangle x1="10.71245" y1="10.68705" x2="10.72515" y2="10.69975" layer="21"/>
<rectangle x1="10.83945" y1="10.68705" x2="11.52525" y2="10.69975" layer="21"/>
<rectangle x1="11.63955" y1="10.68705" x2="12.65555" y2="10.69975" layer="21"/>
<rectangle x1="12.75715" y1="10.68705" x2="12.87145" y2="10.69975" layer="21"/>
<rectangle x1="12.98575" y1="10.68705" x2="14.02715" y2="10.69975" layer="21"/>
<rectangle x1="14.11605" y1="10.68705" x2="14.81455" y2="10.69975" layer="21"/>
<rectangle x1="14.92885" y1="10.68705" x2="15.58925" y2="10.69975" layer="21"/>
<rectangle x1="15.67815" y1="10.68705" x2="17.32915" y2="10.69975" layer="21"/>
<rectangle x1="4.76885" y1="10.69975" x2="6.06425" y2="10.71245" layer="21"/>
<rectangle x1="6.16585" y1="10.69975" x2="6.26745" y2="10.71245" layer="21"/>
<rectangle x1="6.36905" y1="10.69975" x2="7.42315" y2="10.71245" layer="21"/>
<rectangle x1="7.51205" y1="10.69975" x2="8.27405" y2="10.71245" layer="21"/>
<rectangle x1="8.40105" y1="10.69975" x2="8.88365" y2="10.71245" layer="21"/>
<rectangle x1="9.01065" y1="10.69975" x2="9.79805" y2="10.71245" layer="21"/>
<rectangle x1="9.91235" y1="10.69975" x2="10.59815" y2="10.71245" layer="21"/>
<rectangle x1="10.71245" y1="10.69975" x2="10.73785" y2="10.71245" layer="21"/>
<rectangle x1="10.85215" y1="10.69975" x2="11.53795" y2="10.71245" layer="21"/>
<rectangle x1="11.65225" y1="10.69975" x2="12.66825" y2="10.71245" layer="21"/>
<rectangle x1="12.76985" y1="10.69975" x2="12.87145" y2="10.71245" layer="21"/>
<rectangle x1="12.97305" y1="10.69975" x2="14.02715" y2="10.71245" layer="21"/>
<rectangle x1="14.11605" y1="10.69975" x2="14.80185" y2="10.71245" layer="21"/>
<rectangle x1="14.91615" y1="10.69975" x2="15.58925" y2="10.71245" layer="21"/>
<rectangle x1="15.67815" y1="10.69975" x2="17.32915" y2="10.71245" layer="21"/>
<rectangle x1="4.76885" y1="10.71245" x2="6.06425" y2="10.72515" layer="21"/>
<rectangle x1="6.16585" y1="10.71245" x2="6.25475" y2="10.72515" layer="21"/>
<rectangle x1="6.36905" y1="10.71245" x2="7.42315" y2="10.72515" layer="21"/>
<rectangle x1="7.51205" y1="10.71245" x2="8.26135" y2="10.72515" layer="21"/>
<rectangle x1="8.38835" y1="10.71245" x2="8.88365" y2="10.72515" layer="21"/>
<rectangle x1="9.01065" y1="10.71245" x2="9.78535" y2="10.72515" layer="21"/>
<rectangle x1="9.89965" y1="10.71245" x2="10.58545" y2="10.72515" layer="21"/>
<rectangle x1="10.69975" y1="10.71245" x2="10.73785" y2="10.72515" layer="21"/>
<rectangle x1="10.85215" y1="10.71245" x2="11.53795" y2="10.72515" layer="21"/>
<rectangle x1="11.65225" y1="10.71245" x2="12.66825" y2="10.72515" layer="21"/>
<rectangle x1="12.76985" y1="10.71245" x2="12.85875" y2="10.72515" layer="21"/>
<rectangle x1="12.97305" y1="10.71245" x2="14.02715" y2="10.72515" layer="21"/>
<rectangle x1="14.11605" y1="10.71245" x2="14.80185" y2="10.72515" layer="21"/>
<rectangle x1="14.91615" y1="10.71245" x2="15.58925" y2="10.72515" layer="21"/>
<rectangle x1="15.67815" y1="10.71245" x2="17.32915" y2="10.72515" layer="21"/>
<rectangle x1="4.76885" y1="10.72515" x2="6.07695" y2="10.73785" layer="21"/>
<rectangle x1="6.17855" y1="10.72515" x2="6.25475" y2="10.73785" layer="21"/>
<rectangle x1="6.35635" y1="10.72515" x2="7.42315" y2="10.73785" layer="21"/>
<rectangle x1="7.51205" y1="10.72515" x2="8.24865" y2="10.73785" layer="21"/>
<rectangle x1="8.37565" y1="10.72515" x2="8.89635" y2="10.73785" layer="21"/>
<rectangle x1="9.02335" y1="10.72515" x2="9.78535" y2="10.73785" layer="21"/>
<rectangle x1="9.89965" y1="10.72515" x2="10.58545" y2="10.73785" layer="21"/>
<rectangle x1="10.69975" y1="10.72515" x2="10.75055" y2="10.73785" layer="21"/>
<rectangle x1="10.85215" y1="10.72515" x2="11.55065" y2="10.73785" layer="21"/>
<rectangle x1="11.65225" y1="10.72515" x2="12.68095" y2="10.73785" layer="21"/>
<rectangle x1="12.78255" y1="10.72515" x2="12.85875" y2="10.73785" layer="21"/>
<rectangle x1="12.96035" y1="10.72515" x2="14.02715" y2="10.73785" layer="21"/>
<rectangle x1="14.11605" y1="10.72515" x2="14.78915" y2="10.73785" layer="21"/>
<rectangle x1="14.90345" y1="10.72515" x2="15.58925" y2="10.73785" layer="21"/>
<rectangle x1="15.67815" y1="10.72515" x2="17.32915" y2="10.73785" layer="21"/>
<rectangle x1="4.76885" y1="10.73785" x2="6.07695" y2="10.75055" layer="21"/>
<rectangle x1="6.17855" y1="10.73785" x2="6.24205" y2="10.75055" layer="21"/>
<rectangle x1="6.35635" y1="10.73785" x2="7.42315" y2="10.75055" layer="21"/>
<rectangle x1="7.51205" y1="10.73785" x2="8.24865" y2="10.75055" layer="21"/>
<rectangle x1="8.36295" y1="10.73785" x2="8.90905" y2="10.75055" layer="21"/>
<rectangle x1="9.03605" y1="10.73785" x2="9.78535" y2="10.75055" layer="21"/>
<rectangle x1="9.88695" y1="10.73785" x2="10.58545" y2="10.75055" layer="21"/>
<rectangle x1="10.68705" y1="10.73785" x2="10.75055" y2="10.75055" layer="21"/>
<rectangle x1="10.86485" y1="10.73785" x2="11.55065" y2="10.75055" layer="21"/>
<rectangle x1="11.66495" y1="10.73785" x2="12.68095" y2="10.75055" layer="21"/>
<rectangle x1="12.78255" y1="10.73785" x2="12.84605" y2="10.75055" layer="21"/>
<rectangle x1="12.96035" y1="10.73785" x2="14.02715" y2="10.75055" layer="21"/>
<rectangle x1="14.11605" y1="10.73785" x2="14.77645" y2="10.75055" layer="21"/>
<rectangle x1="14.90345" y1="10.73785" x2="15.58925" y2="10.75055" layer="21"/>
<rectangle x1="15.67815" y1="10.73785" x2="17.32915" y2="10.75055" layer="21"/>
<rectangle x1="4.76885" y1="10.75055" x2="6.08965" y2="10.76325" layer="21"/>
<rectangle x1="6.19125" y1="10.75055" x2="6.24205" y2="10.76325" layer="21"/>
<rectangle x1="6.34365" y1="10.75055" x2="7.42315" y2="10.76325" layer="21"/>
<rectangle x1="7.51205" y1="10.75055" x2="8.23595" y2="10.76325" layer="21"/>
<rectangle x1="8.36295" y1="10.75055" x2="8.92175" y2="10.76325" layer="21"/>
<rectangle x1="9.04875" y1="10.75055" x2="9.77265" y2="10.76325" layer="21"/>
<rectangle x1="9.88695" y1="10.75055" x2="10.57275" y2="10.76325" layer="21"/>
<rectangle x1="10.68705" y1="10.75055" x2="10.76325" y2="10.76325" layer="21"/>
<rectangle x1="10.86485" y1="10.75055" x2="11.56335" y2="10.76325" layer="21"/>
<rectangle x1="11.66495" y1="10.75055" x2="12.69365" y2="10.76325" layer="21"/>
<rectangle x1="12.79525" y1="10.75055" x2="12.84605" y2="10.76325" layer="21"/>
<rectangle x1="12.94765" y1="10.75055" x2="14.02715" y2="10.76325" layer="21"/>
<rectangle x1="14.11605" y1="10.75055" x2="14.76375" y2="10.76325" layer="21"/>
<rectangle x1="14.89075" y1="10.75055" x2="15.58925" y2="10.76325" layer="21"/>
<rectangle x1="15.67815" y1="10.75055" x2="17.32915" y2="10.76325" layer="21"/>
<rectangle x1="4.76885" y1="10.76325" x2="6.08965" y2="10.77595" layer="21"/>
<rectangle x1="6.19125" y1="10.76325" x2="6.24205" y2="10.77595" layer="21"/>
<rectangle x1="6.34365" y1="10.76325" x2="7.42315" y2="10.77595" layer="21"/>
<rectangle x1="7.51205" y1="10.76325" x2="8.22325" y2="10.77595" layer="21"/>
<rectangle x1="8.35025" y1="10.76325" x2="8.92175" y2="10.77595" layer="21"/>
<rectangle x1="9.04875" y1="10.76325" x2="9.77265" y2="10.77595" layer="21"/>
<rectangle x1="9.87425" y1="10.76325" x2="10.57275" y2="10.77595" layer="21"/>
<rectangle x1="10.67435" y1="10.76325" x2="10.76325" y2="10.77595" layer="21"/>
<rectangle x1="10.87755" y1="10.76325" x2="11.56335" y2="10.77595" layer="21"/>
<rectangle x1="11.67765" y1="10.76325" x2="12.69365" y2="10.77595" layer="21"/>
<rectangle x1="12.79525" y1="10.76325" x2="12.84605" y2="10.77595" layer="21"/>
<rectangle x1="12.94765" y1="10.76325" x2="14.02715" y2="10.77595" layer="21"/>
<rectangle x1="14.11605" y1="10.76325" x2="14.73835" y2="10.77595" layer="21"/>
<rectangle x1="14.87805" y1="10.76325" x2="15.58925" y2="10.77595" layer="21"/>
<rectangle x1="15.67815" y1="10.76325" x2="17.32915" y2="10.77595" layer="21"/>
<rectangle x1="4.76885" y1="10.77595" x2="6.10235" y2="10.78865" layer="21"/>
<rectangle x1="6.20395" y1="10.77595" x2="6.22935" y2="10.78865" layer="21"/>
<rectangle x1="6.33095" y1="10.77595" x2="7.42315" y2="10.78865" layer="21"/>
<rectangle x1="7.51205" y1="10.77595" x2="8.21055" y2="10.78865" layer="21"/>
<rectangle x1="8.33755" y1="10.77595" x2="8.93445" y2="10.78865" layer="21"/>
<rectangle x1="9.06145" y1="10.77595" x2="9.75995" y2="10.78865" layer="21"/>
<rectangle x1="9.87425" y1="10.77595" x2="10.56005" y2="10.78865" layer="21"/>
<rectangle x1="10.67435" y1="10.77595" x2="10.77595" y2="10.78865" layer="21"/>
<rectangle x1="10.87755" y1="10.77595" x2="11.57605" y2="10.78865" layer="21"/>
<rectangle x1="11.67765" y1="10.77595" x2="12.70635" y2="10.78865" layer="21"/>
<rectangle x1="12.80795" y1="10.77595" x2="12.83335" y2="10.78865" layer="21"/>
<rectangle x1="12.93495" y1="10.77595" x2="14.02715" y2="10.78865" layer="21"/>
<rectangle x1="14.11605" y1="10.77595" x2="14.72565" y2="10.78865" layer="21"/>
<rectangle x1="14.87805" y1="10.77595" x2="15.58925" y2="10.78865" layer="21"/>
<rectangle x1="15.67815" y1="10.77595" x2="17.32915" y2="10.78865" layer="21"/>
<rectangle x1="4.76885" y1="10.78865" x2="6.10235" y2="10.80135" layer="21"/>
<rectangle x1="6.20395" y1="10.78865" x2="6.22935" y2="10.80135" layer="21"/>
<rectangle x1="6.33095" y1="10.78865" x2="7.42315" y2="10.80135" layer="21"/>
<rectangle x1="7.51205" y1="10.78865" x2="8.21055" y2="10.80135" layer="21"/>
<rectangle x1="8.32485" y1="10.78865" x2="8.94715" y2="10.80135" layer="21"/>
<rectangle x1="9.07415" y1="10.78865" x2="9.75995" y2="10.80135" layer="21"/>
<rectangle x1="9.86155" y1="10.78865" x2="10.56005" y2="10.80135" layer="21"/>
<rectangle x1="10.66165" y1="10.78865" x2="10.77595" y2="10.80135" layer="21"/>
<rectangle x1="10.89025" y1="10.78865" x2="11.57605" y2="10.80135" layer="21"/>
<rectangle x1="11.69035" y1="10.78865" x2="12.70635" y2="10.80135" layer="21"/>
<rectangle x1="12.80795" y1="10.78865" x2="12.83335" y2="10.80135" layer="21"/>
<rectangle x1="12.93495" y1="10.78865" x2="14.02715" y2="10.80135" layer="21"/>
<rectangle x1="14.11605" y1="10.78865" x2="14.70025" y2="10.80135" layer="21"/>
<rectangle x1="14.86535" y1="10.78865" x2="15.58925" y2="10.80135" layer="21"/>
<rectangle x1="15.67815" y1="10.78865" x2="17.32915" y2="10.80135" layer="21"/>
<rectangle x1="4.76885" y1="10.80135" x2="6.10235" y2="10.81405" layer="21"/>
<rectangle x1="6.31825" y1="10.80135" x2="7.42315" y2="10.81405" layer="21"/>
<rectangle x1="7.51205" y1="10.80135" x2="8.19785" y2="10.81405" layer="21"/>
<rectangle x1="8.32485" y1="10.80135" x2="8.95985" y2="10.81405" layer="21"/>
<rectangle x1="9.08685" y1="10.80135" x2="9.74725" y2="10.81405" layer="21"/>
<rectangle x1="9.86155" y1="10.80135" x2="10.54735" y2="10.81405" layer="21"/>
<rectangle x1="10.66165" y1="10.80135" x2="10.78865" y2="10.81405" layer="21"/>
<rectangle x1="10.89025" y1="10.80135" x2="11.58875" y2="10.81405" layer="21"/>
<rectangle x1="11.69035" y1="10.80135" x2="12.70635" y2="10.81405" layer="21"/>
<rectangle x1="12.92225" y1="10.80135" x2="14.02715" y2="10.81405" layer="21"/>
<rectangle x1="14.11605" y1="10.80135" x2="14.67485" y2="10.81405" layer="21"/>
<rectangle x1="14.85265" y1="10.80135" x2="15.58925" y2="10.81405" layer="21"/>
<rectangle x1="15.67815" y1="10.80135" x2="17.32915" y2="10.81405" layer="21"/>
<rectangle x1="4.76885" y1="10.81405" x2="6.11505" y2="10.82675" layer="21"/>
<rectangle x1="6.31825" y1="10.81405" x2="7.42315" y2="10.82675" layer="21"/>
<rectangle x1="7.51205" y1="10.81405" x2="8.18515" y2="10.82675" layer="21"/>
<rectangle x1="8.31215" y1="10.81405" x2="8.95985" y2="10.82675" layer="21"/>
<rectangle x1="9.08685" y1="10.81405" x2="9.74725" y2="10.82675" layer="21"/>
<rectangle x1="9.84885" y1="10.81405" x2="10.54735" y2="10.82675" layer="21"/>
<rectangle x1="10.64895" y1="10.81405" x2="10.78865" y2="10.82675" layer="21"/>
<rectangle x1="10.90295" y1="10.81405" x2="11.58875" y2="10.82675" layer="21"/>
<rectangle x1="11.70305" y1="10.81405" x2="12.71905" y2="10.82675" layer="21"/>
<rectangle x1="12.92225" y1="10.81405" x2="14.02715" y2="10.82675" layer="21"/>
<rectangle x1="14.11605" y1="10.81405" x2="14.64945" y2="10.82675" layer="21"/>
<rectangle x1="14.83995" y1="10.81405" x2="15.58925" y2="10.82675" layer="21"/>
<rectangle x1="15.67815" y1="10.81405" x2="17.32915" y2="10.82675" layer="21"/>
<rectangle x1="4.76885" y1="10.82675" x2="6.11505" y2="10.83945" layer="21"/>
<rectangle x1="6.31825" y1="10.82675" x2="7.42315" y2="10.83945" layer="21"/>
<rectangle x1="7.51205" y1="10.82675" x2="8.17245" y2="10.83945" layer="21"/>
<rectangle x1="8.29945" y1="10.82675" x2="8.97255" y2="10.83945" layer="21"/>
<rectangle x1="9.09955" y1="10.82675" x2="9.73455" y2="10.83945" layer="21"/>
<rectangle x1="9.84885" y1="10.82675" x2="10.53465" y2="10.83945" layer="21"/>
<rectangle x1="10.64895" y1="10.82675" x2="10.78865" y2="10.83945" layer="21"/>
<rectangle x1="10.90295" y1="10.82675" x2="11.58875" y2="10.83945" layer="21"/>
<rectangle x1="11.70305" y1="10.82675" x2="12.71905" y2="10.83945" layer="21"/>
<rectangle x1="12.92225" y1="10.82675" x2="14.02715" y2="10.83945" layer="21"/>
<rectangle x1="14.11605" y1="10.82675" x2="14.61135" y2="10.83945" layer="21"/>
<rectangle x1="14.82725" y1="10.82675" x2="15.58925" y2="10.83945" layer="21"/>
<rectangle x1="15.67815" y1="10.82675" x2="17.32915" y2="10.83945" layer="21"/>
<rectangle x1="4.76885" y1="10.83945" x2="6.12775" y2="10.85215" layer="21"/>
<rectangle x1="6.30555" y1="10.83945" x2="7.42315" y2="10.85215" layer="21"/>
<rectangle x1="7.51205" y1="10.83945" x2="8.17245" y2="10.85215" layer="21"/>
<rectangle x1="8.28675" y1="10.83945" x2="8.98525" y2="10.85215" layer="21"/>
<rectangle x1="9.11225" y1="10.83945" x2="9.73455" y2="10.85215" layer="21"/>
<rectangle x1="9.84885" y1="10.83945" x2="10.53465" y2="10.85215" layer="21"/>
<rectangle x1="10.64895" y1="10.83945" x2="10.80135" y2="10.85215" layer="21"/>
<rectangle x1="10.91565" y1="10.83945" x2="11.60145" y2="10.85215" layer="21"/>
<rectangle x1="11.71575" y1="10.83945" x2="12.73175" y2="10.85215" layer="21"/>
<rectangle x1="12.90955" y1="10.83945" x2="14.02715" y2="10.85215" layer="21"/>
<rectangle x1="14.11605" y1="10.83945" x2="14.53515" y2="10.85215" layer="21"/>
<rectangle x1="14.80185" y1="10.83945" x2="15.58925" y2="10.85215" layer="21"/>
<rectangle x1="15.67815" y1="10.83945" x2="17.32915" y2="10.85215" layer="21"/>
<rectangle x1="4.76885" y1="10.85215" x2="6.12775" y2="10.86485" layer="21"/>
<rectangle x1="6.30555" y1="10.85215" x2="7.42315" y2="10.86485" layer="21"/>
<rectangle x1="7.51205" y1="10.85215" x2="8.15975" y2="10.86485" layer="21"/>
<rectangle x1="8.28675" y1="10.85215" x2="8.99795" y2="10.86485" layer="21"/>
<rectangle x1="9.12495" y1="10.85215" x2="9.72185" y2="10.86485" layer="21"/>
<rectangle x1="9.83615" y1="10.85215" x2="10.52195" y2="10.86485" layer="21"/>
<rectangle x1="10.63625" y1="10.85215" x2="10.80135" y2="10.86485" layer="21"/>
<rectangle x1="10.91565" y1="10.85215" x2="11.60145" y2="10.86485" layer="21"/>
<rectangle x1="11.71575" y1="10.85215" x2="12.73175" y2="10.86485" layer="21"/>
<rectangle x1="12.90955" y1="10.85215" x2="14.02715" y2="10.86485" layer="21"/>
<rectangle x1="14.78915" y1="10.85215" x2="15.58925" y2="10.86485" layer="21"/>
<rectangle x1="16.45285" y1="10.85215" x2="17.32915" y2="10.86485" layer="21"/>
<rectangle x1="4.76885" y1="10.86485" x2="6.14045" y2="10.87755" layer="21"/>
<rectangle x1="6.29285" y1="10.86485" x2="7.42315" y2="10.87755" layer="21"/>
<rectangle x1="7.51205" y1="10.86485" x2="8.14705" y2="10.87755" layer="21"/>
<rectangle x1="8.27405" y1="10.86485" x2="8.99795" y2="10.87755" layer="21"/>
<rectangle x1="9.12495" y1="10.86485" x2="9.72185" y2="10.87755" layer="21"/>
<rectangle x1="9.83615" y1="10.86485" x2="10.52195" y2="10.87755" layer="21"/>
<rectangle x1="10.63625" y1="10.86485" x2="10.81405" y2="10.87755" layer="21"/>
<rectangle x1="10.92835" y1="10.86485" x2="11.61415" y2="10.87755" layer="21"/>
<rectangle x1="11.72845" y1="10.86485" x2="12.74445" y2="10.87755" layer="21"/>
<rectangle x1="12.89685" y1="10.86485" x2="14.02715" y2="10.87755" layer="21"/>
<rectangle x1="14.76375" y1="10.86485" x2="15.58925" y2="10.87755" layer="21"/>
<rectangle x1="16.45285" y1="10.86485" x2="17.32915" y2="10.87755" layer="21"/>
<rectangle x1="4.76885" y1="10.87755" x2="6.14045" y2="10.89025" layer="21"/>
<rectangle x1="6.29285" y1="10.87755" x2="7.42315" y2="10.89025" layer="21"/>
<rectangle x1="7.51205" y1="10.87755" x2="8.13435" y2="10.89025" layer="21"/>
<rectangle x1="8.26135" y1="10.87755" x2="9.01065" y2="10.89025" layer="21"/>
<rectangle x1="9.13765" y1="10.87755" x2="9.70915" y2="10.89025" layer="21"/>
<rectangle x1="9.82345" y1="10.87755" x2="10.50925" y2="10.89025" layer="21"/>
<rectangle x1="10.62355" y1="10.87755" x2="10.81405" y2="10.89025" layer="21"/>
<rectangle x1="10.92835" y1="10.87755" x2="11.61415" y2="10.89025" layer="21"/>
<rectangle x1="11.72845" y1="10.87755" x2="12.74445" y2="10.89025" layer="21"/>
<rectangle x1="12.89685" y1="10.87755" x2="14.02715" y2="10.89025" layer="21"/>
<rectangle x1="14.73835" y1="10.87755" x2="15.58925" y2="10.89025" layer="21"/>
<rectangle x1="16.45285" y1="10.87755" x2="17.32915" y2="10.89025" layer="21"/>
<rectangle x1="4.76885" y1="10.89025" x2="6.15315" y2="10.90295" layer="21"/>
<rectangle x1="6.28015" y1="10.89025" x2="7.42315" y2="10.90295" layer="21"/>
<rectangle x1="7.51205" y1="10.89025" x2="8.13435" y2="10.90295" layer="21"/>
<rectangle x1="8.24865" y1="10.89025" x2="9.02335" y2="10.90295" layer="21"/>
<rectangle x1="9.15035" y1="10.89025" x2="9.70915" y2="10.90295" layer="21"/>
<rectangle x1="9.82345" y1="10.89025" x2="10.50925" y2="10.90295" layer="21"/>
<rectangle x1="10.62355" y1="10.89025" x2="10.82675" y2="10.90295" layer="21"/>
<rectangle x1="10.92835" y1="10.89025" x2="11.62685" y2="10.90295" layer="21"/>
<rectangle x1="11.72845" y1="10.89025" x2="12.75715" y2="10.90295" layer="21"/>
<rectangle x1="12.88415" y1="10.89025" x2="14.02715" y2="10.90295" layer="21"/>
<rectangle x1="14.71295" y1="10.89025" x2="15.58925" y2="10.90295" layer="21"/>
<rectangle x1="16.45285" y1="10.89025" x2="17.32915" y2="10.90295" layer="21"/>
<rectangle x1="4.76885" y1="10.90295" x2="6.15315" y2="10.91565" layer="21"/>
<rectangle x1="6.28015" y1="10.90295" x2="7.42315" y2="10.91565" layer="21"/>
<rectangle x1="7.51205" y1="10.90295" x2="8.12165" y2="10.91565" layer="21"/>
<rectangle x1="8.24865" y1="10.90295" x2="9.03605" y2="10.91565" layer="21"/>
<rectangle x1="9.16305" y1="10.90295" x2="9.70915" y2="10.91565" layer="21"/>
<rectangle x1="9.81075" y1="10.90295" x2="10.50925" y2="10.91565" layer="21"/>
<rectangle x1="10.61085" y1="10.90295" x2="10.82675" y2="10.91565" layer="21"/>
<rectangle x1="10.94105" y1="10.90295" x2="11.62685" y2="10.91565" layer="21"/>
<rectangle x1="11.74115" y1="10.90295" x2="12.75715" y2="10.91565" layer="21"/>
<rectangle x1="12.88415" y1="10.90295" x2="14.02715" y2="10.91565" layer="21"/>
<rectangle x1="14.67485" y1="10.90295" x2="15.58925" y2="10.91565" layer="21"/>
<rectangle x1="16.45285" y1="10.90295" x2="17.32915" y2="10.91565" layer="21"/>
<rectangle x1="4.76885" y1="10.91565" x2="6.16585" y2="10.92835" layer="21"/>
<rectangle x1="6.26745" y1="10.91565" x2="7.42315" y2="10.92835" layer="21"/>
<rectangle x1="7.51205" y1="10.91565" x2="8.10895" y2="10.92835" layer="21"/>
<rectangle x1="8.23595" y1="10.91565" x2="9.03605" y2="10.92835" layer="21"/>
<rectangle x1="9.16305" y1="10.91565" x2="9.69645" y2="10.92835" layer="21"/>
<rectangle x1="9.81075" y1="10.91565" x2="10.49655" y2="10.92835" layer="21"/>
<rectangle x1="10.61085" y1="10.91565" x2="10.83945" y2="10.92835" layer="21"/>
<rectangle x1="10.94105" y1="10.91565" x2="11.63955" y2="10.92835" layer="21"/>
<rectangle x1="11.74115" y1="10.91565" x2="12.76985" y2="10.92835" layer="21"/>
<rectangle x1="12.87145" y1="10.91565" x2="14.02715" y2="10.92835" layer="21"/>
<rectangle x1="14.62405" y1="10.91565" x2="15.58925" y2="10.92835" layer="21"/>
<rectangle x1="16.45285" y1="10.91565" x2="17.32915" y2="10.92835" layer="21"/>
<rectangle x1="4.76885" y1="10.92835" x2="6.16585" y2="10.94105" layer="21"/>
<rectangle x1="6.26745" y1="10.92835" x2="7.42315" y2="10.94105" layer="21"/>
<rectangle x1="7.51205" y1="10.92835" x2="8.10895" y2="10.94105" layer="21"/>
<rectangle x1="8.22325" y1="10.92835" x2="9.04875" y2="10.94105" layer="21"/>
<rectangle x1="9.17575" y1="10.92835" x2="9.69645" y2="10.94105" layer="21"/>
<rectangle x1="9.79805" y1="10.92835" x2="10.49655" y2="10.94105" layer="21"/>
<rectangle x1="10.59815" y1="10.92835" x2="10.83945" y2="10.94105" layer="21"/>
<rectangle x1="10.94105" y1="10.92835" x2="11.63955" y2="10.94105" layer="21"/>
<rectangle x1="11.74115" y1="10.92835" x2="12.76985" y2="10.94105" layer="21"/>
<rectangle x1="12.87145" y1="10.92835" x2="14.02715" y2="10.94105" layer="21"/>
<rectangle x1="14.54785" y1="10.92835" x2="15.58925" y2="10.94105" layer="21"/>
<rectangle x1="16.44015" y1="10.92835" x2="17.32915" y2="10.94105" layer="21"/>
<rectangle x1="4.76885" y1="10.94105" x2="17.32915" y2="10.95375" layer="21"/>
<rectangle x1="4.76885" y1="10.95375" x2="17.32915" y2="10.96645" layer="21"/>
<rectangle x1="4.76885" y1="10.96645" x2="17.32915" y2="10.97915" layer="21"/>
<rectangle x1="4.76885" y1="10.97915" x2="17.32915" y2="10.99185" layer="21"/>
<rectangle x1="4.76885" y1="10.99185" x2="17.32915" y2="11.00455" layer="21"/>
<rectangle x1="4.76885" y1="11.00455" x2="17.32915" y2="11.01725" layer="21"/>
<rectangle x1="4.76885" y1="11.01725" x2="17.32915" y2="11.02995" layer="21"/>
<rectangle x1="4.76885" y1="11.02995" x2="17.32915" y2="11.04265" layer="21"/>
<rectangle x1="4.76885" y1="11.04265" x2="17.32915" y2="11.05535" layer="21"/>
<rectangle x1="4.76885" y1="11.05535" x2="17.32915" y2="11.06805" layer="21"/>
<rectangle x1="4.76885" y1="11.06805" x2="17.32915" y2="11.08075" layer="21"/>
<rectangle x1="4.76885" y1="11.08075" x2="17.32915" y2="11.09345" layer="21"/>
<rectangle x1="4.76885" y1="11.09345" x2="17.32915" y2="11.10615" layer="21"/>
<rectangle x1="4.76885" y1="11.10615" x2="17.32915" y2="11.11885" layer="21"/>
<rectangle x1="4.76885" y1="11.11885" x2="17.32915" y2="11.13155" layer="21"/>
<rectangle x1="4.76885" y1="11.13155" x2="17.32915" y2="11.14425" layer="21"/>
<rectangle x1="4.76885" y1="11.14425" x2="17.32915" y2="11.15695" layer="21"/>
<rectangle x1="4.76885" y1="11.15695" x2="17.32915" y2="11.16965" layer="21"/>
<rectangle x1="4.76885" y1="11.16965" x2="17.32915" y2="11.18235" layer="21"/>
<rectangle x1="4.76885" y1="11.18235" x2="17.32915" y2="11.19505" layer="21"/>
<rectangle x1="4.76885" y1="11.19505" x2="17.32915" y2="11.20775" layer="21"/>
<rectangle x1="4.76885" y1="11.20775" x2="17.32915" y2="11.22045" layer="21"/>
<rectangle x1="4.76885" y1="11.22045" x2="17.32915" y2="11.23315" layer="21"/>
<rectangle x1="4.76885" y1="11.23315" x2="17.32915" y2="11.24585" layer="21"/>
<rectangle x1="4.76885" y1="11.24585" x2="17.32915" y2="11.25855" layer="21"/>
<rectangle x1="4.76885" y1="11.25855" x2="17.32915" y2="11.27125" layer="21"/>
<rectangle x1="4.76885" y1="11.27125" x2="17.32915" y2="11.28395" layer="21"/>
<rectangle x1="4.76885" y1="11.28395" x2="17.32915" y2="11.29665" layer="21"/>
<rectangle x1="4.76885" y1="11.29665" x2="17.32915" y2="11.30935" layer="21"/>
<rectangle x1="4.76885" y1="11.30935" x2="17.32915" y2="11.32205" layer="21"/>
<rectangle x1="4.76885" y1="11.32205" x2="17.32915" y2="11.33475" layer="21"/>
<rectangle x1="4.76885" y1="11.33475" x2="17.32915" y2="11.34745" layer="21"/>
<rectangle x1="4.76885" y1="11.34745" x2="17.32915" y2="11.36015" layer="21"/>
<rectangle x1="4.76885" y1="11.36015" x2="17.32915" y2="11.37285" layer="21"/>
<rectangle x1="4.76885" y1="11.37285" x2="17.32915" y2="11.38555" layer="21"/>
<rectangle x1="4.76885" y1="11.38555" x2="17.32915" y2="11.39825" layer="21"/>
<rectangle x1="4.76885" y1="11.39825" x2="17.32915" y2="11.41095" layer="21"/>
<rectangle x1="4.76885" y1="11.41095" x2="17.32915" y2="11.42365" layer="21"/>
<rectangle x1="4.76885" y1="11.42365" x2="17.32915" y2="11.43635" layer="21"/>
<rectangle x1="4.76885" y1="11.43635" x2="17.32915" y2="11.44905" layer="21"/>
<rectangle x1="4.76885" y1="11.44905" x2="17.32915" y2="11.46175" layer="21"/>
<rectangle x1="4.76885" y1="11.46175" x2="17.32915" y2="11.47445" layer="21"/>
<rectangle x1="4.76885" y1="11.47445" x2="17.32915" y2="11.48715" layer="21"/>
<rectangle x1="4.76885" y1="11.48715" x2="17.32915" y2="11.49985" layer="21"/>
<rectangle x1="4.76885" y1="11.49985" x2="17.32915" y2="11.51255" layer="21"/>
<rectangle x1="4.76885" y1="11.51255" x2="17.32915" y2="11.52525" layer="21"/>
<rectangle x1="4.76885" y1="11.52525" x2="17.32915" y2="11.53795" layer="21"/>
<rectangle x1="4.76885" y1="11.53795" x2="17.32915" y2="11.55065" layer="21"/>
<rectangle x1="4.76885" y1="11.55065" x2="17.32915" y2="11.56335" layer="21"/>
<rectangle x1="4.76885" y1="11.56335" x2="17.32915" y2="11.57605" layer="21"/>
<rectangle x1="4.76885" y1="11.57605" x2="17.32915" y2="11.58875" layer="21"/>
<rectangle x1="4.76885" y1="11.58875" x2="17.32915" y2="11.60145" layer="21"/>
<rectangle x1="4.76885" y1="11.60145" x2="17.32915" y2="11.61415" layer="21"/>
<rectangle x1="4.76885" y1="11.61415" x2="17.32915" y2="11.62685" layer="21"/>
<rectangle x1="4.76885" y1="11.62685" x2="17.32915" y2="11.63955" layer="21"/>
<rectangle x1="4.76885" y1="11.63955" x2="17.32915" y2="11.65225" layer="21"/>
<rectangle x1="4.76885" y1="11.65225" x2="17.32915" y2="11.66495" layer="21"/>
<rectangle x1="4.76885" y1="11.66495" x2="17.32915" y2="11.67765" layer="21"/>
<rectangle x1="4.76885" y1="11.67765" x2="17.32915" y2="11.69035" layer="21"/>
<rectangle x1="4.76885" y1="11.69035" x2="17.32915" y2="11.70305" layer="21"/>
<rectangle x1="4.76885" y1="11.70305" x2="17.32915" y2="11.71575" layer="21"/>
<rectangle x1="4.76885" y1="11.71575" x2="17.32915" y2="11.72845" layer="21"/>
<rectangle x1="4.76885" y1="11.72845" x2="17.32915" y2="11.74115" layer="21"/>
<rectangle x1="4.76885" y1="11.74115" x2="17.32915" y2="11.75385" layer="21"/>
<rectangle x1="4.76885" y1="11.75385" x2="17.32915" y2="11.76655" layer="21"/>
<rectangle x1="4.76885" y1="11.76655" x2="17.32915" y2="11.77925" layer="21"/>
<rectangle x1="4.76885" y1="11.77925" x2="17.32915" y2="11.79195" layer="21"/>
<rectangle x1="4.76885" y1="11.79195" x2="17.32915" y2="11.80465" layer="21"/>
<rectangle x1="4.76885" y1="11.80465" x2="17.32915" y2="11.81735" layer="21"/>
<rectangle x1="4.76885" y1="11.81735" x2="17.32915" y2="11.83005" layer="21"/>
<rectangle x1="4.76885" y1="11.83005" x2="17.32915" y2="11.84275" layer="21"/>
<rectangle x1="4.76885" y1="11.84275" x2="17.32915" y2="11.85545" layer="21"/>
<rectangle x1="4.76885" y1="11.85545" x2="17.32915" y2="11.86815" layer="21"/>
<rectangle x1="4.76885" y1="11.86815" x2="17.32915" y2="11.88085" layer="21"/>
<rectangle x1="4.76885" y1="11.88085" x2="17.32915" y2="11.89355" layer="21"/>
<rectangle x1="4.76885" y1="11.89355" x2="17.32915" y2="11.90625" layer="21"/>
<rectangle x1="4.76885" y1="11.90625" x2="17.32915" y2="11.91895" layer="21"/>
<rectangle x1="4.76885" y1="11.91895" x2="17.32915" y2="11.93165" layer="21"/>
<rectangle x1="4.76885" y1="11.93165" x2="17.32915" y2="11.94435" layer="21"/>
<rectangle x1="4.76885" y1="11.94435" x2="17.32915" y2="11.95705" layer="21"/>
<rectangle x1="4.76885" y1="11.95705" x2="17.32915" y2="11.96975" layer="21"/>
<rectangle x1="4.76885" y1="11.96975" x2="17.32915" y2="11.98245" layer="21"/>
<rectangle x1="4.76885" y1="11.98245" x2="17.32915" y2="11.99515" layer="21"/>
<rectangle x1="4.76885" y1="11.99515" x2="17.32915" y2="12.00785" layer="21"/>
<rectangle x1="4.76885" y1="12.00785" x2="17.32915" y2="12.02055" layer="21"/>
<rectangle x1="4.76885" y1="12.02055" x2="17.32915" y2="12.03325" layer="21"/>
<rectangle x1="4.76885" y1="12.03325" x2="17.32915" y2="12.04595" layer="21"/>
<rectangle x1="4.76885" y1="12.04595" x2="17.32915" y2="12.05865" layer="21"/>
<rectangle x1="4.76885" y1="12.05865" x2="17.32915" y2="12.07135" layer="21"/>
<rectangle x1="4.76885" y1="12.07135" x2="17.32915" y2="12.08405" layer="21"/>
<rectangle x1="4.76885" y1="12.08405" x2="17.32915" y2="12.09675" layer="21"/>
<rectangle x1="4.76885" y1="12.09675" x2="17.32915" y2="12.10945" layer="21"/>
<rectangle x1="4.76885" y1="12.10945" x2="17.32915" y2="12.12215" layer="21"/>
<rectangle x1="4.76885" y1="12.12215" x2="17.32915" y2="12.13485" layer="21"/>
<rectangle x1="4.76885" y1="12.13485" x2="17.32915" y2="12.14755" layer="21"/>
<rectangle x1="4.76885" y1="12.14755" x2="17.32915" y2="12.16025" layer="21"/>
<rectangle x1="4.76885" y1="12.16025" x2="17.32915" y2="12.17295" layer="21"/>
<rectangle x1="4.76885" y1="12.17295" x2="17.32915" y2="12.18565" layer="21"/>
<rectangle x1="4.76885" y1="12.18565" x2="17.32915" y2="12.19835" layer="21"/>
<rectangle x1="4.76885" y1="12.19835" x2="17.32915" y2="12.21105" layer="21"/>
<rectangle x1="4.76885" y1="12.21105" x2="17.32915" y2="12.22375" layer="21"/>
<rectangle x1="4.76885" y1="12.22375" x2="17.32915" y2="12.23645" layer="21"/>
<rectangle x1="4.76885" y1="12.23645" x2="17.32915" y2="12.24915" layer="21"/>
<rectangle x1="4.76885" y1="12.24915" x2="17.32915" y2="12.26185" layer="21"/>
<rectangle x1="4.76885" y1="12.26185" x2="17.32915" y2="12.27455" layer="21"/>
<rectangle x1="4.76885" y1="12.27455" x2="17.32915" y2="12.28725" layer="21"/>
<rectangle x1="4.76885" y1="12.28725" x2="17.32915" y2="12.29995" layer="21"/>
<rectangle x1="4.76885" y1="12.29995" x2="17.32915" y2="12.31265" layer="21"/>
<rectangle x1="4.76885" y1="12.31265" x2="17.32915" y2="12.32535" layer="21"/>
<rectangle x1="4.76885" y1="12.32535" x2="17.32915" y2="12.33805" layer="21"/>
<rectangle x1="4.76885" y1="12.33805" x2="17.32915" y2="12.35075" layer="21"/>
<rectangle x1="4.76885" y1="12.35075" x2="17.32915" y2="12.36345" layer="21"/>
<rectangle x1="4.76885" y1="12.36345" x2="17.32915" y2="12.37615" layer="21"/>
<rectangle x1="4.76885" y1="12.37615" x2="17.32915" y2="12.38885" layer="21"/>
<rectangle x1="4.76885" y1="12.38885" x2="17.32915" y2="12.40155" layer="21"/>
<rectangle x1="4.76885" y1="12.40155" x2="17.32915" y2="12.41425" layer="21"/>
<rectangle x1="4.76885" y1="12.41425" x2="17.32915" y2="12.42695" layer="21"/>
<rectangle x1="4.76885" y1="12.42695" x2="17.32915" y2="12.43965" layer="21"/>
<rectangle x1="4.76885" y1="12.43965" x2="17.32915" y2="12.45235" layer="21"/>
<rectangle x1="4.76885" y1="12.45235" x2="17.32915" y2="12.46505" layer="21"/>
<rectangle x1="4.76885" y1="12.46505" x2="17.32915" y2="12.47775" layer="21"/>
<rectangle x1="4.79425" y1="12.47775" x2="17.30375" y2="12.49045" layer="21"/>
<rectangle x1="4.81965" y1="12.49045" x2="17.27835" y2="12.50315" layer="21"/>
<rectangle x1="4.84505" y1="12.50315" x2="17.25295" y2="12.51585" layer="21"/>
<rectangle x1="4.87045" y1="12.51585" x2="17.22755" y2="12.52855" layer="21"/>
<rectangle x1="4.89585" y1="12.52855" x2="17.20215" y2="12.54125" layer="21"/>
<rectangle x1="4.92125" y1="12.54125" x2="17.17675" y2="12.55395" layer="21"/>
<rectangle x1="4.94665" y1="12.55395" x2="17.15135" y2="12.56665" layer="21"/>
<rectangle x1="4.97205" y1="12.56665" x2="17.12595" y2="12.57935" layer="21"/>
<rectangle x1="4.99745" y1="12.57935" x2="17.10055" y2="12.59205" layer="21"/>
<rectangle x1="5.02285" y1="12.59205" x2="17.07515" y2="12.60475" layer="21"/>
<rectangle x1="5.04825" y1="12.60475" x2="17.04975" y2="12.61745" layer="21"/>
<rectangle x1="5.07365" y1="12.61745" x2="17.02435" y2="12.63015" layer="21"/>
<rectangle x1="5.09905" y1="12.63015" x2="16.99895" y2="12.64285" layer="21"/>
<rectangle x1="5.12445" y1="12.64285" x2="16.97355" y2="12.65555" layer="21"/>
<rectangle x1="5.14985" y1="12.65555" x2="16.94815" y2="12.66825" layer="21"/>
<rectangle x1="5.17525" y1="12.66825" x2="16.92275" y2="12.68095" layer="21"/>
<rectangle x1="5.20065" y1="12.68095" x2="16.89735" y2="12.69365" layer="21"/>
<rectangle x1="5.22605" y1="12.69365" x2="16.87195" y2="12.70635" layer="21"/>
<rectangle x1="5.25145" y1="12.70635" x2="16.84655" y2="12.71905" layer="21"/>
<rectangle x1="5.27685" y1="12.71905" x2="16.82115" y2="12.73175" layer="21"/>
<rectangle x1="5.30225" y1="12.73175" x2="16.79575" y2="12.74445" layer="21"/>
<rectangle x1="5.32765" y1="12.74445" x2="16.77035" y2="12.75715" layer="21"/>
<rectangle x1="5.35305" y1="12.75715" x2="16.74495" y2="12.76985" layer="21"/>
<rectangle x1="5.37845" y1="12.76985" x2="16.71955" y2="12.78255" layer="21"/>
<rectangle x1="5.40385" y1="12.78255" x2="16.69415" y2="12.79525" layer="21"/>
<rectangle x1="5.42925" y1="12.79525" x2="16.66875" y2="12.80795" layer="21"/>
<rectangle x1="5.45465" y1="12.80795" x2="16.64335" y2="12.82065" layer="21"/>
<rectangle x1="5.48005" y1="12.82065" x2="16.61795" y2="12.83335" layer="21"/>
<rectangle x1="5.50545" y1="12.83335" x2="16.59255" y2="12.84605" layer="21"/>
<rectangle x1="5.53085" y1="12.84605" x2="16.56715" y2="12.85875" layer="21"/>
<rectangle x1="5.55625" y1="12.85875" x2="16.54175" y2="12.87145" layer="21"/>
<rectangle x1="5.58165" y1="12.87145" x2="16.51635" y2="12.88415" layer="21"/>
<rectangle x1="5.60705" y1="12.88415" x2="16.49095" y2="12.89685" layer="21"/>
<rectangle x1="5.63245" y1="12.89685" x2="16.46555" y2="12.90955" layer="21"/>
<rectangle x1="5.65785" y1="12.90955" x2="16.44015" y2="12.92225" layer="21"/>
<rectangle x1="5.68325" y1="12.92225" x2="16.41475" y2="12.93495" layer="21"/>
<rectangle x1="5.70865" y1="12.93495" x2="16.38935" y2="12.94765" layer="21"/>
<rectangle x1="5.73405" y1="12.94765" x2="16.36395" y2="12.96035" layer="21"/>
<rectangle x1="5.75945" y1="12.96035" x2="16.33855" y2="12.97305" layer="21"/>
<rectangle x1="5.78485" y1="12.97305" x2="16.31315" y2="12.98575" layer="21"/>
<rectangle x1="5.81025" y1="12.98575" x2="16.28775" y2="12.99845" layer="21"/>
<rectangle x1="5.83565" y1="12.99845" x2="16.26235" y2="13.01115" layer="21"/>
<rectangle x1="5.86105" y1="13.01115" x2="16.23695" y2="13.02385" layer="21"/>
<rectangle x1="5.88645" y1="13.02385" x2="16.21155" y2="13.03655" layer="21"/>
<rectangle x1="5.91185" y1="13.03655" x2="16.18615" y2="13.04925" layer="21"/>
<rectangle x1="5.93725" y1="13.04925" x2="16.16075" y2="13.06195" layer="21"/>
<rectangle x1="5.96265" y1="13.06195" x2="16.13535" y2="13.07465" layer="21"/>
<rectangle x1="5.98805" y1="13.07465" x2="16.10995" y2="13.08735" layer="21"/>
<rectangle x1="6.01345" y1="13.08735" x2="16.08455" y2="13.10005" layer="21"/>
<rectangle x1="6.03885" y1="13.10005" x2="16.05915" y2="13.11275" layer="21"/>
<rectangle x1="6.06425" y1="13.11275" x2="16.03375" y2="13.12545" layer="21"/>
<rectangle x1="6.08965" y1="13.12545" x2="16.00835" y2="13.13815" layer="21"/>
<rectangle x1="6.11505" y1="13.13815" x2="15.98295" y2="13.15085" layer="21"/>
<rectangle x1="6.14045" y1="13.15085" x2="15.95755" y2="13.16355" layer="21"/>
<rectangle x1="6.16585" y1="13.16355" x2="15.93215" y2="13.17625" layer="21"/>
<rectangle x1="6.19125" y1="13.17625" x2="15.90675" y2="13.18895" layer="21"/>
<rectangle x1="6.21665" y1="13.18895" x2="15.88135" y2="13.20165" layer="21"/>
<rectangle x1="6.24205" y1="13.20165" x2="15.85595" y2="13.21435" layer="21"/>
<rectangle x1="6.26745" y1="13.21435" x2="15.83055" y2="13.22705" layer="21"/>
<rectangle x1="6.29285" y1="13.22705" x2="15.80515" y2="13.23975" layer="21"/>
<rectangle x1="6.31825" y1="13.23975" x2="15.77975" y2="13.25245" layer="21"/>
<rectangle x1="6.34365" y1="13.25245" x2="15.75435" y2="13.26515" layer="21"/>
<rectangle x1="6.36905" y1="13.26515" x2="15.72895" y2="13.27785" layer="21"/>
<rectangle x1="6.39445" y1="13.27785" x2="15.70355" y2="13.29055" layer="21"/>
<rectangle x1="6.41985" y1="13.29055" x2="15.67815" y2="13.30325" layer="21"/>
<rectangle x1="6.44525" y1="13.30325" x2="15.65275" y2="13.31595" layer="21"/>
<rectangle x1="6.47065" y1="13.31595" x2="15.62735" y2="13.32865" layer="21"/>
<rectangle x1="6.49605" y1="13.32865" x2="15.60195" y2="13.34135" layer="21"/>
<rectangle x1="6.52145" y1="13.34135" x2="15.57655" y2="13.35405" layer="21"/>
<rectangle x1="6.54685" y1="13.35405" x2="15.55115" y2="13.36675" layer="21"/>
<rectangle x1="6.57225" y1="13.36675" x2="15.52575" y2="13.37945" layer="21"/>
<rectangle x1="6.59765" y1="13.37945" x2="15.50035" y2="13.39215" layer="21"/>
<rectangle x1="6.62305" y1="13.39215" x2="15.47495" y2="13.40485" layer="21"/>
<rectangle x1="6.64845" y1="13.40485" x2="15.44955" y2="13.41755" layer="21"/>
<rectangle x1="6.67385" y1="13.41755" x2="15.42415" y2="13.43025" layer="21"/>
<rectangle x1="6.69925" y1="13.43025" x2="15.39875" y2="13.44295" layer="21"/>
<rectangle x1="6.72465" y1="13.44295" x2="15.37335" y2="13.45565" layer="21"/>
<rectangle x1="6.75005" y1="13.45565" x2="15.34795" y2="13.46835" layer="21"/>
<rectangle x1="6.77545" y1="13.46835" x2="15.32255" y2="13.48105" layer="21"/>
<rectangle x1="6.80085" y1="13.48105" x2="15.29715" y2="13.49375" layer="21"/>
<rectangle x1="6.82625" y1="13.49375" x2="15.27175" y2="13.50645" layer="21"/>
<rectangle x1="6.85165" y1="13.50645" x2="15.24635" y2="13.51915" layer="21"/>
<rectangle x1="6.87705" y1="13.51915" x2="15.22095" y2="13.53185" layer="21"/>
<rectangle x1="6.90245" y1="13.53185" x2="15.19555" y2="13.54455" layer="21"/>
<rectangle x1="6.92785" y1="13.54455" x2="15.17015" y2="13.55725" layer="21"/>
<rectangle x1="6.95325" y1="13.55725" x2="15.14475" y2="13.56995" layer="21"/>
<rectangle x1="6.97865" y1="13.56995" x2="15.11935" y2="13.58265" layer="21"/>
<rectangle x1="7.00405" y1="13.58265" x2="15.09395" y2="13.59535" layer="21"/>
<rectangle x1="7.02945" y1="13.59535" x2="15.06855" y2="13.60805" layer="21"/>
<rectangle x1="7.05485" y1="13.60805" x2="15.04315" y2="13.62075" layer="21"/>
<rectangle x1="7.08025" y1="13.62075" x2="15.01775" y2="13.63345" layer="21"/>
<rectangle x1="7.10565" y1="13.63345" x2="14.99235" y2="13.64615" layer="21"/>
<rectangle x1="7.13105" y1="13.64615" x2="14.96695" y2="13.65885" layer="21"/>
<rectangle x1="7.15645" y1="13.65885" x2="14.94155" y2="13.67155" layer="21"/>
<rectangle x1="7.18185" y1="13.67155" x2="14.91615" y2="13.68425" layer="21"/>
<rectangle x1="7.20725" y1="13.68425" x2="14.89075" y2="13.69695" layer="21"/>
<rectangle x1="7.23265" y1="13.69695" x2="14.86535" y2="13.70965" layer="21"/>
<rectangle x1="7.25805" y1="13.70965" x2="14.83995" y2="13.72235" layer="21"/>
<rectangle x1="7.28345" y1="13.72235" x2="14.81455" y2="13.73505" layer="21"/>
<rectangle x1="7.30885" y1="13.73505" x2="14.78915" y2="13.74775" layer="21"/>
<rectangle x1="7.33425" y1="13.74775" x2="14.76375" y2="13.76045" layer="21"/>
<rectangle x1="7.35965" y1="13.76045" x2="14.73835" y2="13.77315" layer="21"/>
<rectangle x1="7.38505" y1="13.77315" x2="14.71295" y2="13.78585" layer="21"/>
<rectangle x1="7.41045" y1="13.78585" x2="14.68755" y2="13.79855" layer="21"/>
<rectangle x1="7.43585" y1="13.79855" x2="14.66215" y2="13.81125" layer="21"/>
<rectangle x1="7.46125" y1="13.81125" x2="14.63675" y2="13.82395" layer="21"/>
<rectangle x1="7.48665" y1="13.82395" x2="14.61135" y2="13.83665" layer="21"/>
<rectangle x1="7.51205" y1="13.83665" x2="14.58595" y2="13.84935" layer="21"/>
<rectangle x1="7.53745" y1="13.84935" x2="14.56055" y2="13.86205" layer="21"/>
<rectangle x1="7.56285" y1="13.86205" x2="14.53515" y2="13.87475" layer="21"/>
<rectangle x1="7.58825" y1="13.87475" x2="14.50975" y2="13.88745" layer="21"/>
<rectangle x1="7.61365" y1="13.88745" x2="14.48435" y2="13.90015" layer="21"/>
<rectangle x1="7.63905" y1="13.90015" x2="14.45895" y2="13.91285" layer="21"/>
<rectangle x1="7.66445" y1="13.91285" x2="14.43355" y2="13.92555" layer="21"/>
<rectangle x1="7.68985" y1="13.92555" x2="14.40815" y2="13.93825" layer="21"/>
<rectangle x1="7.71525" y1="13.93825" x2="14.38275" y2="13.95095" layer="21"/>
<rectangle x1="7.74065" y1="13.95095" x2="14.35735" y2="13.96365" layer="21"/>
<rectangle x1="7.76605" y1="13.96365" x2="14.33195" y2="13.97635" layer="21"/>
<rectangle x1="7.79145" y1="13.97635" x2="14.30655" y2="13.98905" layer="21"/>
<rectangle x1="7.81685" y1="13.98905" x2="14.28115" y2="14.00175" layer="21"/>
<rectangle x1="7.84225" y1="14.00175" x2="14.25575" y2="14.01445" layer="21"/>
<rectangle x1="7.86765" y1="14.01445" x2="14.23035" y2="14.02715" layer="21"/>
<rectangle x1="7.89305" y1="14.02715" x2="14.20495" y2="14.03985" layer="21"/>
<rectangle x1="7.91845" y1="14.03985" x2="14.17955" y2="14.05255" layer="21"/>
<rectangle x1="7.94385" y1="14.05255" x2="14.15415" y2="14.06525" layer="21"/>
<rectangle x1="7.96925" y1="14.06525" x2="14.12875" y2="14.07795" layer="21"/>
<rectangle x1="7.99465" y1="14.07795" x2="14.10335" y2="14.09065" layer="21"/>
<rectangle x1="8.02005" y1="14.09065" x2="14.07795" y2="14.10335" layer="21"/>
<rectangle x1="8.04545" y1="14.10335" x2="14.05255" y2="14.11605" layer="21"/>
<rectangle x1="8.07085" y1="14.11605" x2="14.02715" y2="14.12875" layer="21"/>
<rectangle x1="8.09625" y1="14.12875" x2="14.00175" y2="14.14145" layer="21"/>
<rectangle x1="8.12165" y1="14.14145" x2="13.97635" y2="14.15415" layer="21"/>
<rectangle x1="8.14705" y1="14.15415" x2="13.95095" y2="14.16685" layer="21"/>
<rectangle x1="8.17245" y1="14.16685" x2="13.92555" y2="14.17955" layer="21"/>
<rectangle x1="8.19785" y1="14.17955" x2="13.90015" y2="14.19225" layer="21"/>
<rectangle x1="8.22325" y1="14.19225" x2="13.87475" y2="14.20495" layer="21"/>
<rectangle x1="8.24865" y1="14.20495" x2="13.84935" y2="14.21765" layer="21"/>
<rectangle x1="8.27405" y1="14.21765" x2="13.82395" y2="14.23035" layer="21"/>
<rectangle x1="8.29945" y1="14.23035" x2="13.79855" y2="14.24305" layer="21"/>
<rectangle x1="8.32485" y1="14.24305" x2="13.77315" y2="14.25575" layer="21"/>
<rectangle x1="8.35025" y1="14.25575" x2="13.74775" y2="14.26845" layer="21"/>
<rectangle x1="8.37565" y1="14.26845" x2="13.72235" y2="14.28115" layer="21"/>
<rectangle x1="8.40105" y1="14.28115" x2="13.69695" y2="14.29385" layer="21"/>
<rectangle x1="8.42645" y1="14.29385" x2="13.67155" y2="14.30655" layer="21"/>
<rectangle x1="8.45185" y1="14.30655" x2="13.64615" y2="14.31925" layer="21"/>
<rectangle x1="8.47725" y1="14.31925" x2="13.62075" y2="14.33195" layer="21"/>
<rectangle x1="8.50265" y1="14.33195" x2="13.59535" y2="14.34465" layer="21"/>
<rectangle x1="8.52805" y1="14.34465" x2="13.56995" y2="14.35735" layer="21"/>
<rectangle x1="8.55345" y1="14.35735" x2="13.54455" y2="14.37005" layer="21"/>
<rectangle x1="8.57885" y1="14.37005" x2="13.51915" y2="14.38275" layer="21"/>
<rectangle x1="8.60425" y1="14.38275" x2="13.49375" y2="14.39545" layer="21"/>
<rectangle x1="8.62965" y1="14.39545" x2="13.46835" y2="14.40815" layer="21"/>
<rectangle x1="8.65505" y1="14.40815" x2="13.44295" y2="14.42085" layer="21"/>
<rectangle x1="8.68045" y1="14.42085" x2="13.41755" y2="14.43355" layer="21"/>
<rectangle x1="8.70585" y1="14.43355" x2="13.39215" y2="14.44625" layer="21"/>
<rectangle x1="8.73125" y1="14.44625" x2="13.36675" y2="14.45895" layer="21"/>
<rectangle x1="8.75665" y1="14.45895" x2="13.34135" y2="14.47165" layer="21"/>
<rectangle x1="8.78205" y1="14.47165" x2="13.31595" y2="14.48435" layer="21"/>
<rectangle x1="8.80745" y1="14.48435" x2="13.29055" y2="14.49705" layer="21"/>
<rectangle x1="8.83285" y1="14.49705" x2="13.26515" y2="14.50975" layer="21"/>
<rectangle x1="8.85825" y1="14.50975" x2="13.23975" y2="14.52245" layer="21"/>
<rectangle x1="8.88365" y1="14.52245" x2="13.21435" y2="14.53515" layer="21"/>
<rectangle x1="8.90905" y1="14.53515" x2="13.18895" y2="14.54785" layer="21"/>
<rectangle x1="8.93445" y1="14.54785" x2="13.16355" y2="14.56055" layer="21"/>
<rectangle x1="8.95985" y1="14.56055" x2="13.13815" y2="14.57325" layer="21"/>
<rectangle x1="8.98525" y1="14.57325" x2="13.11275" y2="14.58595" layer="21"/>
<rectangle x1="9.01065" y1="14.58595" x2="13.08735" y2="14.59865" layer="21"/>
<rectangle x1="9.03605" y1="14.59865" x2="13.06195" y2="14.61135" layer="21"/>
<rectangle x1="9.06145" y1="14.61135" x2="13.03655" y2="14.62405" layer="21"/>
<rectangle x1="9.08685" y1="14.62405" x2="13.01115" y2="14.63675" layer="21"/>
<rectangle x1="9.11225" y1="14.63675" x2="12.98575" y2="14.64945" layer="21"/>
<rectangle x1="9.13765" y1="14.64945" x2="12.96035" y2="14.66215" layer="21"/>
<rectangle x1="9.16305" y1="14.66215" x2="12.93495" y2="14.67485" layer="21"/>
<rectangle x1="9.18845" y1="14.67485" x2="12.90955" y2="14.68755" layer="21"/>
<rectangle x1="9.21385" y1="14.68755" x2="12.88415" y2="14.70025" layer="21"/>
<rectangle x1="9.23925" y1="14.70025" x2="12.85875" y2="14.71295" layer="21"/>
<rectangle x1="9.26465" y1="14.71295" x2="12.83335" y2="14.72565" layer="21"/>
<rectangle x1="9.29005" y1="14.72565" x2="12.80795" y2="14.73835" layer="21"/>
<rectangle x1="9.31545" y1="14.73835" x2="12.78255" y2="14.75105" layer="21"/>
<rectangle x1="9.34085" y1="14.75105" x2="12.75715" y2="14.76375" layer="21"/>
<rectangle x1="9.36625" y1="14.76375" x2="12.73175" y2="14.77645" layer="21"/>
<rectangle x1="9.39165" y1="14.77645" x2="12.70635" y2="14.78915" layer="21"/>
<rectangle x1="9.41705" y1="14.78915" x2="12.68095" y2="14.80185" layer="21"/>
<rectangle x1="9.44245" y1="14.80185" x2="12.65555" y2="14.81455" layer="21"/>
<rectangle x1="9.46785" y1="14.81455" x2="12.63015" y2="14.82725" layer="21"/>
<rectangle x1="9.49325" y1="14.82725" x2="12.60475" y2="14.83995" layer="21"/>
<rectangle x1="9.51865" y1="14.83995" x2="12.57935" y2="14.85265" layer="21"/>
<rectangle x1="9.54405" y1="14.85265" x2="12.55395" y2="14.86535" layer="21"/>
<rectangle x1="9.56945" y1="14.86535" x2="12.52855" y2="14.87805" layer="21"/>
<rectangle x1="9.59485" y1="14.87805" x2="12.50315" y2="14.89075" layer="21"/>
<rectangle x1="9.62025" y1="14.89075" x2="12.49045" y2="14.90345" layer="21"/>
<rectangle x1="9.64565" y1="14.90345" x2="12.46505" y2="14.91615" layer="21"/>
<rectangle x1="9.67105" y1="14.91615" x2="12.42695" y2="14.92885" layer="21"/>
<rectangle x1="9.69645" y1="14.92885" x2="12.40155" y2="14.94155" layer="21"/>
<rectangle x1="9.72185" y1="14.94155" x2="12.37615" y2="14.95425" layer="21"/>
<rectangle x1="9.74725" y1="14.95425" x2="12.35075" y2="14.96695" layer="21"/>
<rectangle x1="9.77265" y1="14.96695" x2="12.32535" y2="14.97965" layer="21"/>
<rectangle x1="9.79805" y1="14.97965" x2="12.29995" y2="14.99235" layer="21"/>
<rectangle x1="9.82345" y1="14.99235" x2="12.27455" y2="15.00505" layer="21"/>
<rectangle x1="9.84885" y1="15.00505" x2="12.24915" y2="15.01775" layer="21"/>
<rectangle x1="9.87425" y1="15.01775" x2="12.22375" y2="15.03045" layer="21"/>
<rectangle x1="9.89965" y1="15.03045" x2="12.19835" y2="15.04315" layer="21"/>
<rectangle x1="9.92505" y1="15.04315" x2="12.17295" y2="15.05585" layer="21"/>
<rectangle x1="9.95045" y1="15.05585" x2="12.14755" y2="15.06855" layer="21"/>
<rectangle x1="9.96315" y1="15.06855" x2="12.12215" y2="15.08125" layer="21"/>
<rectangle x1="10.00125" y1="15.08125" x2="12.09675" y2="15.09395" layer="21"/>
<rectangle x1="10.02665" y1="15.09395" x2="12.07135" y2="15.10665" layer="21"/>
<rectangle x1="10.05205" y1="15.10665" x2="12.04595" y2="15.11935" layer="21"/>
<rectangle x1="10.07745" y1="15.11935" x2="12.02055" y2="15.13205" layer="21"/>
<rectangle x1="10.10285" y1="15.13205" x2="11.99515" y2="15.14475" layer="21"/>
<rectangle x1="10.12825" y1="15.14475" x2="11.96975" y2="15.15745" layer="21"/>
<rectangle x1="10.15365" y1="15.15745" x2="11.94435" y2="15.17015" layer="21"/>
<rectangle x1="10.17905" y1="15.17015" x2="11.91895" y2="15.18285" layer="21"/>
<rectangle x1="10.20445" y1="15.18285" x2="11.89355" y2="15.19555" layer="21"/>
<rectangle x1="10.22985" y1="15.19555" x2="11.86815" y2="15.20825" layer="21"/>
<rectangle x1="10.25525" y1="15.20825" x2="11.84275" y2="15.22095" layer="21"/>
<rectangle x1="10.28065" y1="15.22095" x2="11.81735" y2="15.23365" layer="21"/>
<rectangle x1="10.30605" y1="15.23365" x2="11.79195" y2="15.24635" layer="21"/>
<rectangle x1="10.33145" y1="15.24635" x2="11.76655" y2="15.25905" layer="21"/>
<rectangle x1="10.35685" y1="15.25905" x2="11.74115" y2="15.27175" layer="21"/>
<rectangle x1="10.38225" y1="15.27175" x2="11.72845" y2="15.28445" layer="21"/>
<rectangle x1="10.40765" y1="15.28445" x2="11.70305" y2="15.29715" layer="21"/>
<rectangle x1="10.43305" y1="15.29715" x2="11.67765" y2="15.30985" layer="21"/>
<rectangle x1="10.45845" y1="15.30985" x2="11.65225" y2="15.32255" layer="21"/>
<rectangle x1="10.48385" y1="15.32255" x2="11.62685" y2="15.33525" layer="21"/>
<rectangle x1="10.50925" y1="15.33525" x2="11.60145" y2="15.34795" layer="21"/>
<rectangle x1="10.53465" y1="15.34795" x2="11.57605" y2="15.36065" layer="21"/>
<rectangle x1="10.56005" y1="15.36065" x2="11.55065" y2="15.37335" layer="21"/>
<rectangle x1="10.58545" y1="15.37335" x2="11.52525" y2="15.38605" layer="21"/>
<rectangle x1="10.61085" y1="15.38605" x2="11.49985" y2="15.39875" layer="21"/>
<rectangle x1="10.63625" y1="15.39875" x2="11.47445" y2="15.41145" layer="21"/>
<rectangle x1="10.66165" y1="15.41145" x2="11.44905" y2="15.42415" layer="21"/>
<rectangle x1="10.68705" y1="15.42415" x2="11.42365" y2="15.43685" layer="21"/>
<rectangle x1="10.71245" y1="15.43685" x2="11.39825" y2="15.44955" layer="21"/>
<rectangle x1="10.73785" y1="15.44955" x2="11.37285" y2="15.46225" layer="21"/>
<rectangle x1="10.76325" y1="15.46225" x2="11.34745" y2="15.47495" layer="21"/>
<rectangle x1="10.78865" y1="15.47495" x2="11.32205" y2="15.48765" layer="21"/>
<rectangle x1="10.81405" y1="15.48765" x2="11.29665" y2="15.50035" layer="21"/>
<rectangle x1="10.83945" y1="15.50035" x2="11.27125" y2="15.51305" layer="21"/>
<rectangle x1="10.86485" y1="15.51305" x2="11.24585" y2="15.52575" layer="21"/>
<rectangle x1="10.89025" y1="15.52575" x2="11.22045" y2="15.53845" layer="21"/>
<rectangle x1="10.91565" y1="15.53845" x2="11.19505" y2="15.55115" layer="21"/>
<rectangle x1="10.94105" y1="15.55115" x2="11.16965" y2="15.56385" layer="21"/>
<rectangle x1="10.96645" y1="15.56385" x2="11.14425" y2="15.57655" layer="21"/>
<rectangle x1="10.99185" y1="15.57655" x2="11.11885" y2="15.58925" layer="21"/>
<rectangle x1="11.01725" y1="15.58925" x2="11.09345" y2="15.60195" layer="21"/>
<rectangle x1="11.04265" y1="15.60195" x2="11.06805" y2="15.61465" layer="21"/>
<text x="0" y="-0.0635" size="0.0254" layer="21" font="vector">C:/Users/JR/Desktop/AIXLogo8Step1.5.bmp</text>
</package>
</packages>
<symbols>
<symbol name="AIXLOGO">
<wire x1="-10.16" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="2.54" size="1.778" layer="94">AiX-Logo</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AIXWARELOGO">
<gates>
<gate name="G$1" symbol="AIXLOGO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AIXLOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JST_AIX">
<packages>
<package name="JST4POSVER">
<smd name="P$1" x="-5.4" y="2.25" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P1" x="-3" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P2" x="-1" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P3" x="1" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P4" x="3" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P$6" x="5.4" y="2.25" dx="3" dy="1.6" layer="1" rot="R90"/>
<text x="-5.94" y="5.67" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.67" y="-6.21" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5.975" y1="4.75" x2="5.975" y2="4.75" width="0.127" layer="21"/>
<wire x1="5.975" y1="4.75" x2="5.975" y2="-0.25" width="0.127" layer="21"/>
<wire x1="5.975" y1="-0.25" x2="-5.975" y2="-0.25" width="0.127" layer="21"/>
<wire x1="-5.975" y1="-0.25" x2="-5.975" y2="4.75" width="0.127" layer="21"/>
</package>
<package name="JST2POSVER">
<smd name="P$1" x="-3.4" y="2.25" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P1" x="-1" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P2" x="1" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="P$6" x="3.4" y="2.25" dx="3" dy="1.6" layer="1" rot="R90"/>
<text x="-5.94" y="5.67" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.67" y="-6.21" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.975" y1="4.75" x2="3.975" y2="4.75" width="0.127" layer="21"/>
<wire x1="3.975" y1="4.75" x2="3.975" y2="-0.25" width="0.127" layer="21"/>
<wire x1="3.975" y1="-0.25" x2="-3.975" y2="-0.25" width="0.127" layer="21"/>
<wire x1="-3.975" y1="-0.25" x2="-3.975" y2="4.75" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="JST4POSVER">
<wire x1="-5.08" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<pin name="P1" x="-10.16" y="5.08" length="middle"/>
<pin name="P3" x="-10.16" y="0" length="middle"/>
<pin name="P2" x="-10.16" y="2.54" length="middle"/>
<pin name="P4" x="-10.16" y="-2.54" length="middle"/>
<text x="-5.08" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="JST2POSVER">
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<pin name="P1" x="-7.62" y="2.54" length="middle"/>
<pin name="P2" x="-7.62" y="-2.54" length="middle"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="JSTPOS4VERSMT">
<gates>
<gate name="G$1" symbol="JST4POSVER" x="5.08" y="5.08"/>
</gates>
<devices>
<device name="" package="JST4POSVER">
<connects>
<connect gate="G$1" pin="P1" pad="P1"/>
<connect gate="G$1" pin="P2" pad="P2"/>
<connect gate="G$1" pin="P3" pad="P3"/>
<connect gate="G$1" pin="P4" pad="P4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JSTPOS2VERSMT">
<gates>
<gate name="G$1" symbol="JST2POSVER" x="2.54" y="5.08"/>
</gates>
<devices>
<device name="" package="JST2POSVER">
<connects>
<connect gate="G$1" pin="P1" pad="P1"/>
<connect gate="G$1" pin="P2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2X05">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.35" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
</package>
<package name="1X04">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.1562" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINH2X5">
<wire x1="-6.35" y1="-7.62" x2="8.89" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-7.62" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PINHD4">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X5" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X05">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X4" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X04">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Conector_AIX">
<packages>
<package name="PJ-018H-SMT-TR">
<smd name="P1" x="-3" y="4.55" dx="2.8" dy="1.7" layer="1"/>
<smd name="P1.1" x="1.4" y="4.55" dx="2.8" dy="1.7" layer="1"/>
<smd name="P3" x="1.4" y="-4.1" dx="2.8" dy="2.6" layer="1"/>
<smd name="P2" x="-3" y="-4.1" dx="2.8" dy="2.6" layer="1"/>
<hole x="-5" y="0" drill="1"/>
<hole x="0" y="0" drill="1"/>
<wire x1="-6.35" y1="3.81" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="-6.35" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.81" x2="-6.35" y2="3.81" width="0.127" layer="21"/>
<text x="-7.62" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.62" y="-8.89" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="10118192-0001LF">
<smd name="P$1" x="-3.1" y="2.55" dx="2.1" dy="1.6" layer="1"/>
<smd name="VCC" x="-1.27" y="2.675" dx="1.35" dy="0.4" layer="1" rot="R90"/>
<smd name="D-" x="-0.635" y="2.675" dx="1.35" dy="0.4" layer="1" rot="R90"/>
<smd name="D+" x="0" y="2.675" dx="1.35" dy="0.4" layer="1" rot="R90"/>
<smd name="ID" x="0.635" y="2.675" dx="1.35" dy="0.4" layer="1" rot="R90"/>
<smd name="GND" x="1.27" y="2.675" dx="1.35" dy="0.4" layer="1" rot="R90"/>
<smd name="P$7" x="3.1" y="2.55" dx="2.1" dy="1.6" layer="1" rot="R180"/>
<smd name="P$8" x="3.81" y="0" dx="1.8" dy="1.9" layer="1" rot="R180"/>
<smd name="P$9" x="1.2" y="0" dx="1.8" dy="1.9" layer="1" rot="R180"/>
<smd name="P$10" x="-1.2" y="0" dx="1.8" dy="1.9" layer="1" rot="R180"/>
<smd name="P$11" x="-3.8" y="0" dx="1.8" dy="1.9" layer="1" rot="R180"/>
<wire x1="-6.3" y1="-1.45" x2="6.3" y2="-1.45" width="0.127" layer="21"/>
<text x="-3.81" y="-3.81" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="PJ-018H-SMT-TR">
<wire x1="-2.54" y1="5.08" x2="15.24" y2="5.08" width="0.254" layer="94"/>
<wire x1="15.24" y1="5.08" x2="15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<pin name="P1" x="-7.62" y="2.54" length="middle"/>
<pin name="P2" x="-7.62" y="-5.08" length="middle"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-12.7" size="1.27" layer="96">&gt;VALUE</text>
<pin name="P3" x="-7.62" y="0" length="middle"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="10118192-0001LF">
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<pin name="GND" x="10.16" y="-5.08" length="middle" rot="R180"/>
<pin name="ID" x="10.16" y="-2.54" length="middle" rot="R180"/>
<pin name="D+" x="10.16" y="0" length="middle" rot="R180"/>
<pin name="D-" x="10.16" y="2.54" length="middle" rot="R180"/>
<pin name="VCC" x="10.16" y="5.08" length="middle" rot="R180"/>
<text x="-5.334" y="8.382" size="1.778" layer="94">MICRO USB</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PJ-018H-SMT-TR">
<gates>
<gate name="G$1" symbol="PJ-018H-SMT-TR" x="2.54" y="5.08"/>
</gates>
<devices>
<device name="" package="PJ-018H-SMT-TR">
<connects>
<connect gate="G$1" pin="P1" pad="P1 P1.1"/>
<connect gate="G$1" pin="P2" pad="P2"/>
<connect gate="G$1" pin="P3" pad="P3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="10118192-0001LF">
<gates>
<gate name="G$1" symbol="10118192-0001LF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="10118192-0001LF">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="ID" pad="ID"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Misc_AIX">
<packages>
<package name="LOGOTECHMAKE">
<rectangle x1="3.1877" y1="0.4445" x2="3.7973" y2="0.4699" layer="21"/>
<rectangle x1="3.1877" y1="0.4699" x2="3.7973" y2="0.4953" layer="21"/>
<rectangle x1="3.1877" y1="0.4953" x2="3.7973" y2="0.5207" layer="21"/>
<rectangle x1="3.1877" y1="0.5207" x2="3.7973" y2="0.5461" layer="21"/>
<rectangle x1="3.1877" y1="0.5461" x2="3.7973" y2="0.5715" layer="21"/>
<rectangle x1="3.1877" y1="0.5715" x2="3.7973" y2="0.5969" layer="21"/>
<rectangle x1="3.1877" y1="0.5969" x2="3.7973" y2="0.6223" layer="21"/>
<rectangle x1="3.1877" y1="0.6223" x2="3.7973" y2="0.6477" layer="21"/>
<rectangle x1="3.1877" y1="0.6477" x2="3.7973" y2="0.6731" layer="21"/>
<rectangle x1="3.1877" y1="0.6731" x2="3.7973" y2="0.6985" layer="21"/>
<rectangle x1="3.1877" y1="0.6985" x2="3.7973" y2="0.7239" layer="21"/>
<rectangle x1="3.1877" y1="0.7239" x2="3.7973" y2="0.7493" layer="21"/>
<rectangle x1="3.1877" y1="0.7493" x2="3.7973" y2="0.7747" layer="21"/>
<rectangle x1="3.1877" y1="0.7747" x2="3.7973" y2="0.8001" layer="21"/>
<rectangle x1="3.1877" y1="0.8001" x2="3.7973" y2="0.8255" layer="21"/>
<rectangle x1="3.1877" y1="0.8255" x2="3.7973" y2="0.8509" layer="21"/>
<rectangle x1="3.1877" y1="0.8509" x2="3.7973" y2="0.8763" layer="21"/>
<rectangle x1="3.1877" y1="0.8763" x2="3.7973" y2="0.9017" layer="21"/>
<rectangle x1="3.1877" y1="0.9017" x2="3.7973" y2="0.9271" layer="21"/>
<rectangle x1="3.1877" y1="0.9271" x2="3.7973" y2="0.9525" layer="21"/>
<rectangle x1="3.1877" y1="0.9525" x2="3.7973" y2="0.9779" layer="21"/>
<rectangle x1="3.1877" y1="0.9779" x2="3.7973" y2="1.0033" layer="21"/>
<rectangle x1="3.1877" y1="1.0033" x2="3.7973" y2="1.0287" layer="21"/>
<rectangle x1="3.1877" y1="1.0287" x2="3.7973" y2="1.0541" layer="21"/>
<rectangle x1="3.1877" y1="1.0541" x2="3.7973" y2="1.0795" layer="21"/>
<rectangle x1="3.1877" y1="1.0795" x2="3.7973" y2="1.1049" layer="21"/>
<rectangle x1="3.1877" y1="1.1049" x2="3.7973" y2="1.1303" layer="21"/>
<rectangle x1="3.1877" y1="1.1303" x2="3.7973" y2="1.1557" layer="21"/>
<rectangle x1="3.1877" y1="1.1557" x2="3.7973" y2="1.1811" layer="21"/>
<rectangle x1="1.7907" y1="1.1811" x2="2.4257" y2="1.2065" layer="21"/>
<rectangle x1="3.1877" y1="1.1811" x2="3.7973" y2="1.2065" layer="21"/>
<rectangle x1="6.0325" y1="1.1811" x2="6.6421" y2="1.2065" layer="21"/>
<rectangle x1="1.7907" y1="1.2065" x2="2.4257" y2="1.2319" layer="21"/>
<rectangle x1="3.1877" y1="1.2065" x2="3.7973" y2="1.2319" layer="21"/>
<rectangle x1="6.0325" y1="1.2065" x2="6.6421" y2="1.2319" layer="21"/>
<rectangle x1="1.7907" y1="1.2319" x2="2.4257" y2="1.2573" layer="21"/>
<rectangle x1="3.1877" y1="1.2319" x2="3.7973" y2="1.2573" layer="21"/>
<rectangle x1="6.0325" y1="1.2319" x2="6.6421" y2="1.2573" layer="21"/>
<rectangle x1="1.7907" y1="1.2573" x2="2.4257" y2="1.2827" layer="21"/>
<rectangle x1="3.1877" y1="1.2573" x2="3.7973" y2="1.2827" layer="21"/>
<rectangle x1="6.0325" y1="1.2573" x2="6.6421" y2="1.2827" layer="21"/>
<rectangle x1="1.7907" y1="1.2827" x2="2.4257" y2="1.3081" layer="21"/>
<rectangle x1="3.1877" y1="1.2827" x2="3.7973" y2="1.3081" layer="21"/>
<rectangle x1="6.0325" y1="1.2827" x2="6.6421" y2="1.3081" layer="21"/>
<rectangle x1="1.7907" y1="1.3081" x2="2.4257" y2="1.3335" layer="21"/>
<rectangle x1="3.1877" y1="1.3081" x2="3.7973" y2="1.3335" layer="21"/>
<rectangle x1="6.0325" y1="1.3081" x2="6.6421" y2="1.3335" layer="21"/>
<rectangle x1="1.7907" y1="1.3335" x2="2.4257" y2="1.3589" layer="21"/>
<rectangle x1="3.1877" y1="1.3335" x2="3.7973" y2="1.3589" layer="21"/>
<rectangle x1="6.0325" y1="1.3335" x2="6.6421" y2="1.3589" layer="21"/>
<rectangle x1="1.7907" y1="1.3589" x2="2.4257" y2="1.3843" layer="21"/>
<rectangle x1="3.1877" y1="1.3589" x2="3.7973" y2="1.3843" layer="21"/>
<rectangle x1="6.0325" y1="1.3589" x2="6.6421" y2="1.3843" layer="21"/>
<rectangle x1="1.7907" y1="1.3843" x2="2.4257" y2="1.4097" layer="21"/>
<rectangle x1="3.1877" y1="1.3843" x2="3.7973" y2="1.4097" layer="21"/>
<rectangle x1="6.0325" y1="1.3843" x2="6.6421" y2="1.4097" layer="21"/>
<rectangle x1="1.7907" y1="1.4097" x2="2.4257" y2="1.4351" layer="21"/>
<rectangle x1="3.1877" y1="1.4097" x2="3.7973" y2="1.4351" layer="21"/>
<rectangle x1="6.0325" y1="1.4097" x2="6.6421" y2="1.4351" layer="21"/>
<rectangle x1="1.7907" y1="1.4351" x2="2.4257" y2="1.4605" layer="21"/>
<rectangle x1="3.1877" y1="1.4351" x2="3.7973" y2="1.4605" layer="21"/>
<rectangle x1="6.0325" y1="1.4351" x2="6.6421" y2="1.4605" layer="21"/>
<rectangle x1="1.7907" y1="1.4605" x2="2.4257" y2="1.4859" layer="21"/>
<rectangle x1="3.1877" y1="1.4605" x2="3.7973" y2="1.4859" layer="21"/>
<rectangle x1="6.0325" y1="1.4605" x2="6.6421" y2="1.4859" layer="21"/>
<rectangle x1="1.7907" y1="1.4859" x2="2.4257" y2="1.5113" layer="21"/>
<rectangle x1="3.1877" y1="1.4859" x2="3.7973" y2="1.5113" layer="21"/>
<rectangle x1="6.0325" y1="1.4859" x2="6.6421" y2="1.5113" layer="21"/>
<rectangle x1="1.7907" y1="1.5113" x2="2.4257" y2="1.5367" layer="21"/>
<rectangle x1="3.1877" y1="1.5113" x2="3.7973" y2="1.5367" layer="21"/>
<rectangle x1="6.0325" y1="1.5113" x2="6.6421" y2="1.5367" layer="21"/>
<rectangle x1="1.7907" y1="1.5367" x2="2.4257" y2="1.5621" layer="21"/>
<rectangle x1="3.1877" y1="1.5367" x2="3.7973" y2="1.5621" layer="21"/>
<rectangle x1="6.0325" y1="1.5367" x2="6.6421" y2="1.5621" layer="21"/>
<rectangle x1="1.7907" y1="1.5621" x2="2.4257" y2="1.5875" layer="21"/>
<rectangle x1="3.1877" y1="1.5621" x2="3.7973" y2="1.5875" layer="21"/>
<rectangle x1="6.0325" y1="1.5621" x2="6.6421" y2="1.5875" layer="21"/>
<rectangle x1="1.7907" y1="1.5875" x2="2.4257" y2="1.6129" layer="21"/>
<rectangle x1="3.1877" y1="1.5875" x2="3.7973" y2="1.6129" layer="21"/>
<rectangle x1="6.0325" y1="1.5875" x2="6.6421" y2="1.6129" layer="21"/>
<rectangle x1="1.7907" y1="1.6129" x2="2.4257" y2="1.6383" layer="21"/>
<rectangle x1="3.1877" y1="1.6129" x2="3.7973" y2="1.6383" layer="21"/>
<rectangle x1="6.0325" y1="1.6129" x2="6.6421" y2="1.6383" layer="21"/>
<rectangle x1="1.7907" y1="1.6383" x2="2.4257" y2="1.6637" layer="21"/>
<rectangle x1="3.1877" y1="1.6383" x2="3.7973" y2="1.6637" layer="21"/>
<rectangle x1="6.0325" y1="1.6383" x2="6.6421" y2="1.6637" layer="21"/>
<rectangle x1="1.7907" y1="1.6637" x2="2.4257" y2="1.6891" layer="21"/>
<rectangle x1="3.1877" y1="1.6637" x2="3.7973" y2="1.6891" layer="21"/>
<rectangle x1="6.0325" y1="1.6637" x2="6.6421" y2="1.6891" layer="21"/>
<rectangle x1="1.7907" y1="1.6891" x2="2.4257" y2="1.7145" layer="21"/>
<rectangle x1="3.1877" y1="1.6891" x2="3.7973" y2="1.7145" layer="21"/>
<rectangle x1="6.0325" y1="1.6891" x2="6.6421" y2="1.7145" layer="21"/>
<rectangle x1="1.7907" y1="1.7145" x2="2.4257" y2="1.7399" layer="21"/>
<rectangle x1="3.1877" y1="1.7145" x2="3.7973" y2="1.7399" layer="21"/>
<rectangle x1="6.0325" y1="1.7145" x2="6.6421" y2="1.7399" layer="21"/>
<rectangle x1="1.7907" y1="1.7399" x2="2.4257" y2="1.7653" layer="21"/>
<rectangle x1="3.1877" y1="1.7399" x2="3.7973" y2="1.7653" layer="21"/>
<rectangle x1="4.6355" y1="1.7399" x2="5.2451" y2="1.7653" layer="21"/>
<rectangle x1="6.0325" y1="1.7399" x2="6.6421" y2="1.7653" layer="21"/>
<rectangle x1="1.7907" y1="1.7653" x2="2.4257" y2="1.7907" layer="21"/>
<rectangle x1="3.1877" y1="1.7653" x2="3.7973" y2="1.7907" layer="21"/>
<rectangle x1="4.6355" y1="1.7653" x2="5.2451" y2="1.7907" layer="21"/>
<rectangle x1="6.0325" y1="1.7653" x2="6.6421" y2="1.7907" layer="21"/>
<rectangle x1="11.9507" y1="1.7653" x2="12.1793" y2="1.7907" layer="21"/>
<rectangle x1="13.5509" y1="1.7653" x2="13.5763" y2="1.7907" layer="21"/>
<rectangle x1="13.9573" y1="1.7653" x2="14.1859" y2="1.7907" layer="21"/>
<rectangle x1="15.1257" y1="1.7653" x2="15.1511" y2="1.7907" layer="21"/>
<rectangle x1="15.8115" y1="1.7653" x2="16.0401" y2="1.7907" layer="21"/>
<rectangle x1="16.3957" y1="1.7653" x2="16.5989" y2="1.7907" layer="21"/>
<rectangle x1="1.7907" y1="1.7907" x2="2.4257" y2="1.8161" layer="21"/>
<rectangle x1="3.1877" y1="1.7907" x2="3.7973" y2="1.8161" layer="21"/>
<rectangle x1="4.6355" y1="1.7907" x2="5.2451" y2="1.8161" layer="21"/>
<rectangle x1="6.0325" y1="1.7907" x2="6.6421" y2="1.8161" layer="21"/>
<rectangle x1="9.8933" y1="1.7907" x2="10.3759" y2="1.8161" layer="21"/>
<rectangle x1="10.5029" y1="1.7907" x2="10.9855" y2="1.8161" layer="21"/>
<rectangle x1="11.0871" y1="1.7907" x2="11.5951" y2="1.8161" layer="21"/>
<rectangle x1="11.8999" y1="1.7907" x2="12.2809" y2="1.8161" layer="21"/>
<rectangle x1="12.6111" y1="1.7907" x2="12.6873" y2="1.8161" layer="21"/>
<rectangle x1="13.0429" y1="1.7907" x2="13.1191" y2="1.8161" layer="21"/>
<rectangle x1="13.5255" y1="1.7907" x2="13.6017" y2="1.8161" layer="21"/>
<rectangle x1="13.9065" y1="1.7907" x2="14.2621" y2="1.8161" layer="21"/>
<rectangle x1="14.5923" y1="1.7907" x2="14.6685" y2="1.8161" layer="21"/>
<rectangle x1="15.1003" y1="1.7907" x2="15.1765" y2="1.8161" layer="21"/>
<rectangle x1="15.3543" y1="1.7907" x2="15.4051" y2="1.8161" layer="21"/>
<rectangle x1="15.7353" y1="1.7907" x2="16.1417" y2="1.8161" layer="21"/>
<rectangle x1="16.2941" y1="1.7907" x2="16.6751" y2="1.8161" layer="21"/>
<rectangle x1="1.7907" y1="1.8161" x2="2.4257" y2="1.8415" layer="21"/>
<rectangle x1="3.1877" y1="1.8161" x2="3.7973" y2="1.8415" layer="21"/>
<rectangle x1="4.6355" y1="1.8161" x2="5.2451" y2="1.8415" layer="21"/>
<rectangle x1="6.0325" y1="1.8161" x2="6.6421" y2="1.8415" layer="21"/>
<rectangle x1="9.8933" y1="1.8161" x2="10.3759" y2="1.8415" layer="21"/>
<rectangle x1="10.5029" y1="1.8161" x2="10.9855" y2="1.8415" layer="21"/>
<rectangle x1="11.0871" y1="1.8161" x2="11.5951" y2="1.8415" layer="21"/>
<rectangle x1="11.8491" y1="1.8161" x2="12.0015" y2="1.8415" layer="21"/>
<rectangle x1="12.1285" y1="1.8161" x2="12.2809" y2="1.8415" layer="21"/>
<rectangle x1="12.6111" y1="1.8161" x2="12.6873" y2="1.8415" layer="21"/>
<rectangle x1="13.0429" y1="1.8161" x2="13.1191" y2="1.8415" layer="21"/>
<rectangle x1="13.5001" y1="1.8161" x2="13.5763" y2="1.8415" layer="21"/>
<rectangle x1="13.8557" y1="1.8161" x2="14.0081" y2="1.8415" layer="21"/>
<rectangle x1="14.1605" y1="1.8161" x2="14.3129" y2="1.8415" layer="21"/>
<rectangle x1="14.5923" y1="1.8161" x2="14.6685" y2="1.8415" layer="21"/>
<rectangle x1="15.0749" y1="1.8161" x2="15.1765" y2="1.8415" layer="21"/>
<rectangle x1="15.3543" y1="1.8161" x2="15.4051" y2="1.8415" layer="21"/>
<rectangle x1="15.7099" y1="1.8161" x2="15.8623" y2="1.8415" layer="21"/>
<rectangle x1="15.9893" y1="1.8161" x2="16.1417" y2="1.8415" layer="21"/>
<rectangle x1="16.2687" y1="1.8161" x2="16.4211" y2="1.8415" layer="21"/>
<rectangle x1="16.5735" y1="1.8161" x2="16.7005" y2="1.8415" layer="21"/>
<rectangle x1="1.7907" y1="1.8415" x2="2.4257" y2="1.8669" layer="21"/>
<rectangle x1="3.1877" y1="1.8415" x2="3.7973" y2="1.8669" layer="21"/>
<rectangle x1="4.6355" y1="1.8415" x2="5.2451" y2="1.8669" layer="21"/>
<rectangle x1="6.0325" y1="1.8415" x2="6.6421" y2="1.8669" layer="21"/>
<rectangle x1="9.8933" y1="1.8415" x2="9.9441" y2="1.8669" layer="21"/>
<rectangle x1="10.5029" y1="1.8415" x2="10.5791" y2="1.8669" layer="21"/>
<rectangle x1="11.0871" y1="1.8415" x2="11.1633" y2="1.8669" layer="21"/>
<rectangle x1="11.8237" y1="1.8415" x2="11.9253" y2="1.8669" layer="21"/>
<rectangle x1="12.2301" y1="1.8415" x2="12.2809" y2="1.8669" layer="21"/>
<rectangle x1="12.6111" y1="1.8415" x2="12.6873" y2="1.8669" layer="21"/>
<rectangle x1="13.0429" y1="1.8415" x2="13.1191" y2="1.8669" layer="21"/>
<rectangle x1="13.5001" y1="1.8415" x2="13.5763" y2="1.8669" layer="21"/>
<rectangle x1="13.8303" y1="1.8415" x2="13.9319" y2="1.8669" layer="21"/>
<rectangle x1="14.2367" y1="1.8415" x2="14.3383" y2="1.8669" layer="21"/>
<rectangle x1="14.5923" y1="1.8415" x2="14.6685" y2="1.8669" layer="21"/>
<rectangle x1="15.0749" y1="1.8415" x2="15.1765" y2="1.8669" layer="21"/>
<rectangle x1="15.3543" y1="1.8415" x2="15.4051" y2="1.8669" layer="21"/>
<rectangle x1="15.6845" y1="1.8415" x2="15.7861" y2="1.8669" layer="21"/>
<rectangle x1="16.0909" y1="1.8415" x2="16.1417" y2="1.8669" layer="21"/>
<rectangle x1="16.2941" y1="1.8415" x2="16.3449" y2="1.8669" layer="21"/>
<rectangle x1="16.6243" y1="1.8415" x2="16.7259" y2="1.8669" layer="21"/>
<rectangle x1="1.7907" y1="1.8669" x2="2.4257" y2="1.8923" layer="21"/>
<rectangle x1="3.1877" y1="1.8669" x2="3.7973" y2="1.8923" layer="21"/>
<rectangle x1="4.6355" y1="1.8669" x2="5.2451" y2="1.8923" layer="21"/>
<rectangle x1="6.0325" y1="1.8669" x2="6.6421" y2="1.8923" layer="21"/>
<rectangle x1="9.8933" y1="1.8669" x2="9.9441" y2="1.8923" layer="21"/>
<rectangle x1="10.5029" y1="1.8669" x2="10.5791" y2="1.8923" layer="21"/>
<rectangle x1="11.0871" y1="1.8669" x2="11.1633" y2="1.8923" layer="21"/>
<rectangle x1="11.7983" y1="1.8669" x2="11.8999" y2="1.8923" layer="21"/>
<rectangle x1="12.6111" y1="1.8669" x2="12.6873" y2="1.8923" layer="21"/>
<rectangle x1="13.0429" y1="1.8669" x2="13.1191" y2="1.8923" layer="21"/>
<rectangle x1="13.4747" y1="1.8669" x2="13.5509" y2="1.8923" layer="21"/>
<rectangle x1="13.8049" y1="1.8669" x2="13.9065" y2="1.8923" layer="21"/>
<rectangle x1="14.2621" y1="1.8669" x2="14.3637" y2="1.8923" layer="21"/>
<rectangle x1="14.5923" y1="1.8669" x2="14.6685" y2="1.8923" layer="21"/>
<rectangle x1="15.0495" y1="1.8669" x2="15.1765" y2="1.8923" layer="21"/>
<rectangle x1="15.3543" y1="1.8669" x2="15.4051" y2="1.8923" layer="21"/>
<rectangle x1="15.6591" y1="1.8669" x2="15.7353" y2="1.8923" layer="21"/>
<rectangle x1="16.6497" y1="1.8669" x2="16.7513" y2="1.8923" layer="21"/>
<rectangle x1="1.7907" y1="1.8923" x2="2.4257" y2="1.9177" layer="21"/>
<rectangle x1="3.1877" y1="1.8923" x2="3.7973" y2="1.9177" layer="21"/>
<rectangle x1="4.6355" y1="1.8923" x2="5.2451" y2="1.9177" layer="21"/>
<rectangle x1="6.0325" y1="1.8923" x2="6.6421" y2="1.9177" layer="21"/>
<rectangle x1="9.8933" y1="1.8923" x2="9.9441" y2="1.9177" layer="21"/>
<rectangle x1="10.5029" y1="1.8923" x2="10.5791" y2="1.9177" layer="21"/>
<rectangle x1="11.0871" y1="1.8923" x2="11.1633" y2="1.9177" layer="21"/>
<rectangle x1="11.7729" y1="1.8923" x2="11.8745" y2="1.9177" layer="21"/>
<rectangle x1="12.6111" y1="1.8923" x2="12.6873" y2="1.9177" layer="21"/>
<rectangle x1="13.0429" y1="1.8923" x2="13.1191" y2="1.9177" layer="21"/>
<rectangle x1="13.4493" y1="1.8923" x2="13.5255" y2="1.9177" layer="21"/>
<rectangle x1="13.7795" y1="1.8923" x2="13.8811" y2="1.9177" layer="21"/>
<rectangle x1="14.2875" y1="1.8923" x2="14.3891" y2="1.9177" layer="21"/>
<rectangle x1="14.5923" y1="1.8923" x2="14.6685" y2="1.9177" layer="21"/>
<rectangle x1="15.0495" y1="1.8923" x2="15.1765" y2="1.9177" layer="21"/>
<rectangle x1="15.3543" y1="1.8923" x2="15.4051" y2="1.9177" layer="21"/>
<rectangle x1="15.6337" y1="1.8923" x2="15.7099" y2="1.9177" layer="21"/>
<rectangle x1="16.6751" y1="1.8923" x2="16.7513" y2="1.9177" layer="21"/>
<rectangle x1="1.7907" y1="1.9177" x2="2.4257" y2="1.9431" layer="21"/>
<rectangle x1="3.1877" y1="1.9177" x2="3.7973" y2="1.9431" layer="21"/>
<rectangle x1="4.6355" y1="1.9177" x2="5.2451" y2="1.9431" layer="21"/>
<rectangle x1="6.0325" y1="1.9177" x2="6.6421" y2="1.9431" layer="21"/>
<rectangle x1="9.8933" y1="1.9177" x2="9.9441" y2="1.9431" layer="21"/>
<rectangle x1="10.5029" y1="1.9177" x2="10.5791" y2="1.9431" layer="21"/>
<rectangle x1="11.0871" y1="1.9177" x2="11.1633" y2="1.9431" layer="21"/>
<rectangle x1="11.7729" y1="1.9177" x2="11.8491" y2="1.9431" layer="21"/>
<rectangle x1="12.6111" y1="1.9177" x2="12.6873" y2="1.9431" layer="21"/>
<rectangle x1="13.0429" y1="1.9177" x2="13.1191" y2="1.9431" layer="21"/>
<rectangle x1="13.4493" y1="1.9177" x2="13.5255" y2="1.9431" layer="21"/>
<rectangle x1="13.7795" y1="1.9177" x2="13.8557" y2="1.9431" layer="21"/>
<rectangle x1="14.3129" y1="1.9177" x2="14.3891" y2="1.9431" layer="21"/>
<rectangle x1="14.5923" y1="1.9177" x2="14.6685" y2="1.9431" layer="21"/>
<rectangle x1="15.0241" y1="1.9177" x2="15.1003" y2="1.9431" layer="21"/>
<rectangle x1="15.1257" y1="1.9177" x2="15.1765" y2="1.9431" layer="21"/>
<rectangle x1="15.3543" y1="1.9177" x2="15.4051" y2="1.9431" layer="21"/>
<rectangle x1="15.6083" y1="1.9177" x2="15.6845" y2="1.9431" layer="21"/>
<rectangle x1="16.7005" y1="1.9177" x2="16.7513" y2="1.9431" layer="21"/>
<rectangle x1="1.7907" y1="1.9431" x2="2.4257" y2="1.9685" layer="21"/>
<rectangle x1="3.1877" y1="1.9431" x2="3.7973" y2="1.9685" layer="21"/>
<rectangle x1="4.6355" y1="1.9431" x2="5.2451" y2="1.9685" layer="21"/>
<rectangle x1="6.0325" y1="1.9431" x2="6.6421" y2="1.9685" layer="21"/>
<rectangle x1="9.8933" y1="1.9431" x2="9.9441" y2="1.9685" layer="21"/>
<rectangle x1="10.5029" y1="1.9431" x2="10.5791" y2="1.9685" layer="21"/>
<rectangle x1="11.0871" y1="1.9431" x2="11.1633" y2="1.9685" layer="21"/>
<rectangle x1="11.7475" y1="1.9431" x2="11.8237" y2="1.9685" layer="21"/>
<rectangle x1="12.6111" y1="1.9431" x2="12.6873" y2="1.9685" layer="21"/>
<rectangle x1="13.0429" y1="1.9431" x2="13.1191" y2="1.9685" layer="21"/>
<rectangle x1="13.4239" y1="1.9431" x2="13.5001" y2="1.9685" layer="21"/>
<rectangle x1="13.7541" y1="1.9431" x2="13.8303" y2="1.9685" layer="21"/>
<rectangle x1="14.3383" y1="1.9431" x2="14.4145" y2="1.9685" layer="21"/>
<rectangle x1="14.5923" y1="1.9431" x2="14.6685" y2="1.9685" layer="21"/>
<rectangle x1="14.9987" y1="1.9431" x2="15.0749" y2="1.9685" layer="21"/>
<rectangle x1="15.1257" y1="1.9431" x2="15.1765" y2="1.9685" layer="21"/>
<rectangle x1="15.3543" y1="1.9431" x2="15.4051" y2="1.9685" layer="21"/>
<rectangle x1="15.6083" y1="1.9431" x2="15.6845" y2="1.9685" layer="21"/>
<rectangle x1="16.7005" y1="1.9431" x2="16.7513" y2="1.9685" layer="21"/>
<rectangle x1="1.7907" y1="1.9685" x2="2.4257" y2="1.9939" layer="21"/>
<rectangle x1="3.1877" y1="1.9685" x2="3.7973" y2="1.9939" layer="21"/>
<rectangle x1="4.6355" y1="1.9685" x2="5.2451" y2="1.9939" layer="21"/>
<rectangle x1="6.0325" y1="1.9685" x2="6.6421" y2="1.9939" layer="21"/>
<rectangle x1="9.8933" y1="1.9685" x2="9.9441" y2="1.9939" layer="21"/>
<rectangle x1="10.5029" y1="1.9685" x2="10.5791" y2="1.9939" layer="21"/>
<rectangle x1="11.0871" y1="1.9685" x2="11.1633" y2="1.9939" layer="21"/>
<rectangle x1="11.7475" y1="1.9685" x2="11.8237" y2="1.9939" layer="21"/>
<rectangle x1="12.6111" y1="1.9685" x2="12.6873" y2="1.9939" layer="21"/>
<rectangle x1="13.0429" y1="1.9685" x2="13.1191" y2="1.9939" layer="21"/>
<rectangle x1="13.4239" y1="1.9685" x2="13.5001" y2="1.9939" layer="21"/>
<rectangle x1="13.7541" y1="1.9685" x2="13.8303" y2="1.9939" layer="21"/>
<rectangle x1="14.3383" y1="1.9685" x2="14.4145" y2="1.9939" layer="21"/>
<rectangle x1="14.5923" y1="1.9685" x2="14.6685" y2="1.9939" layer="21"/>
<rectangle x1="14.9987" y1="1.9685" x2="15.0749" y2="1.9939" layer="21"/>
<rectangle x1="15.1257" y1="1.9685" x2="15.1765" y2="1.9939" layer="21"/>
<rectangle x1="15.3543" y1="1.9685" x2="15.4051" y2="1.9939" layer="21"/>
<rectangle x1="15.5829" y1="1.9685" x2="15.6591" y2="1.9939" layer="21"/>
<rectangle x1="16.7005" y1="1.9685" x2="16.7767" y2="1.9939" layer="21"/>
<rectangle x1="1.7907" y1="1.9939" x2="2.4257" y2="2.0193" layer="21"/>
<rectangle x1="3.1877" y1="1.9939" x2="3.7973" y2="2.0193" layer="21"/>
<rectangle x1="4.6355" y1="1.9939" x2="5.2451" y2="2.0193" layer="21"/>
<rectangle x1="6.0325" y1="1.9939" x2="6.6421" y2="2.0193" layer="21"/>
<rectangle x1="9.8933" y1="1.9939" x2="9.9441" y2="2.0193" layer="21"/>
<rectangle x1="10.5029" y1="1.9939" x2="10.5791" y2="2.0193" layer="21"/>
<rectangle x1="11.0871" y1="1.9939" x2="11.1633" y2="2.0193" layer="21"/>
<rectangle x1="11.7221" y1="1.9939" x2="11.7983" y2="2.0193" layer="21"/>
<rectangle x1="12.6111" y1="1.9939" x2="12.6873" y2="2.0193" layer="21"/>
<rectangle x1="13.0429" y1="1.9939" x2="13.1191" y2="2.0193" layer="21"/>
<rectangle x1="13.3985" y1="1.9939" x2="13.4747" y2="2.0193" layer="21"/>
<rectangle x1="13.7541" y1="1.9939" x2="13.8049" y2="2.0193" layer="21"/>
<rectangle x1="14.3637" y1="1.9939" x2="14.4145" y2="2.0193" layer="21"/>
<rectangle x1="14.5923" y1="1.9939" x2="14.6685" y2="2.0193" layer="21"/>
<rectangle x1="14.9733" y1="1.9939" x2="15.0495" y2="2.0193" layer="21"/>
<rectangle x1="15.1257" y1="1.9939" x2="15.1765" y2="2.0193" layer="21"/>
<rectangle x1="15.3543" y1="1.9939" x2="15.4051" y2="2.0193" layer="21"/>
<rectangle x1="15.5829" y1="1.9939" x2="15.6591" y2="2.0193" layer="21"/>
<rectangle x1="16.7005" y1="1.9939" x2="16.7513" y2="2.0193" layer="21"/>
<rectangle x1="1.7907" y1="2.0193" x2="2.4257" y2="2.0447" layer="21"/>
<rectangle x1="3.1877" y1="2.0193" x2="3.7973" y2="2.0447" layer="21"/>
<rectangle x1="4.6355" y1="2.0193" x2="5.2451" y2="2.0447" layer="21"/>
<rectangle x1="6.0325" y1="2.0193" x2="6.6421" y2="2.0447" layer="21"/>
<rectangle x1="9.8933" y1="2.0193" x2="9.9441" y2="2.0447" layer="21"/>
<rectangle x1="10.5029" y1="2.0193" x2="10.5791" y2="2.0447" layer="21"/>
<rectangle x1="11.0871" y1="2.0193" x2="11.1633" y2="2.0447" layer="21"/>
<rectangle x1="11.7221" y1="2.0193" x2="11.7983" y2="2.0447" layer="21"/>
<rectangle x1="12.6111" y1="2.0193" x2="12.6873" y2="2.0447" layer="21"/>
<rectangle x1="13.0429" y1="2.0193" x2="13.1191" y2="2.0447" layer="21"/>
<rectangle x1="13.3731" y1="2.0193" x2="13.4493" y2="2.0447" layer="21"/>
<rectangle x1="13.7287" y1="2.0193" x2="13.8049" y2="2.0447" layer="21"/>
<rectangle x1="14.3637" y1="2.0193" x2="14.4399" y2="2.0447" layer="21"/>
<rectangle x1="14.5923" y1="2.0193" x2="14.6685" y2="2.0447" layer="21"/>
<rectangle x1="14.9479" y1="2.0193" x2="15.0241" y2="2.0447" layer="21"/>
<rectangle x1="15.1257" y1="2.0193" x2="15.1765" y2="2.0447" layer="21"/>
<rectangle x1="15.3543" y1="2.0193" x2="15.4051" y2="2.0447" layer="21"/>
<rectangle x1="15.5829" y1="2.0193" x2="15.6337" y2="2.0447" layer="21"/>
<rectangle x1="16.7005" y1="2.0193" x2="16.7513" y2="2.0447" layer="21"/>
<rectangle x1="1.7907" y1="2.0447" x2="2.4257" y2="2.0701" layer="21"/>
<rectangle x1="3.1877" y1="2.0447" x2="3.7973" y2="2.0701" layer="21"/>
<rectangle x1="4.6355" y1="2.0447" x2="5.2451" y2="2.0701" layer="21"/>
<rectangle x1="6.0325" y1="2.0447" x2="6.6421" y2="2.0701" layer="21"/>
<rectangle x1="9.8933" y1="2.0447" x2="9.9441" y2="2.0701" layer="21"/>
<rectangle x1="10.5029" y1="2.0447" x2="10.5791" y2="2.0701" layer="21"/>
<rectangle x1="11.0871" y1="2.0447" x2="11.1633" y2="2.0701" layer="21"/>
<rectangle x1="11.7221" y1="2.0447" x2="11.7983" y2="2.0701" layer="21"/>
<rectangle x1="12.6111" y1="2.0447" x2="12.6873" y2="2.0701" layer="21"/>
<rectangle x1="13.0429" y1="2.0447" x2="13.1191" y2="2.0701" layer="21"/>
<rectangle x1="13.3731" y1="2.0447" x2="13.4493" y2="2.0701" layer="21"/>
<rectangle x1="13.7287" y1="2.0447" x2="13.8049" y2="2.0701" layer="21"/>
<rectangle x1="14.3637" y1="2.0447" x2="14.4399" y2="2.0701" layer="21"/>
<rectangle x1="14.5923" y1="2.0447" x2="14.6685" y2="2.0701" layer="21"/>
<rectangle x1="14.9479" y1="2.0447" x2="15.0241" y2="2.0701" layer="21"/>
<rectangle x1="15.1257" y1="2.0447" x2="15.1765" y2="2.0701" layer="21"/>
<rectangle x1="15.3543" y1="2.0447" x2="15.4051" y2="2.0701" layer="21"/>
<rectangle x1="15.5829" y1="2.0447" x2="15.6337" y2="2.0701" layer="21"/>
<rectangle x1="16.6751" y1="2.0447" x2="16.7513" y2="2.0701" layer="21"/>
<rectangle x1="1.7907" y1="2.0701" x2="2.4257" y2="2.0955" layer="21"/>
<rectangle x1="3.1877" y1="2.0701" x2="3.7973" y2="2.0955" layer="21"/>
<rectangle x1="4.6355" y1="2.0701" x2="5.2451" y2="2.0955" layer="21"/>
<rectangle x1="6.0325" y1="2.0701" x2="6.6421" y2="2.0955" layer="21"/>
<rectangle x1="9.8933" y1="2.0701" x2="9.9441" y2="2.0955" layer="21"/>
<rectangle x1="10.5029" y1="2.0701" x2="10.5791" y2="2.0955" layer="21"/>
<rectangle x1="11.0871" y1="2.0701" x2="11.1633" y2="2.0955" layer="21"/>
<rectangle x1="11.7221" y1="2.0701" x2="11.7729" y2="2.0955" layer="21"/>
<rectangle x1="12.6111" y1="2.0701" x2="12.6873" y2="2.0955" layer="21"/>
<rectangle x1="13.0429" y1="2.0701" x2="13.1191" y2="2.0955" layer="21"/>
<rectangle x1="13.3477" y1="2.0701" x2="13.4239" y2="2.0955" layer="21"/>
<rectangle x1="13.7287" y1="2.0701" x2="13.7795" y2="2.0955" layer="21"/>
<rectangle x1="14.3637" y1="2.0701" x2="14.4399" y2="2.0955" layer="21"/>
<rectangle x1="14.5923" y1="2.0701" x2="14.6685" y2="2.0955" layer="21"/>
<rectangle x1="14.9225" y1="2.0701" x2="14.9987" y2="2.0955" layer="21"/>
<rectangle x1="15.1257" y1="2.0701" x2="15.1765" y2="2.0955" layer="21"/>
<rectangle x1="15.3543" y1="2.0701" x2="15.4051" y2="2.0955" layer="21"/>
<rectangle x1="15.5829" y1="2.0701" x2="15.6337" y2="2.0955" layer="21"/>
<rectangle x1="16.6497" y1="2.0701" x2="16.7259" y2="2.0955" layer="21"/>
<rectangle x1="1.7907" y1="2.0955" x2="2.4257" y2="2.1209" layer="21"/>
<rectangle x1="3.1877" y1="2.0955" x2="3.7973" y2="2.1209" layer="21"/>
<rectangle x1="4.6355" y1="2.0955" x2="5.2451" y2="2.1209" layer="21"/>
<rectangle x1="6.0325" y1="2.0955" x2="6.6421" y2="2.1209" layer="21"/>
<rectangle x1="9.8933" y1="2.0955" x2="9.9441" y2="2.1209" layer="21"/>
<rectangle x1="10.5029" y1="2.0955" x2="10.5791" y2="2.1209" layer="21"/>
<rectangle x1="11.0871" y1="2.0955" x2="11.1633" y2="2.1209" layer="21"/>
<rectangle x1="11.7221" y1="2.0955" x2="11.7729" y2="2.1209" layer="21"/>
<rectangle x1="12.6111" y1="2.0955" x2="12.6873" y2="2.1209" layer="21"/>
<rectangle x1="13.0429" y1="2.0955" x2="13.3985" y2="2.1209" layer="21"/>
<rectangle x1="13.7287" y1="2.0955" x2="13.7795" y2="2.1209" layer="21"/>
<rectangle x1="14.3891" y1="2.0955" x2="14.4399" y2="2.1209" layer="21"/>
<rectangle x1="14.5923" y1="2.0955" x2="14.6685" y2="2.1209" layer="21"/>
<rectangle x1="14.8971" y1="2.0955" x2="14.9733" y2="2.1209" layer="21"/>
<rectangle x1="15.1257" y1="2.0955" x2="15.1765" y2="2.1209" layer="21"/>
<rectangle x1="15.3543" y1="2.0955" x2="15.4051" y2="2.1209" layer="21"/>
<rectangle x1="15.5575" y1="2.0955" x2="15.6337" y2="2.1209" layer="21"/>
<rectangle x1="16.6243" y1="2.0955" x2="16.7259" y2="2.1209" layer="21"/>
<rectangle x1="1.7907" y1="2.1209" x2="2.4257" y2="2.1463" layer="21"/>
<rectangle x1="3.1877" y1="2.1209" x2="3.7973" y2="2.1463" layer="21"/>
<rectangle x1="4.6355" y1="2.1209" x2="5.2451" y2="2.1463" layer="21"/>
<rectangle x1="6.0325" y1="2.1209" x2="6.6421" y2="2.1463" layer="21"/>
<rectangle x1="9.8933" y1="2.1209" x2="9.9441" y2="2.1463" layer="21"/>
<rectangle x1="10.5029" y1="2.1209" x2="10.5791" y2="2.1463" layer="21"/>
<rectangle x1="11.0871" y1="2.1209" x2="11.1633" y2="2.1463" layer="21"/>
<rectangle x1="11.7221" y1="2.1209" x2="11.7729" y2="2.1463" layer="21"/>
<rectangle x1="12.6111" y1="2.1209" x2="12.6873" y2="2.1463" layer="21"/>
<rectangle x1="13.0429" y1="2.1209" x2="13.4239" y2="2.1463" layer="21"/>
<rectangle x1="13.7287" y1="2.1209" x2="13.7795" y2="2.1463" layer="21"/>
<rectangle x1="14.3891" y1="2.1209" x2="14.4399" y2="2.1463" layer="21"/>
<rectangle x1="14.5923" y1="2.1209" x2="14.6685" y2="2.1463" layer="21"/>
<rectangle x1="14.8971" y1="2.1209" x2="14.9733" y2="2.1463" layer="21"/>
<rectangle x1="15.1257" y1="2.1209" x2="15.1765" y2="2.1463" layer="21"/>
<rectangle x1="15.3543" y1="2.1209" x2="15.4051" y2="2.1463" layer="21"/>
<rectangle x1="15.5575" y1="2.1209" x2="15.6337" y2="2.1463" layer="21"/>
<rectangle x1="16.5735" y1="2.1209" x2="16.7005" y2="2.1463" layer="21"/>
<rectangle x1="1.7907" y1="2.1463" x2="2.4257" y2="2.1717" layer="21"/>
<rectangle x1="3.1877" y1="2.1463" x2="3.7973" y2="2.1717" layer="21"/>
<rectangle x1="4.6355" y1="2.1463" x2="5.2451" y2="2.1717" layer="21"/>
<rectangle x1="6.0325" y1="2.1463" x2="6.6421" y2="2.1717" layer="21"/>
<rectangle x1="9.8933" y1="2.1463" x2="10.3251" y2="2.1717" layer="21"/>
<rectangle x1="10.5029" y1="2.1463" x2="10.5791" y2="2.1717" layer="21"/>
<rectangle x1="11.0871" y1="2.1463" x2="11.5189" y2="2.1717" layer="21"/>
<rectangle x1="11.7221" y1="2.1463" x2="11.7729" y2="2.1717" layer="21"/>
<rectangle x1="12.6111" y1="2.1463" x2="12.6873" y2="2.1717" layer="21"/>
<rectangle x1="13.0429" y1="2.1463" x2="13.1699" y2="2.1717" layer="21"/>
<rectangle x1="13.2969" y1="2.1463" x2="13.4747" y2="2.1717" layer="21"/>
<rectangle x1="13.7287" y1="2.1463" x2="13.7795" y2="2.1717" layer="21"/>
<rectangle x1="14.3891" y1="2.1463" x2="14.4399" y2="2.1717" layer="21"/>
<rectangle x1="14.5923" y1="2.1463" x2="14.6685" y2="2.1717" layer="21"/>
<rectangle x1="14.8717" y1="2.1463" x2="14.9479" y2="2.1717" layer="21"/>
<rectangle x1="15.1257" y1="2.1463" x2="15.1765" y2="2.1717" layer="21"/>
<rectangle x1="15.3543" y1="2.1463" x2="15.4051" y2="2.1717" layer="21"/>
<rectangle x1="15.5575" y1="2.1463" x2="15.6337" y2="2.1717" layer="21"/>
<rectangle x1="16.4973" y1="2.1463" x2="16.6497" y2="2.1717" layer="21"/>
<rectangle x1="1.7907" y1="2.1717" x2="2.4257" y2="2.1971" layer="21"/>
<rectangle x1="3.1877" y1="2.1717" x2="3.7973" y2="2.1971" layer="21"/>
<rectangle x1="4.6355" y1="2.1717" x2="5.2451" y2="2.1971" layer="21"/>
<rectangle x1="6.0325" y1="2.1717" x2="6.6421" y2="2.1971" layer="21"/>
<rectangle x1="9.8933" y1="2.1717" x2="10.3251" y2="2.1971" layer="21"/>
<rectangle x1="10.5029" y1="2.1717" x2="10.5791" y2="2.1971" layer="21"/>
<rectangle x1="11.0871" y1="2.1717" x2="11.5443" y2="2.1971" layer="21"/>
<rectangle x1="11.7221" y1="2.1717" x2="11.7729" y2="2.1971" layer="21"/>
<rectangle x1="12.6111" y1="2.1717" x2="12.6873" y2="2.1971" layer="21"/>
<rectangle x1="13.0429" y1="2.1717" x2="13.1191" y2="2.1971" layer="21"/>
<rectangle x1="13.3731" y1="2.1717" x2="13.5001" y2="2.1971" layer="21"/>
<rectangle x1="13.7287" y1="2.1717" x2="13.7795" y2="2.1971" layer="21"/>
<rectangle x1="14.3891" y1="2.1717" x2="14.4399" y2="2.1971" layer="21"/>
<rectangle x1="14.5923" y1="2.1717" x2="14.6685" y2="2.1971" layer="21"/>
<rectangle x1="14.8463" y1="2.1717" x2="14.9225" y2="2.1971" layer="21"/>
<rectangle x1="15.1257" y1="2.1717" x2="15.1765" y2="2.1971" layer="21"/>
<rectangle x1="15.3543" y1="2.1717" x2="15.4051" y2="2.1971" layer="21"/>
<rectangle x1="15.5575" y1="2.1717" x2="15.6337" y2="2.1971" layer="21"/>
<rectangle x1="16.4465" y1="2.1717" x2="16.5989" y2="2.1971" layer="21"/>
<rectangle x1="1.7907" y1="2.1971" x2="2.4257" y2="2.2225" layer="21"/>
<rectangle x1="3.1877" y1="2.1971" x2="3.7973" y2="2.2225" layer="21"/>
<rectangle x1="4.6355" y1="2.1971" x2="5.2451" y2="2.2225" layer="21"/>
<rectangle x1="6.0325" y1="2.1971" x2="6.6421" y2="2.2225" layer="21"/>
<rectangle x1="9.8933" y1="2.1971" x2="9.9695" y2="2.2225" layer="21"/>
<rectangle x1="10.5029" y1="2.1971" x2="10.5791" y2="2.2225" layer="21"/>
<rectangle x1="11.0871" y1="2.1971" x2="11.1633" y2="2.2225" layer="21"/>
<rectangle x1="11.7221" y1="2.1971" x2="11.7729" y2="2.2225" layer="21"/>
<rectangle x1="12.6111" y1="2.1971" x2="12.6873" y2="2.2225" layer="21"/>
<rectangle x1="13.0429" y1="2.1971" x2="13.1191" y2="2.2225" layer="21"/>
<rectangle x1="13.4239" y1="2.1971" x2="13.5255" y2="2.2225" layer="21"/>
<rectangle x1="13.7287" y1="2.1971" x2="13.7795" y2="2.2225" layer="21"/>
<rectangle x1="14.3891" y1="2.1971" x2="14.4399" y2="2.2225" layer="21"/>
<rectangle x1="14.5923" y1="2.1971" x2="14.6685" y2="2.2225" layer="21"/>
<rectangle x1="14.8463" y1="2.1971" x2="14.9225" y2="2.2225" layer="21"/>
<rectangle x1="15.1257" y1="2.1971" x2="15.1765" y2="2.2225" layer="21"/>
<rectangle x1="15.3543" y1="2.1971" x2="15.4051" y2="2.2225" layer="21"/>
<rectangle x1="15.5575" y1="2.1971" x2="15.6337" y2="2.2225" layer="21"/>
<rectangle x1="16.3957" y1="2.1971" x2="16.5481" y2="2.2225" layer="21"/>
<rectangle x1="1.7907" y1="2.2225" x2="2.4257" y2="2.2479" layer="21"/>
<rectangle x1="3.1877" y1="2.2225" x2="3.7973" y2="2.2479" layer="21"/>
<rectangle x1="4.6355" y1="2.2225" x2="5.2451" y2="2.2479" layer="21"/>
<rectangle x1="6.0325" y1="2.2225" x2="6.6421" y2="2.2479" layer="21"/>
<rectangle x1="9.8933" y1="2.2225" x2="9.9441" y2="2.2479" layer="21"/>
<rectangle x1="10.5029" y1="2.2225" x2="10.5791" y2="2.2479" layer="21"/>
<rectangle x1="11.0871" y1="2.2225" x2="11.1633" y2="2.2479" layer="21"/>
<rectangle x1="11.7221" y1="2.2225" x2="11.7729" y2="2.2479" layer="21"/>
<rectangle x1="12.6111" y1="2.2225" x2="12.6873" y2="2.2479" layer="21"/>
<rectangle x1="13.0429" y1="2.2225" x2="13.1191" y2="2.2479" layer="21"/>
<rectangle x1="13.4493" y1="2.2225" x2="13.5255" y2="2.2479" layer="21"/>
<rectangle x1="13.7287" y1="2.2225" x2="13.7795" y2="2.2479" layer="21"/>
<rectangle x1="14.3891" y1="2.2225" x2="14.4399" y2="2.2479" layer="21"/>
<rectangle x1="14.5923" y1="2.2225" x2="14.6685" y2="2.2479" layer="21"/>
<rectangle x1="14.8209" y1="2.2225" x2="14.8971" y2="2.2479" layer="21"/>
<rectangle x1="15.1257" y1="2.2225" x2="15.1765" y2="2.2479" layer="21"/>
<rectangle x1="15.3543" y1="2.2225" x2="15.4051" y2="2.2479" layer="21"/>
<rectangle x1="15.5575" y1="2.2225" x2="15.6337" y2="2.2479" layer="21"/>
<rectangle x1="16.3703" y1="2.2225" x2="16.4973" y2="2.2479" layer="21"/>
<rectangle x1="1.7907" y1="2.2479" x2="2.4257" y2="2.2733" layer="21"/>
<rectangle x1="3.1877" y1="2.2479" x2="3.7973" y2="2.2733" layer="21"/>
<rectangle x1="4.6355" y1="2.2479" x2="5.2451" y2="2.2733" layer="21"/>
<rectangle x1="6.0325" y1="2.2479" x2="6.6421" y2="2.2733" layer="21"/>
<rectangle x1="9.8933" y1="2.2479" x2="9.9441" y2="2.2733" layer="21"/>
<rectangle x1="10.5029" y1="2.2479" x2="10.5791" y2="2.2733" layer="21"/>
<rectangle x1="11.0871" y1="2.2479" x2="11.1633" y2="2.2733" layer="21"/>
<rectangle x1="11.7221" y1="2.2479" x2="11.7729" y2="2.2733" layer="21"/>
<rectangle x1="12.6111" y1="2.2479" x2="12.6873" y2="2.2733" layer="21"/>
<rectangle x1="13.0429" y1="2.2479" x2="13.1191" y2="2.2733" layer="21"/>
<rectangle x1="13.4747" y1="2.2479" x2="13.5509" y2="2.2733" layer="21"/>
<rectangle x1="13.7287" y1="2.2479" x2="13.7795" y2="2.2733" layer="21"/>
<rectangle x1="14.3891" y1="2.2479" x2="14.4399" y2="2.2733" layer="21"/>
<rectangle x1="14.5923" y1="2.2479" x2="14.6685" y2="2.2733" layer="21"/>
<rectangle x1="14.7955" y1="2.2479" x2="14.8717" y2="2.2733" layer="21"/>
<rectangle x1="15.1257" y1="2.2479" x2="15.1765" y2="2.2733" layer="21"/>
<rectangle x1="15.3543" y1="2.2479" x2="15.4051" y2="2.2733" layer="21"/>
<rectangle x1="15.5575" y1="2.2479" x2="15.6337" y2="2.2733" layer="21"/>
<rectangle x1="16.3195" y1="2.2479" x2="16.4465" y2="2.2733" layer="21"/>
<rectangle x1="1.7907" y1="2.2733" x2="2.4257" y2="2.2987" layer="21"/>
<rectangle x1="3.1877" y1="2.2733" x2="3.7973" y2="2.2987" layer="21"/>
<rectangle x1="4.6355" y1="2.2733" x2="5.2451" y2="2.2987" layer="21"/>
<rectangle x1="6.0325" y1="2.2733" x2="6.6421" y2="2.2987" layer="21"/>
<rectangle x1="9.8933" y1="2.2733" x2="9.9441" y2="2.2987" layer="21"/>
<rectangle x1="10.5029" y1="2.2733" x2="10.5791" y2="2.2987" layer="21"/>
<rectangle x1="11.0871" y1="2.2733" x2="11.1633" y2="2.2987" layer="21"/>
<rectangle x1="11.7221" y1="2.2733" x2="11.7729" y2="2.2987" layer="21"/>
<rectangle x1="12.6111" y1="2.2733" x2="12.6873" y2="2.2987" layer="21"/>
<rectangle x1="13.0429" y1="2.2733" x2="13.1191" y2="2.2987" layer="21"/>
<rectangle x1="13.4747" y1="2.2733" x2="13.5509" y2="2.2987" layer="21"/>
<rectangle x1="13.7287" y1="2.2733" x2="13.7795" y2="2.2987" layer="21"/>
<rectangle x1="14.3891" y1="2.2733" x2="14.4399" y2="2.2987" layer="21"/>
<rectangle x1="14.5923" y1="2.2733" x2="14.6685" y2="2.2987" layer="21"/>
<rectangle x1="14.7955" y1="2.2733" x2="14.8717" y2="2.2987" layer="21"/>
<rectangle x1="15.1257" y1="2.2733" x2="15.1765" y2="2.2987" layer="21"/>
<rectangle x1="15.3543" y1="2.2733" x2="15.4051" y2="2.2987" layer="21"/>
<rectangle x1="15.5829" y1="2.2733" x2="15.6337" y2="2.2987" layer="21"/>
<rectangle x1="16.2941" y1="2.2733" x2="16.3957" y2="2.2987" layer="21"/>
<rectangle x1="1.7907" y1="2.2987" x2="2.4257" y2="2.3241" layer="21"/>
<rectangle x1="3.1877" y1="2.2987" x2="3.7973" y2="2.3241" layer="21"/>
<rectangle x1="4.6355" y1="2.2987" x2="5.2451" y2="2.3241" layer="21"/>
<rectangle x1="6.0325" y1="2.2987" x2="6.6421" y2="2.3241" layer="21"/>
<rectangle x1="9.8933" y1="2.2987" x2="9.9441" y2="2.3241" layer="21"/>
<rectangle x1="10.5029" y1="2.2987" x2="10.5791" y2="2.3241" layer="21"/>
<rectangle x1="11.0871" y1="2.2987" x2="11.1633" y2="2.3241" layer="21"/>
<rectangle x1="11.7221" y1="2.2987" x2="11.7729" y2="2.3241" layer="21"/>
<rectangle x1="12.6111" y1="2.2987" x2="12.6873" y2="2.3241" layer="21"/>
<rectangle x1="13.0429" y1="2.2987" x2="13.1191" y2="2.3241" layer="21"/>
<rectangle x1="13.5001" y1="2.2987" x2="13.5509" y2="2.3241" layer="21"/>
<rectangle x1="13.7287" y1="2.2987" x2="13.8049" y2="2.3241" layer="21"/>
<rectangle x1="14.3637" y1="2.2987" x2="14.4399" y2="2.3241" layer="21"/>
<rectangle x1="14.5923" y1="2.2987" x2="14.6685" y2="2.3241" layer="21"/>
<rectangle x1="14.7701" y1="2.2987" x2="14.8463" y2="2.3241" layer="21"/>
<rectangle x1="15.1257" y1="2.2987" x2="15.1765" y2="2.3241" layer="21"/>
<rectangle x1="15.3543" y1="2.2987" x2="15.4051" y2="2.3241" layer="21"/>
<rectangle x1="15.5829" y1="2.2987" x2="15.6337" y2="2.3241" layer="21"/>
<rectangle x1="16.2941" y1="2.2987" x2="16.3703" y2="2.3241" layer="21"/>
<rectangle x1="1.7907" y1="2.3241" x2="2.4257" y2="2.3495" layer="21"/>
<rectangle x1="3.1877" y1="2.3241" x2="3.7973" y2="2.3495" layer="21"/>
<rectangle x1="4.6355" y1="2.3241" x2="5.2451" y2="2.3495" layer="21"/>
<rectangle x1="6.0325" y1="2.3241" x2="6.6421" y2="2.3495" layer="21"/>
<rectangle x1="9.8933" y1="2.3241" x2="9.9441" y2="2.3495" layer="21"/>
<rectangle x1="10.5029" y1="2.3241" x2="10.5791" y2="2.3495" layer="21"/>
<rectangle x1="11.0871" y1="2.3241" x2="11.1633" y2="2.3495" layer="21"/>
<rectangle x1="11.7221" y1="2.3241" x2="11.7983" y2="2.3495" layer="21"/>
<rectangle x1="12.6111" y1="2.3241" x2="12.6873" y2="2.3495" layer="21"/>
<rectangle x1="13.0429" y1="2.3241" x2="13.1191" y2="2.3495" layer="21"/>
<rectangle x1="13.5001" y1="2.3241" x2="13.5509" y2="2.3495" layer="21"/>
<rectangle x1="13.7287" y1="2.3241" x2="13.8049" y2="2.3495" layer="21"/>
<rectangle x1="14.3637" y1="2.3241" x2="14.4399" y2="2.3495" layer="21"/>
<rectangle x1="14.5923" y1="2.3241" x2="14.6685" y2="2.3495" layer="21"/>
<rectangle x1="14.7447" y1="2.3241" x2="14.8209" y2="2.3495" layer="21"/>
<rectangle x1="15.1257" y1="2.3241" x2="15.1765" y2="2.3495" layer="21"/>
<rectangle x1="15.3543" y1="2.3241" x2="15.4051" y2="2.3495" layer="21"/>
<rectangle x1="15.5829" y1="2.3241" x2="15.6337" y2="2.3495" layer="21"/>
<rectangle x1="16.2687" y1="2.3241" x2="16.3449" y2="2.3495" layer="21"/>
<rectangle x1="1.7907" y1="2.3495" x2="2.4257" y2="2.3749" layer="21"/>
<rectangle x1="3.1877" y1="2.3495" x2="3.7973" y2="2.3749" layer="21"/>
<rectangle x1="4.6355" y1="2.3495" x2="5.2451" y2="2.3749" layer="21"/>
<rectangle x1="6.0325" y1="2.3495" x2="6.6421" y2="2.3749" layer="21"/>
<rectangle x1="9.8933" y1="2.3495" x2="9.9441" y2="2.3749" layer="21"/>
<rectangle x1="10.5029" y1="2.3495" x2="10.5791" y2="2.3749" layer="21"/>
<rectangle x1="11.0871" y1="2.3495" x2="11.1633" y2="2.3749" layer="21"/>
<rectangle x1="11.7221" y1="2.3495" x2="11.7983" y2="2.3749" layer="21"/>
<rectangle x1="12.6111" y1="2.3495" x2="12.6873" y2="2.3749" layer="21"/>
<rectangle x1="13.0429" y1="2.3495" x2="13.1191" y2="2.3749" layer="21"/>
<rectangle x1="13.5001" y1="2.3495" x2="13.5509" y2="2.3749" layer="21"/>
<rectangle x1="13.7541" y1="2.3495" x2="13.8049" y2="2.3749" layer="21"/>
<rectangle x1="14.3637" y1="2.3495" x2="14.4145" y2="2.3749" layer="21"/>
<rectangle x1="14.5923" y1="2.3495" x2="14.6685" y2="2.3749" layer="21"/>
<rectangle x1="14.7447" y1="2.3495" x2="14.8209" y2="2.3749" layer="21"/>
<rectangle x1="15.1257" y1="2.3495" x2="15.1765" y2="2.3749" layer="21"/>
<rectangle x1="15.3543" y1="2.3495" x2="15.4051" y2="2.3749" layer="21"/>
<rectangle x1="15.5829" y1="2.3495" x2="15.6591" y2="2.3749" layer="21"/>
<rectangle x1="16.2687" y1="2.3495" x2="16.3195" y2="2.3749" layer="21"/>
<rectangle x1="1.7907" y1="2.3749" x2="2.4257" y2="2.4003" layer="21"/>
<rectangle x1="3.1877" y1="2.3749" x2="3.7973" y2="2.4003" layer="21"/>
<rectangle x1="4.6355" y1="2.3749" x2="5.2451" y2="2.4003" layer="21"/>
<rectangle x1="6.0325" y1="2.3749" x2="6.6421" y2="2.4003" layer="21"/>
<rectangle x1="9.8933" y1="2.3749" x2="9.9441" y2="2.4003" layer="21"/>
<rectangle x1="10.5029" y1="2.3749" x2="10.5791" y2="2.4003" layer="21"/>
<rectangle x1="11.0871" y1="2.3749" x2="11.1633" y2="2.4003" layer="21"/>
<rectangle x1="11.7475" y1="2.3749" x2="11.8237" y2="2.4003" layer="21"/>
<rectangle x1="12.6111" y1="2.3749" x2="12.6873" y2="2.4003" layer="21"/>
<rectangle x1="13.0429" y1="2.3749" x2="13.1191" y2="2.4003" layer="21"/>
<rectangle x1="13.5001" y1="2.3749" x2="13.5509" y2="2.4003" layer="21"/>
<rectangle x1="13.7541" y1="2.3749" x2="13.8303" y2="2.4003" layer="21"/>
<rectangle x1="14.3383" y1="2.3749" x2="14.4145" y2="2.4003" layer="21"/>
<rectangle x1="14.5923" y1="2.3749" x2="14.6685" y2="2.4003" layer="21"/>
<rectangle x1="14.7193" y1="2.3749" x2="14.7955" y2="2.4003" layer="21"/>
<rectangle x1="15.1257" y1="2.3749" x2="15.1765" y2="2.4003" layer="21"/>
<rectangle x1="15.3543" y1="2.3749" x2="15.4051" y2="2.4003" layer="21"/>
<rectangle x1="15.5829" y1="2.3749" x2="15.6591" y2="2.4003" layer="21"/>
<rectangle x1="16.2687" y1="2.3749" x2="16.3195" y2="2.4003" layer="21"/>
<rectangle x1="1.7907" y1="2.4003" x2="2.4257" y2="2.4257" layer="21"/>
<rectangle x1="3.1877" y1="2.4003" x2="3.7973" y2="2.4257" layer="21"/>
<rectangle x1="4.6355" y1="2.4003" x2="5.2451" y2="2.4257" layer="21"/>
<rectangle x1="6.0325" y1="2.4003" x2="6.6421" y2="2.4257" layer="21"/>
<rectangle x1="9.8933" y1="2.4003" x2="9.9441" y2="2.4257" layer="21"/>
<rectangle x1="10.5029" y1="2.4003" x2="10.5791" y2="2.4257" layer="21"/>
<rectangle x1="11.0871" y1="2.4003" x2="11.1633" y2="2.4257" layer="21"/>
<rectangle x1="11.7475" y1="2.4003" x2="11.8237" y2="2.4257" layer="21"/>
<rectangle x1="12.6111" y1="2.4003" x2="12.6873" y2="2.4257" layer="21"/>
<rectangle x1="13.0429" y1="2.4003" x2="13.1191" y2="2.4257" layer="21"/>
<rectangle x1="13.4747" y1="2.4003" x2="13.5509" y2="2.4257" layer="21"/>
<rectangle x1="13.7541" y1="2.4003" x2="13.8303" y2="2.4257" layer="21"/>
<rectangle x1="14.3383" y1="2.4003" x2="14.4145" y2="2.4257" layer="21"/>
<rectangle x1="14.5923" y1="2.4003" x2="14.6685" y2="2.4257" layer="21"/>
<rectangle x1="14.6939" y1="2.4003" x2="14.7701" y2="2.4257" layer="21"/>
<rectangle x1="15.1257" y1="2.4003" x2="15.1765" y2="2.4257" layer="21"/>
<rectangle x1="15.3543" y1="2.4003" x2="15.4051" y2="2.4257" layer="21"/>
<rectangle x1="15.6083" y1="2.4003" x2="15.6845" y2="2.4257" layer="21"/>
<rectangle x1="16.2687" y1="2.4003" x2="16.3195" y2="2.4257" layer="21"/>
<rectangle x1="1.7907" y1="2.4257" x2="2.4257" y2="2.4511" layer="21"/>
<rectangle x1="3.1877" y1="2.4257" x2="3.7973" y2="2.4511" layer="21"/>
<rectangle x1="4.6355" y1="2.4257" x2="5.2451" y2="2.4511" layer="21"/>
<rectangle x1="6.0325" y1="2.4257" x2="6.6421" y2="2.4511" layer="21"/>
<rectangle x1="9.8933" y1="2.4257" x2="9.9441" y2="2.4511" layer="21"/>
<rectangle x1="10.5029" y1="2.4257" x2="10.5791" y2="2.4511" layer="21"/>
<rectangle x1="11.0871" y1="2.4257" x2="11.1633" y2="2.4511" layer="21"/>
<rectangle x1="11.7729" y1="2.4257" x2="11.8491" y2="2.4511" layer="21"/>
<rectangle x1="12.6111" y1="2.4257" x2="12.6873" y2="2.4511" layer="21"/>
<rectangle x1="13.0429" y1="2.4257" x2="13.1191" y2="2.4511" layer="21"/>
<rectangle x1="13.4747" y1="2.4257" x2="13.5509" y2="2.4511" layer="21"/>
<rectangle x1="13.7795" y1="2.4257" x2="13.8557" y2="2.4511" layer="21"/>
<rectangle x1="14.3129" y1="2.4257" x2="14.3891" y2="2.4511" layer="21"/>
<rectangle x1="14.5923" y1="2.4257" x2="14.6685" y2="2.4511" layer="21"/>
<rectangle x1="14.6939" y1="2.4257" x2="14.7701" y2="2.4511" layer="21"/>
<rectangle x1="15.1257" y1="2.4257" x2="15.1765" y2="2.4511" layer="21"/>
<rectangle x1="15.3543" y1="2.4257" x2="15.4051" y2="2.4511" layer="21"/>
<rectangle x1="15.6083" y1="2.4257" x2="15.6845" y2="2.4511" layer="21"/>
<rectangle x1="16.2687" y1="2.4257" x2="16.3195" y2="2.4511" layer="21"/>
<rectangle x1="1.7907" y1="2.4511" x2="2.4257" y2="2.4765" layer="21"/>
<rectangle x1="3.1877" y1="2.4511" x2="3.7973" y2="2.4765" layer="21"/>
<rectangle x1="4.6355" y1="2.4511" x2="5.2451" y2="2.4765" layer="21"/>
<rectangle x1="6.0325" y1="2.4511" x2="6.6421" y2="2.4765" layer="21"/>
<rectangle x1="9.8933" y1="2.4511" x2="9.9441" y2="2.4765" layer="21"/>
<rectangle x1="10.5029" y1="2.4511" x2="10.5791" y2="2.4765" layer="21"/>
<rectangle x1="11.0871" y1="2.4511" x2="11.1633" y2="2.4765" layer="21"/>
<rectangle x1="11.7729" y1="2.4511" x2="11.8745" y2="2.4765" layer="21"/>
<rectangle x1="12.6111" y1="2.4511" x2="12.6873" y2="2.4765" layer="21"/>
<rectangle x1="13.0429" y1="2.4511" x2="13.1191" y2="2.4765" layer="21"/>
<rectangle x1="13.4493" y1="2.4511" x2="13.5255" y2="2.4765" layer="21"/>
<rectangle x1="13.7795" y1="2.4511" x2="13.8811" y2="2.4765" layer="21"/>
<rectangle x1="14.2875" y1="2.4511" x2="14.3891" y2="2.4765" layer="21"/>
<rectangle x1="14.5923" y1="2.4511" x2="14.7447" y2="2.4765" layer="21"/>
<rectangle x1="15.1257" y1="2.4511" x2="15.1765" y2="2.4765" layer="21"/>
<rectangle x1="15.3543" y1="2.4511" x2="15.4051" y2="2.4765" layer="21"/>
<rectangle x1="15.6337" y1="2.4511" x2="15.7099" y2="2.4765" layer="21"/>
<rectangle x1="16.2687" y1="2.4511" x2="16.3449" y2="2.4765" layer="21"/>
<rectangle x1="1.7907" y1="2.4765" x2="2.4257" y2="2.5019" layer="21"/>
<rectangle x1="3.1877" y1="2.4765" x2="3.7973" y2="2.5019" layer="21"/>
<rectangle x1="4.6355" y1="2.4765" x2="5.2451" y2="2.5019" layer="21"/>
<rectangle x1="6.0325" y1="2.4765" x2="6.6421" y2="2.5019" layer="21"/>
<rectangle x1="9.8933" y1="2.4765" x2="9.9441" y2="2.5019" layer="21"/>
<rectangle x1="10.5029" y1="2.4765" x2="10.5791" y2="2.5019" layer="21"/>
<rectangle x1="11.0871" y1="2.4765" x2="11.1633" y2="2.5019" layer="21"/>
<rectangle x1="11.7983" y1="2.4765" x2="11.8999" y2="2.5019" layer="21"/>
<rectangle x1="12.6111" y1="2.4765" x2="12.6873" y2="2.5019" layer="21"/>
<rectangle x1="13.0429" y1="2.4765" x2="13.1191" y2="2.5019" layer="21"/>
<rectangle x1="13.4239" y1="2.4765" x2="13.5255" y2="2.5019" layer="21"/>
<rectangle x1="13.8049" y1="2.4765" x2="13.9065" y2="2.5019" layer="21"/>
<rectangle x1="14.2621" y1="2.4765" x2="14.3637" y2="2.5019" layer="21"/>
<rectangle x1="14.5923" y1="2.4765" x2="14.7193" y2="2.5019" layer="21"/>
<rectangle x1="15.1257" y1="2.4765" x2="15.1765" y2="2.5019" layer="21"/>
<rectangle x1="15.3543" y1="2.4765" x2="15.4051" y2="2.5019" layer="21"/>
<rectangle x1="15.6591" y1="2.4765" x2="15.7353" y2="2.5019" layer="21"/>
<rectangle x1="16.2687" y1="2.4765" x2="16.3703" y2="2.5019" layer="21"/>
<rectangle x1="1.7907" y1="2.5019" x2="2.4257" y2="2.5273" layer="21"/>
<rectangle x1="3.1877" y1="2.5019" x2="3.7973" y2="2.5273" layer="21"/>
<rectangle x1="4.6355" y1="2.5019" x2="5.2451" y2="2.5273" layer="21"/>
<rectangle x1="6.0325" y1="2.5019" x2="6.6421" y2="2.5273" layer="21"/>
<rectangle x1="9.8933" y1="2.5019" x2="9.9441" y2="2.5273" layer="21"/>
<rectangle x1="10.5029" y1="2.5019" x2="10.5791" y2="2.5273" layer="21"/>
<rectangle x1="11.0871" y1="2.5019" x2="11.1633" y2="2.5273" layer="21"/>
<rectangle x1="11.8237" y1="2.5019" x2="11.9253" y2="2.5273" layer="21"/>
<rectangle x1="12.2555" y1="2.5019" x2="12.2809" y2="2.5273" layer="21"/>
<rectangle x1="12.6111" y1="2.5019" x2="12.6873" y2="2.5273" layer="21"/>
<rectangle x1="13.0429" y1="2.5019" x2="13.1191" y2="2.5273" layer="21"/>
<rectangle x1="13.3731" y1="2.5019" x2="13.5001" y2="2.5273" layer="21"/>
<rectangle x1="13.8303" y1="2.5019" x2="13.9319" y2="2.5273" layer="21"/>
<rectangle x1="14.2367" y1="2.5019" x2="14.3383" y2="2.5273" layer="21"/>
<rectangle x1="14.5923" y1="2.5019" x2="14.7193" y2="2.5273" layer="21"/>
<rectangle x1="15.1257" y1="2.5019" x2="15.1765" y2="2.5273" layer="21"/>
<rectangle x1="15.3543" y1="2.5019" x2="15.4051" y2="2.5273" layer="21"/>
<rectangle x1="15.6591" y1="2.5019" x2="15.7861" y2="2.5273" layer="21"/>
<rectangle x1="16.0909" y1="2.5019" x2="16.1417" y2="2.5273" layer="21"/>
<rectangle x1="16.2941" y1="2.5019" x2="16.3957" y2="2.5273" layer="21"/>
<rectangle x1="16.7005" y1="2.5019" x2="16.7259" y2="2.5273" layer="21"/>
<rectangle x1="1.7907" y1="2.5273" x2="2.4257" y2="2.5527" layer="21"/>
<rectangle x1="3.1877" y1="2.5273" x2="3.7973" y2="2.5527" layer="21"/>
<rectangle x1="4.6355" y1="2.5273" x2="5.2451" y2="2.5527" layer="21"/>
<rectangle x1="6.0325" y1="2.5273" x2="6.6421" y2="2.5527" layer="21"/>
<rectangle x1="9.8933" y1="2.5273" x2="10.3759" y2="2.5527" layer="21"/>
<rectangle x1="10.5029" y1="2.5273" x2="10.5791" y2="2.5527" layer="21"/>
<rectangle x1="11.0871" y1="2.5273" x2="11.5951" y2="2.5527" layer="21"/>
<rectangle x1="11.8491" y1="2.5273" x2="12.0015" y2="2.5527" layer="21"/>
<rectangle x1="12.1539" y1="2.5273" x2="12.2809" y2="2.5527" layer="21"/>
<rectangle x1="12.3571" y1="2.5273" x2="12.9667" y2="2.5527" layer="21"/>
<rectangle x1="13.0429" y1="2.5273" x2="13.4747" y2="2.5527" layer="21"/>
<rectangle x1="13.8557" y1="2.5273" x2="14.0081" y2="2.5527" layer="21"/>
<rectangle x1="14.1605" y1="2.5273" x2="14.3129" y2="2.5527" layer="21"/>
<rectangle x1="14.5923" y1="2.5273" x2="14.6939" y2="2.5527" layer="21"/>
<rectangle x1="15.1257" y1="2.5273" x2="15.1765" y2="2.5527" layer="21"/>
<rectangle x1="15.3543" y1="2.5273" x2="15.4051" y2="2.5527" layer="21"/>
<rectangle x1="15.7099" y1="2.5273" x2="15.8369" y2="2.5527" layer="21"/>
<rectangle x1="16.0147" y1="2.5273" x2="16.1417" y2="2.5527" layer="21"/>
<rectangle x1="16.3195" y1="2.5273" x2="16.4465" y2="2.5527" layer="21"/>
<rectangle x1="16.5989" y1="2.5273" x2="16.7259" y2="2.5527" layer="21"/>
<rectangle x1="1.7907" y1="2.5527" x2="2.4257" y2="2.5781" layer="21"/>
<rectangle x1="3.1877" y1="2.5527" x2="3.8227" y2="2.5781" layer="21"/>
<rectangle x1="4.6355" y1="2.5527" x2="5.2705" y2="2.5781" layer="21"/>
<rectangle x1="6.0325" y1="2.5527" x2="6.6675" y2="2.5781" layer="21"/>
<rectangle x1="9.8933" y1="2.5527" x2="10.3759" y2="2.5781" layer="21"/>
<rectangle x1="10.5029" y1="2.5527" x2="10.5791" y2="2.5781" layer="21"/>
<rectangle x1="11.0871" y1="2.5527" x2="11.5951" y2="2.5781" layer="21"/>
<rectangle x1="11.8999" y1="2.5527" x2="12.2809" y2="2.5781" layer="21"/>
<rectangle x1="12.3317" y1="2.5527" x2="12.9667" y2="2.5781" layer="21"/>
<rectangle x1="13.0429" y1="2.5527" x2="13.4239" y2="2.5781" layer="21"/>
<rectangle x1="13.8811" y1="2.5527" x2="14.2875" y2="2.5781" layer="21"/>
<rectangle x1="14.5923" y1="2.5527" x2="14.6939" y2="2.5781" layer="21"/>
<rectangle x1="15.1257" y1="2.5527" x2="15.1765" y2="2.5781" layer="21"/>
<rectangle x1="15.3543" y1="2.5527" x2="15.4051" y2="2.5781" layer="21"/>
<rectangle x1="15.7353" y1="2.5527" x2="16.1417" y2="2.5781" layer="21"/>
<rectangle x1="16.3449" y1="2.5527" x2="16.7259" y2="2.5781" layer="21"/>
<rectangle x1="0.6223" y1="2.5781" x2="7.8613" y2="2.6035" layer="21"/>
<rectangle x1="9.8933" y1="2.5781" x2="10.3759" y2="2.6035" layer="21"/>
<rectangle x1="10.5029" y1="2.5781" x2="10.5537" y2="2.6035" layer="21"/>
<rectangle x1="11.1125" y1="2.5781" x2="11.5951" y2="2.6035" layer="21"/>
<rectangle x1="11.9507" y1="2.5781" x2="12.2047" y2="2.6035" layer="21"/>
<rectangle x1="12.3571" y1="2.5781" x2="12.9667" y2="2.6035" layer="21"/>
<rectangle x1="13.0429" y1="2.5781" x2="13.3223" y2="2.6035" layer="21"/>
<rectangle x1="13.9573" y1="2.5781" x2="14.2113" y2="2.6035" layer="21"/>
<rectangle x1="14.6177" y1="2.5781" x2="14.6685" y2="2.6035" layer="21"/>
<rectangle x1="15.1257" y1="2.5781" x2="15.1765" y2="2.6035" layer="21"/>
<rectangle x1="15.3543" y1="2.5781" x2="15.4051" y2="2.6035" layer="21"/>
<rectangle x1="15.8115" y1="2.5781" x2="16.0655" y2="2.6035" layer="21"/>
<rectangle x1="16.3957" y1="2.5781" x2="16.6497" y2="2.6035" layer="21"/>
<rectangle x1="0.6223" y1="2.6035" x2="7.8613" y2="2.6289" layer="21"/>
<rectangle x1="0.6223" y1="2.6289" x2="7.8613" y2="2.6543" layer="21"/>
<rectangle x1="0.6223" y1="2.6543" x2="7.8613" y2="2.6797" layer="21"/>
<rectangle x1="0.6223" y1="2.6797" x2="7.8613" y2="2.7051" layer="21"/>
<rectangle x1="0.6223" y1="2.7051" x2="7.8613" y2="2.7305" layer="21"/>
<rectangle x1="0.6223" y1="2.7305" x2="7.8613" y2="2.7559" layer="21"/>
<rectangle x1="0.6223" y1="2.7559" x2="7.8613" y2="2.7813" layer="21"/>
<rectangle x1="0.6223" y1="2.7813" x2="7.8613" y2="2.8067" layer="21"/>
<rectangle x1="0.6223" y1="2.8067" x2="7.8613" y2="2.8321" layer="21"/>
<rectangle x1="0.6223" y1="2.8321" x2="7.8613" y2="2.8575" layer="21"/>
<rectangle x1="0.6223" y1="2.8575" x2="7.8613" y2="2.8829" layer="21"/>
<rectangle x1="0.6223" y1="2.8829" x2="7.8613" y2="2.9083" layer="21"/>
<rectangle x1="0.6223" y1="2.9083" x2="7.8613" y2="2.9337" layer="21"/>
<rectangle x1="0.6223" y1="2.9337" x2="3.0861" y2="2.9591" layer="21"/>
<rectangle x1="3.4671" y1="2.9337" x2="4.9657" y2="2.9591" layer="21"/>
<rectangle x1="5.2959" y1="2.9337" x2="7.8613" y2="2.9591" layer="21"/>
<rectangle x1="16.7767" y1="2.9337" x2="16.9291" y2="2.9591" layer="21"/>
<rectangle x1="0.6223" y1="2.9591" x2="1.4859" y2="2.9845" layer="21"/>
<rectangle x1="1.9177" y1="2.9591" x2="2.9591" y2="2.9845" layer="21"/>
<rectangle x1="3.6449" y1="2.9591" x2="4.8641" y2="2.9845" layer="21"/>
<rectangle x1="5.4229" y1="2.9591" x2="7.8613" y2="2.9845" layer="21"/>
<rectangle x1="11.6713" y1="2.9591" x2="12.3063" y2="2.9845" layer="21"/>
<rectangle x1="15.5321" y1="2.9591" x2="16.1163" y2="2.9845" layer="21"/>
<rectangle x1="16.7259" y1="2.9591" x2="16.9799" y2="2.9845" layer="21"/>
<rectangle x1="0.6223" y1="2.9845" x2="1.4097" y2="3.0099" layer="21"/>
<rectangle x1="1.9939" y1="2.9845" x2="2.8575" y2="3.0099" layer="21"/>
<rectangle x1="3.7719" y1="2.9845" x2="4.7879" y2="3.0099" layer="21"/>
<rectangle x1="5.4991" y1="2.9845" x2="5.8801" y2="3.0099" layer="21"/>
<rectangle x1="6.4643" y1="2.9845" x2="6.9215" y2="3.0099" layer="21"/>
<rectangle x1="7.5311" y1="2.9845" x2="7.8613" y2="3.0099" layer="21"/>
<rectangle x1="8.1661" y1="2.9845" x2="8.7249" y2="3.0099" layer="21"/>
<rectangle x1="9.2075" y1="2.9845" x2="9.7663" y2="3.0099" layer="21"/>
<rectangle x1="10.2489" y1="2.9845" x2="10.8077" y2="3.0099" layer="21"/>
<rectangle x1="11.5443" y1="2.9845" x2="12.4333" y2="3.0099" layer="21"/>
<rectangle x1="13.0683" y1="2.9845" x2="13.6271" y2="3.0099" layer="21"/>
<rectangle x1="14.2113" y1="2.9845" x2="14.8463" y2="3.0099" layer="21"/>
<rectangle x1="15.4305" y1="2.9845" x2="16.2433" y2="3.0099" layer="21"/>
<rectangle x1="16.7005" y1="2.9845" x2="16.8021" y2="3.0099" layer="21"/>
<rectangle x1="16.9037" y1="2.9845" x2="17.0053" y2="3.0099" layer="21"/>
<rectangle x1="0.6223" y1="3.0099" x2="1.3589" y2="3.0353" layer="21"/>
<rectangle x1="2.0447" y1="3.0099" x2="2.8067" y2="3.0353" layer="21"/>
<rectangle x1="3.8735" y1="3.0099" x2="4.7371" y2="3.0353" layer="21"/>
<rectangle x1="5.5753" y1="3.0099" x2="5.8801" y2="3.0353" layer="21"/>
<rectangle x1="6.4643" y1="3.0099" x2="6.9215" y2="3.0353" layer="21"/>
<rectangle x1="7.5311" y1="3.0099" x2="7.8613" y2="3.0353" layer="21"/>
<rectangle x1="8.1407" y1="3.0099" x2="8.7249" y2="3.0353" layer="21"/>
<rectangle x1="9.2075" y1="3.0099" x2="9.7663" y2="3.0353" layer="21"/>
<rectangle x1="10.2489" y1="3.0099" x2="10.8077" y2="3.0353" layer="21"/>
<rectangle x1="11.4681" y1="3.0099" x2="12.5349" y2="3.0353" layer="21"/>
<rectangle x1="13.0683" y1="3.0099" x2="13.6271" y2="3.0353" layer="21"/>
<rectangle x1="14.2113" y1="3.0099" x2="14.8463" y2="3.0353" layer="21"/>
<rectangle x1="15.3543" y1="3.0099" x2="16.3449" y2="3.0353" layer="21"/>
<rectangle x1="16.6751" y1="3.0099" x2="16.7513" y2="3.0353" layer="21"/>
<rectangle x1="16.9291" y1="3.0099" x2="17.0307" y2="3.0353" layer="21"/>
<rectangle x1="0.6223" y1="3.0353" x2="1.3335" y2="3.0607" layer="21"/>
<rectangle x1="2.0447" y1="3.0353" x2="2.7559" y2="3.0607" layer="21"/>
<rectangle x1="3.9497" y1="3.0353" x2="4.6863" y2="3.0607" layer="21"/>
<rectangle x1="5.6007" y1="3.0353" x2="5.8801" y2="3.0607" layer="21"/>
<rectangle x1="6.4643" y1="3.0353" x2="6.9215" y2="3.0607" layer="21"/>
<rectangle x1="7.5311" y1="3.0353" x2="7.8613" y2="3.0607" layer="21"/>
<rectangle x1="8.1407" y1="3.0353" x2="8.7249" y2="3.0607" layer="21"/>
<rectangle x1="9.2075" y1="3.0353" x2="9.7663" y2="3.0607" layer="21"/>
<rectangle x1="10.2489" y1="3.0353" x2="10.8077" y2="3.0607" layer="21"/>
<rectangle x1="11.4173" y1="3.0353" x2="12.6365" y2="3.0607" layer="21"/>
<rectangle x1="13.0683" y1="3.0353" x2="13.6271" y2="3.0607" layer="21"/>
<rectangle x1="14.1859" y1="3.0353" x2="14.8209" y2="3.0607" layer="21"/>
<rectangle x1="15.3035" y1="3.0353" x2="16.4465" y2="3.0607" layer="21"/>
<rectangle x1="16.6751" y1="3.0353" x2="16.7259" y2="3.0607" layer="21"/>
<rectangle x1="16.7767" y1="3.0353" x2="16.8275" y2="3.0607" layer="21"/>
<rectangle x1="16.8783" y1="3.0353" x2="16.9545" y2="3.0607" layer="21"/>
<rectangle x1="16.9799" y1="3.0353" x2="17.0307" y2="3.0607" layer="21"/>
<rectangle x1="0.6223" y1="3.0607" x2="1.2827" y2="3.0861" layer="21"/>
<rectangle x1="2.0447" y1="3.0607" x2="2.7051" y2="3.0861" layer="21"/>
<rectangle x1="4.0005" y1="3.0607" x2="4.6355" y2="3.0861" layer="21"/>
<rectangle x1="5.6515" y1="3.0607" x2="5.8801" y2="3.0861" layer="21"/>
<rectangle x1="6.4643" y1="3.0607" x2="6.9215" y2="3.0861" layer="21"/>
<rectangle x1="7.5311" y1="3.0607" x2="7.8613" y2="3.0861" layer="21"/>
<rectangle x1="8.1407" y1="3.0607" x2="8.7249" y2="3.0861" layer="21"/>
<rectangle x1="9.2075" y1="3.0607" x2="9.7663" y2="3.0861" layer="21"/>
<rectangle x1="10.2489" y1="3.0607" x2="10.8077" y2="3.0861" layer="21"/>
<rectangle x1="11.3665" y1="3.0607" x2="12.6873" y2="3.0861" layer="21"/>
<rectangle x1="13.0683" y1="3.0607" x2="13.6271" y2="3.0861" layer="21"/>
<rectangle x1="14.1605" y1="3.0607" x2="14.8209" y2="3.0861" layer="21"/>
<rectangle x1="15.2527" y1="3.0607" x2="16.4973" y2="3.0861" layer="21"/>
<rectangle x1="16.6497" y1="3.0607" x2="16.7259" y2="3.0861" layer="21"/>
<rectangle x1="16.7767" y1="3.0607" x2="16.8275" y2="3.0861" layer="21"/>
<rectangle x1="16.8783" y1="3.0607" x2="16.9291" y2="3.0861" layer="21"/>
<rectangle x1="16.9799" y1="3.0607" x2="17.0561" y2="3.0861" layer="21"/>
<rectangle x1="0.6223" y1="3.0861" x2="1.2573" y2="3.1115" layer="21"/>
<rectangle x1="2.0447" y1="3.0861" x2="2.6543" y2="3.1115" layer="21"/>
<rectangle x1="4.0005" y1="3.0861" x2="4.6101" y2="3.1115" layer="21"/>
<rectangle x1="5.6261" y1="3.0861" x2="5.8801" y2="3.1115" layer="21"/>
<rectangle x1="6.4643" y1="3.0861" x2="6.9215" y2="3.1115" layer="21"/>
<rectangle x1="7.5311" y1="3.0861" x2="7.8613" y2="3.1115" layer="21"/>
<rectangle x1="8.1407" y1="3.0861" x2="8.7249" y2="3.1115" layer="21"/>
<rectangle x1="9.2075" y1="3.0861" x2="9.7663" y2="3.1115" layer="21"/>
<rectangle x1="10.2489" y1="3.0861" x2="10.8077" y2="3.1115" layer="21"/>
<rectangle x1="11.3157" y1="3.0861" x2="12.7127" y2="3.1115" layer="21"/>
<rectangle x1="13.0683" y1="3.0861" x2="13.6271" y2="3.1115" layer="21"/>
<rectangle x1="14.1605" y1="3.0861" x2="14.7955" y2="3.1115" layer="21"/>
<rectangle x1="15.2019" y1="3.0861" x2="16.5227" y2="3.1115" layer="21"/>
<rectangle x1="16.6497" y1="3.0861" x2="16.7259" y2="3.1115" layer="21"/>
<rectangle x1="16.7767" y1="3.0861" x2="16.9291" y2="3.1115" layer="21"/>
<rectangle x1="16.9799" y1="3.0861" x2="17.0561" y2="3.1115" layer="21"/>
<rectangle x1="0.6223" y1="3.1115" x2="1.2319" y2="3.1369" layer="21"/>
<rectangle x1="2.0447" y1="3.1115" x2="2.6289" y2="3.1369" layer="21"/>
<rectangle x1="4.0005" y1="3.1115" x2="4.5593" y2="3.1369" layer="21"/>
<rectangle x1="5.6261" y1="3.1115" x2="5.8801" y2="3.1369" layer="21"/>
<rectangle x1="6.4643" y1="3.1115" x2="6.9215" y2="3.1369" layer="21"/>
<rectangle x1="7.5311" y1="3.1115" x2="7.8613" y2="3.1369" layer="21"/>
<rectangle x1="8.1407" y1="3.1115" x2="8.7249" y2="3.1369" layer="21"/>
<rectangle x1="9.2075" y1="3.1115" x2="9.7663" y2="3.1369" layer="21"/>
<rectangle x1="10.2489" y1="3.1115" x2="10.8077" y2="3.1369" layer="21"/>
<rectangle x1="11.2903" y1="3.1115" x2="12.7127" y2="3.1369" layer="21"/>
<rectangle x1="13.0683" y1="3.1115" x2="13.6271" y2="3.1369" layer="21"/>
<rectangle x1="14.1351" y1="3.1115" x2="14.7701" y2="3.1369" layer="21"/>
<rectangle x1="15.1765" y1="3.1115" x2="16.5227" y2="3.1369" layer="21"/>
<rectangle x1="16.6497" y1="3.1115" x2="16.7259" y2="3.1369" layer="21"/>
<rectangle x1="16.7767" y1="3.1115" x2="16.9037" y2="3.1369" layer="21"/>
<rectangle x1="17.0053" y1="3.1115" x2="17.0561" y2="3.1369" layer="21"/>
<rectangle x1="0.6223" y1="3.1369" x2="1.2319" y2="3.1623" layer="21"/>
<rectangle x1="2.0447" y1="3.1369" x2="2.6035" y2="3.1623" layer="21"/>
<rectangle x1="3.9751" y1="3.1369" x2="4.5339" y2="3.1623" layer="21"/>
<rectangle x1="5.6261" y1="3.1369" x2="5.8801" y2="3.1623" layer="21"/>
<rectangle x1="6.4643" y1="3.1369" x2="6.9215" y2="3.1623" layer="21"/>
<rectangle x1="7.5311" y1="3.1369" x2="7.8613" y2="3.1623" layer="21"/>
<rectangle x1="8.1407" y1="3.1369" x2="8.7249" y2="3.1623" layer="21"/>
<rectangle x1="9.2075" y1="3.1369" x2="9.7663" y2="3.1623" layer="21"/>
<rectangle x1="10.2489" y1="3.1369" x2="10.8077" y2="3.1623" layer="21"/>
<rectangle x1="11.2649" y1="3.1369" x2="12.7127" y2="3.1623" layer="21"/>
<rectangle x1="13.0683" y1="3.1369" x2="13.6271" y2="3.1623" layer="21"/>
<rectangle x1="14.1097" y1="3.1369" x2="14.7701" y2="3.1623" layer="21"/>
<rectangle x1="15.1511" y1="3.1369" x2="16.4973" y2="3.1623" layer="21"/>
<rectangle x1="16.6497" y1="3.1369" x2="16.7259" y2="3.1623" layer="21"/>
<rectangle x1="16.7767" y1="3.1369" x2="16.9291" y2="3.1623" layer="21"/>
<rectangle x1="16.9799" y1="3.1369" x2="17.0561" y2="3.1623" layer="21"/>
<rectangle x1="0.6223" y1="3.1623" x2="1.2065" y2="3.1877" layer="21"/>
<rectangle x1="2.0447" y1="3.1623" x2="2.5527" y2="3.1877" layer="21"/>
<rectangle x1="3.9751" y1="3.1623" x2="4.5085" y2="3.1877" layer="21"/>
<rectangle x1="5.6007" y1="3.1623" x2="5.8801" y2="3.1877" layer="21"/>
<rectangle x1="6.4643" y1="3.1623" x2="6.9215" y2="3.1877" layer="21"/>
<rectangle x1="7.5311" y1="3.1623" x2="7.8613" y2="3.1877" layer="21"/>
<rectangle x1="8.1407" y1="3.1623" x2="8.7249" y2="3.1877" layer="21"/>
<rectangle x1="9.2075" y1="3.1623" x2="9.7663" y2="3.1877" layer="21"/>
<rectangle x1="10.2489" y1="3.1623" x2="10.8077" y2="3.1877" layer="21"/>
<rectangle x1="11.2395" y1="3.1623" x2="12.7127" y2="3.1877" layer="21"/>
<rectangle x1="13.0683" y1="3.1623" x2="13.6271" y2="3.1877" layer="21"/>
<rectangle x1="14.1097" y1="3.1623" x2="14.7447" y2="3.1877" layer="21"/>
<rectangle x1="15.1003" y1="3.1623" x2="16.4973" y2="3.1877" layer="21"/>
<rectangle x1="16.6497" y1="3.1623" x2="16.7259" y2="3.1877" layer="21"/>
<rectangle x1="16.7767" y1="3.1623" x2="16.8275" y2="3.1877" layer="21"/>
<rectangle x1="16.8783" y1="3.1623" x2="16.9291" y2="3.1877" layer="21"/>
<rectangle x1="16.9799" y1="3.1623" x2="17.0561" y2="3.1877" layer="21"/>
<rectangle x1="0.6223" y1="3.1877" x2="1.2065" y2="3.2131" layer="21"/>
<rectangle x1="2.0447" y1="3.1877" x2="2.5273" y2="3.2131" layer="21"/>
<rectangle x1="3.9751" y1="3.1877" x2="4.4831" y2="3.2131" layer="21"/>
<rectangle x1="5.6007" y1="3.1877" x2="5.8801" y2="3.2131" layer="21"/>
<rectangle x1="6.4643" y1="3.1877" x2="6.9215" y2="3.2131" layer="21"/>
<rectangle x1="7.5311" y1="3.1877" x2="7.8613" y2="3.2131" layer="21"/>
<rectangle x1="8.1407" y1="3.1877" x2="8.7249" y2="3.2131" layer="21"/>
<rectangle x1="9.2075" y1="3.1877" x2="9.7663" y2="3.2131" layer="21"/>
<rectangle x1="10.2489" y1="3.1877" x2="10.8077" y2="3.2131" layer="21"/>
<rectangle x1="11.2141" y1="3.1877" x2="12.7127" y2="3.2131" layer="21"/>
<rectangle x1="13.0683" y1="3.1877" x2="13.6271" y2="3.2131" layer="21"/>
<rectangle x1="14.0843" y1="3.1877" x2="14.7193" y2="3.2131" layer="21"/>
<rectangle x1="15.0749" y1="3.1877" x2="16.4719" y2="3.2131" layer="21"/>
<rectangle x1="16.6751" y1="3.1877" x2="16.7259" y2="3.2131" layer="21"/>
<rectangle x1="16.7767" y1="3.1877" x2="16.9291" y2="3.2131" layer="21"/>
<rectangle x1="16.9799" y1="3.1877" x2="17.0307" y2="3.2131" layer="21"/>
<rectangle x1="0.6223" y1="3.2131" x2="1.1811" y2="3.2385" layer="21"/>
<rectangle x1="2.0447" y1="3.2131" x2="2.5019" y2="3.2385" layer="21"/>
<rectangle x1="3.9497" y1="3.2131" x2="4.4577" y2="3.2385" layer="21"/>
<rectangle x1="5.5753" y1="3.2131" x2="5.8801" y2="3.2385" layer="21"/>
<rectangle x1="6.4643" y1="3.2131" x2="6.9215" y2="3.2385" layer="21"/>
<rectangle x1="7.5311" y1="3.2131" x2="7.8613" y2="3.2385" layer="21"/>
<rectangle x1="8.1407" y1="3.2131" x2="8.7249" y2="3.2385" layer="21"/>
<rectangle x1="9.2075" y1="3.2131" x2="9.7663" y2="3.2385" layer="21"/>
<rectangle x1="10.2489" y1="3.2131" x2="10.8077" y2="3.2385" layer="21"/>
<rectangle x1="11.1887" y1="3.2131" x2="12.7127" y2="3.2385" layer="21"/>
<rectangle x1="13.0683" y1="3.2131" x2="13.6271" y2="3.2385" layer="21"/>
<rectangle x1="14.0843" y1="3.2131" x2="14.7193" y2="3.2385" layer="21"/>
<rectangle x1="15.0495" y1="3.2131" x2="16.4719" y2="3.2385" layer="21"/>
<rectangle x1="16.6751" y1="3.2131" x2="16.7513" y2="3.2385" layer="21"/>
<rectangle x1="16.7767" y1="3.2131" x2="16.9037" y2="3.2385" layer="21"/>
<rectangle x1="16.9545" y1="3.2131" x2="17.0307" y2="3.2385" layer="21"/>
<rectangle x1="0.6223" y1="3.2385" x2="1.1811" y2="3.2639" layer="21"/>
<rectangle x1="2.0447" y1="3.2385" x2="2.5019" y2="3.2639" layer="21"/>
<rectangle x1="3.9497" y1="3.2385" x2="4.4577" y2="3.2639" layer="21"/>
<rectangle x1="5.5753" y1="3.2385" x2="5.8801" y2="3.2639" layer="21"/>
<rectangle x1="6.4643" y1="3.2385" x2="6.9215" y2="3.2639" layer="21"/>
<rectangle x1="7.5311" y1="3.2385" x2="7.8613" y2="3.2639" layer="21"/>
<rectangle x1="8.1407" y1="3.2385" x2="8.7249" y2="3.2639" layer="21"/>
<rectangle x1="9.2075" y1="3.2385" x2="9.7663" y2="3.2639" layer="21"/>
<rectangle x1="10.2489" y1="3.2385" x2="10.8077" y2="3.2639" layer="21"/>
<rectangle x1="11.1633" y1="3.2385" x2="12.7127" y2="3.2639" layer="21"/>
<rectangle x1="13.0683" y1="3.2385" x2="13.6271" y2="3.2639" layer="21"/>
<rectangle x1="14.0589" y1="3.2385" x2="14.6939" y2="3.2639" layer="21"/>
<rectangle x1="15.0241" y1="3.2385" x2="16.4719" y2="3.2639" layer="21"/>
<rectangle x1="16.7005" y1="3.2385" x2="16.8021" y2="3.2639" layer="21"/>
<rectangle x1="16.9291" y1="3.2385" x2="17.0053" y2="3.2639" layer="21"/>
<rectangle x1="0.6223" y1="3.2639" x2="1.1557" y2="3.2893" layer="21"/>
<rectangle x1="2.0447" y1="3.2639" x2="2.4765" y2="3.2893" layer="21"/>
<rectangle x1="3.9243" y1="3.2639" x2="4.4323" y2="3.2893" layer="21"/>
<rectangle x1="5.5753" y1="3.2639" x2="5.8801" y2="3.2893" layer="21"/>
<rectangle x1="6.4643" y1="3.2639" x2="6.9215" y2="3.2893" layer="21"/>
<rectangle x1="7.5311" y1="3.2639" x2="7.8613" y2="3.2893" layer="21"/>
<rectangle x1="8.1407" y1="3.2639" x2="8.7249" y2="3.2893" layer="21"/>
<rectangle x1="9.2075" y1="3.2639" x2="9.7663" y2="3.2893" layer="21"/>
<rectangle x1="10.2489" y1="3.2639" x2="10.8077" y2="3.2893" layer="21"/>
<rectangle x1="11.1633" y1="3.2639" x2="12.7127" y2="3.2893" layer="21"/>
<rectangle x1="13.0683" y1="3.2639" x2="13.6271" y2="3.2893" layer="21"/>
<rectangle x1="14.0335" y1="3.2639" x2="14.6939" y2="3.2893" layer="21"/>
<rectangle x1="15.0241" y1="3.2639" x2="16.4465" y2="3.2893" layer="21"/>
<rectangle x1="16.7259" y1="3.2639" x2="16.9799" y2="3.2893" layer="21"/>
<rectangle x1="0.6223" y1="3.2893" x2="1.1557" y2="3.3147" layer="21"/>
<rectangle x1="2.0447" y1="3.2893" x2="2.4511" y2="3.3147" layer="21"/>
<rectangle x1="3.9243" y1="3.2893" x2="4.4069" y2="3.3147" layer="21"/>
<rectangle x1="5.5499" y1="3.2893" x2="5.8801" y2="3.3147" layer="21"/>
<rectangle x1="6.4643" y1="3.2893" x2="6.9215" y2="3.3147" layer="21"/>
<rectangle x1="7.5311" y1="3.2893" x2="7.8613" y2="3.3147" layer="21"/>
<rectangle x1="8.1407" y1="3.2893" x2="8.7249" y2="3.3147" layer="21"/>
<rectangle x1="9.2075" y1="3.2893" x2="9.7663" y2="3.3147" layer="21"/>
<rectangle x1="10.2489" y1="3.2893" x2="10.8077" y2="3.3147" layer="21"/>
<rectangle x1="11.1379" y1="3.2893" x2="12.7127" y2="3.3147" layer="21"/>
<rectangle x1="13.0683" y1="3.2893" x2="13.6271" y2="3.3147" layer="21"/>
<rectangle x1="14.0335" y1="3.2893" x2="14.6685" y2="3.3147" layer="21"/>
<rectangle x1="14.9987" y1="3.2893" x2="16.4465" y2="3.3147" layer="21"/>
<rectangle x1="16.7513" y1="3.2893" x2="16.9545" y2="3.3147" layer="21"/>
<rectangle x1="0.6223" y1="3.3147" x2="1.1557" y2="3.3401" layer="21"/>
<rectangle x1="2.0447" y1="3.3147" x2="2.4257" y2="3.3401" layer="21"/>
<rectangle x1="3.9243" y1="3.3147" x2="4.3815" y2="3.3401" layer="21"/>
<rectangle x1="5.5499" y1="3.3147" x2="5.8801" y2="3.3401" layer="21"/>
<rectangle x1="6.4643" y1="3.3147" x2="6.9215" y2="3.3401" layer="21"/>
<rectangle x1="7.5311" y1="3.3147" x2="7.8613" y2="3.3401" layer="21"/>
<rectangle x1="8.1407" y1="3.3147" x2="8.7249" y2="3.3401" layer="21"/>
<rectangle x1="9.2075" y1="3.3147" x2="9.7663" y2="3.3401" layer="21"/>
<rectangle x1="10.2489" y1="3.3147" x2="10.8077" y2="3.3401" layer="21"/>
<rectangle x1="11.1379" y1="3.3147" x2="12.7127" y2="3.3401" layer="21"/>
<rectangle x1="13.0683" y1="3.3147" x2="13.6271" y2="3.3401" layer="21"/>
<rectangle x1="14.0081" y1="3.3147" x2="14.6431" y2="3.3401" layer="21"/>
<rectangle x1="14.9733" y1="3.3147" x2="16.4211" y2="3.3401" layer="21"/>
<rectangle x1="0.6223" y1="3.3401" x2="1.1557" y2="3.3655" layer="21"/>
<rectangle x1="2.0447" y1="3.3401" x2="2.4257" y2="3.3655" layer="21"/>
<rectangle x1="3.8989" y1="3.3401" x2="4.3815" y2="3.3655" layer="21"/>
<rectangle x1="5.5245" y1="3.3401" x2="5.8801" y2="3.3655" layer="21"/>
<rectangle x1="6.4643" y1="3.3401" x2="6.9215" y2="3.3655" layer="21"/>
<rectangle x1="7.5311" y1="3.3401" x2="7.8613" y2="3.3655" layer="21"/>
<rectangle x1="8.1407" y1="3.3401" x2="8.7249" y2="3.3655" layer="21"/>
<rectangle x1="9.2075" y1="3.3401" x2="9.7663" y2="3.3655" layer="21"/>
<rectangle x1="10.2489" y1="3.3401" x2="10.8077" y2="3.3655" layer="21"/>
<rectangle x1="11.1125" y1="3.3401" x2="11.8745" y2="3.3655" layer="21"/>
<rectangle x1="12.0523" y1="3.3401" x2="12.7127" y2="3.3655" layer="21"/>
<rectangle x1="13.0683" y1="3.3401" x2="13.6271" y2="3.3655" layer="21"/>
<rectangle x1="14.0081" y1="3.3401" x2="14.6431" y2="3.3655" layer="21"/>
<rectangle x1="14.9733" y1="3.3401" x2="16.4211" y2="3.3655" layer="21"/>
<rectangle x1="0.6223" y1="3.3655" x2="1.1303" y2="3.3909" layer="21"/>
<rectangle x1="2.0447" y1="3.3655" x2="2.4003" y2="3.3909" layer="21"/>
<rectangle x1="3.8989" y1="3.3655" x2="4.3561" y2="3.3909" layer="21"/>
<rectangle x1="5.5245" y1="3.3655" x2="5.8801" y2="3.3909" layer="21"/>
<rectangle x1="6.4643" y1="3.3655" x2="6.9215" y2="3.3909" layer="21"/>
<rectangle x1="7.5311" y1="3.3655" x2="7.8613" y2="3.3909" layer="21"/>
<rectangle x1="8.1407" y1="3.3655" x2="8.7249" y2="3.3909" layer="21"/>
<rectangle x1="9.2075" y1="3.3655" x2="9.7663" y2="3.3909" layer="21"/>
<rectangle x1="10.2489" y1="3.3655" x2="10.8077" y2="3.3909" layer="21"/>
<rectangle x1="11.1125" y1="3.3655" x2="11.7729" y2="3.3909" layer="21"/>
<rectangle x1="12.1539" y1="3.3655" x2="12.7127" y2="3.3909" layer="21"/>
<rectangle x1="13.0683" y1="3.3655" x2="13.6271" y2="3.3909" layer="21"/>
<rectangle x1="13.9827" y1="3.3655" x2="14.6177" y2="3.3909" layer="21"/>
<rectangle x1="14.9479" y1="3.3655" x2="16.4211" y2="3.3909" layer="21"/>
<rectangle x1="0.6223" y1="3.3909" x2="1.1303" y2="3.4163" layer="21"/>
<rectangle x1="2.0447" y1="3.3909" x2="2.4003" y2="3.4163" layer="21"/>
<rectangle x1="3.8989" y1="3.3909" x2="4.3561" y2="3.4163" layer="21"/>
<rectangle x1="5.4991" y1="3.3909" x2="5.8801" y2="3.4163" layer="21"/>
<rectangle x1="6.4643" y1="3.3909" x2="6.9215" y2="3.4163" layer="21"/>
<rectangle x1="7.5311" y1="3.3909" x2="7.8613" y2="3.4163" layer="21"/>
<rectangle x1="8.1407" y1="3.3909" x2="8.7249" y2="3.4163" layer="21"/>
<rectangle x1="9.2075" y1="3.3909" x2="9.7663" y2="3.4163" layer="21"/>
<rectangle x1="10.2489" y1="3.3909" x2="10.8077" y2="3.4163" layer="21"/>
<rectangle x1="11.1125" y1="3.3909" x2="11.7221" y2="3.4163" layer="21"/>
<rectangle x1="12.1793" y1="3.3909" x2="12.7127" y2="3.4163" layer="21"/>
<rectangle x1="13.0683" y1="3.3909" x2="13.6271" y2="3.4163" layer="21"/>
<rectangle x1="13.9573" y1="3.3909" x2="14.5923" y2="3.4163" layer="21"/>
<rectangle x1="14.9225" y1="3.3909" x2="15.7353" y2="3.4163" layer="21"/>
<rectangle x1="15.9639" y1="3.3909" x2="16.3957" y2="3.4163" layer="21"/>
<rectangle x1="0.6223" y1="3.4163" x2="1.1303" y2="3.4417" layer="21"/>
<rectangle x1="2.0447" y1="3.4163" x2="2.3749" y2="3.4417" layer="21"/>
<rectangle x1="3.1369" y1="3.4163" x2="3.5179" y2="3.4417" layer="21"/>
<rectangle x1="3.8735" y1="3.4163" x2="4.3307" y2="3.4417" layer="21"/>
<rectangle x1="5.4991" y1="3.4163" x2="5.8801" y2="3.4417" layer="21"/>
<rectangle x1="6.4643" y1="3.4163" x2="6.9215" y2="3.4417" layer="21"/>
<rectangle x1="7.5311" y1="3.4163" x2="7.8613" y2="3.4417" layer="21"/>
<rectangle x1="8.1407" y1="3.4163" x2="8.7249" y2="3.4417" layer="21"/>
<rectangle x1="9.2075" y1="3.4163" x2="9.7663" y2="3.4417" layer="21"/>
<rectangle x1="10.2489" y1="3.4163" x2="10.8077" y2="3.4417" layer="21"/>
<rectangle x1="11.1125" y1="3.4163" x2="11.6967" y2="3.4417" layer="21"/>
<rectangle x1="12.1793" y1="3.4163" x2="12.7127" y2="3.4417" layer="21"/>
<rectangle x1="13.0683" y1="3.4163" x2="13.6271" y2="3.4417" layer="21"/>
<rectangle x1="13.9573" y1="3.4163" x2="14.5923" y2="3.4417" layer="21"/>
<rectangle x1="14.9225" y1="3.4163" x2="15.6337" y2="3.4417" layer="21"/>
<rectangle x1="16.1163" y1="3.4163" x2="16.3957" y2="3.4417" layer="21"/>
<rectangle x1="0.6223" y1="3.4417" x2="1.1303" y2="3.4671" layer="21"/>
<rectangle x1="2.0447" y1="3.4417" x2="2.3749" y2="3.4671" layer="21"/>
<rectangle x1="3.0607" y1="3.4417" x2="3.6449" y2="3.4671" layer="21"/>
<rectangle x1="3.8735" y1="3.4417" x2="4.3307" y2="3.4671" layer="21"/>
<rectangle x1="5.1181" y1="3.4417" x2="5.3213" y2="3.4671" layer="21"/>
<rectangle x1="5.4991" y1="3.4417" x2="5.8801" y2="3.4671" layer="21"/>
<rectangle x1="6.4643" y1="3.4417" x2="6.9215" y2="3.4671" layer="21"/>
<rectangle x1="7.5311" y1="3.4417" x2="7.8613" y2="3.4671" layer="21"/>
<rectangle x1="8.1407" y1="3.4417" x2="8.7249" y2="3.4671" layer="21"/>
<rectangle x1="9.2075" y1="3.4417" x2="9.7663" y2="3.4671" layer="21"/>
<rectangle x1="10.2489" y1="3.4417" x2="10.8077" y2="3.4671" layer="21"/>
<rectangle x1="11.1125" y1="3.4417" x2="11.6713" y2="3.4671" layer="21"/>
<rectangle x1="12.1793" y1="3.4417" x2="12.7127" y2="3.4671" layer="21"/>
<rectangle x1="13.0683" y1="3.4417" x2="13.6271" y2="3.4671" layer="21"/>
<rectangle x1="13.9319" y1="3.4417" x2="14.5669" y2="3.4671" layer="21"/>
<rectangle x1="14.8971" y1="3.4417" x2="15.5575" y2="3.4671" layer="21"/>
<rectangle x1="16.2179" y1="3.4417" x2="16.3957" y2="3.4671" layer="21"/>
<rectangle x1="0.6223" y1="3.4671" x2="1.1303" y2="3.4925" layer="21"/>
<rectangle x1="1.8415" y1="3.4671" x2="1.9177" y2="3.4925" layer="21"/>
<rectangle x1="2.0447" y1="3.4671" x2="2.3495" y2="3.4925" layer="21"/>
<rectangle x1="3.0099" y1="3.4671" x2="3.7211" y2="3.4925" layer="21"/>
<rectangle x1="3.8481" y1="3.4671" x2="4.3307" y2="3.4925" layer="21"/>
<rectangle x1="5.0673" y1="3.4671" x2="5.3975" y2="3.4925" layer="21"/>
<rectangle x1="5.4737" y1="3.4671" x2="5.8801" y2="3.4925" layer="21"/>
<rectangle x1="6.4643" y1="3.4671" x2="6.9215" y2="3.4925" layer="21"/>
<rectangle x1="7.5311" y1="3.4671" x2="7.8613" y2="3.4925" layer="21"/>
<rectangle x1="8.1407" y1="3.4671" x2="8.7249" y2="3.4925" layer="21"/>
<rectangle x1="9.2075" y1="3.4671" x2="9.7663" y2="3.4925" layer="21"/>
<rectangle x1="10.2489" y1="3.4671" x2="10.8077" y2="3.4925" layer="21"/>
<rectangle x1="11.0871" y1="3.4671" x2="11.6459" y2="3.4925" layer="21"/>
<rectangle x1="12.1793" y1="3.4671" x2="12.7127" y2="3.4925" layer="21"/>
<rectangle x1="13.0683" y1="3.4671" x2="13.6271" y2="3.4925" layer="21"/>
<rectangle x1="13.9065" y1="3.4671" x2="14.5669" y2="3.4925" layer="21"/>
<rectangle x1="14.8971" y1="3.4671" x2="15.5321" y2="3.4925" layer="21"/>
<rectangle x1="16.2941" y1="3.4671" x2="16.3703" y2="3.4925" layer="21"/>
<rectangle x1="0.6223" y1="3.4925" x2="1.1303" y2="3.5179" layer="21"/>
<rectangle x1="1.7653" y1="3.4925" x2="2.3495" y2="3.5179" layer="21"/>
<rectangle x1="2.9845" y1="3.4925" x2="3.7973" y2="3.5179" layer="21"/>
<rectangle x1="3.8481" y1="3.4925" x2="4.3053" y2="3.5179" layer="21"/>
<rectangle x1="5.0419" y1="3.4925" x2="5.4483" y2="3.5179" layer="21"/>
<rectangle x1="5.4737" y1="3.4925" x2="5.8801" y2="3.5179" layer="21"/>
<rectangle x1="6.4643" y1="3.4925" x2="6.9215" y2="3.5179" layer="21"/>
<rectangle x1="7.5311" y1="3.4925" x2="7.8613" y2="3.5179" layer="21"/>
<rectangle x1="8.1407" y1="3.4925" x2="8.7249" y2="3.5179" layer="21"/>
<rectangle x1="9.2075" y1="3.4925" x2="9.7663" y2="3.5179" layer="21"/>
<rectangle x1="10.2489" y1="3.4925" x2="10.8077" y2="3.5179" layer="21"/>
<rectangle x1="11.0871" y1="3.4925" x2="11.6459" y2="3.5179" layer="21"/>
<rectangle x1="12.1793" y1="3.4925" x2="12.7127" y2="3.5179" layer="21"/>
<rectangle x1="13.0683" y1="3.4925" x2="13.6271" y2="3.5179" layer="21"/>
<rectangle x1="13.9065" y1="3.4925" x2="14.5415" y2="3.5179" layer="21"/>
<rectangle x1="14.8971" y1="3.4925" x2="15.5067" y2="3.5179" layer="21"/>
<rectangle x1="16.3449" y1="3.4925" x2="16.3703" y2="3.5179" layer="21"/>
<rectangle x1="0.6223" y1="3.5179" x2="1.1303" y2="3.5433" layer="21"/>
<rectangle x1="1.7399" y1="3.5179" x2="2.3241" y2="3.5433" layer="21"/>
<rectangle x1="2.9591" y1="3.5179" x2="4.3053" y2="3.5433" layer="21"/>
<rectangle x1="4.9911" y1="3.5179" x2="5.8801" y2="3.5433" layer="21"/>
<rectangle x1="6.4643" y1="3.5179" x2="6.9215" y2="3.5433" layer="21"/>
<rectangle x1="7.5311" y1="3.5179" x2="7.8613" y2="3.5433" layer="21"/>
<rectangle x1="8.1407" y1="3.5179" x2="8.7249" y2="3.5433" layer="21"/>
<rectangle x1="9.2075" y1="3.5179" x2="9.7663" y2="3.5433" layer="21"/>
<rectangle x1="10.2489" y1="3.5179" x2="10.8077" y2="3.5433" layer="21"/>
<rectangle x1="11.0871" y1="3.5179" x2="11.6205" y2="3.5433" layer="21"/>
<rectangle x1="12.1793" y1="3.5179" x2="12.7127" y2="3.5433" layer="21"/>
<rectangle x1="13.0683" y1="3.5179" x2="13.6271" y2="3.5433" layer="21"/>
<rectangle x1="13.8811" y1="3.5179" x2="14.5161" y2="3.5433" layer="21"/>
<rectangle x1="14.8717" y1="3.5179" x2="15.4813" y2="3.5433" layer="21"/>
<rectangle x1="0.6223" y1="3.5433" x2="1.1303" y2="3.5687" layer="21"/>
<rectangle x1="1.7145" y1="3.5433" x2="2.3241" y2="3.5687" layer="21"/>
<rectangle x1="2.9337" y1="3.5433" x2="4.3053" y2="3.5687" layer="21"/>
<rectangle x1="4.9911" y1="3.5433" x2="5.8801" y2="3.5687" layer="21"/>
<rectangle x1="6.4643" y1="3.5433" x2="6.9215" y2="3.5687" layer="21"/>
<rectangle x1="7.5311" y1="3.5433" x2="7.8613" y2="3.5687" layer="21"/>
<rectangle x1="8.1407" y1="3.5433" x2="8.7249" y2="3.5687" layer="21"/>
<rectangle x1="9.2075" y1="3.5433" x2="9.7663" y2="3.5687" layer="21"/>
<rectangle x1="10.2489" y1="3.5433" x2="10.8077" y2="3.5687" layer="21"/>
<rectangle x1="11.0871" y1="3.5433" x2="11.6205" y2="3.5687" layer="21"/>
<rectangle x1="12.1793" y1="3.5433" x2="12.7127" y2="3.5687" layer="21"/>
<rectangle x1="13.0683" y1="3.5433" x2="13.6271" y2="3.5687" layer="21"/>
<rectangle x1="13.8811" y1="3.5433" x2="14.5161" y2="3.5687" layer="21"/>
<rectangle x1="14.8717" y1="3.5433" x2="15.4559" y2="3.5687" layer="21"/>
<rectangle x1="0.6223" y1="3.5687" x2="1.1303" y2="3.5941" layer="21"/>
<rectangle x1="1.7145" y1="3.5687" x2="2.3241" y2="3.5941" layer="21"/>
<rectangle x1="2.9083" y1="3.5687" x2="4.2799" y2="3.5941" layer="21"/>
<rectangle x1="4.9657" y1="3.5687" x2="5.8801" y2="3.5941" layer="21"/>
<rectangle x1="6.4643" y1="3.5687" x2="6.9215" y2="3.5941" layer="21"/>
<rectangle x1="7.5311" y1="3.5687" x2="7.8613" y2="3.5941" layer="21"/>
<rectangle x1="8.1407" y1="3.5687" x2="8.7249" y2="3.5941" layer="21"/>
<rectangle x1="9.2075" y1="3.5687" x2="9.7663" y2="3.5941" layer="21"/>
<rectangle x1="10.2489" y1="3.5687" x2="10.8077" y2="3.5941" layer="21"/>
<rectangle x1="11.0871" y1="3.5687" x2="11.6205" y2="3.5941" layer="21"/>
<rectangle x1="12.1793" y1="3.5687" x2="12.7127" y2="3.5941" layer="21"/>
<rectangle x1="13.0683" y1="3.5687" x2="13.6271" y2="3.5941" layer="21"/>
<rectangle x1="13.8557" y1="3.5687" x2="14.4907" y2="3.5941" layer="21"/>
<rectangle x1="14.8717" y1="3.5687" x2="15.4305" y2="3.5941" layer="21"/>
<rectangle x1="0.6223" y1="3.5941" x2="1.1303" y2="3.6195" layer="21"/>
<rectangle x1="1.7145" y1="3.5941" x2="2.2987" y2="3.6195" layer="21"/>
<rectangle x1="2.8829" y1="3.5941" x2="4.2799" y2="3.6195" layer="21"/>
<rectangle x1="4.9403" y1="3.5941" x2="5.8801" y2="3.6195" layer="21"/>
<rectangle x1="6.4643" y1="3.5941" x2="6.9215" y2="3.6195" layer="21"/>
<rectangle x1="7.5311" y1="3.5941" x2="7.8613" y2="3.6195" layer="21"/>
<rectangle x1="8.1407" y1="3.5941" x2="8.7249" y2="3.6195" layer="21"/>
<rectangle x1="9.2075" y1="3.5941" x2="9.7663" y2="3.6195" layer="21"/>
<rectangle x1="10.2489" y1="3.5941" x2="10.8077" y2="3.6195" layer="21"/>
<rectangle x1="11.0871" y1="3.5941" x2="11.6205" y2="3.6195" layer="21"/>
<rectangle x1="12.1793" y1="3.5941" x2="12.7127" y2="3.6195" layer="21"/>
<rectangle x1="13.0683" y1="3.5941" x2="13.6271" y2="3.6195" layer="21"/>
<rectangle x1="13.8303" y1="3.5941" x2="14.4653" y2="3.6195" layer="21"/>
<rectangle x1="14.8463" y1="3.5941" x2="15.4051" y2="3.6195" layer="21"/>
<rectangle x1="0.6223" y1="3.6195" x2="1.1303" y2="3.6449" layer="21"/>
<rectangle x1="1.7145" y1="3.6195" x2="2.2987" y2="3.6449" layer="21"/>
<rectangle x1="2.8829" y1="3.6195" x2="4.2799" y2="3.6449" layer="21"/>
<rectangle x1="4.9403" y1="3.6195" x2="5.8801" y2="3.6449" layer="21"/>
<rectangle x1="6.4643" y1="3.6195" x2="6.9215" y2="3.6449" layer="21"/>
<rectangle x1="7.5311" y1="3.6195" x2="7.8613" y2="3.6449" layer="21"/>
<rectangle x1="8.1407" y1="3.6195" x2="8.7249" y2="3.6449" layer="21"/>
<rectangle x1="9.2075" y1="3.6195" x2="9.7663" y2="3.6449" layer="21"/>
<rectangle x1="10.2489" y1="3.6195" x2="10.8077" y2="3.6449" layer="21"/>
<rectangle x1="11.0871" y1="3.6195" x2="11.6205" y2="3.6449" layer="21"/>
<rectangle x1="12.1793" y1="3.6195" x2="12.7127" y2="3.6449" layer="21"/>
<rectangle x1="13.0683" y1="3.6195" x2="13.6271" y2="3.6449" layer="21"/>
<rectangle x1="13.8303" y1="3.6195" x2="14.4653" y2="3.6449" layer="21"/>
<rectangle x1="14.8463" y1="3.6195" x2="15.4051" y2="3.6449" layer="21"/>
<rectangle x1="0.6223" y1="3.6449" x2="1.1303" y2="3.6703" layer="21"/>
<rectangle x1="1.7145" y1="3.6449" x2="2.2987" y2="3.6703" layer="21"/>
<rectangle x1="2.8829" y1="3.6449" x2="4.2799" y2="3.6703" layer="21"/>
<rectangle x1="4.9149" y1="3.6449" x2="5.8801" y2="3.6703" layer="21"/>
<rectangle x1="6.4643" y1="3.6449" x2="6.9215" y2="3.6703" layer="21"/>
<rectangle x1="7.5311" y1="3.6449" x2="7.8613" y2="3.6703" layer="21"/>
<rectangle x1="8.1407" y1="3.6449" x2="8.7249" y2="3.6703" layer="21"/>
<rectangle x1="9.2075" y1="3.6449" x2="9.7663" y2="3.6703" layer="21"/>
<rectangle x1="10.2489" y1="3.6449" x2="10.8077" y2="3.6703" layer="21"/>
<rectangle x1="11.0871" y1="3.6449" x2="11.6459" y2="3.6703" layer="21"/>
<rectangle x1="12.1793" y1="3.6449" x2="12.7127" y2="3.6703" layer="21"/>
<rectangle x1="13.0683" y1="3.6449" x2="13.6271" y2="3.6703" layer="21"/>
<rectangle x1="13.8049" y1="3.6449" x2="14.4399" y2="3.6703" layer="21"/>
<rectangle x1="14.8463" y1="3.6449" x2="15.3797" y2="3.6703" layer="21"/>
<rectangle x1="0.6223" y1="3.6703" x2="1.1303" y2="3.6957" layer="21"/>
<rectangle x1="1.7145" y1="3.6703" x2="2.2987" y2="3.6957" layer="21"/>
<rectangle x1="2.8575" y1="3.6703" x2="4.2545" y2="3.6957" layer="21"/>
<rectangle x1="4.9149" y1="3.6703" x2="5.8801" y2="3.6957" layer="21"/>
<rectangle x1="6.4643" y1="3.6703" x2="6.9215" y2="3.6957" layer="21"/>
<rectangle x1="7.5311" y1="3.6703" x2="7.8613" y2="3.6957" layer="21"/>
<rectangle x1="8.1407" y1="3.6703" x2="8.7249" y2="3.6957" layer="21"/>
<rectangle x1="9.2075" y1="3.6703" x2="9.7663" y2="3.6957" layer="21"/>
<rectangle x1="10.2489" y1="3.6703" x2="10.8077" y2="3.6957" layer="21"/>
<rectangle x1="11.1125" y1="3.6703" x2="11.6459" y2="3.6957" layer="21"/>
<rectangle x1="12.1793" y1="3.6703" x2="12.7127" y2="3.6957" layer="21"/>
<rectangle x1="13.0683" y1="3.6703" x2="13.6271" y2="3.6957" layer="21"/>
<rectangle x1="13.8049" y1="3.6703" x2="14.4145" y2="3.6957" layer="21"/>
<rectangle x1="14.8463" y1="3.6703" x2="15.3797" y2="3.6957" layer="21"/>
<rectangle x1="0.6223" y1="3.6957" x2="1.1303" y2="3.7211" layer="21"/>
<rectangle x1="1.7145" y1="3.6957" x2="2.2987" y2="3.7211" layer="21"/>
<rectangle x1="3.0099" y1="3.6957" x2="4.2545" y2="3.7211" layer="21"/>
<rectangle x1="4.8895" y1="3.6957" x2="5.8801" y2="3.7211" layer="21"/>
<rectangle x1="6.4643" y1="3.6957" x2="6.9215" y2="3.7211" layer="21"/>
<rectangle x1="7.5311" y1="3.6957" x2="7.8613" y2="3.7211" layer="21"/>
<rectangle x1="8.1407" y1="3.6957" x2="8.7249" y2="3.7211" layer="21"/>
<rectangle x1="9.2075" y1="3.6957" x2="9.7663" y2="3.7211" layer="21"/>
<rectangle x1="10.2489" y1="3.6957" x2="10.8077" y2="3.7211" layer="21"/>
<rectangle x1="11.1125" y1="3.6957" x2="11.6713" y2="3.7211" layer="21"/>
<rectangle x1="12.1793" y1="3.6957" x2="12.7127" y2="3.7211" layer="21"/>
<rectangle x1="13.0683" y1="3.6957" x2="13.6271" y2="3.7211" layer="21"/>
<rectangle x1="13.7795" y1="3.6957" x2="14.4145" y2="3.7211" layer="21"/>
<rectangle x1="14.8209" y1="3.6957" x2="15.4305" y2="3.7211" layer="21"/>
<rectangle x1="0.6223" y1="3.7211" x2="1.1303" y2="3.7465" layer="21"/>
<rectangle x1="1.7145" y1="3.7211" x2="2.2733" y2="3.7465" layer="21"/>
<rectangle x1="3.1877" y1="3.7211" x2="4.2545" y2="3.7465" layer="21"/>
<rectangle x1="4.8895" y1="3.7211" x2="5.8801" y2="3.7465" layer="21"/>
<rectangle x1="6.4643" y1="3.7211" x2="6.9215" y2="3.7465" layer="21"/>
<rectangle x1="7.5311" y1="3.7211" x2="7.8613" y2="3.7465" layer="21"/>
<rectangle x1="8.1407" y1="3.7211" x2="8.7249" y2="3.7465" layer="21"/>
<rectangle x1="9.2075" y1="3.7211" x2="9.7663" y2="3.7465" layer="21"/>
<rectangle x1="10.2489" y1="3.7211" x2="10.8077" y2="3.7465" layer="21"/>
<rectangle x1="11.1125" y1="3.7211" x2="11.6967" y2="3.7465" layer="21"/>
<rectangle x1="12.1793" y1="3.7211" x2="12.7127" y2="3.7465" layer="21"/>
<rectangle x1="13.0683" y1="3.7211" x2="13.6271" y2="3.7465" layer="21"/>
<rectangle x1="13.7541" y1="3.7211" x2="14.3891" y2="3.7465" layer="21"/>
<rectangle x1="14.8209" y1="3.7211" x2="15.6337" y2="3.7465" layer="21"/>
<rectangle x1="0.6223" y1="3.7465" x2="1.1303" y2="3.7719" layer="21"/>
<rectangle x1="1.7145" y1="3.7465" x2="2.2733" y2="3.7719" layer="21"/>
<rectangle x1="3.3909" y1="3.7465" x2="4.2545" y2="3.7719" layer="21"/>
<rectangle x1="4.8895" y1="3.7465" x2="5.8801" y2="3.7719" layer="21"/>
<rectangle x1="6.4643" y1="3.7465" x2="6.9215" y2="3.7719" layer="21"/>
<rectangle x1="7.5311" y1="3.7465" x2="7.8613" y2="3.7719" layer="21"/>
<rectangle x1="8.1407" y1="3.7465" x2="8.7249" y2="3.7719" layer="21"/>
<rectangle x1="9.2075" y1="3.7465" x2="9.7663" y2="3.7719" layer="21"/>
<rectangle x1="10.2489" y1="3.7465" x2="10.8077" y2="3.7719" layer="21"/>
<rectangle x1="11.1125" y1="3.7465" x2="11.7221" y2="3.7719" layer="21"/>
<rectangle x1="12.1793" y1="3.7465" x2="12.7127" y2="3.7719" layer="21"/>
<rectangle x1="13.0683" y1="3.7465" x2="13.6271" y2="3.7719" layer="21"/>
<rectangle x1="13.7541" y1="3.7465" x2="14.3891" y2="3.7719" layer="21"/>
<rectangle x1="14.8209" y1="3.7465" x2="15.8115" y2="3.7719" layer="21"/>
<rectangle x1="0.6223" y1="3.7719" x2="1.1303" y2="3.7973" layer="21"/>
<rectangle x1="1.7145" y1="3.7719" x2="2.2733" y2="3.7973" layer="21"/>
<rectangle x1="3.5687" y1="3.7719" x2="4.2545" y2="3.7973" layer="21"/>
<rectangle x1="4.8895" y1="3.7719" x2="5.8801" y2="3.7973" layer="21"/>
<rectangle x1="6.4643" y1="3.7719" x2="6.9215" y2="3.7973" layer="21"/>
<rectangle x1="7.5311" y1="3.7719" x2="7.8613" y2="3.7973" layer="21"/>
<rectangle x1="8.1407" y1="3.7719" x2="8.7249" y2="3.7973" layer="21"/>
<rectangle x1="9.2075" y1="3.7719" x2="9.7663" y2="3.7973" layer="21"/>
<rectangle x1="10.2489" y1="3.7719" x2="10.8077" y2="3.7973" layer="21"/>
<rectangle x1="11.1379" y1="3.7719" x2="11.7475" y2="3.7973" layer="21"/>
<rectangle x1="12.1793" y1="3.7719" x2="12.7127" y2="3.7973" layer="21"/>
<rectangle x1="13.0683" y1="3.7719" x2="13.6271" y2="3.7973" layer="21"/>
<rectangle x1="13.7287" y1="3.7719" x2="14.3637" y2="3.7973" layer="21"/>
<rectangle x1="14.8209" y1="3.7719" x2="16.0147" y2="3.7973" layer="21"/>
<rectangle x1="0.6223" y1="3.7973" x2="1.1303" y2="3.8227" layer="21"/>
<rectangle x1="1.7145" y1="3.7973" x2="2.2733" y2="3.8227" layer="21"/>
<rectangle x1="3.7719" y1="3.7973" x2="4.2545" y2="3.8227" layer="21"/>
<rectangle x1="4.8641" y1="3.7973" x2="5.8801" y2="3.8227" layer="21"/>
<rectangle x1="6.4643" y1="3.7973" x2="6.9215" y2="3.8227" layer="21"/>
<rectangle x1="7.5311" y1="3.7973" x2="7.8613" y2="3.8227" layer="21"/>
<rectangle x1="8.1407" y1="3.7973" x2="8.7249" y2="3.8227" layer="21"/>
<rectangle x1="9.2075" y1="3.7973" x2="9.7663" y2="3.8227" layer="21"/>
<rectangle x1="10.2489" y1="3.7973" x2="10.8077" y2="3.8227" layer="21"/>
<rectangle x1="11.1379" y1="3.7973" x2="11.8237" y2="3.8227" layer="21"/>
<rectangle x1="12.1793" y1="3.7973" x2="12.7127" y2="3.8227" layer="21"/>
<rectangle x1="13.0683" y1="3.7973" x2="13.6271" y2="3.8227" layer="21"/>
<rectangle x1="13.7033" y1="3.7973" x2="14.3383" y2="3.8227" layer="21"/>
<rectangle x1="14.8209" y1="3.7973" x2="16.2179" y2="3.8227" layer="21"/>
<rectangle x1="0.6223" y1="3.8227" x2="1.1303" y2="3.8481" layer="21"/>
<rectangle x1="1.7145" y1="3.8227" x2="2.2733" y2="3.8481" layer="21"/>
<rectangle x1="3.9751" y1="3.8227" x2="4.2545" y2="3.8481" layer="21"/>
<rectangle x1="4.8641" y1="3.8227" x2="5.8801" y2="3.8481" layer="21"/>
<rectangle x1="6.4643" y1="3.8227" x2="6.9215" y2="3.8481" layer="21"/>
<rectangle x1="7.5311" y1="3.8227" x2="7.8613" y2="3.8481" layer="21"/>
<rectangle x1="8.1407" y1="3.8227" x2="8.7249" y2="3.8481" layer="21"/>
<rectangle x1="9.2075" y1="3.8227" x2="9.7663" y2="3.8481" layer="21"/>
<rectangle x1="10.2489" y1="3.8227" x2="10.8077" y2="3.8481" layer="21"/>
<rectangle x1="11.1633" y1="3.8227" x2="11.9507" y2="3.8481" layer="21"/>
<rectangle x1="12.1793" y1="3.8227" x2="12.7127" y2="3.8481" layer="21"/>
<rectangle x1="13.0683" y1="3.8227" x2="13.6271" y2="3.8481" layer="21"/>
<rectangle x1="13.7033" y1="3.8227" x2="14.3383" y2="3.8481" layer="21"/>
<rectangle x1="14.8209" y1="3.8227" x2="16.3957" y2="3.8481" layer="21"/>
<rectangle x1="0.6223" y1="3.8481" x2="1.1303" y2="3.8735" layer="21"/>
<rectangle x1="1.7145" y1="3.8481" x2="2.2733" y2="3.8735" layer="21"/>
<rectangle x1="4.0259" y1="3.8481" x2="4.2545" y2="3.8735" layer="21"/>
<rectangle x1="4.8641" y1="3.8481" x2="5.8801" y2="3.8735" layer="21"/>
<rectangle x1="6.4643" y1="3.8481" x2="6.9215" y2="3.8735" layer="21"/>
<rectangle x1="7.5311" y1="3.8481" x2="7.8613" y2="3.8735" layer="21"/>
<rectangle x1="8.1407" y1="3.8481" x2="8.7249" y2="3.8735" layer="21"/>
<rectangle x1="9.2075" y1="3.8481" x2="9.7663" y2="3.8735" layer="21"/>
<rectangle x1="10.2489" y1="3.8481" x2="10.8077" y2="3.8735" layer="21"/>
<rectangle x1="11.1633" y1="3.8481" x2="12.7127" y2="3.8735" layer="21"/>
<rectangle x1="13.0683" y1="3.8481" x2="13.6271" y2="3.8735" layer="21"/>
<rectangle x1="13.6779" y1="3.8481" x2="14.3129" y2="3.8735" layer="21"/>
<rectangle x1="14.8209" y1="3.8481" x2="16.5481" y2="3.8735" layer="21"/>
<rectangle x1="0.6223" y1="3.8735" x2="1.1303" y2="3.8989" layer="21"/>
<rectangle x1="1.7145" y1="3.8735" x2="2.2733" y2="3.8989" layer="21"/>
<rectangle x1="4.0259" y1="3.8735" x2="4.2545" y2="3.8989" layer="21"/>
<rectangle x1="4.8641" y1="3.8735" x2="5.8801" y2="3.8989" layer="21"/>
<rectangle x1="6.4643" y1="3.8735" x2="6.9215" y2="3.8989" layer="21"/>
<rectangle x1="7.5311" y1="3.8735" x2="7.8613" y2="3.8989" layer="21"/>
<rectangle x1="8.1407" y1="3.8735" x2="8.7249" y2="3.8989" layer="21"/>
<rectangle x1="9.2075" y1="3.8735" x2="9.7663" y2="3.8989" layer="21"/>
<rectangle x1="10.2489" y1="3.8735" x2="10.8077" y2="3.8989" layer="21"/>
<rectangle x1="11.1887" y1="3.8735" x2="12.7127" y2="3.8989" layer="21"/>
<rectangle x1="13.0683" y1="3.8735" x2="13.6271" y2="3.8989" layer="21"/>
<rectangle x1="13.6779" y1="3.8735" x2="14.2875" y2="3.8989" layer="21"/>
<rectangle x1="14.8209" y1="3.8735" x2="16.5481" y2="3.8989" layer="21"/>
<rectangle x1="0.6223" y1="3.8989" x2="1.1303" y2="3.9243" layer="21"/>
<rectangle x1="1.7145" y1="3.8989" x2="2.2733" y2="3.9243" layer="21"/>
<rectangle x1="4.0259" y1="3.8989" x2="4.2545" y2="3.9243" layer="21"/>
<rectangle x1="4.8641" y1="3.8989" x2="5.8801" y2="3.9243" layer="21"/>
<rectangle x1="6.4643" y1="3.8989" x2="6.9215" y2="3.9243" layer="21"/>
<rectangle x1="7.5311" y1="3.8989" x2="7.8613" y2="3.9243" layer="21"/>
<rectangle x1="8.1407" y1="3.8989" x2="8.7249" y2="3.9243" layer="21"/>
<rectangle x1="9.2075" y1="3.8989" x2="9.7663" y2="3.9243" layer="21"/>
<rectangle x1="10.2489" y1="3.8989" x2="10.8077" y2="3.9243" layer="21"/>
<rectangle x1="11.2141" y1="3.8989" x2="12.7127" y2="3.9243" layer="21"/>
<rectangle x1="13.0683" y1="3.8989" x2="14.2875" y2="3.9243" layer="21"/>
<rectangle x1="14.8209" y1="3.8989" x2="16.5481" y2="3.9243" layer="21"/>
<rectangle x1="0.6223" y1="3.9243" x2="1.1303" y2="3.9497" layer="21"/>
<rectangle x1="1.7145" y1="3.9243" x2="2.2733" y2="3.9497" layer="21"/>
<rectangle x1="4.0259" y1="3.9243" x2="4.2545" y2="3.9497" layer="21"/>
<rectangle x1="4.8641" y1="3.9243" x2="5.8801" y2="3.9497" layer="21"/>
<rectangle x1="6.4643" y1="3.9243" x2="6.9215" y2="3.9497" layer="21"/>
<rectangle x1="7.5311" y1="3.9243" x2="7.8613" y2="3.9497" layer="21"/>
<rectangle x1="8.1407" y1="3.9243" x2="8.7249" y2="3.9497" layer="21"/>
<rectangle x1="9.2075" y1="3.9243" x2="9.7663" y2="3.9497" layer="21"/>
<rectangle x1="10.2489" y1="3.9243" x2="10.8077" y2="3.9497" layer="21"/>
<rectangle x1="11.2141" y1="3.9243" x2="12.7127" y2="3.9497" layer="21"/>
<rectangle x1="13.0683" y1="3.9243" x2="14.2621" y2="3.9497" layer="21"/>
<rectangle x1="14.8209" y1="3.9243" x2="16.5481" y2="3.9497" layer="21"/>
<rectangle x1="0.6223" y1="3.9497" x2="1.1303" y2="3.9751" layer="21"/>
<rectangle x1="1.7145" y1="3.9497" x2="2.2733" y2="3.9751" layer="21"/>
<rectangle x1="4.0259" y1="3.9497" x2="4.2545" y2="3.9751" layer="21"/>
<rectangle x1="4.8641" y1="3.9497" x2="5.8801" y2="3.9751" layer="21"/>
<rectangle x1="6.4643" y1="3.9497" x2="6.9215" y2="3.9751" layer="21"/>
<rectangle x1="7.5311" y1="3.9497" x2="7.8613" y2="3.9751" layer="21"/>
<rectangle x1="8.1407" y1="3.9497" x2="8.7249" y2="3.9751" layer="21"/>
<rectangle x1="9.2075" y1="3.9497" x2="9.7663" y2="3.9751" layer="21"/>
<rectangle x1="10.2489" y1="3.9497" x2="10.8077" y2="3.9751" layer="21"/>
<rectangle x1="11.2649" y1="3.9497" x2="12.7127" y2="3.9751" layer="21"/>
<rectangle x1="13.0683" y1="3.9497" x2="14.2621" y2="3.9751" layer="21"/>
<rectangle x1="14.8209" y1="3.9497" x2="16.5481" y2="3.9751" layer="21"/>
<rectangle x1="0.6223" y1="3.9751" x2="1.1303" y2="4.0005" layer="21"/>
<rectangle x1="1.7145" y1="3.9751" x2="2.2733" y2="4.0005" layer="21"/>
<rectangle x1="4.0259" y1="3.9751" x2="4.2545" y2="4.0005" layer="21"/>
<rectangle x1="4.8641" y1="3.9751" x2="5.8801" y2="4.0005" layer="21"/>
<rectangle x1="6.4643" y1="3.9751" x2="6.9215" y2="4.0005" layer="21"/>
<rectangle x1="7.5311" y1="3.9751" x2="7.8613" y2="4.0005" layer="21"/>
<rectangle x1="8.1407" y1="3.9751" x2="8.7249" y2="4.0005" layer="21"/>
<rectangle x1="9.2075" y1="3.9751" x2="9.7663" y2="4.0005" layer="21"/>
<rectangle x1="10.2489" y1="3.9751" x2="10.8077" y2="4.0005" layer="21"/>
<rectangle x1="11.2903" y1="3.9751" x2="12.7127" y2="4.0005" layer="21"/>
<rectangle x1="13.0683" y1="3.9751" x2="14.2367" y2="4.0005" layer="21"/>
<rectangle x1="14.8209" y1="3.9751" x2="16.5481" y2="4.0005" layer="21"/>
<rectangle x1="0.6223" y1="4.0005" x2="1.1303" y2="4.0259" layer="21"/>
<rectangle x1="1.7145" y1="4.0005" x2="2.2733" y2="4.0259" layer="21"/>
<rectangle x1="4.0259" y1="4.0005" x2="4.2545" y2="4.0259" layer="21"/>
<rectangle x1="4.8641" y1="4.0005" x2="5.8801" y2="4.0259" layer="21"/>
<rectangle x1="6.4643" y1="4.0005" x2="6.9215" y2="4.0259" layer="21"/>
<rectangle x1="7.5311" y1="4.0005" x2="7.8613" y2="4.0259" layer="21"/>
<rectangle x1="8.1407" y1="4.0005" x2="8.7249" y2="4.0259" layer="21"/>
<rectangle x1="9.2075" y1="4.0005" x2="9.7663" y2="4.0259" layer="21"/>
<rectangle x1="10.2489" y1="4.0005" x2="10.8077" y2="4.0259" layer="21"/>
<rectangle x1="11.3157" y1="4.0005" x2="12.7127" y2="4.0259" layer="21"/>
<rectangle x1="13.0683" y1="4.0005" x2="14.2367" y2="4.0259" layer="21"/>
<rectangle x1="14.8209" y1="4.0005" x2="16.5481" y2="4.0259" layer="21"/>
<rectangle x1="0.6223" y1="4.0259" x2="1.1303" y2="4.0513" layer="21"/>
<rectangle x1="1.7145" y1="4.0259" x2="2.2733" y2="4.0513" layer="21"/>
<rectangle x1="4.0259" y1="4.0259" x2="4.2545" y2="4.0513" layer="21"/>
<rectangle x1="4.8641" y1="4.0259" x2="5.8801" y2="4.0513" layer="21"/>
<rectangle x1="6.4643" y1="4.0259" x2="6.9215" y2="4.0513" layer="21"/>
<rectangle x1="7.5311" y1="4.0259" x2="7.8613" y2="4.0513" layer="21"/>
<rectangle x1="8.1407" y1="4.0259" x2="8.7249" y2="4.0513" layer="21"/>
<rectangle x1="9.2075" y1="4.0259" x2="9.7663" y2="4.0513" layer="21"/>
<rectangle x1="10.2489" y1="4.0259" x2="10.8077" y2="4.0513" layer="21"/>
<rectangle x1="11.3665" y1="4.0259" x2="12.7127" y2="4.0513" layer="21"/>
<rectangle x1="13.0683" y1="4.0259" x2="14.2621" y2="4.0513" layer="21"/>
<rectangle x1="14.8209" y1="4.0259" x2="16.5481" y2="4.0513" layer="21"/>
<rectangle x1="0.6223" y1="4.0513" x2="1.1303" y2="4.0767" layer="21"/>
<rectangle x1="1.7145" y1="4.0513" x2="2.2733" y2="4.0767" layer="21"/>
<rectangle x1="4.0259" y1="4.0513" x2="4.2545" y2="4.0767" layer="21"/>
<rectangle x1="4.8641" y1="4.0513" x2="5.8801" y2="4.0767" layer="21"/>
<rectangle x1="6.4643" y1="4.0513" x2="6.9215" y2="4.0767" layer="21"/>
<rectangle x1="7.5311" y1="4.0513" x2="7.8613" y2="4.0767" layer="21"/>
<rectangle x1="8.1407" y1="4.0513" x2="8.7249" y2="4.0767" layer="21"/>
<rectangle x1="9.2075" y1="4.0513" x2="9.7663" y2="4.0767" layer="21"/>
<rectangle x1="10.2489" y1="4.0513" x2="10.8077" y2="4.0767" layer="21"/>
<rectangle x1="11.4173" y1="4.0513" x2="12.7127" y2="4.0767" layer="21"/>
<rectangle x1="13.0683" y1="4.0513" x2="14.2875" y2="4.0767" layer="21"/>
<rectangle x1="14.8209" y1="4.0513" x2="15.3543" y2="4.0767" layer="21"/>
<rectangle x1="15.3797" y1="4.0513" x2="16.5481" y2="4.0767" layer="21"/>
<rectangle x1="0.6223" y1="4.0767" x2="1.1303" y2="4.1021" layer="21"/>
<rectangle x1="1.7145" y1="4.0767" x2="2.2733" y2="4.1021" layer="21"/>
<rectangle x1="2.8321" y1="4.0767" x2="2.9591" y2="4.1021" layer="21"/>
<rectangle x1="4.0259" y1="4.0767" x2="4.2545" y2="4.1021" layer="21"/>
<rectangle x1="4.8895" y1="4.0767" x2="5.8801" y2="4.1021" layer="21"/>
<rectangle x1="6.4643" y1="4.0767" x2="6.9215" y2="4.1021" layer="21"/>
<rectangle x1="7.5311" y1="4.0767" x2="7.8613" y2="4.1021" layer="21"/>
<rectangle x1="8.1407" y1="4.0767" x2="8.7249" y2="4.1021" layer="21"/>
<rectangle x1="9.2075" y1="4.0767" x2="9.7663" y2="4.1021" layer="21"/>
<rectangle x1="10.2489" y1="4.0767" x2="10.8077" y2="4.1021" layer="21"/>
<rectangle x1="11.4681" y1="4.0767" x2="12.7127" y2="4.1021" layer="21"/>
<rectangle x1="13.0683" y1="4.0767" x2="13.6271" y2="4.1021" layer="21"/>
<rectangle x1="13.6525" y1="4.0767" x2="14.2875" y2="4.1021" layer="21"/>
<rectangle x1="14.8209" y1="4.0767" x2="15.3289" y2="4.1021" layer="21"/>
<rectangle x1="15.5829" y1="4.0767" x2="16.5481" y2="4.1021" layer="21"/>
<rectangle x1="0.6223" y1="4.1021" x2="1.1303" y2="4.1275" layer="21"/>
<rectangle x1="1.7145" y1="4.1021" x2="2.2733" y2="4.1275" layer="21"/>
<rectangle x1="2.8321" y1="4.1021" x2="3.1623" y2="4.1275" layer="21"/>
<rectangle x1="4.0259" y1="4.1021" x2="4.2545" y2="4.1275" layer="21"/>
<rectangle x1="4.8895" y1="4.1021" x2="5.8801" y2="4.1275" layer="21"/>
<rectangle x1="6.4643" y1="4.1021" x2="6.9215" y2="4.1275" layer="21"/>
<rectangle x1="7.5311" y1="4.1021" x2="7.8613" y2="4.1275" layer="21"/>
<rectangle x1="8.1407" y1="4.1021" x2="8.7249" y2="4.1275" layer="21"/>
<rectangle x1="9.2075" y1="4.1021" x2="9.7663" y2="4.1275" layer="21"/>
<rectangle x1="10.2489" y1="4.1021" x2="10.8077" y2="4.1275" layer="21"/>
<rectangle x1="11.5443" y1="4.1021" x2="12.7127" y2="4.1275" layer="21"/>
<rectangle x1="13.0683" y1="4.1021" x2="13.6271" y2="4.1275" layer="21"/>
<rectangle x1="13.6779" y1="4.1021" x2="14.3129" y2="4.1275" layer="21"/>
<rectangle x1="14.8209" y1="4.1021" x2="15.3289" y2="4.1275" layer="21"/>
<rectangle x1="15.7861" y1="4.1021" x2="16.5481" y2="4.1275" layer="21"/>
<rectangle x1="0.6223" y1="4.1275" x2="1.1303" y2="4.1529" layer="21"/>
<rectangle x1="1.7145" y1="4.1275" x2="2.2987" y2="4.1529" layer="21"/>
<rectangle x1="2.8321" y1="4.1275" x2="3.3655" y2="4.1529" layer="21"/>
<rectangle x1="4.0259" y1="4.1275" x2="4.2545" y2="4.1529" layer="21"/>
<rectangle x1="4.8895" y1="4.1275" x2="5.8801" y2="4.1529" layer="21"/>
<rectangle x1="6.4643" y1="4.1275" x2="6.9215" y2="4.1529" layer="21"/>
<rectangle x1="7.5311" y1="4.1275" x2="7.8613" y2="4.1529" layer="21"/>
<rectangle x1="8.1407" y1="4.1275" x2="8.7249" y2="4.1529" layer="21"/>
<rectangle x1="9.2075" y1="4.1275" x2="9.7663" y2="4.1529" layer="21"/>
<rectangle x1="10.2489" y1="4.1275" x2="10.8077" y2="4.1529" layer="21"/>
<rectangle x1="11.6205" y1="4.1275" x2="12.7127" y2="4.1529" layer="21"/>
<rectangle x1="13.0683" y1="4.1275" x2="13.6271" y2="4.1529" layer="21"/>
<rectangle x1="13.7033" y1="4.1275" x2="14.3383" y2="4.1529" layer="21"/>
<rectangle x1="14.8209" y1="4.1275" x2="15.3543" y2="4.1529" layer="21"/>
<rectangle x1="15.9893" y1="4.1275" x2="16.5481" y2="4.1529" layer="21"/>
<rectangle x1="0.6223" y1="4.1529" x2="1.1303" y2="4.1783" layer="21"/>
<rectangle x1="1.7145" y1="4.1529" x2="2.2987" y2="4.1783" layer="21"/>
<rectangle x1="2.8321" y1="4.1529" x2="3.4671" y2="4.1783" layer="21"/>
<rectangle x1="4.0259" y1="4.1529" x2="4.2545" y2="4.1783" layer="21"/>
<rectangle x1="4.8895" y1="4.1529" x2="5.8801" y2="4.1783" layer="21"/>
<rectangle x1="6.4643" y1="4.1529" x2="6.9215" y2="4.1783" layer="21"/>
<rectangle x1="7.5311" y1="4.1529" x2="7.8613" y2="4.1783" layer="21"/>
<rectangle x1="8.1407" y1="4.1529" x2="8.7249" y2="4.1783" layer="21"/>
<rectangle x1="9.2075" y1="4.1529" x2="9.7663" y2="4.1783" layer="21"/>
<rectangle x1="10.2489" y1="4.1529" x2="10.8077" y2="4.1783" layer="21"/>
<rectangle x1="11.7221" y1="4.1529" x2="12.7127" y2="4.1783" layer="21"/>
<rectangle x1="13.0683" y1="4.1529" x2="13.6271" y2="4.1783" layer="21"/>
<rectangle x1="13.7033" y1="4.1529" x2="14.3383" y2="4.1783" layer="21"/>
<rectangle x1="14.8463" y1="4.1529" x2="15.3543" y2="4.1783" layer="21"/>
<rectangle x1="15.9893" y1="4.1529" x2="16.5481" y2="4.1783" layer="21"/>
<rectangle x1="0.6223" y1="4.1783" x2="1.1303" y2="4.2037" layer="21"/>
<rectangle x1="1.7145" y1="4.1783" x2="2.2987" y2="4.2037" layer="21"/>
<rectangle x1="2.8321" y1="4.1783" x2="3.4417" y2="4.2037" layer="21"/>
<rectangle x1="4.0259" y1="4.1783" x2="4.2545" y2="4.2037" layer="21"/>
<rectangle x1="4.9149" y1="4.1783" x2="5.8801" y2="4.2037" layer="21"/>
<rectangle x1="6.4643" y1="4.1783" x2="6.9215" y2="4.2037" layer="21"/>
<rectangle x1="7.5311" y1="4.1783" x2="7.8613" y2="4.2037" layer="21"/>
<rectangle x1="8.1407" y1="4.1783" x2="8.7249" y2="4.2037" layer="21"/>
<rectangle x1="9.2075" y1="4.1783" x2="9.7663" y2="4.2037" layer="21"/>
<rectangle x1="10.2489" y1="4.1783" x2="10.8077" y2="4.2037" layer="21"/>
<rectangle x1="11.8999" y1="4.1783" x2="12.7127" y2="4.2037" layer="21"/>
<rectangle x1="13.0683" y1="4.1783" x2="13.6271" y2="4.2037" layer="21"/>
<rectangle x1="13.7287" y1="4.1783" x2="14.3637" y2="4.2037" layer="21"/>
<rectangle x1="14.8463" y1="4.1783" x2="15.3543" y2="4.2037" layer="21"/>
<rectangle x1="15.9893" y1="4.1783" x2="16.5481" y2="4.2037" layer="21"/>
<rectangle x1="0.6223" y1="4.2037" x2="1.1303" y2="4.2291" layer="21"/>
<rectangle x1="1.7145" y1="4.2037" x2="2.2987" y2="4.2291" layer="21"/>
<rectangle x1="2.8321" y1="4.2037" x2="3.4417" y2="4.2291" layer="21"/>
<rectangle x1="4.0259" y1="4.2037" x2="4.2799" y2="4.2291" layer="21"/>
<rectangle x1="4.9149" y1="4.2037" x2="5.8801" y2="4.2291" layer="21"/>
<rectangle x1="6.4643" y1="4.2037" x2="6.9215" y2="4.2291" layer="21"/>
<rectangle x1="7.5311" y1="4.2037" x2="7.8613" y2="4.2291" layer="21"/>
<rectangle x1="8.1407" y1="4.2037" x2="8.7249" y2="4.2291" layer="21"/>
<rectangle x1="9.2075" y1="4.2037" x2="9.7663" y2="4.2291" layer="21"/>
<rectangle x1="10.2489" y1="4.2037" x2="10.8077" y2="4.2291" layer="21"/>
<rectangle x1="12.1539" y1="4.2037" x2="12.7127" y2="4.2291" layer="21"/>
<rectangle x1="13.0683" y1="4.2037" x2="13.6271" y2="4.2291" layer="21"/>
<rectangle x1="13.7541" y1="4.2037" x2="14.3891" y2="4.2291" layer="21"/>
<rectangle x1="14.8463" y1="4.2037" x2="15.3543" y2="4.2291" layer="21"/>
<rectangle x1="15.9893" y1="4.2037" x2="16.5481" y2="4.2291" layer="21"/>
<rectangle x1="0.6223" y1="4.2291" x2="1.1303" y2="4.2545" layer="21"/>
<rectangle x1="1.7145" y1="4.2291" x2="2.2987" y2="4.2545" layer="21"/>
<rectangle x1="2.8575" y1="4.2291" x2="3.4417" y2="4.2545" layer="21"/>
<rectangle x1="4.0259" y1="4.2291" x2="4.2799" y2="4.2545" layer="21"/>
<rectangle x1="4.9149" y1="4.2291" x2="5.8801" y2="4.2545" layer="21"/>
<rectangle x1="6.4643" y1="4.2291" x2="6.9215" y2="4.2545" layer="21"/>
<rectangle x1="7.5311" y1="4.2291" x2="7.8613" y2="4.2545" layer="21"/>
<rectangle x1="8.1407" y1="4.2291" x2="8.7249" y2="4.2545" layer="21"/>
<rectangle x1="9.2075" y1="4.2291" x2="9.7663" y2="4.2545" layer="21"/>
<rectangle x1="10.2489" y1="4.2291" x2="10.8077" y2="4.2545" layer="21"/>
<rectangle x1="12.1793" y1="4.2291" x2="12.7127" y2="4.2545" layer="21"/>
<rectangle x1="13.0683" y1="4.2291" x2="13.6271" y2="4.2545" layer="21"/>
<rectangle x1="13.7541" y1="4.2291" x2="14.3891" y2="4.2545" layer="21"/>
<rectangle x1="14.8463" y1="4.2291" x2="15.3543" y2="4.2545" layer="21"/>
<rectangle x1="15.9893" y1="4.2291" x2="16.5481" y2="4.2545" layer="21"/>
<rectangle x1="0.6223" y1="4.2545" x2="1.1303" y2="4.2799" layer="21"/>
<rectangle x1="1.7145" y1="4.2545" x2="2.3241" y2="4.2799" layer="21"/>
<rectangle x1="2.8575" y1="4.2545" x2="3.4417" y2="4.2799" layer="21"/>
<rectangle x1="4.0259" y1="4.2545" x2="4.2799" y2="4.2799" layer="21"/>
<rectangle x1="4.9403" y1="4.2545" x2="5.8801" y2="4.2799" layer="21"/>
<rectangle x1="6.4643" y1="4.2545" x2="6.9215" y2="4.2799" layer="21"/>
<rectangle x1="7.5311" y1="4.2545" x2="7.8613" y2="4.2799" layer="21"/>
<rectangle x1="8.1407" y1="4.2545" x2="8.7249" y2="4.2799" layer="21"/>
<rectangle x1="9.1821" y1="4.2545" x2="9.7663" y2="4.2799" layer="21"/>
<rectangle x1="10.2489" y1="4.2545" x2="10.8077" y2="4.2799" layer="21"/>
<rectangle x1="12.1793" y1="4.2545" x2="12.7127" y2="4.2799" layer="21"/>
<rectangle x1="13.0683" y1="4.2545" x2="13.6271" y2="4.2799" layer="21"/>
<rectangle x1="13.7795" y1="4.2545" x2="14.4145" y2="4.2799" layer="21"/>
<rectangle x1="14.8463" y1="4.2545" x2="15.3797" y2="4.2799" layer="21"/>
<rectangle x1="15.9893" y1="4.2545" x2="16.5227" y2="4.2799" layer="21"/>
<rectangle x1="0.6223" y1="4.2799" x2="1.1303" y2="4.3053" layer="21"/>
<rectangle x1="1.7145" y1="4.2799" x2="2.3241" y2="4.3053" layer="21"/>
<rectangle x1="2.8575" y1="4.2799" x2="3.4417" y2="4.3053" layer="21"/>
<rectangle x1="4.0005" y1="4.2799" x2="4.2799" y2="4.3053" layer="21"/>
<rectangle x1="4.9657" y1="4.2799" x2="5.8801" y2="4.3053" layer="21"/>
<rectangle x1="6.4643" y1="4.2799" x2="6.9215" y2="4.3053" layer="21"/>
<rectangle x1="7.5311" y1="4.2799" x2="7.8613" y2="4.3053" layer="21"/>
<rectangle x1="8.1407" y1="4.2799" x2="8.7249" y2="4.3053" layer="21"/>
<rectangle x1="9.1821" y1="4.2799" x2="9.7663" y2="4.3053" layer="21"/>
<rectangle x1="10.2235" y1="4.2799" x2="10.8077" y2="4.3053" layer="21"/>
<rectangle x1="12.1793" y1="4.2799" x2="12.7127" y2="4.3053" layer="21"/>
<rectangle x1="13.0683" y1="4.2799" x2="13.6271" y2="4.3053" layer="21"/>
<rectangle x1="13.7795" y1="4.2799" x2="14.4399" y2="4.3053" layer="21"/>
<rectangle x1="14.8717" y1="4.2799" x2="15.3797" y2="4.3053" layer="21"/>
<rectangle x1="15.9893" y1="4.2799" x2="16.5227" y2="4.3053" layer="21"/>
<rectangle x1="0.6223" y1="4.3053" x2="1.1303" y2="4.3307" layer="21"/>
<rectangle x1="1.7145" y1="4.3053" x2="2.3241" y2="4.3307" layer="21"/>
<rectangle x1="2.8829" y1="4.3053" x2="3.4163" y2="4.3307" layer="21"/>
<rectangle x1="4.0005" y1="4.3053" x2="4.3053" y2="4.3307" layer="21"/>
<rectangle x1="4.9657" y1="4.3053" x2="5.8801" y2="4.3307" layer="21"/>
<rectangle x1="6.4643" y1="4.3053" x2="6.8961" y2="4.3307" layer="21"/>
<rectangle x1="7.5311" y1="4.3053" x2="7.8613" y2="4.3307" layer="21"/>
<rectangle x1="8.1407" y1="4.3053" x2="8.7249" y2="4.3307" layer="21"/>
<rectangle x1="9.1821" y1="4.3053" x2="9.7663" y2="4.3307" layer="21"/>
<rectangle x1="10.2235" y1="4.3053" x2="10.8077" y2="4.3307" layer="21"/>
<rectangle x1="12.1539" y1="4.3053" x2="12.7127" y2="4.3307" layer="21"/>
<rectangle x1="13.0683" y1="4.3053" x2="13.6271" y2="4.3307" layer="21"/>
<rectangle x1="13.8049" y1="4.3053" x2="14.4399" y2="4.3307" layer="21"/>
<rectangle x1="14.8717" y1="4.3053" x2="15.4051" y2="4.3307" layer="21"/>
<rectangle x1="15.9639" y1="4.3053" x2="16.5227" y2="4.3307" layer="21"/>
<rectangle x1="0.6223" y1="4.3307" x2="1.1303" y2="4.3561" layer="21"/>
<rectangle x1="1.7145" y1="4.3307" x2="2.3241" y2="4.3561" layer="21"/>
<rectangle x1="2.8829" y1="4.3307" x2="3.4163" y2="4.3561" layer="21"/>
<rectangle x1="4.0005" y1="4.3307" x2="4.3053" y2="4.3561" layer="21"/>
<rectangle x1="4.9911" y1="4.3307" x2="5.8801" y2="4.3561" layer="21"/>
<rectangle x1="6.4643" y1="4.3307" x2="6.8961" y2="4.3561" layer="21"/>
<rectangle x1="7.5311" y1="4.3307" x2="7.8613" y2="4.3561" layer="21"/>
<rectangle x1="8.1407" y1="4.3307" x2="8.7249" y2="4.3561" layer="21"/>
<rectangle x1="9.1567" y1="4.3307" x2="9.7663" y2="4.3561" layer="21"/>
<rectangle x1="10.2235" y1="4.3307" x2="10.8077" y2="4.3561" layer="21"/>
<rectangle x1="12.1539" y1="4.3307" x2="12.7127" y2="4.3561" layer="21"/>
<rectangle x1="13.0683" y1="4.3307" x2="13.6271" y2="4.3561" layer="21"/>
<rectangle x1="13.8303" y1="4.3307" x2="14.4653" y2="4.3561" layer="21"/>
<rectangle x1="14.8717" y1="4.3307" x2="15.4051" y2="4.3561" layer="21"/>
<rectangle x1="15.9639" y1="4.3307" x2="16.5227" y2="4.3561" layer="21"/>
<rectangle x1="0.6223" y1="4.3561" x2="1.1303" y2="4.3815" layer="21"/>
<rectangle x1="1.7145" y1="4.3561" x2="2.3495" y2="4.3815" layer="21"/>
<rectangle x1="2.9083" y1="4.3561" x2="3.3909" y2="4.3815" layer="21"/>
<rectangle x1="4.0005" y1="4.3561" x2="4.3053" y2="4.3815" layer="21"/>
<rectangle x1="5.0165" y1="4.3561" x2="5.4483" y2="4.3815" layer="21"/>
<rectangle x1="5.4737" y1="4.3561" x2="5.8801" y2="4.3815" layer="21"/>
<rectangle x1="6.4643" y1="4.3561" x2="6.8707" y2="4.3815" layer="21"/>
<rectangle x1="7.5311" y1="4.3561" x2="7.8613" y2="4.3815" layer="21"/>
<rectangle x1="8.1407" y1="4.3561" x2="8.7249" y2="4.3815" layer="21"/>
<rectangle x1="9.1567" y1="4.3561" x2="9.7663" y2="4.3815" layer="21"/>
<rectangle x1="10.1981" y1="4.3561" x2="10.8077" y2="4.3815" layer="21"/>
<rectangle x1="12.1539" y1="4.3561" x2="12.7127" y2="4.3815" layer="21"/>
<rectangle x1="13.0683" y1="4.3561" x2="13.6271" y2="4.3815" layer="21"/>
<rectangle x1="13.8303" y1="4.3561" x2="14.4907" y2="4.3815" layer="21"/>
<rectangle x1="14.8971" y1="4.3561" x2="15.4305" y2="4.3815" layer="21"/>
<rectangle x1="15.9385" y1="4.3561" x2="16.4973" y2="4.3815" layer="21"/>
<rectangle x1="0.6223" y1="4.3815" x2="1.1303" y2="4.4069" layer="21"/>
<rectangle x1="1.7145" y1="4.3815" x2="2.3495" y2="4.4069" layer="21"/>
<rectangle x1="2.9337" y1="4.3815" x2="3.3909" y2="4.4069" layer="21"/>
<rectangle x1="3.9751" y1="4.3815" x2="4.3307" y2="4.4069" layer="21"/>
<rectangle x1="5.0673" y1="4.3815" x2="5.3975" y2="4.4069" layer="21"/>
<rectangle x1="5.4737" y1="4.3815" x2="5.8801" y2="4.4069" layer="21"/>
<rectangle x1="6.4643" y1="4.3815" x2="6.8453" y2="4.4069" layer="21"/>
<rectangle x1="7.5311" y1="4.3815" x2="7.8613" y2="4.4069" layer="21"/>
<rectangle x1="8.1407" y1="4.3815" x2="8.7249" y2="4.4069" layer="21"/>
<rectangle x1="9.1313" y1="4.3815" x2="9.7663" y2="4.4069" layer="21"/>
<rectangle x1="10.1727" y1="4.3815" x2="10.8077" y2="4.4069" layer="21"/>
<rectangle x1="12.1539" y1="4.3815" x2="12.6873" y2="4.4069" layer="21"/>
<rectangle x1="13.0683" y1="4.3815" x2="13.6271" y2="4.4069" layer="21"/>
<rectangle x1="13.8557" y1="4.3815" x2="14.4907" y2="4.4069" layer="21"/>
<rectangle x1="14.8971" y1="4.3815" x2="15.4559" y2="4.4069" layer="21"/>
<rectangle x1="15.9385" y1="4.3815" x2="16.4973" y2="4.4069" layer="21"/>
<rectangle x1="0.6223" y1="4.4069" x2="1.1303" y2="4.4323" layer="21"/>
<rectangle x1="1.7145" y1="4.4069" x2="2.3749" y2="4.4323" layer="21"/>
<rectangle x1="2.9591" y1="4.4069" x2="3.3655" y2="4.4323" layer="21"/>
<rectangle x1="3.9751" y1="4.4069" x2="4.3307" y2="4.4323" layer="21"/>
<rectangle x1="5.1181" y1="4.4069" x2="5.3213" y2="4.4323" layer="21"/>
<rectangle x1="5.4991" y1="4.4069" x2="5.8801" y2="4.4323" layer="21"/>
<rectangle x1="6.5405" y1="4.4069" x2="6.7945" y2="4.4323" layer="21"/>
<rectangle x1="7.5057" y1="4.4069" x2="7.8613" y2="4.4323" layer="21"/>
<rectangle x1="8.1407" y1="4.4069" x2="8.7249" y2="4.4323" layer="21"/>
<rectangle x1="9.1059" y1="4.4069" x2="9.8171" y2="4.4323" layer="21"/>
<rectangle x1="10.1473" y1="4.4069" x2="10.8077" y2="4.4323" layer="21"/>
<rectangle x1="11.3157" y1="4.4069" x2="11.4173" y2="4.4323" layer="21"/>
<rectangle x1="12.1285" y1="4.4069" x2="12.6873" y2="4.4323" layer="21"/>
<rectangle x1="13.0683" y1="4.4069" x2="13.6271" y2="4.4323" layer="21"/>
<rectangle x1="13.8811" y1="4.4069" x2="14.5161" y2="4.4323" layer="21"/>
<rectangle x1="14.9225" y1="4.4069" x2="15.4813" y2="4.4323" layer="21"/>
<rectangle x1="15.9131" y1="4.4069" x2="16.4973" y2="4.4323" layer="21"/>
<rectangle x1="0.6223" y1="4.4323" x2="0.9017" y2="4.4577" layer="21"/>
<rectangle x1="2.0447" y1="4.4323" x2="2.3749" y2="4.4577" layer="21"/>
<rectangle x1="2.9845" y1="4.4323" x2="3.3401" y2="4.4577" layer="21"/>
<rectangle x1="3.9497" y1="4.4323" x2="4.3561" y2="4.4577" layer="21"/>
<rectangle x1="5.4991" y1="4.4323" x2="5.8801" y2="4.4577" layer="21"/>
<rectangle x1="7.5057" y1="4.4323" x2="7.8613" y2="4.4577" layer="21"/>
<rectangle x1="8.1407" y1="4.4323" x2="8.8011" y2="4.4577" layer="21"/>
<rectangle x1="9.0297" y1="4.4323" x2="9.8679" y2="4.4577" layer="21"/>
<rectangle x1="10.0965" y1="4.4323" x2="10.8077" y2="4.4577" layer="21"/>
<rectangle x1="11.3157" y1="4.4323" x2="11.4935" y2="4.4577" layer="21"/>
<rectangle x1="12.1031" y1="4.4323" x2="12.6873" y2="4.4577" layer="21"/>
<rectangle x1="13.0683" y1="4.4323" x2="13.6271" y2="4.4577" layer="21"/>
<rectangle x1="13.8811" y1="4.4323" x2="14.5415" y2="4.4577" layer="21"/>
<rectangle x1="14.9225" y1="4.4323" x2="15.5067" y2="4.4577" layer="21"/>
<rectangle x1="15.8877" y1="4.4323" x2="16.4719" y2="4.4577" layer="21"/>
<rectangle x1="0.6223" y1="4.4577" x2="0.9017" y2="4.4831" layer="21"/>
<rectangle x1="2.0447" y1="4.4577" x2="2.4003" y2="4.4831" layer="21"/>
<rectangle x1="3.0353" y1="4.4577" x2="3.2893" y2="4.4831" layer="21"/>
<rectangle x1="3.9497" y1="4.4577" x2="4.3561" y2="4.4831" layer="21"/>
<rectangle x1="5.4991" y1="4.4577" x2="5.8801" y2="4.4831" layer="21"/>
<rectangle x1="7.5057" y1="4.4577" x2="7.8613" y2="4.4831" layer="21"/>
<rectangle x1="8.1407" y1="4.4577" x2="10.8077" y2="4.4831" layer="21"/>
<rectangle x1="11.3157" y1="4.4577" x2="11.5951" y2="4.4831" layer="21"/>
<rectangle x1="12.0523" y1="4.4577" x2="12.6873" y2="4.4831" layer="21"/>
<rectangle x1="13.0683" y1="4.4577" x2="13.6271" y2="4.4831" layer="21"/>
<rectangle x1="13.9065" y1="4.4577" x2="14.5415" y2="4.4831" layer="21"/>
<rectangle x1="14.9479" y1="4.4577" x2="15.5575" y2="4.4831" layer="21"/>
<rectangle x1="15.8369" y1="4.4577" x2="16.4719" y2="4.4831" layer="21"/>
<rectangle x1="0.6223" y1="4.4831" x2="0.9017" y2="4.5085" layer="21"/>
<rectangle x1="2.0447" y1="4.4831" x2="2.4003" y2="4.5085" layer="21"/>
<rectangle x1="3.9497" y1="4.4831" x2="4.3815" y2="4.5085" layer="21"/>
<rectangle x1="5.5245" y1="4.4831" x2="5.8801" y2="4.5085" layer="21"/>
<rectangle x1="7.5057" y1="4.4831" x2="7.8613" y2="4.5085" layer="21"/>
<rectangle x1="8.1407" y1="4.4831" x2="10.7823" y2="4.5085" layer="21"/>
<rectangle x1="11.2903" y1="4.4831" x2="11.7221" y2="4.5085" layer="21"/>
<rectangle x1="11.9761" y1="4.4831" x2="12.6873" y2="4.5085" layer="21"/>
<rectangle x1="13.0683" y1="4.4831" x2="13.6271" y2="4.5085" layer="21"/>
<rectangle x1="13.9319" y1="4.4831" x2="14.5669" y2="4.5085" layer="21"/>
<rectangle x1="14.9479" y1="4.4831" x2="15.6083" y2="4.5085" layer="21"/>
<rectangle x1="15.7861" y1="4.4831" x2="16.4465" y2="4.5085" layer="21"/>
<rectangle x1="0.6223" y1="4.5085" x2="0.9017" y2="4.5339" layer="21"/>
<rectangle x1="2.0447" y1="4.5085" x2="2.4257" y2="4.5339" layer="21"/>
<rectangle x1="3.9243" y1="4.5085" x2="4.3815" y2="4.5339" layer="21"/>
<rectangle x1="5.5245" y1="4.5085" x2="5.8801" y2="4.5339" layer="21"/>
<rectangle x1="7.4803" y1="4.5085" x2="7.8613" y2="4.5339" layer="21"/>
<rectangle x1="8.1407" y1="4.5085" x2="10.7823" y2="4.5339" layer="21"/>
<rectangle x1="11.2903" y1="4.5085" x2="12.6619" y2="4.5339" layer="21"/>
<rectangle x1="13.0683" y1="4.5085" x2="13.6271" y2="4.5339" layer="21"/>
<rectangle x1="13.9319" y1="4.5085" x2="14.5923" y2="4.5339" layer="21"/>
<rectangle x1="14.9733" y1="4.5085" x2="16.4465" y2="4.5339" layer="21"/>
<rectangle x1="0.6223" y1="4.5339" x2="0.9017" y2="4.5593" layer="21"/>
<rectangle x1="2.0447" y1="4.5339" x2="2.4257" y2="4.5593" layer="21"/>
<rectangle x1="3.8989" y1="4.5339" x2="4.4069" y2="4.5593" layer="21"/>
<rectangle x1="5.5499" y1="4.5339" x2="5.8801" y2="4.5593" layer="21"/>
<rectangle x1="7.4803" y1="4.5339" x2="7.8613" y2="4.5593" layer="21"/>
<rectangle x1="8.1407" y1="4.5339" x2="10.7823" y2="4.5593" layer="21"/>
<rectangle x1="11.2903" y1="4.5339" x2="12.6619" y2="4.5593" layer="21"/>
<rectangle x1="13.0683" y1="4.5339" x2="13.6271" y2="4.5593" layer="21"/>
<rectangle x1="13.9573" y1="4.5339" x2="14.5923" y2="4.5593" layer="21"/>
<rectangle x1="14.9733" y1="4.5339" x2="16.4211" y2="4.5593" layer="21"/>
<rectangle x1="0.6223" y1="4.5593" x2="0.9017" y2="4.5847" layer="21"/>
<rectangle x1="2.0447" y1="4.5593" x2="2.4511" y2="4.5847" layer="21"/>
<rectangle x1="3.8989" y1="4.5593" x2="4.4069" y2="4.5847" layer="21"/>
<rectangle x1="5.5499" y1="4.5593" x2="5.8801" y2="4.5847" layer="21"/>
<rectangle x1="7.4803" y1="4.5593" x2="7.8613" y2="4.5847" layer="21"/>
<rectangle x1="8.1407" y1="4.5593" x2="10.7569" y2="4.5847" layer="21"/>
<rectangle x1="11.2649" y1="4.5593" x2="12.6365" y2="4.5847" layer="21"/>
<rectangle x1="13.0683" y1="4.5593" x2="13.6271" y2="4.5847" layer="21"/>
<rectangle x1="13.9827" y1="4.5593" x2="14.6177" y2="4.5847" layer="21"/>
<rectangle x1="14.9987" y1="4.5593" x2="16.4211" y2="4.5847" layer="21"/>
<rectangle x1="0.6223" y1="4.5847" x2="0.9017" y2="4.6101" layer="21"/>
<rectangle x1="2.0447" y1="4.5847" x2="2.4765" y2="4.6101" layer="21"/>
<rectangle x1="3.8735" y1="4.5847" x2="4.4323" y2="4.6101" layer="21"/>
<rectangle x1="5.5753" y1="4.5847" x2="5.8801" y2="4.6101" layer="21"/>
<rectangle x1="7.4549" y1="4.5847" x2="7.8613" y2="4.6101" layer="21"/>
<rectangle x1="8.1407" y1="4.5847" x2="10.7569" y2="4.6101" layer="21"/>
<rectangle x1="11.2649" y1="4.5847" x2="12.6365" y2="4.6101" layer="21"/>
<rectangle x1="13.0683" y1="4.5847" x2="13.6271" y2="4.6101" layer="21"/>
<rectangle x1="13.9827" y1="4.5847" x2="14.6431" y2="4.6101" layer="21"/>
<rectangle x1="15.0241" y1="4.5847" x2="16.3957" y2="4.6101" layer="21"/>
<rectangle x1="0.6223" y1="4.6101" x2="0.9017" y2="4.6355" layer="21"/>
<rectangle x1="2.0447" y1="4.6101" x2="2.5019" y2="4.6355" layer="21"/>
<rectangle x1="3.8481" y1="4.6101" x2="4.4577" y2="4.6355" layer="21"/>
<rectangle x1="5.5753" y1="4.6101" x2="5.8801" y2="4.6355" layer="21"/>
<rectangle x1="7.4549" y1="4.6101" x2="7.8613" y2="4.6355" layer="21"/>
<rectangle x1="8.1407" y1="4.6101" x2="10.7315" y2="4.6355" layer="21"/>
<rectangle x1="11.2395" y1="4.6101" x2="12.6111" y2="4.6355" layer="21"/>
<rectangle x1="13.0683" y1="4.6101" x2="13.6271" y2="4.6355" layer="21"/>
<rectangle x1="14.0081" y1="4.6101" x2="14.6431" y2="4.6355" layer="21"/>
<rectangle x1="15.0495" y1="4.6101" x2="16.3703" y2="4.6355" layer="21"/>
<rectangle x1="0.6223" y1="4.6355" x2="0.9017" y2="4.6609" layer="21"/>
<rectangle x1="2.0447" y1="4.6355" x2="2.5019" y2="4.6609" layer="21"/>
<rectangle x1="3.8481" y1="4.6355" x2="4.4831" y2="4.6609" layer="21"/>
<rectangle x1="5.6007" y1="4.6355" x2="5.8801" y2="4.6609" layer="21"/>
<rectangle x1="7.4295" y1="4.6355" x2="7.8613" y2="4.6609" layer="21"/>
<rectangle x1="8.1407" y1="4.6355" x2="10.7315" y2="4.6609" layer="21"/>
<rectangle x1="11.2395" y1="4.6355" x2="12.6111" y2="4.6609" layer="21"/>
<rectangle x1="13.0683" y1="4.6355" x2="13.6271" y2="4.6609" layer="21"/>
<rectangle x1="14.0081" y1="4.6355" x2="14.6685" y2="4.6609" layer="21"/>
<rectangle x1="15.0495" y1="4.6355" x2="16.3449" y2="4.6609" layer="21"/>
<rectangle x1="0.6223" y1="4.6609" x2="0.9017" y2="4.6863" layer="21"/>
<rectangle x1="2.0447" y1="4.6609" x2="2.5273" y2="4.6863" layer="21"/>
<rectangle x1="3.8227" y1="4.6609" x2="4.5085" y2="4.6863" layer="21"/>
<rectangle x1="5.6007" y1="4.6609" x2="5.8801" y2="4.6863" layer="21"/>
<rectangle x1="7.4041" y1="4.6609" x2="7.8613" y2="4.6863" layer="21"/>
<rectangle x1="8.1407" y1="4.6609" x2="10.7061" y2="4.6863" layer="21"/>
<rectangle x1="11.2395" y1="4.6609" x2="12.5857" y2="4.6863" layer="21"/>
<rectangle x1="13.0683" y1="4.6609" x2="13.6271" y2="4.6863" layer="21"/>
<rectangle x1="14.0335" y1="4.6609" x2="14.6939" y2="4.6863" layer="21"/>
<rectangle x1="15.0749" y1="4.6609" x2="16.3449" y2="4.6863" layer="21"/>
<rectangle x1="0.6223" y1="4.6863" x2="0.9017" y2="4.7117" layer="21"/>
<rectangle x1="2.0447" y1="4.6863" x2="2.5527" y2="4.7117" layer="21"/>
<rectangle x1="3.7973" y1="4.6863" x2="4.5339" y2="4.7117" layer="21"/>
<rectangle x1="5.6007" y1="4.6863" x2="5.8801" y2="4.7117" layer="21"/>
<rectangle x1="7.3787" y1="4.6863" x2="7.8613" y2="4.7117" layer="21"/>
<rectangle x1="8.1407" y1="4.6863" x2="10.6807" y2="4.7117" layer="21"/>
<rectangle x1="11.2141" y1="4.6863" x2="12.5603" y2="4.7117" layer="21"/>
<rectangle x1="13.0683" y1="4.6863" x2="13.6271" y2="4.7117" layer="21"/>
<rectangle x1="14.0589" y1="4.6863" x2="14.6939" y2="4.7117" layer="21"/>
<rectangle x1="15.1003" y1="4.6863" x2="16.3195" y2="4.7117" layer="21"/>
<rectangle x1="0.6223" y1="4.7117" x2="0.9017" y2="4.7371" layer="21"/>
<rectangle x1="2.0447" y1="4.7117" x2="2.6035" y2="4.7371" layer="21"/>
<rectangle x1="3.7719" y1="4.7117" x2="4.5593" y2="4.7371" layer="21"/>
<rectangle x1="5.6261" y1="4.7117" x2="5.8801" y2="4.7371" layer="21"/>
<rectangle x1="7.3787" y1="4.7117" x2="7.8613" y2="4.7371" layer="21"/>
<rectangle x1="8.1407" y1="4.7117" x2="10.6553" y2="4.7371" layer="21"/>
<rectangle x1="11.2141" y1="4.7117" x2="12.5603" y2="4.7371" layer="21"/>
<rectangle x1="13.0683" y1="4.7117" x2="13.6271" y2="4.7371" layer="21"/>
<rectangle x1="14.0589" y1="4.7117" x2="14.7193" y2="4.7371" layer="21"/>
<rectangle x1="15.1511" y1="4.7117" x2="16.2687" y2="4.7371" layer="21"/>
<rectangle x1="0.6223" y1="4.7371" x2="0.9017" y2="4.7625" layer="21"/>
<rectangle x1="2.0447" y1="4.7371" x2="2.6289" y2="4.7625" layer="21"/>
<rectangle x1="3.7465" y1="4.7371" x2="4.5847" y2="4.7625" layer="21"/>
<rectangle x1="5.6261" y1="4.7371" x2="5.8801" y2="4.7625" layer="21"/>
<rectangle x1="7.3533" y1="4.7371" x2="7.8613" y2="4.7625" layer="21"/>
<rectangle x1="8.1407" y1="4.7371" x2="9.4869" y2="4.7625" layer="21"/>
<rectangle x1="9.5123" y1="4.7371" x2="10.6299" y2="4.7625" layer="21"/>
<rectangle x1="11.2141" y1="4.7371" x2="12.5095" y2="4.7625" layer="21"/>
<rectangle x1="13.0683" y1="4.7371" x2="13.6271" y2="4.7625" layer="21"/>
<rectangle x1="14.0843" y1="4.7371" x2="14.7447" y2="4.7625" layer="21"/>
<rectangle x1="15.1765" y1="4.7371" x2="16.2433" y2="4.7625" layer="21"/>
<rectangle x1="0.6223" y1="4.7625" x2="0.9017" y2="4.7879" layer="21"/>
<rectangle x1="2.0447" y1="4.7625" x2="2.6543" y2="4.7879" layer="21"/>
<rectangle x1="3.6957" y1="4.7625" x2="4.6101" y2="4.7879" layer="21"/>
<rectangle x1="5.6515" y1="4.7625" x2="5.8801" y2="4.7879" layer="21"/>
<rectangle x1="7.3025" y1="4.7625" x2="7.8613" y2="4.7879" layer="21"/>
<rectangle x1="8.1661" y1="4.7625" x2="9.4361" y2="4.7879" layer="21"/>
<rectangle x1="9.5631" y1="4.7625" x2="10.6045" y2="4.7879" layer="21"/>
<rectangle x1="11.2141" y1="4.7625" x2="12.4841" y2="4.7879" layer="21"/>
<rectangle x1="13.0683" y1="4.7625" x2="13.6271" y2="4.7879" layer="21"/>
<rectangle x1="14.1097" y1="4.7625" x2="14.7447" y2="4.7879" layer="21"/>
<rectangle x1="15.2019" y1="4.7625" x2="16.2179" y2="4.7879" layer="21"/>
<rectangle x1="0.6223" y1="4.7879" x2="0.9017" y2="4.8133" layer="21"/>
<rectangle x1="2.0447" y1="4.7879" x2="2.7051" y2="4.8133" layer="21"/>
<rectangle x1="3.6703" y1="4.7879" x2="4.6609" y2="4.8133" layer="21"/>
<rectangle x1="5.6261" y1="4.7879" x2="5.8801" y2="4.8133" layer="21"/>
<rectangle x1="7.2771" y1="4.7879" x2="7.8613" y2="4.8133" layer="21"/>
<rectangle x1="8.2169" y1="4.7879" x2="9.4107" y2="4.8133" layer="21"/>
<rectangle x1="9.6139" y1="4.7879" x2="10.5791" y2="4.8133" layer="21"/>
<rectangle x1="11.2649" y1="4.7879" x2="12.4587" y2="4.8133" layer="21"/>
<rectangle x1="13.0683" y1="4.7879" x2="13.6271" y2="4.8133" layer="21"/>
<rectangle x1="14.1097" y1="4.7879" x2="14.7701" y2="4.8133" layer="21"/>
<rectangle x1="15.2527" y1="4.7879" x2="16.1671" y2="4.8133" layer="21"/>
<rectangle x1="0.6223" y1="4.8133" x2="0.9017" y2="4.8387" layer="21"/>
<rectangle x1="2.0447" y1="4.8133" x2="2.7559" y2="4.8387" layer="21"/>
<rectangle x1="3.6195" y1="4.8133" x2="4.7117" y2="4.8387" layer="21"/>
<rectangle x1="5.5753" y1="4.8133" x2="5.8801" y2="4.8387" layer="21"/>
<rectangle x1="7.2263" y1="4.8133" x2="7.8613" y2="4.8387" layer="21"/>
<rectangle x1="8.2931" y1="4.8133" x2="9.3599" y2="4.8387" layer="21"/>
<rectangle x1="9.6647" y1="4.8133" x2="10.5283" y2="4.8387" layer="21"/>
<rectangle x1="11.3411" y1="4.8133" x2="12.4079" y2="4.8387" layer="21"/>
<rectangle x1="13.0683" y1="4.8133" x2="13.6271" y2="4.8387" layer="21"/>
<rectangle x1="14.1351" y1="4.8133" x2="14.7955" y2="4.8387" layer="21"/>
<rectangle x1="15.3035" y1="4.8133" x2="16.1163" y2="4.8387" layer="21"/>
<rectangle x1="0.6223" y1="4.8387" x2="0.9017" y2="4.8641" layer="21"/>
<rectangle x1="2.0447" y1="4.8387" x2="2.8067" y2="4.8641" layer="21"/>
<rectangle x1="3.5687" y1="4.8387" x2="4.7625" y2="4.8641" layer="21"/>
<rectangle x1="5.5245" y1="4.8387" x2="5.8801" y2="4.8641" layer="21"/>
<rectangle x1="6.4643" y1="4.8387" x2="6.4897" y2="4.8641" layer="21"/>
<rectangle x1="7.1755" y1="4.8387" x2="7.8613" y2="4.8641" layer="21"/>
<rectangle x1="8.3947" y1="4.8387" x2="9.2837" y2="4.8641" layer="21"/>
<rectangle x1="9.7409" y1="4.8387" x2="10.4775" y2="4.8641" layer="21"/>
<rectangle x1="11.4427" y1="4.8387" x2="12.3571" y2="4.8641" layer="21"/>
<rectangle x1="13.0683" y1="4.8387" x2="13.6271" y2="4.8641" layer="21"/>
<rectangle x1="14.1605" y1="4.8387" x2="14.7955" y2="4.8641" layer="21"/>
<rectangle x1="15.3543" y1="4.8387" x2="16.0655" y2="4.8641" layer="21"/>
<rectangle x1="0.6223" y1="4.8641" x2="0.9017" y2="4.8895" layer="21"/>
<rectangle x1="2.0193" y1="4.8641" x2="2.8575" y2="4.8895" layer="21"/>
<rectangle x1="3.4925" y1="4.8641" x2="4.8133" y2="4.8895" layer="21"/>
<rectangle x1="5.4737" y1="4.8641" x2="5.8801" y2="4.8895" layer="21"/>
<rectangle x1="6.4643" y1="4.8641" x2="6.5659" y2="4.8895" layer="21"/>
<rectangle x1="7.0993" y1="4.8641" x2="7.8613" y2="4.8895" layer="21"/>
<rectangle x1="8.5217" y1="4.8641" x2="9.2075" y2="4.8895" layer="21"/>
<rectangle x1="9.8425" y1="4.8641" x2="10.4013" y2="4.8895" layer="21"/>
<rectangle x1="11.5697" y1="4.8641" x2="12.2555" y2="4.8895" layer="21"/>
<rectangle x1="13.0683" y1="4.8641" x2="13.6271" y2="4.8895" layer="21"/>
<rectangle x1="15.4305" y1="4.8641" x2="15.9893" y2="4.8895" layer="21"/>
<rectangle x1="0.6223" y1="4.8895" x2="1.1303" y2="4.9149" layer="21"/>
<rectangle x1="1.7145" y1="4.8895" x2="2.9591" y2="4.9149" layer="21"/>
<rectangle x1="3.4163" y1="4.8895" x2="4.9149" y2="4.9149" layer="21"/>
<rectangle x1="5.3721" y1="4.8895" x2="5.8801" y2="4.9149" layer="21"/>
<rectangle x1="6.4643" y1="4.8895" x2="6.6929" y2="4.9149" layer="21"/>
<rectangle x1="6.9977" y1="4.8895" x2="7.8613" y2="4.9149" layer="21"/>
<rectangle x1="8.6995" y1="4.8895" x2="9.0551" y2="4.9149" layer="21"/>
<rectangle x1="9.9949" y1="4.8895" x2="10.2743" y2="4.9149" layer="21"/>
<rectangle x1="11.7475" y1="4.8895" x2="12.1031" y2="4.9149" layer="21"/>
<rectangle x1="13.0683" y1="4.8895" x2="13.6271" y2="4.9149" layer="21"/>
<rectangle x1="15.5829" y1="4.8895" x2="15.8623" y2="4.9149" layer="21"/>
<rectangle x1="0.6223" y1="4.9149" x2="1.1303" y2="4.9403" layer="21"/>
<rectangle x1="1.7145" y1="4.9149" x2="5.8801" y2="4.9403" layer="21"/>
<rectangle x1="6.4643" y1="4.9149" x2="7.8613" y2="4.9403" layer="21"/>
<rectangle x1="13.0683" y1="4.9149" x2="13.6271" y2="4.9403" layer="21"/>
<rectangle x1="0.6223" y1="4.9403" x2="1.1303" y2="4.9657" layer="21"/>
<rectangle x1="1.7145" y1="4.9403" x2="5.8801" y2="4.9657" layer="21"/>
<rectangle x1="6.4643" y1="4.9403" x2="7.8613" y2="4.9657" layer="21"/>
<rectangle x1="13.0683" y1="4.9403" x2="13.6271" y2="4.9657" layer="21"/>
<rectangle x1="0.6223" y1="4.9657" x2="1.1303" y2="4.9911" layer="21"/>
<rectangle x1="1.7145" y1="4.9657" x2="5.8801" y2="4.9911" layer="21"/>
<rectangle x1="6.4643" y1="4.9657" x2="7.8613" y2="4.9911" layer="21"/>
<rectangle x1="13.0683" y1="4.9657" x2="13.6271" y2="4.9911" layer="21"/>
<rectangle x1="0.6223" y1="4.9911" x2="1.1303" y2="5.0165" layer="21"/>
<rectangle x1="1.7145" y1="4.9911" x2="5.8801" y2="5.0165" layer="21"/>
<rectangle x1="6.4643" y1="4.9911" x2="7.8613" y2="5.0165" layer="21"/>
<rectangle x1="13.0683" y1="4.9911" x2="13.6271" y2="5.0165" layer="21"/>
<rectangle x1="0.6223" y1="5.0165" x2="1.1303" y2="5.0419" layer="21"/>
<rectangle x1="1.7145" y1="5.0165" x2="5.8801" y2="5.0419" layer="21"/>
<rectangle x1="6.4643" y1="5.0165" x2="7.8613" y2="5.0419" layer="21"/>
<rectangle x1="13.0683" y1="5.0165" x2="13.6271" y2="5.0419" layer="21"/>
<rectangle x1="0.6223" y1="5.0419" x2="1.1303" y2="5.0673" layer="21"/>
<rectangle x1="1.7145" y1="5.0419" x2="5.8801" y2="5.0673" layer="21"/>
<rectangle x1="6.4643" y1="5.0419" x2="7.8613" y2="5.0673" layer="21"/>
<rectangle x1="13.0683" y1="5.0419" x2="13.6271" y2="5.0673" layer="21"/>
<rectangle x1="0.6223" y1="5.0673" x2="1.1303" y2="5.0927" layer="21"/>
<rectangle x1="1.7145" y1="5.0673" x2="5.8801" y2="5.0927" layer="21"/>
<rectangle x1="6.4643" y1="5.0673" x2="7.8613" y2="5.0927" layer="21"/>
<rectangle x1="13.0683" y1="5.0673" x2="13.6271" y2="5.0927" layer="21"/>
<rectangle x1="0.6223" y1="5.0927" x2="1.1303" y2="5.1181" layer="21"/>
<rectangle x1="1.7145" y1="5.0927" x2="5.8801" y2="5.1181" layer="21"/>
<rectangle x1="6.4643" y1="5.0927" x2="7.8613" y2="5.1181" layer="21"/>
<rectangle x1="13.0683" y1="5.0927" x2="13.6271" y2="5.1181" layer="21"/>
<rectangle x1="0.6223" y1="5.1181" x2="1.1303" y2="5.1435" layer="21"/>
<rectangle x1="1.7145" y1="5.1181" x2="5.8801" y2="5.1435" layer="21"/>
<rectangle x1="6.4643" y1="5.1181" x2="7.8613" y2="5.1435" layer="21"/>
<rectangle x1="13.0683" y1="5.1181" x2="13.6271" y2="5.1435" layer="21"/>
<rectangle x1="0.6223" y1="5.1435" x2="1.1303" y2="5.1689" layer="21"/>
<rectangle x1="1.7145" y1="5.1435" x2="5.8801" y2="5.1689" layer="21"/>
<rectangle x1="6.4643" y1="5.1435" x2="7.8613" y2="5.1689" layer="21"/>
<rectangle x1="13.0683" y1="5.1435" x2="13.6271" y2="5.1689" layer="21"/>
<rectangle x1="0.6223" y1="5.1689" x2="1.1303" y2="5.1943" layer="21"/>
<rectangle x1="1.7145" y1="5.1689" x2="5.8801" y2="5.1943" layer="21"/>
<rectangle x1="6.4643" y1="5.1689" x2="7.8613" y2="5.1943" layer="21"/>
<rectangle x1="13.0683" y1="5.1689" x2="13.6271" y2="5.1943" layer="21"/>
<rectangle x1="0.6223" y1="5.1943" x2="1.1303" y2="5.2197" layer="21"/>
<rectangle x1="1.7145" y1="5.1943" x2="5.8801" y2="5.2197" layer="21"/>
<rectangle x1="6.4643" y1="5.1943" x2="7.8613" y2="5.2197" layer="21"/>
<rectangle x1="13.0683" y1="5.1943" x2="13.6271" y2="5.2197" layer="21"/>
<rectangle x1="0.6223" y1="5.2197" x2="1.1303" y2="5.2451" layer="21"/>
<rectangle x1="1.7145" y1="5.2197" x2="5.8801" y2="5.2451" layer="21"/>
<rectangle x1="6.4643" y1="5.2197" x2="7.8613" y2="5.2451" layer="21"/>
<rectangle x1="13.0683" y1="5.2197" x2="13.6271" y2="5.2451" layer="21"/>
<rectangle x1="0.6223" y1="5.2451" x2="1.1303" y2="5.2705" layer="21"/>
<rectangle x1="1.7145" y1="5.2451" x2="5.8801" y2="5.2705" layer="21"/>
<rectangle x1="6.4643" y1="5.2451" x2="7.8613" y2="5.2705" layer="21"/>
<rectangle x1="13.0683" y1="5.2451" x2="13.6271" y2="5.2705" layer="21"/>
<rectangle x1="0.6223" y1="5.2705" x2="1.1303" y2="5.2959" layer="21"/>
<rectangle x1="1.7145" y1="5.2705" x2="5.8801" y2="5.2959" layer="21"/>
<rectangle x1="6.4643" y1="5.2705" x2="7.8613" y2="5.2959" layer="21"/>
<rectangle x1="13.0683" y1="5.2705" x2="13.6271" y2="5.2959" layer="21"/>
<rectangle x1="0.6223" y1="5.2959" x2="1.2319" y2="5.3213" layer="21"/>
<rectangle x1="1.7145" y1="5.2959" x2="5.8801" y2="5.3213" layer="21"/>
<rectangle x1="6.4643" y1="5.2959" x2="7.8613" y2="5.3213" layer="21"/>
<rectangle x1="13.0683" y1="5.2959" x2="13.6271" y2="5.3213" layer="21"/>
<rectangle x1="0.6223" y1="5.3213" x2="1.3081" y2="5.3467" layer="21"/>
<rectangle x1="1.7145" y1="5.3213" x2="5.8801" y2="5.3467" layer="21"/>
<rectangle x1="6.4643" y1="5.3213" x2="7.8613" y2="5.3467" layer="21"/>
<rectangle x1="13.0683" y1="5.3213" x2="13.6271" y2="5.3467" layer="21"/>
<rectangle x1="0.6223" y1="5.3467" x2="1.4097" y2="5.3721" layer="21"/>
<rectangle x1="1.7145" y1="5.3467" x2="5.8801" y2="5.3721" layer="21"/>
<rectangle x1="6.4643" y1="5.3467" x2="7.8613" y2="5.3721" layer="21"/>
<rectangle x1="13.0683" y1="5.3467" x2="13.6271" y2="5.3721" layer="21"/>
<rectangle x1="0.6223" y1="5.3721" x2="1.4859" y2="5.3975" layer="21"/>
<rectangle x1="1.7145" y1="5.3721" x2="5.8801" y2="5.3975" layer="21"/>
<rectangle x1="6.4643" y1="5.3721" x2="7.8613" y2="5.3975" layer="21"/>
<rectangle x1="13.0683" y1="5.3721" x2="13.6271" y2="5.3975" layer="21"/>
<rectangle x1="0.6223" y1="5.3975" x2="1.5875" y2="5.4229" layer="21"/>
<rectangle x1="1.7145" y1="5.3975" x2="5.8801" y2="5.4229" layer="21"/>
<rectangle x1="6.4643" y1="5.3975" x2="7.8613" y2="5.4229" layer="21"/>
<rectangle x1="13.0683" y1="5.3975" x2="13.6271" y2="5.4229" layer="21"/>
<rectangle x1="0.6223" y1="5.4229" x2="1.6891" y2="5.4483" layer="21"/>
<rectangle x1="1.7145" y1="5.4229" x2="5.8801" y2="5.4483" layer="21"/>
<rectangle x1="6.4643" y1="5.4229" x2="7.8613" y2="5.4483" layer="21"/>
<rectangle x1="13.0683" y1="5.4229" x2="13.6271" y2="5.4483" layer="21"/>
<rectangle x1="0.6223" y1="5.4483" x2="5.8801" y2="5.4737" layer="21"/>
<rectangle x1="6.4643" y1="5.4483" x2="7.8613" y2="5.4737" layer="21"/>
<rectangle x1="13.0683" y1="5.4483" x2="13.6271" y2="5.4737" layer="21"/>
<rectangle x1="0.6223" y1="5.4737" x2="5.8801" y2="5.4991" layer="21"/>
<rectangle x1="6.4643" y1="5.4737" x2="7.8613" y2="5.4991" layer="21"/>
<rectangle x1="13.0683" y1="5.4737" x2="13.6271" y2="5.4991" layer="21"/>
<rectangle x1="0.6223" y1="5.4991" x2="5.8801" y2="5.5245" layer="21"/>
<rectangle x1="6.4643" y1="5.4991" x2="7.8613" y2="5.5245" layer="21"/>
<rectangle x1="13.0683" y1="5.4991" x2="13.6271" y2="5.5245" layer="21"/>
<rectangle x1="0.6223" y1="5.5245" x2="5.8801" y2="5.5499" layer="21"/>
<rectangle x1="6.4643" y1="5.5245" x2="7.8613" y2="5.5499" layer="21"/>
<rectangle x1="13.0683" y1="5.5245" x2="13.6271" y2="5.5499" layer="21"/>
<rectangle x1="0.6223" y1="5.5499" x2="5.8801" y2="5.5753" layer="21"/>
<rectangle x1="6.4643" y1="5.5499" x2="7.8613" y2="5.5753" layer="21"/>
<rectangle x1="13.0683" y1="5.5499" x2="13.6271" y2="5.5753" layer="21"/>
<rectangle x1="0.6223" y1="5.5753" x2="5.8801" y2="5.6007" layer="21"/>
<rectangle x1="6.4643" y1="5.5753" x2="7.8613" y2="5.6007" layer="21"/>
<rectangle x1="13.0683" y1="5.5753" x2="13.6271" y2="5.6007" layer="21"/>
<rectangle x1="0.6223" y1="5.6007" x2="5.8801" y2="5.6261" layer="21"/>
<rectangle x1="6.4643" y1="5.6007" x2="7.8613" y2="5.6261" layer="21"/>
<rectangle x1="13.0683" y1="5.6007" x2="13.6271" y2="5.6261" layer="21"/>
<rectangle x1="0.6223" y1="5.6261" x2="5.8801" y2="5.6515" layer="21"/>
<rectangle x1="6.4643" y1="5.6261" x2="7.8613" y2="5.6515" layer="21"/>
<rectangle x1="13.0683" y1="5.6261" x2="13.6271" y2="5.6515" layer="21"/>
<rectangle x1="0.6223" y1="5.6515" x2="5.8801" y2="5.6769" layer="21"/>
<rectangle x1="6.4643" y1="5.6515" x2="7.8613" y2="5.6769" layer="21"/>
<rectangle x1="13.0683" y1="5.6515" x2="13.6271" y2="5.6769" layer="21"/>
<rectangle x1="0.6223" y1="5.6769" x2="5.8801" y2="5.7023" layer="21"/>
<rectangle x1="6.4643" y1="5.6769" x2="7.8613" y2="5.7023" layer="21"/>
<rectangle x1="13.0683" y1="5.6769" x2="13.6271" y2="5.7023" layer="21"/>
<rectangle x1="0.6223" y1="5.7023" x2="5.8801" y2="5.7277" layer="21"/>
<rectangle x1="6.4643" y1="5.7023" x2="7.8613" y2="5.7277" layer="21"/>
<rectangle x1="13.0683" y1="5.7023" x2="13.6271" y2="5.7277" layer="21"/>
<rectangle x1="0.6223" y1="5.7277" x2="7.8613" y2="5.7531" layer="21"/>
<rectangle x1="0.6223" y1="5.7531" x2="7.8613" y2="5.7785" layer="21"/>
<rectangle x1="0.6223" y1="5.7785" x2="7.8613" y2="5.8039" layer="21"/>
<rectangle x1="0.6223" y1="5.8039" x2="7.8613" y2="5.8293" layer="21"/>
<rectangle x1="0.6223" y1="5.8293" x2="7.8613" y2="5.8547" layer="21"/>
<rectangle x1="0.6223" y1="5.8547" x2="7.8613" y2="5.8801" layer="21"/>
<rectangle x1="0.6223" y1="5.8801" x2="7.8613" y2="5.9055" layer="21"/>
<rectangle x1="0.6223" y1="5.9055" x2="7.8613" y2="5.9309" layer="21"/>
<rectangle x1="0.6223" y1="5.9309" x2="7.8613" y2="5.9563" layer="21"/>
<rectangle x1="0.6223" y1="5.9563" x2="7.8613" y2="5.9817" layer="21"/>
<rectangle x1="0.6223" y1="5.9817" x2="7.8613" y2="6.0071" layer="21"/>
<rectangle x1="0.6223" y1="6.0071" x2="7.8613" y2="6.0325" layer="21"/>
<rectangle x1="1.7907" y1="6.0325" x2="2.4257" y2="6.0579" layer="21"/>
<rectangle x1="3.1877" y1="6.0325" x2="3.8227" y2="6.0579" layer="21"/>
<rectangle x1="4.6355" y1="6.0325" x2="5.2705" y2="6.0579" layer="21"/>
<rectangle x1="6.0325" y1="6.0325" x2="6.6675" y2="6.0579" layer="21"/>
<rectangle x1="1.7907" y1="6.0579" x2="2.4257" y2="6.0833" layer="21"/>
<rectangle x1="3.1877" y1="6.0579" x2="3.7973" y2="6.0833" layer="21"/>
<rectangle x1="4.6355" y1="6.0579" x2="5.2451" y2="6.0833" layer="21"/>
<rectangle x1="6.0325" y1="6.0579" x2="6.6421" y2="6.0833" layer="21"/>
<rectangle x1="1.7907" y1="6.0833" x2="2.4257" y2="6.1087" layer="21"/>
<rectangle x1="3.1877" y1="6.0833" x2="3.7973" y2="6.1087" layer="21"/>
<rectangle x1="4.6355" y1="6.0833" x2="5.2451" y2="6.1087" layer="21"/>
<rectangle x1="6.0325" y1="6.0833" x2="6.6421" y2="6.1087" layer="21"/>
<rectangle x1="1.7907" y1="6.1087" x2="2.4257" y2="6.1341" layer="21"/>
<rectangle x1="3.1877" y1="6.1087" x2="3.7973" y2="6.1341" layer="21"/>
<rectangle x1="4.6355" y1="6.1087" x2="5.2451" y2="6.1341" layer="21"/>
<rectangle x1="6.0325" y1="6.1087" x2="6.6421" y2="6.1341" layer="21"/>
<rectangle x1="1.7907" y1="6.1341" x2="2.4257" y2="6.1595" layer="21"/>
<rectangle x1="3.1877" y1="6.1341" x2="3.7973" y2="6.1595" layer="21"/>
<rectangle x1="4.6355" y1="6.1341" x2="5.2451" y2="6.1595" layer="21"/>
<rectangle x1="6.0325" y1="6.1341" x2="6.6421" y2="6.1595" layer="21"/>
<rectangle x1="1.7907" y1="6.1595" x2="2.4257" y2="6.1849" layer="21"/>
<rectangle x1="3.1877" y1="6.1595" x2="3.7973" y2="6.1849" layer="21"/>
<rectangle x1="4.6355" y1="6.1595" x2="5.2451" y2="6.1849" layer="21"/>
<rectangle x1="6.0325" y1="6.1595" x2="6.6421" y2="6.1849" layer="21"/>
<rectangle x1="1.7907" y1="6.1849" x2="2.4257" y2="6.2103" layer="21"/>
<rectangle x1="3.1877" y1="6.1849" x2="3.7973" y2="6.2103" layer="21"/>
<rectangle x1="4.6355" y1="6.1849" x2="5.2451" y2="6.2103" layer="21"/>
<rectangle x1="6.0325" y1="6.1849" x2="6.6421" y2="6.2103" layer="21"/>
<rectangle x1="1.7907" y1="6.2103" x2="2.4257" y2="6.2357" layer="21"/>
<rectangle x1="3.1877" y1="6.2103" x2="3.7973" y2="6.2357" layer="21"/>
<rectangle x1="4.6355" y1="6.2103" x2="5.2451" y2="6.2357" layer="21"/>
<rectangle x1="6.0325" y1="6.2103" x2="6.6421" y2="6.2357" layer="21"/>
<rectangle x1="1.7907" y1="6.2357" x2="2.4257" y2="6.2611" layer="21"/>
<rectangle x1="3.1877" y1="6.2357" x2="3.7973" y2="6.2611" layer="21"/>
<rectangle x1="4.6355" y1="6.2357" x2="5.2451" y2="6.2611" layer="21"/>
<rectangle x1="6.0325" y1="6.2357" x2="6.6421" y2="6.2611" layer="21"/>
<rectangle x1="1.7907" y1="6.2611" x2="2.4257" y2="6.2865" layer="21"/>
<rectangle x1="3.1877" y1="6.2611" x2="3.7973" y2="6.2865" layer="21"/>
<rectangle x1="4.6355" y1="6.2611" x2="5.2451" y2="6.2865" layer="21"/>
<rectangle x1="6.0325" y1="6.2611" x2="6.6421" y2="6.2865" layer="21"/>
<rectangle x1="1.7907" y1="6.2865" x2="2.4257" y2="6.3119" layer="21"/>
<rectangle x1="3.1877" y1="6.2865" x2="3.7973" y2="6.3119" layer="21"/>
<rectangle x1="4.6355" y1="6.2865" x2="5.2451" y2="6.3119" layer="21"/>
<rectangle x1="6.0325" y1="6.2865" x2="6.6421" y2="6.3119" layer="21"/>
<rectangle x1="1.7907" y1="6.3119" x2="2.4257" y2="6.3373" layer="21"/>
<rectangle x1="3.1877" y1="6.3119" x2="3.7973" y2="6.3373" layer="21"/>
<rectangle x1="4.6355" y1="6.3119" x2="5.2451" y2="6.3373" layer="21"/>
<rectangle x1="6.0325" y1="6.3119" x2="6.6421" y2="6.3373" layer="21"/>
<rectangle x1="1.7907" y1="6.3373" x2="2.4257" y2="6.3627" layer="21"/>
<rectangle x1="3.1877" y1="6.3373" x2="3.7973" y2="6.3627" layer="21"/>
<rectangle x1="4.6355" y1="6.3373" x2="5.2451" y2="6.3627" layer="21"/>
<rectangle x1="6.0325" y1="6.3373" x2="6.6421" y2="6.3627" layer="21"/>
<rectangle x1="1.7907" y1="6.3627" x2="2.4257" y2="6.3881" layer="21"/>
<rectangle x1="3.1877" y1="6.3627" x2="3.7973" y2="6.3881" layer="21"/>
<rectangle x1="4.6355" y1="6.3627" x2="5.2451" y2="6.3881" layer="21"/>
<rectangle x1="6.0325" y1="6.3627" x2="6.6421" y2="6.3881" layer="21"/>
<rectangle x1="1.7907" y1="6.3881" x2="2.4257" y2="6.4135" layer="21"/>
<rectangle x1="3.1877" y1="6.3881" x2="3.7973" y2="6.4135" layer="21"/>
<rectangle x1="4.6355" y1="6.3881" x2="5.2451" y2="6.4135" layer="21"/>
<rectangle x1="6.0325" y1="6.3881" x2="6.6421" y2="6.4135" layer="21"/>
<rectangle x1="1.7907" y1="6.4135" x2="2.4257" y2="6.4389" layer="21"/>
<rectangle x1="3.1877" y1="6.4135" x2="3.7973" y2="6.4389" layer="21"/>
<rectangle x1="4.6355" y1="6.4135" x2="5.2451" y2="6.4389" layer="21"/>
<rectangle x1="6.0325" y1="6.4135" x2="6.6421" y2="6.4389" layer="21"/>
<rectangle x1="1.7907" y1="6.4389" x2="2.4257" y2="6.4643" layer="21"/>
<rectangle x1="3.1877" y1="6.4389" x2="3.7973" y2="6.4643" layer="21"/>
<rectangle x1="4.6355" y1="6.4389" x2="5.2451" y2="6.4643" layer="21"/>
<rectangle x1="6.0325" y1="6.4389" x2="6.6421" y2="6.4643" layer="21"/>
<rectangle x1="1.7907" y1="6.4643" x2="2.4257" y2="6.4897" layer="21"/>
<rectangle x1="3.1877" y1="6.4643" x2="3.7973" y2="6.4897" layer="21"/>
<rectangle x1="4.6355" y1="6.4643" x2="5.2451" y2="6.4897" layer="21"/>
<rectangle x1="6.0325" y1="6.4643" x2="6.6421" y2="6.4897" layer="21"/>
<rectangle x1="1.7907" y1="6.4897" x2="2.4257" y2="6.5151" layer="21"/>
<rectangle x1="3.1877" y1="6.4897" x2="3.7973" y2="6.5151" layer="21"/>
<rectangle x1="4.6355" y1="6.4897" x2="5.2451" y2="6.5151" layer="21"/>
<rectangle x1="6.0325" y1="6.4897" x2="6.6421" y2="6.5151" layer="21"/>
<rectangle x1="1.7907" y1="6.5151" x2="2.4257" y2="6.5405" layer="21"/>
<rectangle x1="3.1877" y1="6.5151" x2="3.7973" y2="6.5405" layer="21"/>
<rectangle x1="4.6355" y1="6.5151" x2="5.2451" y2="6.5405" layer="21"/>
<rectangle x1="6.0325" y1="6.5151" x2="6.6421" y2="6.5405" layer="21"/>
<rectangle x1="1.7907" y1="6.5405" x2="2.4257" y2="6.5659" layer="21"/>
<rectangle x1="3.1877" y1="6.5405" x2="3.7973" y2="6.5659" layer="21"/>
<rectangle x1="4.6355" y1="6.5405" x2="5.2451" y2="6.5659" layer="21"/>
<rectangle x1="6.0325" y1="6.5405" x2="6.6421" y2="6.5659" layer="21"/>
<rectangle x1="1.7907" y1="6.5659" x2="2.4257" y2="6.5913" layer="21"/>
<rectangle x1="3.1877" y1="6.5659" x2="3.7973" y2="6.5913" layer="21"/>
<rectangle x1="4.6355" y1="6.5659" x2="5.2451" y2="6.5913" layer="21"/>
<rectangle x1="6.0325" y1="6.5659" x2="6.6421" y2="6.5913" layer="21"/>
<rectangle x1="1.7907" y1="6.5913" x2="2.4257" y2="6.6167" layer="21"/>
<rectangle x1="3.1877" y1="6.5913" x2="3.7973" y2="6.6167" layer="21"/>
<rectangle x1="4.6355" y1="6.5913" x2="5.2451" y2="6.6167" layer="21"/>
<rectangle x1="6.0325" y1="6.5913" x2="6.6421" y2="6.6167" layer="21"/>
<rectangle x1="1.7907" y1="6.6167" x2="2.4257" y2="6.6421" layer="21"/>
<rectangle x1="3.1877" y1="6.6167" x2="3.7973" y2="6.6421" layer="21"/>
<rectangle x1="4.6355" y1="6.6167" x2="5.2451" y2="6.6421" layer="21"/>
<rectangle x1="6.0325" y1="6.6167" x2="6.6421" y2="6.6421" layer="21"/>
<rectangle x1="1.7907" y1="6.6421" x2="2.4257" y2="6.6675" layer="21"/>
<rectangle x1="3.1877" y1="6.6421" x2="3.7973" y2="6.6675" layer="21"/>
<rectangle x1="4.6355" y1="6.6421" x2="5.2451" y2="6.6675" layer="21"/>
<rectangle x1="6.0325" y1="6.6421" x2="6.6421" y2="6.6675" layer="21"/>
<rectangle x1="1.7907" y1="6.6675" x2="2.4257" y2="6.6929" layer="21"/>
<rectangle x1="3.1877" y1="6.6675" x2="3.7973" y2="6.6929" layer="21"/>
<rectangle x1="4.6355" y1="6.6675" x2="5.2451" y2="6.6929" layer="21"/>
<rectangle x1="6.0325" y1="6.6675" x2="6.6421" y2="6.6929" layer="21"/>
<rectangle x1="1.7907" y1="6.6929" x2="2.4257" y2="6.7183" layer="21"/>
<rectangle x1="3.1877" y1="6.6929" x2="3.7973" y2="6.7183" layer="21"/>
<rectangle x1="4.6355" y1="6.6929" x2="5.2451" y2="6.7183" layer="21"/>
<rectangle x1="6.0325" y1="6.6929" x2="6.6421" y2="6.7183" layer="21"/>
<rectangle x1="1.7907" y1="6.7183" x2="2.4257" y2="6.7437" layer="21"/>
<rectangle x1="3.1877" y1="6.7183" x2="3.7973" y2="6.7437" layer="21"/>
<rectangle x1="4.6355" y1="6.7183" x2="5.2451" y2="6.7437" layer="21"/>
<rectangle x1="6.0325" y1="6.7183" x2="6.6421" y2="6.7437" layer="21"/>
<rectangle x1="1.7907" y1="6.7437" x2="2.4257" y2="6.7691" layer="21"/>
<rectangle x1="3.1877" y1="6.7437" x2="3.7973" y2="6.7691" layer="21"/>
<rectangle x1="4.6355" y1="6.7437" x2="5.2451" y2="6.7691" layer="21"/>
<rectangle x1="6.0325" y1="6.7437" x2="6.6421" y2="6.7691" layer="21"/>
<rectangle x1="1.7907" y1="6.7691" x2="2.4257" y2="6.7945" layer="21"/>
<rectangle x1="3.1877" y1="6.7691" x2="3.7973" y2="6.7945" layer="21"/>
<rectangle x1="4.6355" y1="6.7691" x2="5.2451" y2="6.7945" layer="21"/>
<rectangle x1="6.0325" y1="6.7691" x2="6.6421" y2="6.7945" layer="21"/>
<rectangle x1="1.7907" y1="6.7945" x2="2.4257" y2="6.8199" layer="21"/>
<rectangle x1="3.1877" y1="6.7945" x2="3.7973" y2="6.8199" layer="21"/>
<rectangle x1="4.6355" y1="6.7945" x2="5.2451" y2="6.8199" layer="21"/>
<rectangle x1="6.0325" y1="6.7945" x2="6.6421" y2="6.8199" layer="21"/>
<rectangle x1="1.7907" y1="6.8199" x2="2.4257" y2="6.8453" layer="21"/>
<rectangle x1="3.1877" y1="6.8199" x2="3.7973" y2="6.8453" layer="21"/>
<rectangle x1="4.6355" y1="6.8199" x2="5.2451" y2="6.8453" layer="21"/>
<rectangle x1="6.0325" y1="6.8199" x2="6.6421" y2="6.8453" layer="21"/>
<rectangle x1="1.7907" y1="6.8453" x2="2.4257" y2="6.8707" layer="21"/>
<rectangle x1="3.1877" y1="6.8453" x2="3.7973" y2="6.8707" layer="21"/>
<rectangle x1="4.6355" y1="6.8453" x2="5.2451" y2="6.8707" layer="21"/>
<rectangle x1="6.0325" y1="6.8453" x2="6.6421" y2="6.8707" layer="21"/>
<rectangle x1="1.7907" y1="6.8707" x2="2.4257" y2="6.8961" layer="21"/>
<rectangle x1="3.1877" y1="6.8707" x2="3.7973" y2="6.8961" layer="21"/>
<rectangle x1="4.6355" y1="6.8707" x2="5.2451" y2="6.8961" layer="21"/>
<rectangle x1="6.0325" y1="6.8707" x2="6.6421" y2="6.8961" layer="21"/>
<rectangle x1="1.7907" y1="6.8961" x2="2.4257" y2="6.9215" layer="21"/>
<rectangle x1="3.1877" y1="6.8961" x2="3.7973" y2="6.9215" layer="21"/>
<rectangle x1="4.6355" y1="6.8961" x2="5.2451" y2="6.9215" layer="21"/>
<rectangle x1="6.0325" y1="6.8961" x2="6.6421" y2="6.9215" layer="21"/>
<rectangle x1="1.7907" y1="6.9215" x2="2.4257" y2="6.9469" layer="21"/>
<rectangle x1="3.1877" y1="6.9215" x2="3.7973" y2="6.9469" layer="21"/>
<rectangle x1="4.6355" y1="6.9215" x2="5.2451" y2="6.9469" layer="21"/>
<rectangle x1="6.0325" y1="6.9215" x2="6.6421" y2="6.9469" layer="21"/>
<rectangle x1="1.7907" y1="6.9469" x2="2.4257" y2="6.9723" layer="21"/>
<rectangle x1="3.1877" y1="6.9469" x2="3.7973" y2="6.9723" layer="21"/>
<rectangle x1="4.6355" y1="6.9469" x2="5.2451" y2="6.9723" layer="21"/>
<rectangle x1="6.0325" y1="6.9469" x2="6.6421" y2="6.9723" layer="21"/>
<rectangle x1="1.7907" y1="6.9723" x2="2.4257" y2="6.9977" layer="21"/>
<rectangle x1="3.1877" y1="6.9723" x2="3.7973" y2="6.9977" layer="21"/>
<rectangle x1="4.6355" y1="6.9723" x2="5.2451" y2="6.9977" layer="21"/>
<rectangle x1="6.0325" y1="6.9723" x2="6.6421" y2="6.9977" layer="21"/>
<rectangle x1="1.7907" y1="6.9977" x2="2.4257" y2="7.0231" layer="21"/>
<rectangle x1="4.6355" y1="6.9977" x2="5.2451" y2="7.0231" layer="21"/>
<rectangle x1="6.0325" y1="6.9977" x2="6.6421" y2="7.0231" layer="21"/>
<rectangle x1="1.7907" y1="7.0231" x2="2.4257" y2="7.0485" layer="21"/>
<rectangle x1="4.6355" y1="7.0231" x2="5.2451" y2="7.0485" layer="21"/>
<rectangle x1="6.0325" y1="7.0231" x2="6.6421" y2="7.0485" layer="21"/>
<rectangle x1="1.7907" y1="7.0485" x2="2.4257" y2="7.0739" layer="21"/>
<rectangle x1="4.6355" y1="7.0485" x2="5.2451" y2="7.0739" layer="21"/>
<rectangle x1="6.0325" y1="7.0485" x2="6.6421" y2="7.0739" layer="21"/>
<rectangle x1="1.7907" y1="7.0739" x2="2.4257" y2="7.0993" layer="21"/>
<rectangle x1="4.6355" y1="7.0739" x2="5.2451" y2="7.0993" layer="21"/>
<rectangle x1="6.0325" y1="7.0739" x2="6.6421" y2="7.0993" layer="21"/>
<rectangle x1="1.7907" y1="7.0993" x2="2.4257" y2="7.1247" layer="21"/>
<rectangle x1="4.6355" y1="7.0993" x2="5.2451" y2="7.1247" layer="21"/>
<rectangle x1="6.0325" y1="7.0993" x2="6.6421" y2="7.1247" layer="21"/>
<rectangle x1="1.7907" y1="7.1247" x2="2.4257" y2="7.1501" layer="21"/>
<rectangle x1="4.6355" y1="7.1247" x2="5.2451" y2="7.1501" layer="21"/>
<rectangle x1="6.0325" y1="7.1247" x2="6.6421" y2="7.1501" layer="21"/>
<rectangle x1="1.7907" y1="7.1501" x2="2.4257" y2="7.1755" layer="21"/>
<rectangle x1="4.6355" y1="7.1501" x2="5.2451" y2="7.1755" layer="21"/>
<rectangle x1="6.0325" y1="7.1501" x2="6.6421" y2="7.1755" layer="21"/>
<rectangle x1="1.7907" y1="7.1755" x2="2.4257" y2="7.2009" layer="21"/>
<rectangle x1="4.6355" y1="7.1755" x2="5.2451" y2="7.2009" layer="21"/>
<rectangle x1="6.0325" y1="7.1755" x2="6.6421" y2="7.2009" layer="21"/>
<rectangle x1="1.7907" y1="7.2009" x2="2.4257" y2="7.2263" layer="21"/>
<rectangle x1="4.6355" y1="7.2009" x2="5.2451" y2="7.2263" layer="21"/>
<rectangle x1="6.0325" y1="7.2009" x2="6.6421" y2="7.2263" layer="21"/>
<rectangle x1="1.7907" y1="7.2263" x2="2.4257" y2="7.2517" layer="21"/>
<rectangle x1="4.6355" y1="7.2263" x2="5.2451" y2="7.2517" layer="21"/>
<rectangle x1="6.0325" y1="7.2263" x2="6.6421" y2="7.2517" layer="21"/>
<rectangle x1="1.7907" y1="7.2517" x2="2.4257" y2="7.2771" layer="21"/>
<rectangle x1="4.6355" y1="7.2517" x2="5.2451" y2="7.2771" layer="21"/>
<rectangle x1="6.0325" y1="7.2517" x2="6.6421" y2="7.2771" layer="21"/>
<rectangle x1="1.7907" y1="7.2771" x2="2.4257" y2="7.3025" layer="21"/>
<rectangle x1="4.6355" y1="7.2771" x2="5.2451" y2="7.3025" layer="21"/>
<rectangle x1="6.0325" y1="7.2771" x2="6.6421" y2="7.3025" layer="21"/>
<rectangle x1="1.7907" y1="7.3025" x2="2.4257" y2="7.3279" layer="21"/>
<rectangle x1="4.6355" y1="7.3025" x2="5.2451" y2="7.3279" layer="21"/>
<rectangle x1="6.0325" y1="7.3025" x2="6.6421" y2="7.3279" layer="21"/>
<rectangle x1="1.7907" y1="7.3279" x2="2.4257" y2="7.3533" layer="21"/>
<rectangle x1="4.6355" y1="7.3279" x2="5.2451" y2="7.3533" layer="21"/>
<rectangle x1="6.0325" y1="7.3279" x2="6.6421" y2="7.3533" layer="21"/>
<rectangle x1="1.7907" y1="7.3533" x2="2.4257" y2="7.3787" layer="21"/>
<rectangle x1="4.6355" y1="7.3533" x2="5.2451" y2="7.3787" layer="21"/>
<rectangle x1="6.0325" y1="7.3533" x2="6.6421" y2="7.3787" layer="21"/>
<rectangle x1="1.7907" y1="7.3787" x2="2.4257" y2="7.4041" layer="21"/>
<rectangle x1="4.6355" y1="7.3787" x2="5.2451" y2="7.4041" layer="21"/>
<rectangle x1="6.0325" y1="7.3787" x2="6.6421" y2="7.4041" layer="21"/>
<rectangle x1="1.7907" y1="7.4041" x2="2.4257" y2="7.4295" layer="21"/>
<rectangle x1="4.6355" y1="7.4041" x2="5.2451" y2="7.4295" layer="21"/>
<rectangle x1="6.0325" y1="7.4041" x2="6.6421" y2="7.4295" layer="21"/>
<rectangle x1="1.7907" y1="7.4295" x2="2.4257" y2="7.4549" layer="21"/>
<rectangle x1="4.6355" y1="7.4295" x2="5.2451" y2="7.4549" layer="21"/>
<rectangle x1="6.0325" y1="7.4295" x2="6.6421" y2="7.4549" layer="21"/>
<rectangle x1="1.7907" y1="7.4549" x2="2.4257" y2="7.4803" layer="21"/>
<rectangle x1="4.6355" y1="7.4549" x2="5.2451" y2="7.4803" layer="21"/>
<rectangle x1="6.0325" y1="7.4549" x2="6.6421" y2="7.4803" layer="21"/>
<rectangle x1="1.7907" y1="7.4803" x2="2.4257" y2="7.5057" layer="21"/>
<rectangle x1="4.6355" y1="7.4803" x2="5.2451" y2="7.5057" layer="21"/>
<rectangle x1="6.0325" y1="7.4803" x2="6.6421" y2="7.5057" layer="21"/>
<rectangle x1="1.7907" y1="7.5057" x2="2.4257" y2="7.5311" layer="21"/>
<rectangle x1="4.6355" y1="7.5057" x2="5.2451" y2="7.5311" layer="21"/>
<rectangle x1="6.0325" y1="7.5057" x2="6.6421" y2="7.5311" layer="21"/>
<rectangle x1="1.7907" y1="7.5311" x2="2.4257" y2="7.5565" layer="21"/>
<rectangle x1="4.6355" y1="7.5311" x2="5.2451" y2="7.5565" layer="21"/>
<rectangle x1="6.0325" y1="7.5311" x2="6.6421" y2="7.5565" layer="21"/>
<rectangle x1="1.7907" y1="7.5565" x2="2.4257" y2="7.5819" layer="21"/>
<rectangle x1="4.6355" y1="7.5565" x2="5.2451" y2="7.5819" layer="21"/>
<rectangle x1="6.0325" y1="7.5565" x2="6.6421" y2="7.5819" layer="21"/>
<rectangle x1="1.7907" y1="7.5819" x2="2.4257" y2="7.6073" layer="21"/>
<rectangle x1="4.6355" y1="7.5819" x2="5.2451" y2="7.6073" layer="21"/>
<rectangle x1="6.0325" y1="7.5819" x2="6.6421" y2="7.6073" layer="21"/>
<rectangle x1="1.7907" y1="7.6073" x2="2.4257" y2="7.6327" layer="21"/>
<rectangle x1="4.6355" y1="7.6073" x2="5.2451" y2="7.6327" layer="21"/>
<rectangle x1="6.0325" y1="7.6073" x2="6.6421" y2="7.6327" layer="21"/>
<rectangle x1="1.7907" y1="7.6327" x2="2.4257" y2="7.6581" layer="21"/>
<rectangle x1="4.6355" y1="7.6327" x2="5.2451" y2="7.6581" layer="21"/>
<rectangle x1="6.0325" y1="7.6327" x2="6.6421" y2="7.6581" layer="21"/>
<rectangle x1="1.7907" y1="7.6581" x2="2.4257" y2="7.6835" layer="21"/>
<rectangle x1="4.6355" y1="7.6581" x2="5.2451" y2="7.6835" layer="21"/>
<rectangle x1="6.0325" y1="7.6581" x2="6.6421" y2="7.6835" layer="21"/>
<rectangle x1="1.7907" y1="7.6835" x2="2.4257" y2="7.7089" layer="21"/>
<rectangle x1="4.6355" y1="7.6835" x2="5.2451" y2="7.7089" layer="21"/>
<rectangle x1="6.0325" y1="7.6835" x2="6.6421" y2="7.7089" layer="21"/>
<rectangle x1="1.7907" y1="7.7089" x2="2.4257" y2="7.7343" layer="21"/>
<rectangle x1="4.6355" y1="7.7089" x2="5.2451" y2="7.7343" layer="21"/>
<rectangle x1="6.0325" y1="7.7089" x2="6.6421" y2="7.7343" layer="21"/>
<rectangle x1="4.6355" y1="7.7343" x2="5.2451" y2="7.7597" layer="21"/>
<rectangle x1="4.6355" y1="7.7597" x2="5.2451" y2="7.7851" layer="21"/>
<rectangle x1="4.6355" y1="7.7851" x2="5.2451" y2="7.8105" layer="21"/>
<rectangle x1="4.6355" y1="7.8105" x2="5.2451" y2="7.8359" layer="21"/>
<rectangle x1="4.6355" y1="7.8359" x2="5.2451" y2="7.8613" layer="21"/>
<rectangle x1="4.6355" y1="7.8613" x2="5.2451" y2="7.8867" layer="21"/>
<rectangle x1="4.6355" y1="7.8867" x2="5.2451" y2="7.9121" layer="21"/>
<rectangle x1="4.6355" y1="7.9121" x2="5.2451" y2="7.9375" layer="21"/>
<rectangle x1="4.6355" y1="7.9375" x2="5.2451" y2="7.9629" layer="21"/>
<rectangle x1="4.6355" y1="7.9629" x2="5.2451" y2="7.9883" layer="21"/>
<rectangle x1="4.6355" y1="7.9883" x2="5.2451" y2="8.0137" layer="21"/>
<rectangle x1="4.6355" y1="8.0137" x2="5.2451" y2="8.0391" layer="21"/>
<rectangle x1="4.6355" y1="8.0391" x2="5.2451" y2="8.0645" layer="21"/>
<rectangle x1="4.6355" y1="8.0645" x2="5.2451" y2="8.0899" layer="21"/>
<rectangle x1="4.6355" y1="8.0899" x2="5.2451" y2="8.1153" layer="21"/>
<rectangle x1="4.6355" y1="8.1153" x2="5.2451" y2="8.1407" layer="21"/>
<rectangle x1="4.6355" y1="8.1407" x2="5.2451" y2="8.1661" layer="21"/>
<rectangle x1="4.6355" y1="8.1661" x2="5.2451" y2="8.1915" layer="21"/>
<rectangle x1="4.6355" y1="8.1915" x2="5.2451" y2="8.2169" layer="21"/>
<rectangle x1="4.6355" y1="8.2169" x2="5.2451" y2="8.2423" layer="21"/>
<rectangle x1="4.6355" y1="8.2423" x2="5.2451" y2="8.2677" layer="21"/>
<rectangle x1="4.6355" y1="8.2677" x2="5.2451" y2="8.2931" layer="21"/>
<rectangle x1="4.6355" y1="8.2931" x2="5.2451" y2="8.3185" layer="21"/>
<rectangle x1="4.6355" y1="8.3185" x2="5.2451" y2="8.3439" layer="21"/>
<rectangle x1="4.6355" y1="8.3439" x2="5.2451" y2="8.3693" layer="21"/>
<rectangle x1="4.6355" y1="8.3693" x2="5.2451" y2="8.3947" layer="21"/>
<rectangle x1="4.6355" y1="8.3947" x2="5.2451" y2="8.4201" layer="21"/>
<rectangle x1="4.6355" y1="8.4201" x2="5.2451" y2="8.4455" layer="21"/>
<rectangle x1="4.6355" y1="8.4455" x2="5.2451" y2="8.4709" layer="21"/>
<rectangle x1="4.6355" y1="8.4709" x2="5.2451" y2="8.4963" layer="21"/>
<rectangle x1="4.6355" y1="8.4963" x2="5.2451" y2="8.5217" layer="21"/>
<rectangle x1="4.6355" y1="8.5217" x2="5.2451" y2="8.5471" layer="21"/>
<rectangle x1="4.6355" y1="8.5471" x2="5.2451" y2="8.5725" layer="21"/>
<rectangle x1="4.6355" y1="8.5725" x2="5.2451" y2="8.5979" layer="21"/>
</package>
<package name="CABLECAL12">
<pad name="+" x="0" y="3.81" drill="2.3" diameter="3.5"/>
<pad name="-" x="0" y="-3.81" drill="2.3" diameter="3.5"/>
<wire x1="-3.81" y1="6.35" x2="3.81" y2="6.35" width="0.127" layer="21"/>
<wire x1="3.81" y1="6.35" x2="3.81" y2="-6.35" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.35" x2="-3.81" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-6.35" x2="-3.81" y2="6.35" width="0.127" layer="21"/>
<text x="-3.175" y="3.175" size="1.27" layer="21">+</text>
<text x="-3.175" y="-3.81" size="1.27" layer="21">-</text>
<text x="-3.81" y="7.62" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="LOGOTECHMAKE">
<wire x1="-12.7" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<text x="-10.16" y="2.54" size="1.778" layer="94">Techmake Logo</text>
</symbol>
<symbol name="CABLECAL12">
<wire x1="-2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<pin name="+" x="-7.62" y="2.54" length="middle"/>
<pin name="-" x="-7.62" y="-2.54" length="middle"/>
<text x="-2.54" y="7.62" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TECHMAKELOGO">
<gates>
<gate name="G$1" symbol="LOGOTECHMAKE" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="LOGOTECHMAKE">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CABLECAL12">
<gates>
<gate name="G$1" symbol="CABLECAL12" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CABLECAL12">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Aix_IC">
<packages>
<package name="SP3222EUEY-L/TR">
<smd name="EN" x="-2.97" y="2.925" dx="1.78" dy="0.42" layer="1"/>
<smd name="C1+" x="-2.97" y="2.275" dx="1.78" dy="0.42" layer="1"/>
<smd name="V+" x="-2.97" y="1.625" dx="1.78" dy="0.42" layer="1"/>
<smd name="C1-" x="-2.97" y="0.975" dx="1.78" dy="0.42" layer="1"/>
<smd name="C2+" x="-2.97" y="0.325" dx="1.78" dy="0.42" layer="1"/>
<smd name="C2-" x="-2.97" y="-0.325" dx="1.78" dy="0.42" layer="1"/>
<smd name="V-" x="-2.97" y="-0.975" dx="1.78" dy="0.42" layer="1"/>
<smd name="T2OUT" x="-2.97" y="-1.625" dx="1.78" dy="0.42" layer="1"/>
<smd name="R2IN" x="-2.97" y="-2.275" dx="1.78" dy="0.42" layer="1"/>
<smd name="R2OUT" x="-2.97" y="-2.925" dx="1.78" dy="0.42" layer="1"/>
<smd name="SHDN" x="2.97" y="2.925" dx="1.78" dy="0.42" layer="1"/>
<smd name="VCC" x="2.97" y="2.275" dx="1.78" dy="0.42" layer="1"/>
<smd name="GND" x="2.97" y="1.625" dx="1.78" dy="0.42" layer="1"/>
<smd name="T1OUT" x="2.97" y="0.975" dx="1.78" dy="0.42" layer="1"/>
<smd name="R1IN" x="2.97" y="0.325" dx="1.78" dy="0.42" layer="1"/>
<smd name="N.C." x="2.97" y="-0.975" dx="1.78" dy="0.42" layer="1"/>
<smd name="R1OUT" x="2.97" y="-0.325" dx="1.78" dy="0.42" layer="1"/>
<smd name="T1IN" x="2.97" y="-1.625" dx="1.78" dy="0.42" layer="1"/>
<smd name="T2IN" x="2.97" y="-2.275" dx="1.78" dy="0.42" layer="1"/>
<smd name="N.C.2" x="2.97" y="-2.925" dx="1.78" dy="0.42" layer="1"/>
<wire x1="-2.2" y1="-3.25" x2="2.2" y2="-3.25" width="0.127" layer="21"/>
<wire x1="2.2" y1="-3.25" x2="2.2" y2="3.25" width="0.127" layer="21"/>
<wire x1="2.2" y1="3.25" x2="-2.2" y2="3.25" width="0.127" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.127" layer="21"/>
<text x="-3.81" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SP3222EUEY-L/TR">
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<pin name="EN" x="-15.24" y="12.7" length="middle"/>
<pin name="C1+" x="-15.24" y="10.16" length="middle"/>
<pin name="V+" x="-15.24" y="7.62" length="middle"/>
<pin name="C1-" x="-15.24" y="5.08" length="middle"/>
<pin name="C2+" x="-15.24" y="2.54" length="middle"/>
<pin name="C2-" x="-15.24" y="0" length="middle"/>
<pin name="V-" x="-15.24" y="-2.54" length="middle"/>
<pin name="T2OUT" x="-15.24" y="-5.08" length="middle"/>
<pin name="R2IN" x="-15.24" y="-7.62" length="middle"/>
<pin name="R2OUT" x="-15.24" y="-10.16" length="middle"/>
<pin name="SHDN" x="15.24" y="12.7" length="middle" rot="R180"/>
<pin name="VCC" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="GND" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="T1OUT" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="R1IN" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="R1OUT" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="N.C." x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="T1IN" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="T2IN" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="N.C.2" x="15.24" y="-10.16" length="middle" rot="R180"/>
<text x="-10.16" y="17.78" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SP3222EUEY-L/TR">
<gates>
<gate name="G$1" symbol="SP3222EUEY-L/TR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SP3222EUEY-L/TR">
<connects>
<connect gate="G$1" pin="C1+" pad="C1+"/>
<connect gate="G$1" pin="C1-" pad="C1-"/>
<connect gate="G$1" pin="C2+" pad="C2+"/>
<connect gate="G$1" pin="C2-" pad="C2-"/>
<connect gate="G$1" pin="EN" pad="EN"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="N.C." pad="N.C."/>
<connect gate="G$1" pin="N.C.2" pad="N.C.2"/>
<connect gate="G$1" pin="R1IN" pad="R1IN"/>
<connect gate="G$1" pin="R1OUT" pad="R1OUT"/>
<connect gate="G$1" pin="R2IN" pad="R2IN"/>
<connect gate="G$1" pin="R2OUT" pad="R2OUT"/>
<connect gate="G$1" pin="SHDN" pad="SHDN"/>
<connect gate="G$1" pin="T1IN" pad="T1IN"/>
<connect gate="G$1" pin="T1OUT" pad="T1OUT"/>
<connect gate="G$1" pin="T2IN" pad="T2IN"/>
<connect gate="G$1" pin="T2OUT" pad="T2OUT"/>
<connect gate="G$1" pin="V+" pad="V+"/>
<connect gate="G$1" pin="V-" pad="V-"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;map name="nav_main"&gt;
&lt;area shape="rect" coords="0,1,140,23" href="../military_specs.asp" title=""&gt;
&lt;area shape="rect" coords="0,24,140,51" href="../about.asp" title=""&gt;
&lt;area shape="rect" coords="1,52,140,77" href="../rfq.asp" title=""&gt;
&lt;area shape="rect" coords="0,78,139,103" href="../products.asp" title=""&gt;
&lt;area shape="rect" coords="1,102,138,128" href="../excess_inventory.asp" title=""&gt;
&lt;area shape="rect" coords="1,129,138,150" href="../edge.asp" title=""&gt;
&lt;area shape="rect" coords="1,151,139,178" href="../industry_links.asp" title=""&gt;
&lt;area shape="rect" coords="0,179,139,201" href="../comments.asp" title=""&gt;
&lt;area shape="rect" coords="1,203,138,231" href="../directory.asp" title=""&gt;
&lt;area shape="default" nohref&gt;
&lt;/map&gt;

&lt;html&gt;

&lt;title&gt;&lt;/title&gt;

 &lt;LINK REL="StyleSheet" TYPE="text/css" HREF="style-sheet.css"&gt;

&lt;body bgcolor="#ffffff" text="#000000" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0"&gt;
&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0 height="55%"&gt;
&lt;tr valign="top"&gt;

&lt;/td&gt;
&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/BODY&gt;&lt;/HTML&gt;</description>
<packages>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.683" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.826" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.032" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.683" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.826" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="C-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-US" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1005" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.2032" drill="0">
</class>
<class number="1" name="power" width="0.254" drill="0">
</class>
</classes>
<parts>
<part name="J3" library="adafruit" deviceset="1X2" device="-3.5MM"/>
<part name="P+1" library="supply1" deviceset="VCC" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="D1" library="SparkFun" deviceset="DIODE" device="SOD" value="BAS16"/>
<part name="C3" library="SparkFun" deviceset="CAP_POL" device="1206" value="10uF"/>
<part name="R2" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="10K"/>
<part name="GND7" library="SparkFun" deviceset="GND" device=""/>
<part name="GND8" library="SparkFun" deviceset="GND" device=""/>
<part name="R1" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="10K"/>
<part name="P+5" library="SparkFun" deviceset="VCC" device=""/>
<part name="U2" library="SparkFun" deviceset="XN04312" device="SOT" value="XN04312"/>
<part name="LED1" library="SparkFun" deviceset="LED" device="1206" value="Red"/>
<part name="LED3" library="SparkFun" deviceset="LED" device="1206" value="Grn"/>
<part name="R3" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="330"/>
<part name="R4" library="SparkFun" deviceset="RESISTOR" device="0603-RES" value="330"/>
<part name="P+3" library="SparkFun" deviceset="VCC" device=""/>
<part name="P+4" library="SparkFun" deviceset="VCC" device=""/>
<part name="C1" library="SparkFun-Capacitors" deviceset="0.1UF-25V(+80/-20%)(0603)" device="" value="0.1uF"/>
<part name="C2" library="SparkFun-Capacitors" deviceset="10UF-16V-10%(TANT)" device="" value="10uF"/>
<part name="GND6" library="SparkFun" deviceset="GND" device=""/>
<part name="GND9" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="U1" library="SparkFun" deviceset="FT232RL" device="SSOP" value="FT232R"/>
<part name="GND11" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND12" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R5" library="SparkFun-Resistors" deviceset="330OHM1/10W1%(0603)" device="" value="330"/>
<part name="R6" library="SparkFun-Resistors" deviceset="330OHM1/10W1%(0603)" device="" value="330"/>
<part name="LED2" library="SparkFun-LED" deviceset="LED" device="1206" value="RED"/>
<part name="LED4" library="SparkFun-LED" deviceset="LED" device="1206" value="GREEN"/>
<part name="F1" library="SparkFun" deviceset="PTC" device="SMD" value="500mA"/>
<part name="P+7" library="supply1" deviceset="+5V" device=""/>
<part name="P+2" library="supply1" deviceset="+5V" device=""/>
<part name="P+6" library="supply1" deviceset="+5V" device=""/>
<part name="U$1" library="AiXWare" deviceset="AIXWARELOGO" device=""/>
<part name="J8" library="JST_AIX" deviceset="JSTPOS4VERSMT" device="" value="B4B-PH-SM4-TB(LF)(SN)"/>
<part name="J2" library="JST_AIX" deviceset="JSTPOS2VERSMT" device=""/>
<part name="J7" library="pinhead" deviceset="PINHD-2X5" device="" value="SFH11-PBPC-D05-ST-BK"/>
<part name="J1" library="JST_AIX" deviceset="JSTPOS2VERSMT" device=""/>
<part name="GND10" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="J5" library="Conector_AIX" deviceset="PJ-018H-SMT-TR" device=""/>
<part name="JP1" library="pinhead" deviceset="PINHD-1X4" device=""/>
<part name="JP2" library="pinhead" deviceset="PINHD-1X2" device=""/>
<part name="J6" library="pinhead" deviceset="PINHD-1X2" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="U$2" library="Misc_AIX" deviceset="TECHMAKELOGO" device=""/>
<part name="JP8" library="Conector_AIX" deviceset="10118192-0001LF" device=""/>
<part name="JC1" library="Misc_AIX" deviceset="CABLECAL12" device=""/>
<part name="Z1" library="Aix_IC" deviceset="SP3222EUEY-L/TR" device=""/>
<part name="C4" library="resistor" deviceset="C-US" device="C0805K"/>
<part name="C5" library="resistor" deviceset="C-US" device="C0805K"/>
<part name="C6" library="resistor" deviceset="C-US" device="C0805K"/>
<part name="C7" library="resistor" deviceset="C-US" device="C0805K"/>
<part name="C8" library="resistor" deviceset="C-US" device="C0805K"/>
<part name="GND13" library="SparkFun" deviceset="GND" device=""/>
<part name="GND14" library="SparkFun" deviceset="GND" device=""/>
<part name="GND15" library="SparkFun" deviceset="GND" device=""/>
<part name="P+8" library="supply1" deviceset="+5V" device=""/>
<part name="GND16" library="SparkFun" deviceset="GND" device=""/>
<part name="GND17" library="SparkFun" deviceset="GND" device=""/>
<part name="GND18" library="SparkFun" deviceset="GND" device=""/>
<part name="P+9" library="supply1" deviceset="+5V" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="208.28" y="7.62" size="1.778" layer="97">Note: The internal regulator can only source 50mA
when running at 3.3V. Keep this in mind when
attempting to power 3.3V devices off of the 3.3V pin. </text>
<wire x1="0" y1="60.96" x2="86.36" y2="60.96" width="0.1524" layer="97" style="longdash"/>
<wire x1="86.36" y1="60.96" x2="86.36" y2="0" width="0.1524" layer="97" style="longdash"/>
<wire x1="86.36" y1="0" x2="0" y2="0" width="0.1524" layer="97" style="longdash"/>
<wire x1="0" y1="0" x2="0" y2="60.96" width="0.1524" layer="97" style="longdash"/>
<wire x1="127" y1="76.2" x2="406.4" y2="76.2" width="0.1524" layer="97" style="longdash"/>
<wire x1="406.4" y1="76.2" x2="406.4" y2="2.54" width="0.1524" layer="97" style="longdash"/>
<wire x1="406.4" y1="2.54" x2="127" y2="2.54" width="0.1524" layer="97" style="longdash"/>
<wire x1="127" y1="2.54" x2="127" y2="76.2" width="0.1524" layer="97" style="longdash"/>
<text x="132.08" y="71.12" size="1.778" layer="97">FTDI</text>
<text x="1.524" y="57.658" size="1.778" layer="97">RS232</text>
</plain>
<instances>
<instance part="J3" gate="G$1" x="2.54" y="127" rot="MR90"/>
<instance part="P+1" gate="VCC" x="-7.62" y="127"/>
<instance part="GND1" gate="1" x="-17.78" y="114.3"/>
<instance part="GND2" gate="1" x="-43.18" y="96.52"/>
<instance part="GND3" gate="1" x="-5.08" y="73.66"/>
<instance part="GND4" gate="1" x="66.04" y="91.44" rot="R90"/>
<instance part="D1" gate="G$1" x="17.78" y="38.1" smashed="yes" rot="R180">
<attribute name="NAME" x="15.494" y="40.8686" size="1.778" layer="95"/>
<attribute name="VALUE" x="12.954" y="34.2646" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="27.94" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="26.924" y="29.845" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="26.924" y="27.051" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R2" gate="G$1" x="30.48" y="45.72" rot="R90"/>
<instance part="GND7" gate="1" x="35.56" y="30.48"/>
<instance part="GND8" gate="1" x="27.94" y="20.32"/>
<instance part="R1" gate="G$1" x="78.74" y="48.26" rot="R90"/>
<instance part="P+5" gate="1" x="58.42" y="55.88"/>
<instance part="U2" gate="G$1" x="45.72" y="48.26" smashed="yes" rot="MR0">
<attribute name="NAME" x="37.338" y="57.15" size="1.778" layer="95" rot="MR270"/>
</instance>
<instance part="LED1" gate="G$1" x="45.72" y="15.24"/>
<instance part="LED3" gate="G$1" x="55.88" y="15.24"/>
<instance part="R3" gate="G$1" x="45.72" y="25.4" rot="R90"/>
<instance part="R4" gate="G$1" x="55.88" y="25.4" rot="R90"/>
<instance part="P+3" gate="1" x="45.72" y="33.02"/>
<instance part="P+4" gate="1" x="55.88" y="33.02"/>
<instance part="C1" gate="G$1" x="187.96" y="48.26"/>
<instance part="C2" gate="G$1" x="198.12" y="50.8"/>
<instance part="GND6" gate="1" x="208.28" y="22.86"/>
<instance part="GND9" gate="1" x="177.8" y="50.8"/>
<instance part="U1" gate="G$1" x="226.06" y="45.72"/>
<instance part="GND11" gate="1" x="187.96" y="38.1"/>
<instance part="GND12" gate="1" x="198.12" y="38.1"/>
<instance part="R5" gate="G$1" x="256.54" y="58.42" rot="R90"/>
<instance part="R6" gate="G$1" x="266.7" y="58.42" rot="R90"/>
<instance part="LED2" gate="G$1" x="256.54" y="48.26"/>
<instance part="LED4" gate="G$1" x="266.7" y="48.26"/>
<instance part="F1" gate="G$1" x="177.8" y="66.04" smashed="yes">
<attribute name="NAME" x="175.26" y="69.088" size="1.778" layer="95"/>
<attribute name="VALUE" x="179.578" y="69.088" size="1.778" layer="96"/>
</instance>
<instance part="P+7" gate="1" x="198.12" y="71.12"/>
<instance part="P+2" gate="1" x="256.54" y="71.12"/>
<instance part="P+6" gate="1" x="266.7" y="71.12"/>
<instance part="U$1" gate="G$1" x="82.677" y="111.2012"/>
<instance part="J8" gate="G$1" x="-20.32" y="81.28" rot="R180"/>
<instance part="J2" gate="G$1" x="17.78" y="104.14" rot="R90"/>
<instance part="J7" gate="A" x="43.18" y="88.9" rot="R180"/>
<instance part="J1" gate="G$1" x="185.42" y="88.9" rot="R90"/>
<instance part="GND10" gate="1" x="187.96" y="76.2"/>
<instance part="J5" gate="G$1" x="-55.88" y="104.14" rot="MR0"/>
<instance part="JP1" gate="A" x="-30.48" y="81.28" rot="R180"/>
<instance part="JP2" gate="G$1" x="185.42" y="99.06" rot="R90"/>
<instance part="J6" gate="G$1" x="17.78" y="114.3" rot="R90"/>
<instance part="GND5" gate="1" x="17.78" y="124.46" rot="R180"/>
<instance part="U$2" gate="G$1" x="112.014" y="111.252"/>
<instance part="JP8" gate="G$1" x="157.48" y="60.96"/>
<instance part="JC1" gate="G$1" x="-30.48" y="119.38" rot="MR0"/>
<instance part="Z1" gate="G$1" x="353.06" y="43.18"/>
<instance part="C4" gate="G$1" x="312.42" y="58.42"/>
<instance part="C5" gate="G$1" x="312.42" y="48.26"/>
<instance part="C6" gate="G$1" x="378.46" y="63.5" rot="R90"/>
<instance part="C7" gate="G$1" x="312.42" y="27.94"/>
<instance part="C8" gate="G$1" x="312.42" y="38.1"/>
<instance part="GND13" gate="1" x="297.18" y="43.18" rot="R270"/>
<instance part="GND14" gate="1" x="332.74" y="22.86"/>
<instance part="GND15" gate="1" x="375.92" y="33.02"/>
<instance part="P+8" gate="1" x="375.92" y="73.66"/>
<instance part="GND16" gate="1" x="396.24" y="50.8" rot="R90"/>
<instance part="GND17" gate="1" x="391.16" y="63.5" rot="R90"/>
<instance part="GND18" gate="1" x="335.28" y="60.96" rot="R270"/>
<instance part="P+9" gate="1" x="368.3" y="63.5"/>
</instances>
<busses>
</busses>
<nets>
<net name="VCC" class="0">
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="-7.62" y1="121.92" x2="-7.62" y2="124.46" width="0.1524" layer="91"/>
<wire x1="2.54" y1="121.92" x2="-2.54" y2="121.92" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="121.92" x2="-7.62" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="121.92" x2="-2.54" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="121.92" x2="-22.86" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="106.68" x2="-48.26" y2="106.68" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="P1"/>
<pinref part="JC1" gate="G$1" pin="+"/>
</segment>
<segment>
<wire x1="78.74" y1="53.34" x2="58.42" y2="53.34" width="0.1524" layer="91"/>
<wire x1="58.42" y1="53.34" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
<wire x1="58.42" y1="53.34" x2="55.88" y2="53.34" width="0.1524" layer="91"/>
<junction x="58.42" y="53.34"/>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="P+5" gate="1" pin="VCC"/>
<pinref part="U2" gate="G$1" pin="E2"/>
</segment>
<segment>
<wire x1="45.72" y1="30.48" x2="45.72" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="P+3" gate="1" pin="VCC"/>
</segment>
<segment>
<wire x1="55.88" y1="30.48" x2="55.88" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="P+4" gate="1" pin="VCC"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="J8" gate="G$1" pin="P1"/>
<wire x1="-5.08" y1="76.2" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="1"/>
<wire x1="-10.16" y1="76.2" x2="-27.94" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="J5" gate="G$1" pin="P3"/>
<pinref part="J5" gate="G$1" pin="P2"/>
<wire x1="-48.26" y1="104.14" x2="-48.26" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="99.06" x2="-48.26" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="63.5" y1="91.44" x2="45.72" y2="91.44" width="0.1524" layer="91"/>
<pinref part="J7" gate="A" pin="7"/>
</segment>
<segment>
<wire x1="35.56" y1="33.02" x2="35.56" y2="43.18" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="U2" gate="G$1" pin="E1"/>
</segment>
<segment>
<wire x1="27.94" y1="22.86" x2="27.94" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="+"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="210.82" y1="38.1" x2="208.28" y2="38.1" width="0.1524" layer="91"/>
<wire x1="208.28" y1="38.1" x2="208.28" y2="35.56" width="0.1524" layer="91"/>
<wire x1="208.28" y1="35.56" x2="210.82" y2="35.56" width="0.1524" layer="91"/>
<wire x1="208.28" y1="35.56" x2="208.28" y2="33.02" width="0.1524" layer="91"/>
<wire x1="208.28" y1="33.02" x2="210.82" y2="33.02" width="0.1524" layer="91"/>
<wire x1="210.82" y1="30.48" x2="208.28" y2="30.48" width="0.1524" layer="91"/>
<wire x1="208.28" y1="30.48" x2="208.28" y2="33.02" width="0.1524" layer="91"/>
<wire x1="210.82" y1="27.94" x2="208.28" y2="27.94" width="0.1524" layer="91"/>
<wire x1="208.28" y1="27.94" x2="208.28" y2="30.48" width="0.1524" layer="91"/>
<wire x1="208.28" y1="25.4" x2="208.28" y2="27.94" width="0.1524" layer="91"/>
<junction x="208.28" y="35.56"/>
<junction x="208.28" y="33.02"/>
<junction x="208.28" y="30.48"/>
<junction x="208.28" y="27.94"/>
<pinref part="U1" gate="G$1" pin="TEST"/>
<pinref part="U1" gate="G$1" pin="AGND"/>
<pinref part="U1" gate="G$1" pin="GND7"/>
<pinref part="U1" gate="G$1" pin="GND18"/>
<pinref part="U1" gate="G$1" pin="GND21"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="187.96" y1="45.72" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="198.12" y1="40.64" x2="198.12" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="-"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="177.8" y1="55.88" x2="177.8" y2="53.34" width="0.1524" layer="91"/>
<pinref part="JP8" gate="G$1" pin="GND"/>
<wire x1="177.8" y1="55.88" x2="167.64" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="P2"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="187.96" y1="81.28" x2="187.96" y2="78.74" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="2"/>
<wire x1="187.96" y1="81.28" x2="185.42" y2="81.28" width="0.1524" layer="91"/>
<wire x1="185.42" y1="81.28" x2="185.42" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="JC1" gate="G$1" pin="-"/>
<wire x1="-17.78" y1="116.84" x2="-22.86" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="P2"/>
<pinref part="J6" gate="G$1" pin="2"/>
<wire x1="20.32" y1="96.52" x2="17.78" y2="96.52" width="0.1524" layer="91"/>
<wire x1="17.78" y1="96.52" x2="17.78" y2="111.76" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="17.78" y1="111.76" x2="17.78" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="312.42" y1="43.18" x2="302.26" y2="43.18" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="302.26" y1="43.18" x2="299.72" y2="43.18" width="0.1524" layer="91"/>
<wire x1="312.42" y1="30.48" x2="302.26" y2="30.48" width="0.1524" layer="91"/>
<wire x1="302.26" y1="30.48" x2="302.26" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="R2IN"/>
<wire x1="337.82" y1="35.56" x2="332.74" y2="35.56" width="0.1524" layer="91"/>
<wire x1="332.74" y1="35.56" x2="332.74" y2="25.4" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="T2IN"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="368.3" y1="35.56" x2="375.92" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="GND"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="368.3" y1="50.8" x2="393.7" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="388.62" y1="63.5" x2="383.54" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="EN"/>
<wire x1="337.82" y1="55.88" x2="337.82" y2="60.96" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
</segment>
</net>
<net name="RS_OUT" class="0">
<segment>
<label x="33.02" y="88.9" size="1.778" layer="95" rot="R180"/>
<wire x1="27.94" y1="86.36" x2="38.1" y2="86.36" width="0.1524" layer="91"/>
<pinref part="J7" gate="A" pin="4"/>
</segment>
<segment>
<wire x1="30.48" y1="50.8" x2="30.48" y2="53.34" width="0.1524" layer="91"/>
<wire x1="30.48" y1="53.34" x2="35.56" y2="53.34" width="0.1524" layer="91"/>
<wire x1="30.48" y1="53.34" x2="10.16" y2="53.34" width="0.1524" layer="91"/>
<junction x="30.48" y="53.34"/>
<label x="7.62" y="53.34" size="1.778" layer="95"/>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="C2"/>
</segment>
</net>
<net name="RS_IN" class="0">
<segment>
<label x="50.8" y="88.9" size="1.778" layer="95"/>
<wire x1="55.88" y1="88.9" x2="45.72" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J7" gate="A" pin="5"/>
</segment>
<segment>
<wire x1="15.24" y1="38.1" x2="10.16" y2="38.1" width="0.1524" layer="91"/>
<wire x1="35.56" y1="48.26" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<wire x1="33.02" y1="48.26" x2="33.02" y2="7.62" width="0.1524" layer="91"/>
<wire x1="33.02" y1="7.62" x2="10.16" y2="7.62" width="0.1524" layer="91"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="38.1" width="0.1524" layer="91"/>
<wire x1="10.16" y1="38.1" x2="2.54" y2="38.1" width="0.1524" layer="91"/>
<junction x="10.16" y="38.1"/>
<label x="2.54" y="38.1" size="1.778" layer="95"/>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="U2" gate="G$1" pin="B1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="27.94" y1="35.56" x2="27.94" y2="38.1" width="0.1524" layer="91"/>
<wire x1="27.94" y1="38.1" x2="30.48" y2="38.1" width="0.1524" layer="91"/>
<wire x1="30.48" y1="38.1" x2="30.48" y2="40.64" width="0.1524" layer="91"/>
<wire x1="20.32" y1="38.1" x2="27.94" y2="38.1" width="0.1524" layer="91"/>
<junction x="27.94" y="38.1"/>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="C3" gate="G$1" pin="-"/>
<pinref part="D1" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="45.72" y1="17.78" x2="45.72" y2="20.32" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="A"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="55.88" y1="17.78" x2="55.88" y2="20.32" width="0.1524" layer="91"/>
<pinref part="LED3" gate="G$1" pin="A"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="USB_D-" class="0">
<segment>
<wire x1="210.82" y1="63.5" x2="167.64" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="USBDM"/>
<label x="190.5" y="63.5" size="1.778" layer="95"/>
<pinref part="JP8" gate="G$1" pin="D-"/>
</segment>
</net>
<net name="USB_D+" class="0">
<segment>
<wire x1="210.82" y1="60.96" x2="167.64" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="USBDP"/>
<label x="190.5" y="60.96" size="1.778" layer="95"/>
<pinref part="JP8" gate="G$1" pin="D+"/>
</segment>
</net>
<net name="RTS" class="0">
<segment>
<wire x1="241.3" y1="53.34" x2="246.38" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RTS"/>
<label x="246.38" y="53.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CTS" class="0">
<segment>
<wire x1="241.3" y1="55.88" x2="246.38" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="CTS"/>
<label x="246.38" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="DTR" class="0">
<segment>
<wire x1="241.3" y1="50.8" x2="246.38" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="DTR"/>
<label x="246.38" y="50.8" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="DSR" class="0">
<segment>
<wire x1="241.3" y1="48.26" x2="246.38" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="DSR"/>
<label x="246.38" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="DCD" class="0">
<segment>
<wire x1="241.3" y1="45.72" x2="246.38" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="DCD"/>
<label x="246.38" y="45.72" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RI" class="0">
<segment>
<wire x1="241.3" y1="43.18" x2="246.38" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RI"/>
<label x="246.38" y="43.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TXLED" class="0">
<segment>
<wire x1="241.3" y1="38.1" x2="256.54" y2="38.1" width="0.1524" layer="91"/>
<wire x1="256.54" y1="38.1" x2="256.54" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="TXLED"/>
<pinref part="LED2" gate="G$1" pin="C"/>
</segment>
</net>
<net name="RXLED" class="0">
<segment>
<wire x1="241.3" y1="35.56" x2="266.7" y2="35.56" width="0.1524" layer="91"/>
<wire x1="266.7" y1="35.56" x2="266.7" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RXLED"/>
<pinref part="LED4" gate="G$1" pin="C"/>
</segment>
</net>
<net name="PWREN" class="0">
<segment>
<wire x1="241.3" y1="33.02" x2="246.38" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PWREN"/>
<label x="246.38" y="33.02" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TXDEN" class="0">
<segment>
<wire x1="241.3" y1="30.48" x2="246.38" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="TXDEN"/>
<label x="246.38" y="30.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SLEEP" class="0">
<segment>
<wire x1="241.3" y1="27.94" x2="246.38" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="SLEEP"/>
<label x="246.38" y="27.94" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="256.54" y1="53.34" x2="256.54" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="LED2" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="266.7" y1="53.34" x2="266.7" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="LED4" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="F1" gate="G$1" pin="1"/>
<pinref part="JP8" gate="G$1" pin="VCC"/>
<wire x1="172.72" y1="66.04" x2="167.64" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RX_FTDI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RXD"/>
<wire x1="241.3" y1="60.96" x2="246.38" y2="60.96" width="0.1524" layer="91"/>
<label x="243.84" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="R1IN"/>
<wire x1="368.3" y1="45.72" x2="375.92" y2="45.72" width="0.1524" layer="91"/>
<label x="375.92" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX_FTDI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="TXD"/>
<wire x1="241.3" y1="63.5" x2="246.38" y2="63.5" width="0.1524" layer="91"/>
<label x="243.84" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="T1IN"/>
<wire x1="368.3" y1="38.1" x2="375.92" y2="38.1" width="0.1524" layer="91"/>
<label x="375.92" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX_RS232" class="0">
<segment>
<label x="12.192" y="83.058" size="1.778" layer="95" rot="R180"/>
<pinref part="J8" gate="G$1" pin="P3"/>
<wire x1="2.54" y1="81.28" x2="-10.16" y2="81.28" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="3"/>
<wire x1="-10.16" y1="81.28" x2="-27.94" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="55.88" y1="48.26" x2="66.04" y2="48.26" width="0.1524" layer="91"/>
<label x="60.96" y="48.26" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="B2"/>
</segment>
<segment>
<wire x1="45.72" y1="10.16" x2="45.72" y2="7.62" width="0.1524" layer="91"/>
<wire x1="45.72" y1="7.62" x2="40.64" y2="7.62" width="0.1524" layer="91"/>
<label x="40.64" y="7.62" size="1.778" layer="95"/>
<pinref part="LED1" gate="G$1" pin="C"/>
</segment>
</net>
<net name="TX_RS232" class="0">
<segment>
<label x="11.938" y="77.978" size="1.778" layer="95" rot="R180"/>
<wire x1="2.54" y1="78.74" x2="-10.16" y2="78.74" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="P2"/>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="-10.16" y1="78.74" x2="-27.94" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="78.74" y1="43.18" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
<wire x1="66.04" y1="43.18" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<wire x1="66.04" y1="43.18" x2="66.04" y2="35.56" width="0.1524" layer="91"/>
<junction x="66.04" y="43.18"/>
<label x="66.04" y="35.56" size="1.778" layer="95"/>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="C1"/>
</segment>
<segment>
<wire x1="55.88" y1="10.16" x2="55.88" y2="7.62" width="0.1524" layer="91"/>
<wire x1="55.88" y1="7.62" x2="60.96" y2="7.62" width="0.1524" layer="91"/>
<label x="58.42" y="7.62" size="1.778" layer="95"/>
<pinref part="LED3" gate="G$1" pin="C"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="256.54" y1="68.58" x2="256.54" y2="63.5" width="0.1524" layer="91"/>
<pinref part="P+2" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="266.7" y1="63.5" x2="266.7" y2="68.58" width="0.1524" layer="91"/>
<pinref part="P+6" gate="1" pin="+5V"/>
</segment>
<segment>
<wire x1="187.96" y1="66.04" x2="190.5" y2="66.04" width="0.1524" layer="91"/>
<wire x1="190.5" y1="66.04" x2="198.12" y2="66.04" width="0.1524" layer="91"/>
<wire x1="198.12" y1="66.04" x2="218.44" y2="66.04" width="0.1524" layer="91"/>
<wire x1="218.44" y1="66.04" x2="218.44" y2="55.88" width="0.1524" layer="91"/>
<wire x1="218.44" y1="55.88" x2="210.82" y2="55.88" width="0.1524" layer="91"/>
<wire x1="198.12" y1="53.34" x2="198.12" y2="66.04" width="0.1524" layer="91"/>
<wire x1="187.96" y1="53.34" x2="187.96" y2="66.04" width="0.1524" layer="91"/>
<wire x1="185.42" y1="66.04" x2="187.96" y2="66.04" width="0.1524" layer="91"/>
<junction x="198.12" y="66.04"/>
<junction x="187.96" y="66.04"/>
<label x="203.2" y="55.88" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="VCC"/>
<pinref part="C2" gate="G$1" pin="+"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="F1" gate="G$1" pin="2"/>
<wire x1="218.44" y1="55.88" x2="218.44" y2="50.8" width="0.1524" layer="91" style="longdash"/>
<pinref part="U1" gate="G$1" pin="VCCIO"/>
<wire x1="218.44" y1="50.8" x2="210.82" y2="50.8" width="0.1524" layer="91" style="longdash"/>
<pinref part="P+7" gate="1" pin="+5V"/>
<wire x1="198.12" y1="66.04" x2="198.12" y2="68.58" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="P1"/>
<wire x1="182.88" y1="81.28" x2="182.88" y2="73.66" width="0.1524" layer="91"/>
<wire x1="182.88" y1="73.66" x2="190.5" y2="73.66" width="0.1524" layer="91"/>
<wire x1="190.5" y1="73.66" x2="190.5" y2="66.04" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="182.88" y1="81.28" x2="182.88" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="VCC"/>
<wire x1="368.3" y1="53.34" x2="375.92" y2="53.34" width="0.1524" layer="91"/>
<wire x1="375.92" y1="53.34" x2="375.92" y2="63.5" width="0.1524" layer="91"/>
<pinref part="P+8" gate="1" pin="+5V"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="375.92" y1="63.5" x2="375.92" y2="71.12" width="0.1524" layer="91"/>
<junction x="375.92" y="63.5"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="SHDN"/>
<wire x1="368.3" y1="55.88" x2="368.3" y2="60.96" width="0.1524" layer="91"/>
<pinref part="P+9" gate="1" pin="+5V"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="1"/>
<pinref part="J2" gate="G$1" pin="P1"/>
<wire x1="15.24" y1="96.52" x2="15.24" y2="111.76" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="38.1" y1="91.44" x2="15.24" y2="91.44" width="0.1524" layer="91"/>
<wire x1="15.24" y1="91.44" x2="5.08" y2="91.44" width="0.1524" layer="91"/>
<wire x1="5.08" y1="91.44" x2="5.08" y2="83.82" width="0.1524" layer="91"/>
<wire x1="5.08" y1="83.82" x2="-10.16" y2="83.82" width="0.1524" layer="91"/>
<wire x1="5.08" y1="91.44" x2="5.08" y2="121.92" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="P4"/>
<pinref part="J7" gate="A" pin="8"/>
<pinref part="JP1" gate="A" pin="4"/>
<wire x1="-10.16" y1="83.82" x2="-27.94" y2="83.82" width="0.1524" layer="91"/>
<wire x1="15.24" y1="96.52" x2="15.24" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="C1+"/>
<wire x1="337.82" y1="53.34" x2="320.04" y2="53.34" width="0.1524" layer="91"/>
<wire x1="320.04" y1="53.34" x2="320.04" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="320.04" y1="60.96" x2="312.42" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="V+"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="337.82" y1="50.8" x2="312.42" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="C1-"/>
<wire x1="337.82" y1="48.26" x2="317.5" y2="48.26" width="0.1524" layer="91"/>
<wire x1="317.5" y1="48.26" x2="317.5" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="317.5" y1="53.34" x2="312.42" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="C2+"/>
<wire x1="337.82" y1="45.72" x2="320.04" y2="45.72" width="0.1524" layer="91"/>
<wire x1="320.04" y1="45.72" x2="320.04" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="320.04" y1="40.64" x2="312.42" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="C2-"/>
<wire x1="337.82" y1="43.18" x2="322.58" y2="43.18" width="0.1524" layer="91"/>
<wire x1="322.58" y1="43.18" x2="322.58" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="322.58" y1="33.02" x2="312.42" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="V-"/>
<wire x1="337.82" y1="40.64" x2="327.66" y2="40.64" width="0.1524" layer="91"/>
<wire x1="327.66" y1="40.64" x2="327.66" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="327.66" y1="22.86" x2="312.42" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX_INERTIAL" class="0">
<segment>
<pinref part="J7" gate="A" pin="2"/>
<wire x1="38.1" y1="83.82" x2="27.94" y2="83.82" width="0.1524" layer="91"/>
<label x="20.32" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="T1OUT"/>
<wire x1="368.3" y1="48.26" x2="375.92" y2="48.26" width="0.1524" layer="91"/>
<label x="375.92" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX_INERTIAL" class="0">
<segment>
<pinref part="Z1" gate="G$1" pin="R1OUT"/>
<wire x1="368.3" y1="43.18" x2="375.92" y2="43.18" width="0.1524" layer="91"/>
<label x="375.92" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J7" gate="A" pin="3"/>
<wire x1="45.72" y1="86.36" x2="55.88" y2="86.36" width="0.1524" layer="91"/>
<label x="50.8" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
